var namespaceDigikamGenericGLViewerPlugin =
[
    [ "GLViewerHelpDlg", "classDigikamGenericGLViewerPlugin_1_1GLViewerHelpDlg.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerHelpDlg" ],
    [ "GLViewerPlugin", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin" ],
    [ "GLViewerTexture", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture" ],
    [ "GLViewerTimer", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer" ],
    [ "GLViewerWidget", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget" ]
];