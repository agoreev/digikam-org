var classUi__PresentationAdvPage =
[
    [ "retranslateUi", "classUi__PresentationAdvPage.html#aab4145f2e7c07674a802cf20d7e36938", null ],
    [ "setupUi", "classUi__PresentationAdvPage.html#a5e981bb45b76dfbb2f1eeb3d0559b4e2", null ],
    [ "groupBox", "classUi__PresentationAdvPage.html#ac87592cbe888c25c2cd54bf738b7b1ac", null ],
    [ "groupBox_2", "classUi__PresentationAdvPage.html#ae56a4f2025acba7df829ce1e30fd9453", null ],
    [ "groupBox_3", "classUi__PresentationAdvPage.html#a04767a5651d5a6d3249dc3367d7baca2", null ],
    [ "groupBox_4", "classUi__PresentationAdvPage.html#acf0ce0cf776a5a37616e051371fae71d", null ],
    [ "horizontalLayout_2", "classUi__PresentationAdvPage.html#ade87fa48af800e23325a7dffa2751c08", null ],
    [ "m_enableMouseWheelCheckBox", "classUi__PresentationAdvPage.html#a4275be2adb6f08d14928b4c117be19e5", null ],
    [ "m_kbDisableCrossfadeCheckBox", "classUi__PresentationAdvPage.html#ab880761723171afd3b0fcf9f1e5c9025", null ],
    [ "m_kbDisableFadeCheckBox", "classUi__PresentationAdvPage.html#a665e44b5ee843bae36967ea8220b23a7", null ],
    [ "m_openGlFullScale", "classUi__PresentationAdvPage.html#ad068b352532f075cc4a687ca5df31624", null ],
    [ "m_useMillisecondsCheckBox", "classUi__PresentationAdvPage.html#abcd1074fbbe04496bb28a55d9b5cce15", null ],
    [ "verticalLayout", "classUi__PresentationAdvPage.html#aba698d3e15d0cf128ae2244ac923c1bd", null ],
    [ "verticalLayout_2", "classUi__PresentationAdvPage.html#a369263c100534dcc0173152561c25238", null ],
    [ "verticalLayout_3", "classUi__PresentationAdvPage.html#ad8e68d1a3f2c15ffc41d5f92e54400ee", null ],
    [ "verticalLayout_4", "classUi__PresentationAdvPage.html#a99502d14dd710961dc40eaafd79bf059", null ],
    [ "verticalSpacer_3", "classUi__PresentationAdvPage.html#a75865660a6321136654f8efa00a54f01", null ]
];