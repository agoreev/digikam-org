var classDigikam_1_1LoadSaveTask =
[
    [ "TaskType", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "LoadSaveTask", "classDigikam_1_1LoadSaveTask.html#acbdeedac61bd171264bb13b9654ca67d", null ],
    [ "~LoadSaveTask", "classDigikam_1_1LoadSaveTask.html#ae32431108e0e489cf37b45dae6269ef2", null ],
    [ "continueQuery", "classDigikam_1_1LoadSaveTask.html#a07990aca5b5dd5898a8dbcc8544e4aa3", null ],
    [ "execute", "classDigikam_1_1LoadSaveTask.html#af5f621afea3dacd2941154ff9df8e6a0", null ],
    [ "progressInfo", "classDigikam_1_1LoadSaveTask.html#a39350865af8fc2a8ab79cde74f098784", null ],
    [ "type", "classDigikam_1_1LoadSaveTask.html#ae15afbd0011ee301fc4230ba16477ae0", null ],
    [ "m_thread", "classDigikam_1_1LoadSaveTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ]
];