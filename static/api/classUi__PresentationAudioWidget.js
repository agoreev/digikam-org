var classUi__PresentationAudioWidget =
[
    [ "retranslateUi", "classUi__PresentationAudioWidget.html#a7076a1e4767bf1500067a3aaef3b0ad5", null ],
    [ "setupUi", "classUi__PresentationAudioWidget.html#ad5d03a2c03331f293a2c75231a2e7f58", null ],
    [ "gridLayout", "classUi__PresentationAudioWidget.html#ab21e9b9c6d29733fa8d7308acf6fcd4e", null ],
    [ "gridLayout_2", "classUi__PresentationAudioWidget.html#a5de9fe1e1e4acf48570777a161550aae", null ],
    [ "horizontalLayout", "classUi__PresentationAudioWidget.html#a982a4f8cf6441f99835a41a6d892468e", null ],
    [ "horizontalLayout_2", "classUi__PresentationAudioWidget.html#a7a59ab09b3fac8cd6b082b146dabeadd", null ],
    [ "label", "classUi__PresentationAudioWidget.html#af241f44f67a7af1a7fb20ae0b83bb1aa", null ],
    [ "label_2", "classUi__PresentationAudioWidget.html#a7fd86dd9ff444ec318aea1347f4d6382", null ],
    [ "label_3", "classUi__PresentationAudioWidget.html#a2d86c398fa0c7eb11d0e2e2cfe479abe", null ],
    [ "m_elapsedTimeLabel", "classUi__PresentationAudioWidget.html#a38430ad35d40d0f590d32e8103488676", null ],
    [ "m_nextButton", "classUi__PresentationAudioWidget.html#a708d2d0082679c917fda8dab50573222", null ],
    [ "m_playButton", "classUi__PresentationAudioWidget.html#a59bf894a6802f2aaa3925274dd119ab2", null ],
    [ "m_prevButton", "classUi__PresentationAudioWidget.html#a50d452aaa2778b2616d6cf8bd9fa343b", null ],
    [ "m_soundLabel", "classUi__PresentationAudioWidget.html#a91ee5a7465234ed0b652cab20384d06c", null ],
    [ "m_stopButton", "classUi__PresentationAudioWidget.html#a8d6ee259ada690863c846a32ca8006cf", null ],
    [ "m_totalTimeLabel", "classUi__PresentationAudioWidget.html#aac847c686c1ad9727a3a621988074e88", null ],
    [ "m_volumeWidget", "classUi__PresentationAudioWidget.html#a114f9255cc7b5ff57802a015bd78b19e", null ],
    [ "verticalLayout", "classUi__PresentationAudioWidget.html#a1892f763c07d2278ef9083c39688bf84", null ],
    [ "verticalLayout_2", "classUi__PresentationAudioWidget.html#a9280c9750234702d35f408f784aba1b2", null ]
];