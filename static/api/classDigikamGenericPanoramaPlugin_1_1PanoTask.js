var classDigikamGenericPanoramaPlugin_1_1PanoTask =
[
    [ "PanoTask", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#afd7554ce7e87a59232ab4f2d20f40f3e", null ],
    [ "~PanoTask", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#a8ef457cc6ceb71f93c228a29f7c0055d", null ],
    [ "requestAbort", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#a15228bf2da096a55ae6de3708b8757c8", null ],
    [ "success", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#a4ff48dab112fef323ebf31de15e9de77", null ],
    [ "action", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#af949ba46cb0bb012617393355e4ea84d", null ],
    [ "errString", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#aa90275e853288356e82aa20533743979", null ],
    [ "isAbortedFlag", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#a76d4c758d68120c53b8f97de2dceb215", null ],
    [ "successFlag", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#a1aa4f8297647e81f691f581f8bc62395", null ],
    [ "tmpDir", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html#ae832263dbe52631964f42cec91671e34", null ]
];