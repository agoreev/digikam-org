var classDigikamGenericJAlbumPlugin_1_1JAlbumSettings =
[
    [ "ImageGetOption", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#abaf427642a0db6b767276fa5cac1e350", [
      [ "ALBUMS", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#abaf427642a0db6b767276fa5cac1e350aa22d365cf6eabfc9d0dc8ccec80db68e", null ],
      [ "IMAGES", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#abaf427642a0db6b767276fa5cac1e350a7fc227cae5ccc2b42378a5f23960ee5c", null ]
    ] ],
    [ "JAlbumSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#ac73403eb9e120e00c31e73941a353e66", null ],
    [ "~JAlbumSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#ada89d95ae9f21c78f11e07e1ad9b44f6", null ],
    [ "readSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#ae60998e136f80406c4016496adbc9ef5", null ],
    [ "writeSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a50ba254373634fe6b5f3787ead8952fc", null ],
    [ "m_albumList", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a5b1e7fa8fd6dd04a59161a946727bbb8", null ],
    [ "m_destPath", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a97553672852eed30534227744566e9b5", null ],
    [ "m_getOption", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a380eecfeeb908a3d2e2fccd500f0ff63", null ],
    [ "m_iface", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a9b3fb1b0cbe37b7afe02dfd6c86d68fe", null ],
    [ "m_imageList", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a4dd993cae5afc94750c8fcc62f3f628c", null ],
    [ "m_imageSelectionTitle", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#aa2e20d15a6ee14d1814ba264c254ee01", null ],
    [ "m_jalbumPath", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#a81df0fabfade3a1280bbd96ce8a4d145", null ],
    [ "m_javaPath", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html#ad66c0f38e0ab258b49d77c8ccc2f91ba", null ]
];