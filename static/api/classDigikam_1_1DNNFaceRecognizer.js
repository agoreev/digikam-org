var classDigikam_1_1DNNFaceRecognizer =
[
    [ "DNNFaceRecognizer", "classDigikam_1_1DNNFaceRecognizer.html#a74b59bdf35624657c977111f9c9c5d9f", null ],
    [ "DNNFaceRecognizer", "classDigikam_1_1DNNFaceRecognizer.html#a7e2dcbc8c5fd7228c974ab00e046849a", null ],
    [ "~DNNFaceRecognizer", "classDigikam_1_1DNNFaceRecognizer.html#a53e574f89470f59fb85a40803a32f61c", null ],
    [ "getLabels", "classDigikam_1_1DNNFaceRecognizer.html#aae7cbd94aeffd3d4c66d3c21c6ba8cc1", null ],
    [ "getSrc", "classDigikam_1_1DNNFaceRecognizer.html#ae28a9d4f7da61e8bf1c46f68297c2242", null ],
    [ "getThreshold", "classDigikam_1_1DNNFaceRecognizer.html#a80628057a81c9384bd8d97d20698498b", null ],
    [ "predict", "classDigikam_1_1DNNFaceRecognizer.html#a69d9db2ba4ed7d24d62d9aa2aadcd028", null ],
    [ "predict", "classDigikam_1_1DNNFaceRecognizer.html#a244a2699baf7780255058c341b4f012c", null ],
    [ "setLabels", "classDigikam_1_1DNNFaceRecognizer.html#aa4741e79ac916a66b74e33c8de5cc653", null ],
    [ "setSrc", "classDigikam_1_1DNNFaceRecognizer.html#a9fb2c546902e2e44100bf043c14ace6b", null ],
    [ "setThreshold", "classDigikam_1_1DNNFaceRecognizer.html#a71673cb09630db5643c0ec948feff07a", null ],
    [ "train", "classDigikam_1_1DNNFaceRecognizer.html#aeeeef704fbbbaaf004a649bc2d6fa168", null ],
    [ "update", "classDigikam_1_1DNNFaceRecognizer.html#ad97df98e47c283acc988c4b894abc059", null ]
];