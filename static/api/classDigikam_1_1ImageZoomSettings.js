var classDigikam_1_1ImageZoomSettings =
[
    [ "FitToSizeMode", "classDigikam_1_1ImageZoomSettings.html#a44a43d11a416bfce1a66bb3d32d26f2b", [
      [ "AlwaysFit", "classDigikam_1_1ImageZoomSettings.html#a44a43d11a416bfce1a66bb3d32d26f2ba7cde96545c34447a1a67cb2af824ca7c", null ],
      [ "OnlyScaleDown", "classDigikam_1_1ImageZoomSettings.html#a44a43d11a416bfce1a66bb3d32d26f2ba4bd82c8f77d69f4ec416bb6341af28a2", null ]
    ] ],
    [ "ImageZoomSettings", "classDigikam_1_1ImageZoomSettings.html#afbc2cdcc6ac3f6e0f5ce9bc8ee2848b8", null ],
    [ "ImageZoomSettings", "classDigikam_1_1ImageZoomSettings.html#a6ee7aa2b9426e71723b4c9bfde24b15f", null ],
    [ "fitToSize", "classDigikam_1_1ImageZoomSettings.html#a1bc1364bfc80f6faa17413f715de5375", null ],
    [ "fitToSizeZoomFactor", "classDigikam_1_1ImageZoomSettings.html#aeef5bd55e3b1d93685036a6ba0edfdc0", null ],
    [ "imageSize", "classDigikam_1_1ImageZoomSettings.html#a7e42bdfe343996ea7534eaaa0742ec31", null ],
    [ "isFitToSize", "classDigikam_1_1ImageZoomSettings.html#a33d7cc451e923e1b0b2f3a08c1b8b626", null ],
    [ "mapImageToZoom", "classDigikam_1_1ImageZoomSettings.html#a4b9662c5fb035e5fc9e6bc1ace86a6a9", null ],
    [ "mapImageToZoom", "classDigikam_1_1ImageZoomSettings.html#ac8dd00433d452d5c86f1b9ace10e93ff", null ],
    [ "mapZoomToImage", "classDigikam_1_1ImageZoomSettings.html#ab2e814b8e201844bd3086bbc477fa374", null ],
    [ "mapZoomToImage", "classDigikam_1_1ImageZoomSettings.html#ac461c07023590661a24444aeb39b51b9", null ],
    [ "originalImageSize", "classDigikam_1_1ImageZoomSettings.html#a395fc50dfccfb5adfa43468035a084db", null ],
    [ "realZoomFactor", "classDigikam_1_1ImageZoomSettings.html#a3e63b192da2330a5aa3e5357e481a647", null ],
    [ "setImageSize", "classDigikam_1_1ImageZoomSettings.html#a2656271f2e341436b438e5715930383f", null ],
    [ "setZoomFactor", "classDigikam_1_1ImageZoomSettings.html#a74cc222a044a8068c6a803dfd988ab5b", null ],
    [ "snappedZoomFactor", "classDigikam_1_1ImageZoomSettings.html#a3a2f938d7a455d7c6d317fe3cbb8ec02", null ],
    [ "snappedZoomStep", "classDigikam_1_1ImageZoomSettings.html#ac9c7c0c54b6042fbc3cde4b1cf6f7e7e", null ],
    [ "sourceRect", "classDigikam_1_1ImageZoomSettings.html#a98b880f03a1146663facf80fa40bfda3", null ],
    [ "zoomedSize", "classDigikam_1_1ImageZoomSettings.html#ac38ae898234ff3eca1370700440c073a", null ],
    [ "zoomFactor", "classDigikam_1_1ImageZoomSettings.html#a88c12b157f42b9dd8c26a053cbcc6ff1", null ],
    [ "m_size", "classDigikam_1_1ImageZoomSettings.html#a52aca236ba3cfeb792851ac90d548f69", null ],
    [ "m_zoom", "classDigikam_1_1ImageZoomSettings.html#a83add37802f511d8bc5565f68dc2acc0", null ],
    [ "m_zoomConst", "classDigikam_1_1ImageZoomSettings.html#a23ab05b7f0b796a50ad39ad5aacbbade", null ],
    [ "m_zoomRatio", "classDigikam_1_1ImageZoomSettings.html#a9b76b919257477d74cc9ab668c0e268a", null ]
];