var classDigikam_1_1DImgBuiltinFilter =
[
    [ "Type", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850ef", [
      [ "NoOperation", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa950da442a67e4e2813491ad62ebc0171", null ],
      [ "Rotate90", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa63ed4d76665d9d93e0203848d4b8055e", null ],
      [ "Rotate180", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efafefbf6b039eee244110cd0b41d08a058", null ],
      [ "Rotate270", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa6b79740155b298a0465698de51cdd9e0", null ],
      [ "FlipHorizontally", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efabb0bf60d3f4ef980dffa3cb3e7dd7684", null ],
      [ "FlipVertically", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa516d097fb25645442ca4b870d5ab141a", null ],
      [ "Crop", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efaf31d35b9a59a98558020ef780b7d9fa1", null ],
      [ "Resize", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa58926657ef9519fb764f920d4fe064d5", null ],
      [ "ConvertTo8Bit", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efad04fd4c47d9b5e08371b1b70611231d6", null ],
      [ "ConvertTo16Bit", "classDigikam_1_1DImgBuiltinFilter.html#aa998a7ced6f5296c86627690a30850efa5be893fe77373bad7afe660b69332118", null ]
    ] ],
    [ "DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html#a7a831412f72c111ae250859e1a9bb940", null ],
    [ "DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html#ac78e51db47082684fe9f4cdb7a387fc7", null ],
    [ "DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html#a323da63bebc1afe998b20f9a12eeba09", null ],
    [ "apply", "classDigikam_1_1DImgBuiltinFilter.html#a959644daed67c714ca378413ac36a469", null ],
    [ "createThreadedFilter", "classDigikam_1_1DImgBuiltinFilter.html#ad79c859df175d55dc597400e287a0d9f", null ],
    [ "createThreadedFilter", "classDigikam_1_1DImgBuiltinFilter.html#aee335814d66dc6fa240c7e571d733999", null ],
    [ "displayableName", "classDigikam_1_1DImgBuiltinFilter.html#aec3f7acc8350299794eb228b906b9216", null ],
    [ "filterAction", "classDigikam_1_1DImgBuiltinFilter.html#a291b2d9d910b65759956461d5a66858c", null ],
    [ "filterIcon", "classDigikam_1_1DImgBuiltinFilter.html#a91fb118ccf2d0241299421f6fbb81057", null ],
    [ "i18nDisplayableName", "classDigikam_1_1DImgBuiltinFilter.html#adc5b4eaefe1f71c81137030019688647", null ],
    [ "isReversible", "classDigikam_1_1DImgBuiltinFilter.html#a85caa2a50fb8ecb4109eaf1ebad01094", null ],
    [ "isValid", "classDigikam_1_1DImgBuiltinFilter.html#a8543c3136b887684fa3b3d4427a8ef63", null ],
    [ "reverseFilter", "classDigikam_1_1DImgBuiltinFilter.html#a2ad67e194b51bbaa8d1901d020150142", null ],
    [ "setAction", "classDigikam_1_1DImgBuiltinFilter.html#a06aa9c90dee74ffb2bdce7517af3fa27", null ],
    [ "setAction", "classDigikam_1_1DImgBuiltinFilter.html#a4c06adc07b66cf937224f5fa0f7437cb", null ],
    [ "m_arg", "classDigikam_1_1DImgBuiltinFilter.html#a33adcb273b5a58b627713b5d8711ea71", null ],
    [ "m_type", "classDigikam_1_1DImgBuiltinFilter.html#a5047b2d8dc9e2e821d8fe41bece5fa68", null ]
];