var classDigikam_1_1Ellipsoid =
[
    [ "Ellipsoid", "classDigikam_1_1Ellipsoid.html#a6e0de4c1f7d2891b50de698dfbe1920d", null ],
    [ "Ellipsoid", "classDigikam_1_1Ellipsoid.html#a1e1484050cce7f4c533f80507ff0f29b", null ],
    [ "eccentricity", "classDigikam_1_1Ellipsoid.html#abd6bd8c0f494738e1c43d5a77d46ae2f", null ],
    [ "inverseFlattening", "classDigikam_1_1Ellipsoid.html#a768df601d21ad3610bbbedc08a233d5c", null ],
    [ "isIvfDefinitive", "classDigikam_1_1Ellipsoid.html#abcd82d1d95691fc9bddd5982be07486e", null ],
    [ "isSphere", "classDigikam_1_1Ellipsoid.html#a33943711f33aaa2d5269bd0a06b33855", null ],
    [ "orthodromicDistance", "classDigikam_1_1Ellipsoid.html#af57370a775d22e87ddf7905842fab469", null ],
    [ "radiusOfCurvature", "classDigikam_1_1Ellipsoid.html#a2af936e1cbe400e9a92e635adf6f02ca", null ],
    [ "semiMajorAxis", "classDigikam_1_1Ellipsoid.html#ac91347bbbed743c436ea63d8ecc84402", null ],
    [ "semiMinorAxis", "classDigikam_1_1Ellipsoid.html#a6b8da8b95cdbb496f5487705eb566d44", null ],
    [ "m_inverseFlattening", "classDigikam_1_1Ellipsoid.html#aab947118f543e2abb2a095d5e8f45702", null ],
    [ "m_isSphere", "classDigikam_1_1Ellipsoid.html#a9e000396327658d6472a7843836f7680", null ],
    [ "m_ivfDefinitive", "classDigikam_1_1Ellipsoid.html#a4d7f220d500e8436fbf6c92fb752a4cd", null ],
    [ "m_semiMajorAxis", "classDigikam_1_1Ellipsoid.html#ad6a5636c488a47fc3828e7c1c30131a2", null ],
    [ "m_semiMinorAxis", "classDigikam_1_1Ellipsoid.html#adcf6f22f4c90ac2e44137a24ef85a561", null ],
    [ "name", "classDigikam_1_1Ellipsoid.html#acd8c56989304b30545af2b1c2704a3a7", null ]
];