var structDigikam_1_1PTOType =
[
    [ "ControlPoint", "structDigikam_1_1PTOType_1_1ControlPoint.html", "structDigikam_1_1PTOType_1_1ControlPoint" ],
    [ "Image", "structDigikam_1_1PTOType_1_1Image.html", "structDigikam_1_1PTOType_1_1Image" ],
    [ "Mask", "structDigikam_1_1PTOType_1_1Mask.html", "structDigikam_1_1PTOType_1_1Mask" ],
    [ "Optimization", "structDigikam_1_1PTOType_1_1Optimization.html", "structDigikam_1_1PTOType_1_1Optimization" ],
    [ "Project", "structDigikam_1_1PTOType_1_1Project.html", "structDigikam_1_1PTOType_1_1Project" ],
    [ "Stitcher", "structDigikam_1_1PTOType_1_1Stitcher.html", "structDigikam_1_1PTOType_1_1Stitcher" ],
    [ "PRE_V2014", "structDigikam_1_1PTOType.html#a99aad4f1fb9d812acd6cb77c2b42b09ead661d807bca9d46eb61ad2b18d6ee0e1", null ],
    [ "V2014", "structDigikam_1_1PTOType.html#a99aad4f1fb9d812acd6cb77c2b42b09ea4fa5938ded3769173ff3d5fb977e1254", null ],
    [ "PTOType", "structDigikam_1_1PTOType.html#a92025f28054b131429a887684af2f49d", null ],
    [ "PTOType", "structDigikam_1_1PTOType.html#a8859f9dd0b0c79b501241a0d543107f3", null ],
    [ "createFile", "structDigikam_1_1PTOType.html#a3384103e8d5595fef3c4e0700bc1f279", null ],
    [ "controlPoints", "structDigikam_1_1PTOType.html#a50aca4fb1b2768c00a9923263ddad891", null ],
    [ "images", "structDigikam_1_1PTOType.html#ae6650c8122cfa570b0cf57d265a29042", null ],
    [ "lastComments", "structDigikam_1_1PTOType.html#a15df2c8fb8f68c13403ca550877fadd9", null ],
    [ "project", "structDigikam_1_1PTOType.html#a4ee2a730e412bd5cd8e383d8e2c8cec1", null ],
    [ "stitcher", "structDigikam_1_1PTOType.html#a84898ea6980acc4e0b17cd4b580a9262", null ],
    [ "version", "structDigikam_1_1PTOType.html#abe9b616afe0d1cc0ffc2045e210a79b9", null ]
];