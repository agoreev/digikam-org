var classDigikam_1_1DeltaTime =
[
    [ "DeltaTime", "classDigikam_1_1DeltaTime.html#a59a36079c2c3b60eb1d4032cee932d30", null ],
    [ "~DeltaTime", "classDigikam_1_1DeltaTime.html#ad714837849a4109e3f9720de937d1edd", null ],
    [ "isNull", "classDigikam_1_1DeltaTime.html#aab04a5dc3457e379ef156bac7a198193", null ],
    [ "deltaDays", "classDigikam_1_1DeltaTime.html#ae3531db72f24d6f56b88dc1d46281b28", null ],
    [ "deltaHours", "classDigikam_1_1DeltaTime.html#a45e4142525940ec7c2fc59663e5ae696", null ],
    [ "deltaMinutes", "classDigikam_1_1DeltaTime.html#ac6125470440c1476f818b992ca9b4f18", null ],
    [ "deltaNegative", "classDigikam_1_1DeltaTime.html#ac6ebff72ac2b098c35a883fa5fa3ea47", null ],
    [ "deltaSeconds", "classDigikam_1_1DeltaTime.html#ac680d67cd6b306830afb3b1124f98959", null ]
];