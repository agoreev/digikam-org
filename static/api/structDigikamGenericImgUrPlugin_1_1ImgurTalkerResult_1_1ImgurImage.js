var structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage =
[
    [ "animated", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a4ae31b172f308e801388eb880cbb5326", null ],
    [ "bandwidth", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a010f4d627bd9ad1e2702c9c6e81d2008", null ],
    [ "datetime", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a11c81a2fa6158bce3be92f853abace08", null ],
    [ "deletehash", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a35763aac785ed97693e51652724a2488", null ],
    [ "description", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a3ec753e0ecec6083cb7dfb188c752de6", null ],
    [ "hash", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a8d71a0f9b99be2cab36d7156d1a2923c", null ],
    [ "height", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#abcc17d35a3759130707b3ded363e4c7b", null ],
    [ "name", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a91b8b1eb60c3298fc331c2bdd35bd834", null ],
    [ "size", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#abe438c893c260cb6f9159ba5098b6c80", null ],
    [ "title", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#ab42b2db0ea56eeaf98115d229ffe9e8b", null ],
    [ "type", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#ac39485030786b853cd4002f4c164000d", null ],
    [ "url", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a8c45252a1d4f4e68fcffc0b294089b37", null ],
    [ "views", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a6df654c1104f6fb4df91c5f76a7fa38a", null ],
    [ "width", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#aab88a143a881155dc50dc482c7e62c8f", null ]
];