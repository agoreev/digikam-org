var classDigikamBqmCropPlugin_1_1CropPlugin =
[
    [ "CropPlugin", "classDigikamBqmCropPlugin_1_1CropPlugin.html#ac17d0b764785d276c580968cf661c491", null ],
    [ "~CropPlugin", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a86c7367806b528c078d921b48bc14b16", null ],
    [ "addTool", "classDigikamBqmCropPlugin_1_1CropPlugin.html#aada8336ba5b96006673a89244ab7f4fc", null ],
    [ "authors", "classDigikamBqmCropPlugin_1_1CropPlugin.html#af2e868525b43c08f5a5ef6a4f3853bfb", null ],
    [ "categories", "classDigikamBqmCropPlugin_1_1CropPlugin.html#ace50667ec58d94b184bb064cf897c031", null ],
    [ "cleanUp", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamBqmCropPlugin_1_1CropPlugin.html#aafc9ca722aabb57dfbfc3845d230adad", null ],
    [ "description", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a7634bb46268a2f5174681b5a92febabf", null ],
    [ "details", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a569b0645faa7c158b396695f67b928a1", null ],
    [ "extraAboutData", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findToolByName", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a2cc55f4c57b24b392c510fda76224b57", null ],
    [ "hasVisibilityProperty", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a5428e743adbd7d4c5d4258af04ad591e", null ],
    [ "icon", "classDigikamBqmCropPlugin_1_1CropPlugin.html#aa69286fc67b76111435c881cbcd01a93", null ],
    [ "ifaceIid", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a333f538f23a6d798bf35ddac6f8b33e8", null ],
    [ "iid", "classDigikamBqmCropPlugin_1_1CropPlugin.html#acab078dd43ce01f37a4aaa8d5f4b22be", null ],
    [ "libraryFileName", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a2d593fcb899c5aff1f443ed579c8fde0", null ],
    [ "pluginAuthors", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a285e718c1b0fde7f076bf5a52b93cfd7", null ],
    [ "setLibraryFileName", "classDigikamBqmCropPlugin_1_1CropPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamBqmCropPlugin_1_1CropPlugin.html#abf68d69b7578777df6ded0cb8deb88e8", null ],
    [ "setVisible", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a634d1026d4b7abcb1d131c34cfa4aca3", null ],
    [ "shouldLoaded", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "signalVisible", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a6be70fd2b36cbd87e78741c349eb30d8", null ],
    [ "tools", "classDigikamBqmCropPlugin_1_1CropPlugin.html#aeb5a12dd500c1106b4f1f7b7e98fa837", null ],
    [ "version", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "libraryFileName", "classDigikamBqmCropPlugin_1_1CropPlugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ],
    [ "tools", "classDigikamBqmCropPlugin_1_1CropPlugin.html#a08ecb65fb679e3490f60dc9269588673", null ]
];