var classDigikam_1_1ActionData =
[
    [ "ActionStatus", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1", [
      [ "None", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a7410f5d198240538e828fd1336adf580", null ],
      [ "BatchStarted", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a6babefe3cd2b909a4f81b4b63701e60b", null ],
      [ "BatchDone", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1aea6c7f38bd7cff8083519bcaff120e7a", null ],
      [ "BatchFailed", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a037708129f057d125d41090daf7fa061", null ],
      [ "BatchCanceled", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a1445f200f2b57a4887492e49601bb5fd", null ],
      [ "TaskDone", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a8517c92286576572fb8631dda1a902d9", null ],
      [ "TaskFailed", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a9a62b6951075cb00e0ef25646cacc566", null ],
      [ "TaskCanceled", "classDigikam_1_1ActionData.html#ae41774c7074013ca110ad8ef2fb928f1a091aa895f2a1af3d8be212427a6fe38a", null ]
    ] ],
    [ "ActionData", "classDigikam_1_1ActionData.html#aea84116f128eb52d967488bec7fee00c", null ],
    [ "destUrl", "classDigikam_1_1ActionData.html#a5189f7d00ed4cba460c2fb181cfaa0d3", null ],
    [ "fileUrl", "classDigikam_1_1ActionData.html#a3800c25a29055e0c861c8b0483edc5f8", null ],
    [ "message", "classDigikam_1_1ActionData.html#aa2c5b6ce2d25860220a61a5e8ee6b306", null ],
    [ "status", "classDigikam_1_1ActionData.html#a0ab53d9e23ab79f343d33456b5421407", null ]
];