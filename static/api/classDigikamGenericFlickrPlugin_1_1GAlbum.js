var classDigikamGenericFlickrPlugin_1_1GAlbum =
[
    [ "GAlbum", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a2f019ee4ad6b9624818b8511c30f4392", null ],
    [ "add", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#ab9bc82d398dbe76f75eada3ce7b52a4a", null ],
    [ "baseurl", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#ae49811a7db326d9d66aab81cbcc41609", null ],
    [ "create_sub", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a0ca6333b7ae95965b0784229d108b859", null ],
    [ "del_alb", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#af938292d7ed7cdaf80d3fb86f945c3e9", null ],
    [ "del_item", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a57fbbd1e48f4ee6d9d89bc6ed4bf2750", null ],
    [ "name", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a881342dcb5305c2e35afb97f61a61f3a", null ],
    [ "parent_ref_num", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#abacbeb6100af5b2784aea669ce65c23f", null ],
    [ "parentName", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a82aeb821b795390f5b05d2e0656bd523", null ],
    [ "ref_num", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a273aa10776caefb384c62a34e22ede0a", null ],
    [ "summary", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a300c60e4d82f65be41a467c6c8822ff0", null ],
    [ "title", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a9b8edede353f6cffc95198670c4f88dd", null ],
    [ "write", "classDigikamGenericFlickrPlugin_1_1GAlbum.html#a3971e24f3fba6fa118e5f59e08ea58a8", null ]
];