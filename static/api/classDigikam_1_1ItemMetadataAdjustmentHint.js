var classDigikam_1_1ItemMetadataAdjustmentHint =
[
    [ "AdjustmentStatus", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a7087185df118ad167517c7b3adf49430", [
      [ "AboutToEditMetadata", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a7087185df118ad167517c7b3adf49430a15b34e59a6e7d21f2abaf3b4ff7e550f", null ],
      [ "MetadataEditingFinished", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a7087185df118ad167517c7b3adf49430ac4583972cd1bbed83623bf07ac7f5452", null ],
      [ "MetadataEditingAborted", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a7087185df118ad167517c7b3adf49430a681e7369c95a59523ce0b11b5aa9f185", null ]
    ] ],
    [ "ItemMetadataAdjustmentHint", "classDigikam_1_1ItemMetadataAdjustmentHint.html#ac9ab4717f312d74b44f73d06e9565e9e", null ],
    [ "ItemMetadataAdjustmentHint", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a52da862c8f2a6a9d0080d9daca5a9f7d", null ],
    [ "adjustmentStatus", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a6b608aac01835ec09ce0612cb6fb1828", null ],
    [ "fileSize", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a03a67be99c42d0373af4b38c23cc132d", null ],
    [ "id", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a421526089af831e5543f6de66b2ad1b1", null ],
    [ "isAboutToEdit", "classDigikam_1_1ItemMetadataAdjustmentHint.html#aa4344f07d0c2dd6e80528ac7f048094d", null ],
    [ "isEditingFinished", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a886b368dd4f632e93b8658354f438768", null ],
    [ "isEditingFinishedAborted", "classDigikam_1_1ItemMetadataAdjustmentHint.html#ac9393a42d6ac98a01f7752cd7e8a408b", null ],
    [ "modificationDate", "classDigikam_1_1ItemMetadataAdjustmentHint.html#ab294c47204abd5fd5ca7dd07899bfe99", null ],
    [ "m_fileSize", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a927d24ba37338a7def663ecc18eeb605", null ],
    [ "m_id", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a2da6f322b9ed750f9ccf8ffcdebbd63c", null ],
    [ "m_modificationDate", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a283014952512a6eb5ac70e5a74715fe9", null ],
    [ "m_status", "classDigikam_1_1ItemMetadataAdjustmentHint.html#a84985a5089634cac2917a3905f718311", null ]
];