var classDigikamGenericPresentationPlugin_1_1KBEffect =
[
    [ "Type", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#ae59a33ea693d708fb55f96866dcfb82d", [
      [ "Fade", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#ae59a33ea693d708fb55f96866dcfb82da38c5123803aafd0abb031760015399c1", null ],
      [ "Blend", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#ae59a33ea693d708fb55f96866dcfb82da9d08f56b35621ecea24131f362d8019e", null ]
    ] ],
    [ "KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#ae853758534a3685a2ed9cde65715804d", null ],
    [ "~KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a3872ed2f572e27b6d23c40526c73ff72", null ],
    [ "advanceTime", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#aae9669eae8f6d39b426a308c21850478", null ],
    [ "done", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a8c86a532f7c05cda6bb2da11a5ecb425", null ],
    [ "fadeIn", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a0728df20c3286680bdae75dbe24af88f", null ],
    [ "image", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a6f8da4c219b308a9f5c49406303c1102", null ],
    [ "setupNewImage", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#af5f8ade4377a03bb7782b26dfa0354ff", null ],
    [ "swapImages", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a24b94cd2df28a24aa6c79cc15584481e", null ],
    [ "type", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#af889064c924ea852f961a1e9d47d6481", null ],
    [ "m_img", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#a953470df139168e2a3dc1bf2804cf270", null ],
    [ "m_needFadeIn", "classDigikamGenericPresentationPlugin_1_1KBEffect.html#aedf385b892130b54820685b741e1d406", null ]
];