var classDigikam_1_1RedEye_1_1ShapePredictor =
[
    [ "ShapePredictor", "classDigikam_1_1RedEye_1_1ShapePredictor.html#a97207dca180bb787989a8a44b41a2142", null ],
    [ "num_features", "classDigikam_1_1RedEye_1_1ShapePredictor.html#a311da996cca91ad255d0559c72cdbc99", null ],
    [ "num_parts", "classDigikam_1_1RedEye_1_1ShapePredictor.html#aefcbe80346cf951ec79eb2edf6b50650", null ],
    [ "operator()", "classDigikam_1_1RedEye_1_1ShapePredictor.html#ad86feac040148d3f0db3c22bb5c2344a", null ],
    [ "anchor_idx", "classDigikam_1_1RedEye_1_1ShapePredictor.html#a5f1872a5d8f66439a599e9666cdea43e", null ],
    [ "deltas", "classDigikam_1_1RedEye_1_1ShapePredictor.html#abf8b30c417dfc048a7610ae044194e21", null ],
    [ "forests", "classDigikam_1_1RedEye_1_1ShapePredictor.html#ae9c37197663453cd6321558496d49647", null ],
    [ "initial_shape", "classDigikam_1_1RedEye_1_1ShapePredictor.html#a5d26cf21532a14422b3aa3a5265530d9", null ]
];