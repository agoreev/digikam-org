var structDigikamGenericCalendarPlugin_1_1CalParams =
[
    [ "ItemPosition", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a02c255e420217b9b6d87b6d24de4ce7d", [
      [ "Top", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a02c255e420217b9b6d87b6d24de4ce7da5f42a2a3b4964fcf11ba5acd7b7a9bdb", null ],
      [ "Left", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a02c255e420217b9b6d87b6d24de4ce7da562e97d8a195dfd2dafd1507fae769b4", null ],
      [ "Right", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a02c255e420217b9b6d87b6d24de4ce7da446e0596955789d2f76857607ed8fb5c", null ]
    ] ],
    [ "baseFont", "structDigikamGenericCalendarPlugin_1_1CalParams.html#ad893280f0bbb098e121e968e677cc4a3", null ],
    [ "drawLines", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a585a174d88377bd6b1938d849ed2c807", null ],
    [ "height", "structDigikamGenericCalendarPlugin_1_1CalParams.html#ab58da93801bbcc6e10827d35abcc4d23", null ],
    [ "imgPos", "structDigikamGenericCalendarPlugin_1_1CalParams.html#ae8134f5fbe4eda177e94bb8b487fab0a", null ],
    [ "pageSize", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a81eba6e978b4906abd5cd502de12cea3", null ],
    [ "paperHeight", "structDigikamGenericCalendarPlugin_1_1CalParams.html#adaaf112a871473c4a29e41d920f981fc", null ],
    [ "paperWidth", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a0badb202fc56e3a2324ede1bc5747715", null ],
    [ "printResolution", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a2c17d39163eda4b6e99c559f45e61cea", null ],
    [ "ratio", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a8fd53df1c5c946784fdfa157e2013d7a", null ],
    [ "width", "structDigikamGenericCalendarPlugin_1_1CalParams.html#a1479ad71ece7fe9811f57bddbb97e6dc", null ],
    [ "year", "structDigikamGenericCalendarPlugin_1_1CalParams.html#af0f2d6adadf9fb9ed53d0b2cc6777235", null ]
];