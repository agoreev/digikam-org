var classheif_1_1StreamReader =
[
    [ "grow_status", "classheif_1_1StreamReader.html#a053ecfc73f3aa86fd7124c67e07b293d", [
      [ "size_reached", "classheif_1_1StreamReader.html#a053ecfc73f3aa86fd7124c67e07b293daa01031f5674c88e9c7a7cdf725052206", null ],
      [ "timeout", "classheif_1_1StreamReader.html#a053ecfc73f3aa86fd7124c67e07b293da5c5fb607a90f14d46a00f957d246f2bb", null ],
      [ "size_beyond_eof", "classheif_1_1StreamReader.html#a053ecfc73f3aa86fd7124c67e07b293da02f265b40741b4b568a9e60285098657", null ]
    ] ],
    [ "~StreamReader", "classheif_1_1StreamReader.html#ab75b55232d61a7cce0d24f57382f9577", null ],
    [ "get_position", "classheif_1_1StreamReader.html#af4f6793d0b167ccabffbfba158b5a21b", null ],
    [ "read", "classheif_1_1StreamReader.html#ab5ceb2312ea7ace534911a6661f7c5ed", null ],
    [ "seek", "classheif_1_1StreamReader.html#ac11aaee89f7a719dac7134c5af299f5b", null ],
    [ "seek_cur", "classheif_1_1StreamReader.html#a5fef44e1d5988070c93b1cfdd3ad021f", null ],
    [ "wait_for_file_size", "classheif_1_1StreamReader.html#ae6310db85c979b0954b26b8fdd6b3ed4", null ]
];