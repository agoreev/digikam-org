var classDigikam_1_1DNNFaceModel =
[
    [ "DNNFaceModel", "classDigikam_1_1DNNFaceModel.html#a5866945a5179d2000fc780472d7a7d8b", null ],
    [ "~DNNFaceModel", "classDigikam_1_1DNNFaceModel.html#aef4c93370c93809454a7ec468a289e86", null ],
    [ "getLabels", "classDigikam_1_1DNNFaceModel.html#a4fa0520568930550508a65c8449fe344", null ],
    [ "getSrc", "classDigikam_1_1DNNFaceModel.html#a78401f38b6d32fe58301824865107709", null ],
    [ "ptr", "classDigikam_1_1DNNFaceModel.html#a4f62ccda2cec91023493a939f7b4067c", null ],
    [ "ptr", "classDigikam_1_1DNNFaceModel.html#a781c8bb2e72bcb7242593f4c2dcb6042", null ],
    [ "setLabels", "classDigikam_1_1DNNFaceModel.html#a9f2cf1afcaf92c560b97ca187e20b38c", null ],
    [ "setMats", "classDigikam_1_1DNNFaceModel.html#a4b69ed4af3253d672ec7c24632127fa6", null ],
    [ "setSrc", "classDigikam_1_1DNNFaceModel.html#a805e50a19fdb75f193fba1280efb3910", null ],
    [ "setWrittenToDatabase", "classDigikam_1_1DNNFaceModel.html#ae29a2c6777d9fefeee549edcbdaae1f7", null ],
    [ "update", "classDigikam_1_1DNNFaceModel.html#a3cd11d381f1f69ae955f83aca0390f45", null ],
    [ "vecData", "classDigikam_1_1DNNFaceModel.html#a8aedb1de1da1f02d9ebc194c07c03c8b", null ],
    [ "vecMetadata", "classDigikam_1_1DNNFaceModel.html#ac4a0aae805d70e75497e4936110b0e57", null ],
    [ "m_vecMetadata", "classDigikam_1_1DNNFaceModel.html#a34f3a6b31e04724a6b2a02b31cd9bbc4", null ]
];