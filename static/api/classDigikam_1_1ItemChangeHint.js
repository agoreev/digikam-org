var classDigikam_1_1ItemChangeHint =
[
    [ "ChangeType", "classDigikam_1_1ItemChangeHint.html#ad21b834109b75b46a294af7f6f7f4482", [
      [ "ItemModified", "classDigikam_1_1ItemChangeHint.html#ad21b834109b75b46a294af7f6f7f4482a7a1009fd0e779d8492b14108e1a76448", null ],
      [ "ItemRescan", "classDigikam_1_1ItemChangeHint.html#ad21b834109b75b46a294af7f6f7f4482a1e6fed244b4cc05f4dd5186df5d73013", null ]
    ] ],
    [ "ItemChangeHint", "classDigikam_1_1ItemChangeHint.html#a86e47f3b80c8503fee176528b4058239", null ],
    [ "ItemChangeHint", "classDigikam_1_1ItemChangeHint.html#ab17aa745947057137777ff4732c2e134", null ],
    [ "changeType", "classDigikam_1_1ItemChangeHint.html#ae543b0d645a7af2ce89da3e1b4fb625c", null ],
    [ "ids", "classDigikam_1_1ItemChangeHint.html#ae3f29bf1faf2df49830ebe29f60f793e", null ],
    [ "isId", "classDigikam_1_1ItemChangeHint.html#a6e500c187ff68cfa2b21a3ad0cb4937f", null ],
    [ "isModified", "classDigikam_1_1ItemChangeHint.html#aaa2c44981470c4c753f918531b9269f4", null ],
    [ "needsRescan", "classDigikam_1_1ItemChangeHint.html#a9bad2f0bf3aa188141400bb94d64484c", null ],
    [ "m_ids", "classDigikam_1_1ItemChangeHint.html#a61e3dc09dfe4fa268b2c81898515a82a", null ],
    [ "m_type", "classDigikam_1_1ItemChangeHint.html#a4a37f8ed0470c732e4ff74cee92981de", null ]
];