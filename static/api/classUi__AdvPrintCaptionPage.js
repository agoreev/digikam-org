var classUi__AdvPrintCaptionPage =
[
    [ "retranslateUi", "classUi__AdvPrintCaptionPage.html#aa091e6669ed28e03e3626fb62e55fda2", null ],
    [ "setupUi", "classUi__AdvPrintCaptionPage.html#ab1f307795e0b4b3d2521e6fc4303d863", null ],
    [ "gridLayout", "classUi__AdvPrintCaptionPage.html#afa5bf0945eb7771350efcb83fc1ad075", null ],
    [ "gridLayout_2", "classUi__AdvPrintCaptionPage.html#a546d989e31621a18f41bd5aa6b4b1a16", null ],
    [ "groupBox_3", "classUi__AdvPrintCaptionPage.html#a4fdeed51cfd80de7fd3cb4da9b76f14a", null ],
    [ "horizontalLayout", "classUi__AdvPrintCaptionPage.html#a746ed1643d5d63fae5ad2c25a5853b8f", null ],
    [ "horizontalLayout_12", "classUi__AdvPrintCaptionPage.html#a1247286aa84f550f68e6064fa4a7bc78", null ],
    [ "horizontalLayout_4", "classUi__AdvPrintCaptionPage.html#aa6dd17f65f41c78d3a132d88da5739bb", null ],
    [ "horizontalLayout_5", "classUi__AdvPrintCaptionPage.html#a5f64b7c1c543680007837973fd3f4158", null ],
    [ "horizontalLayout_6", "classUi__AdvPrintCaptionPage.html#a4c21b4d4a2a3f8eac6964151295f2909", null ],
    [ "horizontalLayout_7", "classUi__AdvPrintCaptionPage.html#aa308f726dc3ea9bd0f7641e07810f3a7", null ],
    [ "horizontalLayout_9", "classUi__AdvPrintCaptionPage.html#a3083bb9a8db725ea0f494fbd1e636db9", null ],
    [ "label", "classUi__AdvPrintCaptionPage.html#a13d50624de1e33b758fb3bbda5bd3429", null ],
    [ "label_2", "classUi__AdvPrintCaptionPage.html#aa55b288f21940bd2fdeb2dd5babda988", null ],
    [ "label_3", "classUi__AdvPrintCaptionPage.html#a59223856a592d85676f04aecb82ec9fb", null ],
    [ "label_4", "classUi__AdvPrintCaptionPage.html#a0b1e3886ec835e3210a6044b41f2c3fc", null ],
    [ "label_5", "classUi__AdvPrintCaptionPage.html#a85e655d6c68d8b4e9252fdbb5bf0966a", null ],
    [ "label_6", "classUi__AdvPrintCaptionPage.html#a5f3d5b5cee9fb5277a3b22595a0845ec", null ],
    [ "label_7", "classUi__AdvPrintCaptionPage.html#a310b21fc233e31fca80dfb008e72ae47", null ],
    [ "m_captionType", "classUi__AdvPrintCaptionPage.html#a39f127d42cfcbab8e66455a213576b41", null ],
    [ "m_customCaptionGB", "classUi__AdvPrintCaptionPage.html#a3788e4e3f47b8fcfb5e41bfe56e2f0fd", null ],
    [ "m_font_color", "classUi__AdvPrintCaptionPage.html#afdcdc574d911b0bda123c20f34d7f2b2", null ],
    [ "m_font_name", "classUi__AdvPrintCaptionPage.html#a89ed2c6530898e1d75ed787f2af4cd98", null ],
    [ "m_font_size", "classUi__AdvPrintCaptionPage.html#a983319042e86c6c485b236859c2c14f1", null ],
    [ "m_free_label", "classUi__AdvPrintCaptionPage.html#a9a9c22c5ffc0cbeb457926fae697a0d5", null ],
    [ "m_FreeCaptionFormat", "classUi__AdvPrintCaptionPage.html#a323adc2fd96ded6c5fb62d5488420667", null ],
    [ "mPrintList", "classUi__AdvPrintCaptionPage.html#a68a2be02f8d491da937e038a117326ed", null ],
    [ "textLabel1_2", "classUi__AdvPrintCaptionPage.html#a5deee24479167be868db14e90d1aa3e7", null ],
    [ "verticalLayout", "classUi__AdvPrintCaptionPage.html#a3ea6d63190349f76b7bd699dedaa06f7", null ],
    [ "verticalLayout_3", "classUi__AdvPrintCaptionPage.html#a07a5b3397ad1814443b80da2eb71f4b3", null ],
    [ "verticalLayout_4", "classUi__AdvPrintCaptionPage.html#ad43c26d70fec83c715dcc7d60c2bcd45", null ],
    [ "verticalLayout_5", "classUi__AdvPrintCaptionPage.html#aa29bcebbcef27672e479205291b5573e", null ],
    [ "verticalLayout_6", "classUi__AdvPrintCaptionPage.html#a06abb3b2338808d956f0fa15ab0dd986", null ]
];