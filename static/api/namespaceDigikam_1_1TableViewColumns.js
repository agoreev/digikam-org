var namespaceDigikam_1_1TableViewColumns =
[
    [ "ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties" ],
    [ "ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties" ],
    [ "ColumnFileConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget" ],
    [ "ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties" ],
    [ "ColumnGeoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget" ],
    [ "ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties" ],
    [ "ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties" ],
    [ "ColumnPhotoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget.html", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget" ],
    [ "ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties" ],
    [ "ColumnThumbnail", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail.html", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail" ]
];