var classDigikam_1_1ItemListerJobPartsSendingReceiver =
[
    [ "ItemListerJobPartsSendingReceiver", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a49abd15b1cce4e149efb49fcab7f6457", null ],
    [ "error", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#af34c70acd3b7b37be09d7fcf1f7736aa", null ],
    [ "receive", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a17711f35cb7011b446a3c08a184dcb96", null ],
    [ "sendData", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#ac7cf47cf332b6b3c3eb0c9b86dd93a57", null ],
    [ "hasError", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a5e529d4d9ccf57afae01c5172b6e73d3", null ],
    [ "m_count", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#aca40c7493837a0945e4a0df24867fcab", null ],
    [ "m_job", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a47b387e3d1a171704a550ee5f98cd5ce", null ],
    [ "m_limit", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a663eafa06d497dca22da7f4a835e5206", null ],
    [ "records", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html#a6edac3c71d899d914c4a657a1a65ee5a", null ]
];