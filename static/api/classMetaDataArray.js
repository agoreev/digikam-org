var classMetaDataArray =
[
    [ "MetaDataArray", "classMetaDataArray.html#a0561205fb8b2a22ab4bd092b62248d20", null ],
    [ "~MetaDataArray", "classMetaDataArray.html#aeadb6b09130d85a75629ab5c7006ab8c", null ],
    [ "alloc", "classMetaDataArray.html#ab36300d5bd92f3f96e94281d7fca60b4", null ],
    [ "clear", "classMetaDataArray.html#aed0438b50e6081cd61476ee3a4210ff0", null ],
    [ "get", "classMetaDataArray.html#a95c53f7a56dfc09e366e62a4f7a3d8c5", null ],
    [ "get", "classMetaDataArray.html#a46fb9c8659d550324b8017ba9ec4fc31", null ],
    [ "operator[]", "classMetaDataArray.html#a4a98fee8f14534443acec53c8ee25daf", null ],
    [ "operator[]", "classMetaDataArray.html#a35f8b4d9ffcccc845e81d3c277811b82", null ],
    [ "set", "classMetaDataArray.html#a4517f8e1afc0a1e92fede7abf70e17e1", null ],
    [ "size", "classMetaDataArray.html#ae05436c479be7bb7edd61c4016e78581", null ],
    [ "data", "classMetaDataArray.html#ae9c5717ffc82f896934a91eac7119883", null ],
    [ "data_size", "classMetaDataArray.html#aa8aad5f864e181169530435c7d6e6e8c", null ],
    [ "height_in_units", "classMetaDataArray.html#a7997050e35aada12595060747d873791", null ],
    [ "log2unitSize", "classMetaDataArray.html#a612b4b73bb9a603e35839f4afd4411fc", null ],
    [ "width_in_units", "classMetaDataArray.html#a95e235a49d781a7a68ed0a07530fcbf6", null ]
];