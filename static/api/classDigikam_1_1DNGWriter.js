var classDigikam_1_1DNGWriter =
[
    [ "ConvertError", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610", [
      [ "PROCESSCOMPLETE", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a43f07a05cad98f5aa9d5fac6ef9ead61", null ],
      [ "PROCESSFAILED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a2aa2493eb2c0e3ef6787d52c691e277b", null ],
      [ "PROCESSCANCELED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a3dc01aa6e1b38166e30524433e1b85b3", null ],
      [ "FILENOTSUPPORTED", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a49591c7d5129c5f19b0e3ccd01afda4d", null ],
      [ "DNGSDKINTERNALERROR", "classDigikam_1_1DNGWriter.html#a49b6e960e65e1c78394a83a553923610a7517311a25ce583303821996d051b8a6", null ]
    ] ],
    [ "DNGBayerPattern", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1", [
      [ "Unknown", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1a9bdfc04c915f83ad80edf8ff95d94f7e", null ],
      [ "LinearRaw", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1a684cfffb27028e55963315f89475b575", null ],
      [ "Standard", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1adb8086a7337f7bb59c6bbe6ee64a354b", null ],
      [ "Fuji", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1a7a53217d5446463b927b82bcf959b84b", null ],
      [ "FourColor", "classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1a658d0a518906349c93774846748bf601", null ]
    ] ],
    [ "JPEGPreview", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41", [
      [ "NONE", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41a6ed0a9ac66ddbf28e33ce690ed2a4681", null ],
      [ "MEDIUM", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41ab6b411cb020aacf2d4ab915f05a6002c", null ],
      [ "FULLSIZE", "classDigikam_1_1DNGWriter.html#aa4c24136abbcbdf717d343695f435c41a895f656362a2459e9055ff8c3d91dc4a", null ]
    ] ],
    [ "DNGWriter", "classDigikam_1_1DNGWriter.html#ab670bf66af481d1a7c023420cc549ec3", null ],
    [ "~DNGWriter", "classDigikam_1_1DNGWriter.html#a9156d010a27cc20867bd023acf4a2184", null ],
    [ "~Private", "classDigikam_1_1DNGWriter.html#a564acbb402f4565414077b5a006af10d", null ],
    [ "backupOriginalRawFile", "classDigikam_1_1DNGWriter.html#a7a477392ae540a8f0bb5bcf25f6a2186", null ],
    [ "cancel", "classDigikam_1_1DNGWriter.html#afd5d0800bcb5341d5a5840e32c793fc6", null ],
    [ "cleanup", "classDigikam_1_1DNGWriter.html#a1b01eeb5a22b3c5e2a29c2e38e9fca8c", null ],
    [ "compressLossLess", "classDigikam_1_1DNGWriter.html#aa69d0499b990da5e68dff1e7cf8ae760", null ],
    [ "convert", "classDigikam_1_1DNGWriter.html#ad43f2fdd32ea9b6bdac43efbd7c8ec13", null ],
    [ "dngDateTime", "classDigikam_1_1DNGWriter.html#a1f82cdabc842d52d97ff412d0615a215", null ],
    [ "fujiRotate", "classDigikam_1_1DNGWriter.html#a368f49b6388351ba3a9b904552bf40c6", null ],
    [ "inputFile", "classDigikam_1_1DNGWriter.html#ad68e8d81c769c25e52caf43f6afc117f", null ],
    [ "outputFile", "classDigikam_1_1DNGWriter.html#a353de1fe44d172c7ebbb1f9b03000b6d", null ],
    [ "previewMode", "classDigikam_1_1DNGWriter.html#a59a4b34ee2cbcd5f9f88f5fa03d076b3", null ],
    [ "Private", "classDigikam_1_1DNGWriter.html#a2f8c04fdc8c5e3062151bbc77dea0741", null ],
    [ "reset", "classDigikam_1_1DNGWriter.html#ac09fae7e86f22b26ebaf6a152f1e19a9", null ],
    [ "reset", "classDigikam_1_1DNGWriter.html#ac09fae7e86f22b26ebaf6a152f1e19a9", null ],
    [ "setBackupOriginalRawFile", "classDigikam_1_1DNGWriter.html#afc4b28eb99cc0390b37f2dbcaf8d272e", null ],
    [ "setCompressLossLess", "classDigikam_1_1DNGWriter.html#a26c38f7ab84005f17efb03ddf8349f01", null ],
    [ "setInputFile", "classDigikam_1_1DNGWriter.html#a330f440a342183e95cc51d0a0c4973c1", null ],
    [ "setOutputFile", "classDigikam_1_1DNGWriter.html#a658ad72409145d88be64e813d713d548", null ],
    [ "setPreviewMode", "classDigikam_1_1DNGWriter.html#a5caf6e23e5697b23834ba45e61cfa089", null ],
    [ "setUpdateFileDate", "classDigikam_1_1DNGWriter.html#a923a641e965bfc46b1197c7e9a2bad12", null ],
    [ "updateFileDate", "classDigikam_1_1DNGWriter.html#ab34fc8347e798d7bbb663e37358ee8ea", null ],
    [ "backupOriginalRawFile", "classDigikam_1_1DNGWriter.html#a49acf7a48665b69ecd59a092d0d9d1e5", null ],
    [ "cancel", "classDigikam_1_1DNGWriter.html#a9a7b26c38b01997be33a7f48ba8758e0", null ],
    [ "inputFile", "classDigikam_1_1DNGWriter.html#a8e2594f4e98d0ab9ce2a027caf743813", null ],
    [ "jpegLossLessCompression", "classDigikam_1_1DNGWriter.html#ab676fc9e2bf50fe8c799c3da3ba64e3c", null ],
    [ "outputFile", "classDigikam_1_1DNGWriter.html#ac43db1d2f07b09018ba3d42845f5fe38", null ],
    [ "previewMode", "classDigikam_1_1DNGWriter.html#a7d8f296b24e38a8bdfff19ef38b856bd", null ],
    [ "updateFileDate", "classDigikam_1_1DNGWriter.html#ade8d1795108690fa96e5b3fceb2dead4", null ]
];