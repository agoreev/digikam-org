var hierarchy =
[
    [ "acceleration_functions", "structacceleration__functions.html", null ],
    [ "Digikam::AlbumPointer< Digikam::Album >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumPointer< Digikam::SAlbum >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumPointer< Digikam::TAlbum >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Algo", "classAlgo.html", [
      [ "Algo_CB", "classAlgo__CB.html", [
        [ "Algo_CB_InterPartMode", "classAlgo__CB__InterPartMode.html", [
          [ "Algo_CB_InterPartMode_Fixed", "classAlgo__CB__InterPartMode__Fixed.html", null ]
        ] ],
        [ "Algo_CB_IntraInter", "classAlgo__CB__IntraInter.html", [
          [ "Algo_CB_IntraInter_BruteForce", "classAlgo__CB__IntraInter__BruteForce.html", null ]
        ] ],
        [ "Algo_CB_IntraPartMode", "classAlgo__CB__IntraPartMode.html", [
          [ "Algo_CB_IntraPartMode_BruteForce", "classAlgo__CB__IntraPartMode__BruteForce.html", null ],
          [ "Algo_CB_IntraPartMode_Fixed", "classAlgo__CB__IntraPartMode__Fixed.html", null ]
        ] ],
        [ "Algo_CB_MergeIndex", "classAlgo__CB__MergeIndex.html", [
          [ "Algo_CB_MergeIndex_Fixed", "classAlgo__CB__MergeIndex__Fixed.html", null ]
        ] ],
        [ "Algo_CB_Skip", "classAlgo__CB__Skip.html", [
          [ "Algo_CB_Skip_BruteForce", "classAlgo__CB__Skip__BruteForce.html", null ]
        ] ],
        [ "Algo_CB_Split", "classAlgo__CB__Split.html", [
          [ "Algo_CB_Split_BruteForce", "classAlgo__CB__Split__BruteForce.html", null ]
        ] ]
      ] ],
      [ "Algo_CTB_QScale", "classAlgo__CTB__QScale.html", [
        [ "Algo_CTB_QScale_Constant", "classAlgo__CTB__QScale__Constant.html", null ]
      ] ],
      [ "Algo_PB", "classAlgo__PB.html", [
        [ "Algo_PB_MV", "classAlgo__PB__MV.html", [
          [ "Algo_PB_MV_Search", "classAlgo__PB__MV__Search.html", null ],
          [ "Algo_PB_MV_Test", "classAlgo__PB__MV__Test.html", null ]
        ] ]
      ] ],
      [ "Algo_TB_IntraPredMode", "classAlgo__TB__IntraPredMode.html", [
        [ "Algo_TB_IntraPredMode_ModeSubset", "classAlgo__TB__IntraPredMode__ModeSubset.html", [
          [ "Algo_TB_IntraPredMode_BruteForce", "classAlgo__TB__IntraPredMode__BruteForce.html", null ],
          [ "Algo_TB_IntraPredMode_FastBrute", "classAlgo__TB__IntraPredMode__FastBrute.html", null ],
          [ "Algo_TB_IntraPredMode_MinResidual", "classAlgo__TB__IntraPredMode__MinResidual.html", null ]
        ] ]
      ] ],
      [ "Algo_TB_RateEstimation", "classAlgo__TB__RateEstimation.html", [
        [ "Algo_TB_RateEstimation_Exact", "classAlgo__TB__RateEstimation__Exact.html", null ],
        [ "Algo_TB_RateEstimation_None", "classAlgo__TB__RateEstimation__None.html", null ]
      ] ],
      [ "Algo_TB_Residual", "classAlgo__TB__Residual.html", [
        [ "Algo_TB_Transform", "classAlgo__TB__Transform.html", null ]
      ] ],
      [ "Algo_TB_Split", "classAlgo__TB__Split.html", [
        [ "Algo_TB_Split_BruteForce", "classAlgo__TB__Split__BruteForce.html", null ]
      ] ]
    ] ],
    [ "Algo_CB_InterPartMode_Fixed::params", "structAlgo__CB__InterPartMode__Fixed_1_1params.html", null ],
    [ "Algo_CB_IntraPartMode_Fixed::params", "structAlgo__CB__IntraPartMode__Fixed_1_1params.html", null ],
    [ "Algo_CTB_QScale_Constant::params", "structAlgo__CTB__QScale__Constant_1_1params.html", null ],
    [ "Algo_PB_MV_Search::params", "structAlgo__PB__MV__Search_1_1params.html", null ],
    [ "Algo_PB_MV_Test::params", "structAlgo__PB__MV__Test_1_1params.html", null ],
    [ "Algo_TB_IntraPredMode_FastBrute::params", "structAlgo__TB__IntraPredMode__FastBrute_1_1params.html", null ],
    [ "Algo_TB_IntraPredMode_MinResidual::params", "structAlgo__TB__IntraPredMode__MinResidual_1_1params.html", null ],
    [ "Algo_TB_Split_BruteForce::params", "structAlgo__TB__Split__BruteForce_1_1params.html", null ],
    [ "Algorithm", null, [
      [ "Digikam::Face::FaceRecognizer", "classDigikam_1_1Face_1_1FaceRecognizer.html", [
        [ "Digikam::EigenFaceRecognizer", "classDigikam_1_1EigenFaceRecognizer.html", null ],
        [ "Digikam::FisherFaceRecognizer", "classDigikam_1_1FisherFaceRecognizer.html", null ],
        [ "Digikam::LBPHFaceRecognizer", "classDigikam_1_1LBPHFaceRecognizer.html", null ]
      ] ]
    ] ],
    [ "alloc_pool", "classalloc__pool.html", null ],
    [ "bitreader", "structbitreader.html", null ],
    [ "default_bfs_visitor", null, [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::BreadthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1BreadthFirstSearchVisitor.html", null ]
    ] ],
    [ "default_dfs_visitor", null, [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::DepthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1DepthFirstSearchVisitor.html", null ]
    ] ],
    [ "CABAC_decoder", "structCABAC__decoder.html", null ],
    [ "CABAC_encoder", "classCABAC__encoder.html", [
      [ "CABAC_encoder_bitstream", "classCABAC__encoder__bitstream.html", null ],
      [ "CABAC_encoder_estim", "classCABAC__encoder__estim.html", [
        [ "CABAC_encoder_estim_constant", "classCABAC__encoder__estim__constant.html", null ]
      ] ]
    ] ],
    [ "CB_ref_info", "structCB__ref__info.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::AlbumChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::AlbumRootChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::CollectionImageChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::ImageChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::ImageTagChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::SearchChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::TagChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "CodingOption< node >", "classCodingOption.html", null ],
    [ "CodingOptions< node >", "classCodingOptions.html", null ],
    [ "config_parameters", "classconfig__parameters.html", null ],
    [ "context_model", "structcontext__model.html", null ],
    [ "context_model_table", "classcontext__model__table.html", null ],
    [ "CTB_info", "structCTB__info.html", null ],
    [ "CTBTreeMatrix", "classCTBTreeMatrix.html", null ],
    [ "CascadeClassifier", null, [
      [ "Digikam::Cascade", "classDigikam_1_1Cascade.html", null ]
    ] ],
    [ "Ptr", null, [
      [ "Digikam::DNNFaceModel", "classDigikam_1_1DNNFaceModel.html", null ],
      [ "Digikam::EigenFaceModel", "classDigikam_1_1EigenFaceModel.html", null ],
      [ "Digikam::FisherFaceModel", "classDigikam_1_1FisherFaceModel.html", null ],
      [ "Digikam::LBPHFaceModel", "classDigikam_1_1LBPHFaceModel.html", null ]
    ] ],
    [ "DigikamGenericHtmlGalleryPlugin::CWrapper< xmlTextWriterPtr, xmlFreeTextWriter >", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper.html", null ],
    [ "de265_image", "structde265__image.html", null ],
    [ "de265_image_allocation", "structde265__image__allocation.html", null ],
    [ "de265_image_spec", "structde265__image__spec.html", null ],
    [ "de265_progress_lock", "classde265__progress__lock.html", null ],
    [ "decoded_picture_buffer", "classdecoded__picture__buffer.html", null ],
    [ "Digikam::AbstractAlbumTreeView::ContextMenuElement", "classDigikam_1_1AbstractAlbumTreeView_1_1ContextMenuElement.html", null ],
    [ "Digikam::AbstractMarkerTiler::ClickInfo", "classDigikam_1_1AbstractMarkerTiler_1_1ClickInfo.html", null ],
    [ "Digikam::AbstractMarkerTiler::NonEmptyIterator", "classDigikam_1_1AbstractMarkerTiler_1_1NonEmptyIterator.html", null ],
    [ "Digikam::AbstractMarkerTiler::Tile", "classDigikam_1_1AbstractMarkerTiler_1_1Tile.html", null ],
    [ "Digikam::ActionData", "classDigikam_1_1ActionData.html", null ],
    [ "Digikam::Album", "classDigikam_1_1Album.html", [
      [ "Digikam::DAlbum", "classDigikam_1_1DAlbum.html", null ],
      [ "Digikam::PAlbum", "classDigikam_1_1PAlbum.html", null ],
      [ "Digikam::SAlbum", "classDigikam_1_1SAlbum.html", null ],
      [ "Digikam::TAlbum", "classDigikam_1_1TAlbum.html", null ]
    ] ],
    [ "Digikam::AlbumChangeset", "classDigikam_1_1AlbumChangeset.html", null ],
    [ "Digikam::AlbumCopyMoveHint", "classDigikam_1_1AlbumCopyMoveHint.html", null ],
    [ "Digikam::AlbumInfo", "classDigikam_1_1AlbumInfo.html", null ],
    [ "Digikam::AlbumIterator", "classDigikam_1_1AlbumIterator.html", null ],
    [ "Digikam::AlbumManagerCreator", "classDigikam_1_1AlbumManagerCreator.html", null ],
    [ "Digikam::AlbumPointer< T >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumRootChangeset", "classDigikam_1_1AlbumRootChangeset.html", null ],
    [ "Digikam::AlbumRootInfo", "classDigikam_1_1AlbumRootInfo.html", null ],
    [ "Digikam::AlbumShortInfo", "classDigikam_1_1AlbumShortInfo.html", null ],
    [ "Digikam::AlbumSimplified", "classDigikam_1_1AlbumSimplified.html", null ],
    [ "Digikam::AntiVignettingContainer", "classDigikam_1_1AntiVignettingContainer.html", null ],
    [ "Digikam::AssignedBatchTools", "classDigikam_1_1AssignedBatchTools.html", null ],
    [ "Digikam::BalooInfo", "classDigikam_1_1BalooInfo.html", null ],
    [ "Digikam::BatchToolSet", "classDigikam_1_1BatchToolSet.html", null ],
    [ "Digikam::BCGContainer", "classDigikam_1_1BCGContainer.html", null ],
    [ "Digikam::BdEngineBackend::QueryState", "classDigikam_1_1BdEngineBackend_1_1QueryState.html", null ],
    [ "Digikam::BdEngineBackendPrivate::AbstractUnlocker", "classDigikam_1_1BdEngineBackendPrivate_1_1AbstractUnlocker.html", [
      [ "Digikam::BdEngineBackendPrivate::AbstractWaitingUnlocker", "classDigikam_1_1BdEngineBackendPrivate_1_1AbstractWaitingUnlocker.html", [
        [ "Digikam::BdEngineBackendPrivate::BusyWaiter", "classDigikam_1_1BdEngineBackendPrivate_1_1BusyWaiter.html", null ],
        [ "Digikam::BdEngineBackendPrivate::ErrorLocker", "classDigikam_1_1BdEngineBackendPrivate_1_1ErrorLocker.html", null ]
      ] ]
    ] ],
    [ "Digikam::BorderContainer", "classDigikam_1_1BorderContainer.html", null ],
    [ "Digikam::BWSepiaContainer", "classDigikam_1_1BWSepiaContainer.html", null ],
    [ "Digikam::CachedPixmapKey", "classDigikam_1_1CachedPixmapKey.html", null ],
    [ "Digikam::CachedPixmaps", "classDigikam_1_1CachedPixmaps.html", null ],
    [ "Digikam::CameraMessageBox", "classDigikam_1_1CameraMessageBox.html", null ],
    [ "Digikam::CameraNameHelper", "classDigikam_1_1CameraNameHelper.html", null ],
    [ "Digikam::CamItemInfo", "classDigikam_1_1CamItemInfo.html", null ],
    [ "Digikam::CamItemSortSettings", "classDigikam_1_1CamItemSortSettings.html", null ],
    [ "Digikam::CaptionValues", "classDigikam_1_1CaptionValues.html", null ],
    [ "Digikam::CBContainer", "classDigikam_1_1CBContainer.html", null ],
    [ "Digikam::ChangingDB", "classDigikam_1_1ChangingDB.html", null ],
    [ "Digikam::ChoiceSearchModel::Entry", "classDigikam_1_1ChoiceSearchModel_1_1Entry.html", null ],
    [ "Digikam::CMat", "structDigikam_1_1CMat.html", null ],
    [ "Digikam::CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html", null ],
    [ "Digikam::CollectionLocation", "classDigikam_1_1CollectionLocation.html", [
      [ "Digikam::AlbumRootLocation", "classDigikam_1_1AlbumRootLocation.html", null ]
    ] ],
    [ "Digikam::CollectionScannerHintContainer", "classDigikam_1_1CollectionScannerHintContainer.html", [
      [ "Digikam::CollectionScannerHintContainerImplementation", "classDigikam_1_1CollectionScannerHintContainerImplementation.html", null ]
    ] ],
    [ "Digikam::CollectionScannerHints::Album", "classDigikam_1_1CollectionScannerHints_1_1Album.html", null ],
    [ "Digikam::CollectionScannerHints::DstPath", "classDigikam_1_1CollectionScannerHints_1_1DstPath.html", null ],
    [ "Digikam::CollectionScannerHints::Item", "classDigikam_1_1CollectionScannerHints_1_1Item.html", null ],
    [ "Digikam::CollectionScannerObserver", "classDigikam_1_1CollectionScannerObserver.html", [
      [ "Digikam::InitializationObserver", "classDigikam_1_1InitializationObserver.html", [
        [ "Digikam::ScanController", "classDigikam_1_1ScanController.html", null ]
      ] ],
      [ "Digikam::SimpleCollectionScannerObserver", "classDigikam_1_1SimpleCollectionScannerObserver.html", null ]
    ] ],
    [ "Digikam::ColorFXContainer", "classDigikam_1_1ColorFXContainer.html", null ],
    [ "Digikam::CommentInfo", "classDigikam_1_1CommentInfo.html", null ],
    [ "Digikam::ContentAwareContainer", "classDigikam_1_1ContentAwareContainer.html", null ],
    [ "Digikam::CopyrightInfo", "classDigikam_1_1CopyrightInfo.html", null ],
    [ "Digikam::CoreDbAccess", "classDigikam_1_1CoreDbAccess.html", null ],
    [ "Digikam::CoreDbAccessUnlock", "classDigikam_1_1CoreDbAccessUnlock.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< T >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbDownloadHistory", "classDigikam_1_1CoreDbDownloadHistory.html", null ],
    [ "Digikam::CoreDbNameFilter", "classDigikam_1_1CoreDbNameFilter.html", null ],
    [ "Digikam::CoreDbPrivilegesChecker", "classDigikam_1_1CoreDbPrivilegesChecker.html", null ],
    [ "Digikam::CoreDbTransaction", "classDigikam_1_1CoreDbTransaction.html", null ],
    [ "Digikam::CurvesContainer", "classDigikam_1_1CurvesContainer.html", null ],
    [ "Digikam::DAlbumInfo", "classDigikam_1_1DAlbumInfo.html", null ],
    [ "Digikam::DatabaseBlob", "classDigikam_1_1DatabaseBlob.html", null ],
    [ "Digikam::DatabaseFields::DatabaseFieldsEnumIterator< FieldName >", "classDigikam_1_1DatabaseFields_1_1DatabaseFieldsEnumIterator.html", null ],
    [ "Digikam::DatabaseFields::DatabaseFieldsEnumIteratorSetOnly< FieldName >", "classDigikam_1_1DatabaseFields_1_1DatabaseFieldsEnumIteratorSetOnly.html", null ],
    [ "Digikam::DatabaseFields::FieldMetaInfo< FieldName >", "classDigikam_1_1DatabaseFields_1_1FieldMetaInfo.html", null ],
    [ "Digikam::DatabaseFields::Set", "classDigikam_1_1DatabaseFields_1_1Set.html", null ],
    [ "Digikam::DatabaseServerError", "classDigikam_1_1DatabaseServerError.html", null ],
    [ "Digikam::DateFormat", "classDigikam_1_1DateFormat.html", null ],
    [ "Digikam::DbEngineAccess", "classDigikam_1_1DbEngineAccess.html", null ],
    [ "Digikam::DbEngineAction", "classDigikam_1_1DbEngineAction.html", null ],
    [ "Digikam::DbEngineActionElement", "classDigikam_1_1DbEngineActionElement.html", null ],
    [ "Digikam::DbEngineConfig", "classDigikam_1_1DbEngineConfig.html", null ],
    [ "Digikam::DbEngineConfigSettings", "classDigikam_1_1DbEngineConfigSettings.html", null ],
    [ "Digikam::DbEngineConfigSettingsLoader", "classDigikam_1_1DbEngineConfigSettingsLoader.html", null ],
    [ "Digikam::DbEngineErrorAnswer", "classDigikam_1_1DbEngineErrorAnswer.html", [
      [ "Digikam::BdEngineBackendPrivate", "classDigikam_1_1BdEngineBackendPrivate.html", [
        [ "Digikam::CoreDbBackendPrivate", "classDigikam_1_1CoreDbBackendPrivate.html", null ]
      ] ]
    ] ],
    [ "Digikam::DbEngineLocking", "classDigikam_1_1DbEngineLocking.html", null ],
    [ "Digikam::DbEngineParameters", "classDigikam_1_1DbEngineParameters.html", null ],
    [ "Digikam::DbEngineThreadData", "classDigikam_1_1DbEngineThreadData.html", null ],
    [ "Digikam::DBJobInfo", "classDigikam_1_1DBJobInfo.html", [
      [ "Digikam::AlbumsDBJobInfo", "classDigikam_1_1AlbumsDBJobInfo.html", null ],
      [ "Digikam::DatesDBJobInfo", "classDigikam_1_1DatesDBJobInfo.html", null ],
      [ "Digikam::GPSDBJobInfo", "classDigikam_1_1GPSDBJobInfo.html", null ],
      [ "Digikam::SearchesDBJobInfo", "classDigikam_1_1SearchesDBJobInfo.html", null ],
      [ "Digikam::TagsDBJobInfo", "classDigikam_1_1TagsDBJobInfo.html", null ]
    ] ],
    [ "Digikam::DbKeysCollection", "classDigikam_1_1DbKeysCollection.html", [
      [ "Digikam::CommonKeys", "classDigikam_1_1CommonKeys.html", null ],
      [ "Digikam::MetadataKeys", "classDigikam_1_1MetadataKeys.html", null ],
      [ "Digikam::PositionKeys", "classDigikam_1_1PositionKeys.html", null ]
    ] ],
    [ "Digikam::DCategorizedView::ElementInfo", "structDigikam_1_1DCategorizedView_1_1ElementInfo.html", null ],
    [ "Digikam::DColor", "classDigikam_1_1DColor.html", null ],
    [ "Digikam::DColorComposer", "classDigikam_1_1DColorComposer.html", null ],
    [ "Digikam::DConfigDlgModelPrivate", "classDigikam_1_1DConfigDlgModelPrivate.html", [
      [ "Digikam::DConfigDlgWdgModelPrivate", "classDigikam_1_1DConfigDlgWdgModelPrivate.html", null ]
    ] ],
    [ "Digikam::DConfigDlgViewPrivate", "classDigikam_1_1DConfigDlgViewPrivate.html", [
      [ "Digikam::DConfigDlgWdgPrivate", "classDigikam_1_1DConfigDlgWdgPrivate.html", null ]
    ] ],
    [ "Digikam::DDateTable::DatePaintingMode", "structDigikam_1_1DDateTable_1_1DatePaintingMode.html", null ],
    [ "Digikam::DeltaTime", "classDigikam_1_1DeltaTime.html", null ],
    [ "Digikam::DetectObjectParameters", "classDigikam_1_1DetectObjectParameters.html", null ],
    [ "Digikam::DFileOperations", "classDigikam_1_1DFileOperations.html", null ],
    [ "Digikam::DImageHistory::Entry", "classDigikam_1_1DImageHistory_1_1Entry.html", null ],
    [ "Digikam::DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html", null ],
    [ "Digikam::DImgFilterGenerator", "classDigikam_1_1DImgFilterGenerator.html", [
      [ "Digikam::BasicDImgFilterGenerator< T >", "classDigikam_1_1BasicDImgFilterGenerator.html", null ]
    ] ],
    [ "Digikam::DImgLoader", "classDigikam_1_1DImgLoader.html", [
      [ "Digikam::DImgHEIFLoader", "classDigikam_1_1DImgHEIFLoader.html", null ],
      [ "Digikam::DImgPGFLoader", "classDigikam_1_1DImgPGFLoader.html", null ],
      [ "DigikamImageMagickDImgPlugin::DImgImageMagickLoader", "classDigikamImageMagickDImgPlugin_1_1DImgImageMagickLoader.html", null ],
      [ "DigikamJPEG2000DImgPlugin::DImgJPEG2000Loader", "classDigikamJPEG2000DImgPlugin_1_1DImgJPEG2000Loader.html", null ],
      [ "DigikamJPEGDImgPlugin::DImgJPEGLoader", "classDigikamJPEGDImgPlugin_1_1DImgJPEGLoader.html", null ],
      [ "DigikamPNGDImgPlugin::DImgPNGLoader", "classDigikamPNGDImgPlugin_1_1DImgPNGLoader.html", null ],
      [ "DigikamQImageDImgPlugin::DImgQImageLoader", "classDigikamQImageDImgPlugin_1_1DImgQImageLoader.html", null ],
      [ "DigikamRAWDImgPlugin::DImgRAWLoader", "classDigikamRAWDImgPlugin_1_1DImgRAWLoader.html", null ],
      [ "DigikamTIFFDImgPlugin::DImgTIFFLoader", "classDigikamTIFFDImgPlugin_1_1DImgTIFFLoader.html", null ]
    ] ],
    [ "Digikam::DImgLoaderObserver", "classDigikam_1_1DImgLoaderObserver.html", [
      [ "Digikam::IccTransformFilter", "classDigikam_1_1IccTransformFilter.html", null ],
      [ "Digikam::LoadingTask", "classDigikam_1_1LoadingTask.html", [
        [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", [
          [ "Digikam::PreviewLoadingTask", "classDigikam_1_1PreviewLoadingTask.html", null ],
          [ "Digikam::ThumbnailLoadingTask", "classDigikam_1_1ThumbnailLoadingTask.html", null ]
        ] ]
      ] ],
      [ "Digikam::SavingTask", "classDigikam_1_1SavingTask.html", null ]
    ] ],
    [ "Digikam::DItemInfo", "classDigikam_1_1DItemInfo.html", null ],
    [ "Digikam::DMessageBox", "classDigikam_1_1DMessageBox.html", null ],
    [ "Digikam::DNNDbscan", "classDigikam_1_1DNNDbscan.html", null ],
    [ "Digikam::DNNDbscanPointCustomized", "classDigikam_1_1DNNDbscanPointCustomized.html", null ],
    [ "Digikam::DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html", [
      [ "Digikam::DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html", null ],
      [ "Digikam::DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html", null ]
    ] ],
    [ "Digikam::DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html", null ],
    [ "Digikam::DNNFaceRecognizer", "classDigikam_1_1DNNFaceRecognizer.html", null ],
    [ "Digikam::DNNFaceVecMetadata", "classDigikam_1_1DNNFaceVecMetadata.html", null ],
    [ "Digikam::DownloadSettings", "classDigikam_1_1DownloadSettings.html", null ],
    [ "Digikam::DPixelsAliasFilter", "classDigikam_1_1DPixelsAliasFilter.html", null ],
    [ "Digikam::DPluginAuthor", "classDigikam_1_1DPluginAuthor.html", null ],
    [ "Digikam::DragDropModelImplementation", "classDigikam_1_1DragDropModelImplementation.html", [
      [ "Digikam::ItemModel", null, [
        [ "Digikam::ItemThumbnailModel", null, [
          [ "Digikam::ItemListModel", "classDigikam_1_1ItemListModel.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::DragDropViewImplementation", "classDigikam_1_1DragDropViewImplementation.html", [
      [ "Digikam::ItemViewCategorized", null, [
        [ "Digikam::ImportCategorizedView", null, [
          [ "Digikam::ImportIconView", "classDigikam_1_1ImportIconView.html", null ]
        ] ],
        [ "Digikam::ItemCategorizedView", null, [
          [ "Digikam::DigikamItemView", "classDigikam_1_1DigikamItemView.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::DRawDecoderSettings", "classDigikam_1_1DRawDecoderSettings.html", null ],
    [ "Digikam::DRawDecoding", "classDigikam_1_1DRawDecoding.html", null ],
    [ "Digikam::DRawInfo", "classDigikam_1_1DRawInfo.html", null ],
    [ "Digikam::DSharedData", "classDigikam_1_1DSharedData.html", [
      [ "Digikam::ItemInfoData", "classDigikam_1_1ItemInfoData.html", null ]
    ] ],
    [ "Digikam::DSharedDataPointer< T >", "classDigikam_1_1DSharedDataPointer.html", null ],
    [ "Digikam::DToolTipStyleSheet", "classDigikam_1_1DToolTipStyleSheet.html", null ],
    [ "Digikam::DTrash", "classDigikam_1_1DTrash.html", null ],
    [ "Digikam::DTrashItemInfo", "classDigikam_1_1DTrashItemInfo.html", null ],
    [ "Digikam::DWItemDelegatePool", "classDigikam_1_1DWItemDelegatePool.html", null ],
    [ "Digikam::DWItemDelegatePoolPrivate", "classDigikam_1_1DWItemDelegatePoolPrivate.html", null ],
    [ "Digikam::DWorkingPixmap", "classDigikam_1_1DWorkingPixmap.html", null ],
    [ "Digikam::EditorCore::FileToSave", "classDigikam_1_1EditorCore_1_1FileToSave.html", null ],
    [ "Digikam::EigenFaceMatMetadata", "classDigikam_1_1EigenFaceMatMetadata.html", null ],
    [ "Digikam::Ellipsoid", "classDigikam_1_1Ellipsoid.html", null ],
    [ "Digikam::ExposureSettingsContainer", "classDigikam_1_1ExposureSettingsContainer.html", null ],
    [ "Digikam::Face::PredictCollector", "classDigikam_1_1Face_1_1PredictCollector.html", [
      [ "Digikam::Face::StandardCollector", "classDigikam_1_1Face_1_1StandardCollector.html", null ]
    ] ],
    [ "Digikam::Face::StandardCollector::PredictResult", "classDigikam_1_1Face_1_1StandardCollector_1_1PredictResult.html", null ],
    [ "Digikam::FaceDbAccess", "classDigikam_1_1FaceDbAccess.html", null ],
    [ "Digikam::FaceDbAccessUnlock", "classDigikam_1_1FaceDbAccessUnlock.html", null ],
    [ "Digikam::FaceItemRetriever", "classDigikam_1_1FaceItemRetriever.html", null ],
    [ "Digikam::FacePipelinePackage", "classDigikam_1_1FacePipelinePackage.html", [
      [ "Digikam::FacePipelineExtendedPackage", "classDigikam_1_1FacePipelineExtendedPackage.html", null ]
    ] ],
    [ "Digikam::FaceScanSettings", "classDigikam_1_1FaceScanSettings.html", null ],
    [ "Digikam::FaceTags", "classDigikam_1_1FaceTags.html", null ],
    [ "Digikam::FaceTagsEditor", "classDigikam_1_1FaceTagsEditor.html", [
      [ "Digikam::FaceUtils", "classDigikam_1_1FaceUtils.html", null ]
    ] ],
    [ "Digikam::FaceTagsIface", "classDigikam_1_1FaceTagsIface.html", [
      [ "Digikam::FacePipelineFaceTagsIface", "classDigikam_1_1FacePipelineFaceTagsIface.html", null ]
    ] ],
    [ "Digikam::FieldQueryBuilder", "classDigikam_1_1FieldQueryBuilder.html", null ],
    [ "Digikam::FileActionProgressItemCreator", "classDigikam_1_1FileActionProgressItemCreator.html", [
      [ "Digikam::PrivateProgressItemCreator", "classDigikam_1_1PrivateProgressItemCreator.html", null ]
    ] ],
    [ "Digikam::FileReadLocker", "classDigikam_1_1FileReadLocker.html", null ],
    [ "Digikam::FileReadWriteLockKey", "classDigikam_1_1FileReadWriteLockKey.html", null ],
    [ "Digikam::FileWriteLocker", "classDigikam_1_1FileWriteLocker.html", null ],
    [ "Digikam::FilmGrainContainer", "classDigikam_1_1FilmGrainContainer.html", null ],
    [ "Digikam::FilmProfile", "classDigikam_1_1FilmProfile.html", null ],
    [ "Digikam::Filter", "classDigikam_1_1Filter.html", null ],
    [ "Digikam::FilterAction", "classDigikam_1_1FilterAction.html", [
      [ "Digikam::DImgThreadedFilter::DefaultFilterAction< Filter >", "classDigikam_1_1DImgThreadedFilter_1_1DefaultFilterAction.html", null ]
    ] ],
    [ "Digikam::FisherFaceMatMetadata", "classDigikam_1_1FisherFaceMatMetadata.html", null ],
    [ "Digikam::FrameUtils", "classDigikam_1_1FrameUtils.html", null ],
    [ "Digikam::FreeRotationContainer", "classDigikam_1_1FreeRotationContainer.html", null ],
    [ "Digikam::FullObjectDetection", "classDigikam_1_1FullObjectDetection.html", null ],
    [ "Digikam::GeoCoordinates", "classDigikam_1_1GeoCoordinates.html", null ],
    [ "Digikam::GeodeticCalculator", "classDigikam_1_1GeodeticCalculator.html", null ],
    [ "Digikam::GeoIfaceCluster", "classDigikam_1_1GeoIfaceCluster.html", null ],
    [ "Digikam::GeoIfaceInternalWidgetInfo", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html", null ],
    [ "Digikam::GPSDataContainer", "classDigikam_1_1GPSDataContainer.html", null ],
    [ "Digikam::GPSItemContainer", "classDigikam_1_1GPSItemContainer.html", [
      [ "Digikam::ItemGPS", "classDigikam_1_1ItemGPS.html", null ]
    ] ],
    [ "Digikam::GPSItemInfo", "classDigikam_1_1GPSItemInfo.html", null ],
    [ "Digikam::GPSUndoCommand::UndoInfo", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >", "classDigikam_1_1Graph.html", [
      [ "Digikam::ItemHistoryGraphData", "classDigikam_1_1ItemHistoryGraphData.html", null ]
    ] ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::DominatorTree", "classDigikam_1_1Graph_1_1DominatorTree.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Edge", "classDigikam_1_1Graph_1_1Edge.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch", "classDigikam_1_1Graph_1_1GraphSearch.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::CommonVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1CommonVisitor.html", [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::BreadthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1BreadthFirstSearchVisitor.html", null ],
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::DepthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1DepthFirstSearchVisitor.html", null ]
    ] ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::lessThanMapEdgeToTarget< GraphType, VertexLessThan >", "classDigikam_1_1Graph_1_1GraphSearch_1_1lessThanMapEdgeToTarget.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Path", "classDigikam_1_1Graph_1_1Path.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Vertex", "classDigikam_1_1Graph_1_1Vertex.html", null ],
    [ "Digikam::GreycstorationContainer", "classDigikam_1_1GreycstorationContainer.html", null ],
    [ "Digikam::GroupedImagesFinder", "classDigikam_1_1GroupedImagesFinder.html", null ],
    [ "Digikam::GroupingViewImplementation", "classDigikam_1_1GroupingViewImplementation.html", [
      [ "Digikam::DigikamItemView", "classDigikam_1_1DigikamItemView.html", null ]
    ] ],
    [ "Digikam::GroupItemFilterSettings", "classDigikam_1_1GroupItemFilterSettings.html", null ],
    [ "Digikam::Haar::Calculator", "classDigikam_1_1Haar_1_1Calculator.html", null ],
    [ "Digikam::Haar::ImageData", "classDigikam_1_1Haar_1_1ImageData.html", null ],
    [ "Digikam::Haar::SignatureData", "classDigikam_1_1Haar_1_1SignatureData.html", null ],
    [ "Digikam::Haar::SignatureMap", "classDigikam_1_1Haar_1_1SignatureMap.html", null ],
    [ "Digikam::Haar::WeightBin", "classDigikam_1_1Haar_1_1WeightBin.html", null ],
    [ "Digikam::Haar::Weights", "classDigikam_1_1Haar_1_1Weights.html", null ],
    [ "Digikam::HaarProgressObserver", "classDigikam_1_1HaarProgressObserver.html", [
      [ "Digikam::DuplicatesProgressObserver", "classDigikam_1_1DuplicatesProgressObserver.html", null ]
    ] ],
    [ "Digikam::HistoryEdgeProperties", "classDigikam_1_1HistoryEdgeProperties.html", null ],
    [ "Digikam::HistoryImageId", "classDigikam_1_1HistoryImageId.html", null ],
    [ "Digikam::HistoryVertexProperties", "classDigikam_1_1HistoryVertexProperties.html", null ],
    [ "Digikam::HSLContainer", "classDigikam_1_1HSLContainer.html", null ],
    [ "Digikam::ICCSettingsContainer", "classDigikam_1_1ICCSettingsContainer.html", null ],
    [ "Digikam::ImageChangeset", "classDigikam_1_1ImageChangeset.html", null ],
    [ "Digikam::ImageCommonContainer", "classDigikam_1_1ImageCommonContainer.html", null ],
    [ "Digikam::ImageHistoryEntry", "classDigikam_1_1ImageHistoryEntry.html", null ],
    [ "Digikam::ImageListProvider", "classDigikam_1_1ImageListProvider.html", [
      [ "Digikam::EmptyImageListProvider", "classDigikam_1_1EmptyImageListProvider.html", null ],
      [ "Digikam::QListImageListProvider", "classDigikam_1_1QListImageListProvider.html", null ]
    ] ],
    [ "Digikam::ImageMetadataContainer", "classDigikam_1_1ImageMetadataContainer.html", null ],
    [ "Digikam::ImageQualityContainer", "classDigikam_1_1ImageQualityContainer.html", null ],
    [ "Digikam::ImageRelation", "classDigikam_1_1ImageRelation.html", null ],
    [ "Digikam::ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html", null ],
    [ "Digikam::ImageTagProperty", "classDigikam_1_1ImageTagProperty.html", null ],
    [ "Digikam::ImageTagPropertyName", "classDigikam_1_1ImageTagPropertyName.html", null ],
    [ "Digikam::ImageZoomSettings", "classDigikam_1_1ImageZoomSettings.html", null ],
    [ "Digikam::InfraredContainer", "classDigikam_1_1InfraredContainer.html", null ],
    [ "Digikam::InternalTagName", "classDigikam_1_1InternalTagName.html", null ],
    [ "Digikam::IOFileSettings", "classDigikam_1_1IOFileSettings.html", null ],
    [ "Digikam::IptcCoreContactInfo", "classDigikam_1_1IptcCoreContactInfo.html", null ],
    [ "Digikam::IptcCoreLocationInfo", "classDigikam_1_1IptcCoreLocationInfo.html", null ],
    [ "Digikam::ItemChangeHint", "classDigikam_1_1ItemChangeHint.html", null ],
    [ "Digikam::ItemCopyMoveHint", "classDigikam_1_1ItemCopyMoveHint.html", null ],
    [ "Digikam::ItemCopyright", "classDigikam_1_1ItemCopyright.html", null ],
    [ "Digikam::ItemDelegateOverlayContainer", "classDigikam_1_1ItemDelegateOverlayContainer.html", [
      [ "Digikam::ItemViewDelegate", "classDigikam_1_1ItemViewDelegate.html", [
        [ "Digikam::ItemDelegate", "classDigikam_1_1ItemDelegate.html", [
          [ "Digikam::DigikamItemDelegate", "classDigikam_1_1DigikamItemDelegate.html", [
            [ "Digikam::ItemFaceDelegate", "classDigikam_1_1ItemFaceDelegate.html", null ]
          ] ],
          [ "Digikam::ItemThumbnailDelegate", "classDigikam_1_1ItemThumbnailDelegate.html", null ]
        ] ]
      ] ],
      [ "Digikam::ItemViewImportDelegate", "classDigikam_1_1ItemViewImportDelegate.html", [
        [ "Digikam::ImportDelegate", "classDigikam_1_1ImportDelegate.html", [
          [ "Digikam::ImportNormalDelegate", "classDigikam_1_1ImportNormalDelegate.html", null ],
          [ "Digikam::ImportThumbnailDelegate", "classDigikam_1_1ImportThumbnailDelegate.html", null ]
        ] ]
      ] ],
      [ "ShowFoto::ItemViewShowfotoDelegate", "classShowFoto_1_1ItemViewShowfotoDelegate.html", [
        [ "ShowFoto::ShowfotoDelegate", "classShowFoto_1_1ShowfotoDelegate.html", [
          [ "ShowFoto::ShowfotoNormalDelegate", "classShowFoto_1_1ShowfotoNormalDelegate.html", null ],
          [ "ShowFoto::ShowfotoThumbnailDelegate", "classShowFoto_1_1ShowfotoThumbnailDelegate.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::ItemExtendedProperties", "classDigikam_1_1ItemExtendedProperties.html", null ],
    [ "Digikam::ItemFilterModelPrepareHook", "classDigikam_1_1ItemFilterModelPrepareHook.html", null ],
    [ "Digikam::ItemFilterModelTodoPackage", "classDigikam_1_1ItemFilterModelTodoPackage.html", null ],
    [ "Digikam::ItemFilterSettings", "classDigikam_1_1ItemFilterSettings.html", null ],
    [ "Digikam::ItemHistoryGraph", "classDigikam_1_1ItemHistoryGraph.html", null ],
    [ "Digikam::ItemInfo", "classDigikam_1_1ItemInfo.html", null ],
    [ "Digikam::ItemInfoSet", "classDigikam_1_1ItemInfoSet.html", null ],
    [ "Digikam::ItemInfoStatic", "classDigikam_1_1ItemInfoStatic.html", null ],
    [ "Digikam::ItemListerReceiver", "classDigikam_1_1ItemListerReceiver.html", [
      [ "Digikam::ItemListerValueListReceiver", "classDigikam_1_1ItemListerValueListReceiver.html", [
        [ "Digikam::ItemListerJobReceiver", "classDigikam_1_1ItemListerJobReceiver.html", [
          [ "Digikam::ItemListerJobPartsSendingReceiver", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html", [
            [ "Digikam::ItemListerJobGrowingPartsSendingReceiver", "classDigikam_1_1ItemListerJobGrowingPartsSendingReceiver.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::ItemListerRecord", "classDigikam_1_1ItemListerRecord.html", null ],
    [ "Digikam::ItemMetadataAdjustmentHint", "classDigikam_1_1ItemMetadataAdjustmentHint.html", null ],
    [ "Digikam::ItemPosition", "classDigikam_1_1ItemPosition.html", null ],
    [ "Digikam::ItemQueryBuilder", "classDigikam_1_1ItemQueryBuilder.html", null ],
    [ "Digikam::ItemQueryPostHook", "classDigikam_1_1ItemQueryPostHook.html", null ],
    [ "Digikam::ItemQueryPostHooks", "classDigikam_1_1ItemQueryPostHooks.html", null ],
    [ "Digikam::ItemScanInfo", "classDigikam_1_1ItemScanInfo.html", null ],
    [ "Digikam::ItemScannerCommit", "classDigikam_1_1ItemScannerCommit.html", null ],
    [ "Digikam::ItemShortInfo", "classDigikam_1_1ItemShortInfo.html", null ],
    [ "Digikam::ItemSortSettings", "classDigikam_1_1ItemSortSettings.html", null ],
    [ "Digikam::ItemTagPair", "classDigikam_1_1ItemTagPair.html", null ],
    [ "Digikam::ItemViewDelegatePrivate", "classDigikam_1_1ItemViewDelegatePrivate.html", null ],
    [ "Digikam::ItemViewImportDelegatePrivate", "classDigikam_1_1ItemViewImportDelegatePrivate.html", null ],
    [ "Digikam::JPEGUtils::digikam_source_mgr", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr.html", null ],
    [ "Digikam::JPEGUtils::JpegRotator", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html", null ],
    [ "Digikam::LBPHistogramMetadata", "classDigikam_1_1LBPHistogramMetadata.html", null ],
    [ "Digikam::LcmsLock", "classDigikam_1_1LcmsLock.html", null ],
    [ "Digikam::LensDistortionPixelAccess", "classDigikam_1_1LensDistortionPixelAccess.html", null ],
    [ "Digikam::LensFunContainer", "classDigikam_1_1LensFunContainer.html", null ],
    [ "Digikam::LessThanByProximityToSubject", "classDigikam_1_1LessThanByProximityToSubject.html", null ],
    [ "Digikam::LevelsContainer", "classDigikam_1_1LevelsContainer.html", null ],
    [ "Digikam::LoadingCache::CacheLock", "classDigikam_1_1LoadingCache_1_1CacheLock.html", null ],
    [ "Digikam::LoadingCacheFileWatch", "classDigikam_1_1LoadingCacheFileWatch.html", [
      [ "Digikam::ClassicLoadingCacheFileWatch", "classDigikam_1_1ClassicLoadingCacheFileWatch.html", [
        [ "Digikam::ScanControllerLoadingCacheFileWatch", "classDigikam_1_1ScanControllerLoadingCacheFileWatch.html", null ]
      ] ]
    ] ],
    [ "Digikam::LoadingCacheInterface", "classDigikam_1_1LoadingCacheInterface.html", null ],
    [ "Digikam::LoadingDescription", "classDigikam_1_1LoadingDescription.html", null ],
    [ "Digikam::LoadingDescription::PostProcessingParameters", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html", null ],
    [ "Digikam::LoadingDescription::PreviewParameters", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html", null ],
    [ "Digikam::LoadingProcess", "classDigikam_1_1LoadingProcess.html", [
      [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", null ]
    ] ],
    [ "Digikam::LoadingProcessListener", "classDigikam_1_1LoadingProcessListener.html", [
      [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", null ]
    ] ],
    [ "Digikam::LoadSaveFileInfoProvider", "classDigikam_1_1LoadSaveFileInfoProvider.html", [
      [ "Digikam::DatabaseLoadSaveFileInfoProvider", "classDigikam_1_1DatabaseLoadSaveFileInfoProvider.html", null ]
    ] ],
    [ "Digikam::LoadSaveNotifier", "classDigikam_1_1LoadSaveNotifier.html", [
      [ "Digikam::LoadSaveThread", null, [
        [ "Digikam::ManagedLoadSaveThread", "classDigikam_1_1ManagedLoadSaveThread.html", [
          [ "Digikam::PreviewLoadThread", "classDigikam_1_1PreviewLoadThread.html", [
            [ "Digikam::FacePreviewLoader", "classDigikam_1_1FacePreviewLoader.html", null ]
          ] ],
          [ "Digikam::SharedLoadSaveThread", "classDigikam_1_1SharedLoadSaveThread.html", null ],
          [ "Digikam::ThumbnailLoadThread", "classDigikam_1_1ThumbnailLoadThread.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::LoadSaveTask", "classDigikam_1_1LoadSaveTask.html", [
      [ "Digikam::LoadingTask", "classDigikam_1_1LoadingTask.html", null ],
      [ "Digikam::SavingTask", "classDigikam_1_1SavingTask.html", null ]
    ] ],
    [ "Digikam::LocalContrastContainer", "classDigikam_1_1LocalContrastContainer.html", null ],
    [ "Digikam::LookupAltitude::Request", "classDigikam_1_1LookupAltitude_1_1Request.html", null ],
    [ "Digikam::LookupFactory", "classDigikam_1_1LookupFactory.html", null ],
    [ "Digikam::MaintenanceSettings", "classDigikam_1_1MaintenanceSettings.html", null ],
    [ "Digikam::Mat", "structDigikam_1_1Mat.html", null ],
    [ "Digikam::MetaEngineMergeHelper< Data, Key, KeyString, KeyStringList >", "classDigikam_1_1MetaEngineMergeHelper.html", null ],
    [ "Digikam::MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html", null ],
    [ "Digikam::MetaEngineSettingsContainer", "classDigikam_1_1MetaEngineSettingsContainer.html", null ],
    [ "Digikam::MixerContainer", "classDigikam_1_1MixerContainer.html", null ],
    [ "Digikam::NamespaceEntry", "classDigikam_1_1NamespaceEntry.html", null ],
    [ "Digikam::NewlyAppearedFile", "classDigikam_1_1NewlyAppearedFile.html", null ],
    [ "Digikam::NRContainer", "classDigikam_1_1NRContainer.html", null ],
    [ "Digikam::OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html", null ],
    [ "Digikam::OpenCVMatData", "classDigikam_1_1OpenCVMatData.html", null ],
    [ "Digikam::OpenfacePreprocessor", "classDigikam_1_1OpenfacePreprocessor.html", null ],
    [ "Digikam::PageItem", "classDigikam_1_1PageItem.html", null ],
    [ "Digikam::PAlbumPath", "classDigikam_1_1PAlbumPath.html", null ],
    [ "Digikam::ParallelWorkers", "classDigikam_1_1ParallelWorkers.html", [
      [ "Digikam::ParallelAdapter< A >", "classDigikam_1_1ParallelAdapter.html", null ],
      [ "Digikam::ParallelAdapter< Digikam::FileWorkerInterface >", "classDigikam_1_1ParallelAdapter.html", null ]
    ] ],
    [ "Digikam::ParseResults", "classDigikam_1_1ParseResults.html", null ],
    [ "Digikam::ParseSettings", "classDigikam_1_1ParseSettings.html", null ],
    [ "Digikam::PhotoInfoContainer", "classDigikam_1_1PhotoInfoContainer.html", null ],
    [ "Digikam::PointTransformAffine", "classDigikam_1_1PointTransformAffine.html", null ],
    [ "Digikam::Preprocessor", "classDigikam_1_1Preprocessor.html", [
      [ "Digikam::RecognitionPreprocessor", "classDigikam_1_1RecognitionPreprocessor.html", null ]
    ] ],
    [ "Digikam::PreviewSettings", "classDigikam_1_1PreviewSettings.html", null ],
    [ "Digikam::ProgressEntry", "classDigikam_1_1ProgressEntry.html", null ],
    [ "Digikam::PTOType", "structDigikam_1_1PTOType.html", null ],
    [ "Digikam::PTOType::ControlPoint", "structDigikam_1_1PTOType_1_1ControlPoint.html", null ],
    [ "Digikam::PTOType::Image", "structDigikam_1_1PTOType_1_1Image.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< T >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Mask", "structDigikam_1_1PTOType_1_1Mask.html", null ],
    [ "Digikam::PTOType::Optimization", "structDigikam_1_1PTOType_1_1Optimization.html", null ],
    [ "Digikam::PTOType::Project", "structDigikam_1_1PTOType_1_1Project.html", null ],
    [ "Digikam::PTOType::Project::FileFormat", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html", null ],
    [ "Digikam::PTOType::Stitcher", "structDigikam_1_1PTOType_1_1Stitcher.html", null ],
    [ "Digikam::QueueSettings", "classDigikam_1_1QueueSettings.html", null ],
    [ "Digikam::RatingStarDrawer", "classDigikam_1_1RatingStarDrawer.html", [
      [ "Digikam::RatingComboBoxDelegate", "classDigikam_1_1RatingComboBoxDelegate.html", null ],
      [ "Digikam::RatingComboBoxWidget", "classDigikam_1_1RatingComboBoxWidget.html", null ]
    ] ],
    [ "Digikam::RecognitionBenchmarker::Statistics", "classDigikam_1_1RecognitionBenchmarker_1_1Statistics.html", null ],
    [ "Digikam::RedEye::RegressionTree", "structDigikam_1_1RedEye_1_1RegressionTree.html", null ],
    [ "Digikam::RedEye::ShapePredictor", "classDigikam_1_1RedEye_1_1ShapePredictor.html", null ],
    [ "Digikam::RedEye::SplitFeature", "structDigikam_1_1RedEye_1_1SplitFeature.html", null ],
    [ "Digikam::RedEyeCorrectionContainer", "classDigikam_1_1RedEyeCorrectionContainer.html", null ],
    [ "Digikam::RefocusMatrix", "classDigikam_1_1RefocusMatrix.html", null ],
    [ "Digikam::RGInfo", "classDigikam_1_1RGInfo.html", null ],
    [ "Digikam::RuleType", "classDigikam_1_1RuleType.html", null ],
    [ "Digikam::RuleTypeForConversion", "classDigikam_1_1RuleTypeForConversion.html", null ],
    [ "Digikam::SaveProperties", "classDigikam_1_1SaveProperties.html", null ],
    [ "Digikam::SavingContext", "classDigikam_1_1SavingContext.html", null ],
    [ "Digikam::ScanController::FileMetadataWrite", "classDigikam_1_1ScanController_1_1FileMetadataWrite.html", null ],
    [ "Digikam::ScanControllerCreator", "classDigikam_1_1ScanControllerCreator.html", null ],
    [ "Digikam::SchemeManager", "classDigikam_1_1SchemeManager.html", null ],
    [ "Digikam::SearchChangeset", "classDigikam_1_1SearchChangeset.html", null ],
    [ "Digikam::SearchInfo", "classDigikam_1_1SearchInfo.html", null ],
    [ "Digikam::SearchTextSettings", "classDigikam_1_1SearchTextSettings.html", [
      [ "Digikam::SearchTextFilterSettings", "classDigikam_1_1SearchTextFilterSettings.html", null ]
    ] ],
    [ "Digikam::SearchViewThemedPartsCache", "classDigikam_1_1SearchViewThemedPartsCache.html", null ],
    [ "Digikam::SetupCollectionModel::Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html", null ],
    [ "Digikam::SharpContainer", "classDigikam_1_1SharpContainer.html", null ],
    [ "Digikam::SidecarFinder", "classDigikam_1_1SidecarFinder.html", null ],
    [ "Digikam::SimilarityDbAccess", "classDigikam_1_1SimilarityDbAccess.html", null ],
    [ "Digikam::SimpleTreeModel::Item", "classDigikam_1_1SimpleTreeModel_1_1Item.html", null ],
    [ "Digikam::SlideShowSettings", "classDigikam_1_1SlideShowSettings.html", null ],
    [ "Digikam::SolidVolumeInfo", "classDigikam_1_1SolidVolumeInfo.html", null ],
    [ "Digikam::State", "structDigikam_1_1State.html", null ],
    [ "Digikam::SubjectData", "classDigikam_1_1SubjectData.html", null ],
    [ "Digikam::SubQueryBuilder", "classDigikam_1_1SubQueryBuilder.html", null ],
    [ "Digikam::TableViewColumnConfiguration", "classDigikam_1_1TableViewColumnConfiguration.html", null ],
    [ "Digikam::TableViewColumnDescription", "classDigikam_1_1TableViewColumnDescription.html", null ],
    [ "Digikam::TableViewColumnProfile", "classDigikam_1_1TableViewColumnProfile.html", null ],
    [ "Digikam::TableViewModel::Item", "classDigikam_1_1TableViewModel_1_1Item.html", null ],
    [ "Digikam::TableViewShared", "classDigikam_1_1TableViewShared.html", null ],
    [ "Digikam::TagChangeset", "classDigikam_1_1TagChangeset.html", null ],
    [ "Digikam::TagData", "structDigikam_1_1TagData.html", null ],
    [ "Digikam::TaggingAction", "classDigikam_1_1TaggingAction.html", null ],
    [ "Digikam::TaggingActionFactory::ConstraintInterface", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface.html", null ],
    [ "Digikam::TagInfo", "classDigikam_1_1TagInfo.html", null ],
    [ "Digikam::TagProperty", "classDigikam_1_1TagProperty.html", null ],
    [ "Digikam::TagPropertyName", "classDigikam_1_1TagPropertyName.html", null ],
    [ "Digikam::TagRegion", "classDigikam_1_1TagRegion.html", null ],
    [ "Digikam::TagShortInfo", "classDigikam_1_1TagShortInfo.html", null ],
    [ "Digikam::TanTriggsPreprocessor", "classDigikam_1_1TanTriggsPreprocessor.html", null ],
    [ "Digikam::Template", "classDigikam_1_1Template.html", null ],
    [ "Digikam::ThumbnailIdentifier", "classDigikam_1_1ThumbnailIdentifier.html", [
      [ "Digikam::ThumbnailInfo", "classDigikam_1_1ThumbnailInfo.html", null ]
    ] ],
    [ "Digikam::ThumbnailImage", "classDigikam_1_1ThumbnailImage.html", null ],
    [ "Digikam::ThumbnailImageCatcher::CatcherResult", "classDigikam_1_1ThumbnailImageCatcher_1_1CatcherResult.html", null ],
    [ "Digikam::ThumbnailInfoProvider", "classDigikam_1_1ThumbnailInfoProvider.html", [
      [ "Digikam::ThumbsDbInfoProvider", "classDigikam_1_1ThumbsDbInfoProvider.html", null ]
    ] ],
    [ "Digikam::ThumbnailLoadThreadStaticPriv", "classDigikam_1_1ThumbnailLoadThreadStaticPriv.html", null ],
    [ "Digikam::ThumbnailResult", "classDigikam_1_1ThumbnailResult.html", null ],
    [ "Digikam::ThumbnailSize", "classDigikam_1_1ThumbnailSize.html", null ],
    [ "Digikam::ThumbsDbAccess", "classDigikam_1_1ThumbsDbAccess.html", null ],
    [ "Digikam::ThumbsDbInfo", "classDigikam_1_1ThumbsDbInfo.html", null ],
    [ "Digikam::TileIndex", "classDigikam_1_1TileIndex.html", null ],
    [ "Digikam::TimeAdjustContainer", "classDigikam_1_1TimeAdjustContainer.html", null ],
    [ "Digikam::TonalityContainer", "classDigikam_1_1TonalityContainer.html", null ],
    [ "Digikam::TooltipCreator", "classDigikam_1_1TooltipCreator.html", null ],
    [ "Digikam::TrackCorrelator::Correlation", "classDigikam_1_1TrackCorrelator_1_1Correlation.html", null ],
    [ "Digikam::TrackCorrelator::CorrelationOptions", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html", null ],
    [ "Digikam::TrackManager::Track", "classDigikam_1_1TrackManager_1_1Track.html", null ],
    [ "Digikam::TrackManager::TrackPoint", "classDigikam_1_1TrackManager_1_1TrackPoint.html", null ],
    [ "Digikam::TrackReader::TrackReadResult", "classDigikam_1_1TrackReader_1_1TrackReadResult.html", null ],
    [ "Digikam::TrainingDataProvider", "classDigikam_1_1TrainingDataProvider.html", [
      [ "Digikam::RecognitionTrainingProvider", "classDigikam_1_1RecognitionTrainingProvider.html", null ]
    ] ],
    [ "Digikam::TreeBranch", "classDigikam_1_1TreeBranch.html", null ],
    [ "Digikam::UndoMetadataContainer", "classDigikam_1_1UndoMetadataContainer.html", null ],
    [ "Digikam::UndoState", "classDigikam_1_1UndoState.html", null ],
    [ "Digikam::VersionFileInfo", "classDigikam_1_1VersionFileInfo.html", null ],
    [ "Digikam::VersionFileOperation", "classDigikam_1_1VersionFileOperation.html", null ],
    [ "Digikam::VersionItemFilterSettings", "classDigikam_1_1VersionItemFilterSettings.html", null ],
    [ "Digikam::VersionManagerSettings", "classDigikam_1_1VersionManagerSettings.html", null ],
    [ "Digikam::VersionNamingScheme", "classDigikam_1_1VersionNamingScheme.html", null ],
    [ "Digikam::VideoFrame", "classDigikam_1_1VideoFrame.html", null ],
    [ "Digikam::VideoInfoContainer", "classDigikam_1_1VideoInfoContainer.html", null ],
    [ "Digikam::VideoMetadataContainer", "classDigikam_1_1VideoMetadataContainer.html", null ],
    [ "Digikam::VideoStripFilter", "classDigikam_1_1VideoStripFilter.html", null ],
    [ "Digikam::VideoThumbWriter", "classDigikam_1_1VideoThumbWriter.html", null ],
    [ "Digikam::VidSlideSettings", "classDigikam_1_1VidSlideSettings.html", null ],
    [ "Digikam::VisibilityObject", "classDigikam_1_1VisibilityObject.html", [
      [ "Digikam::SearchField", "classDigikam_1_1SearchField.html", [
        [ "Digikam::SearchFieldAlbum", "classDigikam_1_1SearchFieldAlbum.html", null ],
        [ "Digikam::SearchFieldCheckBox", "classDigikam_1_1SearchFieldCheckBox.html", null ],
        [ "Digikam::SearchFieldChoice", "classDigikam_1_1SearchFieldChoice.html", null ],
        [ "Digikam::SearchFieldComboBox", "classDigikam_1_1SearchFieldComboBox.html", [
          [ "Digikam::SearchFieldColorDepth", "classDigikam_1_1SearchFieldColorDepth.html", null ],
          [ "Digikam::SearchFieldPageOrientation", "classDigikam_1_1SearchFieldPageOrientation.html", null ]
        ] ],
        [ "Digikam::SearchFieldLabels", "classDigikam_1_1SearchFieldLabels.html", null ],
        [ "Digikam::SearchFieldRangeDate", "classDigikam_1_1SearchFieldRangeDate.html", null ],
        [ "Digikam::SearchFieldRangeDouble", "classDigikam_1_1SearchFieldRangeDouble.html", null ],
        [ "Digikam::SearchFieldRangeInt", "classDigikam_1_1SearchFieldRangeInt.html", null ],
        [ "Digikam::SearchFieldRating", "classDigikam_1_1SearchFieldRating.html", null ],
        [ "Digikam::SearchFieldText", "classDigikam_1_1SearchFieldText.html", [
          [ "Digikam::SearchFieldKeyword", "classDigikam_1_1SearchFieldKeyword.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::WBContainer", "classDigikam_1_1WBContainer.html", null ],
    [ "Digikam::Workflow", "classDigikam_1_1Workflow.html", null ],
    [ "Digikam::WSAlbum", "classDigikam_1_1WSAlbum.html", [
      [ "DigikamGenericFaceBookPlugin::FbAlbum", "classDigikamGenericFaceBookPlugin_1_1FbAlbum.html", null ]
    ] ],
    [ "Digikam::WSToolUtils", "classDigikam_1_1WSToolUtils.html", null ],
    [ "DigikamEditorHotPixelsToolPlugin::HotPixel", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixel.html", null ],
    [ "DigikamEditorHotPixelsToolPlugin::HotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html", null ],
    [ "DigikamEditorPerspectiveToolPlugin::PerspectiveMatrix", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html", null ],
    [ "DigikamEditorPerspectiveToolPlugin::PerspectiveTriangle", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle.html", null ],
    [ "DigikamGenericBoxPlugin::BOXFolder", "classDigikamGenericBoxPlugin_1_1BOXFolder.html", null ],
    [ "DigikamGenericBoxPlugin::BOXPhoto", "classDigikamGenericBoxPlugin_1_1BOXPhoto.html", null ],
    [ "DigikamGenericCalendarPlugin::CalParams", "structDigikamGenericCalendarPlugin_1_1CalParams.html", null ],
    [ "DigikamGenericCalendarPlugin::CalSystem", "classDigikamGenericCalendarPlugin_1_1CalSystem.html", null ],
    [ "DigikamGenericDebianScreenshotsPlugin::DSMPForm", "classDigikamGenericDebianScreenshotsPlugin_1_1DSMPForm.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBFolder", "classDigikamGenericDropBoxPlugin_1_1DBFolder.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBMPForm", "classDigikamGenericDropBoxPlugin_1_1DBMPForm.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBPhoto", "classDigikamGenericDropBoxPlugin_1_1DBPhoto.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::EnfuseSettings", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseSettings.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingActionData", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingActionData.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingItemPreprocessedUrls", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingItemPreprocessedUrls.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbMPForm", "classDigikamGenericFaceBookPlugin_1_1FbMPForm.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbPhoto", "classDigikamGenericFaceBookPlugin_1_1FbPhoto.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbUser", "classDigikamGenericFaceBookPlugin_1_1FbUser.html", null ],
    [ "DigikamGenericFlickrPlugin::FlickrMPForm", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm.html", null ],
    [ "DigikamGenericFlickrPlugin::FPhotoInfo", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo.html", null ],
    [ "DigikamGenericFlickrPlugin::FPhotoSet", "classDigikamGenericFlickrPlugin_1_1FPhotoSet.html", null ],
    [ "DigikamGenericFlickrPlugin::GAlbum", "classDigikamGenericFlickrPlugin_1_1GAlbum.html", null ],
    [ "DigikamGenericFlickrPlugin::GPhoto", "classDigikamGenericFlickrPlugin_1_1GPhoto.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::GeoDataContainer", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataContainer.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::GeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataParser.html", [
      [ "DigikamGenericGeolocationEditPlugin::KMLGeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1KMLGeoDataParser.html", null ]
    ] ],
    [ "DigikamGenericGeolocationEditPlugin::SearchBackend::SearchResult", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::SearchResultModel::SearchResultItem", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem.html", null ],
    [ "DigikamGenericGLViewerPlugin::GLViewerTimer", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GDMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GPMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::CWrapper< Ptr, freeFcn >", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig::EnumFullFormat", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig_1_1EnumFullFormat.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig::EnumThumbnailFormat", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig_1_1EnumThumbnailFormat.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryElement", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElement.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryElementFunctor", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElementFunctor.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryNameHelper", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryNameHelper.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLAttributeList", "classDigikamGenericHtmlGalleryPlugin_1_1XMLAttributeList.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLElement", "classDigikamGenericHtmlGalleryPlugin_1_1XMLElement.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLWriter", "classDigikamGenericHtmlGalleryPlugin_1_1XMLWriter.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackGallery", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackMPForm", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackPhoto", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerAction", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult::ImgurAccount", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult::ImgurImage", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerAction", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerResult", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerResult::IPFSImage", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult_1_1IPFSImage.html", null ],
    [ "DigikamGenericJAlbumPlugin::JAlbumSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODFolder", "classDigikamGenericOneDrivePlugin_1_1ODFolder.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODMPForm", "classDigikamGenericOneDrivePlugin_1_1ODMPForm.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODPhoto", "classDigikamGenericOneDrivePlugin_1_1ODPhoto.html", null ],
    [ "DigikamGenericPanoramaPlugin::PanoActionData", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html", null ],
    [ "DigikamGenericPanoramaPlugin::PanoramaPreprocessedUrls", "structDigikamGenericPanoramaPlugin_1_1PanoramaPreprocessedUrls.html", null ],
    [ "DigikamGenericPinterestPlugin::PFolder", "classDigikamGenericPinterestPlugin_1_1PFolder.html", null ],
    [ "DigikamGenericPinterestPlugin::PPhoto", "classDigikamGenericPinterestPlugin_1_1PPhoto.html", null ],
    [ "DigikamGenericPiwigoPlugin::PiwigoAlbum", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum.html", null ],
    [ "DigikamGenericPresentationPlugin::KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html", [
      [ "DigikamGenericPresentationPlugin::BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html", null ],
      [ "DigikamGenericPresentationPlugin::FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html", null ]
    ] ],
    [ "DigikamGenericPresentationPlugin::KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html", null ],
    [ "DigikamGenericPresentationPlugin::KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html", null ],
    [ "DigikamGenericPresentationPlugin::PresentationContainer", "classDigikamGenericPresentationPlugin_1_1PresentationContainer.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintPhotoSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AtkinsPageLayoutTree", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree.html", null ],
    [ "DigikamGenericRajcePlugin::RajceAlbum", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html", null ],
    [ "DigikamGenericRajcePlugin::RajceMPForm", "classDigikamGenericRajcePlugin_1_1RajceMPForm.html", null ],
    [ "DigikamGenericSendByMailPlugin::MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html", null ],
    [ "DigikamGenericSmugPlugin::SmugAlbum", "classDigikamGenericSmugPlugin_1_1SmugAlbum.html", null ],
    [ "DigikamGenericSmugPlugin::SmugAlbumTmpl", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl.html", null ],
    [ "DigikamGenericSmugPlugin::SmugCategory", "classDigikamGenericSmugPlugin_1_1SmugCategory.html", null ],
    [ "DigikamGenericSmugPlugin::SmugMPForm", "classDigikamGenericSmugPlugin_1_1SmugMPForm.html", null ],
    [ "DigikamGenericSmugPlugin::SmugPhoto", "classDigikamGenericSmugPlugin_1_1SmugPhoto.html", null ],
    [ "DigikamGenericSmugPlugin::SmugUser", "classDigikamGenericSmugPlugin_1_1SmugUser.html", null ],
    [ "DigikamGenericTwitterPlugin::TwAlbum", "classDigikamGenericTwitterPlugin_1_1TwAlbum.html", null ],
    [ "DigikamGenericTwitterPlugin::TwMPForm", "classDigikamGenericTwitterPlugin_1_1TwMPForm.html", null ],
    [ "DigikamGenericTwitterPlugin::TwPhoto", "classDigikamGenericTwitterPlugin_1_1TwPhoto.html", null ],
    [ "DigikamGenericTwitterPlugin::TwUser", "classDigikamGenericTwitterPlugin_1_1TwUser.html", null ],
    [ "DigikamGenericVKontaktePlugin::VKNewAlbumDlg::AlbumProperties", "structDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg_1_1AlbumProperties.html", null ],
    [ "DigikamGenericYFPlugin::YandexFotkiAlbum", "classDigikamGenericYFPlugin_1_1YandexFotkiAlbum.html", null ],
    [ "DigikamGenericYFPlugin::YFPhoto", "classDigikamGenericYFPlugin_1_1YFPhoto.html", null ],
    [ "GraphicsDImgItemPrivate", null, [
      [ "Digikam::DImgPreviewItem", "classDigikam_1_1DImgPreviewItem.html", null ]
    ] ],
    [ "dng_host", null, [
      [ "Digikam::DNGWriterHost", "classDigikam_1_1DNGWriterHost.html", null ]
    ] ],
    [ "Digikam::DSharedDataPointer< Digikam::ItemInfoData >", "classDigikam_1_1DSharedDataPointer.html", null ],
    [ "en265_packet", "structen265__packet.html", null ],
    [ "enc_node", "classenc__node.html", [
      [ "enc_cb", "classenc__cb.html", null ],
      [ "enc_tb", "classenc__tb.html", null ]
    ] ],
    [ "enc_pb_inter", "structenc__pb__inter.html", null ],
    [ "encoder_params", "structencoder__params.html", null ],
    [ "encoder_picture_buffer", "classencoder__picture__buffer.html", null ],
    [ "EncoderCore", "classEncoderCore.html", [
      [ "EncoderCore_Custom", "classEncoderCore__Custom.html", null ]
    ] ],
    [ "error_queue", "classerror__queue.html", [
      [ "base_context", "classbase__context.html", [
        [ "decoder_context", "classdecoder__context.html", null ],
        [ "encoder_context", "classencoder__context.html", null ]
      ] ]
    ] ],
    [ "GraphicsDImgItemPrivate", null, [
      [ "Digikam::GraphicsDImgItem", "classDigikam_1_1GraphicsDImgItem.html", [
        [ "Digikam::DImgPreviewItem", "classDigikam_1_1DImgPreviewItem.html", null ],
        [ "Digikam::ImagePreviewItem", "classDigikam_1_1ImagePreviewItem.html", null ]
      ] ]
    ] ],
    [ "heif::BitReader", "classheif_1_1BitReader.html", null ],
    [ "heif::BitstreamRange", "classheif_1_1BitstreamRange.html", null ],
    [ "heif::Box_grpl::EntityGroup", "structheif_1_1Box__grpl_1_1EntityGroup.html", null ],
    [ "heif::Box_hvcC::configuration", "structheif_1_1Box__hvcC_1_1configuration.html", null ],
    [ "heif::Box_iloc::Extent", "structheif_1_1Box__iloc_1_1Extent.html", null ],
    [ "heif::Box_iloc::Item", "structheif_1_1Box__iloc_1_1Item.html", null ],
    [ "heif::Box_ipco::Property", "structheif_1_1Box__ipco_1_1Property.html", null ],
    [ "heif::Box_ipma::Entry", "structheif_1_1Box__ipma_1_1Entry.html", null ],
    [ "heif::Box_ipma::PropertyAssociation", "structheif_1_1Box__ipma_1_1PropertyAssociation.html", null ],
    [ "heif::Box_iref::Reference", "structheif_1_1Box__iref_1_1Reference.html", null ],
    [ "heif::BoxHeader", "classheif_1_1BoxHeader.html", [
      [ "heif::Box", "classheif_1_1Box.html", [
        [ "heif::Box_auxC", "classheif_1_1Box__auxC.html", null ],
        [ "heif::Box_clap", "classheif_1_1Box__clap.html", null ],
        [ "heif::Box_colr", "classheif_1_1Box__colr.html", null ],
        [ "heif::Box_dinf", "classheif_1_1Box__dinf.html", null ],
        [ "heif::Box_dref", "classheif_1_1Box__dref.html", null ],
        [ "heif::Box_ftyp", "classheif_1_1Box__ftyp.html", null ],
        [ "heif::Box_grpl", "classheif_1_1Box__grpl.html", null ],
        [ "heif::Box_hdlr", "classheif_1_1Box__hdlr.html", null ],
        [ "heif::Box_hvcC", "classheif_1_1Box__hvcC.html", null ],
        [ "heif::Box_idat", "classheif_1_1Box__idat.html", null ],
        [ "heif::Box_iinf", "classheif_1_1Box__iinf.html", null ],
        [ "heif::Box_iloc", "classheif_1_1Box__iloc.html", null ],
        [ "heif::Box_imir", "classheif_1_1Box__imir.html", null ],
        [ "heif::Box_infe", "classheif_1_1Box__infe.html", null ],
        [ "heif::Box_ipco", "classheif_1_1Box__ipco.html", null ],
        [ "heif::Box_ipma", "classheif_1_1Box__ipma.html", null ],
        [ "heif::Box_iprp", "classheif_1_1Box__iprp.html", null ],
        [ "heif::Box_iref", "classheif_1_1Box__iref.html", null ],
        [ "heif::Box_irot", "classheif_1_1Box__irot.html", null ],
        [ "heif::Box_ispe", "classheif_1_1Box__ispe.html", null ],
        [ "heif::Box_meta", "classheif_1_1Box__meta.html", null ],
        [ "heif::Box_pitm", "classheif_1_1Box__pitm.html", null ],
        [ "heif::Box_pixi", "classheif_1_1Box__pixi.html", null ],
        [ "heif::Box_url", "classheif_1_1Box__url.html", null ]
      ] ]
    ] ],
    [ "heif::color_profile", "classheif_1_1color__profile.html", [
      [ "heif::color_profile_nclx", "classheif_1_1color__profile__nclx.html", null ],
      [ "heif::color_profile_raw", "classheif_1_1color__profile__raw.html", null ]
    ] ],
    [ "heif::ColorConversionCosts", "structheif_1_1ColorConversionCosts.html", null ],
    [ "heif::ColorConversionOperation", "classheif_1_1ColorConversionOperation.html", null ],
    [ "heif::ColorConversionOptions", "structheif_1_1ColorConversionOptions.html", null ],
    [ "heif::ColorConversionPipeline", "classheif_1_1ColorConversionPipeline.html", null ],
    [ "heif::ColorState", "structheif_1_1ColorState.html", null ],
    [ "heif::ColorStateWithCost", "structheif_1_1ColorStateWithCost.html", null ],
    [ "heif::Context", "classheif_1_1Context.html", null ],
    [ "heif::Context::Reader", "classheif_1_1Context_1_1Reader.html", null ],
    [ "heif::Context::ReadingOptions", "classheif_1_1Context_1_1ReadingOptions.html", null ],
    [ "heif::Context::Writer", "classheif_1_1Context_1_1Writer.html", null ],
    [ "heif::Encoder", "classheif_1_1Encoder.html", null ],
    [ "heif::EncoderDescriptor", "classheif_1_1EncoderDescriptor.html", null ],
    [ "heif::EncoderParameter", "classheif_1_1EncoderParameter.html", null ],
    [ "heif::Error", "classheif_1_1Error.html", null ],
    [ "heif::ErrorBuffer", "classheif_1_1ErrorBuffer.html", [
      [ "heif::HeifContext", "classheif_1_1HeifContext.html", null ],
      [ "heif::HeifContext::Image", "classheif_1_1HeifContext_1_1Image.html", null ],
      [ "heif::HeifPixelImage", "classheif_1_1HeifPixelImage.html", null ]
    ] ],
    [ "heif::Fraction", "classheif_1_1Fraction.html", null ],
    [ "heif::HeifFile", "classheif_1_1HeifFile.html", null ],
    [ "heif::Image", "classheif_1_1Image.html", null ],
    [ "heif::Image::ScalingOptions", "classheif_1_1Image_1_1ScalingOptions.html", null ],
    [ "heif::ImageHandle", "classheif_1_1ImageHandle.html", null ],
    [ "heif::ImageHandle::DecodingOptions", "classheif_1_1ImageHandle_1_1DecodingOptions.html", null ],
    [ "heif::ImageMetadata", "classheif_1_1ImageMetadata.html", null ],
    [ "heif::Indent", "classheif_1_1Indent.html", null ],
    [ "heif::SEIMessage", "classheif_1_1SEIMessage.html", [
      [ "heif::SEIMessage_depth_representation_info", "classheif_1_1SEIMessage__depth__representation__info.html", null ]
    ] ],
    [ "heif::StreamReader", "classheif_1_1StreamReader.html", [
      [ "heif::StreamReader_CApi", "classheif_1_1StreamReader__CApi.html", null ],
      [ "heif::StreamReader_istream", "classheif_1_1StreamReader__istream.html", null ],
      [ "heif::StreamReader_memory", "classheif_1_1StreamReader__memory.html", null ]
    ] ],
    [ "heif::StreamWriter", "classheif_1_1StreamWriter.html", null ],
    [ "heif_color_profile_nclx", "structheif__color__profile__nclx.html", null ],
    [ "heif_context", "structheif__context.html", null ],
    [ "heif_decoder_plugin", "structheif__decoder__plugin.html", null ],
    [ "heif_decoding_options", "structheif__decoding__options.html", null ],
    [ "heif_depth_representation_info", "structheif__depth__representation__info.html", [
      [ "heif::SEIMessage_depth_representation_info", "classheif_1_1SEIMessage__depth__representation__info.html", null ]
    ] ],
    [ "heif_encoder", "structheif__encoder.html", null ],
    [ "heif_encoder_descriptor", "structheif__encoder__descriptor.html", null ],
    [ "heif_encoder_parameter", "structheif__encoder__parameter.html", null ],
    [ "heif_encoder_plugin", "structheif__encoder__plugin.html", null ],
    [ "heif_encoding_options", "structheif__encoding__options.html", [
      [ "heif::Context::EncodingOptions", "classheif_1_1Context_1_1EncodingOptions.html", null ]
    ] ],
    [ "heif_error", "structheif__error.html", null ],
    [ "heif_image", "structheif__image.html", null ],
    [ "heif_image_handle", "structheif__image__handle.html", null ],
    [ "heif_reader", "structheif__reader.html", null ],
    [ "heif_writer", "structheif__writer.html", null ],
    [ "image_data", "structimage__data.html", null ],
    [ "image_unit", "classimage__unit.html", null ],
    [ "ImageSink", "classImageSink.html", [
      [ "ImageSink_YUV", "classImageSink__YUV.html", null ]
    ] ],
    [ "ImageSource", "classImageSource.html", [
      [ "ImageSource_YUV", "classImageSource__YUV.html", null ]
    ] ],
    [ "ImportDelegatePrivate", null, [
      [ "Digikam::ImportNormalDelegatePrivate", "classDigikam_1_1ImportNormalDelegatePrivate.html", null ],
      [ "Digikam::ImportThumbnailDelegatePrivate", "classDigikam_1_1ImportThumbnailDelegatePrivate.html", null ]
    ] ],
    [ "ImportDelegatePrivate public ItemViewImportDelegatePrivate", null, [
      [ "Digikam::ImportDelegate", "classDigikam_1_1ImportDelegate.html", null ]
    ] ],
    [ "intra_border_computer< pixel_t >", "classintra__border__computer.html", null ],
    [ "ItemDelegatePrivate", null, [
      [ "Digikam::DigikamItemDelegatePrivate", "classDigikam_1_1DigikamItemDelegatePrivate.html", [
        [ "Digikam::ItemFaceDelegatePrivate", "classDigikam_1_1ItemFaceDelegatePrivate.html", null ]
      ] ],
      [ "Digikam::ItemThumbnailDelegatePrivate", "classDigikam_1_1ItemThumbnailDelegatePrivate.html", null ]
    ] ],
    [ "ItemDelegatePrivate public ItemViewDelegatePrivate", null, [
      [ "Digikam::ItemDelegate", "classDigikam_1_1ItemDelegate.html", null ]
    ] ],
    [ "ItemFilterModelPrivate public QObject", null, [
      [ "Digikam::ItemFilterModel", "classDigikam_1_1ItemFilterModel.html", [
        [ "Digikam::ItemAlbumFilterModel", "classDigikam_1_1ItemAlbumFilterModel.html", null ]
      ] ]
    ] ],
    [ "KConfigSkeleton", null, [
      [ "DigikamEditorPrintToolPlugin::PrintConfig", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig.html", [
        [ "DigikamGenericHtmlGalleryPlugin::GalleryInfo", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryInfo.html", null ]
      ] ]
    ] ],
    [ "KJob", null, [
      [ "DigikamGenericMediaWikiPlugin::MediaWikiTalker", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiTalker.html", null ]
    ] ],
    [ "KXmlGuiWindow", null, [
      [ "Digikam::DXmlGuiWindow", null, [
        [ "Digikam::DigikamApp", "classDigikam_1_1DigikamApp.html", null ],
        [ "Digikam::EditorWindow", "classDigikam_1_1EditorWindow.html", [
          [ "Digikam::ImageWindow", "classDigikam_1_1ImageWindow.html", null ],
          [ "ShowFoto::ShowFoto", "classShowFoto_1_1ShowFoto.html", null ]
        ] ],
        [ "Digikam::ImportUI", "classDigikam_1_1ImportUI.html", null ],
        [ "Digikam::LightTableWindow", "classDigikam_1_1LightTableWindow.html", null ],
        [ "Digikam::QueueMgrWindow", "classDigikam_1_1QueueMgrWindow.html", null ]
      ] ]
    ] ],
    [ "layer_data", "structlayer__data.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< double >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< int >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< VignettingMode >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Logging", "classLogging.html", null ],
    [ "LayerInterface", null, [
      [ "Digikam::BackendMarbleLayer", "classDigikam_1_1BackendMarbleLayer.html", null ]
    ] ],
    [ "MD5_CTX", "structMD5__CTX.html", null ],
    [ "MetaDataArray< DataUnit >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< CB_ref_info >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< CTB_info >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< PBMotion >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< uint8_t >", "classMetaDataArray.html", null ],
    [ "Digikam::MetaEngineMergeHelper< Exiv2::ExifData, Exiv2::ExifKey, QLatin1String >", "classDigikam_1_1MetaEngineMergeHelper.html", [
      [ "Digikam::ExifMetaEngineMergeHelper", "classDigikam_1_1ExifMetaEngineMergeHelper.html", null ]
    ] ],
    [ "Digikam::MetaEngineMergeHelper< Exiv2::IptcData, Exiv2::IptcKey, QLatin1String >", "classDigikam_1_1MetaEngineMergeHelper.html", [
      [ "Digikam::IptcMetaEngineMergeHelper", "classDigikam_1_1IptcMetaEngineMergeHelper.html", null ]
    ] ],
    [ "MotionVector", "classMotionVector.html", null ],
    [ "MotionVectorAccess", "classMotionVectorAccess.html", null ],
    [ "nal_header", "structnal__header.html", null ],
    [ "NAL_Parser", "classNAL__Parser.html", null ],
    [ "NAL_unit", "classNAL__unit.html", null ],
    [ "option_base", "classoption__base.html", [
      [ "choice_option_base", "classchoice__option__base.html", [
        [ "choice_option< T >", "classchoice__option.html", null ],
        [ "choice_option< enum ALGO_CB_IntraPartMode >", "classchoice__option.html", [
          [ "option_ALGO_CB_IntraPartMode", "classoption__ALGO__CB__IntraPartMode.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_IntraPredMode >", "classchoice__option.html", [
          [ "option_ALGO_TB_IntraPredMode", "classoption__ALGO__TB__IntraPredMode.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_IntraPredMode_Subset >", "classchoice__option.html", [
          [ "option_ALGO_TB_IntraPredMode_Subset", "classoption__ALGO__TB__IntraPredMode__Subset.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_RateEstimation >", "classchoice__option.html", [
          [ "option_ALGO_TB_RateEstimation", "classoption__ALGO__TB__RateEstimation.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_Split_BruteForce_ZeroBlockPrune >", "classchoice__option.html", [
          [ "option_ALGO_TB_Split_BruteForce_ZeroBlockPrune", "classoption__ALGO__TB__Split__BruteForce__ZeroBlockPrune.html", null ]
        ] ],
        [ "choice_option< enum MEMode >", "classchoice__option.html", [
          [ "option_MEMode", "classoption__MEMode.html", null ]
        ] ],
        [ "choice_option< enum MVSearchAlgo >", "classchoice__option.html", [
          [ "option_MVSearchAlgo", "classoption__MVSearchAlgo.html", null ]
        ] ],
        [ "choice_option< enum MVTestMode >", "classchoice__option.html", [
          [ "option_MVTestMode", "classoption__MVTestMode.html", null ]
        ] ],
        [ "choice_option< enum PartMode >", "classchoice__option.html", [
          [ "option_InterPartMode", "classoption__InterPartMode.html", null ],
          [ "option_PartMode", "classoption__PartMode.html", null ]
        ] ],
        [ "choice_option< enum SOP_Structure >", "classchoice__option.html", [
          [ "option_SOP_Structure", "classoption__SOP__Structure.html", null ]
        ] ],
        [ "choice_option< enum TBBitrateEstimMethod >", "classchoice__option.html", [
          [ "option_TBBitrateEstimMethod", "classoption__TBBitrateEstimMethod.html", null ]
        ] ]
      ] ],
      [ "option_bool", "classoption__bool.html", null ],
      [ "option_int", "classoption__int.html", null ],
      [ "option_string", "classoption__string.html", null ]
    ] ],
    [ "PacketSink", "classPacketSink.html", [
      [ "PacketSink_File", "classPacketSink__File.html", null ]
    ] ],
    [ "PBMotion", "classPBMotion.html", null ],
    [ "PBMotionCoding", "classPBMotionCoding.html", null ],
    [ "pic_order_counter", "classpic__order__counter.html", [
      [ "sop_creator", "classsop__creator.html", [
        [ "sop_creator_intra_only", "classsop__creator__intra__only.html", null ],
        [ "sop_creator_trivial_low_delay", "classsop__creator__trivial__low__delay.html", null ]
      ] ]
    ] ],
    [ "pic_parameter_set", "classpic__parameter__set.html", null ],
    [ "PixelAccessor", "classPixelAccessor.html", null ],
    [ "PLT_MediaServer", null, [
      [ "Digikam::DLNAMediaServer", "classDigikam_1_1DLNAMediaServer.html", null ]
    ] ],
    [ "PLT_MediaServerDelegate", null, [
      [ "Digikam::DLNAMediaServerDelegate", null, [
        [ "Digikam::DLNAMediaServer", "classDigikam_1_1DLNAMediaServer.html", null ]
      ] ]
    ] ],
    [ "position", "structposition.html", null ],
    [ "pps_range_extension", "classpps__range__extension.html", null ],
    [ "Private", null, [
      [ "Digikam::AbstractAlbumModel", null, [
        [ "Digikam::AbstractSpecificAlbumModel", "classDigikam_1_1AbstractSpecificAlbumModel.html", [
          [ "Digikam::AbstractCountingAlbumModel", null, [
            [ "Digikam::AbstractCheckableAlbumModel", null, [
              [ "Digikam::AlbumModel", "classDigikam_1_1AlbumModel.html", null ],
              [ "Digikam::SearchModel", "classDigikam_1_1SearchModel.html", null ],
              [ "Digikam::TagModel", "classDigikam_1_1TagModel.html", null ]
            ] ],
            [ "Digikam::DateAlbumModel", "classDigikam_1_1DateAlbumModel.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::AbstractAlbumTreeView", "classDigikam_1_1AbstractAlbumTreeView.html", [
        [ "Digikam::AbstractCountingAlbumTreeView", "classDigikam_1_1AbstractCountingAlbumTreeView.html", [
          [ "Digikam::AbstractCheckableAlbumTreeView", null, [
            [ "Digikam::AlbumTreeView", "classDigikam_1_1AlbumTreeView.html", null ],
            [ "Digikam::SearchTreeView", "classDigikam_1_1SearchTreeView.html", null ],
            [ "Digikam::TagTreeView", "classDigikam_1_1TagTreeView.html", null ]
          ] ],
          [ "Digikam::DateTreeView", "classDigikam_1_1DateTreeView.html", null ]
        ] ]
      ] ],
      [ "Digikam::AbstractCheckableAlbumModel", null, null ],
      [ "Digikam::AbstractCheckableAlbumTreeView", null, null ],
      [ "Digikam::AbstractCountingAlbumModel", null, null ],
      [ "Digikam::ActionThreadBase", null, [
        [ "Digikam::DBJobsThread", "classDigikam_1_1DBJobsThread.html", [
          [ "Digikam::AlbumsDBJobsThread", "classDigikam_1_1AlbumsDBJobsThread.html", null ],
          [ "Digikam::DatesDBJobsThread", "classDigikam_1_1DatesDBJobsThread.html", null ],
          [ "Digikam::GPSDBJobsThread", "classDigikam_1_1GPSDBJobsThread.html", null ],
          [ "Digikam::SearchesDBJobsThread", "classDigikam_1_1SearchesDBJobsThread.html", null ],
          [ "Digikam::TagsDBJobsThread", "classDigikam_1_1TagsDBJobsThread.html", null ]
        ] ],
        [ "Digikam::MaintenanceThread", "classDigikam_1_1MaintenanceThread.html", null ],
        [ "Digikam::VidSlideThread", "classDigikam_1_1VidSlideThread.html", null ],
        [ "DigikamGenericFileCopyPlugin::FCThread", "classDigikamGenericFileCopyPlugin_1_1FCThread.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintThread", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread.html", null ],
        [ "DigikamGenericSendByMailPlugin::ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html", null ]
      ] ],
      [ "Digikam::AlbumManager", "classDigikam_1_1AlbumManager.html", null ],
      [ "Digikam::AlbumSelectComboBox", null, [
        [ "Digikam::AbstractAlbumTreeViewSelectComboBox", "classDigikam_1_1AbstractAlbumTreeViewSelectComboBox.html", [
          [ "Digikam::AlbumTreeViewSelectComboBox", "classDigikam_1_1AlbumTreeViewSelectComboBox.html", null ],
          [ "Digikam::TagTreeViewSelectComboBox", "classDigikam_1_1TagTreeViewSelectComboBox.html", null ]
        ] ]
      ] ],
      [ "Digikam::ApplicationSettings", "classDigikam_1_1ApplicationSettings.html", null ],
      [ "Digikam::AssignNameWidget", "classDigikam_1_1AssignNameWidget.html", null ],
      [ "Digikam::BatchTool", null, [
        [ "Digikam::Restoration", "classDigikam_1_1Restoration.html", null ],
        [ "DigikamBqmAntiVignettingPlugin::AntiVignetting", "classDigikamBqmAntiVignettingPlugin_1_1AntiVignetting.html", null ],
        [ "DigikamBqmAssignTemplatePlugin::AssignTemplate", "classDigikamBqmAssignTemplatePlugin_1_1AssignTemplate.html", null ],
        [ "DigikamBqmAutoCorrectionPlugin::AutoCorrection", "classDigikamBqmAutoCorrectionPlugin_1_1AutoCorrection.html", null ],
        [ "DigikamBqmBCGCorrectionPlugin::BCGCorrection", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrection.html", null ],
        [ "DigikamBqmBlurPlugin::Blur", "classDigikamBqmBlurPlugin_1_1Blur.html", null ],
        [ "DigikamBqmBorderPlugin::Border", "classDigikamBqmBorderPlugin_1_1Border.html", null ],
        [ "DigikamBqmBWConvertPlugin::BWConvert", "classDigikamBqmBWConvertPlugin_1_1BWConvert.html", null ],
        [ "DigikamBqmChannelMixerPlugin::ChannelMixer", "classDigikamBqmChannelMixerPlugin_1_1ChannelMixer.html", null ],
        [ "DigikamBqmColorBalancePlugin::ColorBalance", "classDigikamBqmColorBalancePlugin_1_1ColorBalance.html", null ],
        [ "DigikamBqmColorFXPlugin::ColorFX", "classDigikamBqmColorFXPlugin_1_1ColorFX.html", null ],
        [ "DigikamBqmConvert16To8Plugin::Convert16to8", "classDigikamBqmConvert16To8Plugin_1_1Convert16to8.html", null ],
        [ "DigikamBqmConvert8To16Plugin::Convert8to16", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html", null ],
        [ "DigikamBqmConvertToDngPlugin::ConvertToDNG", "classDigikamBqmConvertToDngPlugin_1_1ConvertToDNG.html", null ],
        [ "DigikamBqmConvertToHeifPlugin::ConvertToHEIF", "classDigikamBqmConvertToHeifPlugin_1_1ConvertToHEIF.html", null ],
        [ "DigikamBqmConvertToJp2Plugin::ConvertToJP2", "classDigikamBqmConvertToJp2Plugin_1_1ConvertToJP2.html", null ],
        [ "DigikamBqmConvertToJpegPlugin::ConvertToJPEG", "classDigikamBqmConvertToJpegPlugin_1_1ConvertToJPEG.html", null ],
        [ "DigikamBqmConvertToPgfPlugin::ConvertToPGF", "classDigikamBqmConvertToPgfPlugin_1_1ConvertToPGF.html", null ],
        [ "DigikamBqmConvertToPngPlugin::ConvertToPNG", "classDigikamBqmConvertToPngPlugin_1_1ConvertToPNG.html", null ],
        [ "DigikamBqmConvertToTiffPlugin::ConvertToTIFF", "classDigikamBqmConvertToTiffPlugin_1_1ConvertToTIFF.html", null ],
        [ "DigikamBqmCurvesAdjustPlugin::CurvesAdjust", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html", null ],
        [ "DigikamBqmFilmGrainPlugin::FilmGrain", "classDigikamBqmFilmGrainPlugin_1_1FilmGrain.html", null ],
        [ "DigikamBqmFlipPlugin::Flip", "classDigikamBqmFlipPlugin_1_1Flip.html", null ],
        [ "DigikamBqmHSLCorrectionPlugin::HSLCorrection", "classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrection.html", null ],
        [ "DigikamBqmIccConvertPlugin::IccConvert", "classDigikamBqmIccConvertPlugin_1_1IccConvert.html", null ],
        [ "DigikamBqmInvertPlugin::Invert", "classDigikamBqmInvertPlugin_1_1Invert.html", null ],
        [ "DigikamBqmLocalContrastPlugin::LocalContrast", "classDigikamBqmLocalContrastPlugin_1_1LocalContrast.html", null ],
        [ "DigikamBqmNoiseReductionPlugin::NoiseReduction", "classDigikamBqmNoiseReductionPlugin_1_1NoiseReduction.html", null ],
        [ "DigikamBqmRemoveMetadataPlugin::RemoveMetadata", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html", null ],
        [ "DigikamBqmRestorationPlugin::RedEyeCorrection", "classDigikamBqmRestorationPlugin_1_1RedEyeCorrection.html", null ],
        [ "DigikamBqmSharpenPlugin::Sharpen", "classDigikamBqmSharpenPlugin_1_1Sharpen.html", null ],
        [ "DigikamBqmTimeAdjustPlugin::TimeAdjust", "classDigikamBqmTimeAdjustPlugin_1_1TimeAdjust.html", null ],
        [ "DigikamBqmWhiteBalancePlugin::WhiteBalance", "classDigikamBqmWhiteBalancePlugin_1_1WhiteBalance.html", null ]
      ] ],
      [ "Digikam::CollectionManager", "classDigikam_1_1CollectionManager.html", null ],
      [ "Digikam::CollectionScanner", "classDigikam_1_1CollectionScanner.html", null ],
      [ "Digikam::ColorLabelWidget", null, [
        [ "Digikam::ColorLabelFilter", "classDigikam_1_1ColorLabelFilter.html", null ]
      ] ],
      [ "Digikam::DAdjustableLabel", null, [
        [ "Digikam::DSqueezedClickLabel", "classDigikam_1_1DSqueezedClickLabel.html", null ],
        [ "Digikam::DTextLabelValue", "classDigikam_1_1DTextLabelValue.html", null ]
      ] ],
      [ "Digikam::DCategorizedSortFilterProxyModel", "classDigikam_1_1DCategorizedSortFilterProxyModel.html", [
        [ "Digikam::ActionSortFilterProxyModel", "classDigikam_1_1ActionSortFilterProxyModel.html", null ],
        [ "Digikam::ImageSortFilterModel", "classDigikam_1_1ImageSortFilterModel.html", [
          [ "Digikam::ItemFilterModel", "classDigikam_1_1ItemFilterModel.html", null ],
          [ "Digikam::NoDuplicatesItemFilterModel", "classDigikam_1_1NoDuplicatesItemFilterModel.html", null ]
        ] ],
        [ "Digikam::ImportSortFilterModel", "classDigikam_1_1ImportSortFilterModel.html", [
          [ "Digikam::NoDuplicatesImportFilterModel", "classDigikam_1_1NoDuplicatesImportFilterModel.html", null ]
        ] ],
        [ "ShowFoto::ShowfotoSortFilterModel", "classShowFoto_1_1ShowfotoSortFilterModel.html", [
          [ "ShowFoto::NoDuplicatesShowfotoFilterModel", "classShowFoto_1_1NoDuplicatesShowfotoFilterModel.html", null ]
        ] ]
      ] ],
      [ "Digikam::DCategorizedView", "classDigikam_1_1DCategorizedView.html", [
        [ "Digikam::ActionCategorizedView", "classDigikam_1_1ActionCategorizedView.html", null ],
        [ "Digikam::ItemViewCategorized", null, null ]
      ] ],
      [ "Digikam::DConfigDlgTitle", "classDigikam_1_1DConfigDlgTitle.html", null ],
      [ "Digikam::DDatePicker", "classDigikam_1_1DDatePicker.html", null ],
      [ "Digikam::DExpanderBox", null, [
        [ "Digikam::DExpanderBoxExclusive", "classDigikam_1_1DExpanderBoxExclusive.html", null ]
      ] ],
      [ "Digikam::DigikamApp", "classDigikam_1_1DigikamApp.html", null ],
      [ "Digikam::DImgChildItem", null, [
        [ "Digikam::RegionFrameItem", null, [
          [ "Digikam::FaceItem", "classDigikam_1_1FaceItem.html", null ]
        ] ]
      ] ],
      [ "Digikam::DItemDelegate", null, [
        [ "Digikam::ItemViewDelegate", "classDigikam_1_1ItemViewDelegate.html", null ],
        [ "Digikam::ItemViewImportDelegate", "classDigikam_1_1ItemViewImportDelegate.html", null ],
        [ "ShowFoto::ItemViewShowfotoDelegate", "classShowFoto_1_1ItemViewShowfotoDelegate.html", null ]
      ] ],
      [ "Digikam::DItemsList", null, [
        [ "DigikamGenericImgUrPlugin::ImgurImagesList", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html", null ],
        [ "DigikamGenericIpfsPlugin::IpfsImagesList", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimeAdjustList", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList.html", null ]
      ] ],
      [ "Digikam::DItemsListViewItem", null, [
        [ "DigikamGenericImgUrPlugin::ImgurImageListViewItem", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem.html", null ],
        [ "DigikamGenericIpfsPlugin::IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html", null ]
      ] ],
      [ "Digikam::DLNAMediaServerDelegate", null, null ],
      [ "Digikam::DMultiTabBar", null, [
        [ "Digikam::Sidebar", null, [
          [ "Digikam::ItemPropertiesSideBar", "classDigikam_1_1ItemPropertiesSideBar.html", null ]
        ] ]
      ] ],
      [ "Digikam::DNGWriter", "classDigikam_1_1DNGWriter.html", null ],
      [ "Digikam::DPlugin", null, [
        [ "Digikam::DPluginBqm", null, [
          [ "Digikam::RestorationPlugin", "classDigikam_1_1RestorationPlugin.html", null ],
          [ "DigikamBqmAntiVignettingPlugin::AntiVignettingPlugin", "classDigikamBqmAntiVignettingPlugin_1_1AntiVignettingPlugin.html", null ],
          [ "DigikamBqmAssignTemplatePlugin::AssignTemplatePlugin", "classDigikamBqmAssignTemplatePlugin_1_1AssignTemplatePlugin.html", null ],
          [ "DigikamBqmAutoCorrectionPlugin::AutoCorrectionPlugin", "classDigikamBqmAutoCorrectionPlugin_1_1AutoCorrectionPlugin.html", null ],
          [ "DigikamBqmBCGCorrectionPlugin::BCGCorrectionPlugin", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html", null ],
          [ "DigikamBqmBlurPlugin::BlurPlugin", "classDigikamBqmBlurPlugin_1_1BlurPlugin.html", null ],
          [ "DigikamBqmBorderPlugin::BorderPlugin", "classDigikamBqmBorderPlugin_1_1BorderPlugin.html", null ],
          [ "DigikamBqmBWConvertPlugin::BWConvertPlugin", "classDigikamBqmBWConvertPlugin_1_1BWConvertPlugin.html", null ],
          [ "DigikamBqmChannelMixerPlugin::ChannelMixerPlugin", "classDigikamBqmChannelMixerPlugin_1_1ChannelMixerPlugin.html", null ],
          [ "DigikamBqmColorBalancePlugin::ColorBalancePlugin", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html", null ],
          [ "DigikamBqmColorFXPlugin::ColorFXPlugin", "classDigikamBqmColorFXPlugin_1_1ColorFXPlugin.html", null ],
          [ "DigikamBqmConvert16To8Plugin::Convert16To8Plugin", "classDigikamBqmConvert16To8Plugin_1_1Convert16To8Plugin.html", null ],
          [ "DigikamBqmConvert8To16Plugin::Convert8To16Plugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html", null ],
          [ "DigikamBqmConvertToDngPlugin::ConvertToDngPlugin", "classDigikamBqmConvertToDngPlugin_1_1ConvertToDngPlugin.html", null ],
          [ "DigikamBqmConvertToHeifPlugin::ConvertToHeifPlugin", "classDigikamBqmConvertToHeifPlugin_1_1ConvertToHeifPlugin.html", null ],
          [ "DigikamBqmConvertToJp2Plugin::ConvertToJp2Plugin", "classDigikamBqmConvertToJp2Plugin_1_1ConvertToJp2Plugin.html", null ],
          [ "DigikamBqmConvertToJpegPlugin::ConvertToJpegPlugin", "classDigikamBqmConvertToJpegPlugin_1_1ConvertToJpegPlugin.html", null ],
          [ "DigikamBqmConvertToPgfPlugin::ConvertToPgfPlugin", "classDigikamBqmConvertToPgfPlugin_1_1ConvertToPgfPlugin.html", null ],
          [ "DigikamBqmConvertToPngPlugin::ConvertToPngPlugin", "classDigikamBqmConvertToPngPlugin_1_1ConvertToPngPlugin.html", null ],
          [ "DigikamBqmConvertToTiffPlugin::ConvertToTiffPlugin", "classDigikamBqmConvertToTiffPlugin_1_1ConvertToTiffPlugin.html", null ],
          [ "DigikamBqmCropPlugin::CropPlugin", "classDigikamBqmCropPlugin_1_1CropPlugin.html", null ],
          [ "DigikamBqmCurvesAdjustPlugin::CurvesAdjustPlugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html", null ],
          [ "DigikamBqmFilmGrainPlugin::FilmGrainPlugin", "classDigikamBqmFilmGrainPlugin_1_1FilmGrainPlugin.html", null ],
          [ "DigikamBqmFlipPlugin::FlipPlugin", "classDigikamBqmFlipPlugin_1_1FlipPlugin.html", null ],
          [ "DigikamBqmHSLCorrectionPlugin::HSLCorrectionPlugin", "classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrectionPlugin.html", null ],
          [ "DigikamBqmIccConvertPlugin::IccConvertPlugin", "classDigikamBqmIccConvertPlugin_1_1IccConvertPlugin.html", null ],
          [ "DigikamBqmInvertPlugin::InvertPlugin", "classDigikamBqmInvertPlugin_1_1InvertPlugin.html", null ],
          [ "DigikamBqmLensAutoFixPlugin::LensAutoFixPlugin", "classDigikamBqmLensAutoFixPlugin_1_1LensAutoFixPlugin.html", null ],
          [ "DigikamBqmLocalContrastPlugin::LocalContrastPlugin", "classDigikamBqmLocalContrastPlugin_1_1LocalContrastPlugin.html", null ],
          [ "DigikamBqmNoiseReductionPlugin::NoiseReductionPlugin", "classDigikamBqmNoiseReductionPlugin_1_1NoiseReductionPlugin.html", null ],
          [ "DigikamBqmRemoveMetadataPlugin::RemoveMetadataPlugin", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadataPlugin.html", null ],
          [ "DigikamBqmResizePlugin::ResizePlugin", "classDigikamBqmResizePlugin_1_1ResizePlugin.html", null ],
          [ "DigikamBqmRestorationPlugin::RedEyeCorrectionPlugin", "classDigikamBqmRestorationPlugin_1_1RedEyeCorrectionPlugin.html", null ],
          [ "DigikamBqmRotatePlugin::RotatePlugin", "classDigikamBqmRotatePlugin_1_1RotatePlugin.html", null ],
          [ "DigikamBqmSharpenPlugin::SharpenPlugin", "classDigikamBqmSharpenPlugin_1_1SharpenPlugin.html", null ],
          [ "DigikamBqmTimeAdjustPlugin::TimeAdjustPlugin", "classDigikamBqmTimeAdjustPlugin_1_1TimeAdjustPlugin.html", null ],
          [ "DigikamBqmUserScriptPlugin::UserScriptPlugin", "classDigikamBqmUserScriptPlugin_1_1UserScriptPlugin.html", null ],
          [ "DigikamBqmWatermarkPlugin::WaterMarkPlugin", "classDigikamBqmWatermarkPlugin_1_1WaterMarkPlugin.html", null ],
          [ "DigikamBqmWhiteBalancePlugin::WhiteBalancePlugin", "classDigikamBqmWhiteBalancePlugin_1_1WhiteBalancePlugin.html", null ]
        ] ],
        [ "Digikam::DPluginDImg", "classDigikam_1_1DPluginDImg.html", [
          [ "DigikamHEIFDImgPlugin::DImgHEIFPlugin", "classDigikamHEIFDImgPlugin_1_1DImgHEIFPlugin.html", null ],
          [ "DigikamImageMagickDImgPlugin::DImgImageMagickPlugin", "classDigikamImageMagickDImgPlugin_1_1DImgImageMagickPlugin.html", null ],
          [ "DigikamJPEG2000DImgPlugin::DImgJPEG2000Plugin", "classDigikamJPEG2000DImgPlugin_1_1DImgJPEG2000Plugin.html", null ],
          [ "DigikamJPEGDImgPlugin::DImgJPEGPlugin", "classDigikamJPEGDImgPlugin_1_1DImgJPEGPlugin.html", null ],
          [ "DigikamPGFDImgPlugin::DImgPGFPlugin", "classDigikamPGFDImgPlugin_1_1DImgPGFPlugin.html", null ],
          [ "DigikamPNGDImgPlugin::DImgPNGPlugin", "classDigikamPNGDImgPlugin_1_1DImgPNGPlugin.html", null ],
          [ "DigikamQImageDImgPlugin::DImgQImagePlugin", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html", null ],
          [ "DigikamRAWDImgPlugin::DImgRAWPlugin", "classDigikamRAWDImgPlugin_1_1DImgRAWPlugin.html", null ],
          [ "DigikamTIFFDImgPlugin::DImgTIFFPlugin", "classDigikamTIFFDImgPlugin_1_1DImgTIFFPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginEditor", null, [
          [ "DigikamEditorAdjustCurvesToolPlugin::AdjustCurvesToolPlugin", "classDigikamEditorAdjustCurvesToolPlugin_1_1AdjustCurvesToolPlugin.html", null ],
          [ "DigikamEditorAdjustLevelsToolPlugin::AdjustLevelsToolPlugin", "classDigikamEditorAdjustLevelsToolPlugin_1_1AdjustLevelsToolPlugin.html", null ],
          [ "DigikamEditorAntivignettingToolPlugin::AntiVignettingToolPlugin", "classDigikamEditorAntivignettingToolPlugin_1_1AntiVignettingToolPlugin.html", null ],
          [ "DigikamEditorAutoCorrectionToolPlugin::AutoCorrectionToolPlugin", "classDigikamEditorAutoCorrectionToolPlugin_1_1AutoCorrectionToolPlugin.html", null ],
          [ "DigikamEditorAutoCropToolPlugin::AutoCropToolPlugin", "classDigikamEditorAutoCropToolPlugin_1_1AutoCropToolPlugin.html", null ],
          [ "DigikamEditorBCGToolPlugin::BCGToolPlugin", "classDigikamEditorBCGToolPlugin_1_1BCGToolPlugin.html", null ],
          [ "DigikamEditorBlurFxToolPlugin::BlurFXToolPlugin", "classDigikamEditorBlurFxToolPlugin_1_1BlurFXToolPlugin.html", null ],
          [ "DigikamEditorBlurToolPlugin::BlurToolPlugin", "classDigikamEditorBlurToolPlugin_1_1BlurToolPlugin.html", null ],
          [ "DigikamEditorBorderToolPlugin::BorderToolPlugin", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html", null ],
          [ "DigikamEditorBWSepiaToolPlugin::BWSepiaToolPlugin", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html", null ],
          [ "DigikamEditorChannelMixerToolPlugin::ChannelMixerToolPlugin", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerToolPlugin.html", null ],
          [ "DigikamEditorCharcoalToolPlugin::CharcoalToolPlugin", "classDigikamEditorCharcoalToolPlugin_1_1CharcoalToolPlugin.html", null ],
          [ "DigikamEditorColorBalanceToolPlugin::CBToolPlugin", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html", null ],
          [ "DigikamEditorColorFxToolPlugin::ColorFXToolPlugin", "classDigikamEditorColorFxToolPlugin_1_1ColorFXToolPlugin.html", null ],
          [ "DigikamEditorContentAwareResizeToolPlugin::ContentAwareResizeToolPlugin", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeToolPlugin.html", null ],
          [ "DigikamEditorConvert16To8ToolPlugin::Convert16To8ToolPlugin", "classDigikamEditorConvert16To8ToolPlugin_1_1Convert16To8ToolPlugin.html", null ],
          [ "DigikamEditorConvert8To16ToolPlugin::Convert8To16ToolPlugin", "classDigikamEditorConvert8To16ToolPlugin_1_1Convert8To16ToolPlugin.html", null ],
          [ "DigikamEditorDistortionFxToolPlugin::DistortionFXToolPlugin", "classDigikamEditorDistortionFxToolPlugin_1_1DistortionFXToolPlugin.html", null ],
          [ "DigikamEditorEmbossToolPlugin::EmbossToolPlugin", "classDigikamEditorEmbossToolPlugin_1_1EmbossToolPlugin.html", null ],
          [ "DigikamEditorFilmGrainToolPlugin::FilmGrainToolPlugin", "classDigikamEditorFilmGrainToolPlugin_1_1FilmGrainToolPlugin.html", null ],
          [ "DigikamEditorFilmToolPlugin::FilmToolPlugin", "classDigikamEditorFilmToolPlugin_1_1FilmToolPlugin.html", null ],
          [ "DigikamEditorFreeRotationToolPlugin::FreeRotationToolPlugin", "classDigikamEditorFreeRotationToolPlugin_1_1FreeRotationToolPlugin.html", null ],
          [ "DigikamEditorHealingCloneToolPlugin::HealingCloneToolPlugin", "classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolPlugin.html", null ],
          [ "DigikamEditorHotPixelsToolPlugin::HotPixelsToolPlugin", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin.html", null ],
          [ "DigikamEditorHSLToolPlugin::HSLToolPlugin", "classDigikamEditorHSLToolPlugin_1_1HSLToolPlugin.html", null ],
          [ "DigikamEditorInsertTextToolPlugin::InsertTextToolPlugin", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextToolPlugin.html", null ],
          [ "DigikamEditorInvertToolPlugin::InvertToolPlugin", "classDigikamEditorInvertToolPlugin_1_1InvertToolPlugin.html", null ],
          [ "DigikamEditorLensAutoFixToolPlugin::LensAutoFixToolPlugin", "classDigikamEditorLensAutoFixToolPlugin_1_1LensAutoFixToolPlugin.html", null ],
          [ "DigikamEditorLensDistortionToolPlugin::LensDistortionToolPlugin", "classDigikamEditorLensDistortionToolPlugin_1_1LensDistortionToolPlugin.html", null ],
          [ "DigikamEditorLocalContrastToolPlugin::LocalContrastToolPlugin", "classDigikamEditorLocalContrastToolPlugin_1_1LocalContrastToolPlugin.html", null ],
          [ "DigikamEditorNoiseReductionToolPlugin::LocalContrastToolPlugin", "classDigikamEditorNoiseReductionToolPlugin_1_1LocalContrastToolPlugin.html", null ],
          [ "DigikamEditorOilPaintToolPlugin::OilPaintToolPlugin", "classDigikamEditorOilPaintToolPlugin_1_1OilPaintToolPlugin.html", null ],
          [ "DigikamEditorPerspectiveToolPlugin::PerspectiveToolPlugin", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveToolPlugin.html", null ],
          [ "DigikamEditorPrintToolPlugin::PrintToolPlugin", "classDigikamEditorPrintToolPlugin_1_1PrintToolPlugin.html", null ],
          [ "DigikamEditorProfileConversionToolPlugin::ProfileConversionToolPlugin", "classDigikamEditorProfileConversionToolPlugin_1_1ProfileConversionToolPlugin.html", null ],
          [ "DigikamEditorRainDropToolPlugin::RainDropToolPlugin", "classDigikamEditorRainDropToolPlugin_1_1RainDropToolPlugin.html", null ],
          [ "DigikamEditorRatioCropToolPlugin::RatioCropToolPlugin", "classDigikamEditorRatioCropToolPlugin_1_1RatioCropToolPlugin.html", null ],
          [ "DigikamEditorRedEyeToolPlugin::RedEyeToolPlugin", "classDigikamEditorRedEyeToolPlugin_1_1RedEyeToolPlugin.html", null ],
          [ "DigikamEditorResizeToolPlugin::ResizeToolPlugin", "classDigikamEditorResizeToolPlugin_1_1ResizeToolPlugin.html", null ],
          [ "DigikamEditorRestorationToolPlugin::RestoreToolPlugin", "classDigikamEditorRestorationToolPlugin_1_1RestoreToolPlugin.html", null ],
          [ "DigikamEditorSharpenToolPlugin::SharpenToolPlugin", "classDigikamEditorSharpenToolPlugin_1_1SharpenToolPlugin.html", null ],
          [ "DigikamEditorShearToolPlugin::ShearToolPlugin", "classDigikamEditorShearToolPlugin_1_1ShearToolPlugin.html", null ],
          [ "DigikamEditorTextureToolPlugin::TextureToolPlugin", "classDigikamEditorTextureToolPlugin_1_1TextureToolPlugin.html", null ],
          [ "DigikamEditorWhiteBalanceToolPlugin::WhiteBalanceToolPlugin", "classDigikamEditorWhiteBalanceToolPlugin_1_1WhiteBalanceToolPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginGeneric", null, [
          [ "DigikamGenericBoxPlugin::BoxPlugin", "classDigikamGenericBoxPlugin_1_1BoxPlugin.html", null ],
          [ "DigikamGenericCalendarPlugin::CalendarPlugin", "classDigikamGenericCalendarPlugin_1_1CalendarPlugin.html", null ],
          [ "DigikamGenericDebianScreenshotsPlugin::DSPlugin", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPlugin.html", null ],
          [ "DigikamGenericDropBoxPlugin::DBPlugin", "classDigikamGenericDropBoxPlugin_1_1DBPlugin.html", null ],
          [ "DigikamGenericDScannerPlugin::DigitalScannerPlugin", "classDigikamGenericDScannerPlugin_1_1DigitalScannerPlugin.html", null ],
          [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingPlugin", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingPlugin.html", null ],
          [ "DigikamGenericFaceBookPlugin::FbPlugin", "classDigikamGenericFaceBookPlugin_1_1FbPlugin.html", null ],
          [ "DigikamGenericFileCopyPlugin::FCPlugin", "classDigikamGenericFileCopyPlugin_1_1FCPlugin.html", null ],
          [ "DigikamGenericFileTransferPlugin::FTPlugin", "classDigikamGenericFileTransferPlugin_1_1FTPlugin.html", null ],
          [ "DigikamGenericFlickrPlugin::FlickrPlugin", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin.html", null ],
          [ "DigikamGenericGeolocationEditPlugin::GeolocationEditPlugin", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEditPlugin.html", null ],
          [ "DigikamGenericGLViewerPlugin::GLViewerPlugin", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin.html", null ],
          [ "DigikamGenericGoogleServicesPlugin::GSPlugin", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin.html", null ],
          [ "DigikamGenericHtmlGalleryPlugin::HtmlGalleryPlugin", "classDigikamGenericHtmlGalleryPlugin_1_1HtmlGalleryPlugin.html", null ],
          [ "DigikamGenericImageShackPlugin::ImageShackPlugin", "classDigikamGenericImageShackPlugin_1_1ImageShackPlugin.html", null ],
          [ "DigikamGenericImgUrPlugin::ImgUrPlugin", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin.html", null ],
          [ "DigikamGenericIpfsPlugin::IpfsPlugin", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin.html", null ],
          [ "DigikamGenericJAlbumPlugin::JAlbumPlugin", "classDigikamGenericJAlbumPlugin_1_1JAlbumPlugin.html", null ],
          [ "DigikamGenericMediaServerPlugin::MediaServerPlugin", "classDigikamGenericMediaServerPlugin_1_1MediaServerPlugin.html", null ],
          [ "DigikamGenericMediaWikiPlugin::MediaWikiPlugin", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiPlugin.html", null ],
          [ "DigikamGenericMetadataEditPlugin::MetadataEditPlugin", "classDigikamGenericMetadataEditPlugin_1_1MetadataEditPlugin.html", null ],
          [ "DigikamGenericOneDrivePlugin::ODPlugin", "classDigikamGenericOneDrivePlugin_1_1ODPlugin.html", null ],
          [ "DigikamGenericPanoramaPlugin::PanoramaPlugin", "classDigikamGenericPanoramaPlugin_1_1PanoramaPlugin.html", null ],
          [ "DigikamGenericPinterestPlugin::PPlugin", "classDigikamGenericPinterestPlugin_1_1PPlugin.html", null ],
          [ "DigikamGenericPiwigoPlugin::PiwigoPlugin", "classDigikamGenericPiwigoPlugin_1_1PiwigoPlugin.html", null ],
          [ "DigikamGenericPresentationPlugin::PresentationPlugin", "classDigikamGenericPresentationPlugin_1_1PresentationPlugin.html", null ],
          [ "DigikamGenericPrintCreatorPlugin::PrintCreatorPlugin", "classDigikamGenericPrintCreatorPlugin_1_1PrintCreatorPlugin.html", null ],
          [ "DigikamGenericRajcePlugin::RajcePlugin", "classDigikamGenericRajcePlugin_1_1RajcePlugin.html", null ],
          [ "DigikamGenericSendByMailPlugin::SendByMailPlugin", "classDigikamGenericSendByMailPlugin_1_1SendByMailPlugin.html", null ],
          [ "DigikamGenericSlideShowPlugin::SlideShowPlugin", "classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html", null ],
          [ "DigikamGenericSmugPlugin::SmugPlugin", "classDigikamGenericSmugPlugin_1_1SmugPlugin.html", null ],
          [ "DigikamGenericTimeAdjustPlugin::TimeAdjustPlugin", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustPlugin.html", null ],
          [ "DigikamGenericTwitterPlugin::TwitterPlugin", "classDigikamGenericTwitterPlugin_1_1TwitterPlugin.html", null ],
          [ "DigikamGenericUnifiedPlugin::UnifiedPlugin", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin.html", null ],
          [ "DigikamGenericVideoSlideShowPlugin::VideoSlideShowPlugin", "classDigikamGenericVideoSlideShowPlugin_1_1VideoSlideShowPlugin.html", null ],
          [ "DigikamGenericVKontaktePlugin::VKontaktePlugin", "classDigikamGenericVKontaktePlugin_1_1VKontaktePlugin.html", null ],
          [ "DigikamGenericWallpaperPlugin::WallpaperPlugin", "classDigikamGenericWallpaperPlugin_1_1WallpaperPlugin.html", null ],
          [ "DigikamGenericYFPlugin::YFPlugin", "classDigikamGenericYFPlugin_1_1YFPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginRawImport", "classDigikam_1_1DPluginRawImport.html", [
          [ "DigikamRawImportNativePlugin::RawImportNativePlugin", "classDigikamRawImportNativePlugin_1_1RawImportNativePlugin.html", null ]
        ] ]
      ] ],
      [ "Digikam::DPluginBqm", null, null ],
      [ "Digikam::DPluginConfView", null, [
        [ "Digikam::DPluginConfViewBqm", "classDigikam_1_1DPluginConfViewBqm.html", null ],
        [ "Digikam::DPluginConfViewDImg", "classDigikam_1_1DPluginConfViewDImg.html", null ],
        [ "Digikam::DPluginConfViewEditor", "classDigikam_1_1DPluginConfViewEditor.html", null ],
        [ "Digikam::DPluginConfViewGeneric", "classDigikam_1_1DPluginConfViewGeneric.html", null ]
      ] ],
      [ "Digikam::DPluginEditor", null, null ],
      [ "Digikam::DPluginGeneric", null, null ],
      [ "Digikam::DPluginLoader", "classDigikam_1_1DPluginLoader.html", null ],
      [ "Digikam::DRawDecoder", "classDigikam_1_1DRawDecoder.html", [
        [ "DigikamRAWDImgPlugin::DImgRAWLoader", "classDigikamRAWDImgPlugin_1_1DImgRAWLoader.html", null ]
      ] ],
      [ "Digikam::DWizardPage", null, [
        [ "Digikam::StartScanPage", "classDigikam_1_1StartScanPage.html", null ],
        [ "Digikam::WelcomePage", "classDigikam_1_1WelcomePage.html", null ],
        [ "DigikamGenericCalendarPlugin::CalIntroPage", "classDigikamGenericCalendarPlugin_1_1CalIntroPage.html", null ]
      ] ],
      [ "Digikam::DXmlGuiWindow", null, null ],
      [ "Digikam::EditorCore", "classDigikam_1_1EditorCore.html", null ],
      [ "Digikam::EditorWindow", "classDigikam_1_1EditorWindow.html", null ],
      [ "Digikam::EffectMngr", "classDigikam_1_1EffectMngr.html", null ],
      [ "Digikam::FaceDb", "classDigikam_1_1FaceDb.html", null ],
      [ "Digikam::FaceGroup", "classDigikam_1_1FaceGroup.html", null ],
      [ "Digikam::FaceScanDialog", "classDigikam_1_1FaceScanDialog.html", null ],
      [ "Digikam::FilmContainer", "classDigikam_1_1FilmContainer.html", null ],
      [ "Digikam::FilmFilter", "classDigikam_1_1FilmFilter.html", null ],
      [ "Digikam::GraphicsDImgView", null, [
        [ "Digikam::ItemPreviewView", null, [
          [ "Digikam::LightTablePreview", "classDigikam_1_1LightTablePreview.html", null ]
        ] ]
      ] ],
      [ "Digikam::HaarIface", "classDigikam_1_1HaarIface.html", null ],
      [ "Digikam::IccManager", null, [
        [ "Digikam::IccPostLoadingManager", "classDigikam_1_1IccPostLoadingManager.html", null ]
      ] ],
      [ "Digikam::ImageQualityParser", "classDigikam_1_1ImageQualityParser.html", null ],
      [ "Digikam::ImageWindow", "classDigikam_1_1ImageWindow.html", null ],
      [ "Digikam::ImportCategorizedView", null, null ],
      [ "Digikam::ImportUI", "classDigikam_1_1ImportUI.html", null ],
      [ "Digikam::InfoDlg", null, [
        [ "Digikam::DBStatDlg", "classDigikam_1_1DBStatDlg.html", null ],
        [ "Digikam::LibsInfoDlg", "classDigikam_1_1LibsInfoDlg.html", null ]
      ] ],
      [ "Digikam::ItemCategorizedView", null, null ],
      [ "Digikam::ItemLister", "classDigikam_1_1ItemLister.html", null ],
      [ "Digikam::ItemModel", null, null ],
      [ "Digikam::ItemPreviewView", null, null ],
      [ "Digikam::ItemScanner", "classDigikam_1_1ItemScanner.html", null ],
      [ "Digikam::ItemThumbnailModel", null, null ],
      [ "Digikam::ItemViewCategorized", null, null ],
      [ "Digikam::ItemVisibilityController", null, [
        [ "Digikam::HidingStateChanger", "classDigikam_1_1HidingStateChanger.html", [
          [ "Digikam::AssignNameWidgetStates", "classDigikam_1_1AssignNameWidgetStates.html", null ]
        ] ]
      ] ],
      [ "Digikam::LightTableWindow", "classDigikam_1_1LightTableWindow.html", null ],
      [ "Digikam::LoadSaveThread", null, null ],
      [ "Digikam::MetadataWidget", null, [
        [ "Digikam::ExifWidget", "classDigikam_1_1ExifWidget.html", null ],
        [ "Digikam::IptcWidget", "classDigikam_1_1IptcWidget.html", null ],
        [ "Digikam::MakerNoteWidget", "classDigikam_1_1MakerNoteWidget.html", null ],
        [ "Digikam::XmpWidget", "classDigikam_1_1XmpWidget.html", null ]
      ] ],
      [ "Digikam::MetaEngine", "classDigikam_1_1MetaEngine.html", [
        [ "Digikam::DMetadata", "classDigikam_1_1DMetadata.html", null ]
      ] ],
      [ "Digikam::Modifier", null, [
        [ "Digikam::CaseModifier", "classDigikam_1_1CaseModifier.html", null ],
        [ "Digikam::DefaultValueModifier", "classDigikam_1_1DefaultValueModifier.html", null ],
        [ "Digikam::RangeModifier", "classDigikam_1_1RangeModifier.html", null ],
        [ "Digikam::RemoveDoublesModifier", "classDigikam_1_1RemoveDoublesModifier.html", null ],
        [ "Digikam::ReplaceModifier", "classDigikam_1_1ReplaceModifier.html", null ],
        [ "Digikam::TrimmedModifier", "classDigikam_1_1TrimmedModifier.html", null ],
        [ "Digikam::UniqueModifier", "classDigikam_1_1UniqueModifier.html", null ]
      ] ],
      [ "Digikam::OpenCVFaceDetector", "classDigikam_1_1OpenCVFaceDetector.html", null ],
      [ "Digikam::Option", null, [
        [ "Digikam::CameraNameOption", "classDigikam_1_1CameraNameOption.html", null ],
        [ "Digikam::DatabaseOption", "classDigikam_1_1DatabaseOption.html", null ],
        [ "Digikam::DateOption", "classDigikam_1_1DateOption.html", null ],
        [ "Digikam::DirectoryNameOption", "classDigikam_1_1DirectoryNameOption.html", null ],
        [ "Digikam::FilePropertiesOption", "classDigikam_1_1FilePropertiesOption.html", null ],
        [ "Digikam::MetadataOption", "classDigikam_1_1MetadataOption.html", null ],
        [ "Digikam::SequenceNumberOption", "classDigikam_1_1SequenceNumberOption.html", null ]
      ] ],
      [ "Digikam::Parser", null, [
        [ "Digikam::DefaultRenameParser", "classDigikam_1_1DefaultRenameParser.html", null ],
        [ "Digikam::ImportRenameParser", "classDigikam_1_1ImportRenameParser.html", null ]
      ] ],
      [ "Digikam::PickLabelWidget", null, [
        [ "Digikam::PickLabelFilter", "classDigikam_1_1PickLabelFilter.html", null ]
      ] ],
      [ "Digikam::ProgressItem", null, [
        [ "Digikam::FileActionProgress", "classDigikam_1_1FileActionProgress.html", null ]
      ] ],
      [ "Digikam::QueueMgrWindow", "classDigikam_1_1QueueMgrWindow.html", null ],
      [ "Digikam::RatingWidget", null, [
        [ "Digikam::RatingComboBoxWidget", "classDigikam_1_1RatingComboBoxWidget.html", null ]
      ] ],
      [ "Digikam::RecognitionDatabase", "classDigikam_1_1RecognitionDatabase.html", null ],
      [ "Digikam::RegionFrameItem", null, null ],
      [ "Digikam::Rule", null, [
        [ "Digikam::Modifier", null, null ],
        [ "Digikam::Option", null, null ]
      ] ],
      [ "Digikam::RuleDialog", null, [
        [ "Digikam::DatabaseOptionDialog", "classDigikam_1_1DatabaseOptionDialog.html", null ],
        [ "Digikam::DateOptionDialog", "classDigikam_1_1DateOptionDialog.html", null ],
        [ "Digikam::DefaultValueDialog", "classDigikam_1_1DefaultValueDialog.html", null ],
        [ "Digikam::MetadataOptionDialog", "classDigikam_1_1MetadataOptionDialog.html", null ],
        [ "Digikam::RangeDialog", "classDigikam_1_1RangeDialog.html", null ],
        [ "Digikam::ReplaceDialog", "classDigikam_1_1ReplaceDialog.html", null ],
        [ "Digikam::SequenceNumberDialog", "classDigikam_1_1SequenceNumberDialog.html", null ]
      ] ],
      [ "Digikam::ScanController", "classDigikam_1_1ScanController.html", null ],
      [ "Digikam::Sidebar", null, null ],
      [ "Digikam::SqueezedComboBox", null, [
        [ "Digikam::IccProfilesComboBox", "classDigikam_1_1IccProfilesComboBox.html", null ]
      ] ],
      [ "Digikam::StateSavingObject", null, [
        [ "Digikam::AbstractAlbumTreeView", "classDigikam_1_1AbstractAlbumTreeView.html", null ],
        [ "Digikam::FaceScanDialog", "classDigikam_1_1FaceScanDialog.html", null ],
        [ "Digikam::Sidebar", null, null ],
        [ "Digikam::SidebarWidget", "classDigikam_1_1SidebarWidget.html", null ]
      ] ],
      [ "Digikam::SubjectWidget", null, [
        [ "Digikam::SubjectEdit", "classDigikam_1_1SubjectEdit.html", null ],
        [ "DigikamGenericMetadataEditPlugin::IPTCSubjects", "classDigikamGenericMetadataEditPlugin_1_1IPTCSubjects.html", null ],
        [ "DigikamGenericMetadataEditPlugin::XMPSubjects", "classDigikamGenericMetadataEditPlugin_1_1XMPSubjects.html", null ]
      ] ],
      [ "Digikam::ThemeManager", "classDigikam_1_1ThemeManager.html", null ],
      [ "Digikam::ThumbnailCreator", "classDigikam_1_1ThumbnailCreator.html", null ],
      [ "Digikam::ThumbnailImageCatcher", "classDigikam_1_1ThumbnailImageCatcher.html", null ],
      [ "Digikam::ThumbnailLoadThread", "classDigikam_1_1ThumbnailLoadThread.html", null ],
      [ "Digikam::TransitionMngr", "classDigikam_1_1TransitionMngr.html", null ],
      [ "Digikam::UndoAction", null, [
        [ "Digikam::UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html", null ],
        [ "Digikam::UndoActionReversible", "classDigikam_1_1UndoActionReversible.html", null ]
      ] ],
      [ "Digikam::VideoDecoder", "classDigikam_1_1VideoDecoder.html", null ],
      [ "Digikam::WBFilter", null, [
        [ "Digikam::AutoExpoFilter", "classDigikam_1_1AutoExpoFilter.html", null ]
      ] ],
      [ "Digikam::WorkerObject", null, [
        [ "Digikam::DatabaseWorkerInterface", "classDigikam_1_1DatabaseWorkerInterface.html", [
          [ "Digikam::FileActionMngrDatabaseWorker", "classDigikam_1_1FileActionMngrDatabaseWorker.html", null ]
        ] ],
        [ "Digikam::DatabaseWriter", "classDigikam_1_1DatabaseWriter.html", null ],
        [ "Digikam::DetectionBenchmarker", "classDigikam_1_1DetectionBenchmarker.html", null ],
        [ "Digikam::DetectionWorker", "classDigikam_1_1DetectionWorker.html", null ],
        [ "Digikam::FileWorkerInterface", "classDigikam_1_1FileWorkerInterface.html", [
          [ "Digikam::FileActionMngrFileWorker", "classDigikam_1_1FileActionMngrFileWorker.html", null ],
          [ "Digikam::ParallelAdapter< Digikam::FileWorkerInterface >", "classDigikam_1_1ParallelAdapter.html", null ]
        ] ],
        [ "Digikam::ItemFilterModelWorker", "classDigikam_1_1ItemFilterModelWorker.html", [
          [ "Digikam::ItemFilterModelFilterer", "classDigikam_1_1ItemFilterModelFilterer.html", null ],
          [ "Digikam::ItemFilterModelPreparer", "classDigikam_1_1ItemFilterModelPreparer.html", null ]
        ] ],
        [ "Digikam::RecognitionBenchmarker", "classDigikam_1_1RecognitionBenchmarker.html", null ],
        [ "Digikam::RecognitionWorker", "classDigikam_1_1RecognitionWorker.html", null ],
        [ "Digikam::TrainerWorker", "classDigikam_1_1TrainerWorker.html", null ]
      ] ],
      [ "Digikam::WSNewAlbumDialog", null, [
        [ "DigikamGenericBoxPlugin::BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html", null ],
        [ "DigikamGenericDropBoxPlugin::DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html", null ],
        [ "DigikamGenericFaceBookPlugin::FbNewAlbumDlg", "classDigikamGenericFaceBookPlugin_1_1FbNewAlbumDlg.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrNewAlbumDlg", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg.html", null ],
        [ "DigikamGenericImageShackPlugin::ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html", null ],
        [ "DigikamGenericOneDrivePlugin::ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html", null ],
        [ "DigikamGenericPinterestPlugin::PNewAlbumDlg", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html", null ],
        [ "DigikamGenericRajcePlugin::RajceNewAlbumDlg", "classDigikamGenericRajcePlugin_1_1RajceNewAlbumDlg.html", null ],
        [ "DigikamGenericTwitterPlugin::TwNewAlbumDlg", "classDigikamGenericTwitterPlugin_1_1TwNewAlbumDlg.html", null ]
      ] ],
      [ "Digikam::WSSettingsWidget", null, [
        [ "DigikamGenericBoxPlugin::BOXWidget", "classDigikamGenericBoxPlugin_1_1BOXWidget.html", null ],
        [ "DigikamGenericDropBoxPlugin::DBWidget", "classDigikamGenericDropBoxPlugin_1_1DBWidget.html", null ],
        [ "DigikamGenericFaceBookPlugin::FbWidget", "classDigikamGenericFaceBookPlugin_1_1FbWidget.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrWidget", "classDigikamGenericFlickrPlugin_1_1FlickrWidget.html", null ],
        [ "DigikamGenericGoogleServicesPlugin::GSWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html", null ],
        [ "DigikamGenericImageShackPlugin::ImageShackWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget.html", null ],
        [ "DigikamGenericOneDrivePlugin::ODWidget", "classDigikamGenericOneDrivePlugin_1_1ODWidget.html", null ],
        [ "DigikamGenericPinterestPlugin::PWidget", "classDigikamGenericPinterestPlugin_1_1PWidget.html", null ],
        [ "DigikamGenericTwitterPlugin::TwWidget", "classDigikamGenericTwitterPlugin_1_1TwWidget.html", null ]
      ] ],
      [ "Digikam::WSToolDialog", null, [
        [ "DigikamGenericDebianScreenshotsPlugin::DSWindow", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html", null ],
        [ "DigikamGenericRajcePlugin::RajceWindow", "classDigikamGenericRajcePlugin_1_1RajceWindow.html", null ]
      ] ],
      [ "DigikamGenericFlickrPlugin::FlickrWidget", "classDigikamGenericFlickrPlugin_1_1FlickrWidget.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::AbstractThemeParameter", null, [
        [ "DigikamGenericHtmlGalleryPlugin::ColorThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1ColorThemeParameter.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::StringThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1StringThemeParameter.html", null ]
      ] ],
      [ "DigikamGenericImageShackPlugin::ImageShackWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget.html", null ],
      [ "DigikamGenericRajcePlugin::RajceCommand", null, [
        [ "DigikamGenericRajcePlugin::AlbumListCommand", "classDigikamGenericRajcePlugin_1_1AlbumListCommand.html", null ],
        [ "DigikamGenericRajcePlugin::CloseAlbumCommand", "classDigikamGenericRajcePlugin_1_1CloseAlbumCommand.html", null ],
        [ "DigikamGenericRajcePlugin::CreateAlbumCommand", "classDigikamGenericRajcePlugin_1_1CreateAlbumCommand.html", null ],
        [ "DigikamGenericRajcePlugin::LoginCommand", "classDigikamGenericRajcePlugin_1_1LoginCommand.html", null ],
        [ "DigikamGenericRajcePlugin::OpenAlbumCommand", "classDigikamGenericRajcePlugin_1_1OpenAlbumCommand.html", null ]
      ] ],
      [ "ShowFoto::ShowFoto", "classShowFoto_1_1ShowFoto.html", null ]
    ] ],
    [ "Private public QObject", null, [
      [ "Digikam::DDateTable", "classDigikam_1_1DDateTable.html", null ],
      [ "Digikam::DigikamItemView", "classDigikam_1_1DigikamItemView.html", null ],
      [ "Digikam::DNotificationWidget", "classDigikam_1_1DNotificationWidget.html", null ],
      [ "Digikam::FacePipeline", "classDigikam_1_1FacePipeline.html", null ],
      [ "Digikam::FileActionMngr", "classDigikam_1_1FileActionMngr.html", null ],
      [ "Digikam::ImportIconView", "classDigikam_1_1ImportIconView.html", null ]
    ] ],
    [ "Private public QRunnable", null, [
      [ "Digikam::DynamicThread", null, [
        [ "Digikam::DImgThreadedFilter", "classDigikam_1_1DImgThreadedFilter.html", [
          [ "Digikam::AntiVignettingFilter", "classDigikam_1_1AntiVignettingFilter.html", null ],
          [ "Digikam::AutoLevelsFilter", "classDigikam_1_1AutoLevelsFilter.html", null ],
          [ "Digikam::ColorFXFilter", "classDigikam_1_1ColorFXFilter.html", null ],
          [ "Digikam::CurvesFilter", "classDigikam_1_1CurvesFilter.html", null ],
          [ "Digikam::DImgThreadedAnalyser", "classDigikam_1_1DImgThreadedAnalyser.html", null ],
          [ "Digikam::EmbossFilter", "classDigikam_1_1EmbossFilter.html", null ],
          [ "Digikam::EqualizeFilter", "classDigikam_1_1EqualizeFilter.html", null ],
          [ "Digikam::FilmFilter", "classDigikam_1_1FilmFilter.html", null ],
          [ "Digikam::IccTransformFilter", "classDigikam_1_1IccTransformFilter.html", null ],
          [ "Digikam::InfraredFilter", "classDigikam_1_1InfraredFilter.html", null ],
          [ "Digikam::InvertFilter", "classDigikam_1_1InvertFilter.html", null ],
          [ "Digikam::LensDistortionFilter", "classDigikam_1_1LensDistortionFilter.html", null ],
          [ "Digikam::LevelsFilter", "classDigikam_1_1LevelsFilter.html", null ],
          [ "Digikam::MixerFilter", "classDigikam_1_1MixerFilter.html", null ],
          [ "Digikam::NormalizeFilter", "classDigikam_1_1NormalizeFilter.html", null ],
          [ "Digikam::RawProcessingFilter", "classDigikam_1_1RawProcessingFilter.html", null ],
          [ "Digikam::SharpenFilter", "classDigikam_1_1SharpenFilter.html", null ],
          [ "Digikam::StretchFilter", "classDigikam_1_1StretchFilter.html", null ],
          [ "Digikam::TextureFilter", "classDigikam_1_1TextureFilter.html", null ],
          [ "Digikam::TonalityFilter", "classDigikam_1_1TonalityFilter.html", null ],
          [ "Digikam::UnsharpMaskFilter", "classDigikam_1_1UnsharpMaskFilter.html", null ],
          [ "Digikam::WBFilter", null, null ],
          [ "DigikamEditorHotPixelsToolPlugin::HotPixelFixer", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer.html", null ],
          [ "DigikamRawImportNativePlugin::RawPostProcessing", "classDigikamRawImportNativePlugin_1_1RawPostProcessing.html", null ]
        ] ],
        [ "Digikam::LoadSaveThread", null, null ],
        [ "Digikam::ScanStateFilter", "classDigikam_1_1ScanStateFilter.html", null ]
      ] ]
    ] ],
    [ "Private public QSharedData", null, [
      [ "Digikam::DImg", "classDigikam_1_1DImg.html", null ],
      [ "Digikam::MetaEngineData", "classDigikam_1_1MetaEngineData.html", null ]
    ] ],
    [ "Private", null, [
      [ "DigikamGenericPresentationPlugin::PresentationKB", "classDigikamGenericPresentationPlugin_1_1PresentationKB.html", null ]
    ] ],
    [ "profile_data", "classprofile__data.html", null ],
    [ "profile_tier_level", "classprofile__tier__level.html", null ],
    [ "pt_point", "structpt__point.html", null ],
    [ "pt_point_double", "structpt__point__double.html", null ],
    [ "pt_script", "structpt__script.html", null ],
    [ "pt_script_ctrl_point", "structpt__script__ctrl__point.html", null ],
    [ "pt_script_image", "structpt__script__image.html", null ],
    [ "pt_script_mask", "structpt__script__mask.html", null ],
    [ "pt_script_optimize", "structpt__script__optimize.html", null ],
    [ "pt_script_optimize_var", "structpt__script__optimize__var.html", null ],
    [ "pt_script_pano", "structpt__script__pano.html", null ],
    [ "QAbstractButton", null, [
      [ "Digikam::CoordinatesOverlayWidget", "classDigikam_1_1CoordinatesOverlayWidget.html", null ],
      [ "Digikam::GroupIndicatorOverlayWidget", "classDigikam_1_1GroupIndicatorOverlayWidget.html", null ],
      [ "Digikam::ImportOverlayWidget", "classDigikam_1_1ImportOverlayWidget.html", null ],
      [ "Digikam::ItemViewHoverButton", "classDigikam_1_1ItemViewHoverButton.html", [
        [ "Digikam::FaceRejectionOverlayButton", "classDigikam_1_1FaceRejectionOverlayButton.html", null ],
        [ "Digikam::ImportRotateOverlayButton", "classDigikam_1_1ImportRotateOverlayButton.html", null ],
        [ "Digikam::ItemFullScreenOverlayButton", "classDigikam_1_1ItemFullScreenOverlayButton.html", null ],
        [ "Digikam::ItemRotateOverlayButton", "classDigikam_1_1ItemRotateOverlayButton.html", null ],
        [ "Digikam::ItemSelectionOverlayButton", "classDigikam_1_1ItemSelectionOverlayButton.html", null ]
      ] ],
      [ "ShowFoto::ShowfotoCoordinatesOverlayWidget", "classShowFoto_1_1ShowfotoCoordinatesOverlayWidget.html", null ]
    ] ],
    [ "QAbstractItemDelegate", null, [
      [ "Digikam::DConfigDlgInternal::DConfigDlgListViewDelegate", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewDelegate.html", null ],
      [ "Digikam::DItemDelegate", null, null ],
      [ "Digikam::DWItemDelegate", "classDigikam_1_1DWItemDelegate.html", [
        [ "Digikam::SetupCollectionDelegate", "classDigikam_1_1SetupCollectionDelegate.html", null ]
      ] ]
    ] ],
    [ "QAbstractItemModel", null, [
      [ "Digikam::AbstractAlbumModel", null, null ],
      [ "Digikam::DConfigDlgModel", "classDigikam_1_1DConfigDlgModel.html", [
        [ "Digikam::DConfigDlgWdgModel", "classDigikam_1_1DConfigDlgWdgModel.html", null ]
      ] ],
      [ "Digikam::SetupCollectionModel", "classDigikam_1_1SetupCollectionModel.html", null ]
    ] ],
    [ "QAbstractItemView", null, [
      [ "Digikam::DConfigDlgInternal::DConfigDlgPlainView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgPlainView.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgTabbedView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTabbedView.html", null ]
    ] ],
    [ "QAbstractListModel", null, [
      [ "Digikam::ChoiceSearchModel", "classDigikam_1_1ChoiceSearchModel.html", null ],
      [ "Digikam::ItemModel", null, null ],
      [ "Digikam::RatingComboBoxModel", "classDigikam_1_1RatingComboBoxModel.html", null ]
    ] ],
    [ "QAbstractProxyModel", null, [
      [ "Digikam::DConfigDlgInternal::DConfigDlgListViewProxy", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewProxy.html", null ]
    ] ],
    [ "QAction", null, [
      [ "Digikam::DPluginAction", "classDigikam_1_1DPluginAction.html", null ],
      [ "Digikam::RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", null ],
      [ "Digikam::RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", null ]
    ] ],
    [ "QByteArray", null, [
      [ "Digikam::NonDeterministicRandomData", "classDigikam_1_1NonDeterministicRandomData.html", null ]
    ] ],
    [ "QCheckBox", null, [
      [ "DigikamGenericMetadataEditPlugin::MetadataCheckBox", "classDigikamGenericMetadataEditPlugin_1_1MetadataCheckBox.html", null ]
    ] ],
    [ "QComboBox", null, [
      [ "Digikam::GeolocationFilter", "classDigikam_1_1GeolocationFilter.html", null ],
      [ "Digikam::IccRenderingIntentComboBox", "classDigikam_1_1IccRenderingIntentComboBox.html", null ],
      [ "Digikam::MimeFilter", "classDigikam_1_1MimeFilter.html", null ],
      [ "Digikam::ModelIndexBasedComboBox", "classDigikam_1_1ModelIndexBasedComboBox.html", [
        [ "Digikam::RatingComboBox", "classDigikam_1_1RatingComboBox.html", null ],
        [ "Digikam::StayPoppedUpComboBox", "classDigikam_1_1StayPoppedUpComboBox.html", [
          [ "Digikam::ListViewComboBox", "classDigikam_1_1ListViewComboBox.html", [
            [ "Digikam::ChoiceSearchComboBox", "classDigikam_1_1ChoiceSearchComboBox.html", null ]
          ] ],
          [ "Digikam::TreeViewComboBox", "classDigikam_1_1TreeViewComboBox.html", [
            [ "Digikam::TreeViewLineEditComboBox", "classDigikam_1_1TreeViewLineEditComboBox.html", [
              [ "Digikam::AlbumSelectComboBox", null, null ]
            ] ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::SqueezedComboBox", null, null ],
      [ "Digikam::TimeZoneComboBox", "classDigikam_1_1TimeZoneComboBox.html", null ]
    ] ],
    [ "QDBusAbstractAdaptor", null, [
      [ "CoreDbWatchAdaptor", "classCoreDbWatchAdaptor.html", null ],
      [ "DigikamAdaptor", "classDigikamAdaptor.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "Digikam::CameraInfoDialog", "classDigikam_1_1CameraInfoDialog.html", null ],
      [ "Digikam::DConfigDlg", "classDigikam_1_1DConfigDlg.html", null ],
      [ "Digikam::DPluginAboutDlg", "classDigikam_1_1DPluginAboutDlg.html", null ],
      [ "Digikam::DPluginDialog", "classDigikam_1_1DPluginDialog.html", [
        [ "Digikam::WSToolDialog", null, null ],
        [ "DigikamGenericGLViewerPlugin::GLViewerHelpDlg", "classDigikamGenericGLViewerPlugin_1_1GLViewerHelpDlg.html", null ]
      ] ],
      [ "Digikam::FaceScanDialog", "classDigikam_1_1FaceScanDialog.html", null ],
      [ "Digikam::FileSaveOptionsDlg", "classDigikam_1_1FileSaveOptionsDlg.html", null ],
      [ "Digikam::ICCProfileInfoDlg", "classDigikam_1_1ICCProfileInfoDlg.html", null ],
      [ "Digikam::InfoDlg", null, null ],
      [ "Digikam::RuleDialog", null, null ],
      [ "Digikam::SlideHelp", "classDigikam_1_1SlideHelp.html", null ],
      [ "Digikam::WSNewAlbumDialog", null, null ],
      [ "DigikamGenericPresentationPlugin::SoundtrackPreview", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview.html", null ],
      [ "DigikamGenericPrintCreatorPlugin::AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", null ]
    ] ],
    [ "QDockWidget", null, [
      [ "Digikam::ThumbBarDock", "classDigikam_1_1ThumbBarDock.html", null ]
    ] ],
    [ "QFileDialog", null, [
      [ "Digikam::DFileDialog", "classDigikam_1_1DFileDialog.html", null ]
    ] ],
    [ "QFileIconProvider", null, [
      [ "Digikam::DFileIconProvider", "classDigikam_1_1DFileIconProvider.html", null ]
    ] ],
    [ "QFrame", null, [
      [ "Digikam::AssignNameWidget", "classDigikam_1_1AssignNameWidget.html", null ],
      [ "Digikam::DDatePicker", "classDigikam_1_1DDatePicker.html", null ],
      [ "Digikam::DHBox", "classDigikam_1_1DHBox.html", [
        [ "Digikam::DVBox", "classDigikam_1_1DVBox.html", [
          [ "Digikam::ColorLabelWidget", null, null ],
          [ "Digikam::PickLabelWidget", null, null ]
        ] ],
        [ "Digikam::OverlayWidget", "classDigikam_1_1OverlayWidget.html", null ]
      ] ],
      [ "Digikam::DLineWidget", "classDigikam_1_1DLineWidget.html", null ],
      [ "Digikam::DNotificationWidget", "classDigikam_1_1DNotificationWidget.html", null ]
    ] ],
    [ "QGraphicsObject", null, [
      [ "Digikam::DImgChildItem", null, null ],
      [ "Digikam::GraphicsDImgItem", "classDigikam_1_1GraphicsDImgItem.html", null ]
    ] ],
    [ "QGraphicsView", null, [
      [ "Digikam::GraphicsDImgView", null, null ]
    ] ],
    [ "QHash", null, [
      [ "Digikam::DatabaseFields::Hash< T >", "classDigikam_1_1DatabaseFields_1_1Hash.html", null ],
      [ "Digikam::DatabaseFields::Hash< QVariant >", "classDigikam_1_1DatabaseFields_1_1Hash.html", null ]
    ] ],
    [ "QItemDelegate", null, [
      [ "Digikam::RatingComboBoxDelegate", "classDigikam_1_1RatingComboBoxDelegate.html", null ],
      [ "Digikam::TableViewItemDelegate", "classDigikam_1_1TableViewItemDelegate.html", null ]
    ] ],
    [ "QItemSelectionModel", null, [
      [ "Digikam::DConfigDlgInternal::SelectionModel", "classDigikam_1_1DConfigDlgInternal_1_1SelectionModel.html", null ],
      [ "Digikam::GPSLinkItemSelectionModel", "classDigikam_1_1GPSLinkItemSelectionModel.html", null ]
    ] ],
    [ "QLabel", null, [
      [ "Digikam::DActiveLabel", "classDigikam_1_1DActiveLabel.html", null ],
      [ "Digikam::DAdjustableLabel", null, null ],
      [ "Digikam::DClickLabel", "classDigikam_1_1DClickLabel.html", null ],
      [ "Digikam::DTextLabelName", "classDigikam_1_1DTextLabelName.html", null ]
    ] ],
    [ "QLineEdit", null, [
      [ "Digikam::DatePickerYearSelector", "classDigikam_1_1DatePickerYearSelector.html", null ],
      [ "Digikam::ProxyLineEdit", "classDigikam_1_1ProxyLineEdit.html", [
        [ "Digikam::ProxyClickLineEdit", "classDigikam_1_1ProxyClickLineEdit.html", null ]
      ] ]
    ] ],
    [ "QList< T >", "classQList.html", null ],
    [ "QList< AbstractThemeParameter * >", "classQList.html", null ],
    [ "QList< Album * >", "classQList.html", null ],
    [ "QList< AlbumPointer< T > >", "classQList.html", [
      [ "Digikam::AlbumPointerList< T >", "classDigikam_1_1AlbumPointerList.html", null ]
    ] ],
    [ "QList< AnimationControl * >", "classQList.html", null ],
    [ "QList< BatchTool * >", "classQList.html", null ],
    [ "QList< BatchToolSet >", "classQList.html", null ],
    [ "QList< BookmarkNode * >", "classQList.html", null ],
    [ "QList< CameraCommand * >", "classQList.html", null ],
    [ "QList< CameraType * >", "classQList.html", null ],
    [ "QList< CamItemInfo >", "classQList.html", null ],
    [ "QList< CHUpdateItem >", "classQList.html", null ],
    [ "QList< Correlation >", "classQList.html", null ],
    [ "QList< CropHandle >", "classQList.html", null ],
    [ "QList< DateFormatDescriptor >", "classQList.html", null ],
    [ "QList< Digikam::AbstractAlbumTreeView::ContextMenuElement * >", "classQList.html", null ],
    [ "QList< Digikam::Album * >", "classQList.html", null ],
    [ "QList< Digikam::AlbumChangeset >", "classQList.html", null ],
    [ "QList< Digikam::AlbumRootChangeset >", "classQList.html", null ],
    [ "QList< Digikam::AlbumShortInfo >", "classQList.html", null ],
    [ "QList< Digikam::CamItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::Cascade >", "classQList.html", null ],
    [ "QList< Digikam::ChoiceSearchModel::Entry >", "classQList.html", null ],
    [ "QList< Digikam::CollectionImageChangeset >", "classQList.html", null ],
    [ "QList< Digikam::CommentInfo >", "classQList.html", null ],
    [ "QList< Digikam::CopyrightInfo >", "classQList.html", null ],
    [ "QList< Digikam::DbEngineActionElement >", "classQList.html", null ],
    [ "QList< Digikam::DImageHistory::Entry >", "classQList.html", null ],
    [ "QList< Digikam::DMultiTabBarButton * >", "classQList.html", null ],
    [ "QList< Digikam::DNNFaceVecMetadata >", "classQList.html", null ],
    [ "QList< Digikam::DPluginAction * >", "classQList.html", null ],
    [ "QList< Digikam::EditorCore::FileToSave >", "classQList.html", null ],
    [ "QList< Digikam::EigenFaceMatMetadata >", "classQList.html", null ],
    [ "QList< Digikam::FaceItem * >", "classQList.html", null ],
    [ "QList< Digikam::Filter * >", "classQList.html", null ],
    [ "QList< Digikam::FilterAction >", "classQList.html", null ],
    [ "QList< Digikam::FisherFaceMatMetadata >", "classQList.html", null ],
    [ "QList< Digikam::GeoIfaceInternalWidgetInfo >", "classQList.html", null ],
    [ "QList< Digikam::GeoModelHelper * >", "classQList.html", null ],
    [ "QList< Digikam::GPSItemContainer * >", "classQList.html", null ],
    [ "QList< Digikam::GPSItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::Graph::Vertex >", "classQList.html", null ],
    [ "QList< Digikam::HistoryImageId >", "classQList.html", null ],
    [ "QList< Digikam::ImageChangeset >", "classQList.html", null ],
    [ "QList< Digikam::ImageTagChangeset >", "classQList.html", null ],
    [ "QList< Digikam::ItemDelegateOverlay * >", "classQList.html", null ],
    [ "QList< Digikam::ItemFilterModelPrepareHook * >", "classQList.html", null ],
    [ "QList< Digikam::ItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::ItemListerRecord >", "classQList.html", null ],
    [ "QList< Digikam::ItemQueryPostHook * >", "classQList.html", null ],
    [ "QList< Digikam::LBPHistogramMetadata >", "classQList.html", null ],
    [ "QList< Digikam::LoadingDescription >", "classQList.html", null ],
    [ "QList< Digikam::LoadingProcessListener * >", "classQList.html", null ],
    [ "QList< Digikam::LoadSaveTask * >", "classQList.html", null ],
    [ "QList< Digikam::MapBackend * >", "classQList.html", null ],
    [ "QList< Digikam::PageItem * >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::ControlPoint >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::Mask >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::Optimization >", "classQList.html", null ],
    [ "QList< Digikam::RGBackend * >", "classQList.html", null ],
    [ "QList< Digikam::RGInfo >", "classQList.html", null ],
    [ "QList< Digikam::SearchChangeset >", "classQList.html", null ],
    [ "QList< Digikam::SearchField * >", "classQList.html", null ],
    [ "QList< Digikam::SearchFieldGroup * >", "classQList.html", null ],
    [ "QList< Digikam::SearchFieldGroupLabel * >", "classQList.html", null ],
    [ "QList< Digikam::SearchGroup * >", "classQList.html", null ],
    [ "QList< Digikam::SetupCollectionModel::Item >", "classQList.html", null ],
    [ "QList< Digikam::SidebarWidget * >", "classQList.html", null ],
    [ "QList< Digikam::SimpleTreeModel::Item * >", "classQList.html", null ],
    [ "QList< Digikam::SolidVolumeInfo >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumn * >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnConfiguration >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnDescription >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnProfile >", "classQList.html", null ],
    [ "QList< Digikam::TableViewModel::Item * >", "classQList.html", null ],
    [ "QList< Digikam::TagChangeset >", "classQList.html", null ],
    [ "QList< Digikam::TaggingAction >", "classQList.html", null ],
    [ "QList< Digikam::TagProperty >", "classQList.html", null ],
    [ "QList< Digikam::TagShortInfo >", "classQList.html", null ],
    [ "QList< Digikam::TAlbum * >", "classQList.html", null ],
    [ "QList< Digikam::Template >", "classQList.html", null ],
    [ "QList< Digikam::ThumbnailLoadThread * >", "classQList.html", null ],
    [ "QList< Digikam::TileIndex >", "classQList.html", null ],
    [ "QList< Digikam::TrackManager::TrackPoint >", "classQList.html", null ],
    [ "QList< Digikam::TreeBranch * >", "classQList.html", null ],
    [ "QList< Digikam::VisibilityObject * >", "classQList.html", null ],
    [ "QList< Digikam::Workflow >", "classQList.html", null ],
    [ "QList< DigikamEditorHotPixelsToolPlugin::HotPixel >", "classQList.html", null ],
    [ "QList< DigikamEditorHotPixelsToolPlugin::HotPixelsWeights >", "classQList.html", null ],
    [ "QList< DigikamGenericGeolocationEditPlugin::SearchResultModel::SearchResultItem >", "classQList.html", null ],
    [ "QList< DigikamGenericGoogleServicesPlugin::GSFolder >", "classQList.html", null ],
    [ "QList< DigikamGenericPrintCreatorPlugin::AdvPrintPhoto * >", "classQList.html", null ],
    [ "QList< DigikamGenericPrintCreatorPlugin::AdvPrintPhotoSize * >", "classQList.html", null ],
    [ "QList< DigikamGenericYFPlugin::YandexFotkiAlbum >", "classQList.html", null ],
    [ "QList< DigikamGenericYFPlugin::YFPhoto >", "classQList.html", null ],
    [ "QList< DLabelExpander * >", "classQList.html", null ],
    [ "QList< DMultiTabBarTab * >", "classQList.html", null ],
    [ "QList< double >", "classQList.html", null ],
    [ "QList< DPlugin * >", "classQList.html", null ],
    [ "QList< DPluginCB * >", "classQList.html", null ],
    [ "QList< DrawEvent >", "classQList.html", null ],
    [ "QList< DTrashItemInfo >", "classQList.html", null ],
    [ "QList< Exiv2::PreviewProperties >", "classQList.html", null ],
    [ "QList< FacePipelineExtendedPackage::Ptr >", "classQList.html", [
      [ "Digikam::PackageLoadingDescriptionList", "classDigikam_1_1PackageLoadingDescriptionList.html", null ]
    ] ],
    [ "QList< FacePipelineFaceTagsIface >", "classQList.html", [
      [ "Digikam::FacePipelineFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html", null ]
    ] ],
    [ "QList< Filter * >", "classQList.html", null ],
    [ "QList< GeoIfaceCluster >", "classQList.html", null ],
    [ "QList< GeonamesInternalJobs >", "classQList.html", null ],
    [ "QList< GeonamesUSInternalJobs >", "classQList.html", null ],
    [ "QList< GPSItemInfo >", "classQList.html", null ],
    [ "QList< HistoryItem >", "classQList.html", null ],
    [ "QList< HistoryTreeItem * >", "classQList.html", null ],
    [ "QList< IccProfile >", "classQList.html", null ],
    [ "QList< Identity >", "classQList.html", null ],
    [ "QList< ImgFilterPtr >", "classQList.html", null ],
    [ "QList< int >", "classQList.html", null ],
    [ "QList< InternalJobs >", "classQList.html", null ],
    [ "QList< ItemFiltersHistoryTreeItem * >", "classQList.html", null ],
    [ "QList< ItemInfo >", "classQList.html", [
      [ "Digikam::FileActionItemInfoList", "classDigikam_1_1FileActionItemInfoList.html", [
        [ "Digikam::ItemInfoTaskSplitter", "classDigikam_1_1ItemInfoTaskSplitter.html", null ]
      ] ],
      [ "Digikam::ItemInfoList", "classDigikam_1_1ItemInfoList.html", null ]
    ] ],
    [ "QList< KActionCollection * >", "classQList.html", null ],
    [ "QList< KJob * >", "classQList.html", null ],
    [ "QList< ListItem * >", "classQList.html", null ],
    [ "QList< MergedRequests >", "classQList.html", null ],
    [ "QList< NewNameInfo >", "classQList.html", null ],
    [ "QList< OsmInternalJobs >", "classQList.html", null ],
    [ "QList< Private::CatcherResult >", "classQList.html", null ],
    [ "QList< QAction * >", "classQList.html", null ],
    [ "QList< QByteArray >", "classQList.html", null ],
    [ "QList< QDateTime >", "classQList.html", null ],
    [ "QList< QImage >", "classQList.html", null ],
    [ "QList< QLatin1String >", "classQList.html", null ],
    [ "QList< QLayoutItem * >", "classQList.html", null ],
    [ "QList< QList >", "classQList.html", null ],
    [ "QList< QList< Digikam::TagData > >", "classQList.html", null ],
    [ "QList< QList< QWidget * > >", "classQList.html", null ],
    [ "QList< qlonglong >", "classQList.html", null ],
    [ "QList< QMap< int, QVariant > >", "classQList.html", null ],
    [ "QList< QMetaMethod >", "classQList.html", null ],
    [ "QList< QModelIndex >", "classQList.html", null ],
    [ "QList< QObject * >", "classQList.html", null ],
    [ "QList< QPair< Digikam::GeoCoordinates, QList > >", "classQList.html", null ],
    [ "QList< QPair< Digikam::TileIndex, Digikam::TileIndex > >", "classQList.html", null ],
    [ "QList< QPair< QString, int > >", "classQList.html", null ],
    [ "QList< QPair< QString, QString > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, DigikamGenericFlickrPlugin::FPhotoInfo > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, DigikamGenericGoogleServicesPlugin::GSPhoto > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, QString > >", "classQList.html", null ],
    [ "QList< QPersistentModelIndex >", "classQList.html", null ],
    [ "QList< QPoint >", "classQList.html", null ],
    [ "QList< QPointer< const QAbstractProxyModel > >", "classQList.html", null ],
    [ "QList< QPointer< MapWidget > >", "classQList.html", null ],
    [ "QList< QPrinterInfo >", "classQList.html", null ],
    [ "QList< QRect * >", "classQList.html", null ],
    [ "QList< QRectF >", "classQList.html", null ],
    [ "QList< QRegExp >", "classQList.html", null ],
    [ "QList< QStandardItemModel * >", "classQList.html", null ],
    [ "QList< QString >", "classQList.html", null ],
    [ "QList< QToolButton * >", "classQList.html", null ],
    [ "QList< QUrl >", "classQList.html", null ],
    [ "QList< QVariant >", "classQList.html", null ],
    [ "QList< RatingComboBox::RatingValue >", "classQList.html", null ],
    [ "QList< Rule * >", "classQList.html", null ],
    [ "QList< SearchResult >", "classQList.html", null ],
    [ "QList< ShowfotoItemInfo >", "classQList.html", null ],
    [ "QList< Sidebar * >", "classQList.html", null ],
    [ "QList< Task * >", "classQList.html", null ],
    [ "QList< TileIndex >", "classQList.html", null ],
    [ "QList< TodoPair >", "classQList.html", null ],
    [ "QList< Token * >", "classQList.html", null ],
    [ "QList< Track >", "classQList.html", null ],
    [ "QList< TrackManager::TrackChanges >", "classQList.html", null ],
    [ "QList< Type >", "classQList.html", null ],
    [ "QList< UndoAction * >", "classQList.html", null ],
    [ "QList< UndoInfo >", "classQList.html", null ],
    [ "QList< VertexItem * >", "classQList.html", null ],
    [ "QList< WorkerObject * >", "classQList.html", null ],
    [ "QListView", null, [
      [ "Digikam::DCategorizedView", "classDigikam_1_1DCategorizedView.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgListView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListView.html", null ],
      [ "Digikam::NamespaceListView", "classDigikam_1_1NamespaceListView.html", null ]
    ] ],
    [ "QListWidget", null, [
      [ "Digikam::DTextList", "classDigikam_1_1DTextList.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioList", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList.html", null ]
    ] ],
    [ "QListWidgetItem", null, [
      [ "Digikam::FilmContainer::ListItem", "classDigikam_1_1FilmContainer_1_1ListItem.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::ThemeListBoxItem", "classDigikamGenericHtmlGalleryPlugin_1_1ThemeListBoxItem.html", null ]
    ] ],
    [ "QMap< Key, Value >", "classQMap.html", [
      [ "Digikam::QMapForAdaptors< Key, Value >", "classDigikam_1_1QMapForAdaptors.html", null ]
    ] ],
    [ "QMap< ActionJob *, int >", "classQMap.html", null ],
    [ "QMap< const ProgressItem *, TransactionItem * >", "classQMap.html", null ],
    [ "QMap< const void *, void * >", "classQMap.html", null ],
    [ "QMap< EffectMngr::EffectType, EffectMethod >", "classQMap.html", null ],
    [ "QMap< int, Digikam::AlbumRootLocation * >", "classQMap.html", null ],
    [ "QMap< int, Digikam::DImgThreadedFilter * >", "classQMap.html", null ],
    [ "QMap< int, Digikam::QListImageListProvider >", "classQMap.html", null ],
    [ "QMap< int, Digikam::RecognitionBenchmarker::Statistics >", "classQMap.html", null ],
    [ "QMap< int, Digikam::State >", "classQMap.html", null ],
    [ "QMap< int, Digikam::VersionFileInfo >", "classQMap.html", null ],
    [ "QMap< int, DisjointMetadata::Status >", "classQMap.html", null ],
    [ "QMap< int, FlashMode >", "classQMap.html", null ],
    [ "QMap< int, int >", "classQMap.html", null ],
    [ "QMap< int, MetadataHub::Status >", "classQMap.html", null ],
    [ "QMap< int, QAction * >", "classQMap.html", null ],
    [ "QMap< int, QList< ListItem * > >", "classQMap.html", null ],
    [ "QMap< int, QPixmap >", "classQMap.html", null ],
    [ "QMap< int, QString >", "classQMap.html", null ],
    [ "QMap< int, QUrl >", "classQMap.html", null ],
    [ "QMap< int, QVariant >", "classQMap.html", null ],
    [ "QMap< int, StatPair >", "classQMap.html", null ],
    [ "QMap< KToolBar *, bool >", "classQMap.html", null ],
    [ "QMap< MailClient, QString >", "classQMap.html", null ],
    [ "QMap< ParseResults::ResultsKey, QStringList >", "classQMap.html", null ],
    [ "QMap< ProgressItem *, bool >", "classQMap.html", null ],
    [ "QMap< QAction *, QAction * >", "classQMap.html", null ],
    [ "QMap< QByteArray, QWidget * >", "classQMap.html", null ],
    [ "QMap< QDate, Day >", "classQMap.html", null ],
    [ "QMap< QDateTime, bool >", "classQMap.html", null ],
    [ "QMap< QDateTime, GeoDataContainer >", "classQMap.html", null ],
    [ "QMap< QPair< int, int >, QVariant >", "classQMap.html", null ],
    [ "QMap< QPair< qlonglong, QString >, QList< int > >", "classQMap.html", null ],
    [ "QMap< QString, CaptionValues >", "classQMap.html", [
      [ "Digikam::CaptionsMap", "classDigikam_1_1CaptionsMap.html", null ]
    ] ],
    [ "QMap< QString, DbKeysCollection * >", "classQMap.html", null ],
    [ "QMap< QString, Digikam::DbEngineAction >", "classQMap.html", null ],
    [ "QMap< QString, Digikam::DbEngineConfigSettings >", "classQMap.html", null ],
    [ "QMap< QString, Digikam::LoadingProcess * >", "classQMap.html", null ],
    [ "QMap< QString, EffectMethod >", "classQMap.html", null ],
    [ "QMap< QString, ICCTagInfo >", "classQMap.html", null ],
    [ "QMap< QString, ImgFilterPtr >", "classQMap.html", null ],
    [ "QMap< QString, int >", "classQMap.html", null ],
    [ "QMap< QString, KService::Ptr >", "classQMap.html", null ],
    [ "QMap< QString, QDateTime >", "classQMap.html", null ],
    [ "QMap< QString, QList< Digikam::NamespaceEntry > >", "classQMap.html", null ],
    [ "QMap< QString, QList< QUrl > >", "classQMap.html", null ],
    [ "QMap< QString, QMap< QString, QString > >", "classQMap.html", null ],
    [ "QMap< QString, QPixmap >", "classQMap.html", null ],
    [ "QMap< QString, QPointer< Digikam::ImportUI > >", "classQMap.html", null ],
    [ "QMap< QString, QString >", "classQMap.html", null ],
    [ "QMap< QString, QVariant >", "classQMap.html", null ],
    [ "QMap< QString, SubjectData >", "classQMap.html", null ],
    [ "QMap< QUrl, ExpoBlendingItemPreprocessedUrls >", "classQMap.html", null ],
    [ "QMap< QUrl, int >", "classQMap.html", null ],
    [ "QMap< QUrl, PanoramaPreprocessedUrls >", "classQMap.html", null ],
    [ "QMap< QUrl, PresentationAudioListItem * >", "classQMap.html", null ],
    [ "QMap< QUrl, QDateTime >", "classQMap.html", null ],
    [ "QMap< QUrl, QTime >", "classQMap.html", null ],
    [ "QMap< QUrl, QUrl >", "classQMap.html", null ],
    [ "QMap< ResultsKey, ResultsValue >", "classQMap.html", null ],
    [ "QMap< TransitionMngr::TransType, TransMethod >", "classQMap.html", null ],
    [ "QMap< Vertex, int >", "classQMap.html", [
      [ "Digikam::QMapForAdaptors< Vertex, int >", "classDigikam_1_1QMapForAdaptors.html", null ]
    ] ],
    [ "QMap< Vertex, Vertex >", "classQMap.html", [
      [ "Digikam::QMapForAdaptors< Vertex, Vertex >", "classDigikam_1_1QMapForAdaptors.html", null ]
    ] ],
    [ "QMap< YearMonth, int >", "classQMap.html", null ],
    [ "QMap< YearRefPair, StatPair >", "classQMap.html", null ],
    [ "QMenu", null, [
      [ "Digikam::ColorLabelMenuAction", "classDigikam_1_1ColorLabelMenuAction.html", null ],
      [ "Digikam::IccProfilesMenuAction", "classDigikam_1_1IccProfilesMenuAction.html", null ],
      [ "Digikam::PickLabelMenuAction", "classDigikam_1_1PickLabelMenuAction.html", null ],
      [ "Digikam::RatingMenuAction", "classDigikam_1_1RatingMenuAction.html", null ]
    ] ],
    [ "QMimeData", null, [
      [ "Digikam::DAlbumDrag", "classDigikam_1_1DAlbumDrag.html", null ],
      [ "Digikam::DCameraDragObject", "classDigikam_1_1DCameraDragObject.html", null ],
      [ "Digikam::DCameraItemListDrag", "classDigikam_1_1DCameraItemListDrag.html", null ],
      [ "Digikam::DItemDrag", "classDigikam_1_1DItemDrag.html", null ],
      [ "Digikam::DTagListDrag", "classDigikam_1_1DTagListDrag.html", null ],
      [ "Digikam::MapDragData", "classDigikam_1_1MapDragData.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Digikam::AbstractItemDragDropHandler", "classDigikam_1_1AbstractItemDragDropHandler.html", [
        [ "Digikam::ImportDragDropHandler", "classDigikam_1_1ImportDragDropHandler.html", null ],
        [ "Digikam::ItemDragDropHandler", "classDigikam_1_1ItemDragDropHandler.html", null ],
        [ "ShowFoto::ShowfotoDragDropHandler", "classShowFoto_1_1ShowfotoDragDropHandler.html", null ]
      ] ],
      [ "Digikam::ActionJob", "classDigikam_1_1ActionJob.html", [
        [ "Digikam::DBJob", "classDigikam_1_1DBJob.html", [
          [ "Digikam::AlbumsJob", "classDigikam_1_1AlbumsJob.html", null ],
          [ "Digikam::DatesJob", "classDigikam_1_1DatesJob.html", null ],
          [ "Digikam::GPSJob", "classDigikam_1_1GPSJob.html", null ],
          [ "Digikam::SearchesJob", "classDigikam_1_1SearchesJob.html", null ],
          [ "Digikam::TagsJob", "classDigikam_1_1TagsJob.html", null ]
        ] ],
        [ "Digikam::IOJob", "classDigikam_1_1IOJob.html", [
          [ "Digikam::CopyOrMoveJob", "classDigikam_1_1CopyOrMoveJob.html", null ],
          [ "Digikam::DeleteDTrashItemsJob", "classDigikam_1_1DeleteDTrashItemsJob.html", null ],
          [ "Digikam::DeleteJob", "classDigikam_1_1DeleteJob.html", null ],
          [ "Digikam::DTrashItemsListingJob", "classDigikam_1_1DTrashItemsListingJob.html", null ],
          [ "Digikam::RenameFileJob", "classDigikam_1_1RenameFileJob.html", null ],
          [ "Digikam::RestoreDTrashItemsJob", "classDigikam_1_1RestoreDTrashItemsJob.html", null ]
        ] ],
        [ "DigikamGenericSendByMailPlugin::ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html", null ]
      ] ],
      [ "Digikam::AkonadiIface", "classDigikam_1_1AkonadiIface.html", null ],
      [ "Digikam::AlbumManager", "classDigikam_1_1AlbumManager.html", null ],
      [ "Digikam::AlbumModelDragDropHandler", "classDigikam_1_1AlbumModelDragDropHandler.html", [
        [ "Digikam::AlbumDragDropHandler", "classDigikam_1_1AlbumDragDropHandler.html", null ],
        [ "Digikam::TagDragDropHandler", "classDigikam_1_1TagDragDropHandler.html", null ]
      ] ],
      [ "Digikam::ApplicationSettings", "classDigikam_1_1ApplicationSettings.html", null ],
      [ "Digikam::BatchTool", null, null ],
      [ "Digikam::BdEngineBackend", "classDigikam_1_1BdEngineBackend.html", [
        [ "Digikam::CoreDbBackend", "classDigikam_1_1CoreDbBackend.html", null ],
        [ "Digikam::FaceDbBackend", "classDigikam_1_1FaceDbBackend.html", null ],
        [ "Digikam::SimilarityDbBackend", "classDigikam_1_1SimilarityDbBackend.html", null ],
        [ "Digikam::ThumbsDbBackend", "classDigikam_1_1ThumbsDbBackend.html", null ]
      ] ],
      [ "Digikam::ClassicLoadingCacheFileWatch", "classDigikam_1_1ClassicLoadingCacheFileWatch.html", null ],
      [ "Digikam::CollectionManager", "classDigikam_1_1CollectionManager.html", null ],
      [ "Digikam::CollectionScanner", "classDigikam_1_1CollectionScanner.html", null ],
      [ "Digikam::CoreDbCopyManager", "classDigikam_1_1CoreDbCopyManager.html", null ],
      [ "Digikam::DAboutData", "classDigikam_1_1DAboutData.html", null ],
      [ "Digikam::DbEngineErrorHandler", "classDigikam_1_1DbEngineErrorHandler.html", null ],
      [ "Digikam::DbHeaderListItem", "classDigikam_1_1DbHeaderListItem.html", null ],
      [ "Digikam::DBinaryIface", "classDigikam_1_1DBinaryIface.html", [
        [ "Digikam::MysqlInitBinary", "classDigikam_1_1MysqlInitBinary.html", null ],
        [ "Digikam::MysqlServBinary", "classDigikam_1_1MysqlServBinary.html", null ],
        [ "Digikam::OutlookBinary", "classDigikam_1_1OutlookBinary.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::AlignBinary", "classDigikamGenericExpoBlendingPlugin_1_1AlignBinary.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::EnfuseBinary", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseBinary.html", null ],
        [ "DigikamGenericJAlbumPlugin::JalbumJar", "classDigikamGenericJAlbumPlugin_1_1JalbumJar.html", null ],
        [ "DigikamGenericJAlbumPlugin::JalbumJava", "classDigikamGenericJAlbumPlugin_1_1JalbumJava.html", null ],
        [ "DigikamGenericPanoramaPlugin::AutoOptimiserBinary", "classDigikamGenericPanoramaPlugin_1_1AutoOptimiserBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::CPCleanBinary", "classDigikamGenericPanoramaPlugin_1_1CPCleanBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::CPFindBinary", "classDigikamGenericPanoramaPlugin_1_1CPFindBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::EnblendBinary", "classDigikamGenericPanoramaPlugin_1_1EnblendBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::HuginExecutorBinary", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::MakeBinary", "classDigikamGenericPanoramaPlugin_1_1MakeBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::NonaBinary", "classDigikamGenericPanoramaPlugin_1_1NonaBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoModifyBinary", "classDigikamGenericPanoramaPlugin_1_1PanoModifyBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::Pto2MkBinary", "classDigikamGenericPanoramaPlugin_1_1Pto2MkBinary.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::GimpBinary", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::BalsaBinary", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::ClawsMailBinary", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::EvolutionBinary", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::KmailBinary", "classDigikamGenericSendByMailPlugin_1_1KmailBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::NetscapeBinary", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::SylpheedBinary", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::ThunderbirdBinary", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary.html", null ]
      ] ],
      [ "Digikam::DBJobsManager", "classDigikam_1_1DBJobsManager.html", null ],
      [ "Digikam::DInfoInterface", "classDigikam_1_1DInfoInterface.html", null ],
      [ "Digikam::DIO", "classDigikam_1_1DIO.html", null ],
      [ "Digikam::DKCamera", "classDigikam_1_1DKCamera.html", [
        [ "Digikam::UMSCamera", "classDigikam_1_1UMSCamera.html", null ]
      ] ],
      [ "Digikam::DPlugin", null, null ],
      [ "Digikam::DPluginLoader", "classDigikam_1_1DPluginLoader.html", null ],
      [ "Digikam::DRawDecoder", "classDigikam_1_1DRawDecoder.html", null ],
      [ "Digikam::DWItemDelegatePrivate", "classDigikam_1_1DWItemDelegatePrivate.html", null ],
      [ "Digikam::DynamicThread", null, null ],
      [ "Digikam::EditorCore", "classDigikam_1_1EditorCore.html", null ],
      [ "Digikam::FaceGroup", "classDigikam_1_1FaceGroup.html", null ],
      [ "Digikam::FacePipeline", "classDigikam_1_1FacePipeline.html", null ],
      [ "Digikam::FileActionMngr", "classDigikam_1_1FileActionMngr.html", null ],
      [ "Digikam::FileActionProgressItemContainer", "classDigikam_1_1FileActionProgressItemContainer.html", null ],
      [ "Digikam::GeoDragDropHandler", "classDigikam_1_1GeoDragDropHandler.html", [
        [ "Digikam::MapDragDropHandler", "classDigikam_1_1MapDragDropHandler.html", null ]
      ] ],
      [ "Digikam::GeoModelHelper", "classDigikam_1_1GeoModelHelper.html", null ],
      [ "Digikam::GPSModelIndexProxyMapper", "classDigikam_1_1GPSModelIndexProxyMapper.html", null ],
      [ "Digikam::IOJobsManager", "classDigikam_1_1IOJobsManager.html", null ],
      [ "Digikam::ItemAttributesWatch", "classDigikam_1_1ItemAttributesWatch.html", null ],
      [ "Digikam::ItemDelegateOverlay", "classDigikam_1_1ItemDelegateOverlay.html", [
        [ "Digikam::AbstractWidgetDelegateOverlay", "classDigikam_1_1AbstractWidgetDelegateOverlay.html", [
          [ "Digikam::GroupIndicatorOverlay", "classDigikam_1_1GroupIndicatorOverlay.html", null ],
          [ "Digikam::HoverButtonDelegateOverlay", "classDigikam_1_1HoverButtonDelegateOverlay.html", [
            [ "Digikam::FaceRejectionOverlay", "classDigikam_1_1FaceRejectionOverlay.html", null ],
            [ "Digikam::ImportRotateOverlay", "classDigikam_1_1ImportRotateOverlay.html", null ],
            [ "Digikam::ItemFullScreenOverlay", "classDigikam_1_1ItemFullScreenOverlay.html", null ],
            [ "Digikam::ItemRotateOverlay", "classDigikam_1_1ItemRotateOverlay.html", null ],
            [ "Digikam::ItemSelectionOverlay", "classDigikam_1_1ItemSelectionOverlay.html", null ]
          ] ],
          [ "Digikam::ImportCoordinatesOverlay", "classDigikam_1_1ImportCoordinatesOverlay.html", null ],
          [ "Digikam::ImportDownloadOverlay", "classDigikam_1_1ImportDownloadOverlay.html", null ],
          [ "Digikam::ImportLockOverlay", "classDigikam_1_1ImportLockOverlay.html", null ],
          [ "Digikam::ImportRatingOverlay", "classDigikam_1_1ImportRatingOverlay.html", null ],
          [ "Digikam::ItemCoordinatesOverlay", "classDigikam_1_1ItemCoordinatesOverlay.html", null ],
          [ "Digikam::ItemRatingOverlay", "classDigikam_1_1ItemRatingOverlay.html", null ],
          [ "Digikam::TagsLineEditOverlay", "classDigikam_1_1TagsLineEditOverlay.html", null ],
          [ "ShowFoto::ShowfotoCoordinatesOverlay", "classShowFoto_1_1ShowfotoCoordinatesOverlay.html", null ]
        ] ]
      ] ],
      [ "Digikam::ItemInfoCache", "classDigikam_1_1ItemInfoCache.html", null ],
      [ "Digikam::ItemListDragDropHandler", "classDigikam_1_1ItemListDragDropHandler.html", [
        [ "Digikam::GPSItemListDragDropHandler", "classDigikam_1_1GPSItemListDragDropHandler.html", null ]
      ] ],
      [ "Digikam::ItemViewUtilities", "classDigikam_1_1ItemViewUtilities.html", null ],
      [ "Digikam::ItemVisibilityController", null, null ],
      [ "Digikam::ItemVisibilityControllerPropertyObject", "classDigikam_1_1ItemVisibilityControllerPropertyObject.html", [
        [ "Digikam::AnimatedVisibility", "classDigikam_1_1AnimatedVisibility.html", null ]
      ] ],
      [ "Digikam::LookupAltitude", "classDigikam_1_1LookupAltitude.html", null ],
      [ "Digikam::MapBackend", "classDigikam_1_1MapBackend.html", null ],
      [ "Digikam::MdKeyListViewItem", "classDigikam_1_1MdKeyListViewItem.html", null ],
      [ "Digikam::ParallelPipes", "classDigikam_1_1ParallelPipes.html", null ],
      [ "Digikam::PrivateProgressItemCreator", "classDigikam_1_1PrivateProgressItemCreator.html", null ],
      [ "Digikam::ProgressItem", null, null ],
      [ "Digikam::RGBackend", "classDigikam_1_1RGBackend.html", null ],
      [ "Digikam::Rule", null, null ],
      [ "Digikam::SearchField", "classDigikam_1_1SearchField.html", null ],
      [ "Digikam::TableViewColumn", "classDigikam_1_1TableViewColumn.html", [
        [ "Digikam::TableViewColumns::ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnThumbnail", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail.html", null ]
      ] ],
      [ "Digikam::TableViewColumnFactory", "classDigikam_1_1TableViewColumnFactory.html", null ],
      [ "Digikam::ThemeManager", "classDigikam_1_1ThemeManager.html", null ],
      [ "Digikam::ThumbnailImageCatcher", "classDigikam_1_1ThumbnailImageCatcher.html", null ],
      [ "Digikam::Token", "classDigikam_1_1Token.html", null ],
      [ "Digikam::WorkerObject", null, null ],
      [ "Digikam::WSSettings", "classDigikam_1_1WSSettings.html", null ],
      [ "DigikamEditorHotPixelsToolPlugin::BlackFrameListViewItem", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListViewItem.html", null ],
      [ "DigikamEditorHotPixelsToolPlugin::BlackFrameParser", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameParser.html", null ],
      [ "DigikamGenericDebianScreenshotsPlugin::DSTalker", "classDigikamGenericDebianScreenshotsPlugin_1_1DSTalker.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::KmlExport", "classDigikamGenericGeolocationEditPlugin_1_1KmlExport.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationMngr", "classDigikamGenericPresentationPlugin_1_1PresentationMngr.html", null ],
      [ "DigikamGenericRajcePlugin::RajceCommand", null, null ],
      [ "DigikamGenericUnifiedPlugin::WSTalker", "classDigikamGenericUnifiedPlugin_1_1WSTalker.html", null ]
    ] ],
    [ "QOpenGLTexture", null, [
      [ "DigikamGenericGLViewerPlugin::GLViewerTexture", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html", null ]
    ] ],
    [ "QOpenGLWidget", null, [
      [ "DigikamGenericGLViewerPlugin::GLViewerWidget", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationKB", "classDigikamGenericPresentationPlugin_1_1PresentationKB.html", null ]
    ] ],
    [ "QPushButton", null, [
      [ "Digikam::CtrlButton", "classDigikam_1_1CtrlButton.html", null ],
      [ "Digikam::DetByClockPhotoButton", "classDigikam_1_1DetByClockPhotoButton.html", null ],
      [ "Digikam::DMultiTabBarButton", "classDigikam_1_1DMultiTabBarButton.html", null ]
    ] ],
    [ "QReadLocker", null, [
      [ "Digikam::ItemInfoReadLocker", "classDigikam_1_1ItemInfoReadLocker.html", null ]
    ] ],
    [ "QRunnable", null, [
      [ "Digikam::ActionJob", "classDigikam_1_1ActionJob.html", null ],
      [ "Digikam::DynamicThread", null, null ]
    ] ],
    [ "QScrollArea", null, [
      [ "Digikam::DExpanderBox", null, null ],
      [ "Digikam::ICCPreviewWidget", "classDigikam_1_1ICCPreviewWidget.html", null ],
      [ "Digikam::TransactionItemView", "classDigikam_1_1TransactionItemView.html", null ]
    ] ],
    [ "QSharedData", null, [
      [ "Digikam::FacePipelineExtendedPackage", "classDigikam_1_1FacePipelineExtendedPackage.html", null ],
      [ "Digikam::GeoIfaceSharedData", "classDigikam_1_1GeoIfaceSharedData.html", null ],
      [ "Digikam::ItemHistoryGraphData", "classDigikam_1_1ItemHistoryGraphData.html", null ],
      [ "Digikam::TwoProgressItemsContainer", "classDigikam_1_1TwoProgressItemsContainer.html", [
        [ "Digikam::FileActionProgressItemContainer", "classDigikam_1_1FileActionProgressItemContainer.html", null ]
      ] ]
    ] ],
    [ "QSortFilterProxyModel", null, [
      [ "Digikam::AddBookmarkProxyModel", "classDigikam_1_1AddBookmarkProxyModel.html", null ],
      [ "Digikam::AlbumFilterModel", "classDigikam_1_1AlbumFilterModel.html", [
        [ "Digikam::CheckableAlbumFilterModel", "classDigikam_1_1CheckableAlbumFilterModel.html", [
          [ "Digikam::SearchFilterModel", "classDigikam_1_1SearchFilterModel.html", null ],
          [ "Digikam::TagPropertiesFilterModel", "classDigikam_1_1TagPropertiesFilterModel.html", [
            [ "Digikam::TagsManagerFilterModel", "classDigikam_1_1TagsManagerFilterModel.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::DCategorizedSortFilterProxyModel", "classDigikam_1_1DCategorizedSortFilterProxyModel.html", null ],
      [ "Digikam::TreeProxyModel", "classDigikam_1_1TreeProxyModel.html", null ]
    ] ],
    [ "QSqlQuery", null, [
      [ "Digikam::DbEngineSqlQuery", "classDigikam_1_1DbEngineSqlQuery.html", null ]
    ] ],
    [ "QStackedWidget", null, [
      [ "Digikam::DConfigDlgStackedWidget", "classDigikam_1_1DConfigDlgStackedWidget.html", null ]
    ] ],
    [ "QStandardItemModel", null, [
      [ "Digikam::CategorizedItemModel", "classDigikam_1_1CategorizedItemModel.html", [
        [ "Digikam::ActionItemModel", "classDigikam_1_1ActionItemModel.html", null ]
      ] ]
    ] ],
    [ "QStyledItemDelegate", null, [
      [ "Digikam::AlbumTreeViewDelegate", "classDigikam_1_1AlbumTreeViewDelegate.html", null ],
      [ "Digikam::ItemFiltersHistoryItemDelegate", "classDigikam_1_1ItemFiltersHistoryItemDelegate.html", null ],
      [ "Digikam::ThumbnailAligningDelegate", "classDigikam_1_1ThumbnailAligningDelegate.html", null ],
      [ "DigikamGenericDebianScreenshotsPlugin::DSPackageDelegate", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPackageDelegate.html", null ]
    ] ],
    [ "QSyntaxHighlighter", null, [
      [ "Digikam::Highlighter", "classDigikam_1_1Highlighter.html", null ]
    ] ],
    [ "QTabBar", null, [
      [ "Digikam::QueuePoolBar", "classDigikam_1_1QueuePoolBar.html", null ]
    ] ],
    [ "QTabWidget", null, [
      [ "Digikam::QueuePool", "classDigikam_1_1QueuePool.html", null ]
    ] ],
    [ "QTemporaryFile", null, [
      [ "Digikam::SafeTemporaryFile", "classDigikam_1_1SafeTemporaryFile.html", null ]
    ] ],
    [ "QTextBrowser", null, [
      [ "Digikam::DTextBrowser", "classDigikam_1_1DTextBrowser.html", null ]
    ] ],
    [ "QThread", null, [
      [ "Digikam::ActionThreadBase", null, null ],
      [ "Digikam::DBusyThread", "classDigikam_1_1DBusyThread.html", null ],
      [ "Digikam::ScanController", "classDigikam_1_1ScanController.html", null ],
      [ "Digikam::TrackCorrelatorThread", "classDigikam_1_1TrackCorrelatorThread.html", null ]
    ] ],
    [ "QTreeView", null, [
      [ "Digikam::AbstractAlbumTreeView", "classDigikam_1_1AbstractAlbumTreeView.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgTreeView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTreeView.html", null ],
      [ "Digikam::SetupCollectionTreeView", "classDigikam_1_1SetupCollectionTreeView.html", null ],
      [ "Digikam::TagMngrListView", "classDigikam_1_1TagMngrListView.html", null ]
    ] ],
    [ "QTreeWidget", null, [
      [ "Digikam::AssignedListView", "classDigikam_1_1AssignedListView.html", null ],
      [ "Digikam::DbKeySelector", "classDigikam_1_1DbKeySelector.html", null ],
      [ "Digikam::DHistoryView", "classDigikam_1_1DHistoryView.html", null ],
      [ "Digikam::DItemsListView", "classDigikam_1_1DItemsListView.html", null ],
      [ "Digikam::DPluginConfView", null, null ],
      [ "Digikam::MetadataListView", "classDigikam_1_1MetadataListView.html", null ],
      [ "Digikam::MetadataSelector", "classDigikam_1_1MetadataSelector.html", null ],
      [ "Digikam::TemplateList", "classDigikam_1_1TemplateList.html", null ],
      [ "Digikam::ToolsListView", "classDigikam_1_1ToolsListView.html", null ],
      [ "Digikam::WorkflowList", "classDigikam_1_1WorkflowList.html", null ],
      [ "DigikamEditorHotPixelsToolPlugin::BlackFrameListView", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListView.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::BracketStackList", "classDigikamGenericExpoBlendingPlugin_1_1BracketStackList.html", null ]
    ] ],
    [ "QTreeWidgetItem", null, [
      [ "Digikam::AssignedListViewItem", "classDigikam_1_1AssignedListViewItem.html", null ],
      [ "Digikam::DbHeaderListItem", "classDigikam_1_1DbHeaderListItem.html", null ],
      [ "Digikam::DbKeySelectorItem", "classDigikam_1_1DbKeySelectorItem.html", null ],
      [ "Digikam::DItemsListViewItem", null, null ],
      [ "Digikam::MdKeyListViewItem", "classDigikam_1_1MdKeyListViewItem.html", null ],
      [ "Digikam::MetadataListViewItem", "classDigikam_1_1MetadataListViewItem.html", null ],
      [ "Digikam::MetadataSelectorItem", "classDigikam_1_1MetadataSelectorItem.html", null ],
      [ "Digikam::TemplateListItem", "classDigikam_1_1TemplateListItem.html", null ],
      [ "Digikam::ToolListViewGroup", "classDigikam_1_1ToolListViewGroup.html", null ],
      [ "Digikam::ToolListViewItem", "classDigikam_1_1ToolListViewItem.html", null ],
      [ "Digikam::WorkflowItem", "classDigikam_1_1WorkflowItem.html", null ],
      [ "DigikamEditorHotPixelsToolPlugin::BlackFrameListViewItem", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListViewItem.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::BracketStackItem", "classDigikamGenericExpoBlendingPlugin_1_1BracketStackItem.html", null ]
    ] ],
    [ "QUndoCommand", null, [
      [ "Digikam::GPSUndoCommand", "classDigikam_1_1GPSUndoCommand.html", null ],
      [ "Digikam::RemoveBookmarksCommand", "classDigikam_1_1RemoveBookmarksCommand.html", [
        [ "Digikam::InsertBookmarksCommand", "classDigikam_1_1InsertBookmarksCommand.html", null ]
      ] ]
    ] ],
    [ "QUrl", null, [
      [ "Digikam::CoreDbUrl", "classDigikam_1_1CoreDbUrl.html", null ]
    ] ],
    [ "QValidator", null, [
      [ "Digikam::DatePickerValidator", "classDigikam_1_1DatePickerValidator.html", null ]
    ] ],
    [ "QVector", null, [
      [ "Digikam::SparseModelIndexVector", "classDigikam_1_1SparseModelIndexVector.html", null ]
    ] ],
    [ "QWebEnginePage", null, [
      [ "Digikam::HTMLWidgetPage", "classDigikam_1_1HTMLWidgetPage.html", null ]
    ] ],
    [ "QWebPage", null, [
      [ "DigikamGenericUnifiedPlugin::WSAuthenticationPage", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage.html", null ]
    ] ],
    [ "QWebView", null, [
      [ "Digikam::WelcomePageView", "classDigikam_1_1WelcomePageView.html", null ],
      [ "DigikamGenericUnifiedPlugin::WSAuthenticationPageView", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "Digikam::AbstractSearchGroupContainer", "classDigikam_1_1AbstractSearchGroupContainer.html", [
        [ "Digikam::SearchGroup", "classDigikam_1_1SearchGroup.html", null ]
      ] ],
      [ "Digikam::DAbstractSliderSpinBox", "classDigikam_1_1DAbstractSliderSpinBox.html", [
        [ "Digikam::DDoubleSliderSpinBox", "classDigikam_1_1DDoubleSliderSpinBox.html", null ],
        [ "Digikam::DSliderSpinBox", "classDigikam_1_1DSliderSpinBox.html", null ]
      ] ],
      [ "Digikam::DArrowClickLabel", "classDigikam_1_1DArrowClickLabel.html", null ],
      [ "Digikam::DConfigDlgTitle", "classDigikam_1_1DConfigDlgTitle.html", null ],
      [ "Digikam::DConfigDlgView", "classDigikam_1_1DConfigDlgView.html", [
        [ "Digikam::DConfigDlgWdg", "classDigikam_1_1DConfigDlgWdg.html", null ]
      ] ],
      [ "Digikam::DDateTable", "classDigikam_1_1DDateTable.html", null ],
      [ "Digikam::DItemsList", null, null ],
      [ "Digikam::DMultiTabBar", null, null ],
      [ "Digikam::MetadataWidget", null, null ],
      [ "Digikam::RatingWidget", null, null ],
      [ "Digikam::SearchFieldGroup", "classDigikam_1_1SearchFieldGroup.html", null ],
      [ "Digikam::SearchFieldGroupLabel", "classDigikam_1_1SearchFieldGroupLabel.html", null ],
      [ "Digikam::SearchViewBottomBar", "classDigikam_1_1SearchViewBottomBar.html", null ],
      [ "Digikam::SidebarWidget", "classDigikam_1_1SidebarWidget.html", null ],
      [ "Digikam::SlideEnd", "classDigikam_1_1SlideEnd.html", null ],
      [ "Digikam::StyleSheetDebugger", "classDigikam_1_1StyleSheetDebugger.html", null ],
      [ "Digikam::SubjectWidget", null, null ],
      [ "Digikam::TableViewColumnConfigurationWidget", "classDigikam_1_1TableViewColumnConfigurationWidget.html", [
        [ "Digikam::TableViewColumns::ColumnFileConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget.html", null ],
        [ "Digikam::TableViewColumns::ColumnGeoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget.html", null ],
        [ "Digikam::TableViewColumns::ColumnPhotoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget.html", null ]
      ] ],
      [ "Digikam::WSSettingsWidget", null, null ],
      [ "DigikamGenericCalendarPlugin::CalWidget", "classDigikamGenericCalendarPlugin_1_1CalWidget.html", null ],
      [ "DigikamGenericDebianScreenshotsPlugin::DSWidget", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWidget.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::KmlWidget", "classDigikamGenericGeolocationEditPlugin_1_1KmlWidget.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html", null ],
      [ "DigikamGenericSmugPlugin::SmugWidget", "classDigikamGenericSmugPlugin_1_1SmugWidget.html", null ]
    ] ],
    [ "QWizard", null, [
      [ "Digikam::DWizardDlg", "classDigikam_1_1DWizardDlg.html", null ]
    ] ],
    [ "QWizardPage", null, [
      [ "Digikam::DWizardPage", null, null ]
    ] ],
    [ "QWriteLocker", null, [
      [ "Digikam::ItemInfoWriteLocker", "classDigikam_1_1ItemInfoWriteLocker.html", null ]
    ] ],
    [ "QXmlStreamReader", null, [
      [ "Digikam::SearchXmlReader", "classDigikam_1_1SearchXmlReader.html", [
        [ "Digikam::KeywordSearchReader", "classDigikam_1_1KeywordSearchReader.html", null ],
        [ "Digikam::SearchXmlCachingReader", "classDigikam_1_1SearchXmlCachingReader.html", null ]
      ] ],
      [ "Digikam::XbelReader", "classDigikam_1_1XbelReader.html", null ]
    ] ],
    [ "QXmlStreamWriter", null, [
      [ "Digikam::SearchXmlWriter", "classDigikam_1_1SearchXmlWriter.html", [
        [ "Digikam::KeywordSearchWriter", "classDigikam_1_1KeywordSearchWriter.html", null ]
      ] ],
      [ "Digikam::XbelWriter", "classDigikam_1_1XbelWriter.html", null ]
    ] ],
    [ "ref_pic_set", "classref__pic__set.html", null ],
    [ "sao_info", "structsao__info.html", null ],
    [ "scaling_list_data", "structscaling__list__data.html", null ],
    [ "scan_position", "structscan__position.html", null ],
    [ "sei_decoded_picture_hash", "structsei__decoded__picture__hash.html", null ],
    [ "sei_message", "structsei__message.html", null ],
    [ "seq_parameter_set", "classseq__parameter__set.html", null ],
    [ "ShowFoto::ItemViewShowfotoDelegatePrivate", "classShowFoto_1_1ItemViewShowfotoDelegatePrivate.html", null ],
    [ "ShowFoto::ShowfotoItemInfo", "classShowFoto_1_1ShowfotoItemInfo.html", null ],
    [ "ShowFoto::ShowfotoItemSortSettings", "classShowFoto_1_1ShowfotoItemSortSettings.html", null ],
    [ "ShowfotoDelegatePrivate", null, [
      [ "ShowFoto::ShowfotoNormalDelegatePrivate", "classShowFoto_1_1ShowfotoNormalDelegatePrivate.html", null ],
      [ "ShowFoto::ShowfotoThumbnailDelegatePrivate", "classShowFoto_1_1ShowfotoThumbnailDelegatePrivate.html", null ]
    ] ],
    [ "ShowfotoDelegatePrivate public ItemViewShowfotoDelegatePrivate", null, [
      [ "ShowFoto::ShowfotoDelegate", "classShowFoto_1_1ShowfotoDelegate.html", null ]
    ] ],
    [ "slice_segment_header", "classslice__segment__header.html", null ],
    [ "slice_unit", "classslice__unit.html", null ],
    [ "small_image_buffer", "classsmall__image__buffer.html", null ],
    [ "sop_creator_trivial_low_delay::params", "structsop__creator__trivial__low__delay_1_1params.html", null ],
    [ "sps_range_extension", "classsps__range__extension.html", null ],
    [ "enable_shared_from_this", null, [
      [ "heif::HeifPixelImage", "classheif_1_1HeifPixelImage.html", null ]
    ] ],
    [ "thread_context", "classthread__context.html", null ],
    [ "thread_pool", "classthread__pool.html", null ],
    [ "thread_task", "classthread__task.html", [
      [ "thread_task_ctb_row", "classthread__task__ctb__row.html", null ],
      [ "thread_task_slice_segment", "classthread__task__slice__segment.html", null ]
    ] ],
    [ "Job", null, [
      [ "DigikamGenericPanoramaPlugin::PanoTask", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html", [
        [ "DigikamGenericPanoramaPlugin::CommandTask", "classDigikamGenericPanoramaPlugin_1_1CommandTask.html", [
          [ "DigikamGenericPanoramaPlugin::AutoCropTask", "classDigikamGenericPanoramaPlugin_1_1AutoCropTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CompileMKStepTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKStepTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CompileMKTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CpCleanTask", "classDigikamGenericPanoramaPlugin_1_1CpCleanTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CpFindTask", "classDigikamGenericPanoramaPlugin_1_1CpFindTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CreateMKTask", "classDigikamGenericPanoramaPlugin_1_1CreateMKTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::HuginExecutorTask", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::OptimisationTask", "classDigikamGenericPanoramaPlugin_1_1OptimisationTask.html", null ]
        ] ],
        [ "DigikamGenericPanoramaPlugin::CopyFilesTask", "classDigikamGenericPanoramaPlugin_1_1CopyFilesTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreateFinalPtoTask", "classDigikamGenericPanoramaPlugin_1_1CreateFinalPtoTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreatePreviewTask", "classDigikamGenericPanoramaPlugin_1_1CreatePreviewTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreatePtoTask", "classDigikamGenericPanoramaPlugin_1_1CreatePtoTask.html", null ]
      ] ]
    ] ],
    [ "Ui_AdvPrintCaptionPage", "classUi__AdvPrintCaptionPage.html", null ],
    [ "Ui_AdvPrintCropPage", "classUi__AdvPrintCropPage.html", null ],
    [ "Ui_AdvPrintCustomLayout", "classUi__AdvPrintCustomLayout.html", [
      [ "Ui::AdvPrintCustomLayout", null, [
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", null ]
      ] ]
    ] ],
    [ "Ui_AdvPrintPhotoPage", "classUi__AdvPrintPhotoPage.html", null ],
    [ "Ui_CalEvents", "classUi__CalEvents.html", null ],
    [ "Ui_CalProgress", "classUi__CalProgress.html", null ],
    [ "Ui_CalTemplate", "classUi__CalTemplate.html", null ],
    [ "Ui_DateOptionDialogWidget", "classUi__DateOptionDialogWidget.html", null ],
    [ "Ui_FillModifierDialogWidget", "classUi__FillModifierDialogWidget.html", null ],
    [ "Ui_PresentationAdvPage", "classUi__PresentationAdvPage.html", [
      [ "Ui::PresentationAdvPage", null, [
        [ "DigikamGenericPresentationPlugin::PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", null ]
      ] ]
    ] ],
    [ "Ui_PresentationAudioPage", "classUi__PresentationAudioPage.html", null ],
    [ "Ui_PresentationAudioWidget", "classUi__PresentationAudioWidget.html", null ],
    [ "Ui_PresentationCaptionPage", "classUi__PresentationCaptionPage.html", [
      [ "Ui::PresentationCaptionPage", null, [
        [ "DigikamGenericPresentationPlugin::PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", null ]
      ] ]
    ] ],
    [ "Ui_PresentationCtrlWidget", "classUi__PresentationCtrlWidget.html", [
      [ "Ui::PresentationCtrlWidget", null, [
        [ "DigikamGenericPresentationPlugin::PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html", null ]
      ] ]
    ] ],
    [ "Ui_PresentationMainPage", "classUi__PresentationMainPage.html", null ],
    [ "Ui_PrintOptionsPage", "classUi__PrintOptionsPage.html", null ],
    [ "Ui_RangeModifierDialogWidget", "classUi__RangeModifierDialogWidget.html", null ],
    [ "Ui_ReplaceModifierDialogWidget", "classUi__ReplaceModifierDialogWidget.html", null ],
    [ "Ui_SequenceNumberOptionDialogWidget", "classUi__SequenceNumberOptionDialogWidget.html", null ],
    [ "VersionManagerPriv", null, [
      [ "Digikam::VersionManager", null, [
        [ "Digikam::DatabaseVersionManager", "classDigikam_1_1DatabaseVersionManager.html", null ]
      ] ]
    ] ],
    [ "video_parameter_set", "classvideo__parameter__set.html", null ],
    [ "video_usability_information", "classvideo__usability__information.html", null ],
    [ "YFAuth::CCryptoProviderRSA", "classYFAuth_1_1CCryptoProviderRSA.html", null ],
    [ "YFAuth::public_key", "classYFAuth_1_1public__key.html", [
      [ "YFAuth::private_key", "classYFAuth_1_1private__key.html", null ]
    ] ],
    [ "YFAuth::vlong", "classYFAuth_1_1vlong.html", null ],
    [ "A", null, [
      [ "Digikam::ParallelAdapter< A >", "classDigikam_1_1ParallelAdapter.html", null ]
    ] ]
];