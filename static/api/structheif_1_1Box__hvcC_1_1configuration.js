var structheif_1_1Box__hvcC_1_1configuration =
[
    [ "avg_frame_rate", "structheif_1_1Box__hvcC_1_1configuration.html#afb116222fc9f9bde556d0a47226dd835", null ],
    [ "bit_depth_chroma", "structheif_1_1Box__hvcC_1_1configuration.html#a21a26a09c0576d9865b9bfa06b50c450", null ],
    [ "bit_depth_luma", "structheif_1_1Box__hvcC_1_1configuration.html#ad73ea5217d760713562c43531109a852", null ],
    [ "chroma_format", "structheif_1_1Box__hvcC_1_1configuration.html#a32f2c67a66ef19d466684c1d71592bf0", null ],
    [ "configuration_version", "structheif_1_1Box__hvcC_1_1configuration.html#a48f6e0edc701f3aa00c30988d936d426", null ],
    [ "constant_frame_rate", "structheif_1_1Box__hvcC_1_1configuration.html#a6a48aab33812a078973f5822d0c3c261", null ],
    [ "general_constraint_indicator_flags", "structheif_1_1Box__hvcC_1_1configuration.html#a2285b6bbc65c9009ea53585bcb7b6aff", null ],
    [ "general_level_idc", "structheif_1_1Box__hvcC_1_1configuration.html#ab1b209fcc8876b176b64251b42dc5041", null ],
    [ "general_profile_compatibility_flags", "structheif_1_1Box__hvcC_1_1configuration.html#a07d6a817900e40fd66df1ec5772fdaa9", null ],
    [ "general_profile_idc", "structheif_1_1Box__hvcC_1_1configuration.html#a61c4dc723a65da53d8072f707bb86b49", null ],
    [ "general_profile_space", "structheif_1_1Box__hvcC_1_1configuration.html#a69c53f3c8caf6285bfb274402c953cfd", null ],
    [ "general_tier_flag", "structheif_1_1Box__hvcC_1_1configuration.html#a029de5ae534b7f2bfef64e5389fb4614", null ],
    [ "min_spatial_segmentation_idc", "structheif_1_1Box__hvcC_1_1configuration.html#a594a727af2e7d022052bec7d2ac17de5", null ],
    [ "num_temporal_layers", "structheif_1_1Box__hvcC_1_1configuration.html#ae5e4102f4edb3805c03388df9c0af803", null ],
    [ "parallelism_type", "structheif_1_1Box__hvcC_1_1configuration.html#a46c6033cb855f50e3b82829bffe19467", null ],
    [ "temporal_id_nested", "structheif_1_1Box__hvcC_1_1configuration.html#a3ffcea3bbd2e88613853b6b8c39709de", null ]
];