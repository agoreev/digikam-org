var classDigikam_1_1ICCSettingsContainer =
[
    [ "BehaviorEnum", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496", [
      [ "InvalidBehavior", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496afc9b40c4fe76325cd2cd65c700ac482d", null ],
      [ "UseEmbeddedProfile", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ab8c30bac71a7c486bb448f9f5f4b4f72", null ],
      [ "UseSRGB", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ade382ca3cbd4a593887be3dafdab62f9", null ],
      [ "UseWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a7c03cd95a4ee10eee54810461d057f9e", null ],
      [ "UseDefaultInputProfile", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a73526ae50a7e7800721ee64dfdc42882", null ],
      [ "UseSpecifiedProfile", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496acffccdbb97130634e9027172dcecd3ee", null ],
      [ "AutomaticColors", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ae9d3702f719cf7ff08d1ad841bcf31ee", null ],
      [ "DoNotInterpret", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496aca6b477a92c3316f6c8fcb56d8111171", null ],
      [ "KeepProfile", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a6ba3defb1ca616dfc5b06b63ab1d202f", null ],
      [ "ConvertToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ad661637321d29e31e7db657396dd0e3c", null ],
      [ "LeaveFileUntagged", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a48d147d853de883b1562e5c917b0f06b", null ],
      [ "AskUser", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a7e36fee574fab9db98a6d920465ff6f4", null ],
      [ "SafestBestAction", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a4a0dfb9ac9ab001838e912404b311b0e", null ],
      [ "PreserveEmbeddedProfile", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a6466f0d6e581501862d56b9ac54d3037", null ],
      [ "EmbeddedToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ace5e649b6bbf4152be6efd68e4718f36", null ],
      [ "SRGBToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a00833a6233c06815833a126a3fa4d623", null ],
      [ "AutoToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a0cef0c27730c6ed9ed0f894a4d7e829e", null ],
      [ "InputToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496acc0e2e4b2336da0629bf5d0e775903b8", null ],
      [ "SpecifiedToWorkspace", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496ad5f9c4704cf7f273f673715ba161ccc8", null ],
      [ "NoColorManagement", "classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a0ac6ef5f9e365cee217ecb4f5a4939c8", null ]
    ] ],
    [ "ICCSettingsContainer", "classDigikam_1_1ICCSettingsContainer.html#ab776ee2f1bdd7f1238f96e73739da867", null ],
    [ "~ICCSettingsContainer", "classDigikam_1_1ICCSettingsContainer.html#a25d672a8562a9f5803e56067a764d194", null ],
    [ "readFromConfig", "classDigikam_1_1ICCSettingsContainer.html#a4a27969d519050bc02ed9ec4c1d3668f", null ],
    [ "writeManagedPreviewsToConfig", "classDigikam_1_1ICCSettingsContainer.html#a89c14a4b072ea8afe74498e8faf18c42", null ],
    [ "writeManagedViewToConfig", "classDigikam_1_1ICCSettingsContainer.html#a22524ee07e3d3ddb23e67fcef63f232b", null ],
    [ "writeToConfig", "classDigikam_1_1ICCSettingsContainer.html#a0107c67811d7d94cd027655852610184", null ],
    [ "defaultInputProfile", "classDigikam_1_1ICCSettingsContainer.html#a81eafa10e5155fe6f7c7fe7f095234c9", null ],
    [ "defaultMismatchBehavior", "classDigikam_1_1ICCSettingsContainer.html#a74ec0bc07250458d08bf038e89fe77e2", null ],
    [ "defaultMissingProfileBehavior", "classDigikam_1_1ICCSettingsContainer.html#a64be789f31776eb565a653913f5a4b32", null ],
    [ "defaultProofProfile", "classDigikam_1_1ICCSettingsContainer.html#abea40884d774ccdc80399cfa9ed0183b", null ],
    [ "defaultUncalibratedBehavior", "classDigikam_1_1ICCSettingsContainer.html#a7760fef23ec5de94cb0afa5ebea15993", null ],
    [ "doGamutCheck", "classDigikam_1_1ICCSettingsContainer.html#a910c26a1f2d9fa15c2dd47f1467229a8", null ],
    [ "enableCM", "classDigikam_1_1ICCSettingsContainer.html#af8a1d8acbc6937b08af91691e606efe1", null ],
    [ "gamutCheckMaskColor", "classDigikam_1_1ICCSettingsContainer.html#a85519e4d9a0222679c497f3b9f827fcf", null ],
    [ "iccFolder", "classDigikam_1_1ICCSettingsContainer.html#a1c51ea434ffbd536bf0947d90c9b0c94", null ],
    [ "lastMismatchBehavior", "classDigikam_1_1ICCSettingsContainer.html#a0c540362415dba5a1501326900c3b7fe", null ],
    [ "lastMissingProfileBehavior", "classDigikam_1_1ICCSettingsContainer.html#af2c9d8624858b65586743a8c65e26534", null ],
    [ "lastSpecifiedAssignProfile", "classDigikam_1_1ICCSettingsContainer.html#a9e059f446f51070f91346125e86a637c", null ],
    [ "lastSpecifiedInputProfile", "classDigikam_1_1ICCSettingsContainer.html#a9ca4e1925b8c8fcc7bdf5b85f7002a36", null ],
    [ "lastUncalibratedBehavior", "classDigikam_1_1ICCSettingsContainer.html#a35d6dcb93f402725dc32c1cbf40f7758", null ],
    [ "monitorProfile", "classDigikam_1_1ICCSettingsContainer.html#ac44387ceeb089ea8c99b9a85ea662e94", null ],
    [ "proofingRenderingIntent", "classDigikam_1_1ICCSettingsContainer.html#a80672a3acd7f86b596f857c0678119aa", null ],
    [ "renderingIntent", "classDigikam_1_1ICCSettingsContainer.html#a8adab49c1c92430ddd00fa6e8d7d380f", null ],
    [ "useBPC", "classDigikam_1_1ICCSettingsContainer.html#a07c14b7c50679ca3c29fa569919af381", null ],
    [ "useManagedPreviews", "classDigikam_1_1ICCSettingsContainer.html#a316c2e7a2769fc74ba3fd326a5e1d1b1", null ],
    [ "useManagedView", "classDigikam_1_1ICCSettingsContainer.html#a188ac71656e1994701f59c645d259e47", null ],
    [ "workspaceProfile", "classDigikam_1_1ICCSettingsContainer.html#abcc86c4349e496fd02455019f296c1ff", null ]
];