/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "digiKam", "index.html", [
    [ "digiKam API reference page.", "index.html", [
      [ "Source Code Directories", "index.html#sourcedirs", null ],
      [ "External Dependencies", "index.html#externaldeps", [
        [ "Dependencies To Checkout All Source Code", "index.html#depscheckout", null ],
        [ "Dependencies To Process Translations Files (optional)", "index.html#depstrans", null ],
        [ "Dependencies To Compile And Link Source Code", "index.html#depscomplink", null ]
      ] ],
      [ "Get Source Code", "index.html#getsourcecode", [
        [ "Software Components", "index.html#softcomponents", null ]
      ] ],
      [ "Development Envirronnement", "index.html#develenv", null ],
      [ "Cmake Configuration Options", "index.html#cmakeoptions", [
        [ "Top Level Configuration", "index.html#topleveloptions", null ],
        [ "Core Configuration", "index.html#coreoptions", null ]
      ] ],
      [ "Setup Local Compilation and Run-Time", "index.html#setuplocaldev", null ],
      [ "Debug Traces At Run-Time", "index.html#debugtraces", [
        [ "Logging Using an Environment Variable", "index.html#Enable", null ],
        [ "Logging Categories in digiKam", "index.html#digiKamloggincat", null ],
        [ "Further Reading", "index.html#moreaboutloggincat", null ]
      ] ],
      [ "Cmake compilation rules", "index.html#cmakecompilrules", [
        [ "Introduction", "index.html#cmakeintro", null ],
        [ "CMake Implementation Details", "index.html#cmakeimpldetails", [
          [ "Include Directories", "index.html#includedirs", null ],
          [ "Shared Libraries", "index.html#sharedlibs", null ],
          [ "Static Libraries", "index.html#staticlibs", null ],
          [ "Object Libraries", "index.html#objlibs", null ]
        ] ]
      ] ],
      [ "Contribute To The Code", "index.html#codecontrib", [
        [ "Starting With Open-Source", "index.html#startopensource", null ],
        [ "Source Code Formatting", "index.html#codeformat", [
          [ "Indentation length", "index.html#indentlength", null ],
          [ "Tabs vs Spaces", "index.html#tabsspaces", null ],
          [ "Line length", "index.html#linelength", null ],
          [ "Bracketing", "index.html#bracketing", null ],
          [ "Positioning of Access modifiers", "index.html#accessmod", null ]
        ] ],
        [ "Class, file and Variable names", "index.html#itemsnames", [
          [ "Class and filenames", "index.html#classfilenames", null ],
          [ "Protected Member variables", "index.html#protectvars", null ],
          [ "Non-Member variables", "index.html#nomembvars", null ],
          [ "Private Member variables", "index.html#privmembvars", null ]
        ] ],
        [ "Comments and Whitespace", "index.html#commentsspaces", null ],
        [ "Header Files", "index.html#headerfiles", null ],
        [ "Automatic source code formatting", "index.html#autocodeformat", null ],
        [ "General recommendations", "index.html#generalrecommend", null ],
        [ "GDB Backtrace", "index.html#gdbbacktrace", null ],
        [ "Memory Leak", "index.html#memleak", null ],
        [ "Profiling With Cachegrind", "index.html#profilingcode", null ],
        [ "Unit Testing / Automated Testing", "index.html#unittests", null ],
        [ "Checking For Corrupt Qt Signal Slot Connection", "index.html#corruptedsignalslot", null ],
        [ "Finding Duplicated Code", "index.html#dupcodes", null ],
        [ "API Documentation Validation, User Documentation Validation, Source Code Checking", "index.html#docvalidcodecheck", null ],
        [ "Usability Issues", "index.html#appusability", null ],
        [ "Generate API Documentation", "index.html#apidoc", null ],
        [ "Speed Up The Code-Compile-Test Cycle", "index.html#speedupcompil", null ],
        [ "Working With Branches From Git Repository", "index.html#gitbranches", null ]
      ] ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", "namespacemembers_eval" ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", "functions_enum" ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classAlgo__TB__IntraPredMode__BruteForce.html#ace022ffaf8d88aba411ee1b869fe6083",
"classCodingOption.html#a228353f47facd3f1be2f57b13d913cdf",
"classDigikamBqmAssignTemplatePlugin_1_1AssignTemplate.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50",
"classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrection.html#af339b08111d419eea4a8515c45f3accc",
"classDigikamBqmBlurPlugin_1_1Blur.html#acc1ce64746976144f211e2d537d3b851",
"classDigikamBqmChannelMixerPlugin_1_1ChannelMixer.html#abaf9a8b885ffb8d7b690136312bc6968",
"classDigikamBqmColorFXPlugin_1_1ColorFX.html#aad228c7e8dcf09d545ae0b8d8a579f06",
"classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a36d8541d1b9a820534e87902b54d088c",
"classDigikamBqmConvertToHeifPlugin_1_1ConvertToHEIF.html#a78786c22c95ece3d7ad4abd8b1e7b6d3",
"classDigikamBqmConvertToJpegPlugin_1_1ConvertToJPEG.html#a65aeadc8181da5b5af721d8439f51b24",
"classDigikamBqmConvertToPngPlugin_1_1ConvertToPNG.html#a4337c17e1bc828dc77eb8dba74099db0",
"classDigikamBqmCropPlugin_1_1CropPlugin.html#a634d1026d4b7abcb1d131c34cfa4aca3",
"classDigikamBqmFilmGrainPlugin_1_1FilmGrainPlugin.html#a2acc10db740d60d639b0963d8a73d1d1",
"classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrection.html#afd910fab457aa527e72634f2de834c48",
"classDigikamBqmInvertPlugin_1_1Invert.html#afa76b46ac346747b289ce17be3124a72a3f77943d1787b72f8a1c3d5a9a04d4db",
"classDigikamBqmNoiseReductionPlugin_1_1NoiseReduction.html#aa34b534ee277b0d6af52d0821423b5ee",
"classDigikamBqmRestorationPlugin_1_1RedEyeCorrection.html#a26545d325022d41008516556f7933548",
"classDigikamBqmSharpenPlugin_1_1SharpenPlugin.html",
"classDigikamBqmWhiteBalancePlugin_1_1WhiteBalance.html#a561e9e51ca9cadf1fecdbf147b5e0588",
"classDigikamEditorAutoCropToolPlugin_1_1AutoCropToolPlugin.html#a7aa1f0e7d5365c77f9dd44b7b0dd42da",
"classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a4e3fc04cabcb7258c21a1158573283a9",
"classDigikamEditorFilmToolPlugin_1_1FilmToolPlugin.html#a2a64b992e50d15b4c67f6112b6bcaef8",
"classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b",
"classDigikamEditorOilPaintToolPlugin_1_1OilPaintToolPlugin.html#a3895819eddf64a8800a5b0f155f114b1",
"classDigikamEditorResizeToolPlugin_1_1ResizeToolPlugin.html#a3a5ca08740cf279b1f73520bc83d1b41",
"classDigikamGenericBoxPlugin_1_1BOXWidget.html#a75e4ab684dbb658d4055a4ccec14cb63",
"classDigikamGenericDebianScreenshotsPlugin_1_1DSPlugin.html#a0806aedffe128e70253d910c1dbf5690",
"classDigikamGenericExpoBlendingPlugin_1_1AlignBinary.html#ae169774080fb936c05f8006b1494e7b8",
"classDigikamGenericFaceBookPlugin_1_1FbWidget.html#a189360439d05f6a941df655f29969b51",
"classDigikamGenericFlickrPlugin_1_1FlickrWidget.html#a06a0369ba37c96f97fd196f71bcb2c2e",
"classDigikamGenericGeolocationEditPlugin_1_1KmlExport.html#a695b5f8ef92f955517aac3322dd1fb54",
"classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig.html#abe1db2ae58bcbc238984c0def943e410",
"classDigikamGenericImageShackPlugin_1_1ImageShackPlugin.html",
"classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html#aba343e2651f8c481194f6c04ed362dc7",
"classDigikamGenericJAlbumPlugin_1_1JalbumJar.html#a6c575297ed4f440a536d791816324245",
"classDigikamGenericMetadataEditPlugin_1_1XMPSubjects.html#a29d25f5b98689ee6bf80c84dcba4ad08",
"classDigikamGenericPanoramaPlugin_1_1AutoOptimiserBinary.html#afd2b50336c88126d81a454d046427092",
"classDigikamGenericPanoramaPlugin_1_1EnblendBinary.html",
"classDigikamGenericPanoramaPlugin_1_1PanoModifyBinary.html#a25280b609f966f445c6d62ea2588ef34",
"classDigikamGenericPinterestPlugin_1_1PWidget.html#ab918fa166e8473000f9ad8212fa8ac83",
"classDigikamGenericPresentationPlugin_1_1PresentationKB.html#a9e0c7d16f4ea60c07568e35ca0abbe34",
"classDigikamGenericPrintCreatorPlugin_1_1GimpBinary.html#a0d0e3a2a9eb5694c081a3ef1554d04f2",
"classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html",
"classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a2b70a92d0b9c66eb15e8662a70b2ef0b",
"classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html#ac862a28fd5a8feb6a1edb407ba9fd1ce",
"classDigikamGenericTwitterPlugin_1_1TwMPForm.html#a4ccdcb5cd3f1c79e4c01ed76614ebc95",
"classDigikamGenericVKontaktePlugin_1_1VKontaktePlugin.html",
"classDigikamImageMagickDImgPlugin_1_1DImgImageMagickLoader.html#a44deed55b6d423613c1a5ea6faecd32c",
"classDigikamPGFDImgPlugin_1_1DImgPGFPlugin.html#aeefa145c55498e77daf94fbe27c335e4",
"classDigikamRawImportNativePlugin_1_1RawImportNativePlugin.html#a0806aedffe128e70253d910c1dbf5690",
"classDigikam_1_1AbstractAlbumTreeView.html#a99632d84fea5a5bcc23df13f2bca5903",
"classDigikam_1_1AbstractSpecificAlbumModel.html#a71df8e2dcde122d00631f15efa496dedae122f09cde4e957e45f4253ca1c5e468",
"classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaa9db3005d9583671396b9c84fe01ccb30",
"classDigikam_1_1AlbumModel.html#a2179b441a4319355c7d63911b9419b1a",
"classDigikam_1_1AlbumTreeView.html#a6e7b9c3aabb025ec5e988e5064f71945a55452c30a48e220c60eba6cfb12e93a6",
"classDigikam_1_1AntiVignettingFilter.html#ae55525c9a24ff341bd2884c124dc3451",
"classDigikam_1_1ApplicationSettings.html#a9ead25986e8718f03d2e31f6a9ed8519",
"classDigikam_1_1AssignNameWidgetStates.html#adb360bb32c216a7aaa20f299d2ba10e6",
"classDigikam_1_1BWSepiaContainer.html#a21f1935856e3952e3264fd687715bea6aa64a315fa43f82bd79daa6c124b1c282",
"classDigikam_1_1BorderContainer.html#aac72996c0f6294e90b8010d41e2ebf7a",
"classDigikam_1_1CheckableAlbumFilterModel.html#ac2c92bb7c92ad5dd46d7cb8b523d4900",
"classDigikam_1_1CollectionScanner.html#ad33f8da9d4779a5b122b39d27baec0e5",
"classDigikam_1_1CopyOrMoveJob.html#ad801a5f7a879375239c3acc000cb91a4",
"classDigikam_1_1CurvesFilter.html#a12afda8991b5a3f1daf62d88f0e46076",
"classDigikam_1_1DBinaryIface.html#a01ce7630c89c8b5f6d8652a6cf7396bc",
"classDigikam_1_1DConfigDlg.html#ae39fef8bbf4df77733708b00203dc19c",
"classDigikam_1_1DConfigDlgWdgPrivate.html#ab33b40daf486617429a1bf18a8c86edf",
"classDigikam_1_1DImg.html#a065a0040983d91de15c638b8fe17b749",
"classDigikam_1_1DImgLoader.html#a11a0b5effa0f7ebc02c7a74385ba0bfd",
"classDigikam_1_1DImgThreadedFilter.html#a826941afe47c9bc0f51d074c9c58eae3",
"classDigikam_1_1DLNAMediaServer.html#a6abfe70e7387ea7ee743531d80d3743f",
"classDigikam_1_1DMetadata.html#af50fe6b727329edeb6023991baf79a10",
"classDigikam_1_1DPluginAction.html#a8e0bbb45f7dcf8d88f331c5082fecc7f",
"classDigikam_1_1DRawDecoderSettings.html#a8815eca533953ec35875c3934e0d194e",
"classDigikam_1_1DWItemDelegate.html#a6282ad114411160ea51e0e762be36ec5",
"classDigikam_1_1DatabaseWriter.html#a8360c2a5a4bce223ac31f0967e930825",
"classDigikam_1_1DateTreeView.html#aa88373f8e697c9a55fa07a6d5741cd4d",
"classDigikam_1_1DefaultValueModifier.html#ac95510178a03318696fe117067293a30",
"classDigikam_1_1DigikamApp.html#a98d852be32b97e9df0e9195473446d25",
"classDigikam_1_1DigikamItemView.html#a33491535a7fb6d4d8526a785a11ae8f8",
"classDigikam_1_1DragDropModelImplementation.html#a5441a482c8dedebae835b48eb09c7bda",
"classDigikam_1_1EditorWindow.html#a657a75498da5ec3f0865ab185ed618b0",
"classDigikam_1_1EmbossFilter.html#a1678d94b251ed38e13be41c56406282f",
"classDigikam_1_1FaceDb.html#aa60f71ade9ac7c3fa221d82e0cf8cea9",
"classDigikam_1_1FaceItem.html#ab9ba46c45e299d0dc9098b11b52166cd",
"classDigikam_1_1FacePreviewLoader.html#a550c2c5d6d2e5a477ff4bf432eb2ab0fa74e56a9cb639518c7ccd78d08397831e",
"classDigikam_1_1FaceTagsEditor.html#ac896275d0d718526001eb3efac045e2e",
"classDigikam_1_1FileActionMngrDatabaseWorker.html#a55d536a66cc80f349aef0bd295db1305ac9fda253787f588e35a4b3cc7ec18fa1",
"classDigikam_1_1FilmContainer.html#a5c5af25d32eaaa4b5718d66cee1b1048a0f7ee8c2746bd73aac32307bd3164a8c",
"classDigikam_1_1FisherFaceRecognizer.html#a444dc888b8743e0a0469b24e2610c516",
"classDigikam_1_1GeoCoordinates.html#a8ab514aab9c08db9229fe4ed176c0165",
"classDigikam_1_1Graph.html#acb03434163c72312dc8455f487e495ff",
"classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59ac8ca00e9cff5944c83a9c0febbcbaacd",
"classDigikam_1_1ICCSettingsContainer.html#a25d672a8562a9f5803e56067a764d194",
"classDigikam_1_1ImageMetadataContainer.html#a6cf1de323b77d613051eed0ab4232faf",
"classDigikam_1_1ImageWindow.html#a64d1432f9b08d0f495bf16a81552b85b",
"classDigikam_1_1ImportDelegate.html#a6dffdfeed76b89d88f92ad1194a3e6bc",
"classDigikam_1_1ImportIconView.html#a9625338a7b137c2f9fcaba08b2908b65",
"classDigikam_1_1ImportRatingOverlay.html#a002310a3d4482099ff04b131bab04ffa",
"classDigikam_1_1ImportThumbnailDelegate.html#ab34a3b86e6bd2fec3d9c8f8e108a8b66",
"classDigikam_1_1InfraredFilter.html#af10a2c0036d059b7c7a94ef90daca42c",
"classDigikam_1_1ItemAlbumFilterModel.html#a625802b662e24b0b3fcb5f3f8648ae85aaa9634930683596440a38e527bee5137",
"classDigikam_1_1ItemDelegate.html#a14b7dd70480c84ed2236fb0e4567426d",
"classDigikam_1_1ItemFaceDelegate.html#a97fbbdc6500a3562f10d3d613344c15f",
"classDigikam_1_1ItemFilterModelPreparer.html#a0e4fe71b3aaeaf90979d9c7f19d7b280a9a11c84626a985c3577959f273ee5f4a",
"classDigikam_1_1ItemHistoryGraph.html#a6e8dd68f367cf7826d3d6f95996c7358",
"classDigikam_1_1ItemInfoData.html#a42c5fe660c381f2e8d045ddb7f545cc2",
"classDigikam_1_1ItemLister.html#a929b192429374b6cbb470cc016621058",
"classDigikam_1_1ItemQueryPostHooks.html#af084670baba1265799fcc61c925140a3",
"classDigikam_1_1ItemSelectionOverlay.html#aa9fdfb1a650cc84f49f72451931365b0",
"classDigikam_1_1ItemViewDelegate.html#a02838afc380ac9c6e0f879291efe69c0",
"classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a1c14b083b4a2b96bdd04830954931e89",
"classDigikam_1_1LevelsContainer.html",
"classDigikam_1_1LightTableWindow.html#a9a70eddf80dd9eaf1821a87dbc286159",
"classDigikam_1_1MaintenanceSettings.html#ab51ae39f98ce92e0cc084b522746b0c4",
"classDigikam_1_1MapDragData.html",
"classDigikam_1_1MetaEngineSettingsContainer.html",
"classDigikam_1_1MysqlInitBinary.html#a36726da6ac3b0db9b222db47a6e353f2",
"classDigikam_1_1NormalizeFilter.html#a31bff7bcce0e539a08f2f469f8d6e7f3",
"classDigikam_1_1PageItem.html#a824394b4031b1d50cccda6ddfe7e9cb5",
"classDigikam_1_1PreviewLoadThread.html#a9cdd61f25e27f10dd79612e3f8563d47",
"classDigikam_1_1QueueMgrWindow.html#adb0abb342829351ba1bf8d0c5dac02e8",
"classDigikam_1_1RawProcessingFilter.html#a44b8605eea3bd450bc8823933ddb33bb",
"classDigikam_1_1RecognitionWorker.html#ae57b234ee8aadf2cd69a6dd39c2198ac",
"classDigikam_1_1RestorationPlugin.html#afde82d7d92a1d75dbae094a66c669057",
"classDigikam_1_1ScanStateFilter.html#a901ef9d13271a082592eed6e172a0ca4",
"classDigikam_1_1SearchFieldColorDepth.html#aca6e44cab4fe103ccc5eed96dfd80356",
"classDigikam_1_1SearchFieldRangeInt.html#a2db0e3f6b9e691d428e5656fe295ba40",
"classDigikam_1_1SearchModel.html#a6006551a313ffa49f6e6aa80a13da6af",
"classDigikam_1_1SearchXmlCachingReader.html#a7b67801654919c992207f70818fb012c",
"classDigikam_1_1SetupCollectionModel.html#a604c0eafc230d33d2a2fa74c72ad754b",
"classDigikam_1_1SharpenFilter.html#a9e527ee614116c0516166dcdcfecc453",
"classDigikam_1_1StayPoppedUpComboBox.html#a70ef8572cb25a8fd419043a57f1c2a58",
"classDigikam_1_1TableViewColumnProfile.html#a437e34050fdd191b2cd409d32d295620",
"classDigikam_1_1TableViewColumns_1_1ColumnThumbnail.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c",
"classDigikam_1_1TagRegion.html#a3411e1eb4a227712c72234a67b056ace",
"classDigikam_1_1TagsJob.html#a736bc91a70d7892ae9fe8d1bd76b795e",
"classDigikam_1_1TextureFilter.html#af9f6b14e0559edb0db360d2ecd5e009a",
"classDigikam_1_1ThumbnailLoadThread.html#ab2dac9f4df95ae482e1f6b9a0d1f76ba",
"classDigikam_1_1TileIndex.html#abe4b3c05471729d7a9cb11cd1cdd0bdcac96bbb73408c02ff9ad256fd30541d9c",
"classDigikam_1_1TransitionMngr.html#a44b76fae6de0b3845638ce525ceb4051",
"classDigikam_1_1UniqueModifier.html#a1bcba596fbc4ec57ec70b856fc001c92",
"classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a92e3731cbb75500bcccf6c7f8c5a94f0",
"classDigikam_1_1XmpWidget.html#a677dc844dfb4314eab76f1595ebfe21c",
"classShowFoto_1_1NoDuplicatesShowfotoFilterModel.html#a64fdd7d929a5765d39458da7bdc4f85f",
"classShowFoto_1_1ShowfotoCoordinatesOverlay.html#a20bc5e22301fc7c1488e372f999bf74d",
"classShowFoto_1_1ShowfotoNormalDelegate.html#ab58e3b2e2fa82118645dec88eb372fb6",
"classUi__AdvPrintPhotoPage.html#a17785e53c863f0c66b7797d36bebe81e",
"classUi__PresentationMainPage.html#a7f96820070abd2e08e55b756f84c7504",
"classdecoded__picture__buffer.html#a44a4ba5c4e284e822b86ec134bf02377",
"classheif_1_1Box.html#a3fd5d77a13b528eeaba0118bbd828605",
"classheif_1_1Box__grpl.html#a39d39e81b35c6e87a9928e7ddcbd3673",
"classheif_1_1Box__infe.html#a886ed0a0a458470382cb31c040850d49",
"classheif_1_1Box__pitm.html#a39d39e81b35c6e87a9928e7ddcbd3673",
"classheif_1_1HeifContext_1_1Image.html#aa1f0d5977dcb160268cb96ef94d5f682",
"classimage__unit.html#aec271bf3f9708a7b470dc384ebfb33aa",
"classoption__MEMode.html#aa2afafcd5568d10d4e326c16580101c9",
"classoption__int.html#a95052061b89c8354a55bbdf5e1d672cb",
"classseq__parameter__set.html#aef21706fa177f036679faeb8fa5016e7",
"classthread__task__ctb__row.html#a8a25bdbfffc883a59b068322c260b413a8b448690e8f9ae177b7688ec09c1bc66",
"index.html#develenv",
"namespaceDigikamGenericPrintCreatorPlugin.html",
"structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html#a6df654c1104f6fb4df91c5f76a7fa38a",
"structDigikam_1_1PTOType_1_1Stitcher.html#ad9f9d4c504a7ffb08f79d0df95df078f",
"structde265__image__allocation.html#a7875a39133df35dadbec9d0b35c65a5d",
"structimage__data.html#a227e5830acf7f0aed5982d9751915235"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';