var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto =
[
    [ "AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a51e06282eefe62503ca1dbe88b346bb0", null ],
    [ "AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#ae8e7bae6812e2ae78b08129c0fdd104a", null ],
    [ "~AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a00d4eeeab3efcd591799791b5386fed0", null ],
    [ "height", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#acfb0baee690e79adddc1d993b82d33f7", null ],
    [ "loadPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a6cdcb83f6f01e8d98ecf0b62cdd4ac35", null ],
    [ "scaleHeight", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a91ea75d0acb9afa3fea79079a03bfed6", null ],
    [ "scaleWidth", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a2c68a14c8e2663774ada95a82a47180d", null ],
    [ "size", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a3f6928860cbb6702ec8c3754bb930d41", null ],
    [ "thumbnail", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#acef011fbca3d97dc8715a2e782e077c5", null ],
    [ "updateCropRegion", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a5e62f7644a98d04b32bf615f93ae28e4", null ],
    [ "width", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#ac15937a46ffb198ce490c410d060673d", null ],
    [ "m_copies", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#ab9228d45d8a420b80ba69baa98793d8c", null ],
    [ "m_cropRegion", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#aa4b3bb3fef768ad7ff989238a4a9fd17", null ],
    [ "m_first", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#aec435e21700e45e41f00a520a8ee1b30", null ],
    [ "m_iface", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a0e5d381e5775e11abf5b418960fda902", null ],
    [ "m_pAddInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#aa15adc619fd77022893516cbb90339ed", null ],
    [ "m_pAdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a92552dc144d5fe04482eba345659aa8e", null ],
    [ "m_rotation", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#aeded9d409ed2521f0bacdc6a42c30f25", null ],
    [ "m_thumbnailSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#ad819317cb3f9c5702c881979744eb068", null ],
    [ "m_url", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html#a7a371e17e0f056b6274f1f5dea7e001b", null ]
];