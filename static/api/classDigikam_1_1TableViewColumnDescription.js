var classDigikam_1_1TableViewColumnDescription =
[
    [ "List", "classDigikam_1_1TableViewColumnDescription.html#abe6f01b8c28feba2433bde5a45a073b8", null ],
    [ "TableViewColumnDescription", "classDigikam_1_1TableViewColumnDescription.html#a6181a9079dd87b56a72ac5f83430c381", null ],
    [ "TableViewColumnDescription", "classDigikam_1_1TableViewColumnDescription.html#a92f76f62ec0a5e017df5fd6b44149c34", null ],
    [ "addSetting", "classDigikam_1_1TableViewColumnDescription.html#a191e993ce5a0283316dc14374270ef9e", null ],
    [ "addSubColumn", "classDigikam_1_1TableViewColumnDescription.html#ac36d28b221b53b1166e36a5c4ac997b8", null ],
    [ "setIcon", "classDigikam_1_1TableViewColumnDescription.html#a83b12eabebbb5ab46464bc6283c87427", null ],
    [ "toConfiguration", "classDigikam_1_1TableViewColumnDescription.html#a2eeec7ab6879080ae835eae5675ef1bf", null ],
    [ "columnIcon", "classDigikam_1_1TableViewColumnDescription.html#a2419538915efcab495fd38b88891cf25", null ],
    [ "columnId", "classDigikam_1_1TableViewColumnDescription.html#aeb7a7ab90c2f6cb602d24a0b541a2e2e", null ],
    [ "columnSettings", "classDigikam_1_1TableViewColumnDescription.html#aa09451f04ccfb923c3eb05377cdccf6e", null ],
    [ "columnTitle", "classDigikam_1_1TableViewColumnDescription.html#ae1b40213bd7939d019bbcb68912d9221", null ],
    [ "subColumns", "classDigikam_1_1TableViewColumnDescription.html#a80e36d9d3d5ebc87833c951f32210549", null ]
];