var classDigikam_1_1FacePipelineFaceTagsIfaceList =
[
    [ "FacePipelineFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a94dfddf2b06b8dcdd8ff42019d5f6802", null ],
    [ "FacePipelineFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a2998ef7dfa9877b0d3294f3ee5a6f8d9", null ],
    [ "clearRole", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a03c4eb78c7ebcb7c01fbae01112ae637", null ],
    [ "facesForRole", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a9fe17963729e0090651f5c01d1105f98", null ],
    [ "operator=", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a62e463d7981052d15dae50f8f2dd45ae", null ],
    [ "replaceRole", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a92a4868bb759c043621aa884d07e1f73", null ],
    [ "setRole", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#a12359f45a3be1389a500e19104d6bef0", null ],
    [ "toFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html#adb4bda38113fe2ac7d133bbad32abea2", null ]
];