var classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust =
[
    [ "BatchToolGroup", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72", [
      [ "BaseTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72abf7d05254a90fb96b64257b37ab2571c", null ],
      [ "CustomTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50", null ],
      [ "ColorTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72a678db3327b06483d6eec8601a6b65457", null ],
      [ "EnhanceTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72ac99e79b29944cded7f1466dad3f31c22", null ],
      [ "TransformTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72a5abc81bbd353db5e71868a59ec402d3f", null ],
      [ "DecorateTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72ac275940dc7d00089f0a46924d40413ac", null ],
      [ "FiltersTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72a3f77943d1787b72f8a1c3d5a9a04d4db", null ],
      [ "ConvertTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72a87f1c29bf0d78ea00d5ea67a99dc063d", null ],
      [ "MetadataTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afa76b46ac346747b289ce17be3124a72abb96e2d5f48eeda7bb30755e25cd6756", null ]
    ] ],
    [ "CurvesAdjust", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aa1357642c4249e6f8d67e9b9e856d5dd", null ],
    [ "~CurvesAdjust", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aade9aa97340b86920600bec92a58f5fe", null ],
    [ "apply", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a36d8541d1b9a820534e87902b54d088c", null ],
    [ "applyFilter", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a561e9e51ca9cadf1fecdbf147b5e0588", null ],
    [ "applyFilter", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a90a5d88988617961068fb2242dbf88d7", null ],
    [ "applyFilterChangedProperties", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ac596446d1ca3d457b7a8a6c9f360d763", null ],
    [ "cancel", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aad228c7e8dcf09d545ae0b8d8a579f06", null ],
    [ "clone", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a6f40f61207f6263506b3d732fe72e61a", null ],
    [ "defaultSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ae165a021d130273f39878255db2c1756", null ],
    [ "deleteSettingsWidget", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#abd1e5b945bfc28740f7cd79534d3dabf", null ],
    [ "errorDescription", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a9a6bd8dccc7132093bebf21c0368d8ef", null ],
    [ "getBranchHistory", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aa423a1039987bba7bd0d9369eb233957", null ],
    [ "getNeedResetExifOrientation", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a8d1951cc07e2e31fd6763a8f2abfd4c7", null ],
    [ "getResetExifOrientationAllowed", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a8fba6bed21021f2e8b9d34c5dbe54605", null ],
    [ "image", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a2b073350b84e16e091f27c251e391dc6", null ],
    [ "imageData", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a23eb9f26cab4ebe516e1047b99ec02e9", null ],
    [ "imageInfo", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a74fcc66bd46387a69238cd0b49376d32", null ],
    [ "inputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aaed484c6d693e73ce09d405803355d04", null ],
    [ "ioFileSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a29b94a1be7c1548b30032809a9e0e91a", null ],
    [ "isCancelled", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aec1749bf7cedd5261dcb7caf710b91b3", null ],
    [ "isLastChainedTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af9c31ea2af0d09924370a483161b1dde", null ],
    [ "isRawFile", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#afd910fab457aa527e72634f2de834c48", null ],
    [ "loadToDImg", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a070c2544bad70f9d7e1f85688de9c27f", null ],
    [ "outputSuffix", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aeee8ce2604a87f131d75e9bea8a595aa", null ],
    [ "outputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a4703fb10c14dd3b84edba248fed832bd", null ],
    [ "plugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a5fa11d110f75bc3dc6848ffe632f506f", null ],
    [ "Private", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a734eb6dae151d06cdd3bf42e7e2f447e", null ],
    [ "rawDecodingSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a285e355bed564ca3d4651a6c264bb5d0", null ],
    [ "registerSettingsWidget", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a53c5962ddc5b69d25057312ac2db078b", null ],
    [ "savefromDImg", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a71045c04fa4bb019c021c9dacd32348d", null ],
    [ "setBranchHistory", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a5003b8ab574560903b0dcffa408d139a", null ],
    [ "setDRawDecoderSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a6a3fc7a2cd0c1890543b5a0f3b8d5f59", null ],
    [ "setErrorDescription", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a73db6ff94e50240e68594c8635413659", null ],
    [ "setImageData", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a8547dbb306a9811d0e7242dec09ac68b", null ],
    [ "setInputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a57e6cc4aa683fa7a83e2ffe3d4651a1b", null ],
    [ "setIOFileSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a282091b29cea96aeae8bdb20f6aa73d0", null ],
    [ "setItemInfo", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a9c057fdf245570ab92de8133ce134d70", null ],
    [ "setLastChainedTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ae76700064b9ffacb124a8163dc693546", null ],
    [ "setNeedResetExifOrientation", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aa34b534ee277b0d6af52d0821423b5ee", null ],
    [ "setOutputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a9774274355899f382590dc68d782497a", null ],
    [ "setOutputUrlFromInputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aacfa495346a2334b263b8c6a425d740a", null ],
    [ "setPlugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a3da22fb32151df2f75e1073bb42626db", null ],
    [ "setRawLoadingRules", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ac3f13be06dcb2e9be6af7486154e1ab0", null ],
    [ "setResetExifOrientationAllowed", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#abfbf72c25a9b65b9fb4b572d46fca3ef", null ],
    [ "setSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a0a6c7c5630bd3a81fe65a351114f2b84", null ],
    [ "settings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a101d327668f9d9f7cc9695b1b9bf0d20", null ],
    [ "settingsWidget", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#acc1ce64746976144f211e2d537d3b851", null ],
    [ "setToolDescription", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a1c9ffb404b2597cdfc08515b3e9f86e7", null ],
    [ "setToolIcon", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a78786c22c95ece3d7ad4abd8b1e7b6d3", null ],
    [ "setToolIconName", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af339b08111d419eea4a8515c45f3accc", null ],
    [ "setToolTitle", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a4337c17e1bc828dc77eb8dba74099db0", null ],
    [ "setWorkingUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aae015f01fb686cfec93c764ca2c06013", null ],
    [ "signalAssignSettings2Widget", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af44500f1c3827c0e643f6494ef49b660", null ],
    [ "signalSettingsChanged", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a77b20102fb03bdb2e388c306df43b044", null ],
    [ "signalVisible", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#acaa2b52161bee3aa9d24db22ff2d72b1", null ],
    [ "slotResetSettingsToDefault", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a0067046d1ec93658bbb35977303046cb", null ],
    [ "slotSettingsChanged", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a3a0cda57c6cb05452e3a1af200c098ea", null ],
    [ "toolDescription", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a2f87e45801f6e53bdbdec85c52240a60", null ],
    [ "toolGroup", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af39a8dde244c4dfc83997db9cbc0029e", null ],
    [ "toolGroupToString", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a7a2ffb5cdde6950f6505525546639cba", null ],
    [ "toolIcon", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a847551f8c091a7b598e5782348bafbd8", null ],
    [ "toolTitle", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#add18d9118dc64a85e1b2c7f86c871f86", null ],
    [ "toolVersion", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ac65a61bd8560a8e2a39ea9b9f75eb669", null ],
    [ "workingUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ac84787fb1a697cdec8c21758896f49a3", null ],
    [ "branchHistory", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a65aeadc8181da5b5af721d8439f51b24", null ],
    [ "cancel", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af49685a432a1f3b3b0c5e8295fac13b7", null ],
    [ "errorMessage", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a398c542b011b949bb7385fea68c60e98", null ],
    [ "exifCanEditOrientation", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a2fb5df5353f97d75bf26b3189cbdb250", null ],
    [ "exifResetOrientation", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ab553a63ec51c96d5f0acb4f9ee7adda1", null ],
    [ "image", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a13e4394c3219e9046ced4da7f43a3337", null ],
    [ "imageinfo", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ac10f73b746cf9b8baf82e589608bfac6", null ],
    [ "inputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#af5b647dfc03873761cb4aa546efc8d73", null ],
    [ "ioFileSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ad023baae2efb91c23fb66576c0b66a85", null ],
    [ "last", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aaad2062397c2e203ab15832241c12b7a", null ],
    [ "m_settingsWidget", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a50a1ed7dcd42d42698fe50fa16eb4868", null ],
    [ "observer", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a5d2f149e0da3b8d7c431ae3d4f3eb950", null ],
    [ "outputUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ae0090c01c1a944e24e806a8f7b70908d", null ],
    [ "plugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ab468723dec6a06310b9fe6b7200e0817", null ],
    [ "rawDecodingSettings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a1ec1d044d40ccc29b1b7412b7351bb3a", null ],
    [ "rawLoadingRule", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a9fbe73ae3e08aac7b544f7da9aaf85b5", null ],
    [ "settings", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aaf635930b74caabe08713cca83a145ba", null ],
    [ "toolDescription", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ab7fbba166583932ebf1b03ba919a1bf6", null ],
    [ "toolGroup", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#ade10236d317e365261fe9a8c39e3a157", null ],
    [ "toolIcon", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#aab34578eff38ffc2f0159149fd0bd88e", null ],
    [ "toolTitle", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a3a53bc4b745c38e3e42a80fa4e2e2960", null ],
    [ "workingUrl", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html#a26545d325022d41008516556f7933548", null ]
];