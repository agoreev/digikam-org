var classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin =
[
    [ "CurvesAdjustPlugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#ac9f01ba853bdc951629b26330aa89ef3", null ],
    [ "~CurvesAdjustPlugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#abe569c856e78b7248d53d6b99798411f", null ],
    [ "addTool", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#aada8336ba5b96006673a89244ab7f4fc", null ],
    [ "authors", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a63c9562eee14d093358c594a40041ec3", null ],
    [ "categories", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#ace50667ec58d94b184bb064cf897c031", null ],
    [ "cleanUp", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#aafc9ca722aabb57dfbfc3845d230adad", null ],
    [ "description", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a3c2e7fc72cb2daca05f1186960d9a7df", null ],
    [ "details", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#ac8f480e3c535704eeb2921ee50c61a80", null ],
    [ "extraAboutData", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findToolByName", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a2cc55f4c57b24b392c510fda76224b57", null ],
    [ "hasVisibilityProperty", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a5428e743adbd7d4c5d4258af04ad591e", null ],
    [ "icon", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a7483e58d4ebc356f5404387561439380", null ],
    [ "ifaceIid", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a333f538f23a6d798bf35ddac6f8b33e8", null ],
    [ "iid", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a0fde530ae84f832e33bddc6eb3d33b08", null ],
    [ "libraryFileName", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a54f5db47aadcfa39e0ffbb9006163016", null ],
    [ "pluginAuthors", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a285e718c1b0fde7f076bf5a52b93cfd7", null ],
    [ "setLibraryFileName", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#acb40d2eaff5d4b16624a14df8b3ab711", null ],
    [ "setVisible", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a634d1026d4b7abcb1d131c34cfa4aca3", null ],
    [ "shouldLoaded", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "signalVisible", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a6be70fd2b36cbd87e78741c349eb30d8", null ],
    [ "tools", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#aeb5a12dd500c1106b4f1f7b7e98fa837", null ],
    [ "version", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "libraryFileName", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ],
    [ "tools", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html#a08ecb65fb679e3490f60dc9269588673", null ]
];