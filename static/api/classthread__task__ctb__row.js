var classthread__task__ctb__row =
[
    [ "Queued", "classthread__task__ctb__row.html#a8a25bdbfffc883a59b068322c260b413afae702c3b480ea4a75d1562830fdc588", null ],
    [ "Running", "classthread__task__ctb__row.html#a8a25bdbfffc883a59b068322c260b413a8b448690e8f9ae177b7688ec09c1bc66", null ],
    [ "Blocked", "classthread__task__ctb__row.html#a8a25bdbfffc883a59b068322c260b413a8cbfa031260c7d5d1c8deb86ecd32fcc", null ],
    [ "Finished", "classthread__task__ctb__row.html#a8a25bdbfffc883a59b068322c260b413af1589adeb38fc10379b95220e5e3e60e", null ],
    [ "name", "classthread__task__ctb__row.html#ac42f060bd60ffdae7224954e281c3c08", null ],
    [ "work", "classthread__task__ctb__row.html#af6b5b9e0ee69590e2a4e3e45c62ec788", null ],
    [ "debug_startCtbRow", "classthread__task__ctb__row.html#ac935d0281c8a253966f92bd3152402e8", null ],
    [ "firstSliceSubstream", "classthread__task__ctb__row.html#a0b298a3f487e4f737929c69bbcbc0456", null ],
    [ "state", "classthread__task__ctb__row.html#a486c8b4d9cf0e6cfaf201d146dd24618", null ],
    [ "tctx", "classthread__task__ctb__row.html#a77d2204197aee20382575f691f77bb42", null ]
];