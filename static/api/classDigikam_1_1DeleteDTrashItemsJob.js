var classDigikam_1_1DeleteDTrashItemsJob =
[
    [ "DeleteDTrashItemsJob", "classDigikam_1_1DeleteDTrashItemsJob.html#a9f49f4896ee77d45cfff860c2d5c5666", null ],
    [ "cancel", "classDigikam_1_1DeleteDTrashItemsJob.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "run", "classDigikam_1_1DeleteDTrashItemsJob.html#a6096982f93a6e31a8614ab2629505079", null ],
    [ "signalDone", "classDigikam_1_1DeleteDTrashItemsJob.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalError", "classDigikam_1_1DeleteDTrashItemsJob.html#aa7647d3964bc97817ffc803e924b2228", null ],
    [ "signalOneProccessed", "classDigikam_1_1DeleteDTrashItemsJob.html#aadd6792e2a0bb35265eda1044a671434", null ],
    [ "signalProgress", "classDigikam_1_1DeleteDTrashItemsJob.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikam_1_1DeleteDTrashItemsJob.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "m_cancel", "classDigikam_1_1DeleteDTrashItemsJob.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];