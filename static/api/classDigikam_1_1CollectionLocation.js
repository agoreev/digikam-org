var classDigikam_1_1CollectionLocation =
[
    [ "Status", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2d", [
      [ "LocationNull", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da057dbf9b75e101e4178a7d1f3c413157", null ],
      [ "LocationAvailable", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da91d729b7349c62799d23af1bc1cfb913", null ],
      [ "LocationHidden", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da431831542cb1b9caece19b62515328d4", null ],
      [ "LocationUnavailable", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da89f81dd6a9696696e33180ff8e45fa28", null ],
      [ "LocationDeleted", "classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da09737556f383ccb9e85afe0f3edede43", null ]
    ] ],
    [ "Type", "classDigikam_1_1CollectionLocation.html#a85099ad3d611d54dcede1412cc802d01", [
      [ "TypeVolumeHardWired", "classDigikam_1_1CollectionLocation.html#a85099ad3d611d54dcede1412cc802d01a1474e42c357307d28ff1b7e3d1a2c58b", null ],
      [ "TypeVolumeRemovable", "classDigikam_1_1CollectionLocation.html#a85099ad3d611d54dcede1412cc802d01a65b629dff58128a78d92fb324a1beb3d", null ],
      [ "TypeNetwork", "classDigikam_1_1CollectionLocation.html#a85099ad3d611d54dcede1412cc802d01ac76af2d83399ce80d362552c9f1bc379", null ]
    ] ],
    [ "CollectionLocation", "classDigikam_1_1CollectionLocation.html#a2089257f3d7a19cb1a1410ed1b55c2ef", null ],
    [ "albumRootPath", "classDigikam_1_1CollectionLocation.html#a721e46fc67375a1973527813f64c8fba", null ],
    [ "hash", "classDigikam_1_1CollectionLocation.html#a287f8c7721c4f5366f4fdacc9cf853df", null ],
    [ "id", "classDigikam_1_1CollectionLocation.html#ac06170d5fc9ebac5060bfb8d206c28f1", null ],
    [ "isAvailable", "classDigikam_1_1CollectionLocation.html#a724144b9c9515fed0018a7be5178aac0", null ],
    [ "isNull", "classDigikam_1_1CollectionLocation.html#ab5274efe3f06df6b3bce4b73ac147373", null ],
    [ "label", "classDigikam_1_1CollectionLocation.html#a00fa4f70e23e2ab247888b60d58f9ebc", null ],
    [ "status", "classDigikam_1_1CollectionLocation.html#af43fc302968374cd6db23217a1791c8e", null ],
    [ "type", "classDigikam_1_1CollectionLocation.html#ac3f77cf399e5a6349206f75f6972e217", null ],
    [ "m_id", "classDigikam_1_1CollectionLocation.html#a77b7dad959dab06cc5b47fae3a6bf5ef", null ],
    [ "m_label", "classDigikam_1_1CollectionLocation.html#a138793e521715cc1267bc4ff53695def", null ],
    [ "m_path", "classDigikam_1_1CollectionLocation.html#a717e7401f12dace947b540dbda0dcf6d", null ],
    [ "m_status", "classDigikam_1_1CollectionLocation.html#a12651a877f7a3144a3c57b1d4ec8a2b3", null ],
    [ "m_type", "classDigikam_1_1CollectionLocation.html#afff9a980cb73a5733df59af3e85a3bf7", null ]
];