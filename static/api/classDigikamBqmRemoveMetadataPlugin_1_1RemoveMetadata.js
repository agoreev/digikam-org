var classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata =
[
    [ "BatchToolGroup", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72", [
      [ "BaseTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72abf7d05254a90fb96b64257b37ab2571c", null ],
      [ "CustomTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50", null ],
      [ "ColorTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72a678db3327b06483d6eec8601a6b65457", null ],
      [ "EnhanceTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72ac99e79b29944cded7f1466dad3f31c22", null ],
      [ "TransformTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72a5abc81bbd353db5e71868a59ec402d3f", null ],
      [ "DecorateTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72ac275940dc7d00089f0a46924d40413ac", null ],
      [ "FiltersTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72a3f77943d1787b72f8a1c3d5a9a04d4db", null ],
      [ "ConvertTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72a87f1c29bf0d78ea00d5ea67a99dc063d", null ],
      [ "MetadataTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afa76b46ac346747b289ce17be3124a72abb96e2d5f48eeda7bb30755e25cd6756", null ]
    ] ],
    [ "RemoveMetadata", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af2121b1b53a3e4e13dc17d828bd7362a", null ],
    [ "~RemoveMetadata", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a87bc795e76a1a0254e2e3629b185beee", null ],
    [ "apply", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a36d8541d1b9a820534e87902b54d088c", null ],
    [ "applyFilter", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a561e9e51ca9cadf1fecdbf147b5e0588", null ],
    [ "applyFilter", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a90a5d88988617961068fb2242dbf88d7", null ],
    [ "applyFilterChangedProperties", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ac596446d1ca3d457b7a8a6c9f360d763", null ],
    [ "cancel", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aad228c7e8dcf09d545ae0b8d8a579f06", null ],
    [ "clone", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a092ad7d0963ff269679de511408e8f37", null ],
    [ "defaultSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a409d1bfad312b233cf59bd67c34bee3c", null ],
    [ "deleteSettingsWidget", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#abd1e5b945bfc28740f7cd79534d3dabf", null ],
    [ "errorDescription", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a9a6bd8dccc7132093bebf21c0368d8ef", null ],
    [ "getBranchHistory", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aa423a1039987bba7bd0d9369eb233957", null ],
    [ "getNeedResetExifOrientation", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a8d1951cc07e2e31fd6763a8f2abfd4c7", null ],
    [ "getResetExifOrientationAllowed", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a8fba6bed21021f2e8b9d34c5dbe54605", null ],
    [ "image", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a2b073350b84e16e091f27c251e391dc6", null ],
    [ "imageData", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a23eb9f26cab4ebe516e1047b99ec02e9", null ],
    [ "imageInfo", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a74fcc66bd46387a69238cd0b49376d32", null ],
    [ "inputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aaed484c6d693e73ce09d405803355d04", null ],
    [ "ioFileSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a29b94a1be7c1548b30032809a9e0e91a", null ],
    [ "isCancelled", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aec1749bf7cedd5261dcb7caf710b91b3", null ],
    [ "isLastChainedTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af9c31ea2af0d09924370a483161b1dde", null ],
    [ "isRawFile", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#afd910fab457aa527e72634f2de834c48", null ],
    [ "loadToDImg", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a070c2544bad70f9d7e1f85688de9c27f", null ],
    [ "outputSuffix", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aeee8ce2604a87f131d75e9bea8a595aa", null ],
    [ "outputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a4703fb10c14dd3b84edba248fed832bd", null ],
    [ "plugin", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a5fa11d110f75bc3dc6848ffe632f506f", null ],
    [ "Private", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a734eb6dae151d06cdd3bf42e7e2f447e", null ],
    [ "rawDecodingSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a285e355bed564ca3d4651a6c264bb5d0", null ],
    [ "registerSettingsWidget", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a7f5f10437ec93450a53eb2e5657c8146", null ],
    [ "savefromDImg", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a71045c04fa4bb019c021c9dacd32348d", null ],
    [ "setBranchHistory", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a5003b8ab574560903b0dcffa408d139a", null ],
    [ "setDRawDecoderSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a6a3fc7a2cd0c1890543b5a0f3b8d5f59", null ],
    [ "setErrorDescription", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a73db6ff94e50240e68594c8635413659", null ],
    [ "setImageData", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a8547dbb306a9811d0e7242dec09ac68b", null ],
    [ "setInputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a57e6cc4aa683fa7a83e2ffe3d4651a1b", null ],
    [ "setIOFileSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a282091b29cea96aeae8bdb20f6aa73d0", null ],
    [ "setItemInfo", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a9c057fdf245570ab92de8133ce134d70", null ],
    [ "setLastChainedTool", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ae76700064b9ffacb124a8163dc693546", null ],
    [ "setNeedResetExifOrientation", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aa34b534ee277b0d6af52d0821423b5ee", null ],
    [ "setOutputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a9774274355899f382590dc68d782497a", null ],
    [ "setOutputUrlFromInputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aacfa495346a2334b263b8c6a425d740a", null ],
    [ "setPlugin", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a3da22fb32151df2f75e1073bb42626db", null ],
    [ "setRawLoadingRules", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ac3f13be06dcb2e9be6af7486154e1ab0", null ],
    [ "setResetExifOrientationAllowed", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#abfbf72c25a9b65b9fb4b572d46fca3ef", null ],
    [ "setSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a0a6c7c5630bd3a81fe65a351114f2b84", null ],
    [ "settings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a101d327668f9d9f7cc9695b1b9bf0d20", null ],
    [ "settingsWidget", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#acc1ce64746976144f211e2d537d3b851", null ],
    [ "setToolDescription", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a1c9ffb404b2597cdfc08515b3e9f86e7", null ],
    [ "setToolIcon", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a78786c22c95ece3d7ad4abd8b1e7b6d3", null ],
    [ "setToolIconName", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af339b08111d419eea4a8515c45f3accc", null ],
    [ "setToolTitle", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a4337c17e1bc828dc77eb8dba74099db0", null ],
    [ "setWorkingUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aae015f01fb686cfec93c764ca2c06013", null ],
    [ "signalAssignSettings2Widget", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af44500f1c3827c0e643f6494ef49b660", null ],
    [ "signalSettingsChanged", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a77b20102fb03bdb2e388c306df43b044", null ],
    [ "signalVisible", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#acaa2b52161bee3aa9d24db22ff2d72b1", null ],
    [ "slotResetSettingsToDefault", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ab6fc0970f527dadbddc041ef1d9a1d1c", null ],
    [ "slotSettingsChanged", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a3a0cda57c6cb05452e3a1af200c098ea", null ],
    [ "toolDescription", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a2f87e45801f6e53bdbdec85c52240a60", null ],
    [ "toolGroup", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af39a8dde244c4dfc83997db9cbc0029e", null ],
    [ "toolGroupToString", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a7a2ffb5cdde6950f6505525546639cba", null ],
    [ "toolIcon", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a847551f8c091a7b598e5782348bafbd8", null ],
    [ "toolTitle", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#add18d9118dc64a85e1b2c7f86c871f86", null ],
    [ "toolVersion", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ac65a61bd8560a8e2a39ea9b9f75eb669", null ],
    [ "workingUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ac84787fb1a697cdec8c21758896f49a3", null ],
    [ "branchHistory", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a65aeadc8181da5b5af721d8439f51b24", null ],
    [ "cancel", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af49685a432a1f3b3b0c5e8295fac13b7", null ],
    [ "errorMessage", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a398c542b011b949bb7385fea68c60e98", null ],
    [ "exifCanEditOrientation", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a2fb5df5353f97d75bf26b3189cbdb250", null ],
    [ "exifResetOrientation", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ab553a63ec51c96d5f0acb4f9ee7adda1", null ],
    [ "image", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a13e4394c3219e9046ced4da7f43a3337", null ],
    [ "imageinfo", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ac10f73b746cf9b8baf82e589608bfac6", null ],
    [ "inputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#af5b647dfc03873761cb4aa546efc8d73", null ],
    [ "ioFileSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ad023baae2efb91c23fb66576c0b66a85", null ],
    [ "last", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aaad2062397c2e203ab15832241c12b7a", null ],
    [ "m_settingsWidget", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a50a1ed7dcd42d42698fe50fa16eb4868", null ],
    [ "observer", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a5d2f149e0da3b8d7c431ae3d4f3eb950", null ],
    [ "outputUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ae0090c01c1a944e24e806a8f7b70908d", null ],
    [ "plugin", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ab468723dec6a06310b9fe6b7200e0817", null ],
    [ "rawDecodingSettings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a1ec1d044d40ccc29b1b7412b7351bb3a", null ],
    [ "rawLoadingRule", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a9fbe73ae3e08aac7b544f7da9aaf85b5", null ],
    [ "settings", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aaf635930b74caabe08713cca83a145ba", null ],
    [ "toolDescription", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ab7fbba166583932ebf1b03ba919a1bf6", null ],
    [ "toolGroup", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#ade10236d317e365261fe9a8c39e3a157", null ],
    [ "toolIcon", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#aab34578eff38ffc2f0159149fd0bd88e", null ],
    [ "toolTitle", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a3a53bc4b745c38e3e42a80fa4e2e2960", null ],
    [ "workingUrl", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html#a26545d325022d41008516556f7933548", null ]
];