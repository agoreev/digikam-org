var classUi__AdvPrintCropPage =
[
    [ "retranslateUi", "classUi__AdvPrintCropPage.html#adcb4ef09640ba5e65244db23e34b1cdf", null ],
    [ "setupUi", "classUi__AdvPrintCropPage.html#aaa8f53586edff2e3fde9ea4efd6eef7c", null ],
    [ "BtnCropNext", "classUi__AdvPrintCropPage.html#a98f9fdc7068973e587ea4b2a22876e1e", null ],
    [ "BtnCropPrev", "classUi__AdvPrintCropPage.html#a5c51eb4deff6e426f80ed57e701214ce", null ],
    [ "BtnCropRotateLeft", "classUi__AdvPrintCropPage.html#a678ab4ba761c762df3c3b69383bc7775", null ],
    [ "BtnCropRotateRight", "classUi__AdvPrintCropPage.html#ab4890c91d7abe3a4015ea38077bbabcd", null ],
    [ "cropFrame", "classUi__AdvPrintCropPage.html#af81b0c36fee48b84db46ea35246a0d4b", null ],
    [ "gridLayout", "classUi__AdvPrintCropPage.html#a3a9eadc6976e79187969cad9b57c67b3", null ],
    [ "horizontalLayout", "classUi__AdvPrintCropPage.html#a594d765fe41ea902607858238fb77f64", null ],
    [ "horizontalSpacer", "classUi__AdvPrintCropPage.html#ad9ea27ad2cb92f01e671d9c8a2173407", null ],
    [ "LblCropPhoto", "classUi__AdvPrintCropPage.html#ae99aa6bc729d283c1432077315e0bcb7", null ],
    [ "m_disableCrop", "classUi__AdvPrintCropPage.html#a0eb58710c2c7cb1a5bdc9d1746f7032d", null ],
    [ "spacerItem", "classUi__AdvPrintCropPage.html#a9f0a1991176c93c4c47c8e57e63cd084", null ],
    [ "spacerItem1", "classUi__AdvPrintCropPage.html#aa3c3954035b08c430c17fd95cdb4be0a", null ],
    [ "verticalLayout", "classUi__AdvPrintCropPage.html#acd3e91205b2d7e85a79f51ee2d450f51", null ]
];