var classDigikam_1_1CollectionImageChangeset =
[
    [ "Operation", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9b", [
      [ "Unknown", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9bae99c799ce10b9259d8ed55b259277275", null ],
      [ "Added", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9ba1a681bd8ae018dd50c5cdb9c6102854e", null ],
      [ "Removed", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9ba1137efb36892657bb7f1e95bf04defae", null ],
      [ "RemovedAll", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9bae302c1aae0dd732d00b0755e74c559b2", null ],
      [ "Deleted", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9ba2040990116ae0b162a96be28e0b77a13", null ],
      [ "RemovedDeleted", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9bacf5f76e5f7df5e0e58f40a868a3ef7a4", null ],
      [ "Moved", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9ba9a96cd4fc764b0c24e325fbba4a00f03", null ],
      [ "Copied", "classDigikam_1_1CollectionImageChangeset.html#a368523a7c0362ef9cbb90024a1741e9baf01fea630c64fbe728a9a705b970edcf", null ]
    ] ],
    [ "CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html#a62d923ac3d2f8fafc63d412681e9115c", null ],
    [ "CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html#a39e064958e94b07d85a2d2b570ab42a4", null ],
    [ "CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html#a599fc19a51dc25d4b2d4ecdec6e2aa2a", null ],
    [ "CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html#a4fc4c7a437aa5cee800f166b679e3c2f", null ],
    [ "albums", "classDigikam_1_1CollectionImageChangeset.html#adddf4f6f658341b7910d30a7b926ec2f", null ],
    [ "containsAlbum", "classDigikam_1_1CollectionImageChangeset.html#a0bee074bd87c2ef5be3d511f8f9a62df", null ],
    [ "containsImage", "classDigikam_1_1CollectionImageChangeset.html#ad1f1188f3b3bc00438bf8d56029b2c46", null ],
    [ "ids", "classDigikam_1_1CollectionImageChangeset.html#a8ae713a061c508714d1b685e5ccd3a6c", null ],
    [ "operation", "classDigikam_1_1CollectionImageChangeset.html#a65ffde1abf2dde65efa763b0a82e5694", null ],
    [ "operator<<", "classDigikam_1_1CollectionImageChangeset.html#a9f2438578cf8ccaadecf594a8fc52fea", null ]
];