var namespaceDigikamGenericDebianScreenshotsPlugin =
[
    [ "DSMPForm", "classDigikamGenericDebianScreenshotsPlugin_1_1DSMPForm.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSMPForm" ],
    [ "DSPackageDelegate", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPackageDelegate.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPackageDelegate" ],
    [ "DSPlugin", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPlugin.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSPlugin" ],
    [ "DSTalker", "classDigikamGenericDebianScreenshotsPlugin_1_1DSTalker.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSTalker" ],
    [ "DSWidget", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWidget.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWidget" ],
    [ "DSWindow", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow" ]
];