var classDigikam_1_1LBPHFaceModel =
[
    [ "LBPHFaceModel", "classDigikam_1_1LBPHFaceModel.html#a213679efe147a2269cc422ad1aac40be", null ],
    [ "~LBPHFaceModel", "classDigikam_1_1LBPHFaceModel.html#a6a982b70c589db486c682006c005eb0c", null ],
    [ "gridX", "classDigikam_1_1LBPHFaceModel.html#a74a15e42bea3dd8fb0bcc25cff4059a6", null ],
    [ "gridY", "classDigikam_1_1LBPHFaceModel.html#abd16a872dc8a391df4d87091a653c748", null ],
    [ "histogramData", "classDigikam_1_1LBPHFaceModel.html#aed42acac029dc5c30daebbd3b4e5a125", null ],
    [ "histogramMetadata", "classDigikam_1_1LBPHFaceModel.html#a3d08757ec73ab7fa8b516df7f3db2f58", null ],
    [ "neighbors", "classDigikam_1_1LBPHFaceModel.html#a320c837e01485ee6612acc1a22db008a", null ],
    [ "ptr", "classDigikam_1_1LBPHFaceModel.html#a17d3bd4d28db2a8abab78d0675d92271", null ],
    [ "ptr", "classDigikam_1_1LBPHFaceModel.html#aff1cc3c2e864af3ad227758cf19f9d10", null ],
    [ "radius", "classDigikam_1_1LBPHFaceModel.html#a977994c725a3ffb8a015a409c6b7d063", null ],
    [ "setGridX", "classDigikam_1_1LBPHFaceModel.html#a7c2936d63ddc64864d05fa8e864491fd", null ],
    [ "setGridY", "classDigikam_1_1LBPHFaceModel.html#a97a12d28bc24fa13fbb6b89aa2d7961b", null ],
    [ "setHistograms", "classDigikam_1_1LBPHFaceModel.html#ac7c68294926428ff328b163b3acd9ff6", null ],
    [ "setNeighbors", "classDigikam_1_1LBPHFaceModel.html#adbe90ed839fd0ea9854842aad7606f4c", null ],
    [ "setRadius", "classDigikam_1_1LBPHFaceModel.html#a131da93fc483ee190e16015f3fe68aea", null ],
    [ "setWrittenToDatabase", "classDigikam_1_1LBPHFaceModel.html#ad13b3d0d103a5f78b7404f1361f6e5b6", null ],
    [ "update", "classDigikam_1_1LBPHFaceModel.html#aefd7926a25626779fd3ffa89a486e84d", null ],
    [ "databaseId", "classDigikam_1_1LBPHFaceModel.html#a745217c239d6325117f5769b59898e17", null ],
    [ "m_histogramMetadata", "classDigikam_1_1LBPHFaceModel.html#a9de919f33e4a02d28a4e7923af1b7244", null ]
];