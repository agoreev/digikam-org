var namespaceDigikamGenericOneDrivePlugin =
[
    [ "ODFolder", "classDigikamGenericOneDrivePlugin_1_1ODFolder.html", "classDigikamGenericOneDrivePlugin_1_1ODFolder" ],
    [ "ODMPForm", "classDigikamGenericOneDrivePlugin_1_1ODMPForm.html", "classDigikamGenericOneDrivePlugin_1_1ODMPForm" ],
    [ "ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg" ],
    [ "ODPhoto", "classDigikamGenericOneDrivePlugin_1_1ODPhoto.html", "classDigikamGenericOneDrivePlugin_1_1ODPhoto" ],
    [ "ODPlugin", "classDigikamGenericOneDrivePlugin_1_1ODPlugin.html", "classDigikamGenericOneDrivePlugin_1_1ODPlugin" ],
    [ "ODWidget", "classDigikamGenericOneDrivePlugin_1_1ODWidget.html", "classDigikamGenericOneDrivePlugin_1_1ODWidget" ]
];