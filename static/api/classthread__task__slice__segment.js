var classthread__task__slice__segment =
[
    [ "Queued", "classthread__task__slice__segment.html#a8a25bdbfffc883a59b068322c260b413afae702c3b480ea4a75d1562830fdc588", null ],
    [ "Running", "classthread__task__slice__segment.html#a8a25bdbfffc883a59b068322c260b413a8b448690e8f9ae177b7688ec09c1bc66", null ],
    [ "Blocked", "classthread__task__slice__segment.html#a8a25bdbfffc883a59b068322c260b413a8cbfa031260c7d5d1c8deb86ecd32fcc", null ],
    [ "Finished", "classthread__task__slice__segment.html#a8a25bdbfffc883a59b068322c260b413af1589adeb38fc10379b95220e5e3e60e", null ],
    [ "name", "classthread__task__slice__segment.html#aa5a8b7254b76ccd596333648b0a1bd7c", null ],
    [ "work", "classthread__task__slice__segment.html#ad69b99e253919149f2183ed1fe640349", null ],
    [ "debug_startCtbX", "classthread__task__slice__segment.html#adeaff31b5c07d1564bd3a3bdd7d14353", null ],
    [ "debug_startCtbY", "classthread__task__slice__segment.html#aa2bc40da59f898652d6045bb962bf5d4", null ],
    [ "firstSliceSubstream", "classthread__task__slice__segment.html#afcaa72836c7be326a23f927b6d7eafc1", null ],
    [ "state", "classthread__task__slice__segment.html#a486c8b4d9cf0e6cfaf201d146dd24618", null ],
    [ "tctx", "classthread__task__slice__segment.html#a35eb8215c5b883336ee58c3701f698fa", null ]
];