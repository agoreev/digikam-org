var structheif__encoder__parameter =
[
    [ "boolean", "structheif__encoder__parameter.html#abf08ae14d7317337cc6ffe6455daa5d6", null ],
    [ "default_value", "structheif__encoder__parameter.html#a631846d4643cc9392f1c8f486acfda89", null ],
    [ "default_value", "structheif__encoder__parameter.html#ae27d7d66f3c65a60fad0965e9831e0a3", null ],
    [ "has_default", "structheif__encoder__parameter.html#a335e489ba49839eb1142d53f546a9342", null ],
    [ "have_minimum_maximum", "structheif__encoder__parameter.html#a23a7912ad8ccb474ee92323fd8e5f76e", null ],
    [ "integer", "structheif__encoder__parameter.html#a02372b3e5878517782f348d30c7066fc", null ],
    [ "maximum", "structheif__encoder__parameter.html#ab7072f888b7b9929569cab5f9b731a64", null ],
    [ "minimum", "structheif__encoder__parameter.html#ac1ec16c2555977d992a57edda0867d85", null ],
    [ "name", "structheif__encoder__parameter.html#ac1326899cccbcc56c2eec347b2f5f3ed", null ],
    [ "num_valid_values", "structheif__encoder__parameter.html#abda6db95669de0a0fd96fc0e851b0f78", null ],
    [ "string", "structheif__encoder__parameter.html#acd83235e0de61df07f23e45b1c249362", null ],
    [ "type", "structheif__encoder__parameter.html#ad3b55d3af3e5b73ba186f6a1a61af174", null ],
    [ "valid_values", "structheif__encoder__parameter.html#ae6ddfe26d06dcc30cb37c5acd9c75856", null ],
    [ "valid_values", "structheif__encoder__parameter.html#ad0d1820875689091906bc4b1a5588b93", null ],
    [ "version", "structheif__encoder__parameter.html#a8d2915841fc342cd4b8f147a8ada1295", null ]
];