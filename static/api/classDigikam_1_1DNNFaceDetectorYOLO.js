var classDigikam_1_1DNNFaceDetectorYOLO =
[
    [ "DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html#a7c57da144eec7dd31dcbbdb739041700", null ],
    [ "~DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html#a159ffd6e170391711abc7a803a98f03f", null ],
    [ "correctBbox", "classDigikam_1_1DNNFaceDetectorYOLO.html#aa3c3fc9a38724e3d0423989f47d6b153", null ],
    [ "detectFaces", "classDigikam_1_1DNNFaceDetectorYOLO.html#ae00aee79bf5de77277dfbde850bc5724", null ],
    [ "nnInputSizeRequired", "classDigikam_1_1DNNFaceDetectorYOLO.html#a10c2f7b450ebc0a6b0b631e333f4f10d", null ],
    [ "selectBbox", "classDigikam_1_1DNNFaceDetectorYOLO.html#ac03ca956fbe2ad1a2c45e44a24d5dc43", null ],
    [ "inputImageSize", "classDigikam_1_1DNNFaceDetectorYOLO.html#a57327c3e3bc01d71b2c155cb51f10488", null ],
    [ "meanValToSubtract", "classDigikam_1_1DNNFaceDetectorYOLO.html#a800befeba470c89ae6e2b9cad13bf6f3", null ],
    [ "net", "classDigikam_1_1DNNFaceDetectorYOLO.html#a05c55414cc0784bebc3925a6547f8fef", null ],
    [ "scaleFactor", "classDigikam_1_1DNNFaceDetectorYOLO.html#a3c902bace9d612c1990c1eeeec51b22f", null ]
];