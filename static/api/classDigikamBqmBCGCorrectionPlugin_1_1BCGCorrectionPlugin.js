var classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin =
[
    [ "BCGCorrectionPlugin", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#aeab162a99c3fe7a52bcc1b15914f4ffa", null ],
    [ "~BCGCorrectionPlugin", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a9a14cdca31fbc5b45fb5b6290916ab4d", null ],
    [ "addTool", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#aada8336ba5b96006673a89244ab7f4fc", null ],
    [ "authors", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#afc163b05eabb1857bf0a20b53c504820", null ],
    [ "categories", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#ace50667ec58d94b184bb064cf897c031", null ],
    [ "cleanUp", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#aafc9ca722aabb57dfbfc3845d230adad", null ],
    [ "description", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a80ab476118de6a03060118431e691dd0", null ],
    [ "details", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#ad68201a071b5848951892691a7241415", null ],
    [ "extraAboutData", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findToolByName", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a2cc55f4c57b24b392c510fda76224b57", null ],
    [ "hasVisibilityProperty", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a5428e743adbd7d4c5d4258af04ad591e", null ],
    [ "icon", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a9206aea1e337ddb037d14a59c38569ec", null ],
    [ "ifaceIid", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a333f538f23a6d798bf35ddac6f8b33e8", null ],
    [ "iid", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#ac9e5699a7eb8d2e357c82a10eba69ce2", null ],
    [ "libraryFileName", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a44cb7fc8e49e566424865e53314d6ff9", null ],
    [ "pluginAuthors", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a285e718c1b0fde7f076bf5a52b93cfd7", null ],
    [ "setLibraryFileName", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a846610ef70b32e77c4348b5ef1564ff1", null ],
    [ "setVisible", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a634d1026d4b7abcb1d131c34cfa4aca3", null ],
    [ "shouldLoaded", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "signalVisible", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a6be70fd2b36cbd87e78741c349eb30d8", null ],
    [ "tools", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#aeb5a12dd500c1106b4f1f7b7e98fa837", null ],
    [ "version", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "libraryFileName", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ],
    [ "tools", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html#a08ecb65fb679e3490f60dc9269588673", null ]
];