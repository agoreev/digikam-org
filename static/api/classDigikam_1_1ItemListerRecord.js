var classDigikam_1_1ItemListerRecord =
[
    [ "MagicValue", "classDigikam_1_1ItemListerRecord.html#a3b01d7e3c9e4bb1175139dbb190eb031aeb0c0a162f9707576635078563d07423", null ],
    [ "BinaryFormat", "classDigikam_1_1ItemListerRecord.html#a37550d47696051a879697ba14c587e5f", [
      [ "TraditionalFormat", "classDigikam_1_1ItemListerRecord.html#a37550d47696051a879697ba14c587e5fa9062f9857ea173c0bacf49caba87fac6", null ],
      [ "ExtraValueFormat", "classDigikam_1_1ItemListerRecord.html#a37550d47696051a879697ba14c587e5fa5d151cb6c45d6496f868ff8e05e7eed8", null ]
    ] ],
    [ "ItemListerRecord", "classDigikam_1_1ItemListerRecord.html#a3b440e92c024c62e7db75078709d9e36", null ],
    [ "operator==", "classDigikam_1_1ItemListerRecord.html#ab8b93bb019e38fc5c18f9be728e30012", null ],
    [ "albumID", "classDigikam_1_1ItemListerRecord.html#ab4094659b0b99e15a7eaf7d60b2c71eb", null ],
    [ "albumRootID", "classDigikam_1_1ItemListerRecord.html#a77302a7ab798024cbef941202cd80a6b", null ],
    [ "binaryFormat", "classDigikam_1_1ItemListerRecord.html#a0692b9c869d8ed3c322863e0f0b0d2c0", null ],
    [ "category", "classDigikam_1_1ItemListerRecord.html#a8d36a7bb48cf3865315b356f3f60db64", null ],
    [ "creationDate", "classDigikam_1_1ItemListerRecord.html#abf69056dca1f37714a840ea85b11e9a5", null ],
    [ "currentFuzzySearchReferenceImage", "classDigikam_1_1ItemListerRecord.html#a87bcdb790e52a1c91ce1405f26dfc386", null ],
    [ "currentSimilarity", "classDigikam_1_1ItemListerRecord.html#ad745d9a97eb58c4fa79d6bdb90b0d139", null ],
    [ "extraValues", "classDigikam_1_1ItemListerRecord.html#a669ab48df1d1e1f2c1c3b0f11aabc09c", null ],
    [ "fileSize", "classDigikam_1_1ItemListerRecord.html#aafc46a066e013bf47d4c19f3b1b90b54", null ],
    [ "format", "classDigikam_1_1ItemListerRecord.html#a5a1d0917446bccddc2756327c9041fbc", null ],
    [ "imageID", "classDigikam_1_1ItemListerRecord.html#a2afd2484a19385cfdb5fa02eef0b518f", null ],
    [ "imageSize", "classDigikam_1_1ItemListerRecord.html#a8baff734e548d11f4853ddd94f0c5635", null ],
    [ "modificationDate", "classDigikam_1_1ItemListerRecord.html#aad07e6ff9f03ac90f959dcf5b504606c", null ],
    [ "name", "classDigikam_1_1ItemListerRecord.html#a2fc98fccfdc17bfb5d98ed9b6669d0e1", null ],
    [ "rating", "classDigikam_1_1ItemListerRecord.html#ae281e939a5b4f980b406445a74558ccf", null ]
];