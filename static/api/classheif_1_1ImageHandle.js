var classheif_1_1ImageHandle =
[
    [ "DecodingOptions", "classheif_1_1ImageHandle_1_1DecodingOptions.html", null ],
    [ "ImageHandle", "classheif_1_1ImageHandle.html#a2801557d5807956d9e438f995f1408c5", null ],
    [ "ImageHandle", "classheif_1_1ImageHandle.html#a772494c3c796405e4837641447cb2b57", null ],
    [ "decode_image", "classheif_1_1ImageHandle.html#a6fcb619b2f26455d57901b56e9c9d3ea", null ],
    [ "empty", "classheif_1_1ImageHandle.html#ad619f1c54a62bd16e36a764b2ec50a34", null ],
    [ "get_height", "classheif_1_1ImageHandle.html#ab6413dda7efb88d6dd96c119efe380c3", null ],
    [ "get_list_of_metadata_block_IDs", "classheif_1_1ImageHandle.html#ac330006c0130f384575b7644b502ccf8", null ],
    [ "get_list_of_thumbnail_IDs", "classheif_1_1ImageHandle.html#acece974dd5c8d592928ea5457ced197f", null ],
    [ "get_metadata", "classheif_1_1ImageHandle.html#ad70d510994455c9ea98ab853b3f412ad", null ],
    [ "get_metadata_content_type", "classheif_1_1ImageHandle.html#a5ca671b6e29890c1fb518a5e265678a8", null ],
    [ "get_metadata_type", "classheif_1_1ImageHandle.html#a13462ffa4c64f7790d3d88c207e421a6", null ],
    [ "get_number_of_thumbnails", "classheif_1_1ImageHandle.html#a690ef2786dbe804d81b92557f11d31cd", null ],
    [ "get_raw_image_handle", "classheif_1_1ImageHandle.html#a01dd4655c8ed6b61302c995039dbe850", null ],
    [ "get_raw_image_handle", "classheif_1_1ImageHandle.html#a667a2f8e4b7606f40eea232406ab6271", null ],
    [ "get_thumbnail", "classheif_1_1ImageHandle.html#a8bc2dad96ba8a332e79e49162ea4ee7f", null ],
    [ "get_width", "classheif_1_1ImageHandle.html#ae966f852d037bf82ffb2ffb0903b90ca", null ],
    [ "has_alpha_channel", "classheif_1_1ImageHandle.html#ab6318d1feba006a538718b73cea9109f", null ],
    [ "is_primary_image", "classheif_1_1ImageHandle.html#a3ee367875175127004293b79c62766b8", null ]
];