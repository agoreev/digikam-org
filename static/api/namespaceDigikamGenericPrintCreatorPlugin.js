var namespaceDigikamGenericPrintCreatorPlugin =
[
    [ "AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo" ],
    [ "AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo" ],
    [ "AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg" ],
    [ "AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto" ],
    [ "AdvPrintPhotoSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize" ],
    [ "AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings" ],
    [ "AdvPrintThread", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread.html", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread" ],
    [ "AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode" ],
    [ "AtkinsPageLayoutTree", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree.html", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree" ],
    [ "GimpBinary", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary.html", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary" ],
    [ "PrintCreatorPlugin", "classDigikamGenericPrintCreatorPlugin_1_1PrintCreatorPlugin.html", "classDigikamGenericPrintCreatorPlugin_1_1PrintCreatorPlugin" ]
];