var classDigikam_1_1SearchChangeset =
[
    [ "Operation", "classDigikam_1_1SearchChangeset.html#a579c00723ef833f9c0f444f16a13be2b", [
      [ "Unknown", "classDigikam_1_1SearchChangeset.html#a579c00723ef833f9c0f444f16a13be2ba9c42d4e2f435c91eb62933cdecf20caf", null ],
      [ "Added", "classDigikam_1_1SearchChangeset.html#a579c00723ef833f9c0f444f16a13be2baba9e903eb91d738997583d66c408d832", null ],
      [ "Deleted", "classDigikam_1_1SearchChangeset.html#a579c00723ef833f9c0f444f16a13be2ba4060689de0fb0930fc5d1444f21b0252", null ],
      [ "Changed", "classDigikam_1_1SearchChangeset.html#a579c00723ef833f9c0f444f16a13be2ba1e13921492b09ab262176ca51c602132", null ]
    ] ],
    [ "SearchChangeset", "classDigikam_1_1SearchChangeset.html#acc6a6798dec3a505d341bd7c683e8ea2", null ],
    [ "SearchChangeset", "classDigikam_1_1SearchChangeset.html#a7a0bfd77d53c627b1db50e588c13555c", null ],
    [ "operation", "classDigikam_1_1SearchChangeset.html#a0f027656427a5e4593bf32bb3f94a246", null ],
    [ "searchId", "classDigikam_1_1SearchChangeset.html#a5c121a977047d6c4bd0b82a3fe5819de", null ]
];