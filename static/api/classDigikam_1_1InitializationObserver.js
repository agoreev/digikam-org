var classDigikam_1_1InitializationObserver =
[
    [ "UpdateResult", "classDigikam_1_1InitializationObserver.html#a5d6bd3af81ffa3dad815d1e80b04a17f", [
      [ "UpdateSuccess", "classDigikam_1_1InitializationObserver.html#a5d6bd3af81ffa3dad815d1e80b04a17facfc91ffd77ef26162adb29bb00c1a7be", null ],
      [ "UpdateError", "classDigikam_1_1InitializationObserver.html#a5d6bd3af81ffa3dad815d1e80b04a17fa2cccb914b70e5292cccaad64b98e6758", null ],
      [ "UpdateErrorMustAbort", "classDigikam_1_1InitializationObserver.html#a5d6bd3af81ffa3dad815d1e80b04a17fad73004a14df523b873ba8f135b1b279e", null ]
    ] ],
    [ "~InitializationObserver", "classDigikam_1_1InitializationObserver.html#a30207c77b88448e149265b76233e3864", null ],
    [ "connectCollectionScanner", "classDigikam_1_1InitializationObserver.html#ae4d11a335b2e34da5293b8383461a503", null ],
    [ "continueQuery", "classDigikam_1_1InitializationObserver.html#a7d2233f47fc613bde7a7247e5667b45f", null ],
    [ "error", "classDigikam_1_1InitializationObserver.html#ad19c4ced5a29d61ed13d48ccde5a6e63", null ],
    [ "finishedSchemaUpdate", "classDigikam_1_1InitializationObserver.html#aa22ef26b809ba3d55d903d8137cb3438", null ],
    [ "moreSchemaUpdateSteps", "classDigikam_1_1InitializationObserver.html#a926b44568da7815a324a2852bfd274c1", null ],
    [ "schemaUpdateProgress", "classDigikam_1_1InitializationObserver.html#a2cb91473be51929f0cb7bba98d8e69d1", null ]
];