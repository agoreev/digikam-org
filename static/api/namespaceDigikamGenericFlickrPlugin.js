var namespaceDigikamGenericFlickrPlugin =
[
    [ "FlickrMPForm", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm.html", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm" ],
    [ "FlickrNewAlbumDlg", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg.html", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg" ],
    [ "FlickrPlugin", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin.html", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin" ],
    [ "FlickrWidget", "classDigikamGenericFlickrPlugin_1_1FlickrWidget.html", "classDigikamGenericFlickrPlugin_1_1FlickrWidget" ],
    [ "FPhotoInfo", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo.html", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo" ],
    [ "FPhotoSet", "classDigikamGenericFlickrPlugin_1_1FPhotoSet.html", "classDigikamGenericFlickrPlugin_1_1FPhotoSet" ],
    [ "GAlbum", "classDigikamGenericFlickrPlugin_1_1GAlbum.html", "classDigikamGenericFlickrPlugin_1_1GAlbum" ],
    [ "GPhoto", "classDigikamGenericFlickrPlugin_1_1GPhoto.html", "classDigikamGenericFlickrPlugin_1_1GPhoto" ]
];