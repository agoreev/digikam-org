var classslice__unit =
[
    [ "SliceDecodingProgress", "classslice__unit.html#ad9a6b4659f44d0433d4d0a103054f50f", [
      [ "Unprocessed", "classslice__unit.html#ad9a6b4659f44d0433d4d0a103054f50fa9e6761ffe98ab61a53b7a5f837924610", null ],
      [ "InProgress", "classslice__unit.html#ad9a6b4659f44d0433d4d0a103054f50fae39a758482d21ce2e9ae75c140c51b7f", null ],
      [ "Decoded", "classslice__unit.html#ad9a6b4659f44d0433d4d0a103054f50fa2e88366d46ed7755c13394c21fd0a338", null ]
    ] ],
    [ "slice_unit", "classslice__unit.html#ac1930dc2fb12fcb82ccd7d1a0b6fb84a", null ],
    [ "~slice_unit", "classslice__unit.html#a4ce290731cb84bcf373ec74cbe4a76ad", null ],
    [ "allocate_thread_contexts", "classslice__unit.html#adb81aa66e9f3fe955f4d9cb3b48e21cc", null ],
    [ "get_thread_context", "classslice__unit.html#af0b86d17b1d70ab9f6309cfdb26ca238", null ],
    [ "num_thread_contexts", "classslice__unit.html#a713de2a03e861425a5e4eec033ea9a06", null ],
    [ "ctx", "classslice__unit.html#ad1025ff706ba08c46dba11e73a922ac0", null ],
    [ "finished_threads", "classslice__unit.html#aa93e7cda0fc9a061b1aebd87d4af9139", null ],
    [ "first_decoded_CTB_RS", "classslice__unit.html#a12bae7d638254aea03d4dd3c510e8858", null ],
    [ "flush_reorder_buffer", "classslice__unit.html#a32ff8c0768bb1d6b6cd14df0a9f3e64a", null ],
    [ "imgunit", "classslice__unit.html#aca72ae30b7a615a3a31ce7cea73bd012", null ],
    [ "last_decoded_CTB_RS", "classslice__unit.html#a88cd7459ba1c4ab7a0a32b06c6aad133", null ],
    [ "nal", "classslice__unit.html#aa6b244e2252b5d44997f7199e1a8b023", null ],
    [ "nThreads", "classslice__unit.html#aec3e1ab8dc81699b698ac1975a589851", null ],
    [ "reader", "classslice__unit.html#a2185f641e87544e87d39abfb4af2b5ff", null ],
    [ "shdr", "classslice__unit.html#a8856edbb8c38c6fe0a00b9ef58d8d412", null ],
    [ "state", "classslice__unit.html#aabccf807aea4c922a52cbe7e3d0dcfe7", null ]
];