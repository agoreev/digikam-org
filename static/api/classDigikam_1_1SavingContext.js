var classDigikam_1_1SavingContext =
[
    [ "SavingState", "classDigikam_1_1SavingContext.html#a7f1081a2a9502446c0fae3dddc3c0c8e", [
      [ "SavingStateNone", "classDigikam_1_1SavingContext.html#a7f1081a2a9502446c0fae3dddc3c0c8eac4c5b8eb5bc37a42987ac97be5e77196", null ],
      [ "SavingStateSave", "classDigikam_1_1SavingContext.html#a7f1081a2a9502446c0fae3dddc3c0c8ea3f5c644e6d4e50d535cb297e93c26f0a", null ],
      [ "SavingStateSaveAs", "classDigikam_1_1SavingContext.html#a7f1081a2a9502446c0fae3dddc3c0c8ea149b02bb231adc78b5552fcc004732b4", null ],
      [ "SavingStateVersion", "classDigikam_1_1SavingContext.html#a7f1081a2a9502446c0fae3dddc3c0c8ea5c887dde37ef69c993f426e389266541", null ]
    ] ],
    [ "SynchronizingState", "classDigikam_1_1SavingContext.html#af3a647586d20fed6421d0487b80b1082", [
      [ "NormalSaving", "classDigikam_1_1SavingContext.html#af3a647586d20fed6421d0487b80b1082a1f07850d235fbce7bc69f2f61f7fb943", null ],
      [ "SynchronousSaving", "classDigikam_1_1SavingContext.html#af3a647586d20fed6421d0487b80b1082a3b621bb874720f6ec675c95a0ae4abd1", null ]
    ] ],
    [ "SavingContext", "classDigikam_1_1SavingContext.html#ae993db6ce4880c5a01bb8831c532f9b1", null ],
    [ "abortingSaving", "classDigikam_1_1SavingContext.html#a7ed619fabdc971cd0217c571f4401d8d", null ],
    [ "destinationExisted", "classDigikam_1_1SavingContext.html#a097de70a4349971292ccae4e2170de79", null ],
    [ "destinationURL", "classDigikam_1_1SavingContext.html#ab5610c16815bb179efcbd7142daf9771", null ],
    [ "executedOperation", "classDigikam_1_1SavingContext.html#af1e17a02754ef5d614b6d6ecf3185436", null ],
    [ "format", "classDigikam_1_1SavingContext.html#a5c153ac648fb85142b53beffa1179520", null ],
    [ "moveSrcURL", "classDigikam_1_1SavingContext.html#adbed6d8d5df83337dd534165cb6e63e0", null ],
    [ "originalFormat", "classDigikam_1_1SavingContext.html#acb66a5f9544357b039b08fb31135902e", null ],
    [ "saveTempFile", "classDigikam_1_1SavingContext.html#a4b0e3e675c4f2ce0d187b8a1b127cc9c", null ],
    [ "saveTempFileName", "classDigikam_1_1SavingContext.html#a128bd1b027ee5061bbb52c647286eb87", null ],
    [ "savingState", "classDigikam_1_1SavingContext.html#af0b6e114aaa9d7c0bca9fa3f10ac560a", null ],
    [ "srcURL", "classDigikam_1_1SavingContext.html#ad63d922cdb8b3ca8f36d612b9cc04e22", null ],
    [ "synchronizingState", "classDigikam_1_1SavingContext.html#aecc642f6abe6d8220824a83129cd4d8d", null ],
    [ "synchronousSavingResult", "classDigikam_1_1SavingContext.html#a3f8014515d2ece278f37651987f3be1c", null ],
    [ "versionFileOperation", "classDigikam_1_1SavingContext.html#ab083628af8131d0adbc3ab658892e364", null ]
];