var classDigikam_1_1LookupAltitude =
[
    [ "Request", "classDigikam_1_1LookupAltitude_1_1Request.html", "classDigikam_1_1LookupAltitude_1_1Request" ],
    [ "StatusEnum", "classDigikam_1_1LookupAltitude.html#ad3320913e71eebe73799045a4de67f4c", [
      [ "StatusInProgress", "classDigikam_1_1LookupAltitude.html#ad3320913e71eebe73799045a4de67f4cac5d94f1275993d0144722e86040514ba", null ],
      [ "StatusSuccess", "classDigikam_1_1LookupAltitude.html#ad3320913e71eebe73799045a4de67f4caa61b7ceac9bbd483754033f6fa3cfc87", null ],
      [ "StatusCanceled", "classDigikam_1_1LookupAltitude.html#ad3320913e71eebe73799045a4de67f4cada85b12d51a22dcbbf658ab7da15ceab", null ],
      [ "StatusError", "classDigikam_1_1LookupAltitude.html#ad3320913e71eebe73799045a4de67f4caacad9eb8c3792cf2414816e3a4699b5b", null ]
    ] ],
    [ "LookupAltitude", "classDigikam_1_1LookupAltitude.html#aa4ebd2ed76b9a3c44f688378f7b4a442", null ],
    [ "~LookupAltitude", "classDigikam_1_1LookupAltitude.html#a46d15994c4ec1124cb57086d1f0bd8b7", null ],
    [ "addRequests", "classDigikam_1_1LookupAltitude.html#a806f9bb2496804b7aedc36939444497c", null ],
    [ "backendHumanName", "classDigikam_1_1LookupAltitude.html#ab02bd12117d7d6a917f34d1e5dd776d4", null ],
    [ "backendName", "classDigikam_1_1LookupAltitude.html#aacafa41d9858717cadec498a85f9b61d", null ],
    [ "cancel", "classDigikam_1_1LookupAltitude.html#a54c4ba21372494649caa43ec3aadded9", null ],
    [ "errorMessage", "classDigikam_1_1LookupAltitude.html#a67b8cbc5d1e6dde7eb63fc8136fa034f", null ],
    [ "getRequest", "classDigikam_1_1LookupAltitude.html#ac2a81bf036e0cffb65ab1ab61560447f", null ],
    [ "getRequests", "classDigikam_1_1LookupAltitude.html#a5597eb84fa9f03307e5f7f0e456db298", null ],
    [ "getStatus", "classDigikam_1_1LookupAltitude.html#a11a9719bd29e82e05d3ebbc9be2a698b", null ],
    [ "signalDone", "classDigikam_1_1LookupAltitude.html#a5988d5ffe26c81482d15860f85ccc846", null ],
    [ "signalRequestsReady", "classDigikam_1_1LookupAltitude.html#a58e7b77d3097d70978005b3472bd6082", null ],
    [ "startLookup", "classDigikam_1_1LookupAltitude.html#aaf60b9ad8e784768c73b42e2e3872c9c", null ]
];