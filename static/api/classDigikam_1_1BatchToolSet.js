var classDigikam_1_1BatchToolSet =
[
    [ "BatchToolSet", "classDigikam_1_1BatchToolSet.html#addc6103cfc1b1fa9cc00632050fef26e", null ],
    [ "~BatchToolSet", "classDigikam_1_1BatchToolSet.html#aa2f676f67b6f436ef1ab38b25babcc72", null ],
    [ "operator==", "classDigikam_1_1BatchToolSet.html#a72055843b1f49988e621db365722a6c4", null ],
    [ "group", "classDigikam_1_1BatchToolSet.html#afe1187bb6aceac2a1685b9f086b8925a", null ],
    [ "index", "classDigikam_1_1BatchToolSet.html#a201dd399a4b6aa20a4e83de0e8291a61", null ],
    [ "name", "classDigikam_1_1BatchToolSet.html#ad932d6e8eb1498de9dba05179186ec92", null ],
    [ "settings", "classDigikam_1_1BatchToolSet.html#a484bf8a49a9d78ff5d914c32f43ff96c", null ],
    [ "version", "classDigikam_1_1BatchToolSet.html#ab3ef4490dd57ff577cc43ff7b8b6ca17", null ]
];