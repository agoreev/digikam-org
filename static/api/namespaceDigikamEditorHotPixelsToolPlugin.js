var namespaceDigikamEditorHotPixelsToolPlugin =
[
    [ "BlackFrameListView", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListView.html", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListView" ],
    [ "BlackFrameListViewItem", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListViewItem.html", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListViewItem" ],
    [ "BlackFrameParser", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameParser.html", "classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameParser" ],
    [ "HotPixel", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixel.html", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixel" ],
    [ "HotPixelFixer", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer.html", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer" ],
    [ "HotPixelsToolPlugin", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin.html", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin" ],
    [ "HotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights" ]
];