var classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights =
[
    [ "HotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a80e0bc5d1d9347aca6056cedfcc8f014", null ],
    [ "HotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a53facc54079862d9aee4e2a22b001730", null ],
    [ "~HotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#acdb730120dce037169ca0f27f4f7d690", null ],
    [ "calculateHotPixelsWeights", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a4b242482d0d1e83257cf4ce069e6a0fe", null ],
    [ "coefficientNumber", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a3cbf8b3f812b398c5feebf6298ffd9f8", null ],
    [ "height", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a7ce5d3263e4f9bdf6b0e441aade5b84c", null ],
    [ "operator=", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a64fd8a2f4552a9e064a6cb019d3c72c0", null ],
    [ "operator==", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a8595f605f10170a12f03e23c8da5ae02", null ],
    [ "operator[]", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a211ba4cdc264f949ec75788c643337d0", null ],
    [ "polynomeOrder", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a10b28c10d85a3d9a64a43e417e85e896", null ],
    [ "positions", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#afa0bebcadcca48c1c10e7df16081ea1e", null ],
    [ "setHeight", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#ad3474cbb4a5ba36f8acc4338d9da07dc", null ],
    [ "setPolynomeOrder", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a4d2dbba158287efb2f39213767104c0e", null ],
    [ "setTwoDim", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a4df4f009a51637d9402fc4e3272f15b3", null ],
    [ "setWidth", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a366244dcc1e7258c8fcebab96441c226", null ],
    [ "twoDim", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#ade8368efcf906c2078eaa4860f568644", null ],
    [ "weightMatrices", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#ae6eebc55905ccf5baf4c81db8b0f3edf", null ],
    [ "width", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html#a18766236649902c37f4d4e658377f15f", null ]
];