var classDigikam_1_1Cascade =
[
    [ "Cascade", "classDigikam_1_1Cascade.html#a999972ea888167fb4a54cbe0610a0e2f", null ],
    [ "faceROI", "classDigikam_1_1Cascade.html#acd755f0b548767fddb30f2a09f1be848", null ],
    [ "getOriginalWindowSize", "classDigikam_1_1Cascade.html#a438791547e2d681b63fae467a5620770", null ],
    [ "isFacialFeature", "classDigikam_1_1Cascade.html#a139acae331dd97395b9df831963a694d", null ],
    [ "lessThanWindowSize", "classDigikam_1_1Cascade.html#ae3e1b5c820a5edd33f6c486243608790", null ],
    [ "minSizeForFace", "classDigikam_1_1Cascade.html#a6ccb496a04180e93a32c5efa50a9a0cb", null ],
    [ "requestedInputScaleFactor", "classDigikam_1_1Cascade.html#a8e0e096f819cbaed780eafe68707a4ba", null ],
    [ "setPrimaryCascade", "classDigikam_1_1Cascade.html#a78be7e958305355d4443eb8db6af3853", null ],
    [ "setROI", "classDigikam_1_1Cascade.html#ae8f918a4df8681a0a0926e128eabfb5d", null ],
    [ "primaryCascade", "classDigikam_1_1Cascade.html#a091d28892613a3bb6d77e95fdf200131", null ],
    [ "roi", "classDigikam_1_1Cascade.html#ac42adb8a4e86dc4db5954b413cd0c9b2", null ],
    [ "verifyingCascade", "classDigikam_1_1Cascade.html#a73e42823ef462c7aa19901509d3f4d11", null ]
];