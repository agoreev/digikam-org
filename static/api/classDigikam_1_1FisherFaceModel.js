var classDigikam_1_1FisherFaceModel =
[
    [ "FisherFaceModel", "classDigikam_1_1FisherFaceModel.html#a8b2de7957f7fcfae6fb8534c1bef15e6", null ],
    [ "~FisherFaceModel", "classDigikam_1_1FisherFaceModel.html#ac5d51072769e986c52356c92b20562b5", null ],
    [ "getLabels", "classDigikam_1_1FisherFaceModel.html#af0dbb965f0198374234694c75e9f34be", null ],
    [ "getSrc", "classDigikam_1_1FisherFaceModel.html#a9976ef35648111944e3612b0f68ac86e", null ],
    [ "matData", "classDigikam_1_1FisherFaceModel.html#ac64dc94641210b8fdaa94d9a540a1750", null ],
    [ "matMetadata", "classDigikam_1_1FisherFaceModel.html#a4cde76776f6d8a266ddeb8ac73a820e2", null ],
    [ "ptr", "classDigikam_1_1FisherFaceModel.html#a8b6acb6d8e6d727d0967f92d42b28fa3", null ],
    [ "ptr", "classDigikam_1_1FisherFaceModel.html#ae6fe5dd08244799637c08a9f2103b500", null ],
    [ "setLabels", "classDigikam_1_1FisherFaceModel.html#af7180099f28c3bfe56d86bb5112ee7b2", null ],
    [ "setMats", "classDigikam_1_1FisherFaceModel.html#a7a9cd61385199561d6a5c89229ed72d4", null ],
    [ "setSrc", "classDigikam_1_1FisherFaceModel.html#a6e3c937d7e3aab91fba1e5f293cf0084", null ],
    [ "setWrittenToDatabase", "classDigikam_1_1FisherFaceModel.html#a45ad47644c86040bb23ffd45d1ca0166", null ],
    [ "update", "classDigikam_1_1FisherFaceModel.html#a7957a58841302554950373209bb12137", null ],
    [ "m_matMetadata", "classDigikam_1_1FisherFaceModel.html#ab0312468b983e16241f63fa6c285849f", null ]
];