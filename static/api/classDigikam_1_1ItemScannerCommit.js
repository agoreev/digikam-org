var classDigikam_1_1ItemScannerCommit =
[
    [ "Operation", "classDigikam_1_1ItemScannerCommit.html#a27d800786fc534f4b9bc21d57c5caadf", [
      [ "NoOp", "classDigikam_1_1ItemScannerCommit.html#a27d800786fc534f4b9bc21d57c5caadfafaf74f47a5dade01258d4b42251b0ffd", null ],
      [ "AddItem", "classDigikam_1_1ItemScannerCommit.html#a27d800786fc534f4b9bc21d57c5caadfa11402e75723a74ece5df631aff6a13b8", null ],
      [ "UpdateItem", "classDigikam_1_1ItemScannerCommit.html#a27d800786fc534f4b9bc21d57c5caadfad6b763c769b8448ef2b3fec36a2c43f8", null ]
    ] ],
    [ "ItemScannerCommit", "classDigikam_1_1ItemScannerCommit.html#a64f5a6bd85082b13027fdddb93ec60a9", null ],
    [ "captions", "classDigikam_1_1ItemScannerCommit.html#a2a830e28b0b19afd6f5d347e10b5a185", null ],
    [ "commitFaces", "classDigikam_1_1ItemScannerCommit.html#a0e79a433106b249a245dda262862f018", null ],
    [ "commitImageMetadata", "classDigikam_1_1ItemScannerCommit.html#ab87243bf3aaf521478ea684d184468f1", null ],
    [ "commitIPTCCore", "classDigikam_1_1ItemScannerCommit.html#a2e1eb694dc6a162cf22888d4f0b359ce", null ],
    [ "commitItemComments", "classDigikam_1_1ItemScannerCommit.html#a64d7a38dce45f63fa77ee80185772823", null ],
    [ "commitItemCopyright", "classDigikam_1_1ItemScannerCommit.html#a7c693441537cf750f478ed7722713d6d", null ],
    [ "commitItemInformation", "classDigikam_1_1ItemScannerCommit.html#a25e3c2bb9fb24a5bb131da591ca75080", null ],
    [ "commitItemPosition", "classDigikam_1_1ItemScannerCommit.html#a3a9cca91b7ebf695fa5a2fd7aa074b8d", null ],
    [ "commitVideoMetadata", "classDigikam_1_1ItemScannerCommit.html#a1a511a070138ee9aec6c45387982e4b9", null ],
    [ "copyImageAttributesId", "classDigikam_1_1ItemScannerCommit.html#a23dc36a096be569e8fcafea7ef65dba3", null ],
    [ "copyrightTemplate", "classDigikam_1_1ItemScannerCommit.html#ad46713bf25c2e35df1cd02bf07f9d29e", null ],
    [ "hasColorTag", "classDigikam_1_1ItemScannerCommit.html#a7a15e43490816cd69c30fe19b4085029", null ],
    [ "hasPickTag", "classDigikam_1_1ItemScannerCommit.html#a49c1d03495c4e285ba5aea3fca09f991", null ],
    [ "headline", "classDigikam_1_1ItemScannerCommit.html#a376b29512ab8b6f260608c61dfecde36", null ],
    [ "historyXml", "classDigikam_1_1ItemScannerCommit.html#ab19e353e9ff40b0afc3a9188b1e5f17b", null ],
    [ "imageInformationFields", "classDigikam_1_1ItemScannerCommit.html#af261fcfa3002c3d4926a4361a368672e", null ],
    [ "imageInformationInfos", "classDigikam_1_1ItemScannerCommit.html#ab5a640915757b0801539f7e8f195fca3", null ],
    [ "imageMetadataInfos", "classDigikam_1_1ItemScannerCommit.html#ad14e04f005ed0b40c11e783a4db2210b", null ],
    [ "imagePositionInfos", "classDigikam_1_1ItemScannerCommit.html#a9c0a2f4a250fbdd28c5ba8c560333a4a", null ],
    [ "iptcCoreMetadataInfos", "classDigikam_1_1ItemScannerCommit.html#aab9862fc35f00d01b7ad5f83acd0ddd0", null ],
    [ "metadataFacesMap", "classDigikam_1_1ItemScannerCommit.html#a2f474adf22c65ed07c0121af1bcdc15a", null ],
    [ "operation", "classDigikam_1_1ItemScannerCommit.html#af04450dcdb09146bb317ddf062218b63", null ],
    [ "tagIds", "classDigikam_1_1ItemScannerCommit.html#a22a23272b1d54726c302d1a53260f41b", null ],
    [ "title", "classDigikam_1_1ItemScannerCommit.html#a55b0eab2fcd1551db501cc16d243fac9", null ],
    [ "uuid", "classDigikam_1_1ItemScannerCommit.html#ab036ecb6753bfead9ddf20e3072f6cb7", null ]
];