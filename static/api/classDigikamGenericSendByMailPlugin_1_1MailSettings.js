var classDigikamGenericSendByMailPlugin_1_1MailSettings =
[
    [ "ImageFormat", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae00af5b7bd3ce8b8567c56e1abdc3f89", [
      [ "JPEG", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae00af5b7bd3ce8b8567c56e1abdc3f89a59f1ceced0918bc3a81244749b1f1a5c", null ],
      [ "PNG", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae00af5b7bd3ce8b8567c56e1abdc3f89abb5422c7162166b951371b83ea773378", null ]
    ] ],
    [ "MailClient", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00", [
      [ "BALSA", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a3638043964ddc447ea3aecba3038e077", null ],
      [ "CLAWSMAIL", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a01c8612097a1ff711cfb755db565a0f8", null ],
      [ "EVOLUTION", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00aa9887778c4d32df419540cf1baed86a3", null ],
      [ "KMAIL", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a24af6ee9fcbe055b7cf49bb681e5b47c", null ],
      [ "NETSCAPE", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a9a195094d9968cb7ae59df16ed970215", null ],
      [ "OUTLOOK", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a39d847b9c47ad275153a5711f254730e", null ],
      [ "SYLPHEED", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a7d15c1e7eac3d492728c2210a7a4a36f", null ],
      [ "THUNDERBIRD", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00a3a581036d930d0244e03ad336cc8e470", null ]
    ] ],
    [ "Selection", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ac11981c5f55498ea5c73bd11f1010f3d", [
      [ "IMAGES", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ac11981c5f55498ea5c73bd11f1010f3dad2af098f7c331a895e868ed5fce399c1", null ],
      [ "ALBUMS", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ac11981c5f55498ea5c73bd11f1010f3da65b81ff42acdf8264cea70e31e49354e", null ]
    ] ],
    [ "MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a5abbb07bab4fc68751f8f8427d5df107", null ],
    [ "~MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a2bb8ab710c2a607c409b60ae38ae1825", null ],
    [ "attachementLimit", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a453c94065c3961689c92473a154d9f12", null ],
    [ "format", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a2b70a92d0b9c66eb15e8662a70b2ef0b", null ],
    [ "mailUrl", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a99670806f24c6fd4dc272fe0280274a5", null ],
    [ "readSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a3ef676cf546345db1acbd07c479a2b25", null ],
    [ "setMailUrl", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a3ac7365b034f6401dbcf17cbacc34285", null ],
    [ "writeSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a9318e63fe18c01b3f4de8f45c5a17b5f", null ],
    [ "addFileProperties", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a08abe106f8c2619698c9be30543072a0", null ],
    [ "attLimitInMbytes", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a440f3320cd463113589ee93f33b40249", null ],
    [ "binPaths", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a68cbe04344838971c5a0ce9e478b9a66", null ],
    [ "imageCompression", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#abf0313c011f1febf44202c8de7c460bd", null ],
    [ "imageFormat", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a9051dd56817c6526c8fd6ffa7b0bdea0", null ],
    [ "imagesChangeProp", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a339564682ce682efa58e0ec0c0483523", null ],
    [ "imageSize", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a56107006421d318e95b0d0a717bbe63d", null ],
    [ "inputImages", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#aa04d2ef21ec209f13f3335fc626304a4", null ],
    [ "itemsList", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ab062fd4a64f7f3fa7e8357822c6ca213", null ],
    [ "mailProgram", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a65ae9f627bbda1ca2963fd2034a3dfa1", null ],
    [ "removeMetadata", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a2fb494a97d95cbce65561ba8904706b2", null ],
    [ "selMode", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#aa925277562f1b93fd02288c1b9a6adac", null ],
    [ "tempPath", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html#a0d938c3b5bf7007efebc55724791847b", null ]
];