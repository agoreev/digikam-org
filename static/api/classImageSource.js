var classImageSource =
[
    [ "ImageSource", "classImageSource.html#a4e78c8a3729092c7449d8599f027ed93", null ],
    [ "~ImageSource", "classImageSource.html#a9d490b894a3476a51ba064c8809d7bb5", null ],
    [ "get_height", "classImageSource.html#a47ccb4f8576e7a6a5245b0c3e29d5fd2", null ],
    [ "get_image", "classImageSource.html#a56b93eb3efa63e68570ac5bc398629fd", null ],
    [ "get_width", "classImageSource.html#a6260a7bc708c6dfa8e9d8993ab391b13", null ],
    [ "skip_frames", "classImageSource.html#a7eb37cdc5ea1f02c844276e0b693fe8b", null ]
];