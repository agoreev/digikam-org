var classDigikam_1_1PreviewSettings =
[
    [ "Quality", "classDigikam_1_1PreviewSettings.html#a238383a504ca3eba24adb0f1af3de32e", [
      [ "FastPreview", "classDigikam_1_1PreviewSettings.html#a238383a504ca3eba24adb0f1af3de32ea2cd370f5461552651e1da6ba08f5afc2", null ],
      [ "FastButLargePreview", "classDigikam_1_1PreviewSettings.html#a238383a504ca3eba24adb0f1af3de32ea35001c9db57d714f942fefd710bcb3b0", null ],
      [ "HighQualityPreview", "classDigikam_1_1PreviewSettings.html#a238383a504ca3eba24adb0f1af3de32ea12670fe3858520b02d58125c29d0867c", null ]
    ] ],
    [ "RawLoading", "classDigikam_1_1PreviewSettings.html#ac8482bdc4192674313c8c56d01350003", [
      [ "RawPreviewAutomatic", "classDigikam_1_1PreviewSettings.html#ac8482bdc4192674313c8c56d01350003a8d39513910ee4281d87591084773be9c", null ],
      [ "RawPreviewFromEmbeddedPreview", "classDigikam_1_1PreviewSettings.html#ac8482bdc4192674313c8c56d01350003a671b9ed500012bdb8eb488632158fe6c", null ],
      [ "RawPreviewFromRawHalfSize", "classDigikam_1_1PreviewSettings.html#ac8482bdc4192674313c8c56d01350003a16c7efe2b0afdb43627aac9d163eac0e", null ]
    ] ],
    [ "PreviewSettings", "classDigikam_1_1PreviewSettings.html#a7e4d024ecaafcecf5ecd1dbd49a654bb", null ],
    [ "operator==", "classDigikam_1_1PreviewSettings.html#a80bc1c5de5c7587950796fd0e4648e74", null ],
    [ "convertToEightBit", "classDigikam_1_1PreviewSettings.html#a0402a381771a3835f02e4b2571f20fbe", null ],
    [ "quality", "classDigikam_1_1PreviewSettings.html#a8d89ae591e4ac4dd4e238626638c58bf", null ],
    [ "rawLoading", "classDigikam_1_1PreviewSettings.html#ac152c5718b8b0927857471eb06dc00e8", null ]
];