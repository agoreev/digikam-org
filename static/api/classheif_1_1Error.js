var classheif_1_1Error =
[
    [ "Error", "classheif_1_1Error.html#a57f17696e30cf91711d985c168c4a475", null ],
    [ "Error", "classheif_1_1Error.html#adb2acb99b8ff7e20a7ac945d42040779", null ],
    [ "Error", "classheif_1_1Error.html#a57f17696e30cf91711d985c168c4a475", null ],
    [ "Error", "classheif_1_1Error.html#a6f32886d4a96d4b5c1553f2f0edbb9e3", null ],
    [ "error_struct", "classheif_1_1Error.html#af39751ac30d9719f8d80e2e4c4b67840", null ],
    [ "get_code", "classheif_1_1Error.html#a008cead0e43a18e9ef97099be1eace63", null ],
    [ "get_message", "classheif_1_1Error.html#a464cfe4a33b77e34097fb6d447b5b62f", null ],
    [ "get_subcode", "classheif_1_1Error.html#a16981774dc39eb09ced6f942e806f36f", null ],
    [ "operator bool", "classheif_1_1Error.html#ae00cbfc152f43924d8938ddfcd0cd802", null ],
    [ "operator bool", "classheif_1_1Error.html#ae00cbfc152f43924d8938ddfcd0cd802", null ],
    [ "operator!=", "classheif_1_1Error.html#a48332cabb685e4a18051ef561b90ebfd", null ],
    [ "operator==", "classheif_1_1Error.html#ad7179b202883e77e550f115b500cfea3", null ],
    [ "error_code", "classheif_1_1Error.html#a0314b9c75c2169ac7afed22dd3f0c269", null ],
    [ "message", "classheif_1_1Error.html#a6fcbe817634b2bc110eb5876d7dbd16d", null ],
    [ "sub_error_code", "classheif_1_1Error.html#ae6b6840f4501529a84faee82c1ce3f6d", null ]
];