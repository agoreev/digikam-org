var classDigikam_1_1SetupCollectionModel_1_1Item =
[
    [ "Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html#af9a013db6286d29fd2ecbb32e0162b72", null ],
    [ "Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html#a52fa3858e28f1dff451325c4d242e5d0", null ],
    [ "Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html#af4150d71b68ce6af0e540fe66f5d4c27", null ],
    [ "deleted", "classDigikam_1_1SetupCollectionModel_1_1Item.html#afcb24b5c6263d80691b0b1d3ad6a8f8e", null ],
    [ "label", "classDigikam_1_1SetupCollectionModel_1_1Item.html#a4e5bc622f693e7a786125bdc730a638d", null ],
    [ "location", "classDigikam_1_1SetupCollectionModel_1_1Item.html#a8274b7ee01f9215d906f8b2bf2734076", null ],
    [ "parentId", "classDigikam_1_1SetupCollectionModel_1_1Item.html#acf015ec66c2a68458fc5d7f42c88a9c3", null ],
    [ "path", "classDigikam_1_1SetupCollectionModel_1_1Item.html#a2ac691012bc0edcbba7bd71ab7d76021", null ],
    [ "updated", "classDigikam_1_1SetupCollectionModel_1_1Item.html#a9c6c2bb8040acec11f2f7462d9f46b65", null ]
];