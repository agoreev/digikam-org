var structDigikam_1_1PTOType_1_1Stitcher =
[
    [ "Interpolator", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95a", [
      [ "POLY3", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa488cf0957934dc00781cd7354ba24b14", null ],
      [ "SPLINE16", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aaaac49222f1726e43094c4e69405aa214", null ],
      [ "SPLINE36", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa0bbb70176f605db63c7dc35888807162", null ],
      [ "SINC256", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa8bedfc1f36fa7d24088d7b916aa2a53e", null ],
      [ "SPLINE64", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa8b737bbe62976c17f868ec3556befd70", null ],
      [ "BILINEAR", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa7abe7f335c0f20fc2c1cfc24668614e4", null ],
      [ "NEARESTNEIGHBOR", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aa623e46b7c7bb04b5c57b7da6327bf919", null ],
      [ "SINC1024", "structDigikam_1_1PTOType_1_1Stitcher.html#acede9af32a7adacc911718025333e95aacb65251e4a2902ffb36dad97aef43f68", null ]
    ] ],
    [ "SpeedUp", "structDigikam_1_1PTOType_1_1Stitcher.html#aa21e3f61c26f729ee7ef238729ef8726", [
      [ "SLOW", "structDigikam_1_1PTOType_1_1Stitcher.html#aa21e3f61c26f729ee7ef238729ef8726a84fe80e31785af0f702a46a959832388", null ],
      [ "MEDIUM", "structDigikam_1_1PTOType_1_1Stitcher.html#aa21e3f61c26f729ee7ef238729ef8726a889a08bdb2cc6315543d80e05b78ae17", null ],
      [ "FAST", "structDigikam_1_1PTOType_1_1Stitcher.html#aa21e3f61c26f729ee7ef238729ef8726a3ad21f066d4fd744758a1faed5762bda", null ]
    ] ],
    [ "Stitcher", "structDigikam_1_1PTOType_1_1Stitcher.html#a6fe1224361f45883f6da5d9cea850f1a", null ],
    [ "gamma", "structDigikam_1_1PTOType_1_1Stitcher.html#a08e7b56495dfc02b7915aeeacb4763a6", null ],
    [ "huberSigma", "structDigikam_1_1PTOType_1_1Stitcher.html#a5fbfcb8d73d0dcddfad96dd62024b59c", null ],
    [ "interpolator", "structDigikam_1_1PTOType_1_1Stitcher.html#ad9f9d4c504a7ffb08f79d0df95df078f", null ],
    [ "photometricHuberSigma", "structDigikam_1_1PTOType_1_1Stitcher.html#a37202ef55f022c59f876dc8464807fc5", null ],
    [ "previousComments", "structDigikam_1_1PTOType_1_1Stitcher.html#a04b860df5e5ab8a89076f87b67d74fb2", null ],
    [ "speedUp", "structDigikam_1_1PTOType_1_1Stitcher.html#addd9c46c4d032e1093127aa019835db0", null ],
    [ "unmatchedParameters", "structDigikam_1_1PTOType_1_1Stitcher.html#a61c58ce9880fd99beec01c6b7abbfa17", null ]
];