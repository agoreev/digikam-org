var classDigikam_1_1OpenCVDNNFaceDetector =
[
    [ "OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html#af9ac3b9233391eca73e97ce0f9f5aae0", null ],
    [ "~OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html#a35373a183c31bd1c47464d0e56cc56fd", null ],
    [ "detectFaces", "classDigikam_1_1OpenCVDNNFaceDetector.html#a74cf8604e38c5f1b32941f75c48745e4", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#a80b08c1056f6cf0a0930edfab04a8bef", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#a27a0c6d0b9944e539c6ac5fc980ef881", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#a6e98bfed59a8a484b0e52f315033b98b", null ]
];