var classDigikam_1_1QueueSettings =
[
    [ "RawLoadingRule", "classDigikam_1_1QueueSettings.html#a5e572c168f4236d9ab98f3ee9947705c", [
      [ "USEEMBEDEDJPEG", "classDigikam_1_1QueueSettings.html#a5e572c168f4236d9ab98f3ee9947705caaa4f3a888f70df3e448dfb0a5944d00f", null ],
      [ "DEMOSAICING", "classDigikam_1_1QueueSettings.html#a5e572c168f4236d9ab98f3ee9947705ca42aea3703496e4e32313f4c2eabe6fbe", null ]
    ] ],
    [ "RenamingRule", "classDigikam_1_1QueueSettings.html#ac3e407a27ffb3fb325c5472151c427e3", [
      [ "USEORIGINAL", "classDigikam_1_1QueueSettings.html#ac3e407a27ffb3fb325c5472151c427e3a1b177b663e18ca11312ea431fb1101fa", null ],
      [ "CUSTOMIZE", "classDigikam_1_1QueueSettings.html#ac3e407a27ffb3fb325c5472151c427e3ab487443f9d1164408f753dc52324db59", null ]
    ] ],
    [ "QueueSettings", "classDigikam_1_1QueueSettings.html#a5c08f80871dfc50cb617ecc0e2aa049b", null ],
    [ "conflictRule", "classDigikam_1_1QueueSettings.html#a81d886ccaefbb5af13897cd74eac6ea1", null ],
    [ "exifSetOrientation", "classDigikam_1_1QueueSettings.html#a9b93c7165b8daa580d7743adb7904070", null ],
    [ "ioFileSettings", "classDigikam_1_1QueueSettings.html#a61559a09e8a7c2fd9e578a7240052a87", null ],
    [ "rawDecodingSettings", "classDigikam_1_1QueueSettings.html#a93a4f31a9fdbc9898dd5dfa2d1e0166e", null ],
    [ "rawLoadingRule", "classDigikam_1_1QueueSettings.html#a296d8fa4e8bffef455f10acce35ef748", null ],
    [ "renamingParser", "classDigikam_1_1QueueSettings.html#a309c0a8501f22fa96c5513b200107a70", null ],
    [ "renamingRule", "classDigikam_1_1QueueSettings.html#a568cbbcf876cc6ad02d3129e2c4cf502", null ],
    [ "useMultiCoreCPU", "classDigikam_1_1QueueSettings.html#a689c42f07276abbe2ad33c7e8fb4e299", null ],
    [ "useOrgAlbum", "classDigikam_1_1QueueSettings.html#a33dbf5672b2cbece7eebc56a8c878242", null ],
    [ "workingUrl", "classDigikam_1_1QueueSettings.html#a9860b1c3fcd7d0df85797a07c4f80977", null ]
];