var structDigikamGenericRajcePlugin_1_1RajceAlbum =
[
    [ "RajceAlbum", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a0b65b77b11c9cef6301cb3197807e78f", null ],
    [ "bestQualityThumbUrl", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a355ff585d278b850bc4e53796c513b6b", null ],
    [ "createDate", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a46590ce02fe206b8d445c02d66a4eb52", null ],
    [ "description", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a68b435d348b702414693adf35b10408c", null ],
    [ "id", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#ac7c831b0eda1066ec5d9fbf4f3040f26", null ],
    [ "isHidden", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a93f31febdaa77c0cac46bc18ce52a2fb", null ],
    [ "isSecure", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#aaa590b43acbad514a42f9be27e37a400", null ],
    [ "name", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a74d8423b7f42a1450e8c38e52ac3f3d3", null ],
    [ "photoCount", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#af862c644b2a3a27671f1ad42ba0b00a0", null ],
    [ "thumbUrl", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#adc34b24a4af3c75b1cd3a0d4de2e4bf9", null ],
    [ "updateDate", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#af8a4ca752188eb26c6ea8a995149e885", null ],
    [ "url", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a29515f1023bc076b9c6e829bd181bd91", null ],
    [ "validFrom", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a8fa48e8828580ba16c7375133506f549", null ],
    [ "validTo", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html#a9ff131213b01aefc962a681bf344a92c", null ]
];