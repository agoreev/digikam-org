var namespaceDigikamGenericUnifiedPlugin =
[
    [ "UnifiedPlugin", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin.html", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin" ],
    [ "WSAuthenticationPage", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage" ],
    [ "WSAuthenticationPageView", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView" ],
    [ "WSTalker", "classDigikamGenericUnifiedPlugin_1_1WSTalker.html", "classDigikamGenericUnifiedPlugin_1_1WSTalker" ]
];