var structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction =
[
    [ "account", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#aa8fda0924fd605ad8902c315134b0811", null ],
    [ "description", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a6d02850677c8501041f96051eac41179", null ],
    [ "imgpath", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a3b493e642aedf472912c7552e6bba2d9", null ],
    [ "title", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a04188cbf8e4283ed438d9f8b44f4a0bc", null ],
    [ "type", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a59c54e586131c9a61bdb712bf4cf523a", null ],
    [ "upload", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#aca0d0f1aed11be55b04548c8d6177f80", null ],
    [ "username", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html#a6ae216ee8fc4edfcb5f0f4b924512b6c", null ]
];