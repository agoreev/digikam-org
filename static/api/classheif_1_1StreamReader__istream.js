var classheif_1_1StreamReader__istream =
[
    [ "grow_status", "classheif_1_1StreamReader__istream.html#a053ecfc73f3aa86fd7124c67e07b293d", [
      [ "size_reached", "classheif_1_1StreamReader__istream.html#a053ecfc73f3aa86fd7124c67e07b293daa01031f5674c88e9c7a7cdf725052206", null ],
      [ "timeout", "classheif_1_1StreamReader__istream.html#a053ecfc73f3aa86fd7124c67e07b293da5c5fb607a90f14d46a00f957d246f2bb", null ],
      [ "size_beyond_eof", "classheif_1_1StreamReader__istream.html#a053ecfc73f3aa86fd7124c67e07b293da02f265b40741b4b568a9e60285098657", null ]
    ] ],
    [ "StreamReader_istream", "classheif_1_1StreamReader__istream.html#ac1707791d1d8711cc882186060e55c20", null ],
    [ "get_position", "classheif_1_1StreamReader__istream.html#ac7303a2c2166622ec7c582ac0f51354f", null ],
    [ "read", "classheif_1_1StreamReader__istream.html#abf28f8cb0444853b64084e85fb2ea929", null ],
    [ "seek", "classheif_1_1StreamReader__istream.html#a4d2aa27b7c41dc4535b425b9dd8b4ca2", null ],
    [ "seek_cur", "classheif_1_1StreamReader__istream.html#a5fef44e1d5988070c93b1cfdd3ad021f", null ],
    [ "wait_for_file_size", "classheif_1_1StreamReader__istream.html#a6337bb39740edf386064dfe72efe8e20", null ]
];