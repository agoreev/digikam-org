var classDigikam_1_1TableViewColumnConfiguration =
[
    [ "TableViewColumnConfiguration", "classDigikam_1_1TableViewColumnConfiguration.html#af94172d34f04f4f278af5d05ffa0446b", null ],
    [ "getSetting", "classDigikam_1_1TableViewColumnConfiguration.html#a2177de146651321359c71f9d857a7e5e", null ],
    [ "loadSettings", "classDigikam_1_1TableViewColumnConfiguration.html#a8551e130aee0d8105c00f39952306ec2", null ],
    [ "saveSettings", "classDigikam_1_1TableViewColumnConfiguration.html#a834592fb6c4f5cad127e078316d66b4b", null ],
    [ "columnId", "classDigikam_1_1TableViewColumnConfiguration.html#ab16368ba18c6146a466b26fb3bb9ab2e", null ],
    [ "columnSettings", "classDigikam_1_1TableViewColumnConfiguration.html#a7d743c185508ea93b5ff780cc5db152e", null ]
];