var classDigikam_1_1ItemFilterSettings =
[
    [ "GeolocationCondition", "classDigikam_1_1ItemFilterSettings.html#a10a7d0c8128c43580dc27f1a85379cfd", [
      [ "GeolocationNoFilter", "classDigikam_1_1ItemFilterSettings.html#a10a7d0c8128c43580dc27f1a85379cfdad3d51280d26b6da51e54de1e7258321d", null ],
      [ "GeolocationNoCoordinates", "classDigikam_1_1ItemFilterSettings.html#a10a7d0c8128c43580dc27f1a85379cfda08eb566c43cc2d14b128c39ea364a714", null ],
      [ "GeolocationHasCoordinates", "classDigikam_1_1ItemFilterSettings.html#a10a7d0c8128c43580dc27f1a85379cfdaf0ac492fa5e660beb6a6aefc0e9bc895", null ]
    ] ],
    [ "MatchingCondition", "classDigikam_1_1ItemFilterSettings.html#afdc0f59869c796fe2bd76824578f6568", [
      [ "OrCondition", "classDigikam_1_1ItemFilterSettings.html#afdc0f59869c796fe2bd76824578f6568a55ed3ba049f81587f051a4a25f064d50", null ],
      [ "AndCondition", "classDigikam_1_1ItemFilterSettings.html#afdc0f59869c796fe2bd76824578f6568a68c82a85655a9d42b5d31a40256e19c5", null ]
    ] ],
    [ "RatingCondition", "classDigikam_1_1ItemFilterSettings.html#a7a8d0fb01744ac1621cd9d35f43d011d", [
      [ "GreaterEqualCondition", "classDigikam_1_1ItemFilterSettings.html#a7a8d0fb01744ac1621cd9d35f43d011da44edbc2979d1890dd8b87a56a58f1341", null ],
      [ "EqualCondition", "classDigikam_1_1ItemFilterSettings.html#a7a8d0fb01744ac1621cd9d35f43d011dab7f08d0894c4c863a25f18f52fcf5159", null ],
      [ "LessEqualCondition", "classDigikam_1_1ItemFilterSettings.html#a7a8d0fb01744ac1621cd9d35f43d011daaccd9bc116d54874a085ac8c13e92436", null ]
    ] ],
    [ "ItemFilterSettings", "classDigikam_1_1ItemFilterSettings.html#a61a29f4c69d8713a0750926d7ee55b54", null ],
    [ "isFiltering", "classDigikam_1_1ItemFilterSettings.html#aa1fb77d9f9338c45bfa0a1d81da72d96", null ],
    [ "isFilteringByColorLabels", "classDigikam_1_1ItemFilterSettings.html#a001873ca330b17523ddf3f6d1220062c", null ],
    [ "isFilteringByDay", "classDigikam_1_1ItemFilterSettings.html#a10e8b8abdb79b3bed2b91abd3da50ec4", null ],
    [ "isFilteringByGeolocation", "classDigikam_1_1ItemFilterSettings.html#a20b6a2b0086019add09aeccf3d8a8189", null ],
    [ "isFilteringByPickLabels", "classDigikam_1_1ItemFilterSettings.html#aa96670954b3cf74e62514e29b7628698", null ],
    [ "isFilteringByRating", "classDigikam_1_1ItemFilterSettings.html#a0672eeea1848e5e9023c007234a7bf4c", null ],
    [ "isFilteringByTags", "classDigikam_1_1ItemFilterSettings.html#a74fd8afc9c8a5aac94d0cb4d7c38576f", null ],
    [ "isFilteringByText", "classDigikam_1_1ItemFilterSettings.html#a20c6e2957f7b408c4ca30f87939ebfad", null ],
    [ "isFilteringByTypeMime", "classDigikam_1_1ItemFilterSettings.html#ac888f5c47c044e29dd32fad1c82e2d5c", null ],
    [ "matches", "classDigikam_1_1ItemFilterSettings.html#ac297049697d80ce21d614f7bc93605e8", null ],
    [ "setAlbumNames", "classDigikam_1_1ItemFilterSettings.html#afc4889a25353de3f73f68f6ad3679849", null ],
    [ "setDayFilter", "classDigikam_1_1ItemFilterSettings.html#ab0e917b8137fbe219d6fbfde286ccc0d", null ],
    [ "setGeolocationFilter", "classDigikam_1_1ItemFilterSettings.html#ae2b41e11409ac52b778d7ec9d71c65ec", null ],
    [ "setIdWhitelist", "classDigikam_1_1ItemFilterSettings.html#a5946b1df0b75e92a2a6a66f03b8f8ab5", null ],
    [ "setMimeTypeFilter", "classDigikam_1_1ItemFilterSettings.html#a3feaf13cb1dbb4b2cb3246f311639519", null ],
    [ "setRatingFilter", "classDigikam_1_1ItemFilterSettings.html#a7d36a28479bc0b2573a2a0658c6d5f2e", null ],
    [ "setTagFilter", "classDigikam_1_1ItemFilterSettings.html#a080bad0d234af28e03b694a7fa644f48", null ],
    [ "setTagNames", "classDigikam_1_1ItemFilterSettings.html#ab2e073e4d121d3c1d9fe47d9cf7b80cb", null ],
    [ "setTextFilter", "classDigikam_1_1ItemFilterSettings.html#a5edb918f6ba278d70f2de800ab7d9b49", null ],
    [ "setUrlWhitelist", "classDigikam_1_1ItemFilterSettings.html#a9a82e1477b84540cd1f2f87127a1a1a4", null ],
    [ "watchFlags", "classDigikam_1_1ItemFilterSettings.html#aae7c8df0153ef992f728647974170b1b", null ]
];