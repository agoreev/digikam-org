var classDigikam_1_1ItemHistoryGraph =
[
    [ "HistoryLoadingFlag", "classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22", [
      [ "LoadRelationCloud", "classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22ac3ec9cb6707fe3a7c940d3d8fb18ba53", null ],
      [ "LoadSubjectHistory", "classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22a2d5e6b0408b5fe665232484db74898b3", null ],
      [ "LoadLeavesHistory", "classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22aad858c6a434dfb8fe98793b975c3dc45", null ],
      [ "LoadAll", "classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22a1a9d2d8bf1692af4a7c3a16c954055a4", null ]
    ] ],
    [ "ProcessingMode", "classDigikam_1_1ItemHistoryGraph.html#a6a3c6a15af9bdbd42e21464f3ae07e8f", [
      [ "NoProcessing", "classDigikam_1_1ItemHistoryGraph.html#a6a3c6a15af9bdbd42e21464f3ae07e8fa8cc098ecbe9f40555e379933a227cf79", null ],
      [ "PrepareForDisplay", "classDigikam_1_1ItemHistoryGraph.html#a6a3c6a15af9bdbd42e21464f3ae07e8fabe2db3a0a75167eded4909079728c47b", null ]
    ] ],
    [ "ItemHistoryGraph", "classDigikam_1_1ItemHistoryGraph.html#a6e8dd68f367cf7826d3d6f95996c7358", null ],
    [ "ItemHistoryGraph", "classDigikam_1_1ItemHistoryGraph.html#aab6977c645ce16d56c4b0f4fb51bd3aa", null ],
    [ "~ItemHistoryGraph", "classDigikam_1_1ItemHistoryGraph.html#af00877baeba67d6a7f5beac2872732af", null ],
    [ "addHistory", "classDigikam_1_1ItemHistoryGraph.html#a9b179fbc5867a8738d450362c6d0e2f4", null ],
    [ "addHistory", "classDigikam_1_1ItemHistoryGraph.html#a900fd054108ec97f9c6b5391c767bbc4", null ],
    [ "addRelations", "classDigikam_1_1ItemHistoryGraph.html#a4d9532c1f1a4febe3a0b2c0322690eb3", null ],
    [ "addScannedHistory", "classDigikam_1_1ItemHistoryGraph.html#a2ceef0013a3e6354390bba25b76c2abc", null ],
    [ "allImageIds", "classDigikam_1_1ItemHistoryGraph.html#a235a596fd9b35bd84ef8650fbc0ab146", null ],
    [ "allImages", "classDigikam_1_1ItemHistoryGraph.html#ac3b6b625c2e48e7dec9137676a20bd7f", null ],
    [ "categorize", "classDigikam_1_1ItemHistoryGraph.html#a176e8dccae5dc5159b04a6c049c7f919", null ],
    [ "clear", "classDigikam_1_1ItemHistoryGraph.html#a4d9dfc0624f1cd3ea0ea784dfd882bdf", null ],
    [ "data", "classDigikam_1_1ItemHistoryGraph.html#ab6d1bd0b757b3e5a3260438533e3bee6", null ],
    [ "data", "classDigikam_1_1ItemHistoryGraph.html#a70ad2a87d1466f531b320d36b35deb31", null ],
    [ "dropUnresolvedEntries", "classDigikam_1_1ItemHistoryGraph.html#a4bea753279adf9f015c0e5aecd9c9e2f", null ],
    [ "hasEdges", "classDigikam_1_1ItemHistoryGraph.html#a2147eb39e61a31b68149e5844f16cee8", null ],
    [ "hasUnresolvedEntries", "classDigikam_1_1ItemHistoryGraph.html#aad1019853293b60ba10b360772ede10c", null ],
    [ "isEmpty", "classDigikam_1_1ItemHistoryGraph.html#af6ef314426af19085c04e6a8e8226387", null ],
    [ "isNull", "classDigikam_1_1ItemHistoryGraph.html#aa3f9eda5e7df69a9ce1801ce895e0b7a", null ],
    [ "isSingleVertex", "classDigikam_1_1ItemHistoryGraph.html#a4e54794366703a1feac8219372925522", null ],
    [ "leafImages", "classDigikam_1_1ItemHistoryGraph.html#a4c45b05d5713353ceee12041f4b9c198", null ],
    [ "operator=", "classDigikam_1_1ItemHistoryGraph.html#a4535f0d7ab228d6ffd645f1dad42e02d", null ],
    [ "prepareForDisplay", "classDigikam_1_1ItemHistoryGraph.html#a82f1b443f7b005ac3e8820ad17a6a86f", null ],
    [ "reduceEdges", "classDigikam_1_1ItemHistoryGraph.html#a4a46165f6a052bd98c6d227b85ee9ae9", null ],
    [ "relationCloud", "classDigikam_1_1ItemHistoryGraph.html#a5e6a8be41006eb62eeb673c383e24d75", null ],
    [ "relationCloudParallel", "classDigikam_1_1ItemHistoryGraph.html#a21a3542c6e57ea89c33aa3205dfc6bbc", null ],
    [ "rootImages", "classDigikam_1_1ItemHistoryGraph.html#a5742f4ce93bf02cce56e570e26d06912", null ],
    [ "sortForInfo", "classDigikam_1_1ItemHistoryGraph.html#a11e2acb6f8cf5ac7e73e692f21ece6fb", null ]
];