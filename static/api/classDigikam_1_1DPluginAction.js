var classDigikam_1_1DPluginAction =
[
    [ "ActionCategory", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ad", [
      [ "InvalidCat", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada58a1363aa6bdeb351252eb30f1a49af3", null ],
      [ "GenericExport", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824adaab29da73725b0cdb4d42cdc81ec9893d", null ],
      [ "GenericImport", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada0293d613a11de3a31046284f0e3160d8", null ],
      [ "GenericTool", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada7ddd645366677c2b1e75fdf087446495", null ],
      [ "GenericMetadata", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada76fb0d466cdef7339f87a3c24fde4371", null ],
      [ "GenericView", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada4a0b17f1a5c68c26487caf18ecf0f5ae", null ],
      [ "EditorFile", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada16c3cb197b19c3520a7b983de1ecaa2d", null ],
      [ "EditorColors", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada92f8c57b9f1c75e9f13d87555027e360", null ],
      [ "EditorEnhance", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada4f859136063712815372e529fe3f1d5d", null ],
      [ "EditorTransform", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824adae4ee6bdaaf9f6ad76e2a1f8697181964", null ],
      [ "EditorDecorate", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824ada6b3924508c6e208dea382acd96537adc", null ],
      [ "EditorFilters", "classDigikam_1_1DPluginAction.html#a31088879b4d46201d492d155be3824adabadd78113920fdd037d60d6b8f2e5d6f", null ]
    ] ],
    [ "ActionType", "classDigikam_1_1DPluginAction.html#a3266f4b72acda93770f3d5a119882885", [
      [ "InvalidType", "classDigikam_1_1DPluginAction.html#a3266f4b72acda93770f3d5a119882885a0bc61c0d96e3b8c46e7f50006edb2101", null ],
      [ "Generic", "classDigikam_1_1DPluginAction.html#a3266f4b72acda93770f3d5a119882885a9a6dac3faca8ae26e77d82c6116e41ad", null ],
      [ "Editor", "classDigikam_1_1DPluginAction.html#a3266f4b72acda93770f3d5a119882885ab2a923681fa504cfbd15d933bd9f8696", null ]
    ] ],
    [ "DPluginAction", "classDigikam_1_1DPluginAction.html#ad333561bf02181f4f406196250c946b5", null ],
    [ "~DPluginAction", "classDigikam_1_1DPluginAction.html#ab809d366988488168d4aa5fef2f26084", null ],
    [ "actionCategory", "classDigikam_1_1DPluginAction.html#a1539df4f05b06ae7c6fdebd239f84562", null ],
    [ "actionCategoryToString", "classDigikam_1_1DPluginAction.html#a4edfa85a2c42af85a27513c5c5efc347", null ],
    [ "actionType", "classDigikam_1_1DPluginAction.html#ac3ab1b84e5abd475c3af3b37dacf4be0", null ],
    [ "pluginId", "classDigikam_1_1DPluginAction.html#a7232c17a5c05968d2d1c4e9a6a23f918", null ],
    [ "setActionCategory", "classDigikam_1_1DPluginAction.html#ad509182992a66421685a73619b9ba59d", null ],
    [ "toString", "classDigikam_1_1DPluginAction.html#a95fb777d908df9bd39636615815872d9", null ],
    [ "xmlSection", "classDigikam_1_1DPluginAction.html#a8e0bbb45f7dcf8d88f331c5082fecc7f", null ]
];