var classheif_1_1HeifContext_1_1Image =
[
    [ "Image", "classheif_1_1HeifContext_1_1Image.html#a5de81a893275839a515ac16ae879d040", null ],
    [ "~Image", "classheif_1_1HeifContext_1_1Image.html#ace22b92b6002b6977fd6f2620dea4423", null ],
    [ "add_metadata", "classheif_1_1HeifContext_1_1Image.html#a5182eb758448ca5573ce8d0d043b6362", null ],
    [ "add_thumbnail", "classheif_1_1HeifContext_1_1Image.html#acef57b04395b044fc3ba09d2185254f2", null ],
    [ "decode_image", "classheif_1_1HeifContext_1_1Image.html#a859dbe6a0040768e623342f3be3b99a2", null ],
    [ "encode_image_as_hevc", "classheif_1_1HeifContext_1_1Image.html#af2f9ce396d2298f13b6628f0f72a3b03", null ],
    [ "get_alpha_channel", "classheif_1_1HeifContext_1_1Image.html#ad426fd4dabd53b0a55e3ef411efeaa9b", null ],
    [ "get_chroma_bits_per_pixel", "classheif_1_1HeifContext_1_1Image.html#a5a8f98380e6bf5de6124cf6405bbbe6b", null ],
    [ "get_color_profile", "classheif_1_1HeifContext_1_1Image.html#a34a9634a82bf25824af8f9c7834374b7", null ],
    [ "get_depth_channel", "classheif_1_1HeifContext_1_1Image.html#a629055006ec99992293bb505d3068b6a", null ],
    [ "get_depth_representation_info", "classheif_1_1HeifContext_1_1Image.html#af84cd90fa7b119e2c13f577df9b16244", null ],
    [ "get_error", "classheif_1_1HeifContext_1_1Image.html#a8c12fae5ee280fd3a29ceac0e90d20eb", null ],
    [ "get_height", "classheif_1_1HeifContext_1_1Image.html#a0e79bb7f0ca61a9af658d4a3517c46af", null ],
    [ "get_id", "classheif_1_1HeifContext_1_1Image.html#a2ed183fe8d20b2c988bd252e7ef65205", null ],
    [ "get_ispe_height", "classheif_1_1HeifContext_1_1Image.html#ad37300f4ee418cbbe2f1d1c0b71ae4ec", null ],
    [ "get_ispe_width", "classheif_1_1HeifContext_1_1Image.html#a296074dcfa8f7bef159f464fdaccbc1d", null ],
    [ "get_luma_bits_per_pixel", "classheif_1_1HeifContext_1_1Image.html#ab381819ee411721dbdc2254f23d7ffd8", null ],
    [ "get_metadata", "classheif_1_1HeifContext_1_1Image.html#aa1f0d5977dcb160268cb96ef94d5f682", null ],
    [ "get_thumbnails", "classheif_1_1HeifContext_1_1Image.html#aa56c1bb4d04514fad7e6059a8c58d6ad", null ],
    [ "get_width", "classheif_1_1HeifContext_1_1Image.html#a254d68aca939fe3583d4a9e101e1fdb2", null ],
    [ "has_depth_representation_info", "classheif_1_1HeifContext_1_1Image.html#a3c7b504a28158f8b2a037dcf58b8d2f0", null ],
    [ "is_alpha_channel", "classheif_1_1HeifContext_1_1Image.html#a20c9df2d3e2dc667bd99adec23737f44", null ],
    [ "is_depth_channel", "classheif_1_1HeifContext_1_1Image.html#a39054a1f213ef97ac39fde3e00e80b46", null ],
    [ "is_primary", "classheif_1_1HeifContext_1_1Image.html#a4b60cd7aaed5345fdcbb1207e0a8ce96", null ],
    [ "is_thumbnail", "classheif_1_1HeifContext_1_1Image.html#a659998fe0d4b90afac7895a9136f7d75", null ],
    [ "set_alpha_channel", "classheif_1_1HeifContext_1_1Image.html#ae4f63f66a47576574b2210259e05dc15", null ],
    [ "set_color_profile", "classheif_1_1HeifContext_1_1Image.html#af29daed6a2988b8ccc5dd80709bb3512", null ],
    [ "set_depth_channel", "classheif_1_1HeifContext_1_1Image.html#a4da13eb2db6fb1a1b85dcfee3a809d25", null ],
    [ "set_depth_representation_info", "classheif_1_1HeifContext_1_1Image.html#aedf668dc32afbdccfcd25e7aa469451e", null ],
    [ "set_error", "classheif_1_1HeifContext_1_1Image.html#a045bafb305d9ee539dfe990aef6c4bbc", null ],
    [ "set_is_alpha_channel_of", "classheif_1_1HeifContext_1_1Image.html#a23352234ef686125efc8dd0414854b25", null ],
    [ "set_is_depth_channel_of", "classheif_1_1HeifContext_1_1Image.html#a9c816d7c68ebe608de444e19afc43240", null ],
    [ "set_is_thumbnail_of", "classheif_1_1HeifContext_1_1Image.html#ad859571cd453db88ed15aaa3724d3455", null ],
    [ "set_ispe_resolution", "classheif_1_1HeifContext_1_1Image.html#add79a35b11bd3c20ffce0e109784feef", null ],
    [ "set_preencoded_hevc_image", "classheif_1_1HeifContext_1_1Image.html#a40f958b1aca7fc1fe2341b481edb49a9", null ],
    [ "set_primary", "classheif_1_1HeifContext_1_1Image.html#a0d749878052bee3cc00d5d8ea7444fdb", null ],
    [ "set_resolution", "classheif_1_1HeifContext_1_1Image.html#adf703f651e917554cb992fad8953d481", null ],
    [ "set_success", "classheif_1_1HeifContext_1_1Image.html#aa9d2e67bb67ab7ce7ba95cd0524d4c1c", null ]
];