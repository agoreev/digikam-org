var classDigikamGenericPresentationPlugin_1_1BlendKBEffect =
[
    [ "Type", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#ae59a33ea693d708fb55f96866dcfb82d", [
      [ "Fade", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#ae59a33ea693d708fb55f96866dcfb82da38c5123803aafd0abb031760015399c1", null ],
      [ "Blend", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#ae59a33ea693d708fb55f96866dcfb82da9d08f56b35621ecea24131f362d8019e", null ]
    ] ],
    [ "BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a0db666b8a2bee0eb1f6715b95e640457", null ],
    [ "~BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a27ddadb42392055ba18ed86c9d7d843b", null ],
    [ "advanceTime", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#aac8d1fbf6fcac9088f495d3249844bf9", null ],
    [ "done", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a08bfe2bde096b4ecb8cb6ad9fb273018", null ],
    [ "fadeIn", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a0728df20c3286680bdae75dbe24af88f", null ],
    [ "image", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a6f8da4c219b308a9f5c49406303c1102", null ],
    [ "setupNewImage", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#af5f8ade4377a03bb7782b26dfa0354ff", null ],
    [ "swapImages", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a24b94cd2df28a24aa6c79cc15584481e", null ],
    [ "type", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a571f1fb17b03e3f523d596a119c42a0c", null ],
    [ "m_img", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#a953470df139168e2a3dc1bf2804cf270", null ],
    [ "m_needFadeIn", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html#aedf385b892130b54820685b741e1d406", null ]
];