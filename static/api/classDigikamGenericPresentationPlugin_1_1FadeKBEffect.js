var classDigikamGenericPresentationPlugin_1_1FadeKBEffect =
[
    [ "Type", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#ae59a33ea693d708fb55f96866dcfb82d", [
      [ "Fade", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#ae59a33ea693d708fb55f96866dcfb82da38c5123803aafd0abb031760015399c1", null ],
      [ "Blend", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#ae59a33ea693d708fb55f96866dcfb82da9d08f56b35621ecea24131f362d8019e", null ]
    ] ],
    [ "FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#abfef0e6591824e27dbc094dde656c136", null ],
    [ "~FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a1c71482ecf6c0beccc113de8c96ca87d", null ],
    [ "advanceTime", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a4942cdb56f5a8df4484fd670fe454a7a", null ],
    [ "done", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#ab511450102a6705d0389422a16a541eb", null ],
    [ "fadeIn", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a0728df20c3286680bdae75dbe24af88f", null ],
    [ "image", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a6f8da4c219b308a9f5c49406303c1102", null ],
    [ "setupNewImage", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#af5f8ade4377a03bb7782b26dfa0354ff", null ],
    [ "swapImages", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a24b94cd2df28a24aa6c79cc15584481e", null ],
    [ "type", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a5ff4478888395c06224b5058362c990f", null ],
    [ "m_img", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#a953470df139168e2a3dc1bf2804cf270", null ],
    [ "m_needFadeIn", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html#aedf385b892130b54820685b741e1d406", null ]
];