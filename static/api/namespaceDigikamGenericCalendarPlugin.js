var namespaceDigikamGenericCalendarPlugin =
[
    [ "CalendarPlugin", "classDigikamGenericCalendarPlugin_1_1CalendarPlugin.html", "classDigikamGenericCalendarPlugin_1_1CalendarPlugin" ],
    [ "CalIntroPage", "classDigikamGenericCalendarPlugin_1_1CalIntroPage.html", "classDigikamGenericCalendarPlugin_1_1CalIntroPage" ],
    [ "CalParams", "structDigikamGenericCalendarPlugin_1_1CalParams.html", "structDigikamGenericCalendarPlugin_1_1CalParams" ],
    [ "CalSystem", "classDigikamGenericCalendarPlugin_1_1CalSystem.html", "classDigikamGenericCalendarPlugin_1_1CalSystem" ],
    [ "CalWidget", "classDigikamGenericCalendarPlugin_1_1CalWidget.html", "classDigikamGenericCalendarPlugin_1_1CalWidget" ]
];