var structheif__reader =
[
    [ "get_position", "structheif__reader.html#a3e71124e94480c8e210920a052c72563", null ],
    [ "read", "structheif__reader.html#a4f9003cb1f91cdd62f3a31bcccdff8c4", null ],
    [ "reader_api_version", "structheif__reader.html#a0f6942812196bb7454f0690eb1650afb", null ],
    [ "seek", "structheif__reader.html#a95f01b9f0a9dfd2a15976d846add3a9a", null ],
    [ "wait_for_file_size", "structheif__reader.html#a3c4b780f711c0003b7d620c3e59df166", null ]
];