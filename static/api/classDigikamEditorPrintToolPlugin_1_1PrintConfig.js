var classDigikamEditorPrintToolPlugin_1_1PrintConfig =
[
    [ "~PrintConfig", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a2fce4a63f1d8d38ff8c212746a677883", null ],
    [ "PrintConfig", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#af58002641fbfb95b229727b2162e3b75", null ],
    [ "PrintConfigHelper", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a0b34fa93a2d86463476e7fb78ac6679f", null ],
    [ "mPrintAutoRotate", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a69a684f1962565d7efed42179425fda9", null ],
    [ "mPrintColorManaged", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a508ff37a3af9ccfa0118f74f7d18941c", null ],
    [ "mPrintEnlargeSmallerImages", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a1663da08da2f12a7a237546f13c82386", null ],
    [ "mPrintHeight", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a549193755a8dc10dca91c686b32e51df", null ],
    [ "mPrintKeepRatio", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a48d0aa90ba0f16ee8ae18516cec23f4c", null ],
    [ "mPrintPosition", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a1bc962523ef9b0d2f115870250bc85f3", null ],
    [ "mPrintScaleMode", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a3d8c428ffd3c268a39404d7bafb23f93", null ],
    [ "mPrintUnit", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#ae3a9cf67f56426c22c01ef1c6656e47e", null ],
    [ "mPrintWidth", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html#a914d27a1d10e24783163723923d1e848", null ]
];