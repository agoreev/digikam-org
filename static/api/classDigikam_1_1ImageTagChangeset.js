var classDigikam_1_1ImageTagChangeset =
[
    [ "Operation", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6", [
      [ "Unknown", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6ae3406ef7b94fb4ddf18ab375370fe840", null ],
      [ "Added", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6a2c7247fb36e432b89f04343d00ad8790", null ],
      [ "Moved", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6a888cd3e2e1da303d7efc27f392f7d562", null ],
      [ "Removed", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6a704e3418fc6ed1c0d5d371b3dfd9d45b", null ],
      [ "RemovedAll", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6a625e26bc238c79c923aa49c17f6bb305", null ],
      [ "PropertiesChanged", "classDigikam_1_1ImageTagChangeset.html#a854403ec9154afe5fb2790991d9abbf6ac2755d4d995e4ffdd4b5313080976334", null ]
    ] ],
    [ "ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html#acc4d621b96316ae68b31cf3bf5d65d3d", null ],
    [ "ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html#a3ef9310346b8128eba78831d1c95e146", null ],
    [ "ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html#a4dd15620c566e2b03f80e17d1c08c21b", null ],
    [ "ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html#ae61283689714e8ae358a81fbbd78b8a1", null ],
    [ "containsImage", "classDigikam_1_1ImageTagChangeset.html#a154a2786a88253f959387ea7ea517a8d", null ],
    [ "containsTag", "classDigikam_1_1ImageTagChangeset.html#a88f5df2cb9761e4bef44f3799d82e207", null ],
    [ "ids", "classDigikam_1_1ImageTagChangeset.html#a2141d08896aaa4d916088144fea8f9ab", null ],
    [ "operation", "classDigikam_1_1ImageTagChangeset.html#a72179a59f23d22e19fcb880211c33828", null ],
    [ "operator<<", "classDigikam_1_1ImageTagChangeset.html#abf864dd39b8dd9c7a00ef7a03afbde9f", null ],
    [ "propertiesWereChanged", "classDigikam_1_1ImageTagChangeset.html#aead6d5b8b4b7c1ee7095c31a06823f29", null ],
    [ "tags", "classDigikam_1_1ImageTagChangeset.html#a7c960eaf18b99e621f9b92183b21ae0a", null ],
    [ "tagsWereAdded", "classDigikam_1_1ImageTagChangeset.html#a2e0f3723894856266e08b4e7d925141c", null ],
    [ "tagsWereRemoved", "classDigikam_1_1ImageTagChangeset.html#a7fcb457b6bd0bb43cc48f06905b0da9f", null ]
];