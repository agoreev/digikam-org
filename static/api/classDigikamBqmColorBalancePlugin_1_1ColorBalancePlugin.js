var classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin =
[
    [ "ColorBalancePlugin", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a4a8d5dc501d296b78fbbfad333533b49", null ],
    [ "~ColorBalancePlugin", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a0fd6103114c71cc09e0fa3fc0f6015d8", null ],
    [ "addTool", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#aada8336ba5b96006673a89244ab7f4fc", null ],
    [ "authors", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a610f2626c000a43cc817f6f0406f9033", null ],
    [ "categories", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#ace50667ec58d94b184bb064cf897c031", null ],
    [ "cleanUp", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#aafc9ca722aabb57dfbfc3845d230adad", null ],
    [ "description", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#aa838ad7f5f969d83a81d172e7aa1bd6f", null ],
    [ "details", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a68bfb2462883ac4c8924b7f9d255a480", null ],
    [ "extraAboutData", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findToolByName", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a2cc55f4c57b24b392c510fda76224b57", null ],
    [ "hasVisibilityProperty", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a5428e743adbd7d4c5d4258af04ad591e", null ],
    [ "icon", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#ae48b2c4d7aef43434952e785ec44c70d", null ],
    [ "ifaceIid", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a333f538f23a6d798bf35ddac6f8b33e8", null ],
    [ "iid", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a7da92bc770d4efda6ef44ae8bc615064", null ],
    [ "libraryFileName", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#aaf9f1db174ec3d8bfc84530895f6abab", null ],
    [ "pluginAuthors", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a285e718c1b0fde7f076bf5a52b93cfd7", null ],
    [ "setLibraryFileName", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#ac905eb8530333569ac9459d6533db4d3", null ],
    [ "setVisible", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a634d1026d4b7abcb1d131c34cfa4aca3", null ],
    [ "shouldLoaded", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "signalVisible", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a6be70fd2b36cbd87e78741c349eb30d8", null ],
    [ "tools", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#aeb5a12dd500c1106b4f1f7b7e98fa837", null ],
    [ "version", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "libraryFileName", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ],
    [ "tools", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html#a08ecb65fb679e3490f60dc9269588673", null ]
];