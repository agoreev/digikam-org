var classDigikam_1_1Album =
[
    [ "Type", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73ea", [
      [ "PHYSICAL", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaa9db3005d9583671396b9c84fe01ccb30", null ],
      [ "TAG", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaaed7ed8181f6115f3dfd762cebd21079e", null ],
      [ "DATE", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaa0d1445491b131a819b20ebb494054e9c", null ],
      [ "SEARCH", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaa437958b2416196287ed4c08cae9f59e7", null ],
      [ "FACE", "classDigikam_1_1Album.html#aadcb8ec638a2a8f969edf9371eab73eaa68580cdea0689ab93e1bf061ad75954d", null ]
    ] ],
    [ "Album", "classDigikam_1_1Album.html#a27f049d8096c3d3de720dbe118541470", null ],
    [ "~Album", "classDigikam_1_1Album.html#a8b8238c5c2b1cbd937259d005bb0f1c1", null ],
    [ "childAlbumIds", "classDigikam_1_1Album.html#a5b746694c8d0d952066e38fe3bc0dc73", null ],
    [ "childAlbums", "classDigikam_1_1Album.html#a0a4ae13f2d5ab815618eadda44beb819", null ],
    [ "childAtRow", "classDigikam_1_1Album.html#a29e9abd18575ef24d815b57fddc0410e", null ],
    [ "childCount", "classDigikam_1_1Album.html#a20e407523ead398f138705dc8c39c074", null ],
    [ "clear", "classDigikam_1_1Album.html#a6c9571e6f5d3e0a7e9678977fefcbb94", null ],
    [ "databaseUrl", "classDigikam_1_1Album.html#ab217fdaaae0aa56ca3dc81460878964b", null ],
    [ "extraData", "classDigikam_1_1Album.html#a60b4d8e8124de01df63d36b7c20795f1", null ],
    [ "firstChild", "classDigikam_1_1Album.html#aa3efc2d945c4de7afae3ac14926aaac7", null ],
    [ "globalID", "classDigikam_1_1Album.html#ad07ddf862e271467e1ba51bb8cbfa1bb", null ],
    [ "id", "classDigikam_1_1Album.html#aebe04e254c3ab25ada80693e4445f035", null ],
    [ "insertChild", "classDigikam_1_1Album.html#a3bbb5f9e21c22867f4c10fd38ce34209", null ],
    [ "isAncestorOf", "classDigikam_1_1Album.html#a92411bc23c824e319cd81282bb014d0c", null ],
    [ "isRoot", "classDigikam_1_1Album.html#a89ee486f759c164c65c00204b8e2bf32", null ],
    [ "isTrashAlbum", "classDigikam_1_1Album.html#a613c5c2afdd39878f4fbea3647f644fc", null ],
    [ "isUsedByLabelsTree", "classDigikam_1_1Album.html#ad7e7318fe819747ec838d25d21a9662c", null ],
    [ "lastChild", "classDigikam_1_1Album.html#a5a9f69aa4b14cd73ee8f14d035f662cb", null ],
    [ "next", "classDigikam_1_1Album.html#a2c0c7011a43e86f5c77b24189ca9db3d", null ],
    [ "parent", "classDigikam_1_1Album.html#a2ce932a51160bf04bf7d241ebfde55bb", null ],
    [ "prev", "classDigikam_1_1Album.html#acdf3b86ecd95939cbe313bf967e4ee48", null ],
    [ "removeChild", "classDigikam_1_1Album.html#afaa0207396d54d84c87e58836c5782d2", null ],
    [ "removeExtraData", "classDigikam_1_1Album.html#ad12af82921ea60701fa4015e80e6d56f", null ],
    [ "rowFromAlbum", "classDigikam_1_1Album.html#aceb17797315e939938682e23de09c064", null ],
    [ "setExtraData", "classDigikam_1_1Album.html#a24f0564ef9acfcd19225f24c24fb931c", null ],
    [ "setParent", "classDigikam_1_1Album.html#ac3b8f8bf44934baff4ea4e4b2c217826", null ],
    [ "setTitle", "classDigikam_1_1Album.html#a3d63a13e8a9a417305ca4ad9feafab56", null ],
    [ "setUsedByLabelsTree", "classDigikam_1_1Album.html#a3b59614171ec739e38e1a9269316b94a", null ],
    [ "title", "classDigikam_1_1Album.html#a5144ca08c97a3da4fbb66aa8407c431e", null ],
    [ "type", "classDigikam_1_1Album.html#a694a1fe572e8edfd7aacddcd7140d09e", null ],
    [ "AlbumManager", "classDigikam_1_1Album.html#a8f501b6178f13c66d30a0a86f5e36715", null ]
];