var classDigikam_1_1VersionManagerSettings =
[
    [ "EditorClosingMode", "classDigikam_1_1VersionManagerSettings.html#ad604e1bc77ed8c34b52e575a0498b614", [
      [ "AlwaysAsk", "classDigikam_1_1VersionManagerSettings.html#ad604e1bc77ed8c34b52e575a0498b614a720fea13dec6c185d712c5d0002e9d1e", null ],
      [ "AutoSave", "classDigikam_1_1VersionManagerSettings.html#ad604e1bc77ed8c34b52e575a0498b614a2f966977b34dcae9e7890703e0e3d66b", null ]
    ] ],
    [ "IntermediateSavepoint", "classDigikam_1_1VersionManagerSettings.html#a642f5346b9afcd5740ea68df9687768a", [
      [ "NoIntermediates", "classDigikam_1_1VersionManagerSettings.html#a642f5346b9afcd5740ea68df9687768aa23b16c6bfe6596abf3300d0d2a64bf96", null ],
      [ "AfterEachSession", "classDigikam_1_1VersionManagerSettings.html#a642f5346b9afcd5740ea68df9687768aa30f5f12ef17e9c6dd2793965c91aa2c5", null ],
      [ "AfterRawConversion", "classDigikam_1_1VersionManagerSettings.html#a642f5346b9afcd5740ea68df9687768aa65b46dda882882379d44e6039c0f1461", null ],
      [ "WhenNotReproducible", "classDigikam_1_1VersionManagerSettings.html#a642f5346b9afcd5740ea68df9687768aae1e5eeeb903cbecb93a7fd8865d5bdb6", null ]
    ] ],
    [ "ShowInViewFlag", "classDigikam_1_1VersionManagerSettings.html#a8b9800b4a0f5660ff638fcdebd1cfca5", [
      [ "OnlyShowCurrent", "classDigikam_1_1VersionManagerSettings.html#a8b9800b4a0f5660ff638fcdebd1cfca5ab01af21f89b2fb7cc6717d00bdc4d9d0", null ],
      [ "ShowOriginal", "classDigikam_1_1VersionManagerSettings.html#a8b9800b4a0f5660ff638fcdebd1cfca5a75cbcb4ccc06310740dca8ba5fcdc2eb", null ],
      [ "ShowIntermediates", "classDigikam_1_1VersionManagerSettings.html#a8b9800b4a0f5660ff638fcdebd1cfca5a438755516f07e587bdf37be50e600581", null ]
    ] ],
    [ "VersionManagerSettings", "classDigikam_1_1VersionManagerSettings.html#a68869082e672caf9f882f2d71346e8a1", null ],
    [ "readFromConfig", "classDigikam_1_1VersionManagerSettings.html#aa7a3d1627415a4a9374439c5357aa249", null ],
    [ "writeToConfig", "classDigikam_1_1VersionManagerSettings.html#a0d87bc038a26c92b372d902b52227ea0", null ],
    [ "editorClosingMode", "classDigikam_1_1VersionManagerSettings.html#ad8493f83044e9dce71fd66dbf2c5f0e2", null ],
    [ "enabled", "classDigikam_1_1VersionManagerSettings.html#a53c17fc372040ffefd82f40486cc124f", null ],
    [ "format", "classDigikam_1_1VersionManagerSettings.html#a4ac9c4e33b3bcf6da777566017bb33ca", null ],
    [ "saveIntermediateVersions", "classDigikam_1_1VersionManagerSettings.html#ae6a27b0af0ddf63336be463cb7067c5d", null ],
    [ "showInViewFlags", "classDigikam_1_1VersionManagerSettings.html#a1491be7a1fa30277ed3da6bd15ed8914", null ]
];