var classDigikam_1_1JPEGUtils_1_1JpegRotator =
[
    [ "JpegRotator", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a92e7f4a4e6c6a6fa14ff9b2cfeb12805", null ],
    [ "autoExifTransform", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#ac8ef2139b769ca67cf62b2bec60dd075", null ],
    [ "exifTransform", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a2c9a37724efa39d66b8a44c4d58fd904", null ],
    [ "exifTransform", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a824136aeb9187cbf605c315fb2a7cd48", null ],
    [ "performJpegTransform", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a9bb53abd88b4b16c219ada9d044f37aa", null ],
    [ "setCurrentOrientation", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a7d34975b2a669b8b72c69889b52cbf49", null ],
    [ "setDestinationFile", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a981b1d4f6152247d2d0b95ebe5be0d62", null ],
    [ "setDocumentName", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a945a0fe021fb234afab1ceff2792e166", null ],
    [ "updateMetadata", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a1f7fb06b2b97615ec62997b11a53369e", null ],
    [ "m_destFile", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#acf9b2839d553bdb8949512632e9fc908", null ],
    [ "m_documentName", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a093600968385650f1c52046b260de0f7", null ],
    [ "m_file", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#afcf203ae1fdb91331c6736727478b627", null ],
    [ "m_metadata", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a1c14b083b4a2b96bdd04830954931e89", null ],
    [ "m_orientation", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a2690d989a50a3364433de27b2a579d9d", null ],
    [ "m_originalSize", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html#a09e2d6e0a8e48a7ee50615ceebb7a3b1", null ]
];