var structheif__decoding__options =
[
    [ "end_progress", "structheif__decoding__options.html#ab9697c0d768867de941fcd5683c7e4f9", null ],
    [ "ignore_transformations", "structheif__decoding__options.html#a442549966f6e4e2b72a72463c4adfd89", null ],
    [ "on_progress", "structheif__decoding__options.html#a1955ed5df05a3c09347bc5e4d278e0b3", null ],
    [ "progress_user_data", "structheif__decoding__options.html#a5d4b7720c065d0767b852d386236ae4b", null ],
    [ "start_progress", "structheif__decoding__options.html#a0de2da0de7e0b238e44a6a11d3abf1cc", null ],
    [ "version", "structheif__decoding__options.html#a6df23e0f5faef81cb4ac36fb5889c09e", null ]
];