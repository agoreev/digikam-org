var classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode =
[
    [ "Type", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a5fa29e79928aa5880c2617bbe2f914ed", [
      [ "TerminalNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a5fa29e79928aa5880c2617bbe2f914edaadd54ee0737784818e6fdbbdb386a61f", null ],
      [ "HorizontalDivision", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a5fa29e79928aa5880c2617bbe2f914edaebcfdfd53751a8de94eb4ac81307c985", null ],
      [ "VerticalDivision", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a5fa29e79928aa5880c2617bbe2f914edac8a18d9c56a3b2c981eecd51ce703f64", null ]
    ] ],
    [ "AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#ac0d89ebcf32d8c959fe2697f1d63e7d1", null ],
    [ "AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a46a0e09f6dc23e5b034d002ed4f980f0", null ],
    [ "AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a7463cee64320bfd204d284add7a3e438", null ],
    [ "~AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a54d9ad341525917170a64b84935e3289", null ],
    [ "aspectRatio", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#ac42d4506d2ec6515db7d71ad7973781e", null ],
    [ "computeDivisions", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#aa9ebaf49d33aad17fb106f83a451479b", null ],
    [ "computeRelativeSizes", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a36fd70a531774be0dafb43a5a7ee9b7f", null ],
    [ "division", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a7eaa11d33dc0633974f94594d5a0399e", null ],
    [ "index", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a8d1f16bc009f171456a4545fcede5e25", null ],
    [ "leftChild", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a0cad89d4b21ffbea814baa4382591915", null ],
    [ "nodeForIndex", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#af0daacbe545c8c73a2386e9a94f4fe3f", null ],
    [ "operator=", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a22878d875a9a4ba3028a3b4549b55dac", null ],
    [ "parentOf", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a2a0a020a0c277e7a0a36d2d525beb8e3", null ],
    [ "relativeArea", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a22919bae35f0123be7ffca96124d0c6e", null ],
    [ "rightChild", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#accaf70616be825983179f2f55a44ed80", null ],
    [ "takeAndSetChild", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a0fd333fd49b2b1c32b6ba54398b2f026", null ],
    [ "type", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html#a7920de6c54f0ba4a4d1ea5d8b831585a", null ]
];