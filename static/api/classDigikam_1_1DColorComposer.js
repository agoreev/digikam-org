var classDigikam_1_1DColorComposer =
[
    [ "CompositingOperation", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693", [
      [ "PorterDuffNone", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a1f3f665d32bcb6595f202f7bd751bb70", null ],
      [ "PorterDuffClear", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693adadfc7177e4bc8b254338aa3f69b65a5", null ],
      [ "PorterDuffSrc", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a7acc512e17d07eb8b3b17873c451170e", null ],
      [ "PorterDuffSrcOver", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693aa1b2cd7475351fd5cb39e912de1eb5f7", null ],
      [ "PorterDuffDstOver", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a8cb0500e9a061c844114400f728867c5", null ],
      [ "PorterDuffSrcIn", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a84968ed413b5e700f08670c2587d9fe6", null ],
      [ "PorterDuffDstIn", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a2fd4ca21779d2417e659780efc00bb40", null ],
      [ "PorterDuffSrcOut", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a5af999e2b105905234d8fa58c4742178", null ],
      [ "PorterDuffDstOut", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a66b08626b66b52d050f125a37661e1e4", null ],
      [ "PorterDuffSrcAtop", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a5c4f81e5cedd2514074d493c2c0a864d", null ],
      [ "PorterDuffDstAtop", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693ad33c6c940a58cae1d99796119d3fa13a", null ],
      [ "PorterDuffXor", "classDigikam_1_1DColorComposer.html#a1900933281b58dcc0e6472b41ce7c693a35c3003538f4a044ba94b2dd9be0deda", null ]
    ] ],
    [ "MultiplicationFlags", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969", [
      [ "NoMultiplication", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969a6eb72b891f72ae4b8f6ea5504452d0bd", null ],
      [ "PremultiplySrc", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969a64b0ae0038cac2500cfa147ff934f022", null ],
      [ "PremultiplyDst", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969a9f5260b7e3497f7ead2f4fcd3f62a869", null ],
      [ "DemultiplyDst", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969ae620b90bfc7a2c716d2125317cded786", null ],
      [ "MultiplicationFlagsDImg", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969a173300884846be266599bad9b242a2cf", null ],
      [ "MultiplicationFlagsPremultipliedColorOnDImg", "classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969a71bc2efb282b0b996d49b5b28a1fc9ab", null ]
    ] ],
    [ "~DColorComposer", "classDigikam_1_1DColorComposer.html#a4bbc82ef866cc46d09572579234172a9", null ],
    [ "compose", "classDigikam_1_1DColorComposer.html#ab566fd7a1fa02245dee04038f0942c6b", null ],
    [ "compose", "classDigikam_1_1DColorComposer.html#af8f00324be7fa27b559a03135f07036c", null ]
];