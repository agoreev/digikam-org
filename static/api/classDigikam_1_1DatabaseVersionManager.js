var classDigikam_1_1DatabaseVersionManager =
[
    [ "FileNameType", "classDigikam_1_1DatabaseVersionManager.html#a05c961bbcd6d4c636a5cad2145319b31", [
      [ "CurrentVersionName", "classDigikam_1_1DatabaseVersionManager.html#a05c961bbcd6d4c636a5cad2145319b31adbac22f0170d4ee459f30349c70a6175", null ],
      [ "NewVersionName", "classDigikam_1_1DatabaseVersionManager.html#a05c961bbcd6d4c636a5cad2145319b31a4ac6a8c2b4b815ff38b2df1cbef719ab", null ]
    ] ],
    [ "namingScheme", "classDigikam_1_1DatabaseVersionManager.html#a0b4ba6799123830b3784aea7c1ff3eb2", null ],
    [ "operation", "classDigikam_1_1DatabaseVersionManager.html#aa6448e6d82d7bf1376c7db113d5b822f", null ],
    [ "operationNewVersionAs", "classDigikam_1_1DatabaseVersionManager.html#aeeb661cecbc9e7333cab55f34a897a62", null ],
    [ "operationNewVersionInFormat", "classDigikam_1_1DatabaseVersionManager.html#ac0d7bc35d79da30a0ed4a868a6df2da3", null ],
    [ "setNamingScheme", "classDigikam_1_1DatabaseVersionManager.html#a57d602dda06439e6a9e4f09e1e0cceb3", null ],
    [ "setSettings", "classDigikam_1_1DatabaseVersionManager.html#af53f8f3b3c2dd64a70530b571d2f7055", null ],
    [ "settings", "classDigikam_1_1DatabaseVersionManager.html#ad71e663c7485dbd41a011744f6dd9594", null ],
    [ "toplevelDirectory", "classDigikam_1_1DatabaseVersionManager.html#a287e390bdf64e52d9949290d6d3e8f3f", null ],
    [ "VersionManagerPriv", "classDigikam_1_1DatabaseVersionManager.html#a46529cb8d8ebe62bd24c5b7a4d1e5041", null ],
    [ "workspaceFileFormats", "classDigikam_1_1DatabaseVersionManager.html#ab1eb6fad03b2cd6feeb7350e40a3334c", null ],
    [ "defaultScheme", "classDigikam_1_1DatabaseVersionManager.html#aadf3b0b777f4b3517a12d61f4910d223", null ],
    [ "scheme", "classDigikam_1_1DatabaseVersionManager.html#a4994d99470e7cf42c7b9f5f085bbfcf5", null ],
    [ "settings", "classDigikam_1_1DatabaseVersionManager.html#ad707714527fa31b9d5b3287f5cc1dee5", null ]
];