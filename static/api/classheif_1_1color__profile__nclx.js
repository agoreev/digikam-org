var classheif_1_1color__profile__nclx =
[
    [ "color_profile_nclx", "classheif_1_1color__profile__nclx.html#ab9b11101b6e760f7c4c2bdb0c4cacafc", null ],
    [ "dump", "classheif_1_1color__profile__nclx.html#a66013e5378c0f91a4cf99d28900ae789", null ],
    [ "get_colour_primaries", "classheif_1_1color__profile__nclx.html#aa38a32cdb5cd0b8d9d3e2ee5895e90b2", null ],
    [ "get_full_range_flag", "classheif_1_1color__profile__nclx.html#adf8008a6ebee9b14b142094c15843192", null ],
    [ "get_matrix_coefficients", "classheif_1_1color__profile__nclx.html#afd4f287578770e70cdeea50d0eaae74a", null ],
    [ "get_transfer_characteristics", "classheif_1_1color__profile__nclx.html#a0c1f358cc0da232d3338c2041f3b1536", null ],
    [ "get_type", "classheif_1_1color__profile__nclx.html#a2845b6dc83dbc415cffadc59dbaaaf18", null ],
    [ "parse", "classheif_1_1color__profile__nclx.html#af0b1654998688a67be6f7c7848336d9b", null ],
    [ "set_colour_primaries", "classheif_1_1color__profile__nclx.html#a61a42928e44552aa91a63dd6291d52c6", null ],
    [ "set_full_range_flag", "classheif_1_1color__profile__nclx.html#a6fa17bbf90b9e814bb134932c5addff7", null ],
    [ "set_matrix_coefficients", "classheif_1_1color__profile__nclx.html#afa3a7a225857d4e953e0ff52afa84f75", null ],
    [ "set_transfer_characteristics", "classheif_1_1color__profile__nclx.html#ad191b8e9941457242e37b7fd3c3af499", null ],
    [ "write", "classheif_1_1color__profile__nclx.html#ace71da97d01ec20a2da56701b990bf8c", null ]
];