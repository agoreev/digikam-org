var structDigikam_1_1PTOType_1_1Mask =
[
    [ "MaskType", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647", [
      [ "NEGATIVE", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647ab11338b86bb1e6e9cc54ce05ca2876c4", null ],
      [ "POSTIVE", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647abde6d7d17cafda4c7206764f80d25407", null ],
      [ "NEGATIVESTACK", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647a0f25855bd81141459c891635b28d913c", null ],
      [ "POSITIVESTACK", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647a7d521766b9d6f3f1802693ece93268a7", null ],
      [ "NEGATIVELENS", "structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647a54e8b49096ee07034fa90b44e6ed6de1", null ]
    ] ],
    [ "hull", "structDigikam_1_1PTOType_1_1Mask.html#a34d92ff7af2c1679891ae83143571a07", null ],
    [ "previousComments", "structDigikam_1_1PTOType_1_1Mask.html#af14d291cc9aaecba189c53f5e6b0131c", null ],
    [ "type", "structDigikam_1_1PTOType_1_1Mask.html#aaac76ff0fc599828502b8cbd62b95912", null ]
];