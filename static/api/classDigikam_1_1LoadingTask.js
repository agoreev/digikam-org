var classDigikam_1_1LoadingTask =
[
    [ "LoadingTaskStatus", "classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255", [
      [ "LoadingTaskStatusLoading", "classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255a9fb917335571fcf5a3c92c4929800b17", null ],
      [ "LoadingTaskStatusPreloading", "classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255a28d9aec69c9254aabc1a0494204b64d4", null ],
      [ "LoadingTaskStatusStopping", "classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255ad8070c2c8f54d077b89a3b211c964b82", null ]
    ] ],
    [ "TaskType", "classDigikam_1_1LoadingTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1LoadingTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1LoadingTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "LoadingTask", "classDigikam_1_1LoadingTask.html#a5eeaa6661b405fd8b9cedb9c7a981b25", null ],
    [ "continueQuery", "classDigikam_1_1LoadingTask.html#ae05a07432aacecaac74ea2f291ccd342", null ],
    [ "execute", "classDigikam_1_1LoadingTask.html#aa9cb11d3aed5bad03ada46191d3b40de", null ],
    [ "filePath", "classDigikam_1_1LoadingTask.html#a31c24110e86375e6fda784927f1e57e7", null ],
    [ "granularity", "classDigikam_1_1LoadingTask.html#ab976f06eb18822b0da8e481fd2ff55d0", null ],
    [ "loadingDescription", "classDigikam_1_1LoadingTask.html#a64266025e0d2fd28711fcd889e91fefb", null ],
    [ "progressInfo", "classDigikam_1_1LoadingTask.html#a11861d5442adb3116ff3e682d844c6b5", null ],
    [ "setStatus", "classDigikam_1_1LoadingTask.html#a52051344cc4589d6d0e8335a938f1b75", null ],
    [ "status", "classDigikam_1_1LoadingTask.html#a85855649f66c9143d20860cdb80f9e0d", null ],
    [ "type", "classDigikam_1_1LoadingTask.html#a5fee5b84b321dfd50f3a054078ea4cd4", null ],
    [ "m_loadingDescription", "classDigikam_1_1LoadingTask.html#ab16d4a9ce3d15d4148a711218bcecf4e", null ],
    [ "m_loadingTaskStatus", "classDigikam_1_1LoadingTask.html#aac204d0b11985a2f5c04ab0087c5fe66", null ],
    [ "m_thread", "classDigikam_1_1LoadingTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ]
];