var classDigikam_1_1DNNFaceDetectorSSD =
[
    [ "DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html#ae40d1c516805ae83be93f42cc74630ff", null ],
    [ "~DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html#a9f74365f242aa653a5257814502be0c1", null ],
    [ "correctBbox", "classDigikam_1_1DNNFaceDetectorSSD.html#aa3c3fc9a38724e3d0423989f47d6b153", null ],
    [ "detectFaces", "classDigikam_1_1DNNFaceDetectorSSD.html#a7652d1e4036d2b44f3b326fd6c5ad9e9", null ],
    [ "nnInputSizeRequired", "classDigikam_1_1DNNFaceDetectorSSD.html#a10c2f7b450ebc0a6b0b631e333f4f10d", null ],
    [ "selectBbox", "classDigikam_1_1DNNFaceDetectorSSD.html#ac03ca956fbe2ad1a2c45e44a24d5dc43", null ],
    [ "inputImageSize", "classDigikam_1_1DNNFaceDetectorSSD.html#a57327c3e3bc01d71b2c155cb51f10488", null ],
    [ "meanValToSubtract", "classDigikam_1_1DNNFaceDetectorSSD.html#a800befeba470c89ae6e2b9cad13bf6f3", null ],
    [ "net", "classDigikam_1_1DNNFaceDetectorSSD.html#a05c55414cc0784bebc3925a6547f8fef", null ],
    [ "scaleFactor", "classDigikam_1_1DNNFaceDetectorSSD.html#a3c902bace9d612c1990c1eeeec51b22f", null ]
];