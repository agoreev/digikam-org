var classDigikam_1_1RGBackend =
[
    [ "RGBackend", "classDigikam_1_1RGBackend.html#acb908d96afd71e082bb837d6ba5886fa", null ],
    [ "RGBackend", "classDigikam_1_1RGBackend.html#ac47df07bc753ad7271ab39d302f5032c", null ],
    [ "~RGBackend", "classDigikam_1_1RGBackend.html#a8cdf542a553b9a927e42b2fea7f9ed72", null ],
    [ "backendName", "classDigikam_1_1RGBackend.html#a06f6ac8cf5dc4c7131bed22edac4d8c3", null ],
    [ "callRGBackend", "classDigikam_1_1RGBackend.html#a7ab62b8b0d374a17235575aca6fc48bb", null ],
    [ "cancelRequests", "classDigikam_1_1RGBackend.html#a5b2230660fe3edd4bcbae6c491111dd9", null ],
    [ "getErrorMessage", "classDigikam_1_1RGBackend.html#a66414cc1607c3b6c4f5401505ad7637f", null ],
    [ "signalRGReady", "classDigikam_1_1RGBackend.html#a6ee2a6e36a160142ffa2fe82b07c4695", null ]
];