var classDigikamGenericSendByMailPlugin_1_1ImageResizeJob =
[
    [ "ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#ac34eb98bb8d53b9d6bf1131f6cbe1e06", null ],
    [ "~ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a7b8925363352b74d926db82bec3babba", null ],
    [ "cancel", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "failedResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a9f3b73a94cffd7ae6ea01cbbc2ff3275", null ],
    [ "finishedResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a072ea09e9762e4bd95bbdfe8be870779", null ],
    [ "signalDone", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalProgress", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "startingResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#af0af7a467f708568af84bec818024d01", null ],
    [ "m_cancel", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ],
    [ "m_count", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a4f6e280af79e2cc241c6ccbfba9d2f8a", null ],
    [ "m_destName", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a9d08835edc280725a5f656fe2ecd93e4", null ],
    [ "m_orgUrl", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#a2d177af1c1632ec9084468f2ac8b9324", null ],
    [ "m_settings", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html#acd26f6cd8b5c94ca731c9daa2fcfb213", null ]
];