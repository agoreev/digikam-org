var classDigikam_1_1GeoIfaceInternalWidgetInfo =
[
    [ "DeleteFunction", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#a92c1c6147917257eeefdfbd535420c4d", null ],
    [ "InternalWidgetState", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#afa87f311e5d5e8fd832d0f05897b4ccd", [
      [ "InternalWidgetReleased", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#afa87f311e5d5e8fd832d0f05897b4ccdaf136103e33bb2cbc7d86e8574844de81", null ],
      [ "InternalWidgetUndocked", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#afa87f311e5d5e8fd832d0f05897b4ccdae0597fedb05215a5a837fcbf3b51c30f", null ],
      [ "InternalWidgetStillDocked", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#afa87f311e5d5e8fd832d0f05897b4ccdabe3d7a68994261d3f3708e093330744d", null ]
    ] ],
    [ "GeoIfaceInternalWidgetInfo", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#a99dcd819e83589c4d77e88a52c51ac73", null ],
    [ "backendData", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#ab1a383278212c3ae2bff196ec37962b0", null ],
    [ "backendName", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#a2c77479769d75efe35fa0abe905ae189", null ],
    [ "currentOwner", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#a51909aa64d4c50e964407d5401383f1c", null ],
    [ "deleteFunction", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#af532891cd0bb4e86c1c92dc4910309f6", null ],
    [ "state", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#a2a8b02d768920db3f3b5c5ba725ff6fa", null ],
    [ "widget", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html#ab155a72bb118ddb02729cdeb86ba5278", null ]
];