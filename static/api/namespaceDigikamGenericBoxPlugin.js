var namespaceDigikamGenericBoxPlugin =
[
    [ "BOXFolder", "classDigikamGenericBoxPlugin_1_1BOXFolder.html", "classDigikamGenericBoxPlugin_1_1BOXFolder" ],
    [ "BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg" ],
    [ "BOXPhoto", "classDigikamGenericBoxPlugin_1_1BOXPhoto.html", "classDigikamGenericBoxPlugin_1_1BOXPhoto" ],
    [ "BoxPlugin", "classDigikamGenericBoxPlugin_1_1BoxPlugin.html", "classDigikamGenericBoxPlugin_1_1BoxPlugin" ],
    [ "BOXWidget", "classDigikamGenericBoxPlugin_1_1BOXWidget.html", "classDigikamGenericBoxPlugin_1_1BOXWidget" ]
];