var classDigikam_1_1AlbumRootChangeset =
[
    [ "Operation", "classDigikam_1_1AlbumRootChangeset.html#af0e9f0754ba996f58b526eab599d922a", [
      [ "Unknown", "classDigikam_1_1AlbumRootChangeset.html#af0e9f0754ba996f58b526eab599d922aa7155810c11cbfc72a80b67b116fcde83", null ],
      [ "Added", "classDigikam_1_1AlbumRootChangeset.html#af0e9f0754ba996f58b526eab599d922aafdeb1709149fcabade0f8084f95d4cf4", null ],
      [ "Deleted", "classDigikam_1_1AlbumRootChangeset.html#af0e9f0754ba996f58b526eab599d922aa28056ace747c4d226c887315a044a4de", null ],
      [ "PropertiesChanged", "classDigikam_1_1AlbumRootChangeset.html#af0e9f0754ba996f58b526eab599d922aaf3ee7d04ca16d96cc4cfb22ad4b7ff63", null ]
    ] ],
    [ "AlbumRootChangeset", "classDigikam_1_1AlbumRootChangeset.html#a2ec0b221b80b5fc7539acbad7a677503", null ],
    [ "AlbumRootChangeset", "classDigikam_1_1AlbumRootChangeset.html#a8f3d01e27b74c692d654c26c90f498b7", null ],
    [ "albumRootId", "classDigikam_1_1AlbumRootChangeset.html#a1ee9a50ef3c7eb86b5b39eb11a98dec1", null ],
    [ "operation", "classDigikam_1_1AlbumRootChangeset.html#af36b90c15d8e8618df7f63ea9a66e8e5", null ]
];