var searchData=
[
  ['haariface',['HaarIface',['../classDigikam_1_1HaarIface.html',1,'Digikam']]],
  ['haarprogressobserver',['HaarProgressObserver',['../classDigikam_1_1HaarProgressObserver.html',1,'Digikam']]],
  ['hash',['Hash',['../classDigikam_1_1DatabaseFields_1_1Hash.html',1,'Digikam::DatabaseFields']]],
  ['hash_3c_20qvariant_20_3e',['Hash&lt; QVariant &gt;',['../classDigikam_1_1DatabaseFields_1_1Hash.html',1,'Digikam::DatabaseFields']]],
  ['healingclonetoolplugin',['HealingCloneToolPlugin',['../classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolPlugin.html',1,'DigikamEditorHealingCloneToolPlugin']]],
  ['heif_5fcolor_5fprofile_5fnclx',['heif_color_profile_nclx',['../structheif__color__profile__nclx.html',1,'']]],
  ['heif_5fcontext',['heif_context',['../structheif__context.html',1,'']]],
  ['heif_5fdecoder_5fplugin',['heif_decoder_plugin',['../structheif__decoder__plugin.html',1,'']]],
  ['heif_5fdecoding_5foptions',['heif_decoding_options',['../structheif__decoding__options.html',1,'']]],
  ['heif_5fdepth_5frepresentation_5finfo',['heif_depth_representation_info',['../structheif__depth__representation__info.html',1,'']]],
  ['heif_5fencoder',['heif_encoder',['../structheif__encoder.html',1,'']]],
  ['heif_5fencoder_5fdescriptor',['heif_encoder_descriptor',['../structheif__encoder__descriptor.html',1,'']]],
  ['heif_5fencoder_5fparameter',['heif_encoder_parameter',['../structheif__encoder__parameter.html',1,'']]],
  ['heif_5fencoder_5fplugin',['heif_encoder_plugin',['../structheif__encoder__plugin.html',1,'']]],
  ['heif_5fencoding_5foptions',['heif_encoding_options',['../structheif__encoding__options.html',1,'']]],
  ['heif_5ferror',['heif_error',['../structheif__error.html',1,'']]],
  ['heif_5fimage',['heif_image',['../structheif__image.html',1,'']]],
  ['heif_5fimage_5fhandle',['heif_image_handle',['../structheif__image__handle.html',1,'']]],
  ['heif_5freader',['heif_reader',['../structheif__reader.html',1,'']]],
  ['heif_5fwriter',['heif_writer',['../structheif__writer.html',1,'']]],
  ['heifcontext',['HeifContext',['../classheif_1_1HeifContext.html',1,'heif']]],
  ['heiffile',['HeifFile',['../classheif_1_1HeifFile.html',1,'heif']]],
  ['heifpixelimage',['HeifPixelImage',['../classheif_1_1HeifPixelImage.html',1,'heif']]],
  ['hidingstatechanger',['HidingStateChanger',['../classDigikam_1_1HidingStateChanger.html',1,'Digikam']]],
  ['highlighter',['Highlighter',['../classDigikam_1_1Highlighter.html',1,'Digikam']]],
  ['historyedgeproperties',['HistoryEdgeProperties',['../classDigikam_1_1HistoryEdgeProperties.html',1,'Digikam']]],
  ['historyimageid',['HistoryImageId',['../classDigikam_1_1HistoryImageId.html',1,'Digikam']]],
  ['historyvertexproperties',['HistoryVertexProperties',['../classDigikam_1_1HistoryVertexProperties.html',1,'Digikam']]],
  ['hotpixel',['HotPixel',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixel.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['hotpixelfixer',['HotPixelFixer',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['hotpixelstoolplugin',['HotPixelsToolPlugin',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['hotpixelsweights',['HotPixelsWeights',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsWeights.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['hoverbuttondelegateoverlay',['HoverButtonDelegateOverlay',['../classDigikam_1_1HoverButtonDelegateOverlay.html',1,'Digikam']]],
  ['hslcontainer',['HSLContainer',['../classDigikam_1_1HSLContainer.html',1,'Digikam']]],
  ['hslcorrection',['HSLCorrection',['../classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrection.html',1,'DigikamBqmHSLCorrectionPlugin']]],
  ['hslcorrectionplugin',['HSLCorrectionPlugin',['../classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrectionPlugin.html',1,'DigikamBqmHSLCorrectionPlugin']]],
  ['hsltoolplugin',['HSLToolPlugin',['../classDigikamEditorHSLToolPlugin_1_1HSLToolPlugin.html',1,'DigikamEditorHSLToolPlugin']]],
  ['htmlgalleryplugin',['HtmlGalleryPlugin',['../classDigikamGenericHtmlGalleryPlugin_1_1HtmlGalleryPlugin.html',1,'DigikamGenericHtmlGalleryPlugin']]],
  ['htmlwidgetpage',['HTMLWidgetPage',['../classDigikam_1_1HTMLWidgetPage.html',1,'Digikam']]],
  ['huginexecutorbinary',['HuginExecutorBinary',['../classDigikamGenericPanoramaPlugin_1_1HuginExecutorBinary.html',1,'DigikamGenericPanoramaPlugin']]],
  ['huginexecutortask',['HuginExecutorTask',['../classDigikamGenericPanoramaPlugin_1_1HuginExecutorTask.html',1,'DigikamGenericPanoramaPlugin']]]
];
