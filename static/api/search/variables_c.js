var searchData=
[
  ['label',['label',['../classUi__CalTemplate.html#ae4aedd095d7f06a6a109345673670ebe',1,'Ui_CalTemplate::label()'],['../classUi__AdvPrintCaptionPage.html#a13d50624de1e33b758fb3bbda5bd3429',1,'Ui_AdvPrintCaptionPage::label()'],['../classUi__AdvPrintCustomLayout.html#a107a4565c9022dc2847d48e334c2445c',1,'Ui_AdvPrintCustomLayout::label()'],['../classUi__PresentationAudioPage.html#ad3122ed34326d57a9c37c43d5af1ef5f',1,'Ui_PresentationAudioPage::label()'],['../classUi__PresentationCaptionPage.html#a9578667301c4e872b8f51619156d2740',1,'Ui_PresentationCaptionPage::label()'],['../classUi__PresentationAudioWidget.html#af241f44f67a7af1a7fb20ae0b83bb1aa',1,'Ui_PresentationAudioWidget::label()'],['../classUi__DateOptionDialogWidget.html#a145b1f42bfac45696ea287c0ecef16a1',1,'Ui_DateOptionDialogWidget::label()'],['../classUi__FillModifierDialogWidget.html#ae5079d6b69861c0209ecaf9f560b8445',1,'Ui_FillModifierDialogWidget::label()'],['../classUi__RangeModifierDialogWidget.html#a4c4c81d360de8db4bedbe950ea561a73',1,'Ui_RangeModifierDialogWidget::label()'],['../classUi__ReplaceModifierDialogWidget.html#a4be1e6ee3229ec82e03e3c0ccf6e075f',1,'Ui_ReplaceModifierDialogWidget::label()'],['../classUi__SequenceNumberOptionDialogWidget.html#a47aa58c583edaa02770c2fe1a02e321b',1,'Ui_SequenceNumberOptionDialogWidget::label()'],['../classDigikam_1_1SolidVolumeInfo.html#a5db323d2879050db995a4d145422d5b5',1,'Digikam::SolidVolumeInfo::label()'],['../classDigikam_1_1AlbumRootInfo.html#a4b31547d76a7afb5ccb810804fb53e02',1,'Digikam::AlbumRootInfo::label()'],['../classDigikam_1_1ImageQualityParser.html#aa081c33790e2bf5b542ca9bdbd90a88c',1,'Digikam::ImageQualityParser::label()'],['../classDigikam_1_1Face_1_1StandardCollector_1_1PredictResult.html#a7b6fe74686a04886e84f29b504e941c1',1,'Digikam::Face::StandardCollector::PredictResult::label()'],['../classDigikam_1_1SetupCollectionModel_1_1Item.html#a4e5bc622f693e7a786125bdc730a638d',1,'Digikam::SetupCollectionModel::Item::label()']]],
  ['label_5f2',['label_2',['../classUi__AdvPrintCaptionPage.html#aa55b288f21940bd2fdeb2dd5babda988',1,'Ui_AdvPrintCaptionPage::label_2()'],['../classUi__AdvPrintCustomLayout.html#a26f61842aa04a9e8872508521e0ccb4b',1,'Ui_AdvPrintCustomLayout::label_2()'],['../classUi__PresentationAudioPage.html#ab01053b8003e891231dd0b3723280889',1,'Ui_PresentationAudioPage::label_2()'],['../classUi__PresentationCaptionPage.html#ac4d0f794e698fe571db758208ee95b33',1,'Ui_PresentationCaptionPage::label_2()'],['../classUi__PresentationAudioWidget.html#a7fd86dd9ff444ec318aea1347f4d6382',1,'Ui_PresentationAudioWidget::label_2()'],['../classUi__DateOptionDialogWidget.html#a64fa04df2c58f3134878cd2a8384696d',1,'Ui_DateOptionDialogWidget::label_2()'],['../classUi__FillModifierDialogWidget.html#ae807980b48354856343debef4d7c15b0',1,'Ui_FillModifierDialogWidget::label_2()'],['../classUi__RangeModifierDialogWidget.html#ac861e058fc1f3f152a10a10f61dad441',1,'Ui_RangeModifierDialogWidget::label_2()'],['../classUi__ReplaceModifierDialogWidget.html#af1bcb1218a789b4c3ddcb7d00e5fa271',1,'Ui_ReplaceModifierDialogWidget::label_2()'],['../classUi__SequenceNumberOptionDialogWidget.html#a9ab85354cce30eaa5f80ceb50d94cae4',1,'Ui_SequenceNumberOptionDialogWidget::label_2()']]],
  ['label_5f3',['label_3',['../classUi__AdvPrintCaptionPage.html#a59223856a592d85676f04aecb82ec9fb',1,'Ui_AdvPrintCaptionPage::label_3()'],['../classUi__AdvPrintCustomLayout.html#a8e7d2cd75ad1061d6a93affa78f9a5ed',1,'Ui_AdvPrintCustomLayout::label_3()'],['../classUi__PresentationCaptionPage.html#ab25acc04da4585c17c89ff2e84a6ed4f',1,'Ui_PresentationCaptionPage::label_3()'],['../classUi__PresentationMainPage.html#a271894135dc2481eaec4dd9ae8f6cc04',1,'Ui_PresentationMainPage::label_3()'],['../classUi__PresentationAudioWidget.html#a2d86c398fa0c7eb11d0e2e2cfe479abe',1,'Ui_PresentationAudioWidget::label_3()'],['../classUi__DateOptionDialogWidget.html#a05b20c7f6d4fa622375e819b01862553',1,'Ui_DateOptionDialogWidget::label_3()'],['../classUi__FillModifierDialogWidget.html#a706f1e366cac2c1a6b7b81fd62b6f971',1,'Ui_FillModifierDialogWidget::label_3()'],['../classUi__SequenceNumberOptionDialogWidget.html#a6468ffeea1c5062e6f69ba53928b3bd5',1,'Ui_SequenceNumberOptionDialogWidget::label_3()']]],
  ['label_5f4',['label_4',['../classUi__AdvPrintCaptionPage.html#a0b1e3886ec835e3210a6044b41f2c3fc',1,'Ui_AdvPrintCaptionPage::label_4()'],['../classUi__AdvPrintCustomLayout.html#a6fded275e40ba01d439b23b49e6c6462',1,'Ui_AdvPrintCustomLayout::label_4()'],['../classUi__PresentationCaptionPage.html#ad0b39023b9805c8eb6acb0ef01d6eb9e',1,'Ui_PresentationCaptionPage::label_4()']]],
  ['label_5f5',['label_5',['../classUi__AdvPrintCaptionPage.html#a85e655d6c68d8b4e9252fdbb5bf0966a',1,'Ui_AdvPrintCaptionPage::label_5()'],['../classUi__AdvPrintCustomLayout.html#a4ec1f2480cf48f116ed0dfecef3f0426',1,'Ui_AdvPrintCustomLayout::label_5()']]],
  ['label_5f6',['label_6',['../classUi__AdvPrintCaptionPage.html#a5f3d5b5cee9fb5277a3b22595a0845ec',1,'Ui_AdvPrintCaptionPage']]],
  ['label_5f7',['label_7',['../classUi__AdvPrintCaptionPage.html#a310b21fc233e31fca80dfb008e72ae47',1,'Ui_AdvPrintCaptionPage']]],
  ['lambda',['lambda',['../classencoder__context.html#ad390828ac12313915955894daf072efd',1,'encoder_context']]],
  ['lanczos_5ffunc',['lanczos_func',['../classDigikam_1_1DImg.html#a0ae2fb1d29d5634829f6e81bce64cb47',1,'Digikam::DImg']]],
  ['language',['language',['../classDigikam_1_1CommentInfo.html#a902667be3f7b0317923edbc41e606f4a',1,'Digikam::CommentInfo']]],
  ['last_5fdecoded_5fctb_5frs',['last_decoded_CTB_RS',['../classslice__unit.html#a88cd7459ba1c4ab7a0a32b06c6aad133',1,'slice_unit']]],
  ['lastaction',['lastAction',['../classDigikam_1_1LightTableWindow.html#a35f85fd83ad2ad0529fde666de91df0f',1,'Digikam::LightTableWindow']]],
  ['lastcomments',['lastComments',['../structDigikam_1_1PTOType.html#a15df2c8fb8f68c13403ca550877fadd9',1,'Digikam::PTOType']]],
  ['lastdescriptions',['lastDescriptions',['../classDigikam_1_1ThumbnailLoadThread.html#aab3e454e122ad639a0296510acee271b',1,'Digikam::ThumbnailLoadThread']]],
  ['lastdesturl',['lastDestURL',['../classDigikam_1_1ImportUI.html#aec78db217c26cfcb45784a5d42ae8763',1,'Digikam::ImportUI']]],
  ['lastdiscardversion',['lastDiscardVersion',['../classDigikam_1_1ItemFilterModel.html#a7901bbbd195a048d2e63371f4b4a7433',1,'Digikam::ItemFilterModel']]],
  ['lastdraggeditemsrect',['lastDraggedItemsRect',['../classDigikam_1_1DCategorizedView.html#aa8b844460594e451dcc9717296fbfe88',1,'Digikam::DCategorizedView']]],
  ['lasterror',['lastError',['../classDigikam_1_1DbEngineThreadData.html#a5c07d9db7651d695837afa3fa097b19f',1,'Digikam::DbEngineThreadData']]],
  ['lastfilteredversion',['lastFilteredVersion',['../classDigikam_1_1ItemFilterModel.html#a595220cb11932eb7006cbf3ddc8953ee',1,'Digikam::ItemFilterModel']]],
  ['lastheight',['lastHeight',['../classDigikam_1_1VideoDecoder.html#a59addadb0c1be7d017e7b7e91ff23127',1,'Digikam::VideoDecoder']]],
  ['lasthintadded',['lastHintAdded',['../classDigikam_1_1ScanController.html#aa9274d5caada029f2663f60b18d501f4',1,'Digikam::ScanController']]],
  ['lastmismatchbehavior',['lastMismatchBehavior',['../classDigikam_1_1ICCSettingsContainer.html#a0c540362415dba5a1501326900c3b7fe',1,'Digikam::ICCSettingsContainer']]],
  ['lastmissingprofilebehavior',['lastMissingProfileBehavior',['../classDigikam_1_1ICCSettingsContainer.html#af2c9d8624858b65586743a8c65e26534',1,'Digikam::ICCSettingsContainer']]],
  ['lastopeneddirectory',['lastOpenedDirectory',['../classShowFoto_1_1ShowFoto.html#a3bdeef81790979b58c7e3a2825d408a1',1,'ShowFoto::ShowFoto']]],
  ['lastpixfmt',['lastPixfmt',['../classDigikam_1_1VideoDecoder.html#af4321e12d68645b57cb135455b5e43d6',1,'Digikam::VideoDecoder']]],
  ['lastqpyinpreviousqg',['lastQPYinPreviousQG',['../classthread__context.html#a84d885b5c8d19ba2beda83f24d972523',1,'thread_context']]],
  ['lastselectedalbum',['lastSelectedAlbum',['../classDigikam_1_1AbstractAlbumTreeView.html#a52c3666a76143e8ed4f534b4f6c1e035',1,'Digikam::AbstractAlbumTreeView']]],
  ['lastselection',['lastSelection',['../classDigikam_1_1DCategorizedView.html#a60ddb26fc35855dab663da6162c06797',1,'Digikam::DCategorizedView']]],
  ['lastspecifiedassignprofile',['lastSpecifiedAssignProfile',['../classDigikam_1_1ICCSettingsContainer.html#a9e059f446f51070f91346125e86a637c',1,'Digikam::ICCSettingsContainer']]],
  ['lastspecifiedinputprofile',['lastSpecifiedInputProfile',['../classDigikam_1_1ICCSettingsContainer.html#a9ca4e1925b8c8fcc7bdf5b85f7002a36',1,'Digikam::ICCSettingsContainer']]],
  ['lastuncalibratedbehavior',['lastUncalibratedBehavior',['../classDigikam_1_1ICCSettingsContainer.html#a35d6dcb93f402725dc32c1cbf40f7758',1,'Digikam::ICCSettingsContainer']]],
  ['lastwidth',['lastWidth',['../classDigikam_1_1VideoDecoder.html#af3881aeedcdb54278e1bd20b3767c28c',1,'Digikam::VideoDecoder']]],
  ['latitude',['latitude',['../classDigikam_1_1ItemInfoData.html#a4d5c956b113b58c2469923e4214baab3',1,'Digikam::ItemInfoData::latitude()'],['../classDigikam_1_1SaveProperties.html#ad59c6b099e62ccbbf9cc1556beb804a4',1,'Digikam::SaveProperties::latitude()']]],
  ['layer',['layer',['../classvideo__parameter__set.html#a67defcdfdc0d0c08c92c0dc9c54b24b3',1,'video_parameter_set']]],
  ['layer_5fid_5fincluded_5fflag',['layer_id_included_flag',['../classvideo__parameter__set.html#aebc1c9dc05c6b3df02487af93dbe0b59',1,'video_parameter_set']]],
  ['layout',['layout',['../classDigikam_1_1DConfigDlgViewPrivate.html#a3257bde2fcad2d79ff823cec2d986c2b',1,'Digikam::DConfigDlgViewPrivate::layout()'],['../classDigikam_1_1AssignNameWidget.html#a5dbc88c62b4fa3726cbc006093485b3d',1,'Digikam::AssignNameWidget::layout()']]],
  ['lblcropphoto',['LblCropPhoto',['../classUi__AdvPrintCropPage.html#ae99aa6bc729d283c1432077315e0bcb7',1,'Ui_AdvPrintCropPage']]],
  ['lblemptyslots',['LblEmptySlots',['../classUi__AdvPrintPhotoPage.html#a0b5c15932149b623997198dc988c95ae',1,'Ui_AdvPrintPhotoPage']]],
  ['lblphotocount',['LblPhotoCount',['../classUi__AdvPrintPhotoPage.html#aad50a3fb1ac157f95863ca268f23001f',1,'Ui_AdvPrintPhotoPage']]],
  ['lblpreview',['LblPreview',['../classUi__AdvPrintPhotoPage.html#a430c318422b1cb89f02d93056061d742',1,'Ui_AdvPrintPhotoPage']]],
  ['lblsheetsprinted',['LblSheetsPrinted',['../classUi__AdvPrintPhotoPage.html#a1586c7018cd958bbbe3cd833c0d380e7',1,'Ui_AdvPrintPhotoPage']]],
  ['leaf_5fvalues',['leaf_values',['../structDigikam_1_1RedEye_1_1RegressionTree.html#a3354696906e8cdb4e603f91184a1dce3',1,'Digikam::RedEye::RegressionTree']]],
  ['leftfilename',['leftFileName',['../classDigikam_1_1LightTableWindow.html#a40f9f8b2fa66c62400f194404b6a6e2b',1,'Digikam::LightTableWindow']]],
  ['leftmargin',['leftMargin',['../classDigikam_1_1DRawInfo.html#a32184d2527d8f272f5c433d07b548e22',1,'Digikam::DRawInfo']]],
  ['leftradio',['leftRadio',['../classUi__CalTemplate.html#af655783514d470dafca9d9008a44daae',1,'Ui_CalTemplate']]],
  ['leftsidebar',['leftSideBar',['../classDigikam_1_1LightTableWindow.html#a691084573aac1441ca7464961447bea9',1,'Digikam::LightTableWindow']]],
  ['leftzoombar',['leftZoomBar',['../classDigikam_1_1LightTableWindow.html#a7b5d9d61a5d36f84509267fe50550736',1,'Digikam::LightTableWindow']]],
  ['leftzoomfittowindowaction',['leftZoomFitToWindowAction',['../classDigikam_1_1LightTableWindow.html#a347d3bc544253c0a7df8b004f8121812',1,'Digikam::LightTableWindow']]],
  ['leftzoomminusaction',['leftZoomMinusAction',['../classDigikam_1_1LightTableWindow.html#a6fccaab37863af8da8a4682b6ded3661',1,'Digikam::LightTableWindow']]],
  ['leftzoomplusaction',['leftZoomPlusAction',['../classDigikam_1_1LightTableWindow.html#a6d6263728c9cbcf51765e2a4d4b722a4',1,'Digikam::LightTableWindow']]],
  ['leftzoomto100percents',['leftZoomTo100percents',['../classDigikam_1_1LightTableWindow.html#aa07c1f8b5d8b53c7fcb2ccddaef12119',1,'Digikam::LightTableWindow']]],
  ['length',['length',['../structen265__packet.html#a6cbe18072b3672e3c8a548d74aa6fc41',1,'en265_packet::length()'],['../structheif_1_1Box__iloc_1_1Extent.html#aeede1e0fe17b756035c42ff97a46ea62',1,'heif::Box_iloc::Extent::length()']]],
  ['lens',['lens',['../classDigikam_1_1ImageMetadataContainer.html#a47be57b16c08cb5371ac6c0453866149',1,'Digikam::ImageMetadataContainer::lens()'],['../classDigikam_1_1PhotoInfoContainer.html#aced0c502d387d2aeeb370f3b59a5a06c',1,'Digikam::PhotoInfoContainer::lens()']]],
  ['lensbarrelcoefficienta',['lensBarrelCoefficientA',['../structDigikam_1_1PTOType_1_1Image.html#aea8506d65a9879f50acc92306cf0fdcc',1,'Digikam::PTOType::Image']]],
  ['lensbarrelcoefficientb',['lensBarrelCoefficientB',['../structDigikam_1_1PTOType_1_1Image.html#a214a6641b078503fbe3e052ed9e5c713',1,'Digikam::PTOType::Image']]],
  ['lensbarrelcoefficientc',['lensBarrelCoefficientC',['../structDigikam_1_1PTOType_1_1Image.html#a25d7a0a206afe4a3f3b10b2c71aafa25',1,'Digikam::PTOType::Image']]],
  ['lenscenteroffsetx',['lensCenterOffsetX',['../structDigikam_1_1PTOType_1_1Image.html#a5dbd41099c7789a9f0977d21aeb95a65',1,'Digikam::PTOType::Image']]],
  ['lenscenteroffsety',['lensCenterOffsetY',['../structDigikam_1_1PTOType_1_1Image.html#abe2c01dd249c4fd66de1529edc1c97c7',1,'Digikam::PTOType::Image']]],
  ['lensmodel',['lensModel',['../classDigikam_1_1LensFunContainer.html#a82c6d65b6ddac864143b79d9ce5ff50b',1,'Digikam::LensFunContainer']]],
  ['lensprojection',['lensProjection',['../structDigikam_1_1PTOType_1_1Image.html#a02f846127c18a6c012f2a20b6f0c6e31',1,'Digikam::PTOType::Image']]],
  ['lensshearx',['lensShearX',['../structDigikam_1_1PTOType_1_1Image.html#a13c5b6250504e53fc041dd032e287817',1,'Digikam::PTOType::Image']]],
  ['lenssheary',['lensShearY',['../structDigikam_1_1PTOType_1_1Image.html#a1aacc208d8753b723d8a113310cc1ad3',1,'Digikam::PTOType::Image']]],
  ['level',['level',['../classDigikam_1_1ColorFXContainer.html#a0580326623ce80e3ab09b8756e96798f',1,'Digikam::ColorFXContainer']]],
  ['level_5fidc',['level_idc',['../classprofile__data.html#a6a9c7c966ff884080022b6b7f07eb3a4',1,'profile_data']]],
  ['level_5fpresent_5fflag',['level_present_flag',['../classprofile__data.html#aa09599a90c907bc7a26181bd6bbb0d0f',1,'profile_data']]],
  ['levels',['levels',['../classDigikamGenericExpoBlendingPlugin_1_1EnfuseSettings.html#a01e016ef3306dd6c7b94384ab2d4b895',1,'DigikamGenericExpoBlendingPlugin::EnfuseSettings']]],
  ['lightness',['lightness',['../classDigikam_1_1HSLContainer.html#a05c9b35ed1dd0ceef1f6db76e2bd349f',1,'Digikam::HSLContainer']]],
  ['line',['line',['../classUi__DateOptionDialogWidget.html#aeafe7b8d4278d333499a4573440de7c3',1,'Ui_DateOptionDialogWidget::line()'],['../classDigikam_1_1DDatePicker.html#a2c4e5213e56763cf28e1778bd13ef737',1,'Digikam::DDatePicker::line()']]],
  ['line1',['line1',['../classUi__CalEvents.html#a9bbe579d9551be9e43d798610508df2c',1,'Ui_CalEvents']]],
  ['lineedit',['lineEdit',['../classDigikam_1_1AssignNameWidget.html#af1faf69b47cbb6f9f3bfccbef71c6271',1,'Digikam::AssignNameWidget']]],
  ['linesize',['lineSize',['../classDigikam_1_1VideoFrame.html#a3c27e4d9b7ecb1cf8b550e013362c816',1,'Digikam::VideoFrame']]],
  ['linput',['lInput',['../classDigikam_1_1LevelsContainer.html#aa2de4a9719ccc368980a27407d647cc6',1,'Digikam::LevelsContainer']]],
  ['list',['list',['../classDigikam_1_1QListImageListProvider.html#a48226f98309a379fafd30da0d020d37d',1,'Digikam::QListImageListProvider']]],
  ['list_5fentry_5fl0',['list_entry_l0',['../classslice__segment__header.html#a40ee0e65d82f7ae9c5f6a55a8b60dab9',1,'slice_segment_header']]],
  ['list_5fentry_5fl1',['list_entry_l1',['../classslice__segment__header.html#ae7debc09a18fe2d11c42e4a531d245c8',1,'slice_segment_header']]],
  ['list_5fparameters',['list_parameters',['../structheif__encoder__plugin.html#a296bb39e4fbf04350e1ba2b4312493a5',1,'heif_encoder_plugin']]],
  ['listonlyavailableimages',['listOnlyAvailableImages',['../classDigikam_1_1ItemLister.html#a0948aeb6a7d6360a629035e2a8263486',1,'Digikam::ItemLister']]],
  ['listphotosizes',['ListPhotoSizes',['../classUi__AdvPrintPhotoPage.html#aad35f031dca1363285f65f80942e1af1',1,'Ui_AdvPrintPhotoPage']]],
  ['lists_5fmodification_5fpresent_5fflag',['lists_modification_present_flag',['../classpic__parameter__set.html#a3f5ee11883a4f586b17d6caebed2d697',1,'pic_parameter_set']]],
  ['listview',['listView',['../classDigikam_1_1DCategorizedView.html#a3925d1c8029865392106c83d2d25907c',1,'Digikam::DCategorizedView']]],
  ['lo',['lo',['../structMD5__CTX.html#a90437ec62a8dda787f1667061d9755fe',1,'MD5_CTX']]],
  ['loadedfile',['loadedFile',['../classDigikam_1_1VersionFileOperation.html#aeea91528f89c800501f6e0f2cb84deaf',1,'Digikam::VersionFileOperation']]],
  ['loadedfromdisk',['loadedFromDisk',['../classDigikam_1_1ItemScanner.html#a665961f43a7d4321e6b10c6006a46fbf',1,'Digikam::ItemScanner']]],
  ['loadedfromsidecar',['loadedFromSidecar',['../classDigikam_1_1MetaEngine.html#a3d587cfc1a4ca431b0bd7fd3af44678f',1,'Digikam::MetaEngine']]],
  ['loaderror',['loadError',['../classDigikam_1_1TrackReader_1_1TrackReadResult.html#a791bbe69176ec02f6651d183b69341d9',1,'Digikam::TrackReader::TrackReadResult']]],
  ['loadingdescription',['loadingDescription',['../classDigikam_1_1ThumbnailResult.html#a8d5cf807630e176154aaa9366f39ec19',1,'Digikam::ThumbnailResult']]],
  ['localfiles',['localFiles',['../classDigikam_1_1SidecarFinder.html#a769860a6cc155afe23df9d66c2881b02',1,'Digikam::SidecarFinder']]],
  ['localfilesuffixes',['localFileSuffixes',['../classDigikam_1_1SidecarFinder.html#a005f081a240eaa6fc1a40803c51a0df9',1,'Digikam::SidecarFinder']]],
  ['localtargetradiobutton_5f',['LocalTargetRadioButton_',['../classDigikamGenericGeolocationEditPlugin_1_1KmlWidget.html#ac34e831448d753102c9a497a74cf14c0',1,'DigikamGenericGeolocationEditPlugin::KmlWidget']]],
  ['location',['location',['../classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#abdab967bb3cb3eb4a9c79fc028046844',1,'DigikamGenericGoogleServicesPlugin::GSPhoto::location()'],['../classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#ab842089750a9573bb77a3661572f68b3',1,'DigikamGenericGoogleServicesPlugin::GSFolder::location()'],['../classDigikamGenericTwitterPlugin_1_1TwAlbum.html#a0ae45bff89b4569696840f19a09e34ea',1,'DigikamGenericTwitterPlugin::TwAlbum::location()'],['../classDigikam_1_1WSAlbum.html#a7fd45d3c50b51b045cb331450ed27549',1,'Digikam::WSAlbum::location()'],['../classDigikam_1_1IptcCoreLocationInfo.html#ab17df4c391486a73f2c2f52af937f6d6',1,'Digikam::IptcCoreLocationInfo::location()'],['../classDigikam_1_1SetupCollectionModel_1_1Item.html#a8274b7ee01f9215d906f8b2bf2734076',1,'Digikam::SetupCollectionModel::Item::location()']]],
  ['locations',['locations',['../classDigikam_1_1CollectionManager.html#ab673b174aff8114b8d57b7ac1aa7c17e',1,'Digikam::CollectionManager']]],
  ['lock',['lock',['../classDigikam_1_1CollectionManager.html#aaea41e50a309e71c119c4c038c445d8b',1,'Digikam::CollectionManager::lock()'],['../classDigikam_1_1CollectionScannerHintContainerImplementation.html#a1690c459f7820550cabe919ba351f466',1,'Digikam::CollectionScannerHintContainerImplementation::lock()'],['../classDigikam_1_1BdEngineBackendPrivate.html#a9b7da8ab5a58a00628f941b4efe4148e',1,'Digikam::BdEngineBackendPrivate::lock()']]],
  ['lockaction',['lockAction',['../classDigikam_1_1ImportUI.html#a44fd59901f6abb163fc6cf9131c06f69',1,'Digikam::ImportUI']]],
  ['lockcount',['lockCount',['../classDigikam_1_1DbEngineLocking.html#ab2a069d718a9e557597b8522cc0302b1',1,'Digikam::DbEngineLocking']]],
  ['lockrect',['lockRect',['../classDigikam_1_1ImportDelegate.html#ab84c4f76c9cf6462db4783ef1e52ce34',1,'Digikam::ImportDelegate']]],
  ['log2_5fdiff_5fmax_5fmin_5fluma_5fcoding_5fblock_5fsize',['log2_diff_max_min_luma_coding_block_size',['../classseq__parameter__set.html#a1838e7e88ee732bc3af9629ca268e471',1,'seq_parameter_set']]],
  ['log2_5fdiff_5fmax_5fmin_5fpcm_5fluma_5fcoding_5fblock_5fsize',['log2_diff_max_min_pcm_luma_coding_block_size',['../classseq__parameter__set.html#ab729dae89c9a25b5d4c76bde6d841f15',1,'seq_parameter_set']]],
  ['log2_5fdiff_5fmax_5fmin_5ftransform_5fblock_5fsize',['log2_diff_max_min_transform_block_size',['../classseq__parameter__set.html#a64523b6cc9bff0834ddd7bc2c631afc8',1,'seq_parameter_set']]],
  ['log2_5fmax_5fmv_5flength_5fhorizontal',['log2_max_mv_length_horizontal',['../classvideo__usability__information.html#a2468909baca67637906f5b8caea7b404',1,'video_usability_information']]],
  ['log2_5fmax_5fmv_5flength_5fvertical',['log2_max_mv_length_vertical',['../classvideo__usability__information.html#a0579e81a7f888835228d8402d202cd2b',1,'video_usability_information']]],
  ['log2_5fmax_5fpic_5forder_5fcnt_5flsb',['log2_max_pic_order_cnt_lsb',['../classseq__parameter__set.html#a437dc5e0cd89c84082f46a5872b582b1',1,'seq_parameter_set']]],
  ['log2_5fmax_5ftransform_5fskip_5fblock_5fsize',['log2_max_transform_skip_block_size',['../classpps__range__extension.html#a6bce3d8553e7adad7b49be27a41bc899',1,'pps_range_extension']]],
  ['log2_5fmin_5fluma_5fcoding_5fblock_5fsize',['log2_min_luma_coding_block_size',['../classseq__parameter__set.html#abb936715b3bc6145b163b8ab8bcc20ce',1,'seq_parameter_set']]],
  ['log2_5fmin_5fpcm_5fluma_5fcoding_5fblock_5fsize',['log2_min_pcm_luma_coding_block_size',['../classseq__parameter__set.html#ab4b26ef2858fae17793afa0700864fc8',1,'seq_parameter_set']]],
  ['log2_5fmin_5ftransform_5fblock_5fsize',['log2_min_transform_block_size',['../classseq__parameter__set.html#a8a3aa8dd3b7f5a716b7f1511c0bc375c',1,'seq_parameter_set']]],
  ['log2_5fparallel_5fmerge_5flevel',['log2_parallel_merge_level',['../classpic__parameter__set.html#aff03ba2bbb250ab6a3c09406c3e3dd04',1,'pic_parameter_set']]],
  ['log2_5fsao_5foffset_5fscale_5fchroma',['log2_sao_offset_scale_chroma',['../classpps__range__extension.html#ab5e06f9b200c008da049631e534c0027',1,'pps_range_extension']]],
  ['log2_5fsao_5foffset_5fscale_5fluma',['log2_sao_offset_scale_luma',['../classpps__range__extension.html#a57262e53319fe5690ac3a4b10cccc2d5',1,'pps_range_extension']]],
  ['log2cbsize',['log2CbSize',['../structCB__ref__info.html#a8a27572b41c8fe302907c1220bdaa280',1,'CB_ref_info']]],
  ['log2ctbsizey',['Log2CtbSizeY',['../classseq__parameter__set.html#a6959c6767df6c1db5448cb74fada859f',1,'seq_parameter_set']]],
  ['log2maxipcmcbsizey',['Log2MaxIpcmCbSizeY',['../classseq__parameter__set.html#a7c0229355241d7052c494b1d038011a2',1,'seq_parameter_set']]],
  ['log2maxtrafosize',['Log2MaxTrafoSize',['../classseq__parameter__set.html#a41ba1405a8975a8254403927c8881332',1,'seq_parameter_set']]],
  ['log2maxtransformskipsize',['Log2MaxTransformSkipSize',['../classpic__parameter__set.html#aa36af106f45a8398c8380cb585d4fb18',1,'pic_parameter_set']]],
  ['log2mincbsizey',['Log2MinCbSizeY',['../classseq__parameter__set.html#a52ad53340afded651b291f01f37c060d',1,'seq_parameter_set']]],
  ['log2mincuchromaqpoffsetsize',['Log2MinCuChromaQpOffsetSize',['../classpic__parameter__set.html#ad2afb9a5e1c0738e4b3a000d0a70f021',1,'pic_parameter_set']]],
  ['log2mincuqpdeltasize',['Log2MinCuQpDeltaSize',['../classpic__parameter__set.html#a853dde8c9cb7badff346e903c09268d6',1,'pic_parameter_set']]],
  ['log2minipcmcbsizey',['Log2MinIpcmCbSizeY',['../classseq__parameter__set.html#a111d6b585b799312c0992241c10dc4aa',1,'seq_parameter_set']]],
  ['log2minpusize',['Log2MinPUSize',['../classseq__parameter__set.html#a6795502f53f2387e67e9d83dd73cf1ac',1,'seq_parameter_set']]],
  ['log2mintrafosize',['Log2MinTrafoSize',['../classseq__parameter__set.html#ae814e6eb5019fe5ca9423c62e1899887',1,'seq_parameter_set']]],
  ['log2size',['log2Size',['../classenc__node.html#a9e7b6dd8250d36cab0601cd3dbfa6b29',1,'enc_node']]],
  ['log2unitsize',['log2unitSize',['../classMetaDataArray.html#a612b4b73bb9a603e35839f4afd4411fc',1,'MetaDataArray']]],
  ['long_5fterm_5fref_5fpics_5fpresent_5fflag',['long_term_ref_pics_present_flag',['../classseq__parameter__set.html#a20f8190378d30451d884613274d4215f',1,'seq_parameter_set']]],
  ['longitude',['longitude',['../classDigikam_1_1ItemInfoData.html#a2869fb04e29ef948c54e5467f5bafd39',1,'Digikam::ItemInfoData::longitude()'],['../classDigikam_1_1SaveProperties.html#a82145504210d65ca87a8066b4d646e07',1,'Digikam::SaveProperties::longitude()']]],
  ['longterm',['longterm',['../structimage__data.html#ab960f8b2652fc8defcd25b7be7600e4e',1,'image_data']]],
  ['longtermrefpic',['LongTermRefPic',['../classslice__segment__header.html#af198a27fcc120cb6cd03201828bbd8f6',1,'slice_segment_header']]],
  ['longtimemessageboxresult',['longTimeMessageBoxResult',['../classDigikam_1_1AlbumManager.html#a17e1beb0fb86b96064c5e6e1548fad96',1,'Digikam::AlbumManager']]],
  ['loop',['loop',['../classDigikamGenericPresentationPlugin_1_1PresentationContainer.html#aec5729a5efa35dabf8768542d5aac346',1,'DigikamGenericPresentationPlugin::PresentationContainer::loop()'],['../classDigikam_1_1SlideShowSettings.html#ac8ab1217da218c622d7f1e781ac01667',1,'Digikam::SlideShowSettings::loop()']]],
  ['loop_5ffilter_5facross_5ftiles_5fenabled_5fflag',['loop_filter_across_tiles_enabled_flag',['../classpic__parameter__set.html#a82a9e3eb552048659a891e91b0e00eca',1,'pic_parameter_set']]],
  ['losslessformat',['losslessFormat',['../classDigikam_1_1DownloadSettings.html#adfa763eb29cbf845673455dd7d1b909c',1,'Digikam::DownloadSettings']]],
  ['loutput',['lOutput',['../classDigikam_1_1LevelsContainer.html#ac953c6546213d0a9bc0c7b1c37f71b0f',1,'Digikam::LevelsContainer']]],
  ['lowqrejected',['lowQRejected',['../classDigikam_1_1ImageQualityContainer.html#a39b737194978b038248a7008d40bf7bb',1,'Digikam::ImageQualityContainer']]],
  ['lowsaturation',['lowSaturation',['../classDigikam_1_1LocalContrastContainer.html#a5d4eee224a1f6e75a485ba5c4362f478',1,'Digikam::LocalContrastContainer']]],
  ['lowthreshold',['lowThreshold',['../classDigikam_1_1ImageQualityParser.html#ad5541cd303df1046c7a5ae3212b25cef',1,'Digikam::ImageQualityParser']]],
  ['lt_5fidx_5fsps',['lt_idx_sps',['../classslice__segment__header.html#a4565572e48e095969fa417148292a841',1,'slice_segment_header']]],
  ['lt_5fref_5fpic_5fpoc_5flsb_5fsps',['lt_ref_pic_poc_lsb_sps',['../classseq__parameter__set.html#af6fa252d5f3ddb6a0b22eaefdc2389f3',1,'seq_parameter_set']]],
  ['ltaction',['ltAction',['../classDigikam_1_1DigikamApp.html#a58e8c1201952e06566ef3acef18f269a',1,'Digikam::DigikamApp']]],
  ['luma_5flog2_5fweight_5fdenom',['luma_log2_weight_denom',['../classslice__segment__header.html#a81363507d8debf9b9ad6bab41bef2956',1,'slice_segment_header']]],
  ['luma_5foffset',['luma_offset',['../classslice__segment__header.html#a3063dfb0ba1ca2173421cd96d974c2dd',1,'slice_segment_header']]],
  ['luma_5fweight_5fflag',['luma_weight_flag',['../classslice__segment__header.html#a5f528f40006b28936d0dea05742897e9',1,'slice_segment_header']]],
  ['lumahighlights',['lumaHighlights',['../classDigikam_1_1FilmGrainContainer.html#af0a7948122515c84bd00774248ce2954',1,'Digikam::FilmGrainContainer']]],
  ['lumaintensity',['lumaIntensity',['../classDigikam_1_1FilmGrainContainer.html#a11a27e1c567297d22aff7f007c112119',1,'Digikam::FilmGrainContainer']]],
  ['lumamidtones',['lumaMidtones',['../classDigikam_1_1FilmGrainContainer.html#a1393582591bc21897238011dc6c99ad5',1,'Digikam::FilmGrainContainer']]],
  ['lumashadows',['lumaShadows',['../classDigikam_1_1FilmGrainContainer.html#a41ebfa127e415a0961eb60c2f5f029fd',1,'Digikam::FilmGrainContainer']]],
  ['lumaweight',['LumaWeight',['../classslice__segment__header.html#a48deb42271ccbe839eaea6afe821106c',1,'slice_segment_header']]],
  ['luminosity',['luminosity',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixel.html#aa995a54d8f61ec87e61261d33215f6ff',1,'DigikamEditorHotPixelsToolPlugin::HotPixel']]]
];
