var searchData=
[
  ['safetemporaryfile',['SafeTemporaryFile',['../classDigikam_1_1SafeTemporaryFile.html',1,'Digikam']]],
  ['salbum',['SAlbum',['../classDigikam_1_1SAlbum.html',1,'Digikam']]],
  ['sao_5finfo',['sao_info',['../structsao__info.html',1,'']]],
  ['saveproperties',['SaveProperties',['../classDigikam_1_1SaveProperties.html',1,'Digikam']]],
  ['savingcontext',['SavingContext',['../classDigikam_1_1SavingContext.html',1,'Digikam']]],
  ['savingtask',['SavingTask',['../classDigikam_1_1SavingTask.html',1,'Digikam']]],
  ['scaling_5flist_5fdata',['scaling_list_data',['../structscaling__list__data.html',1,'']]],
  ['scalingoptions',['ScalingOptions',['../classheif_1_1Image_1_1ScalingOptions.html',1,'heif::Image']]],
  ['scan_5fposition',['scan_position',['../structscan__position.html',1,'']]],
  ['scancontroller',['ScanController',['../classDigikam_1_1ScanController.html',1,'Digikam']]],
  ['scancontrollercreator',['ScanControllerCreator',['../classDigikam_1_1ScanControllerCreator.html',1,'Digikam']]],
  ['scancontrollerloadingcachefilewatch',['ScanControllerLoadingCacheFileWatch',['../classDigikam_1_1ScanControllerLoadingCacheFileWatch.html',1,'Digikam']]],
  ['scanstatefilter',['ScanStateFilter',['../classDigikam_1_1ScanStateFilter.html',1,'Digikam']]],
  ['schememanager',['SchemeManager',['../classDigikam_1_1SchemeManager.html',1,'Digikam']]],
  ['searchchangeset',['SearchChangeset',['../classDigikam_1_1SearchChangeset.html',1,'Digikam']]],
  ['searchesdbjobinfo',['SearchesDBJobInfo',['../classDigikam_1_1SearchesDBJobInfo.html',1,'Digikam']]],
  ['searchesdbjobsthread',['SearchesDBJobsThread',['../classDigikam_1_1SearchesDBJobsThread.html',1,'Digikam']]],
  ['searchesjob',['SearchesJob',['../classDigikam_1_1SearchesJob.html',1,'Digikam']]],
  ['searchfield',['SearchField',['../classDigikam_1_1SearchField.html',1,'Digikam']]],
  ['searchfieldalbum',['SearchFieldAlbum',['../classDigikam_1_1SearchFieldAlbum.html',1,'Digikam']]],
  ['searchfieldcheckbox',['SearchFieldCheckBox',['../classDigikam_1_1SearchFieldCheckBox.html',1,'Digikam']]],
  ['searchfieldchoice',['SearchFieldChoice',['../classDigikam_1_1SearchFieldChoice.html',1,'Digikam']]],
  ['searchfieldcolordepth',['SearchFieldColorDepth',['../classDigikam_1_1SearchFieldColorDepth.html',1,'Digikam']]],
  ['searchfieldcombobox',['SearchFieldComboBox',['../classDigikam_1_1SearchFieldComboBox.html',1,'Digikam']]],
  ['searchfieldgroup',['SearchFieldGroup',['../classDigikam_1_1SearchFieldGroup.html',1,'Digikam']]],
  ['searchfieldgrouplabel',['SearchFieldGroupLabel',['../classDigikam_1_1SearchFieldGroupLabel.html',1,'Digikam']]],
  ['searchfieldkeyword',['SearchFieldKeyword',['../classDigikam_1_1SearchFieldKeyword.html',1,'Digikam']]],
  ['searchfieldlabels',['SearchFieldLabels',['../classDigikam_1_1SearchFieldLabels.html',1,'Digikam']]],
  ['searchfieldpageorientation',['SearchFieldPageOrientation',['../classDigikam_1_1SearchFieldPageOrientation.html',1,'Digikam']]],
  ['searchfieldrangedate',['SearchFieldRangeDate',['../classDigikam_1_1SearchFieldRangeDate.html',1,'Digikam']]],
  ['searchfieldrangedouble',['SearchFieldRangeDouble',['../classDigikam_1_1SearchFieldRangeDouble.html',1,'Digikam']]],
  ['searchfieldrangeint',['SearchFieldRangeInt',['../classDigikam_1_1SearchFieldRangeInt.html',1,'Digikam']]],
  ['searchfieldrating',['SearchFieldRating',['../classDigikam_1_1SearchFieldRating.html',1,'Digikam']]],
  ['searchfieldtext',['SearchFieldText',['../classDigikam_1_1SearchFieldText.html',1,'Digikam']]],
  ['searchfiltermodel',['SearchFilterModel',['../classDigikam_1_1SearchFilterModel.html',1,'Digikam']]],
  ['searchgroup',['SearchGroup',['../classDigikam_1_1SearchGroup.html',1,'Digikam']]],
  ['searchinfo',['SearchInfo',['../classDigikam_1_1SearchInfo.html',1,'Digikam']]],
  ['searchmodel',['SearchModel',['../classDigikam_1_1SearchModel.html',1,'Digikam']]],
  ['searchresult',['SearchResult',['../classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html',1,'DigikamGenericGeolocationEditPlugin::SearchBackend']]],
  ['searchresultitem',['SearchResultItem',['../classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem.html',1,'DigikamGenericGeolocationEditPlugin::SearchResultModel']]],
  ['searchtextfiltersettings',['SearchTextFilterSettings',['../classDigikam_1_1SearchTextFilterSettings.html',1,'Digikam']]],
  ['searchtextsettings',['SearchTextSettings',['../classDigikam_1_1SearchTextSettings.html',1,'Digikam']]],
  ['searchtreeview',['SearchTreeView',['../classDigikam_1_1SearchTreeView.html',1,'Digikam']]],
  ['searchviewbottombar',['SearchViewBottomBar',['../classDigikam_1_1SearchViewBottomBar.html',1,'Digikam']]],
  ['searchviewthemedpartscache',['SearchViewThemedPartsCache',['../classDigikam_1_1SearchViewThemedPartsCache.html',1,'Digikam']]],
  ['searchxmlcachingreader',['SearchXmlCachingReader',['../classDigikam_1_1SearchXmlCachingReader.html',1,'Digikam']]],
  ['searchxmlreader',['SearchXmlReader',['../classDigikam_1_1SearchXmlReader.html',1,'Digikam']]],
  ['searchxmlwriter',['SearchXmlWriter',['../classDigikam_1_1SearchXmlWriter.html',1,'Digikam']]],
  ['sei_5fdecoded_5fpicture_5fhash',['sei_decoded_picture_hash',['../structsei__decoded__picture__hash.html',1,'']]],
  ['sei_5fmessage',['sei_message',['../structsei__message.html',1,'']]],
  ['seimessage',['SEIMessage',['../classheif_1_1SEIMessage.html',1,'heif']]],
  ['seimessage_5fdepth_5frepresentation_5finfo',['SEIMessage_depth_representation_info',['../classheif_1_1SEIMessage__depth__representation__info.html',1,'heif']]],
  ['selectionmodel',['SelectionModel',['../classDigikam_1_1DConfigDlgInternal_1_1SelectionModel.html',1,'Digikam::DConfigDlgInternal']]],
  ['sendbymailplugin',['SendByMailPlugin',['../classDigikamGenericSendByMailPlugin_1_1SendByMailPlugin.html',1,'DigikamGenericSendByMailPlugin']]],
  ['seq_5fparameter_5fset',['seq_parameter_set',['../classseq__parameter__set.html',1,'']]],
  ['sequencenumberdialog',['SequenceNumberDialog',['../classDigikam_1_1SequenceNumberDialog.html',1,'Digikam']]],
  ['sequencenumberoption',['SequenceNumberOption',['../classDigikam_1_1SequenceNumberOption.html',1,'Digikam']]],
  ['set',['Set',['../classDigikam_1_1DatabaseFields_1_1Set.html',1,'Digikam::DatabaseFields']]],
  ['setupcollectiondelegate',['SetupCollectionDelegate',['../classDigikam_1_1SetupCollectionDelegate.html',1,'Digikam']]],
  ['setupcollectionmodel',['SetupCollectionModel',['../classDigikam_1_1SetupCollectionModel.html',1,'Digikam']]],
  ['setupcollectiontreeview',['SetupCollectionTreeView',['../classDigikam_1_1SetupCollectionTreeView.html',1,'Digikam']]],
  ['shapepredictor',['ShapePredictor',['../classDigikam_1_1RedEye_1_1ShapePredictor.html',1,'Digikam::RedEye']]],
  ['sharedloadingtask',['SharedLoadingTask',['../classDigikam_1_1SharedLoadingTask.html',1,'Digikam']]],
  ['sharedloadsavethread',['SharedLoadSaveThread',['../classDigikam_1_1SharedLoadSaveThread.html',1,'Digikam']]],
  ['sharpcontainer',['SharpContainer',['../classDigikam_1_1SharpContainer.html',1,'Digikam']]],
  ['sharpen',['Sharpen',['../classDigikamBqmSharpenPlugin_1_1Sharpen.html',1,'DigikamBqmSharpenPlugin']]],
  ['sharpenfilter',['SharpenFilter',['../classDigikam_1_1SharpenFilter.html',1,'Digikam']]],
  ['sharpenplugin',['SharpenPlugin',['../classDigikamBqmSharpenPlugin_1_1SharpenPlugin.html',1,'DigikamBqmSharpenPlugin']]],
  ['sharpentoolplugin',['SharpenToolPlugin',['../classDigikamEditorSharpenToolPlugin_1_1SharpenToolPlugin.html',1,'DigikamEditorSharpenToolPlugin']]],
  ['sheartoolplugin',['ShearToolPlugin',['../classDigikamEditorShearToolPlugin_1_1ShearToolPlugin.html',1,'DigikamEditorShearToolPlugin']]],
  ['showfoto',['ShowFoto',['../classShowFoto_1_1ShowFoto.html',1,'ShowFoto']]],
  ['showfotocoordinatesoverlay',['ShowfotoCoordinatesOverlay',['../classShowFoto_1_1ShowfotoCoordinatesOverlay.html',1,'ShowFoto']]],
  ['showfotocoordinatesoverlaywidget',['ShowfotoCoordinatesOverlayWidget',['../classShowFoto_1_1ShowfotoCoordinatesOverlayWidget.html',1,'ShowFoto']]],
  ['showfotodelegate',['ShowfotoDelegate',['../classShowFoto_1_1ShowfotoDelegate.html',1,'ShowFoto']]],
  ['showfotodragdrophandler',['ShowfotoDragDropHandler',['../classShowFoto_1_1ShowfotoDragDropHandler.html',1,'ShowFoto']]],
  ['showfotoiteminfo',['ShowfotoItemInfo',['../classShowFoto_1_1ShowfotoItemInfo.html',1,'ShowFoto']]],
  ['showfotoitemsortsettings',['ShowfotoItemSortSettings',['../classShowFoto_1_1ShowfotoItemSortSettings.html',1,'ShowFoto']]],
  ['showfotonormaldelegate',['ShowfotoNormalDelegate',['../classShowFoto_1_1ShowfotoNormalDelegate.html',1,'ShowFoto']]],
  ['showfotonormaldelegateprivate',['ShowfotoNormalDelegatePrivate',['../classShowFoto_1_1ShowfotoNormalDelegatePrivate.html',1,'ShowFoto']]],
  ['showfotosortfiltermodel',['ShowfotoSortFilterModel',['../classShowFoto_1_1ShowfotoSortFilterModel.html',1,'ShowFoto']]],
  ['showfotothumbnaildelegate',['ShowfotoThumbnailDelegate',['../classShowFoto_1_1ShowfotoThumbnailDelegate.html',1,'ShowFoto']]],
  ['showfotothumbnaildelegateprivate',['ShowfotoThumbnailDelegatePrivate',['../classShowFoto_1_1ShowfotoThumbnailDelegatePrivate.html',1,'ShowFoto']]],
  ['sidebarwidget',['SidebarWidget',['../classDigikam_1_1SidebarWidget.html',1,'Digikam']]],
  ['sidecarfinder',['SidecarFinder',['../classDigikam_1_1SidecarFinder.html',1,'Digikam']]],
  ['signaturedata',['SignatureData',['../classDigikam_1_1Haar_1_1SignatureData.html',1,'Digikam::Haar']]],
  ['signaturemap',['SignatureMap',['../classDigikam_1_1Haar_1_1SignatureMap.html',1,'Digikam::Haar']]],
  ['similaritydbaccess',['SimilarityDbAccess',['../classDigikam_1_1SimilarityDbAccess.html',1,'Digikam']]],
  ['similaritydbbackend',['SimilarityDbBackend',['../classDigikam_1_1SimilarityDbBackend.html',1,'Digikam']]],
  ['simplecollectionscannerobserver',['SimpleCollectionScannerObserver',['../classDigikam_1_1SimpleCollectionScannerObserver.html',1,'Digikam']]],
  ['slice_5fsegment_5fheader',['slice_segment_header',['../classslice__segment__header.html',1,'']]],
  ['slice_5funit',['slice_unit',['../classslice__unit.html',1,'']]],
  ['slideend',['SlideEnd',['../classDigikam_1_1SlideEnd.html',1,'Digikam']]],
  ['slidehelp',['SlideHelp',['../classDigikam_1_1SlideHelp.html',1,'Digikam']]],
  ['slideshowplugin',['SlideShowPlugin',['../classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html',1,'DigikamGenericSlideShowPlugin']]],
  ['slideshowsettings',['SlideShowSettings',['../classDigikam_1_1SlideShowSettings.html',1,'Digikam']]],
  ['small_5fimage_5fbuffer',['small_image_buffer',['../classsmall__image__buffer.html',1,'']]],
  ['smugalbum',['SmugAlbum',['../classDigikamGenericSmugPlugin_1_1SmugAlbum.html',1,'DigikamGenericSmugPlugin']]],
  ['smugalbumtmpl',['SmugAlbumTmpl',['../classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl.html',1,'DigikamGenericSmugPlugin']]],
  ['smugcategory',['SmugCategory',['../classDigikamGenericSmugPlugin_1_1SmugCategory.html',1,'DigikamGenericSmugPlugin']]],
  ['smugmpform',['SmugMPForm',['../classDigikamGenericSmugPlugin_1_1SmugMPForm.html',1,'DigikamGenericSmugPlugin']]],
  ['smugphoto',['SmugPhoto',['../classDigikamGenericSmugPlugin_1_1SmugPhoto.html',1,'DigikamGenericSmugPlugin']]],
  ['smugplugin',['SmugPlugin',['../classDigikamGenericSmugPlugin_1_1SmugPlugin.html',1,'DigikamGenericSmugPlugin']]],
  ['smuguser',['SmugUser',['../classDigikamGenericSmugPlugin_1_1SmugUser.html',1,'DigikamGenericSmugPlugin']]],
  ['smugwidget',['SmugWidget',['../classDigikamGenericSmugPlugin_1_1SmugWidget.html',1,'DigikamGenericSmugPlugin']]],
  ['solidvolumeinfo',['SolidVolumeInfo',['../classDigikam_1_1SolidVolumeInfo.html',1,'Digikam']]],
  ['sop_5fcreator',['sop_creator',['../classsop__creator.html',1,'']]],
  ['sop_5fcreator_5fintra_5fonly',['sop_creator_intra_only',['../classsop__creator__intra__only.html',1,'']]],
  ['sop_5fcreator_5ftrivial_5flow_5fdelay',['sop_creator_trivial_low_delay',['../classsop__creator__trivial__low__delay.html',1,'']]],
  ['soundtrackpreview',['SoundtrackPreview',['../classDigikamGenericPresentationPlugin_1_1SoundtrackPreview.html',1,'DigikamGenericPresentationPlugin']]],
  ['sparsemodelindexvector',['SparseModelIndexVector',['../classDigikam_1_1SparseModelIndexVector.html',1,'Digikam']]],
  ['splitfeature',['SplitFeature',['../structDigikam_1_1RedEye_1_1SplitFeature.html',1,'Digikam::RedEye']]],
  ['sps_5frange_5fextension',['sps_range_extension',['../classsps__range__extension.html',1,'']]],
  ['standardcollector',['StandardCollector',['../classDigikam_1_1Face_1_1StandardCollector.html',1,'Digikam::Face']]],
  ['startscanpage',['StartScanPage',['../classDigikam_1_1StartScanPage.html',1,'Digikam']]],
  ['state',['State',['../structDigikam_1_1State.html',1,'Digikam']]],
  ['statistics',['Statistics',['../classDigikam_1_1RecognitionBenchmarker_1_1Statistics.html',1,'Digikam::RecognitionBenchmarker']]],
  ['staypoppedupcombobox',['StayPoppedUpComboBox',['../classDigikam_1_1StayPoppedUpComboBox.html',1,'Digikam']]],
  ['stitcher',['Stitcher',['../structDigikam_1_1PTOType_1_1Stitcher.html',1,'Digikam::PTOType']]],
  ['streamreader',['StreamReader',['../classheif_1_1StreamReader.html',1,'heif']]],
  ['streamreader_5fcapi',['StreamReader_CApi',['../classheif_1_1StreamReader__CApi.html',1,'heif']]],
  ['streamreader_5fistream',['StreamReader_istream',['../classheif_1_1StreamReader__istream.html',1,'heif']]],
  ['streamreader_5fmemory',['StreamReader_memory',['../classheif_1_1StreamReader__memory.html',1,'heif']]],
  ['streamwriter',['StreamWriter',['../classheif_1_1StreamWriter.html',1,'heif']]],
  ['stretchfilter',['StretchFilter',['../classDigikam_1_1StretchFilter.html',1,'Digikam']]],
  ['stringthemeparameter',['StringThemeParameter',['../classDigikamGenericHtmlGalleryPlugin_1_1StringThemeParameter.html',1,'DigikamGenericHtmlGalleryPlugin']]],
  ['stylesheetdebugger',['StyleSheetDebugger',['../classDigikam_1_1StyleSheetDebugger.html',1,'Digikam']]],
  ['subjectdata',['SubjectData',['../classDigikam_1_1SubjectData.html',1,'Digikam']]],
  ['subjectedit',['SubjectEdit',['../classDigikam_1_1SubjectEdit.html',1,'Digikam']]],
  ['subquerybuilder',['SubQueryBuilder',['../classDigikam_1_1SubQueryBuilder.html',1,'Digikam']]],
  ['sylpheedbinary',['SylpheedBinary',['../classDigikamGenericSendByMailPlugin_1_1SylpheedBinary.html',1,'DigikamGenericSendByMailPlugin']]]
];
