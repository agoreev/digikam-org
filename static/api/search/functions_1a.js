var searchData=
[
  ['zoom',['zoom',['../classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html#ad69d3d1ad15a7ba61d0ef8311524e68c',1,'DigikamGenericGLViewerPlugin::GLViewerTexture::zoom()'],['../classDigikam_1_1EditorCore.html#aba90bc70175aca79e0ec566224c3518b',1,'Digikam::EditorCore::zoom()']]],
  ['zoomedsize',['zoomedSize',['../classDigikam_1_1ImageZoomSettings.html#ac38ae898234ff3eca1370700440c073a',1,'Digikam::ImageZoomSettings']]],
  ['zoomfactor',['zoomFactor',['../classDigikam_1_1ImageZoomSettings.html#a88c12b157f42b9dd8c26a053cbcc6ff1',1,'Digikam::ImageZoomSettings']]],
  ['zoomin',['zoomIn',['../classDigikam_1_1MapBackend.html#a65b9b5cbe474a769303bcceed23c5b6b',1,'Digikam::MapBackend']]],
  ['zoomout',['zoomOut',['../classDigikam_1_1MapBackend.html#a1a511ad7643c7079152562c222676750',1,'Digikam::MapBackend']]],
  ['zoomsettings',['zoomSettings',['../classDigikam_1_1GraphicsDImgItem.html#a3775eac5727ac8e3182d8bd65f75d6ed',1,'Digikam::GraphicsDImgItem::zoomSettings() const'],['../classDigikam_1_1GraphicsDImgItem.html#a0aaff72db2c4bc82609b8d5d304e056d',1,'Digikam::GraphicsDImgItem::zoomSettings()']]],
  ['zoomtooriginal',['zoomToOriginal',['../classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html#a23ce1687f273a5c630b4e115a3ae66bc',1,'DigikamGenericGLViewerPlugin::GLViewerTexture']]]
];
