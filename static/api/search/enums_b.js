var searchData=
[
  ['mailclient',['MailClient',['../classDigikamGenericSendByMailPlugin_1_1MailSettings.html#ae9b2528286b1d038309bf10c81265c00',1,'DigikamGenericSendByMailPlugin::MailSettings']]],
  ['maplayout',['MapLayout',['../namespaceDigikam.html#a82b6d92369fccc2448496234a5a21630',1,'Digikam']]],
  ['masktype',['MaskType',['../structDigikam_1_1PTOType_1_1Mask.html#a46f9407a3738a6d74377dc1190834647',1,'Digikam::PTOType::Mask']]],
  ['matchingcondition',['MatchingCondition',['../classDigikam_1_1ItemFilterSettings.html#afdc0f59869c796fe2bd76824578f6568',1,'Digikam::ItemFilterSettings']]],
  ['matchresult',['MatchResult',['../classDigikam_1_1AlbumFilterModel.html#a9ede85e2ba05d126f83fef2e573bba22',1,'Digikam::AlbumFilterModel']]],
  ['meaningofdirection',['MeaningOfDirection',['../namespaceDigikam.html#a608a46386fa8969e0c53f096aa922c29',1,'Digikam']]],
  ['mediawikidownloadtype',['MediaWikiDownloadType',['../namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0',1,'DigikamGenericMediaWikiPlugin']]],
  ['menucategoryflag',['MenuCategoryFlag',['../classDigikam_1_1ActionItemModel.html#ae9174989fbe62331972661b7676a922b',1,'Digikam::ActionItemModel']]],
  ['messagetype',['MessageType',['../classDigikam_1_1DConfigDlgTitle.html#a9b8bccbfe1048a8e630eea6111ee7e20',1,'Digikam::DConfigDlgTitle::MessageType()'],['../classDigikam_1_1DNotificationWidget.html#aab5e8f658f9fc3b4b2061cd34386e448',1,'Digikam::DNotificationWidget::MessageType()']]],
  ['metadatawritingmode',['MetadataWritingMode',['../classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055c',1,'Digikam::MetaEngine']]],
  ['mirroraxis',['MirrorAxis',['../classheif_1_1Box__imir.html#a44d7ee079da58ee49ced7ca594993029',1,'heif::Box_imir']]],
  ['mode',['Mode',['../classDigikam_1_1AssignNameWidget.html#a619924be766f146483cac698c778fe20',1,'Digikam::AssignNameWidget']]],
  ['multiplicationflags',['MultiplicationFlags',['../classDigikam_1_1DColorComposer.html#ad69346d5b432eca772b0d77429df8969',1,'Digikam::DColorComposer']]]
];
