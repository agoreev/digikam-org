var searchData=
[
  ['x',['x',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixel.html#a27f3e820e6d4e421d41f4c63bb09a3b5',1,'DigikamEditorHotPixelsToolPlugin::HotPixel']]],
  ['x265maxbitsdepth',['x265MaxBitsDepth',['../classDigikam_1_1DImgHEIFLoader.html#adb1fa913f1697d0917da31a59fccc119',1,'Digikam::DImgHEIFLoader']]],
  ['xbelreader',['XbelReader',['../classDigikam_1_1XbelReader.html#af560099e11c15fda1944243b84e5a58b',1,'Digikam::XbelReader']]],
  ['xbelwriter',['XbelWriter',['../classDigikam_1_1XbelWriter.html#ad6a6cd1d4601ec94de76d936f5ace255',1,'Digikam::XbelWriter']]],
  ['xml',['xml',['../classDigikam_1_1SearchXmlWriter.html#a84d0cbbbf4e3812b41b391e6c3d77664',1,'Digikam::SearchXmlWriter::xml()'],['../classDigikam_1_1KeywordSearchWriter.html#a9221a2c9582c82bacf1143e7de1dd1d7',1,'Digikam::KeywordSearchWriter::xml()']]],
  ['xmlelement',['XMLElement',['../classDigikamGenericHtmlGalleryPlugin_1_1XMLElement.html#ac343d8db6de4a1b743d8ea8331e41870',1,'DigikamGenericHtmlGalleryPlugin::XMLElement']]],
  ['xmlsection',['xmlSection',['../classDigikam_1_1DPluginAction.html#a8e0bbb45f7dcf8d88f331c5082fecc7f',1,'Digikam::DPluginAction']]],
  ['xmpsubjects',['XMPSubjects',['../classDigikamGenericMetadataEditPlugin_1_1XMPSubjects.html#a81b35bac52b7b1983d8d2183b924ceaf',1,'DigikamGenericMetadataEditPlugin::XMPSubjects']]],
  ['xmpwidget',['XmpWidget',['../classDigikam_1_1XmpWidget.html#a7af252ec17779fe205b6a621312866b3',1,'Digikam::XmpWidget']]],
  ['xscalecorrect',['xScaleCorrect',['../classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a06cefddfe09d88e6bfc7ee8d6a4905ca',1,'DigikamGenericPresentationPlugin::KBViewTrans']]]
];
