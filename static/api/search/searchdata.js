var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghijklmnopqrstuvwxy",
  2: "dhsy",
  3: "_abcdefghijklmnopqrstuvwxyz~",
  4: "_abcdefghijklmnopqrstuvwxyz",
  5: "abcdefghiklmnopqrstuvxy",
  6: "abcdefghijlmnopqrstuvwx",
  7: "abcdefghijklmnopqrstuvwxy",
  8: "acdfilmoptvw",
  9: "abcdefghiklmopsty",
  10: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "related",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Friends",
  10: "Pages"
};

