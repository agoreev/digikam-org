var searchData=
[
  ['backendmarblelayer',['BackendMarbleLayer',['../classDigikam_1_1BackendMarbleLayer.html',1,'Digikam']]],
  ['balooinfo',['BalooInfo',['../classDigikam_1_1BalooInfo.html',1,'Digikam']]],
  ['balsabinary',['BalsaBinary',['../classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html',1,'DigikamGenericSendByMailPlugin']]],
  ['base_5fcontext',['base_context',['../classbase__context.html',1,'']]],
  ['basicdimgfiltergenerator',['BasicDImgFilterGenerator',['../classDigikam_1_1BasicDImgFilterGenerator.html',1,'Digikam']]],
  ['batchtoolset',['BatchToolSet',['../classDigikam_1_1BatchToolSet.html',1,'Digikam']]],
  ['bcgcontainer',['BCGContainer',['../classDigikam_1_1BCGContainer.html',1,'Digikam']]],
  ['bcgcorrection',['BCGCorrection',['../classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrection.html',1,'DigikamBqmBCGCorrectionPlugin']]],
  ['bcgcorrectionplugin',['BCGCorrectionPlugin',['../classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html',1,'DigikamBqmBCGCorrectionPlugin']]],
  ['bcgtoolplugin',['BCGToolPlugin',['../classDigikamEditorBCGToolPlugin_1_1BCGToolPlugin.html',1,'DigikamEditorBCGToolPlugin']]],
  ['bdenginebackend',['BdEngineBackend',['../classDigikam_1_1BdEngineBackend.html',1,'Digikam']]],
  ['bdenginebackendprivate',['BdEngineBackendPrivate',['../classDigikam_1_1BdEngineBackendPrivate.html',1,'Digikam']]],
  ['bitreader',['bitreader',['../structbitreader.html',1,'bitreader'],['../classheif_1_1BitReader.html',1,'heif::BitReader']]],
  ['bitstreamrange',['BitstreamRange',['../classheif_1_1BitstreamRange.html',1,'heif']]],
  ['blackframelistview',['BlackFrameListView',['../classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListView.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['blackframelistviewitem',['BlackFrameListViewItem',['../classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameListViewItem.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['blackframeparser',['BlackFrameParser',['../classDigikamEditorHotPixelsToolPlugin_1_1BlackFrameParser.html',1,'DigikamEditorHotPixelsToolPlugin']]],
  ['blendkbeffect',['BlendKBEffect',['../classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html',1,'DigikamGenericPresentationPlugin']]],
  ['blur',['Blur',['../classDigikamBqmBlurPlugin_1_1Blur.html',1,'DigikamBqmBlurPlugin']]],
  ['blurfxtoolplugin',['BlurFXToolPlugin',['../classDigikamEditorBlurFxToolPlugin_1_1BlurFXToolPlugin.html',1,'DigikamEditorBlurFxToolPlugin']]],
  ['blurplugin',['BlurPlugin',['../classDigikamBqmBlurPlugin_1_1BlurPlugin.html',1,'DigikamBqmBlurPlugin']]],
  ['blurtoolplugin',['BlurToolPlugin',['../classDigikamEditorBlurToolPlugin_1_1BlurToolPlugin.html',1,'DigikamEditorBlurToolPlugin']]],
  ['border',['Border',['../classDigikamBqmBorderPlugin_1_1Border.html',1,'DigikamBqmBorderPlugin']]],
  ['bordercontainer',['BorderContainer',['../classDigikam_1_1BorderContainer.html',1,'Digikam']]],
  ['borderplugin',['BorderPlugin',['../classDigikamBqmBorderPlugin_1_1BorderPlugin.html',1,'DigikamBqmBorderPlugin']]],
  ['bordertoolplugin',['BorderToolPlugin',['../classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html',1,'DigikamEditorBorderToolPlugin']]],
  ['box',['Box',['../classheif_1_1Box.html',1,'heif']]],
  ['box_5fauxc',['Box_auxC',['../classheif_1_1Box__auxC.html',1,'heif']]],
  ['box_5fclap',['Box_clap',['../classheif_1_1Box__clap.html',1,'heif']]],
  ['box_5fcolr',['Box_colr',['../classheif_1_1Box__colr.html',1,'heif']]],
  ['box_5fdinf',['Box_dinf',['../classheif_1_1Box__dinf.html',1,'heif']]],
  ['box_5fdref',['Box_dref',['../classheif_1_1Box__dref.html',1,'heif']]],
  ['box_5fftyp',['Box_ftyp',['../classheif_1_1Box__ftyp.html',1,'heif']]],
  ['box_5fgrpl',['Box_grpl',['../classheif_1_1Box__grpl.html',1,'heif']]],
  ['box_5fhdlr',['Box_hdlr',['../classheif_1_1Box__hdlr.html',1,'heif']]],
  ['box_5fhvcc',['Box_hvcC',['../classheif_1_1Box__hvcC.html',1,'heif']]],
  ['box_5fidat',['Box_idat',['../classheif_1_1Box__idat.html',1,'heif']]],
  ['box_5fiinf',['Box_iinf',['../classheif_1_1Box__iinf.html',1,'heif']]],
  ['box_5filoc',['Box_iloc',['../classheif_1_1Box__iloc.html',1,'heif']]],
  ['box_5fimir',['Box_imir',['../classheif_1_1Box__imir.html',1,'heif']]],
  ['box_5finfe',['Box_infe',['../classheif_1_1Box__infe.html',1,'heif']]],
  ['box_5fipco',['Box_ipco',['../classheif_1_1Box__ipco.html',1,'heif']]],
  ['box_5fipma',['Box_ipma',['../classheif_1_1Box__ipma.html',1,'heif']]],
  ['box_5fiprp',['Box_iprp',['../classheif_1_1Box__iprp.html',1,'heif']]],
  ['box_5firef',['Box_iref',['../classheif_1_1Box__iref.html',1,'heif']]],
  ['box_5firot',['Box_irot',['../classheif_1_1Box__irot.html',1,'heif']]],
  ['box_5fispe',['Box_ispe',['../classheif_1_1Box__ispe.html',1,'heif']]],
  ['box_5fmeta',['Box_meta',['../classheif_1_1Box__meta.html',1,'heif']]],
  ['box_5fpitm',['Box_pitm',['../classheif_1_1Box__pitm.html',1,'heif']]],
  ['box_5fpixi',['Box_pixi',['../classheif_1_1Box__pixi.html',1,'heif']]],
  ['box_5furl',['Box_url',['../classheif_1_1Box__url.html',1,'heif']]],
  ['boxfolder',['BOXFolder',['../classDigikamGenericBoxPlugin_1_1BOXFolder.html',1,'DigikamGenericBoxPlugin']]],
  ['boxheader',['BoxHeader',['../classheif_1_1BoxHeader.html',1,'heif']]],
  ['boxnewalbumdlg',['BOXNewAlbumDlg',['../classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html',1,'DigikamGenericBoxPlugin']]],
  ['boxphoto',['BOXPhoto',['../classDigikamGenericBoxPlugin_1_1BOXPhoto.html',1,'DigikamGenericBoxPlugin']]],
  ['boxplugin',['BoxPlugin',['../classDigikamGenericBoxPlugin_1_1BoxPlugin.html',1,'DigikamGenericBoxPlugin']]],
  ['boxwidget',['BOXWidget',['../classDigikamGenericBoxPlugin_1_1BOXWidget.html',1,'DigikamGenericBoxPlugin']]],
  ['bracketstackitem',['BracketStackItem',['../classDigikamGenericExpoBlendingPlugin_1_1BracketStackItem.html',1,'DigikamGenericExpoBlendingPlugin']]],
  ['bracketstacklist',['BracketStackList',['../classDigikamGenericExpoBlendingPlugin_1_1BracketStackList.html',1,'DigikamGenericExpoBlendingPlugin']]],
  ['breadthfirstsearchvisitor',['BreadthFirstSearchVisitor',['../classDigikam_1_1Graph_1_1GraphSearch_1_1BreadthFirstSearchVisitor.html',1,'Digikam::Graph::GraphSearch']]],
  ['busywaiter',['BusyWaiter',['../classDigikam_1_1BdEngineBackendPrivate_1_1BusyWaiter.html',1,'Digikam::BdEngineBackendPrivate']]],
  ['bwconvert',['BWConvert',['../classDigikamBqmBWConvertPlugin_1_1BWConvert.html',1,'DigikamBqmBWConvertPlugin']]],
  ['bwconvertplugin',['BWConvertPlugin',['../classDigikamBqmBWConvertPlugin_1_1BWConvertPlugin.html',1,'DigikamBqmBWConvertPlugin']]],
  ['bwsepiacontainer',['BWSepiaContainer',['../classDigikam_1_1BWSepiaContainer.html',1,'Digikam']]],
  ['bwsepiatoolplugin',['BWSepiaToolPlugin',['../classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html',1,'DigikamEditorBWSepiaToolPlugin']]]
];
