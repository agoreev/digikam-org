var searchData=
[
  ['databaseservererrorenum',['DatabaseServerErrorEnum',['../classDigikam_1_1DatabaseServerError.html#a6de91fbbed7a6263a1df259a830dad2e',1,'Digikam::DatabaseServerError']]],
  ['datesource',['DateSource',['../classDigikam_1_1DateOptionDialog.html#aa85f65ffddeeb08f990643c5684bada1',1,'Digikam::DateOptionDialog']]],
  ['dbtype',['DbType',['../classDigikam_1_1BdEngineBackend.html#ac713601fae10c153d5ed2be96bc16e0e',1,'Digikam::BdEngineBackend']]],
  ['dcolorchoosermode',['DColorChooserMode',['../namespaceDigikam.html#a94ef3fb31dc08962a53c4040fb71bbfb',1,'Digikam']]],
  ['decodingquality',['DecodingQuality',['../classDigikam_1_1DRawDecoderSettings.html#a6cdd9d3cf382d21b731ebeca49735247',1,'Digikam::DRawDecoderSettings']]],
  ['decorationrole',['DecorationRole',['../classDigikam_1_1SchemeManager.html#a3c3c966c839bec512fb2ab5fff692af4',1,'Digikam::SchemeManager']]],
  ['deletemode',['DeleteMode',['../classDigikam_1_1ItemViewUtilities.html#a92b403afe0847bef751dd704568d2dbc',1,'Digikam::ItemViewUtilities::DeleteMode()'],['../namespaceDigikam_1_1DeleteDialogMode.html#a228226fa80a476233c29ca3208c80908',1,'Digikam::DeleteDialogMode::DeleteMode()']]],
  ['detectornnmodel',['DetectorNNModel',['../namespaceDigikam.html#accf958199f1ea5c8a6ce527fb0c07a0c',1,'Digikam']]],
  ['direction',['Direction',['../classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer.html#a3da67f75f09649a014cc793bf4e1517f',1,'DigikamEditorHotPixelsToolPlugin::HotPixelFixer']]],
  ['dngbayerpattern',['DNGBayerPattern',['../classDigikam_1_1DNGWriter.html#a283a40e83b65f7ccf87506c7cda36cd1',1,'Digikam::DNGWriter']]],
  ['downloadstatus',['DownloadStatus',['../classDigikam_1_1CamItemInfo.html#afd6eb29d93fd23a1bcf50d322e1f7430',1,'Digikam::CamItemInfo']]],
  ['dropaction',['DropAction',['../namespaceDigikam.html#a8e3ba4bc8e51fa8b259781ce8853b4a1',1,'Digikam']]],
  ['duplicatessearchrestrictions',['DuplicatesSearchRestrictions',['../classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59',1,'Digikam::HaarIface']]]
];
