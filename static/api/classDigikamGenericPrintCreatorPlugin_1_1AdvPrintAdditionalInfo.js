var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo =
[
    [ "AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#ad00937739ea80d1883d771f97b50473b", null ],
    [ "AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a5de26b4fda95b1e9311c57b689d0c459", null ],
    [ "~AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a99122b542b23857be032bf7ba714a8a0", null ],
    [ "m_autoRotate", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a3160af5899e88848da9dba0a9c867199", null ],
    [ "m_enlargeSmallerImages", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#afa5d547cb65e37b41747171d7eba24bf", null ],
    [ "m_keepRatio", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a855768fafae0b8f007d916c2e1d648df", null ],
    [ "m_printHeight", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#ab28629b26852e545609e66237929d572", null ],
    [ "m_printPosition", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#aecbdf08f66d70de54a9e56ad0a1b7369", null ],
    [ "m_printWidth", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a7826b1d9d9413d37a8b73a65d7c8b7cd", null ],
    [ "m_scaleMode", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a3b95ec6e212e2b944b35e55536def93b", null ],
    [ "m_unit", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html#a479a434bf8691a2000aec677bbc0bb0a", null ]
];