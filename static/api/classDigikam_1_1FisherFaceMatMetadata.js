var classDigikam_1_1FisherFaceMatMetadata =
[
    [ "StorageStatus", "classDigikam_1_1FisherFaceMatMetadata.html#afb25cbc9ac3966be2f3033bcf2abd96f", [
      [ "Created", "classDigikam_1_1FisherFaceMatMetadata.html#afb25cbc9ac3966be2f3033bcf2abd96fa0ac602a0c28ab31a87e5fe3ddf7c7f29", null ],
      [ "InDatabase", "classDigikam_1_1FisherFaceMatMetadata.html#afb25cbc9ac3966be2f3033bcf2abd96faf8d04198519a76045fd38fa1b6d37bb1", null ]
    ] ],
    [ "FisherFaceMatMetadata", "classDigikam_1_1FisherFaceMatMetadata.html#a4a60d339d2534e3756fe76e4d72f4f68", null ],
    [ "~FisherFaceMatMetadata", "classDigikam_1_1FisherFaceMatMetadata.html#a0c555a1abaa852cdd77f109fafcf6d4f", null ],
    [ "context", "classDigikam_1_1FisherFaceMatMetadata.html#a7f49ae9ca7e28d9fcd8705ec8f13d1b6", null ],
    [ "databaseId", "classDigikam_1_1FisherFaceMatMetadata.html#aa24678332d92273f368da70d19ee4aa2", null ],
    [ "identity", "classDigikam_1_1FisherFaceMatMetadata.html#a05093afcf7574fe3592c13b854aff4cb", null ],
    [ "storageStatus", "classDigikam_1_1FisherFaceMatMetadata.html#ac801ef97a070c28eb3dcebb1695d6d99", null ]
];