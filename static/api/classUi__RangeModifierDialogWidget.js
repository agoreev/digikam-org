var classUi__RangeModifierDialogWidget =
[
    [ "retranslateUi", "classUi__RangeModifierDialogWidget.html#a44f7334416849f7b74f7bd0106205fb7", null ],
    [ "setupUi", "classUi__RangeModifierDialogWidget.html#adafc6d156b36e1d6253461cf047644ad", null ],
    [ "gridLayout", "classUi__RangeModifierDialogWidget.html#a11473c5ff8426c4b02ff68adb6f67c8f", null ],
    [ "label", "classUi__RangeModifierDialogWidget.html#a4c4c81d360de8db4bedbe950ea561a73", null ],
    [ "label_2", "classUi__RangeModifierDialogWidget.html#ac861e058fc1f3f152a10a10f61dad441", null ],
    [ "startInput", "classUi__RangeModifierDialogWidget.html#add97b8d5381b7c4a1e9c287ce5138078", null ],
    [ "stopInput", "classUi__RangeModifierDialogWidget.html#adf4d00181ca56a2d09c8b2bc331faaca", null ],
    [ "toTheEndCheckBox", "classUi__RangeModifierDialogWidget.html#ac4a4821c83b3135c591432ac18ffa008", null ],
    [ "verticalLayout", "classUi__RangeModifierDialogWidget.html#a6d864d3b062aa8be032c5b16155177b6", null ],
    [ "verticalSpacer", "classUi__RangeModifierDialogWidget.html#a8a2c7ba69c09fa0489f39767f5e1d0ca", null ]
];