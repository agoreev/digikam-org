var structDigikam_1_1PTOType_1_1Optimization =
[
    [ "Parameter", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62ed", [
      [ "LENSA", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda2e062e09d7bce441b578cb599353a0bb", null ],
      [ "LENSB", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda0dc40df676e014ba15cc80818ec57dfe", null ],
      [ "LENSC", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edac243438801402567088dcb59d4aa6612", null ],
      [ "LENSD", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda6066203c8b3cc844a814b99a4ad38230", null ],
      [ "LENSE", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edaf316436a5202c243ac0b1cf0ee2e9acd", null ],
      [ "LENSHFOV", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edad9463d2b2439bad674a4628f8d8750ba", null ],
      [ "LENSYAW", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edadbe1394c6d96db41ac1c5545074cf474", null ],
      [ "LENSPITCH", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edad1e37a1fb84bb045837387bc79ebfac1", null ],
      [ "LENSROLL", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edaf65b7dac00b2df0faa85b993da376648", null ],
      [ "EXPOSURE", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda3f68375fd5439a1eda0b5bf9c65f4b51", null ],
      [ "WBR", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda5a254f192cd7c37413217bf04139dfea", null ],
      [ "WBB", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edac18088e6df2190508a4bd9ebab18fa64", null ],
      [ "VA", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edad92f779b08f073ca815eb8093e6c2987", null ],
      [ "VB", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda7a864d572f0869853497c0060615cfca", null ],
      [ "VC", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda4e1b214f30973334e181bcd3d42c274f", null ],
      [ "VD", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edae7b1d22e4da92aaea7983fcb80a337d3", null ],
      [ "VX", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda7b5941ce527c2a661c145e0ef45e5c71", null ],
      [ "VY", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edab58e3ff836958155c5b595ff84dad4d7", null ],
      [ "RA", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edae9772907ba466206c129e79758db36a2", null ],
      [ "RB", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edac29519755d3473510b592aa4db812d8c", null ],
      [ "RC", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edaf28d01b7ea44a59f56ea6ba63a5c4379", null ],
      [ "RD", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edacc0ff1fbe65c2097e75e84ad8a5aca9e", null ],
      [ "RE", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda4aea220e595167007ee326b007781843", null ],
      [ "UNKNOWN", "structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda66dcaf1fe99d0636cb54cbcb18885bc5", null ]
    ] ],
    [ "parameter", "structDigikam_1_1PTOType_1_1Optimization.html#abd41de0cd0d3080b6755ac5acbba3723", null ],
    [ "previousComments", "structDigikam_1_1PTOType_1_1Optimization.html#afd13b6a370c8fd6fbe1adcb94f101700", null ]
];