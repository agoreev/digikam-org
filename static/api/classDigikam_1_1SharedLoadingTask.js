var classDigikam_1_1SharedLoadingTask =
[
    [ "LoadingTaskStatus", "classDigikam_1_1SharedLoadingTask.html#aa4131ed076cc79578e21922f88d08255", [
      [ "LoadingTaskStatusLoading", "classDigikam_1_1SharedLoadingTask.html#aa4131ed076cc79578e21922f88d08255a9fb917335571fcf5a3c92c4929800b17", null ],
      [ "LoadingTaskStatusPreloading", "classDigikam_1_1SharedLoadingTask.html#aa4131ed076cc79578e21922f88d08255a28d9aec69c9254aabc1a0494204b64d4", null ],
      [ "LoadingTaskStatusStopping", "classDigikam_1_1SharedLoadingTask.html#aa4131ed076cc79578e21922f88d08255ad8070c2c8f54d077b89a3b211c964b82", null ]
    ] ],
    [ "TaskType", "classDigikam_1_1SharedLoadingTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1SharedLoadingTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1SharedLoadingTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html#a3964a56093ad51c16a99ca9df7eb1211", null ],
    [ "accessMode", "classDigikam_1_1SharedLoadingTask.html#a85c4a4b6090449bbb12c5bf69f26231d", null ],
    [ "addListener", "classDigikam_1_1SharedLoadingTask.html#abb5fd83653837f547d8c1b88c2b7c18b", null ],
    [ "cacheKey", "classDigikam_1_1SharedLoadingTask.html#a1067dfc3fa8804b03eeb3d1e3ba9bac2", null ],
    [ "completed", "classDigikam_1_1SharedLoadingTask.html#aab624e9a16511a2c6fafafb4b1e1e6db", null ],
    [ "continueQuery", "classDigikam_1_1SharedLoadingTask.html#afcd071904ba13fc59003835459c8eb57", null ],
    [ "execute", "classDigikam_1_1SharedLoadingTask.html#a2ee6a0e72e81c2f97c7bd921d9ec4099", null ],
    [ "filePath", "classDigikam_1_1SharedLoadingTask.html#a5ad4241f26475fb4c16dd1a86aacd533", null ],
    [ "granularity", "classDigikam_1_1SharedLoadingTask.html#ab976f06eb18822b0da8e481fd2ff55d0", null ],
    [ "img", "classDigikam_1_1SharedLoadingTask.html#aea64c5d8b4dbad0e0287ca23370be2b9", null ],
    [ "loadingDescription", "classDigikam_1_1SharedLoadingTask.html#a64266025e0d2fd28711fcd889e91fefb", null ],
    [ "loadSaveNotifier", "classDigikam_1_1SharedLoadingTask.html#a1013b97be358eb54782a8bcf893a6f45", null ],
    [ "needsPostProcessing", "classDigikam_1_1SharedLoadingTask.html#ab1fc0033e7b7d446e9e8f365c23c8c34", null ],
    [ "notifyNewLoadingProcess", "classDigikam_1_1SharedLoadingTask.html#ae66da407c26ab9618675825c30532bf6", null ],
    [ "postProcess", "classDigikam_1_1SharedLoadingTask.html#ad97c67c25d44b7138fc0f9c67fa0dff0", null ],
    [ "progressInfo", "classDigikam_1_1SharedLoadingTask.html#a8aed6d98796046314e14bacb04526868", null ],
    [ "querySendNotifyEvent", "classDigikam_1_1SharedLoadingTask.html#a2c55410c0b046f4b5e9d81e516bc303a", null ],
    [ "removeListener", "classDigikam_1_1SharedLoadingTask.html#a2b5af90f37dcd5ccec5f2d5fe6b58003", null ],
    [ "setResult", "classDigikam_1_1SharedLoadingTask.html#ad94b16dd3cae56ef7973a78d3dfa6997", null ],
    [ "setStatus", "classDigikam_1_1SharedLoadingTask.html#a5687fd89bd332215f8d056adcec1704c", null ],
    [ "status", "classDigikam_1_1SharedLoadingTask.html#a85855649f66c9143d20860cdb80f9e0d", null ],
    [ "type", "classDigikam_1_1SharedLoadingTask.html#a5fee5b84b321dfd50f3a054078ea4cd4", null ],
    [ "m_accessMode", "classDigikam_1_1SharedLoadingTask.html#aae251f49ca1b13d688c9c19957bbc809", null ],
    [ "m_completed", "classDigikam_1_1SharedLoadingTask.html#ae3825bd82ce4ceeb2e8604cfc7521389", null ],
    [ "m_img", "classDigikam_1_1SharedLoadingTask.html#a6a634b1b30dc2d7095b8c583e19f8fb3", null ],
    [ "m_listeners", "classDigikam_1_1SharedLoadingTask.html#ab62925a9c1419e3518ae1ccf42c2f37e", null ],
    [ "m_loadingDescription", "classDigikam_1_1SharedLoadingTask.html#ab16d4a9ce3d15d4148a711218bcecf4e", null ],
    [ "m_loadingTaskStatus", "classDigikam_1_1SharedLoadingTask.html#aac204d0b11985a2f5c04ab0087c5fe66", null ],
    [ "m_resultLoadingDescription", "classDigikam_1_1SharedLoadingTask.html#af983635cf4e998a15abbe29bd70f1dc2", null ],
    [ "m_thread", "classDigikam_1_1SharedLoadingTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ],
    [ "m_usedProcess", "classDigikam_1_1SharedLoadingTask.html#a5c927ae4cd3c12272bedcaaca688cd05", null ]
];