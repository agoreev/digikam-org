var namespaceDigikamGenericFaceBookPlugin =
[
    [ "FbAlbum", "classDigikamGenericFaceBookPlugin_1_1FbAlbum.html", "classDigikamGenericFaceBookPlugin_1_1FbAlbum" ],
    [ "FbMPForm", "classDigikamGenericFaceBookPlugin_1_1FbMPForm.html", "classDigikamGenericFaceBookPlugin_1_1FbMPForm" ],
    [ "FbNewAlbumDlg", "classDigikamGenericFaceBookPlugin_1_1FbNewAlbumDlg.html", "classDigikamGenericFaceBookPlugin_1_1FbNewAlbumDlg" ],
    [ "FbPhoto", "classDigikamGenericFaceBookPlugin_1_1FbPhoto.html", "classDigikamGenericFaceBookPlugin_1_1FbPhoto" ],
    [ "FbPlugin", "classDigikamGenericFaceBookPlugin_1_1FbPlugin.html", "classDigikamGenericFaceBookPlugin_1_1FbPlugin" ],
    [ "FbUser", "classDigikamGenericFaceBookPlugin_1_1FbUser.html", "classDigikamGenericFaceBookPlugin_1_1FbUser" ],
    [ "FbWidget", "classDigikamGenericFaceBookPlugin_1_1FbWidget.html", "classDigikamGenericFaceBookPlugin_1_1FbWidget" ]
];