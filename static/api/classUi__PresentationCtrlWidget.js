var classUi__PresentationCtrlWidget =
[
    [ "retranslateUi", "classUi__PresentationCtrlWidget.html#a5da922e37f293f754d44631f51e2a7b0", null ],
    [ "setupUi", "classUi__PresentationCtrlWidget.html#a8b7262c5eaa847888627d369e9b4a1a8", null ],
    [ "gridLayout", "classUi__PresentationCtrlWidget.html#aa5a484497aad46da0dc763a07efe7091", null ],
    [ "horizontalLayout", "classUi__PresentationCtrlWidget.html#addeee656316ed54c91a6ae0a10cf2680", null ],
    [ "m_nextButton", "classUi__PresentationCtrlWidget.html#a0ea4b574dfe3e54f7c7a48603efa33cb", null ],
    [ "m_playButton", "classUi__PresentationCtrlWidget.html#a2441f5ab02b06aa5382e7ab7ed0612f0", null ],
    [ "m_prevButton", "classUi__PresentationCtrlWidget.html#a5652df29f248890b8d29817c33c231d0", null ],
    [ "m_slideLabel", "classUi__PresentationCtrlWidget.html#a0db1266a46282a24830bd21a33f0c8a5", null ],
    [ "m_stopButton", "classUi__PresentationCtrlWidget.html#a1a78d306a556777d72c9b0c84abefe1a", null ]
];