var classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow =
[
    [ "DSWindow", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a9bd6cca3ae441071c31212a3f45fe3ad", null ],
    [ "~DSWindow", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#aeeee4757d5dbc937ed5b1bd1cf630343", null ],
    [ "addButton", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "Private", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a361b916e5225d3859929b6abf63b1654", null ],
    [ "reactivate", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#ae88cc18ac3cdba27e9e3f881d7c51480", null ],
    [ "restoreDialogSize", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#adf19527fded25cf8b1483f67b53b5e6a", null ],
    [ "m_buttons", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ],
    [ "mainWidget", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a6a437054eb59f11757074e21eac8ee24", null ],
    [ "propagateReject", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a340da613fae80e856cdb428ea06ba20e", null ],
    [ "startButton", "classDigikamGenericDebianScreenshotsPlugin_1_1DSWindow.html#a48cfb1f3cf04813e1f78f47cff038a45", null ]
];