var classDigikam_1_1ItemScanInfo =
[
    [ "ItemScanInfo", "classDigikam_1_1ItemScanInfo.html#a73a4da70fa376d73892b75eaeef9a8e1", null ],
    [ "isNull", "classDigikam_1_1ItemScanInfo.html#ae35dc42d11758cb33afdddb8988128df", null ],
    [ "albumID", "classDigikam_1_1ItemScanInfo.html#a5a4cc6c2e0c4c19afe245df28add7649", null ],
    [ "category", "classDigikam_1_1ItemScanInfo.html#af04ee8d71f2b6eea9674613f1f15ddae", null ],
    [ "fileSize", "classDigikam_1_1ItemScanInfo.html#a3e9200dbdc0f7f1008852317352d7794", null ],
    [ "id", "classDigikam_1_1ItemScanInfo.html#a37a1562709707653879a0d16de4a0ba3", null ],
    [ "itemName", "classDigikam_1_1ItemScanInfo.html#afb2e81db3ab5f81a71d01c3d9396a031", null ],
    [ "modificationDate", "classDigikam_1_1ItemScanInfo.html#a7ae0731abae4291af88928ca86d3623e", null ],
    [ "status", "classDigikam_1_1ItemScanInfo.html#ac066d6c569bb065ab5a5a096b417999c", null ],
    [ "uniqueHash", "classDigikam_1_1ItemScanInfo.html#aa8e5738bb86a3db3a30488edac504326", null ]
];