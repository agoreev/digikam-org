var classDigikam_1_1SchemeManager =
[
    [ "BackgroundRole", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150f", [
      [ "NormalBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa0b58a71b704e97a8789572796603b000", null ],
      [ "AlternateBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fac67408285491239db739609171f171f8", null ],
      [ "ActiveBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa04de1f5cdaadf3acdfa8048eb7e16c9b", null ],
      [ "LinkBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa7acd9442691e9579a54d9e6eb00fb1ce", null ],
      [ "VisitedBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fafc44f6deaedbd4950f821bca92070247", null ],
      [ "NegativeBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa222b9f024cc8286e2d57e1a5dfe2dc23", null ],
      [ "NeutralBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa7854928287df0d1b1edede63404cff0d", null ],
      [ "PositiveBackground", "classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa3eb101f0f3d3acb0c157b9d4677e102e", null ]
    ] ],
    [ "ColorSet", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19b", [
      [ "View", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19ba3e307932ba892f8c1386527927e40644", null ],
      [ "Window", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19bacb92cdeba33da5703d5445bbd794d5e5", null ],
      [ "Button", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19ba3f578a2077aefde236e6ff84b6d66e36", null ],
      [ "Selection", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19baeaa99ebab3b89517071058b23061510e", null ],
      [ "Tooltip", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19ba96f4b843a85eaf6253776c9ec1e825cb", null ],
      [ "Complementary", "classDigikam_1_1SchemeManager.html#a00c9c652c25e4164c76685b75573f19ba10bbc0e53b1343b594a0a7f572e7537a", null ]
    ] ],
    [ "DecorationRole", "classDigikam_1_1SchemeManager.html#a3c3c966c839bec512fb2ab5fff692af4", [
      [ "FocusColor", "classDigikam_1_1SchemeManager.html#a3c3c966c839bec512fb2ab5fff692af4a811f95afab51b1d0a9e0355c448314b2", null ],
      [ "HoverColor", "classDigikam_1_1SchemeManager.html#a3c3c966c839bec512fb2ab5fff692af4a2822a9bac1f9aaa577a08564e0ff5313", null ]
    ] ],
    [ "ForegroundRole", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415c", [
      [ "NormalText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415cafbdb757ae6fc0705c2785ce544e48fc5", null ],
      [ "InactiveText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415cab87d6c8de597213b953d97f9f3d35309", null ],
      [ "ActiveText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415ca96fd8ea56229982dfbe713012a717b52", null ],
      [ "LinkText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415caccaf121b6600edaaaade58ff80cf7962", null ],
      [ "VisitedText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415ca9aa6d91ec8a739948d8670824338b4af", null ],
      [ "NegativeText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415ca1ecfdce2ea92dfe51870b57a487dc368", null ],
      [ "NeutralText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415cac97a62e407a711cbcacb729170ed95f8", null ],
      [ "PositiveText", "classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415ca10499d70c3f33101a25ba25257093185", null ]
    ] ],
    [ "ShadeRole", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57", [
      [ "LightShade", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57a8320256333cd376e0ef7a9d248978e32", null ],
      [ "MidlightShade", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57a165f0aaa784e6edb3abf11fa5ace129e", null ],
      [ "MidShade", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57a071af097e0e6e1242fbd5dd4478d53b3", null ],
      [ "DarkShade", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57ab9c13daf2eded24ca35196fc75d68084", null ],
      [ "ShadowShade", "classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57ab142519569dda6a1032fe1c4a56c40be", null ]
    ] ],
    [ "SchemeManager", "classDigikam_1_1SchemeManager.html#a8c73d211c0650173aae62c3d69016a88", null ],
    [ "~SchemeManager", "classDigikam_1_1SchemeManager.html#a5bd47f2c5c629f8043dfb15ceea4e74c", null ],
    [ "SchemeManager", "classDigikam_1_1SchemeManager.html#aef4b2f89eaed6ce16934d15337d38deb", null ],
    [ "background", "classDigikam_1_1SchemeManager.html#aa24703700f8e24971646493bd2eedeed", null ],
    [ "decoration", "classDigikam_1_1SchemeManager.html#a335260650d13ca93ce843322c0a2512e", null ],
    [ "foreground", "classDigikam_1_1SchemeManager.html#a6382dd238a081bb05c1bd317c22dc47b", null ],
    [ "operator=", "classDigikam_1_1SchemeManager.html#aff897e6a466bde1f50bc39124ec87fdf", null ],
    [ "shade", "classDigikam_1_1SchemeManager.html#afeb1a2b66c9bb0d438c5c6d7332d8456", null ]
];