var classDigikam_1_1DNNFaceVecMetadata =
[
    [ "StorageStatus", "classDigikam_1_1DNNFaceVecMetadata.html#a831833848870a37dcd9f28c260a9ee2d", [
      [ "Created", "classDigikam_1_1DNNFaceVecMetadata.html#a831833848870a37dcd9f28c260a9ee2da17b3414d298933f96572cff35584c3f3", null ],
      [ "InDatabase", "classDigikam_1_1DNNFaceVecMetadata.html#a831833848870a37dcd9f28c260a9ee2daf35cb03437a2879afd2eb4e83f76be3a", null ]
    ] ],
    [ "DNNFaceVecMetadata", "classDigikam_1_1DNNFaceVecMetadata.html#a744cf54153f387049d5ad102d70a3ae4", null ],
    [ "~DNNFaceVecMetadata", "classDigikam_1_1DNNFaceVecMetadata.html#ade9f5db88cb8037bc6f80854383d6816", null ],
    [ "context", "classDigikam_1_1DNNFaceVecMetadata.html#a84cbe3107df2bef5a7629602281ba84c", null ],
    [ "databaseId", "classDigikam_1_1DNNFaceVecMetadata.html#a600566d312736cb9ed46df8646fc98c9", null ],
    [ "identity", "classDigikam_1_1DNNFaceVecMetadata.html#ae39096f24a13998cc76df9fab75f9c6d", null ],
    [ "storageStatus", "classDigikam_1_1DNNFaceVecMetadata.html#a0533ff237ae3ac44403a1e2f1c824359", null ]
];