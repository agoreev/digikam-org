var classDigikam_1_1EditorCore_1_1FileToSave =
[
    [ "fileName", "classDigikam_1_1EditorCore_1_1FileToSave.html#a37ed5e1d336d1ab0f6b1b1d317f21186", null ],
    [ "filePath", "classDigikam_1_1EditorCore_1_1FileToSave.html#a314fb138fb3f3386fd6c70d9825a106c", null ],
    [ "historyStep", "classDigikam_1_1EditorCore_1_1FileToSave.html#aede04213931eb20cb47dfc502dd638ed", null ],
    [ "image", "classDigikam_1_1EditorCore_1_1FileToSave.html#a9bf91459e6d1bf05679b06c875612548", null ],
    [ "intendedFilePath", "classDigikam_1_1EditorCore_1_1FileToSave.html#a8ec7bda0a06f9c76459455b984931015", null ],
    [ "ioAttributes", "classDigikam_1_1EditorCore_1_1FileToSave.html#ac1e4030a576d54593dc1e08de2a1d37b", null ],
    [ "mimeType", "classDigikam_1_1EditorCore_1_1FileToSave.html#a641366b84db8e41084f6fbe4a480e2c8", null ],
    [ "setExifOrientationTag", "classDigikam_1_1EditorCore_1_1FileToSave.html#a1322f298a3a802cffbbb3e30f16c495c", null ]
];