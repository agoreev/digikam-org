var classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin =
[
    [ "CBToolPlugin", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a4e3fc04cabcb7258c21a1158573283a9", null ],
    [ "~CBToolPlugin", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#aac91e58cd6605e343a3d76409b7a3d8b", null ],
    [ "actions", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a1ba16dc7198aa3284cd933d89f3b00c6", null ],
    [ "addAction", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a07e25f4a176a823feb70e6dd80e6b3f7", null ],
    [ "authors", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a7543cf813b3d575c9fd9590b386839b7", null ],
    [ "categories", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a35d45cfa520c713de9348130b0a4d26c", null ],
    [ "cleanUp", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a8199ffdd14c09124360cb4b2ba08e1f9", null ],
    [ "description", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#ab14f735d00d984093083ca8fdcbef3d3", null ],
    [ "details", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#afff8f483f380f1ac3a6358ede6c729a0", null ],
    [ "extraAboutData", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findActionByName", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a7c27ed94fba2b6e4e1eeb63aca55096a", null ],
    [ "hasVisibilityProperty", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a996af3ec25ecb4c44788435b8ca5d079", null ],
    [ "icon", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a5b6a9054659e8522339dc30fdbfbff7d", null ],
    [ "ifaceIid", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a2a64b992e50d15b4c67f6112b6bcaef8", null ],
    [ "iid", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a7323ff139ce0f35ebda2198a421f09e8", null ],
    [ "infoIface", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a7aa1f0e7d5365c77f9dd44b7b0dd42da", null ],
    [ "libraryFileName", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#ab5d681dc68e4c28c790f9138e0435084", null ],
    [ "pluginAuthors", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#ad49433b667637be45f65b4c94da42eb3", null ],
    [ "setLibraryFileName", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a3f65fb319fd725295941b75366d43c5b", null ],
    [ "setVisible", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a8140de1f7aa50c20fd137921f0ad63a8", null ],
    [ "shouldLoaded", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "version", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "actions", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a3a5ca08740cf279b1f73520bc83d1b41", null ],
    [ "libraryFileName", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ]
];