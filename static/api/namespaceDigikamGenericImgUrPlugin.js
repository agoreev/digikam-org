var namespaceDigikamGenericImgUrPlugin =
[
    [ "ImgurImageListViewItem", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem.html", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem" ],
    [ "ImgurImagesList", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList" ],
    [ "ImgUrPlugin", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin.html", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin" ],
    [ "ImgurTalkerAction", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction" ],
    [ "ImgurTalkerResult", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult" ]
];