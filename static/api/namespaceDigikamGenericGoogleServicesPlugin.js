var namespaceDigikamGenericGoogleServicesPlugin =
[
    [ "GDMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm.html", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm" ],
    [ "GPMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm.html", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm" ],
    [ "GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder" ],
    [ "GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto" ],
    [ "GSPlugin", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin.html", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin" ],
    [ "GSWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget" ]
];