var classDigikamGenericFlickrPlugin_1_1GPhoto =
[
    [ "GPhoto", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#aad2898f7f429328ec9298c49a9428815", null ],
    [ "description", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#a17e44e344df4986724266ed79623364b", null ],
    [ "is_family", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#a367583d7850bfd08f3fc92a18f1015a8", null ],
    [ "is_private", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#a44076c93860e5d4236ea8672efcd0b0a", null ],
    [ "is_public", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#ab8f4974158cc957e680c028542dc4acd", null ],
    [ "ref_num", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#ad1d7ed7e31a4404b475e63c96702b9cd", null ],
    [ "tags", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#a130b6d703192b8e365f4cedf46fb6611", null ],
    [ "title", "classDigikamGenericFlickrPlugin_1_1GPhoto.html#a3ac29b35dae5487d0fdadc7b98d51324", null ]
];