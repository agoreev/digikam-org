var classDigikam_1_1FaceItemRetriever =
[
    [ "FaceItemRetriever", "classDigikam_1_1FaceItemRetriever.html#a9ade9b9718c646c09e1d3e6e77e0454b", null ],
    [ "cancel", "classDigikam_1_1FaceItemRetriever.html#a3b523e9ddb6f7b1edf7aaf27aab3cf66", null ],
    [ "getDetails", "classDigikam_1_1FaceItemRetriever.html#aef2191bf3dd24a94149348d3daa73602", null ],
    [ "getDetails", "classDigikam_1_1FaceItemRetriever.html#a2684cf016e0e357b75f4cef983612105", null ],
    [ "getThumbnails", "classDigikam_1_1FaceItemRetriever.html#af1a15d5b25ef2bc07cb10f45f34ce968", null ],
    [ "thumbnailCatcher", "classDigikam_1_1FaceItemRetriever.html#aa761ebb1def72a586da4278711c67fcc", null ],
    [ "catcher", "classDigikam_1_1FaceItemRetriever.html#a1a8705593c2e13cde8b65ff3af00b4c4", null ]
];