var structheif__color__profile__nclx =
[
    [ "color_primaries", "structheif__color__profile__nclx.html#ab4a9370d7a9d76ae6f9a977f794daa55", null ],
    [ "color_primary_blue_x", "structheif__color__profile__nclx.html#a78cbb8491617b6b46c67e07f0cb0a288", null ],
    [ "color_primary_blue_y", "structheif__color__profile__nclx.html#aef67f9a4c4ec4b3124bbc7ad12c4b8df", null ],
    [ "color_primary_green_x", "structheif__color__profile__nclx.html#a0fa6d4388fd7d9010ba5334655a9b05a", null ],
    [ "color_primary_green_y", "structheif__color__profile__nclx.html#ac467ec6cd7c4f3b2e354b421f7063cfa", null ],
    [ "color_primary_red_x", "structheif__color__profile__nclx.html#a9094c7ea87b20aebdac15b68cae74731", null ],
    [ "color_primary_red_y", "structheif__color__profile__nclx.html#a9b5852b47c941c8fe67ad58245339ea5", null ],
    [ "color_primary_white_x", "structheif__color__profile__nclx.html#ab1a836cd3da433d8e45afdcc307169c4", null ],
    [ "color_primary_white_y", "structheif__color__profile__nclx.html#a8f41554c521bea69decb97ce5b0c547a", null ],
    [ "full_range_flag", "structheif__color__profile__nclx.html#af34d4b07ffeb5503ba0c69b6cc86262e", null ],
    [ "matrix_coefficients", "structheif__color__profile__nclx.html#a3f291960210c8d2017a4cd01bc687108", null ],
    [ "transfer_characteristics", "structheif__color__profile__nclx.html#a06ea6e3f19ea789fbfbe8c786bec440f", null ],
    [ "version", "structheif__color__profile__nclx.html#ad4e223707df4d8def3f0ca3289cb6385", null ]
];