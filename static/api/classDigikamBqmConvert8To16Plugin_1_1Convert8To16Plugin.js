var classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin =
[
    [ "Convert8To16Plugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#aebb937c014590747d26b38a5ddc4f420", null ],
    [ "~Convert8To16Plugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a4621698c349dc4b38f3753b4655061da", null ],
    [ "addTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#aada8336ba5b96006673a89244ab7f4fc", null ],
    [ "authors", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#ad21f81085d05deca85df7181c094265f", null ],
    [ "categories", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#ace50667ec58d94b184bb064cf897c031", null ],
    [ "cleanUp", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#aafc9ca722aabb57dfbfc3845d230adad", null ],
    [ "description", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a65b55037b7c098104101890d067c4aa9", null ],
    [ "details", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#af9d65daa3afcc9ac4aa1773abef7514d", null ],
    [ "extraAboutData", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findToolByName", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a2cc55f4c57b24b392c510fda76224b57", null ],
    [ "hasVisibilityProperty", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a5428e743adbd7d4c5d4258af04ad591e", null ],
    [ "icon", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#ac986ea0a209b3c72feab33cf9131365a", null ],
    [ "ifaceIid", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a333f538f23a6d798bf35ddac6f8b33e8", null ],
    [ "iid", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a2a714546cafe468169b5f86ae0ebd2ef", null ],
    [ "libraryFileName", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a0adb7d7de2c91d65ea79de1fb9aedc28", null ],
    [ "pluginAuthors", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "Private", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a285e718c1b0fde7f076bf5a52b93cfd7", null ],
    [ "setLibraryFileName", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a59f4ded8b1b873457ab54de248ec6f02", null ],
    [ "setVisible", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a634d1026d4b7abcb1d131c34cfa4aca3", null ],
    [ "shouldLoaded", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "signalVisible", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a6be70fd2b36cbd87e78741c349eb30d8", null ],
    [ "tools", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#aeb5a12dd500c1106b4f1f7b7e98fa837", null ],
    [ "version", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a71a6f035204fb005960edf5d285884a9", null ],
    [ "libraryFileName", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#ae849627de7d1f1c556804881b921d3b9", null ],
    [ "shouldLoaded", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a2acc10db740d60d639b0963d8a73d1d1", null ],
    [ "tools", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#a08ecb65fb679e3490f60dc9269588673", null ]
];