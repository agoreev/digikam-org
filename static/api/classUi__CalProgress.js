var classUi__CalProgress =
[
    [ "retranslateUi", "classUi__CalProgress.html#adad9de883458dff3b167db55d8c7fade", null ],
    [ "setupUi", "classUi__CalProgress.html#a63dc97cbbad9c6b5503d9af3869ecfda", null ],
    [ "currentLabel", "classUi__CalProgress.html#ac4f2a3a58b2fa5136381aa3bb695d22b", null ],
    [ "currentProgress", "classUi__CalProgress.html#a391ec4e0a711f40a0d9fda75807f1615", null ],
    [ "finishLabel", "classUi__CalProgress.html#a1dc5775888275ec9e238d8f81ef3590c", null ],
    [ "gridLayout", "classUi__CalProgress.html#a7ca9988d777ae77d16bfa72fa0f74b53", null ],
    [ "totalLabel", "classUi__CalProgress.html#ad9ebe07e421d6b3d8ec4658724dad637", null ],
    [ "totalProgress", "classUi__CalProgress.html#ab3c73ef98271260a165910be6e060f20", null ],
    [ "verticalSpacer", "classUi__CalProgress.html#a3d0928237a0fc58631c457fe627c27db", null ]
];