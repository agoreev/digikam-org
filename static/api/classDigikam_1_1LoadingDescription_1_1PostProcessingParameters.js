var classDigikam_1_1LoadingDescription_1_1PostProcessingParameters =
[
    [ "PostProcessingParameters", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#aad167d02b5dffcd090577f45a35d0ec0", null ],
    [ "hasProfile", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a4d677d6ee7553b0fbcf9753d753597e3", null ],
    [ "hasTransform", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a9a968bd7f8807433b2d45ce6b53b482c", null ],
    [ "needsProcessing", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a2b6b2566b71f54df05417f1e2a27dfe5", null ],
    [ "operator==", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#af5a20c732376027d5a20f0de2f89c17a", null ],
    [ "profile", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a57fc5de6d7cbdaceef7537527703b3a2", null ],
    [ "setProfile", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a8692d351f385775c496ec98f3650802e", null ],
    [ "setTransform", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#ac5383f1da035b95e9bdd4d83ee6081e6", null ],
    [ "transform", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#ad3b28c91bc519e88f4af701d6d5260c7", null ],
    [ "colorManagement", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a45ff624ed0f40cf1f2a769ce59e6b338", null ],
    [ "iccData", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html#a6ae9b73dce2d042be3394db84abb62b8", null ]
];