var classDigikam_1_1VersionNamingScheme =
[
    [ "~VersionNamingScheme", "classDigikam_1_1VersionNamingScheme.html#a315eea146fbaf9968f65f18886106934", null ],
    [ "baseName", "classDigikam_1_1VersionNamingScheme.html#a9268f5984769d6d4f9071fe46c341af9", null ],
    [ "directory", "classDigikam_1_1VersionNamingScheme.html#aae0073b2c6b5d016c5c62e1be677c624", null ],
    [ "incrementedCounter", "classDigikam_1_1VersionNamingScheme.html#ae5d047fdc002815d33c3f7cf6d55b3ff", null ],
    [ "initialCounter", "classDigikam_1_1VersionNamingScheme.html#ac53fb79add02a51749077e48c182e284", null ],
    [ "intermediateDirectory", "classDigikam_1_1VersionNamingScheme.html#af2949e2dfe7b174310d024ae06510ec6", null ],
    [ "intermediateFileName", "classDigikam_1_1VersionNamingScheme.html#a503d53959bc84e4c21bae478ded7beed", null ],
    [ "versionFileName", "classDigikam_1_1VersionNamingScheme.html#aa0a434d970f057c2c28b0d2a38a73e43", null ]
];