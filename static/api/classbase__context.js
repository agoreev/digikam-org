var classbase__context =
[
    [ "base_context", "classbase__context.html#acb5c1d86aad6edf6a54ea257b156b737", null ],
    [ "~base_context", "classbase__context.html#a6c70a8405aba463c5c8c58db5aab8759", null ],
    [ "add_warning", "classbase__context.html#a5de74fcedf6ae668cde187faf854ff68", null ],
    [ "get_image", "classbase__context.html#a8076ada959a4b17f07e3d43041af2550", null ],
    [ "get_warning", "classbase__context.html#a84a1a9eb54db323e1d912a531d7e9ec2", null ],
    [ "has_image", "classbase__context.html#a782a753acd39f4f0ed51bbb3f1a85996", null ],
    [ "set_acceleration_functions", "classbase__context.html#a8a1fdfbda139d4c8ada99f5790e5b279", null ],
    [ "acceleration", "classbase__context.html#ab6d12d4221057178c1a96f9e11e7ed40", null ]
];