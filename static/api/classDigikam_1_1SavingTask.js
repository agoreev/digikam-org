var classDigikam_1_1SavingTask =
[
    [ "SavingTaskStatus", "classDigikam_1_1SavingTask.html#af50b4403bf046594bfee3b8887194b3b", [
      [ "SavingTaskStatusSaving", "classDigikam_1_1SavingTask.html#af50b4403bf046594bfee3b8887194b3baa040754344f2fb2ca7b44606cacca42e", null ],
      [ "SavingTaskStatusStopping", "classDigikam_1_1SavingTask.html#af50b4403bf046594bfee3b8887194b3baf2e55f2e8a77e64c231334e34a2daa14", null ]
    ] ],
    [ "TaskType", "classDigikam_1_1SavingTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1SavingTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1SavingTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "SavingTask", "classDigikam_1_1SavingTask.html#a5daa0839d01f5397f6af72d35c45855c", null ],
    [ "continueQuery", "classDigikam_1_1SavingTask.html#a021e076af898d93f62d0b172346281ab", null ],
    [ "execute", "classDigikam_1_1SavingTask.html#add1c2614a9e1d785a7f5d5d14ccd95bc", null ],
    [ "filePath", "classDigikam_1_1SavingTask.html#a667af7c5eab02e65293e1c3bafc36f4a", null ],
    [ "granularity", "classDigikam_1_1SavingTask.html#ab976f06eb18822b0da8e481fd2ff55d0", null ],
    [ "progressInfo", "classDigikam_1_1SavingTask.html#a5a0c7019a4081a388a61a41389ac67ef", null ],
    [ "setStatus", "classDigikam_1_1SavingTask.html#a8f3db46d20d31e6e498feddd571aae89", null ],
    [ "status", "classDigikam_1_1SavingTask.html#af343f3a9899b4ace017a365a256c94f5", null ],
    [ "type", "classDigikam_1_1SavingTask.html#a8542e55cd65ae98f3a265113b511bcc4", null ],
    [ "m_thread", "classDigikam_1_1SavingTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ]
];