var classDigikam_1_1EigenFaceMatMetadata =
[
    [ "StorageStatus", "classDigikam_1_1EigenFaceMatMetadata.html#a3c5b3a15812515d622772e2bce54417f", [
      [ "Created", "classDigikam_1_1EigenFaceMatMetadata.html#a3c5b3a15812515d622772e2bce54417faafcd4862d55ddd7501a737af3d2034e8", null ],
      [ "InDatabase", "classDigikam_1_1EigenFaceMatMetadata.html#a3c5b3a15812515d622772e2bce54417fafc81f0ab0ff2da992cb5e608daf44acd", null ]
    ] ],
    [ "EigenFaceMatMetadata", "classDigikam_1_1EigenFaceMatMetadata.html#a9fdb503c0cb0b543f373a6e0b8329fee", null ],
    [ "~EigenFaceMatMetadata", "classDigikam_1_1EigenFaceMatMetadata.html#ab0b8ad095cd9e8ff34c7ea8b1545a591", null ],
    [ "context", "classDigikam_1_1EigenFaceMatMetadata.html#ad8b1b9654e60d58cbf67a8c65476c4cd", null ],
    [ "databaseId", "classDigikam_1_1EigenFaceMatMetadata.html#a1ed0f4ffefe3f0fab9dc563c3335dd11", null ],
    [ "identity", "classDigikam_1_1EigenFaceMatMetadata.html#a007a12d92dadc6d53166e30bc6044fe1", null ],
    [ "storageStatus", "classDigikam_1_1EigenFaceMatMetadata.html#aed8a715917693733e4c7468d435a2993", null ]
];