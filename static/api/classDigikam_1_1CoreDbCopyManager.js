var classDigikam_1_1CoreDbCopyManager =
[
    [ "FinishStates", "classDigikam_1_1CoreDbCopyManager.html#a3812004ff511f20fddbde198f85db040", [
      [ "success", "classDigikam_1_1CoreDbCopyManager.html#a3812004ff511f20fddbde198f85db040a1ed7296f17eda239c694b364e07696b9", null ],
      [ "failed", "classDigikam_1_1CoreDbCopyManager.html#a3812004ff511f20fddbde198f85db040a01c685d62c6d08c54f8550b2d311e018", null ],
      [ "canceled", "classDigikam_1_1CoreDbCopyManager.html#a3812004ff511f20fddbde198f85db040a4d140dfb328b2d235549abe0b03bee41", null ]
    ] ],
    [ "CoreDbCopyManager", "classDigikam_1_1CoreDbCopyManager.html#a00f9d38d8645443c30d8395602bd72c1", null ],
    [ "~CoreDbCopyManager", "classDigikam_1_1CoreDbCopyManager.html#a00e45f674235362f5c4f2442ab128cf0", null ],
    [ "copyDatabases", "classDigikam_1_1CoreDbCopyManager.html#a27f60c8f287542bb98eb289697215aea", null ],
    [ "finished", "classDigikam_1_1CoreDbCopyManager.html#a35ee7f11e48618ed2fce291ccb20a539", null ],
    [ "smallStepStarted", "classDigikam_1_1CoreDbCopyManager.html#afdc9231ba4b6cc626ba1b0e45b790320", null ],
    [ "stepStarted", "classDigikam_1_1CoreDbCopyManager.html#a1b3b2d896de84b7714224c7ec47dbf35", null ],
    [ "stopProcessing", "classDigikam_1_1CoreDbCopyManager.html#a2c9d8d191ba0e7c8bd64026ae54fa3ef", null ]
];