var structde265__image__spec =
[
    [ "alignment", "structde265__image__spec.html#a3b47c6eb4bd363668b0af6823f7e1234", null ],
    [ "crop_bottom", "structde265__image__spec.html#a43e7b3aae4c3173d782c4c90ab62fe89", null ],
    [ "crop_left", "structde265__image__spec.html#a27f678056cf987e49d5a189a2b1b2817", null ],
    [ "crop_right", "structde265__image__spec.html#a0f056947ac8ad961c1f5c554c915773f", null ],
    [ "crop_top", "structde265__image__spec.html#a9ed3362f4dd118d8959245670024ff16", null ],
    [ "format", "structde265__image__spec.html#ab23120fc1f306bca58e360549f43f3d8", null ],
    [ "height", "structde265__image__spec.html#a6d5629415faaa4ebefee405c7f70e624", null ],
    [ "visible_height", "structde265__image__spec.html#a957a42fddee5f0aca5e213d93ddd4ab0", null ],
    [ "visible_width", "structde265__image__spec.html#a1ab7885c6ad7dce047081455af29d723", null ],
    [ "width", "structde265__image__spec.html#a5ddf463a9692d926665687415e20ac0e", null ]
];