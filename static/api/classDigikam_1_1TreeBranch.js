var classDigikam_1_1TreeBranch =
[
    [ "TreeBranch", "classDigikam_1_1TreeBranch.html#aaa087dc51daf47025186ce6d42c1d136", null ],
    [ "~TreeBranch", "classDigikam_1_1TreeBranch.html#ac06c79a7071adb2986b07ce4ef02c555", null ],
    [ "data", "classDigikam_1_1TreeBranch.html#af84328cc5d5aca7d6b293c2429836289", null ],
    [ "help", "classDigikam_1_1TreeBranch.html#a9b9a333cd45b41fbefd7a84fd65bd591", null ],
    [ "newChildren", "classDigikam_1_1TreeBranch.html#ab031f25d68abfa82c33f7ff53138b4cc", null ],
    [ "oldChildren", "classDigikam_1_1TreeBranch.html#a29d96a07b8657265574d3aa84ff0688d", null ],
    [ "parent", "classDigikam_1_1TreeBranch.html#a569b8ae452b4082d6419a7898c187611", null ],
    [ "sourceIndex", "classDigikam_1_1TreeBranch.html#aca1d19068b2231c9d36e0ea75fdee994", null ],
    [ "spacerChildren", "classDigikam_1_1TreeBranch.html#a0a38b4f797ac591ff1c459684932f046", null ],
    [ "type", "classDigikam_1_1TreeBranch.html#a1ed232e49db76ee650fc4f21a49130eb", null ]
];