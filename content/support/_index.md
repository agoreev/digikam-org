---
date: "2017-03-21"
title: "Support"
author: "digiKam Team"
description: "Support resources for digiKam"
category: "support"
aliases: "/support"
menu: "navbar"
---

<div class="content"><h3>Getting Help</h3>

<p></p>
<p>
New to digikam ? Needs some help ?
</p>
<p>
<strong>First</strong>, read carefully the <a href="/documentation/">digikam documentation</a>.
<br> It's actively maintained and you should find most of what you need there.
</p>

<p>
<strong>Then</strong>, if you still have questions, you should have a look in the <a href="/documentation/faq/">FAQ (Frequently Asked Questions)</a>.
</p>

<p>
If you can't find an answer to your question, feel free to <a href="/support/#mailinglists">subscribe</a> to the digikam mailing list to get support.
<br>
If you think it's a bug, you can report it to bugzilla, see below.
</p>


<p></p>
<h3>Reporting bugs and wishes</h3>

![digiKam Components from Bugzilla](/img/content/support/support_bugzilla.png)

<p>
Please use the bug tracking system for all bug-reports and new feature wishlists. Take a care to use the right component  to post a new file at the right place. You can checkout the current bug-reports and wishlists at these urls:</p>

<ul>
<li><a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam bugs and wishes</a></li>
</ul>

<p>digiKam use external libraries to manage metadata (<a href="http://www.exiv2.org">Exiv2</a>) and digital camera (<a href="http://www.gphoto.org">GPhoto</a>). Please use links listed below to reports specifics bugs relevant of these libraries:</p>

<ul>
<li><a href="https://github.com/Exiv2/exiv2/issues">Exiv2 library bugs and wishes</a></li>
<li><a href="https://github.com/wang-bin/QtAV/issues">QtAV library issues and wishes</a></li>
<li><a href="https://github.com/LibRaw/LibRaw/issues">Libraw library issues and wishes</a></li>
<li><a href="https://github.com/lensfun/lensfun/issues">Lensfun library issues and wishes</a></li>
<li><a href="http://gphoto.org/bugs">GPhoto library bugs and wishes</a></li>
</ul>

<p></p>
<h3>Mailing list Subscription</h3>

<ul>

<li>digikam-users<br>
<a href="https://mail.kde.org/mailman/listinfo/digikam-users">Mailing list subscription</a> for all questions, comments, suggestions, and requests by the <b>digiKam users</b>. <a href="http://mail.kde.org/pipermail/digikam-users/">(Archives)</a>
<br>
with Nabble <a href="http://digikam.1695700.n4.nabble.com/digikam-users-f1735189.html">web interface and search engine</a>
</li>

<br>

<li>digikam-devel<br>
<a href="https://mail.kde.org/mailman/listinfo/digikam-devel">Mailing list subscription</a> for <b>digiKam developers</b> threads. <a href="http://mail.kde.org/pipermail/digikam-devel/">(Archives)</a>
<br>
with Nabble <a href="http://digikam.1695700.n4.nabble.com/digikam-devel-f1695701.html">web interface and search engine</a>
</li>

</ul></div>
