---
date: "2017-03-21"
title: "Documentation"
author: "digiKam Team"
description: "digiKam Documentation and Resources"
category: "documentation"
aliases: "/docs"
menu: "navbar"
---

### Documentation and Useful Resources

![Online Handbook](/img/content/documentation/documentation_online.png)

The digiKam documentation comes as a package usually called _digikam-doc_ that you can install locally. 
You can also read it online (updated automatically every Friday). Some options might differ from your local installation, 
because the online manuals cover to the development version:

* [digiKam HTML version](https://docs.kde.org/trunk5/en/extragear-graphics/digikam/index.html)
* [Showfoto HTML version](https://docs.kde.org/trunk5/en/extragear-graphics/showfoto/index.html)
* [digiKam PDF version](https://docs.kde.org/trunk5/en/extragear-graphics/digikam/digikam.pdf)
* [Showfoto PDF version](https://docs.kde.org/trunk5/en/extragear-graphics/showfoto/showfoto.pdf)

[digiKam tutorials](https://userbase.kde.org/Digikam/Tutorials) wiki offers tutorials that cover various digiKam tools and functionality.

### API and Database Schema

If you plan contribute as a developer to digiKam, you might find the following links useful:

* [digiKam API](https://www.digikam.org/api/)
* [Database schema](https://cgit.kde.org/digikam.git/plain/project/documents/DBSCHEMA.ODS)

Use [SQliteBrowser](https://sqlitebrowser.org/) to work with SQlite3 database files.

### Contribute

We encourage you to improve the existing documentation by submitting comments, corrections, and patches. 
If you'd like to help to the documentation, have a look at the [Contribute](/contribute#documentation) page.
