---
date: "2017-03-30T14:09:49-06:00"
title: "Meta - Writing"
author: "Pat David"
description: "Meta page about www.digikam.org"
---

Writing content for the website is a bit different compared to using a CMS (like the previously used [Drupal][]).

The biggest change is that the website data is now being tracked in a version control system, [Git][].

[Drupal]: https://www.drupal.org/
[Git]: https://git-scm.com/

Contributors that want to write content for the site only need to write their content as a plain text [markdown][] file.

This file and any associated assets required (such as images) can then be delivered to responsible team members to be checked and then published.

[markdown]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

### Writing Quick Start

1. Download or install Hugo (at least v0.19) (and put it in your path).
2. Clone the [digikam-org website from the git repository].
3. Open a terminal and navigate to `/path/to/digikam-org/`
4. Use the `hugo new` command to create new content. For example, the command `hugo new news/2017-01-01-my-new-post.md` creates a new News article with the necessary metadata.
5. Open the new file in your text editor of choice.
6. Fill in the YAML metadata at the top of the file.
7. Write the page content in markdown.
4. Visually inspect your new post using the command `hugo serve`.  Open a web browser and go to http://localhost:1313

### Viewing the Site Locally

Before submitting a patch or pushing to the git repo, you should preview the site locally and ensure that everything appears as you would like it to.

You can view the site locally by completing the following steps:

1. Download or install Hugo (at least v0.19) (and put it in your path).
2. Clone the [digikam-org website from the git repository].
3. Open a terminal and navigate to `/path/to/digikam-org/`
4. Run the command `hugo serve`.
5. Open a web browser and go to http://localhost:1313

   You see the digiKam website.

[digikam-org website from the git repository]: {{< ref "documentation/meta/index.md#git-repository" >}}

### Publishing Changes

#### Git Access

If your account is a developer account (has push access to the repository), you can publish news items by committing and pushing to the repository.
This should normally be done against the `dev` branch first to check that everything works and looks good.

If so, it can then be merged into the `master` branch.

#### Out-of-band Submissions

If you do not have developer access to the website repository or did not want to clone the repo, you can submit your article in several ways:

* Email the markdown content and images to someone with a developer account
* Post the content to the mailing list
* Use a gist or pastebin service to post the text, then send the link to someone with access
* Open a bugzilla bug and include the text of the post
