---
date: "2017-03-30T14:09:49-06:00"
title: "Meta"
author: "Pat David"
description: "Meta page about www.digikam.org"
---


### Overview

This page contains information about contributing to the digiKam website.

There are two URLs in use for the website:

* https://www.digikam.org

    The main digiKam website.

* https://staging.digikam.org

    The test/staging website.
    This should normally mirror the main website for the most part.
    This is where new content is tested before pushing to the main site.

The site is served as a statically built website. It uses [Hugo][] to precompile everything into the final output.

[Hugo]: http://gohugo.io/

This means that the website is now being pre-compiled from various source files ahead of time.
These files are then made available through the web server as static assets (nothing needs to be dynamically created on the server).
This significantly reduces the server requirements (no in-between scripting language or database required) as well as reduces
the possible attack vectors for malicious actors.

The website is managed in a git repository. Pushes to the git repo will be reflected on the server for the branch that was pushed.
There are Jenkins jobs dedicated to buuild website targets.

### Git Repository

The digiKam website is now being version controlled in a [git repository][] hosted in Gitlab.

If you have a [developer account][] (you have been granted push access) you can get the website by cloning it with this address:


    git clone git@invent.kde.org:websites/digikam-org.git

If you want to view the website or write content and create a patch to publish (that you'll submit to someone with push access) you can get the website by cloning it

Note: If you just need to write new content for the website, you can do this step and send a patch to the developers (or you can zip up your markdown file + assets and send that along as well).


[git repository]: https://invent.kde.org/websites/digikam-org
[developer account]: https://community.kde.org/Infrastructure/Get_a_Developer_Account

There are two main branches that will be visible publicly, `dev` and `master`.
They map to live websites like this:

git branch | website                     | Jenkins Job
-----------|-----------------------------|-----------------------------------------------------------------
dev        | https://staging.digikam.org | https://binary-factory.kde.org/job/Website_staging-digikam-org/
master     | https://digikam.org         | https://binary-factory.kde.org/job/Website_digikam-org/

Changes should always first be tested against the `dev` branch.

If they work (and don't implode the website) then they can be merged into the `master` branch.

The two branches should be maintained roughly in-sync with each other, and may occasionally need to be merged or rebased to make sure of this.

### Markdown

Markdown is a simple markup syntax for formatting plain text documents. This section is a simple reference for markdown formatting.

* [Paragraphs](#paragraphs)

---

#### Paragraphs

One or more consecutive lines of text.
Text with a blank line between them will be rendered as a new paragraph.

```
This is a line in a paragraph.
This is another line in the same paragraph
```

Renders as:

This is a line in a paragraph.
This is another line in the same paragraph

Empty lines indicate a new paragraph:

```
This is a different line in some other paragraph.
With some extra sentences here for illustration.

This will be in a different paragraph (notice the blank line separating it).
```
Renders as:

This is a different line in some other paragraph.
With some extra sentences here for illustration.

This will be in a different paragraph (notice the blank line separating it).

---
#### Headers

```
# Heading 1
## Heading 2
### Heading 3
```

Will produce:

# Heading 1
## Heading 2
### Heading 3

---

#### Emphasis

```
Emphasis can be done with *asterisks* or _underscores_.

Strong (bold) can be done with double **asterisks** or __underscores__.

They _can be **combined**_.

You can also strikethrough <del> with ~~double tildes~~.
```

Emphasis can be done with *asterisks* or _underscores_.

Strong (bold) can be done with double **asterisks** or __underscores__.

They _can be **combined**_.

You can also strikethrough <del> with ~~double tildes~~.

### File Naming Convention

News items have been using a naming convention:

`YYYY-MM-DD_The_News_Item_Descriptive_Name.md`

You can use the `hugo new` command to create new content using a template. The template will automatically be chosen according to what path your choose to create your new content. For example, `hugo new news/2000-01-01-the-party-is-over.md` will use the news template located in `digikam-org/archetypes/news.md`.

Contributors can help ease management of data by following a similar convention. :)

If they do not, the committer should probably remember to rename contributed files if possible (it won't break the build or anything, just makes it easier to manage files).

### Image Storage Location

For News articles: `digikam-org/static/img/news/<news article name>/`

For example, `digikam-org/static/img/news/2017-01-01_happy_new_year/ball-drop.jpg` would be the file system path to your image. You could then reference it from your news article `![The ball dropping](/img/news/2017-01-01_happy_new_year/ball-drop.jpg)`. Note that the path starts with `/` making it an absolute location from the web server root.

For other pages: `digikam-org/static/img/<page category>/`

For example, `digikam-org/static/about/digikam-team-photo.jpg` would be the file system path to your image. You could then reference it from your news article `![The ball dropping](/img/about/digikam-team-photo.jpg)`. Note that the path starts with `/` making it an absolute location from the web server root.
