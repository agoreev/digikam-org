---
date: "2018-03-25"
title: "Release Plan"
author: "digiKam Team"
description: "The release plan for the digiKam software."
category: "releases"
aliases: "/about/releaseplan"
---

### digiKam 7.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   22.12.2019  |        7.0.0-beta1     | Re-written Face detection and recognition based on OpenCV deep learning engine (GoSC 2019 project). LibRaw 0.20 support.                     |
|   26.01.2020  |        7.0.0-beta2     |                                                                                                                                              |
|   23.02.2020  |        7.0.0-beta3     |                                                                                                                                              |
|   22.03.2020  |        7.0.0           |                                                                                                                                              |

[![](https://i.imgur.com/BEIAMAsh.png "digiKam 7.0.0 - beta1")](https://imgur.com/BEIAMAs)

### digiKam 6.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   19.08.2018  |        6.0.0-beta1     | Refactored version with less dependencies and with video management support. This version will include also GoSC 2017/2018 projects.         |
|   31.10.2018  |        6.0.0-beta2     | Exiv2 0.27 support. Time-Adjust tool. DrMinGw support under Windows. New separate album contents options.                                    |
|   30.12.2018  |        6.0.0-beta3     | Libraw 0.19 support. New grouping items options.                                                                                             |
|   10.02.2019  |        6.0.0           |                                                                                                                                              |
|   14.04.2019  |        6.1.0           | New plugins interface named "DPlugins" for Generic, Editor, and BQM tools.                                                                   |
|   04.08.2019  |        6.2.0           | Exiv2 0.27.2 and Libraw 0.19.3 support.                                                                                                      |
|   08.09.2019  |        6.3.0           | Libraw 0.19.5 support. First version of exported DPlugin API. GMic-Qt ported as external plugin for Image Editor.                            |
|   09.11.2019  |        6.4.0           | New Clone tool for Image Editor. New DPlugins interface for native image loaders and Raw Import tools.                                                                                                             |

[![](https://c2.staticflickr.com/2/1843/44139156541_8ebdb4c93f_c.jpg "digiKam 6.0.0 - Image Editor Raw Import Tools Using Last Libraw 0.19")](https://www.flickr.com/photos/digikam/44139156541)


### digiKam 5.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   18.10.2015  |        5.0.0-beta1     |                                                                                                                                              |
|   29.11.2015  |        5.0.0-beta2     |                                                                                                                                              |
|   17.01.2016  |        5.0.0-beta3     |                                                                                                                                              |
|   28.02.2016  |        5.0.0-beta4     |                                                                                                                                              |
|   27.03.2016  |        5.0.0-beta5     |                                                                                                                                              |
|   24.04.2016  |        5.0.0-beta6     |                                                                                                                                              |
|   29.05.2016  |        5.0.0-beta7     |                                                                                                                                              |
|   03.07.2016  |        5.0.0           |                                                                                                                                              |
|   07.08.2016  |        5.1.0           |                                                                                                                                              |
|   18.09.2016  |        5.2.0           |                                                                                                                                              |
|   06.11.2016  |        5.3.0           |                                                                                                                                              |
|   08.01.2017  |        5.4.0           | Ported to QtAv for Video Files support.                                                                                                      |
|   12.03.2017  |        5.5.0           | Including database garbage collector.                                                                                                        |
|   11.06.2017  |        5.6.0           | Including new HTML Gallery tool, new Video SlideShow tool, Items Grouping improvements, Mysql garbage collector, GPS Bookmarks improvements. |
|   27.08.2017  |        5.7.0           | Including new Print Creator, Send by Email tool, many bugs fixed and triaged.                                                                |
|   12.01.2018  |        5.8.0           | Including new UPNP/DLNA export tool.                                                                                                         |
|   25.03.2018  |        5.9.0           |                                                                                                                                              |

[![](https://c1.staticflickr.com/5/4785/40984193011_e899c28e21_c.jpg "digikam 5.9.0 - Time search and Lens correction tool")](https://www.flickr.com/photos/digikam/40984193011)
[![](https://c1.staticflickr.com/5/4783/40984191931_ec5bc20553_c.jpg "digikam 5.9.0 - Albums View and Light Table")](https://www.flickr.com/photos/digikam/40984191931)

### digiKam 4.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   01.12.2013  |        4.0.0-beta1     |                                                                                                                                              |
|   12.01.2014  |        4.0.0-beta2     |                                                                                                                                              |
|   23.02.2014  |        4.0.0-beta3     |                                                                                                                                              |
|   30.03.2014  |        4.0.0-beta4     |                                                                                                                                              |
|   13.04.2014  |        4.0.0-RC        |                                                                                                                                              |
|   11.05.2014  |        4.0.0           | Version including all GoSC 2013 and 2014 projects.                                                                                           |
|   22.06.2014  |        4.1.0           |                                                                                                                                              |
|   03.08.2014  |        4.2.0           |                                                                                                                                              |
|   08.09.2014  |        4.3.0           |                                                                                                                                              |
|   06.10.2014  |        4.4.0           |                                                                                                                                              |
|   09.11.2014  |        4.5.0           |                                                                                                                                              |
|   14.12.2014  |        4.6.0           |                                                                                                                                              |
|   25.01.2015  |        4.7.0           |                                                                                                                                              |
|   22.02.2015  |        4.8.0           |                                                                                                                                              |
|   05.04.2015  |        4.9.0           |                                                                                                                                              |
|   10.05.2015  |        4.10.0          |                                                                                                                                              |
|   14.06.2015  |        4.11.0          |                                                                                                                                              |
|   26.07.2015  |        4.12.0          |                                                                                                                                              |
|   30.08.2015  |        4.13.0          |                                                                                                                                              |
|   11.10.2015  |        4.14.0          |                                                                                                                                              |

[![](https://c2.staticflickr.com/4/3929/15428982716_f238a6b06f_c.jpg "digiKam 4.x")](https://www.flickr.com/photos/digikam/15428982716/)
[![](https://c2.staticflickr.com/8/7441/11964874854_c3214b0895_c.jpg "digiKam 4.x")](https://www.flickr.com/photos/digikam/11964874854/)

### digiKam 3.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   06/02/2013  |        3.0.0           | Version including all GoSC 2012 projects.                                                                                                    |
|   10/03/2013  |        3.1.0           | Including bugs-fixes detected by [Coverity SCAN](http://scan.coverity.com/)                                                                  |
|   07/04/2013  |        3.2.0-beta1     | Including new [Table-View Mode](http://www.flickr.com/photos/digikam/8632965113/sizes/l/in/photostream/)                                     |
|   28/04/2013  |        3.2.0-beta2     |                                                                                                                                              |
|   12/05/2013  |        3.2.0           |                                                                                                                                              |
|   02/06/2013  |        3.3.0-beta1     |                                                                                                                                              |
|   16/06/2013  |        3.3.0-beta2     |                                                                                                                                              |
|   03/07/2013  |        3.3.0-beta3     |                                                                                                                                              |
|   04/08/2013  |        3.3.0           | Including [Face Recognition feature](http://youtu.be/iaFGy0n0R-g)                                                                            |
|   01/09/2013  |        3.4.0           |                                                                                                                                              |
|   29/09/2013  |        3.5.0           |                                                                                                                                              |

[![](https://c1.staticflickr.com/9/8462/7919791574_1cdbff1b6d_c.jpg "digiKam 3.x")](https://www.flickr.com/photos/digikam/7919791574/)
[![](https://c1.staticflickr.com/9/8310/8025492511_a2a17d02ac_c.jpg "digiKam 3.x")](https://www.flickr.com/photos/digikam/8025492511/i)

### digiKam 2.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                            |
|      ---      |         ---            |                                                            ---                                                                               |
|   08/01/2012  |        2.5.0           | Version including XMP Sidecar support, Face Recognition, Image Versioning, and Reverse Geo-location.                                         |
|   06/05/2012  |        2.6.0           | Version including Color and Pick labels, Tags keyboard shortcuts, Group of items.                                                            |
|   08/07/2012  |        2.7.0           |                                                                                                                                              |
|   05/08/2012  |        2.8.0           |                                                                                                                                              |
|   02/09/2012  |        2.9.0           |                                                                                                                                              |

[![](https://c1.staticflickr.com/7/6161/6210520128_8d6548d9c8_z.jpg "digiKam 2.x")](http://www.flickr.com/photos/digikam/6210520128/)
