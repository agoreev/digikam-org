---
date: "2017-03-21"
title: "FAQs"
author: "digiKam Team"
description: "Frequently asked questions about digiKam"
category: "documentation"
aliases: "/faq/digikam"
---

### My camera is not in the list of supported cameras in digiKam.

Can you add drivers for my camera ?

Only if you pay me. ;)

Seriously, digiKam doesn't included any camera drivers with it. It makes use of <a href="http://www.gphoto.org/">gphoto2</a> to do camera operations. If your camera is not in the supported list, i would recommend doing some <a href="http://www.google.com">searching</a> around. Many of the new cameras actually provide a USB MassStorage Device Interface, so that you can access the camera like a hard disk. (See, the FAQ about using a USB MassStorage Camera with digiKam).

If you have still have no luck, i would recommend contacting the gphoto2 people.



### How can I inform you about bugs and wishes?


##### Reporting bugs and wishes



Please use the KDE bug tracking system for all bugreports and new feature wishlists. You can checkout the current bugreports and wishlists at these urls

* <a href="http://bugs.kde.org/buglist.cgi?product=digikam&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">digiKam bugs and wishes</a>



### I have a USB Mass Storage Camera. How do I use it with digiKam ?


#### The Easy Way!


If you have a relatively modern Linux distribution that is pre-configured to work with USB Mass Storage Devices, there's nothing you need to set up!. It should be truly "plug and play":


* Plug your device in to the USB slot
* It will be automatically detected by the kernel, and an appropriate entry in /etc/fstab will be added automatically.
* The device will be mounted under /mnt/flash or /mnt/camera
* Run digiKam setup and add a "Mounted Camera". Set the path to the above path, where the camera is mounted.




#### Manual Setup


If you're running a Linux distribution that doesn't set up USB Mass Storage devices automatically, here are the steps you need to follow:


* Most users using a recent Linux Distribution can skip this step. Configure the Linux Kernel:
    * Add SCSI Support
        SCSI Support (CONFIG_SCSI)
        SCSI IDE Support (CONFIG_BLK_DEV_IDESCSI)
        SCSI disk support (CONFIG_BLK_DEV_SD)
        SCSI generic support (CONFIG_CHR_DEV_SG)
    * Add USB Support
      Support for USB (CONFIG_USB)
      USB drivers. One or more of: ECHI HCD (CONFIG_USB_EHCI_HCD), UHCI (CONFIG_USB_UHCI), OHCI (CONFIG_USB_OHCI)
      Preliminary USB device file system (CONFIG_USB_DEVICEFS)
      USB Mass Storage support (CONFIG_USB_STORAGE)
* Load the required kernel modules: modprobe ide-scsi sd_mod sg vfat modprobe usb-ohci (or usb-uhci depending on your usb bus) modprobe usb-storage (or usb-uhci depending on your usb bus)
* Plug in your USB device. You should see your device listed in /proc/bus/usb/devices. If not, you need to fix the kernel or check your cabling.
* Use the sg3-utils package to determine the device name of your USB Mass Storage Device:

    * Install the sg3-utils package (e.g. on Debian, Lindows: apt-get install sg3-utils)
    * Connect the camera and turn it onto the picture display mode
    * List your raw SCSI devices by running sg_scan -i. The output will look something like this:

        ```
        /dev/sg0: scsi0 channel=0 id=0 lun=0 [em] type=5 _NEC CD-ROM CD-3002A C000 [wide=0 sync=0 cmdq=0 sftre=0 pq=0x0]
        /dev/sg1: scsi1 channel=0 id=0 lun=0 [em] type=0 OLYMPUS C-120/D-380 1.00 [wide=0 sync=0 cmdq=0 sftre=0 pq=0x0]
        ```

    * This tells me that I have 2 SCSI devices on my system: My CD drive on /dev/sg0, and a OLYMPUS D-380 Camera on /dev/sg1
    * Determine the real SCSI device associated with your USB Mass Storage Device with sg_map. The output looks like: /dev/sg0 /dev/sr0 /dev/sg1 /dev/sda
    * Now we can match the results of these last two steps. For example the Olympus camera is on the /dev/sda device.
    * In most cases, USB Mass Storage Devices will only have one partition on them, so we can safely assume that the final, mountable device is /dev/sda1 (the first partition on /dev/sda).
    * Make a directory where you can mount the camera: mkdir /mnt/camera; chmod 666 /mnt/camera
    * Try mounting the camera now: mount -t vfat /dev/sda1 /mnt/camera
    * If no errors show up, then you have performed all the steps correctly. To verify everything is right, point you file manager to /mnt/camera and you should be able to see the folder/pictures on the camera
* Add an entry to /etc/fstab for the mount point: /dev/sda1 /mnt/camera auto defaults,user,noauto 0 0
* Now it's ready to go. Any user can mount the device: mount /mnt/camera
* Make sure you unmount it before taking the card out or unplugging it: umount /mnt/camera
* Run digiKam setup and add a USB MassStorage Camera. Set the path to the above path, where the camera is mounted.


### Thumbnail generation fails on large files

Q: On all my albums, digiKam fails to generate thumbnails on some images. The only pattern I can spot is that it fails on largish files (bigger than 1MB). When the thumnails are generated, the ones that fail appear at first and then are replaced by the broken picture icon.

A: try changing the following setting:

kcontrol-&gt;kde components-&gt;file manager-&gt;Previes &amp; Metadata (tab)-&gt;Maximum file size


### Using hotplugging with digiKam


#### Introduction

With Kernel 2.4 came a hotplug support into Linux. This feature is also supported by gPhoto, which is used as camera driver from digiKam. So it should (and is) possible to add an hotplug feature into digiKam. This article tells you what are the requirements and what you have to do.

When you put a device into a hotplug bus like USB, the kernel detects it and looks if it has a suitable driver compiled inside. If not, the hotplug module is called and asked what to do. <a href="http://www.gphoto.org/">gPhoto2</a> offers a way to configure hotplug for USB cameras. Doing so, the hotplug module will call a gphoto2 library to add a driver and afterthat a customizable script calls the application that should interact with the device. This implementation uses two scripts. The first one, usbcam, is called as root and his job is to determine the active user and call the 2nd script digikam-hotplug using the users id. Both scripts could be downloaded here.



#### Requirements

The following requirements should be fullfilled to use digiKam with the hotplugging feature. Notice, it is also possible to change some of the requirements, but then you&amp;apsll have to do additional research to get the solution up and running.


* Linux with Kernel &gt;= 2.4, compiled with hotplugging Support
* gphoto 2.x and libgphoto 2.x


As an additional remark, i should mention that the digikam-hotplug script that you could donwload requires KDE not only installed, it has to be the active Window Manager.



#### Installation and Configuration

This section gives the same information like <a href="http://www.gphoto.org/doc/manual/permissions-usb.html#setting-up-linux-hotplug/">chapter 4.3</a> from the gPhoto manual. It simply describes howto setup the kernel hotplugging feature. The only difference are digiKam specific <a href="http://digikam.free.fr/hotplug/usbcam.tar.gz">scripts</a>.



##### Checking the kernel hotplugging feature


First you should check whether your kernel comes with the required hotplugging feature. Therefore look if you have a file
`cat /proc/sys/kernel/hotplug`

and if it points to a valid hotplug binary. If not, you'll have to compile a new kernel with hotplugging support. It does'nt matter whether the hotplugging support is directly compiled into the kernel or if it is compiled as module.

libgphoto comes with the program print-usb-usermap which is usually located under

`/usr/lib/libgphoto2/print-usb-usermap` or `/usr/local/lib/libgphoto2/print-usb-usermap`

This program tells the hotplug module, which cameras are supported by the installed version from gPhoto. All you have normally to do is

`/usr/lib/libgphoto2/print-usb-usermap > /etc/hotplug/usb/usbcam.usermap`

Now you have to download the scripts, untar them using

`tar xvzf usbcam.tar.gz`

<strong>Notice:</strong> There are two versions of the scripts. The 0.2.x series support digiKam 0.6.x and the 0.3.x series digiKam 0.7.1+. Rename your version to usbcam and digikam-hotplug, and copy them to

`/etc/hotplug/usb/usbcam`

and make usbcam executable for root and digikam-hotplug executable for the users that it should work for. For security reasons you should also check, that only root has write permissions on each file.

To test your installation, you'll have to unplug your camera, wait a few seconds and plug it in again. After a few moments digiKam should start and connect automaticly to your camera.

If you have problems doing this, you should read the original gphoto documentation and check the syslog for notices from the two scripts.


#### Copyright

This script was writen by Oliver DÃ¶rr based on the usbcam script original distributed from the gPhoto Project. Because i'm using their code using the GPL license, this code is also distributed under the same license.


### Which file format should I use?


Most digital cameras store images in jpeg format on the memory card. JPEG is a compressed format, whose compression causes loss of quality. Heavy compression may make the loss of quality visible to the eye. Every time you open a JPEG image, edit it, save the file and close it, the image is compressed and quality is lost. Lost quality can not be regained.


To avoid this repeated loss of quality you should save your edited files in a lossless format. digiKam understands two of these: Tiff and png. In digiKam it is advisable to use png. digiKam supports the exif data in png files, whereas (currently) for tiff files, exif data are lost.

So the recommendation is to save any edited files as png.


You have now finished editing your photos and are content with the results. Should you keep the png-files or convert them to the far smaller JPEG files for storage?. If you are very concerned about storage space, you can do the latter. You could, however rather keep the lossless png files and only create JPEG copies in the moment you want to take your images to the print shop, send them by email or post them on the web.



### Why are my RAW images so dark?

In 16-bit mode, many are surprised that a relatively dark photograph shows up after RAW conversion with digiKam, even so the 'ColorManagement' and 'RAW Conversion' options have been meticulously set-up for the intended purpose. The simple reason is that digiKam uses <a href="http://www.libraw.org">Libraw</a> demosaisic method with linear conversion of RAW data, and if the ICC profile does not include gamma correction and/or tone mapping (most Canon profiles), the result is dark (in 8-bit mode the gamma correction is applied automatically). But nothing is lost, read on!

A RAW file is a container including the raw sensor data, EXIF metadata, and often a JPEG preview thumbnail. It does normally not include an ICC profile. The embedded JPEG file is used by digiKam to display RAW image thumbnails or in slide shows, and it can be used for TV display from the camera. This is fast, but a preview only, no raw data.

RAW conversion requires a couple of steps in order to produce a satisfying result, better adapted to your needs than JPEG out-of-the-camera images:

1. Choice of the internal working space in regard to the intended use
2. Adjust tonal range - define thresholds for highlights and shadows
3. Adjust gradation - adjust mid tones, for example using numerical numbers in the histogram or curves like features
4. Alter white balance if necessary
5. Color cast removal ideally using color temperature (or an according eyedropper tool) as well as a tint / tone slider. An according eye-dropper tool can be applied simply by clicking inside the image on neutral grey subject tones. This procedure doesn't always create the expected result, because quite often the light situation while capturing the image wasn't neutral either.
6. Smoothing and noise removal (if not done in the conversion process already)
7. Apply sharpening (refocus, unsharpmask)
8. Choose final color bitdepth (saving as JPEG always reverts to 8-bit depth, use PNG or TIFF for 16-bit)
9. Select output and pixel dimensions â€“ upsampling on the RAW file often gives better results in comparison to already processed files
10. Continue editing for example retouching or small enhancements in the favorite imaging application

Now, most profiles that come with the camera OEM software are not so suited for linear conversion. If you'd like a good profile, do it yourself or via a service. It will be more precise than the standard profile and it will <b>include gamma correction</b> - no dark images anymore in 16-bit mode!

Some good reading on the subject:


* [digiKam documentation](https://docs.kde.org/trunk5/en/extragear-graphics/digikam/using-setup.html#using-setup-editor)
* [Primer for ColorManagement](http://www.scribus.net/index.php?name=Web_Links&amp;req=viewlink&amp;cid=4)
* [Wikipedia](http://en.wikipedia.org/wiki/Color_management)
* [OpenRAW.org](http://www.openraw.org/actnow/)
* [European Color Initative ECI](http://eci.org/eci/en/020_eci.php)



### digiKam doesn't work when album library is on a network share (nfs, samba)

DigiKam freezes when I store my album library on a NFS server, NAS, windows shared folder, etc. What can I do ?

digiKam uses sqlite to store informations about images (tags, comments, rates, ...). Sqlite relies on file locking features provided by filesystems (check <a href="http://www.sqlite.org/faq.html#q5">this FAQ entry</a>). But, network file systems often have troubles with locking and this can cause unexpected problems with sqlite.

In order to solve this problem, try one of the following workarounds :


* Use a <a href="http://thoughtsonrails.wordpress.com/2007/03/03/digikam-albums-on-network-filesystem/">symlink for some albums stored on a network path</a>
* Use a <a href="http://lists.kde.org/?l=kde-bugs-dist&amp;m=117536777106366&amp;w=2">symlink for the sqlite database file</a>. This method is preferred if you want to have all your album library on a network path.

Note: the digiKam team is aware of this problem and is working on it. In the future, it will be possible to use another database backend to solve it.


### How to create a digiKam theme?


####  Header
Since 0.9.3 release, theme are based on pure XML format. Comments in the theme are done by using common XML markers to include comments. The header is the place where you can add some comments, give credit to yourself... Look an existing XML theme file for details.



#####  Base Properties XML Markers


    name:                              Marine
    CaseColor:                         #FFFFFF
    TextRegularColor:                  #000000
    TextSelectedColor:                 #000000
    TextSpecialRegularColor:           #658857
    TextSpecialSelectedColor:          #FFFFFF

BaseColor is the background color which shall be used for the thumbnail view on the right and album/tag tree on the left.
TextRegularColor is used for regular (unselected) text and TextSelectedColor is used for selected text. usually the selected region tends to be brighter or darker and the selected text needs to contrast with it. TextSpecial... are the colors which are used for rendering of some special text like the tags at the bottom of the thumbnails and the collection/date names in the left view.

The color specification is done by using standard HTML color format (RGB/hexadecimal).



#####  Banner Properties XML Markers

    BannerColor:                        #4BFF9C
    BannerColorTo:                      #35B56F
    BannerBevel:                        RAISED
    BannerGradient:                     HORIZONTAL
    BannerBorder:                       TRUE
    BannerBorderColor:                  #2FA062

These specifications are used to render the banner at the top of the thumbnail view.
BannerBevel indicates the type of bevel to use for the banner, possible choices are RAISED (for a raised appearance), SUNKEN (for a sunken appearance), FLAT (for a flat appearance). BannerGradient is used for rendering a gradient; the possible choices are SOLID (no gradient), HORIZONTAL (horizontal gradient), VERTICAL (vertical gradient), DIAGONAL (diagonal gradient). There are two color specifications: Color and ColorTo. If a SOLID gradient is specified only "Color" is used, for other cases the gradient is rendered from "Color" to "ColorTo". BannerBorder indicates whether a border should be drawn around the banner, using BannerBorderColor. set it to "TRUE" for rendering a border and to "FALSE" otherwise.



#####  Thumbnail Properties XML Markers

    ThumbnailRegularColor:             #E7FFF4
    ThumbnailRegularColorTo:           #C4D8CF
    ThumbnailRegularBevel:             RAISED
    ThumbnailRegularGradient:          HORIZONTAL
    ThumbnailRegularBorder:            TRUE
    ThumbnailRegularBorderColor:       #769286

    ThumbnailSelectedColor:            #4BFF9C
    ThumbnailSelectedColorTo:          #35B56F
    ThumbnailSelectedBevel:            RAISED
    ThumbnailSelectedGradient:         HORIZONTAL
    ThumbnailSelectedBorder:           TRUE
    ThumbnailSelectedBorderColor:      #2FA062

These specifications are used for rendering the thumbnails. Regular... is used for unselected thumbnails and Selected... for selected thumbnails. The specifications are similar to that of the banner.



#####  Listview Properties XML Markers


    ListviewRegularColor:              #FFFFFF
    ListviewRegularColorTo:            #FFFFFF
    ListviewRegular.bevel:             FLAT
    ListviewRegular.gradient:          SOLID
    ListviewRegular.border:            FALSE
    ListviewRegularBorderColor:        #FFFFFF

    ListviewSelectedColor:             #4BFF9C
    ListviewSelectedColorTo:           #35B56F
    ListviewSelectedBevel:             RAISED
    ListviewSelectedGradient:          HORIZONTAL
    ListviewSelectedBorder:            TRUE
    ListviewSelectedBorderColor:       #2FA062

These are used for rendering the items on the left treeview. again, the specifications are same as that of the banner.




Notes:


* Case sensitivity is important for specifiying the property names, but not for the property values.
*  System level installation of themes is done in $(INSTALLATION_PREFIX)/share/apps/digikam/themes and user level installation in $(HOME)/.kde/share/apps/digikam/themes
*  Theme names appear in the menu with the filename. Make sure to capitalize the first letter of the filename
*  Its not necessary to restart digiKam if you modify the theme. switch to a different theme and switch back. But if you add a new theme, you will have to restart digiKam for the new theme to be recognized.



### Color Management with X on Linux


Color management of your monitor is possible with digiKam from version 0.9.0 onwards, but it is slow. The better and more fundamental way is to include the display color management in your X server using profiles. Then all other applications will profit as well.



This <a href="http://linux.vilars.com/">site</a> explains it all, go there for the installation procedure and the How-To's.


Note:

The best way to color-manage the display is to establish a custom profile and use that one, a procedure to be repeated every other year or so for aging reasons. But if you go the easier way and use the profiles delivered from the manufacturer, it is paramount to do a normalization as described for example on the<a href="https://www.cambridgeincolour.com/tutorials/monitor-calibration.htm">cambridgeincolour</a> web site.



### Thumbnail generation fails on video files


#### Why don't I have thumbnails for my video files?


#### Why thumbnail generation stops at the first video file I have ?



digiKam uses [QtAV framework](http://www.qtav.org/) and [FFMpeg codecs](https://www.ffmpeg.org/) to generate thumbnails in background. Also, video support is optional at application compilation time.


So, you should check first if Video Support is present in the list from digiKam Help/Components Info dialog. If yes, you can install the stand alone QtAV media player 
on your system and check if playing video work as expected. If it do not work, probably a ffmpeg codec is missing on your system.


### How can I change the default video player ?


#### When I double-click on a video thumbnail, it opens -unwanted video player-, how could I change it to another video player ?



Start kcontrol then go to KDE Components/File Associations, then
click on video. Here you have a list of file types with the list of
applications that can open them.


E.g. if you want another video player for your .avi files, go to
x-msvideo. The first application listed here will be the default one.


### How can I change the text font size ?


#### Size of fonts for text and menus is too big/too small in Digikam. How can I adjust it ?




Start kcontrol then go to Appearance &amp; Thems", then click on "Fonts". Here you can adjust the font size but also choose another font.

Please be warned that this will change the font behavior of all KDE applications !


### Single-click on a thumbnail opens digiKam editor

#### When I click on a thumbnail, it opens the digiKam editor. How can I disable that ?



Start kcontrol then go to Peripherals/Mouse and choose "double-click
to open files and folders"


### Some "Properties" in digiKam sidebar are unknown or unavailable


#### Why can't I get Image Properties Dimensions or other items ?




Digikam uses [Exiv2 shared library](http://www.exiv2.org/) to display the file properties.
If the "Dimensions" value is "unknown", it means that relevant file format is not yet fully supported by the library.

Note: even if you have the "Dimensions" information, some other properties (Bits depth, Sensitivity, Compression, ...) can still be unavailable depending on the image file (and your digital camera).


#### In Metadata/GPS sidebar, when I click on "More Info...", it opens konqueror, how could I change it to another browser (firefox or anything else) ?


Start kcontrol (or systemsettings), then go to "KDE Components/Component Chooser", navigate to "Default Applications", then click on "Web Browser" and choose "in the following browser" where you can put any command.


#### In Menu-&gt;Tools-&gt;Email Images, kmail is invoked, but I use Thunderbird (or any other email client)


Start kcontrol (or system settings), then go to "KDE Components/Component Chooser", navigate to "Default Applications", then click on "Email Client" and select you preference


The email client can also be specified in the -&gt;Email Images dialog itself on the mail page. The setting will be remembered by digiKam.


Note: If you use Ubuntu, the kcontrol or systemsettings menus will not be available to you, you have do do in the digiKam dialog box as described above.



### How to deal with utf-8 encoding ?


##### I just upgraded my preferred linux distribution but now I have warnings about utf-8. I have a warning when starting digiKam about new character encoding. What should I do ?




Nowadays, most linux distributions use utf8 by default for the character encoding. It includes the content of files, but also the file names.

Look here to learn more about utf-8 and unicode :

<a href="http://en.wikipedia.org/wiki/UTF-8">http://en.wikipedia.org/wiki/UTF-8</a>


Digikam will warn you when you change your locale with a new encoding.
Eg, if you switched from iso latin1 (iso8859-1) to utf8, digiKam will send you this warning :


<pre><small>Your locale has changed from the previous time this album was opened.
Old Locale : ISO 8859-1, New Locale : UTF-8
This can cause unexpected problems. If you are sure that you want to
continue, click on 'Yes' to work with this album. Otherwise, click on 'No'
and correct your locale setting before restarting digiKam
</small></pre>


Before accepting this modification, you should migrate at least the file names in your albums to utf-8 or you might have troubles because digiKam won't be able to access directories which contains accents non encoded in utf-8.


For that purpose, linux distributions propose various solutions and tools, but mainly based on convmv :

</p>



###### Gentoo


Gentoo has a very nice page about using UTF-8 :


<a href="http://www.gentoo.org/doc/en/utf-8.xml">Using UTF-8 with Gentoo</a>


This could also be useful for users of other distributions.







###### Mandriva

For Mandriva 2007, it seems that the same convmv can be applied.
Mandriva provide also a script in their
<a href="http://qa.mandriva.com/twiki/bin/view/Main/MandrivaLinux2007Errata#UTF8_issue_when_reinstalling_and">Mandriva 2007 Errata</a> page.


There is also a <a href="http://sebdeblinux.free.fr/">french page</a> to explain how to convert the filename and the content of the files to utf-8.







###### Suse


There's a FAQ entry about that :
<a href="http://www.novell.com/coolsolutions/qna/1786.html">Converting filenames to UTF-8 encoding</a>


There are also some informations here :
<a href="http://www.suse.de/~mfabian/suse-cjk/encodings-file-names.html">Converting file names</a> (part of CJK Support in SuSE Linux)







###### Ubuntu


With ubuntu, there's a migration tool written in python to do the job :
<a href="http://packages.ubuntulinux.org/dapper/misc/utf8-migration-tool">utf8-migration-tool package</a>







###### Other unix or linux operating systems

The most used tool to do that is <a href="http://j3e.de/linux/convmv/man/">convmv</a> (written in perl)


Eg (try it first in a test directory and backup everything before!) :
<small>
<pre>convmv --notest -r -f latin1 -t utf-8
</pre>
</small>




### Is digiKam available in my language ?


##### How can I use another language instead of the default (english) one ?





###### Distribution package

If you installed digiKam using your distribution package, language files should be included with it and you can run digiKam in any supported language.

You can change the default language by using KDE Control Center (kcontrol) :

Go to "Regional &amp; Accessibility" and then in "Country/Region &amp; Language" to use your default language.

Note, that will affects all your KDE applications.

You can also change the language used in digiKam with the $LANG environment variable.
Eg :
<pre>export LANG=fr.UTF-8 ; digikam</pre>




###### Compiled from stable or beta release

If you compiled yourself digiKam using the official tarballs, you can use digiKam in any supported language. The language files are installed with digikam.

See above for details about how to use another language.


###### Compiled from svn (Subversion)


If you compiled yourself digiKam from the latest trunk files, you will have to get the languages files you need. Check the <a href="http://www.digikam.org/?q=download/svn#homeinstall">svn download page</a> for more details (eg: sample script to compile digiKam in your homedir).


### digiKam doesn't compile


##### I can't compile digikam. What can I do ?




First, check that you have development files installed in your linux system. digiKam depends on a lot of libs and you must have the headers to compile it.


Usually, linux distributions have <b>-dev</b> packages when you want to compile something against a specific library. For a list of required dependencies, read the README file. Here's the <a href="http://websvn.kde.org/branches/stable/extragear/graphics/digikam/README?view=markup">current README (svn version)</a>.


On debian-based distribution (eg Debian, Ubuntu), you can install the required packages with :

<pre>apt-get build-dep digikam</pre>




If there are some warnings about unsermake, set the $UNSERMAKE environment variable as "no". Eg :

<pre>export UNSERMAKE=no</pre>




Look also at the [download page](/download) for more informations (tarball and svn sections). If you still have problems, check the [support page](/support).


### digiKam always crashes when doing xxxx. What can I do ?


##### When editing IPTC, playing with tags, assigning keywords or doing xxxx, digiKam crashes. What's the problem ?




It could be a problem with Exiv2. It's mandatory to use at least version 0.25 with digikam. So, install a recent version (0.16 is highly recommended).


If digiKam still crash, first check if you already have a former exiv2 library with version &lt; 0.11 installed on your system. There are been many reports about crashes due to old exiv2 libs still present in /usr/lib or /usr/local/lib.

If you found one old lib, remove it if possible, or try at least to rename it to see if it solves the problem.

It's strongly recommended to use the latest stable exiv2 version (0.14 now).


Then, check the [support](/support) and the [contrib](/contrib) pages.

Try to see if your problem has already been reported by another user in bugs.kde.org .

If it's not the case, check the contrib page, you will find instructions on how to compile digiKam with full debug and how to send us a backtrace.
