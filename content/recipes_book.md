---
date: "2017-03-21"
title: "digiKam Recipes Book"
author: "Dmitri Popov"
description: "Master digiKam with the digiKam Recipes book!"
category: "documentation"
aliases: "digikamrecipes"
---

digiKam is an immensely powerful photo management application, and mastering it requires time and effort. This book can help you to learn the ropes in the most efficient manner. Instead of going through each and every menu item and feature, the book provides a task-oriented description of digiKam's functionality that can help you to get the most out of this versatile tool. The book offers easy-to-follow instructions on how to organize and manage photos, process RAW files, edit images and apply various effects, export and publish photos, and much more.

![digiKam Recipes Reader](/img/digikam-recipes-reader.png)

Facts about the digiKam Recipes book:

- This is the first and only ebook about digiKam.
- The book was written in close cooperation with the digiKam developers.
- 50% of all book sales go to the digiKam project.</li>
- The book is DRM-free, so you can read it using any ebook reader or software that supports the EPUB or MOBI format.
- You'll receive all future editions of the book free of charge.

## What Readers Say about digiKam Recipes

> Dmitri's book is a great guidebook for those new to digiKam, as well as those already using it. I found that out for myself when I purchased the digiKam Recipes recently. It's a thorough overview, with examples, of all of the features that digiKam provides. There are tips for doing things that even those of us who have been using the software for years may not have stumbled upon. **Highly recommended!** --Scott Gomez

> I really like the regular updates you get, that alone was worth every penny. A living document, as they should all be! --Peter Teuben

> This is worth far more that the purchase price. I was struggling with digiKam prior to buying this book. Now I'm back to enjoying the computer work associated with digital photography. --Joh H (Amazon.com)

> This book is a must-have if you use digiKam to edit, manage and organize your digital photos. For two good reasons: firstly, because it is well written, is getting better and better over time, and has a lot of nice tricks in it. Secondly, because buying it gives money to the project, which is a great thing to do. --Romano Giannetti (Amazon.com)

> DigiKam is an extremely powerful tool; this ebook helps the user to get to grips with its sometimes complex interface and processes; also the project - free and open-source - gets a few quid from your purchase. So if there's something about the monopoly of Photoshop that puts you off, then why not give DigiKam a go? This will help to get you started. --M. Pat (Amazon.co.uk)

<div class="row columns text-center">
	<h3>Buy digiKam Recipes</h3>
</div>
<div class="row text-center download-icons">

	<div class="large-4 medium-4 small-12 columns">
		<a href="http://www.amazon.com/dp/B004774LJS"
			<i class="fa fa-amazon fa-5x"></i>
			<h4 class="index-header">Kindle</h4>
		</a>
	</div>
	<div class="large-4 medium-4 small-12 columns">
		<a href="https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ">
			<i class="fa fa-play-circle fa-5x"></i>
			<h4 class="index-header">Google Play</h4>
		</a>
	</div>
	<div class="large-4 medium-4 small-12 columns">
		<a href="https://gumroad.com/l/digikamrecipes">
			<i class="fa fa-credit-card-alt fa-5x"></i>
			<h4 class="index-header">Gumroad</h4>
		</a>
	</div>
</div>

The ebook released under the <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> license, so you are free to share the ebook and modify it as long as you share your modifications.

I hope you'll find the ebook useful, and if you have any comments, ideas, and suggestions, feel free to contact me at <a href="mailto:dmpop@linux.com">dmpop@linux.com</a>.
