---
date: "2017-03-22T13:13:45-06:00"
title: "Installing a package"
author: "digiKam Team"
description: "Install a binary package of digikam."
category: "download"
aliases: "/download/binary"
---

<div class="content"><h3>Jump to:</h3>
<div style="display:inline-block;width:100%;clear:both;font-size:150%;margin-bottom:20px;height:40px;">
<div style="float:left;height:100%;margin-right:50px;"><a href="#Linux">  <img src="/img/content/download/tux.png" alt="Linux"   title="Linux"   class="image image-_original " width="32" height="32"><span style="height:100%;line-height:100%;display:inline-block;vertical-align:middle;">Linux</span></a></div>
<div style="float:left;height:100%;margin-right:50px;"><a href="#FreeBSD"><img src="/img/content/download/bsd.png" alt="FreeBSD" title="FreeBSD" class="image image-_original " width="32" height="32"><span style="height:100%;line-height:100%;display:inline-block;vertical-align:middle;">FreeBSD</span></a></div>
<div style="float:left;height:100%;margin-right:50px;"><a href="#MacOS">  <img src="/img/content/download/mac.png" alt="MacOS"   title="MacOS"   class="image image-_original " width="32" height="32"><span style="height:100%;line-height:100%;display:inline-block;vertical-align:middle;">MacOS</span></a></div>
<div style="float:left;height:100%;margin-right:50px;"><a href="#Windows"><img src="/img/content/download/win.png" alt="Windows" title="Windows" class="image image-_original " width="32" height="32"><span style="height:100%;line-height:100%;display:inline-block;vertical-align:middle;">Windows</span></a></div>
</div>

<h3>Installing a package</h3>
<p>You can install digiKam using pre-compiled version. This is done by your distribution. A disadvantage is that in some cases the version provided by your distribution is not the latest stable version the digiKam team has released. The links given below will give you information about which version your distribution packages.</p>
<p><a name="Linux"></a><br>
<span class="inline inline-left"><img src="/img/content/download/tux.png" alt="Linux" title="Linux" class="image image-_original " width="32" height="32"></span></p>
<h4>Linux</h4>
<p></p>
<table border="1">
<tbody><tr>
<td>Distribution</td>
<td>Link to the list of digiKam packages</td>
<td>Command line to install</td>
</tr>
<tr>
<td>Kubuntu</td>
<td><a href="https://launchpad.net/~kubuntu-ppa/+archive/backports">here</a></td>
<td>apt-get install digikam</td>
</tr>
<tr><td>Ubuntu</td>
<td><a href="http://packages.ubuntu.com/search?keywords=digikam&amp;searchon=names&amp;suite=all&amp;section=all">here</a></td>
<td>apt-get install digikam</td>

</tr><tr>
<td>Debian</td>
<td><a href="http://packages.debian.org/search?searchon=names&amp;keywords=digikam">here</a> and  reason why the latest version of packages is not in testing can be found <a href="http://release.debian.org/migration/testing.pl?package=digikam">here</a></td>
<td>apt-get install digikam</td>
</tr>
<tr>
<td>Arch Linux</td>
<td><a href="http://www.archlinux.org/packages/?q=digikam">here</a></td>
<td>pacman -Sy digikam</td>
</tr>
<tr>
<td>Ark Linux</td>
<td><a href="https://www.archlinux.org/packages/?sort=&amp;q=digikam">here</a></td>
<td>apt-get install digikam</td>
</tr>
<tr>
<td>Mandriva/Mageia Linux</td>
<td><a href="http://mageia.madb.org/package/show/name/digikam">here</a></td>
<td>urpmi digikam</td>
</tr>
<tr>
<td>SUSE Linux</td>
<td><a href="http://software.opensuse.org/search?baseproject=ALL&amp;p=1&amp;q=digikam">here</a></td>
<td>yast -i digikam</td>
</tr>
<tr>
<td>Fedora Linux</td>
<td><a href="https://apps.fedoraproject.org/packages/digikam">here</a></td>
<td>dnf install digikam</td>
</tr>
<tr>
<td>Gentoo Linux</td>
<td><a href="http://packages.gentoo.org/package/media-gfx/digikam">here</a></td>
<td>emerge digikam</td>
</tr>
</tbody></table>
<p>Here, we provide some usefull tips for getting a slightly more up-to-date version:</p>
<p>Under Ubuntu 10.10 you can use some PPA, look <a href="http://scribblesandsnaps.wordpress.com/2010/12/10/install-digikam-1-6-on-ubuntu-10-10/">here</a>.</p>

<p><a name="FreeBSD"></a><br>
<span class="inline inline-left"><img src="/img/content/download/bsd.png" alt="FreeBSD" title="FreeBSD" class="image image-_original " width="32" height="32"></span></p>
<h4>FreeBSD</h4>
<p>The list of digiKam packages in FreshPorts can be found <a href="http://www.freshports.org/search.php?query=digikam&amp;search=go&amp;num=10&amp;stype=name&amp;method=match&amp;deleted=excludedeleted&amp;start=1&amp;casesensitivity=caseinsensitive">here</a>.</p>

<p><a name="MacOS"></a><br>
<span class="inline inline-left"><img src="/img/content/download/mac.png" alt="MacOS" title="MacOS" class="image image-_original " width="32" height="32"></span></p>
<h4>MacOS</h4>
<p>digiKam for MacOS can installed using our self-contained package available on <a href="http://download.kde.org/stable/digikam/">here</a>.</p>
<p>Be careful as digiKam is not as stable under MacOS as under Linux because of some bugs in the underlying KDE Software Collection libraries that digiKam depends on.</p>
<p>digiKam requires at least MacOs Sierra (10.12) to run.</p> 

<p><a name="Windows"></a><br>
<span class="inline inline-left"><img src="/img/content/download/win.png" alt="Windows" title="Windows" class="image image-_original " width="32" height="32"></span></p>
<h4>Windows</h4>
<p>digiKam for Windows can installed using our self-contained installer available on <a href="http://download.kde.org/stable/digikam/">here</a>.</p>
<p>Be careful as digiKam is not as stable under Windows as under Linux because of some bugs in the underlying KDE Software Collection libraries that digiKam depends on.</p>
<p>digiKam requires at least Windows 7 to run.</p> 

<p><span class="inline inline-left"><img src="http://i18n.kde.org/media/images/trademark_kde_gear_black_logo.png" alt="i18n" title="i18n" class="image image-_original " width="32" height="32"></span></p>
<h4>Internationalization</h4>
<p>If you installed digiKam using your Linux distribution package, language files should<br>
be included with it and you can run digiKam in any supported language.</p>
<p>You can change the default language by using Help Menu in digiKam.</p>
<p>If you don't find the required language in it, you can add a language by<br>
installing that particular language pack. For example, in debian based distros,<br>
if you want to add "french" language, you can do it by following:</p>
<p># sudo apt-get install language-pack-kde-fr-base<br>
# sudo apt-get install language-pack-kde-fr</p>
<p>Note: Windows and MacOS installers include already all translation files.</p>
</div>
