---
date: "2008-10-07T15:39:00Z"
title: "digiKam 0.10.0-beta4 release for KDE4"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release the 4th beta release dedicated to KDE4. The digiKam tarball can"
category: "news"
aliases: "/node/376"

---

<p>Dear all digiKam fans and users!</p>

<p>The digiKam development team is happy to release the 4th beta release dedicated to KDE4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.</p>

<a href="http://www.flickr.com/photos/digikam/2887768310/" title="raw2dngconverter by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3290/2887768310_0872c0b392.jpg" width="500" height="400" alt="raw2dngconverter"></a>

<p>Take care, it's always a BETA code with many of new bugs not yet fixed... Do not use yet in production...</p>
 
<p>KDE3 version still the stable release and have been published <a href="http://www.digikam.org/drupal/node/359">this summer</a>.</p>

<p>To compile digiKam for KDE4, you need <b>libkexiv2</b>, <b>libkdcraw</b>, and <b>libkipi</b> for KDE4. These libraries are now included in KDE core but for the moment, digiKam require libkdcraw and libkexiv2 from svn trunk to compile fine (These versions of libraries will be released with KDE 4.2). To extract libraries source code from svn, look at bottom of <a href="http://www.digikam.org/drupal/download?q=download/svn">this page</a> for details</p>

<p>For others depencies, consult the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/README?view=markup">README file</a>. There are also optional depencies to enable some new features as lensfun for lens auto-correction, and marble for geolocation.</p>
 
<p>The digikam.org web site has been redesigned for the new KDE4 series. All <a href=" http://www.digikam.org/drupal/node/323">screenshots</a> have been updated and a lots of <a href="  http://www.digikam.org/drupal/tour">video demos</a> have been added to present new features included in this version.</p>
 
<h5>NEW FEATURES (since 0.9.x series):</h5><br>

<b>General</b> : Ported to CMake/Qt4/KDE4.<br>
<b>General</b> : Thumbs KIO-Slave removed. digiKam now use multi-threading to generate thumnails.<br>
<b>General</b> : Removed all X11 library dependencies. Code is now portable under MACOS-X and Win32.<br>
<b>General</b> : Support of XMP metadata (require <a href="http://www.exiv2.org">Exiv2 library</a> &gt;= 0.16).<br>
<b>General</b> : Hardware handling using KDE4 Solid interface.<br>
<b>General</b> : Preview of Video and Audio files using KDE4 Phonon interface.<br>
<b>General</b> : Database file can be stored on a customized place to support remote album library path.<br>
<b>General</b> : New database schema to host more photo and collection informations.<br>
<b>General</b> : Database interface fully re-written using Qt4 SQL plugin.<br>
<b>General</b> : Support of multiple roots album paths.<br>
<b>General</b> : Physical root albums are managed as real album.<br>
<b>General</b> : New option in Help menu to list all RAW file formats supported.<br>
<b>General</b> : Geolocation of pictures from sidebars is now delegate to KDE4 Marble widget.<br>
<b>General</b> : New option in Help menu to list all main components/libraries used by digiKam.<br>
<b>General</b> : libkdcraw dependency updated to 0.3.0.<br>
<b>General</b> : Raw metadata can be edited, changed, added to TIFF/EP like RAW file formats (require Exiv2 &gt;= 0.18). Currently DNG, NEF, and PEF raw files are supported. More will be added in the future.<br><br>
 
<b>CameraGUI</b> : New design for camera interface.<br>
<b>CameraGUI</b> : New Capture tool.<br>
<b>CameraGUI</b> : New bargraph to display camera media free-space.<br><br>
 
<b>AlbumGUI</b> : Added Thumbbar with Preview mode to easy navigate between pictures.<br>
<b>AlbumGUI</b> : Integration of Simple Text Search tool to left sidebar as Amarok.<br>
<b>AlbumGUI</b> : New advanced Search tools. Re-design of Search backend, based on XML. Re-design of search dialog for a better usability. Searches based on metadata and image properties are now possible.<br>
<b>AlbumGUI</b> : New fuzzy Search tools based on sketch drawing template. Fuzzy searches backend use an Haar wevelet interface. You simply draw a rough sketch of what you want to find and digiKam displays for you a thumbnail view of the best matches.<br>
<b>AlbumGUI</b> : New Search tools based on marble widget to find pictures over a map.<br>
<b>AlbumGUI</b> : New Search tools to find similar images against a reference image.<br>
<b>AlbumGUI</b> : New Search tools to find duplicates images around whole collections.<br><br>
 
<b>ImageEditor</b> : Added Thumbbar to easy navigate between pictures.<br>
<b>ImageEditor</b> : New plugin based on <a href="http://lensfun.berlios.de">LensFun library</a> to correct automaticaly lens aberrations.<br>
<b>ImageEditor</b> : LensDistortion and AntiVignetting are now merged with LensFun plugin.<br>
<b>ImageEditor</b> : All image plugin tool settings provide default buttons to reset values.<br>
<b>ImageEditor</b> : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
<b>ImageEditor</b> : All image plugin dialogs are removed. All tools are embedded in editor window.<br>

<h5>BUGS FIXES:</h5><br>

001 ==&gt; 146864 : Lesser XMP support in digiKam.<br>
002 ==&gt; 145096 : Request: acquire mass storage from printer as from camera. Change menu "Camera" to "Acquire".<br>
003 ==&gt; 134206 : Rethink about: Iptc.Application2.Urgency digiKam Rating.<br>
004 ==&gt; 149966 : Alternative IPTC Keyword Separator (dot notation).<br>
005 ==&gt; 129437 : Album could point to network path. Now it's impossible to view photos from shared network drive.<br>
006 ==&gt; 137694 : Allow album pictures to be stored on network devices.<br>
007 ==&gt; 114682 : About library path.<br>
008 ==&gt; 122516 : Album Path cannot be on Network device (Unmounted).<br>
009 ==&gt; 107871 : Allow multiple album library path.<br>
010 ==&gt; 105645 : Impossible to not copy images in ~/Pictures.<br>
011 ==&gt; 132697 : Metadata list has no scrollbar.<br>
012 ==&gt; 148502 : Show rating in embedded preview / slideshow.<br>
013 ==&gt; 155408 : Thumbbar in the album view.<br>
014 ==&gt; 138290 : GPSSync plugin integration in the side bar.<br>
015 ==&gt; 098651 : Image Plugin filter based on clens.<br>
016 ==&gt; 147426 : Search for non-voted pics.<br>
017 ==&gt; 149555 : Always present search box instead of search by right-clicking and selecting simple or advanced search.<br>
018 ==&gt; 139283 : IPTC Caption comment in search function.<br>
019 ==&gt; 150265 : Avanced search filter is missing search in comment / description.<br>
020 ==&gt; 155735 : Make it possible to search on IPTC-text.<br>
021 ==&gt; 147636 : GUI error in advanced searches: lots of free space.<br>
022 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
023 ==&gt; 149026 : Search including sub-albums.<br>
024 ==&gt; 153070 : Search for image by geo-location.<br>
025 ==&gt; 154764 : Pictures saved into root album folder are not shown.<br>
026 ==&gt; 162678 : digiKam crashed while loading.<br>
027 ==&gt; 104067 : Duplicate image finder should offer more actions on duplicate images found.<br>
028 ==&gt; 107095 : Double image removal: Use trashcan.<br>
029 ==&gt; 112473 : findimages shows only small thumbnails.<br>
030 ==&gt; 150077 : Find Duplicate Images tool quite unusable on many images (a couple of issues).<br>
031 ==&gt; 161858 : Find Duplicate Image fails with Canon Raw Files.<br>
032 ==&gt; 162152 : Batch Duplicate Image Management.<br>
033 ==&gt; 164418 : GPS window zoom possibility.<br>
034 ==&gt; 117287 : Search albums on read only album path.<br>
035 ==&gt; 164600 : No picture in view pane.<br>
036 ==&gt; 164973 : Showfoto crashed at startup.<br>
037 ==&gt; 165275 : build-failure - imageresize.cpp - 'KToolInvocation' has not been declared.<br>
038 ==&gt; 165292 : albumwidgetstack.cpp can't find Phonon/MediaObject.<br>
039 ==&gt; 165341 : Crash when changing histogram channel when welcome page is shown.<br>
040 ==&gt; 165318 : digiKam doesn't start because of SQL.<br>
041 ==&gt; 165342 : Crash when changing album sort modus.<br>
042 ==&gt; 165338 : Right sidebar initally too big.<br>
043 ==&gt; 165280 : Sqlite2 component build failure.<br>
044 ==&gt; 165769 : adjustcurves.cpp - can't find version.h.<br>
045 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
046 ==&gt; 166663 : Tags not showing pictures without "Include Tag Subtree".<br>
047 ==&gt; 166616 : Filmstrip mode in View.<br>
048 ==&gt; 165885 : Thumbnails and images are NOT displayed in the main view center pane.<br>
049 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
050 ==&gt; 167168 : Timeline view shows redundant extra sections.<br>
051 ==&gt; 166440 : Removing images from Light Table is not working properly.<br>
052 ==&gt; 166484 : digiKam crashes when changing some settings and using "find similar" after the changes made.<br>
053 ==&gt; 167124 : Timeline view not updated when changing selected time range.<br>
054 ==&gt; 166564 : Display of *already* *created* thumbnails is slow.<br>
055 ==&gt; 166483 : Error in "not enough disk space" error message.<br>
056 ==&gt; 167379 : Image selection for export plugin.<br>
057 ==&gt; 166622 : Confusing Add Images use.<br>
058 ==&gt; 166576 : digiKam quits when running "Rebuild all Thumbnail Images" twice.<br>
059 ==&gt; 167562 : Image Editor Shortcut Keys under Edit redo/undo are missing in GUI.<br>
060 ==&gt; 167561 : Crash when moving albums.<br>
061 ==&gt; 167529 : Image Editor not working correctly after setting "use horizontal thumbbar" option.<br>
062 ==&gt; 167621 : digiKam crashes when trying to remove a tag with the context menu.<br>
063 ==&gt; 165348 : Problems when trying to import KDE3 data.<br>
064 ==&gt; 166424 : Crash when editing Caption with Digikam4 SVN.<br>
065 ==&gt; 166310 : Preview not working in image effect dialogs.<br>
066 ==&gt; 167571 : Unnatural order when removing images from light table.<br>
067 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
068 ==&gt; 167778 : Assigning ratings in image editor via shortcuts or toolbar not working.<br>
069 ==&gt; 168567 : Unable to edit or remove iptc and xmp info.<br>
070 ==&gt; 168846 : crash after playing with tags (seems to be rather a crash in Qt or kdelibs).<br>
071 ==&gt; 166671 : Image filter dialog buttons are much too big.<br>
072 ==&gt; 134486 : Keywords are not written to raw files even though they do embed iptc/exif.<br>
073 ==&gt; 168839 : Digikam crashed after tagging.<br>
074 ==&gt; 167085 : Color selection in sketch search tool looks plain black.<br>
075 ==&gt; 168852 : Crash on profile application.<br>
076 ==&gt; 167867 : Album view is reset by changing settings.<br>
077 ==&gt; 168461 : Info in Properties panel not updated after moving to other image.<br>
078 ==&gt; 169704 : Crash during RAW import.<br>
079 ==&gt; 166437 : Deleting images in Image Editor not working properly.<br>
080 ==&gt; 169814 : Compilation error: no exp2().<br>
081 ==&gt; 165345 : Selecting images from bottom right to top left does not work.<br>
082 ==&gt; 170693 : Enable geotagging of Olympus orf images.<br>
083 ==&gt; 170711 : Write single Keywords instead of Hirachy into IPTC.<br>
084 ==&gt; 168569 : New Album Folders are not detected in displayed in GUI until restart.<br>
085 ==&gt; 165337 : Text overlapping box on welcome page.<br>
086 ==&gt; 171310 : No preview photos shown clicking on imported albums.<br>
087 ==&gt; 171778 : digiKam does not load photos in its database.<br>
088 ==&gt; 171736 : Make transferring of metadata between machines easier.<br>
089 ==&gt; 170929 : Compilation of SVN version fails with gcc 4.3.2 unless debug option is activated.<br>
090 ==&gt; 171989 : Can't set gps photo location on .nef images.<br>
091 ==&gt; 172018 : Initial cursor in the "sharpen" dialog is on the "cancel" button.<br>
092 ==&gt; 172033 : Unable to draw rectangle from right to left.<br>

<div class="legacy-comments">

  <a id="comment-17849"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17849" class="active">Batch functions in digikam-0.10.0</a></h3>    <div class="submitted">Submitted by Warren (not verified) on Tue, 2008-10-07 19:51.</div>
    <div class="content">
     <p>I have been tracking the new version and quite surprised by the stability, so much so that I can't remember when last I had to fire up the KDE3/0.9.4 version.</p>
<p>The only feature that I miss in the new version are the extra batch functions, particulary the batch resize option that I use for moving pictures to a digital picture frame.</p>
<p>Can these missing batch functions be added before the final 0.10.0 version is released?  (Is already a goal for the coding sprint?)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17850"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17850" class="active">yes, Batch tools are not yet ported....</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-10-07 20:25.</div>
    <div class="content">
     <p>Like you can see <a href="http://www.kde-apps.org/content/show.php/Kipi-Plugins?content=16061">at this place</a>, there are few plugins not yet ported to KDE4/QT4...</p>
<p>Of course, Coding Sprint will be a good moment to review all pending jobs and complete missing plugins.</p>
<p>But about Batch tools, i'm already thinking about a Batch Queue Manager, to be able to apply more than one batch operations at the same. But it's for the future...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17851"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17851" class="active">Digikam 1.0</a></h3>    <div class="submitted">Submitted by fish (not verified) on Tue, 2008-10-07 20:36.</div>
    <div class="content">
     <p>Hello, when will Digikam 1.0 be released? I think it should have been labeled 1.0 a long time ago. It's stable, has tons of features, looks great and, above all, it's fun to use! While version numbers don't matter to *me* at all, I think for a lot of people there's a "psychological barrier" to use an app that is at version 0.1 ... what do others think? I vote for a Digikam 1.0 release when this is finished!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17853"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17853" class="active">1.0 version</a></h3>    <div class="submitted">Submitted by Fri13 (not verified) on Tue, 2008-10-07 21:21.</div>
    <div class="content">
     <p>I would vote that version-editing and backup functions comes to 0.10.x series until the 1.0 would be released. I feel that it is important to allow photographer easily to select photo, make wanted edits on it and then push wanted versions (size, format etc) from it to wanted locations. So it would improve even more the workflow on digiKam what has got great steps now already.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17857"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17857" class="active">Compilation réussie !!!</a></h3>    <div class="submitted">Submitted by <a href="http://www.lindaetmarguerite.net" rel="nofollow">Floréal</a> (not verified) on Wed, 2008-10-08 22:58.</div>
    <div class="content">
     <p>Bonjour/hello,</p>
<p>Oui, j'ai peu tester ce magnifique logiciel, dans sa version pour KDE4 !!!! Ah je le trouve très joli et tout et tout... question bugs je peux rien dire encore, je viens de l'installer !!!</p>
<p>Bon... encore très peu traduit en français... dommage ! J'espère que ça arrivera bientôt... j'aime pas l'anglais xD !</p>
<p>bonne continuation pour se magnifique logiciel... et comme la question a été posée... à quand digikam 1.0 ?</p>
<p>++</p>
<p>Floréal.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17880"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17880" class="active">not authorized to view attachments on svn page</a></h3>    <div class="submitted">Submitted by <a href="http://www.maysville-linux-users-group.org" rel="nofollow">ed wiget</a> (not verified) on Mon, 2008-10-20 19:39.</div>
    <div class="content">
     <p>I was trying to view the attachments on the svn page but always get:<br>
Access denied<br>
You are not authorized to access this page.  </p>
<p>Figuring you have to be a member of the drupal cms, I couldn't find a way to register either.  The files in question are:</p>
<p>Attachment	Size<br>
start_digikam.txt	237 bytes<br>
compile_exiv2.txt	477 bytes<br>
compile_digikam.txt	1</p>
<p>And the links are:<br>
http://www.digikam.org/drupal/system/files/start_digikam.txt<br>
http://www.digikam.org/drupal/system/files/compile_exiv2.txt<br>
http://www.digikam.org/drupal/system/files/compile_digikam.txt</p>
<p>Or the alternate way:<br>
http://www.digikam.org/?q=system/files&amp;file=start_digikam.txt</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17907"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-17907" class="active">Oui, j'ai peu tester ce</a></h3>    <div class="submitted">Submitted by <a href="http://www.trsohbet.name" rel="nofollow">sohbet</a> (not verified) on Sat, 2008-11-01 10:54.</div>
    <div class="content">
     <p>Oui, j'ai peu tester ce magnifique logiciel, dans sa version pour KDE4 !!!! Ah je le trouve très joli et tout et tout... question bugs je peux rien dire encore, je viens de l'installer !!!</p>
<p>Bon... encore très peu traduit en français... dommage ! J'espère que ça arrivera bientôt... j'aime pas l'anglais xD !</p>
<p>bonne continuation pour se magnifique logiciel... et comme la question a été posée... à quand digikam 1.0 ?</p>
<p>++<br>
 <a href="http://www.trsohbet.name">sohbet</a><br>
Floréal.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18135"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/376#comment-18135" class="active">I think it should have been</a></h3>    <div class="submitted">Submitted by Halo (not verified) on Sat, 2009-01-10 00:01.</div>
    <div class="content">
     <p>I think it should have been labeled 1.0 a long time ago. It's stable, has tons of features, looks great and, above all, it's fun to use! While version numbers don't matter to *me* at all, I think for a lot of people there's a "psychological barrier" to use an app that is at version 0.1 ...</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
