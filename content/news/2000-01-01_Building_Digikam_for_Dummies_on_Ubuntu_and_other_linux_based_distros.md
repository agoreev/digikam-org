---
date: "2000-01-01T00:00:00Z"
title: "Building Digikam for Dummies on Ubuntu and other linux based distro´s"
author: "Nobody"
description: "Building digiKam for dummies on Ubuntu. Updated according the latest, input, feedback and knowledge about the current process per Sept 17, 2011. The following guide"
category: "news"
aliases: "/node/621"

---

<p>Building digiKam for dummies on Ubuntu.</p>
<p>Updated according the latest, input, feedback and knowledge<br>
about the current process per Sept 17, 2011.</p>
<p>The following guide is intended for Ubuntu users with no<br>
experience in compiling software from source. However,<br>
I provide links to instructions for other platforms than<br>
Ubuntu. </p>
<p>IMPORTANT NOTE:<br>
This guide has been tested for Ubuntu 11.04.</p>
<p>It has been verified that this is not going to work, if you are<br>
using Ubuntu prior to 11.04, due to inavailability of some<br>
required dependencies.</p>
<p>Thanks to Dan McDaniel, Philip Johnsson,<br>
Gert kello, Michael G Hansen, and others for their input<br>
and feedback.</p>
<p>Thanks to Dmitri Popov, who revisted the text entirely to<br>
make it a better reading experience.</p>
<p>Usually, you can easily install digiKam or showFoto via the<br>
Ubuntu Software Center. Alternatively, you can install a<br>
more recent version of the application using Philip<br>
Johnsson´s PPA. You can find instructions on how to add the<br>
PPA to your software sources at<br>
<a href="http://launchpad.net/~philip5/+archive/extra">http://launchpad.net/~philip5/+archive/extra</a><br>
But if you want to compile digiKam from source, you can so<br>
using the tar.gz archive of the latest official digiKam<br>
release or source code from the project's Git repository.</p>
<p>NOTE</p>
<p>Git provides the current development source code. Although<br>
it gives you the latest updates and bug fixes, you may also<br>
have a newly introduced bug, in fact it is for testing<br>
purposes, not officially released yet.</p>
<p>If you run into problems, you can ask for support on the<br>
digikam user mailing list. Before you proceed make sure to<br>
back up your system. I recommend using fsarchiver, but<br>
there are other backup tools at your disposal. </p>
<p>Compiling digiKam requires 4-8 hours depending on your<br>
hardware as well as a fair amount of persistence. However,<br>
if goes smoothly the entire process might be completed<br>
within an hour.</p>
<p>If you find typos and errors, please send the maintainer of<br>
this text a note at sleeplessregulus@hetnet.nl.</p>
<p>Commands to execute in the terminal will be put between []<br>
in this way [command].</p>
<p>At the end of the doc you will find some useful notes by<br>
Michael G. Hansen. They appear to be very promising but<br>
have not yet been tested and confirmed by the author of<br>
this text.</p>
<p>Good luck!<br>
Rinus Bakker</p>
<p>#########################################################<br>
START HERE<br>
#########################################################</p>
<p>First off all, you have to uninstall the current version of<br>
digiKam from your machine. If you want latest digikam from<br>
git skip to the DOWNLOAD FROM GIT section.</p>
<p>##########################################################<br>
DOWNLOAD TARBALL<br>
##########################################################</p>
<p>Navigate to<br>
<a href="http://sourceforge.net/projects/digikam/files/digikam">http://sourceforge.net/projects/digikam/files/digikam</a></p>
<p>and download the latest .tar.gz archive to your home<br>
directory (e.g., /home/username/, where username is your<br>
actual user name). Extract the downloaded archive, launch<br>
Terminal and switch to resulting directory.<br>
(e.g., home/username/digikam-software-compilation).<br>
Once you've done that, go to the COMPILING section.</p>
<p>##########################################################<br>
DOWNLOAD FROM GIT<br>
##########################################################</p>
<p>If Git and Perl are not already installed on your machine,<br>
install them using the following command in the Terminal:<br>
sudo apt-get install git perl</p>
<p>If you are unsure, you may do so anyway, it will return a<br>
message that you have already installed the latest version<br>
of git and perl.</p>
<p>Alternatively you can install it via a package manager like<br>
synaptic.</p>
<p>To download the source code, use the following commands:</p>
<p> [cd ~]<br>
 [git clone git://anongit.kde.org/digikam-software-compilation<br>
digikam-software-compilation].</p>
<p>Switch then to the digikam-software-compilation directory:</p>
<p> [cd digikam-software-compilation]</p>
<p>Issue the [ls] command, and you should see the document<br>
called ¨download-repos¨, this is a runnable PERL script. In<br>
the Terminal, run the [perl download-repos] command to<br>
download the source code from digiKam's Git repository to<br>
the ¨/home/username/digikam-software-compilation¨ directory.</p>
<p>Read the COMPILING section</p>
<p>NOTE:<br>
If you later want to update your build of digiKam with the<br>
latest updates use the<br>
 [perl gits pull] command. This compares the local directory<br>
with the remote Git repository and downloads updates.</p>
<p>##########################################################<br>
COMPILING<br>
##########################################################</p>
<p>Create a directory for build files and switch to it:</p>
<p> [mkdir build]<br>
 [cd build]</p>
<p>The path should look like this:</p>
<p>/home/username/digikam-software-compilation/build</p>
<p>Make sure that the gcc, cpp, make, and cmake are installed<br>
on your machine. If cmake is not installed, use the<br>
 [sudo apt-get install cmake] to install and </p>
<p>Now let cmake do its work:</p>
<p> [cmake -DCMAKE_BUILD_TYPE=debugfull -DCMAKE_INSTALL_PREFIX=<br>
`kde4-config --prefix` ..]</p>
<p>Note the space dot dot at the end and make sure NOT to use<br>
sudo in front of cmake. Most likely cmake will complain<br>
about missing stuff like  libkexiv2-dev, libkipi-dev,<br>
libkdcraw-dev, etc. Install the missing packages and run<br>
cmake again.</p>
<p>If cmake has finished without errors, run the [make] command<br>
(you can install make with [sudo apt-get install make]<br>
command).</p>
<p>To install the freshly build digikam on your machine use the<br>
 [sudo make install] command.</p>
<p>In case you want to uninstall, you can use [sudo make<br>
uninstall] from your digikam build folder.</p>
<p>##########################################################<br>
DONE!<br>
##########################################################</p>
<p>You have just build a copy of digiKam with full debug<br>
information. That gives you an opportunity to contribute to<br>
the ongoing improvement of digikam by providing the<br>
developers with useful information. You can read more about<br>
contributing to digiKam at <a href="http://www.digikam.org/drupal/contrib">http://www.digikam.org/drupal/contrib.</a></p>
<p>##########################################################<br>
USEFUL LINKS FOR COMPILING DIGIKAM ON OTHER PLATFORMS<br>
##########################################################</p>
<p>For Fedora users</p>
<p>Marie-Noëlle Augendre´s wiki:</p>
<p><a href="http://www.webmaster-en-herbe.net/wiki/doku.php?id=wiki:configuration-2:digikam">in French</a></p>
<p><a href="http://www.webmaster-en-herbe.net/wiki/doku.php?id=wiki:configuration-2:digikam-en">in English</a></p>
<p>For openSUSE users</p>
<p><a href="http://dodin.org/wiki/pmwiki.php?n=Doc.CompilingDigiKam">Jean-Daniel Dodin´s page</a></p>
<p>###########################################################<br>
NOTES by Michael G. Hansen<br>
###########################################################</p>
<p>On installing dependencies:</p>
<p>You can do a "sudo apt-get build-dep digikam kipi-plugins"<br>
to get some of the dependencies installed in one step. This<br>
will install all the dependencies for building the version<br>
of digikam and kipi-plugins which is in the ubuntu repository,<br>
that's why some packages may still have to be installed for<br>
newer versions, but it should give you a good starting point.</p>
<p>On speeding up the proces:</p>
<p>If you have a dual-core computer, you can run "make -j 2" to<br>
have "make" use two processes at once to speed things up and<br>
take full advantage of your multi-core system. If you have<br>
more cores, simply change "2" to the number of cores. I<br>
normally use a number twice as high as the number of cores,<br>
to account for delay due to disk access. Note that if you<br>
encounter a build failure, error messages may be out-of-order<br>
on the screen, re-run "make" without "-j" to get well-readable<br>
output of the error messages.</p>
<p>On installation and uninstallation:</p>
<p>To make un-installation easier, especially if you delete the<br>
build directory in between, use checkinstall [sudo apt-get<br>
install checkinstall]</p>
<p> [sudo checkinstall --pkgname=my-digkam make install]</p>
<p>This will create a .deb package containing your self-built<br>
digikam, which you can uninstall using "sudo apt-get remove<br>
my-digikam". Another advantage is that you can keep several<br>
versions of compiled digikam in packages, in case you<br>
encounter a problem with a newer build.</p>
<p>###########################################################<br>
 END notes by Michael G. Hansen<br>
###########################################################</p>

<div class="legacy-comments">

  <a id="comment-19991"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-19991" class="active">Tutorial for breaking your Ubuntu packaging system</a></h3>    <div class="submitted">Submitted by <a href="http://vlado-do.de" rel="nofollow">Vlado Plaga</a> (not verified) on Sun, 2011-08-28 18:51.</div>
    <div class="content">
     <p>Ok, my headline may sound a bit drastic, but I don't think it's a good idea to just copy things into your /usr/ directory (except for /usr/local of course). This is especially true for people who don't know what they're doing - apparently the target audience of this tutorial.</p>
<p>Don't misunderstand me: I like the idea of an easy-to-follow tutorial for installing an up-to-date version of digiKam!</p>
<p>I thought if someone could merge Michael G. Hansen's notes into the text it would be a lot better, but apparently even <em>checkinstall</em> does not avoid having to mess with the package management. At least that's what I got:</p>
<p><code><br>
dpkg: error processing /usr/src/digikam2/digikam-2.0.0/build/my-digkam_20110828-1_powerpc.deb (--install):<br>
 trying to overwrite '/usr/include/libksane/ksane.h', which is also in package libksane-dev 4:4.6.2-0ubuntu1<br>
</code></p>
<p>Other conflicting packages where:<br>
<code><br>
libksane-dev libkdcraw-20-dev libkface-dev libkexiv2-10-dev libkgeomap1-data kdegraphics-libs-data<br>
</code></p>
<p>First I tried to deinstall them, but when it came to kdegraphics-libs-data with various other dependecies I gave that up and just installed with [dpkg -i --force-overwrite my-digkam_20110828-1_powerpc.deb].</p>
<p>Maybe some of my problems only came from me having used a custom digiKam package before, or from having forgotten to uninstall digikam2-data. Anyway I recommend using proper Debian packages, like the ones from Philips Johnsson´s PPA. Otherwise the next distribution update will most like not work flawlessly.</p>
<p>P.S.: Why do I have to enter this difficult CAPTCHA for the preview and for the post?t</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19994"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-19994" class="active">Building Digikam for Dummies on Ubuntu and other linux based dis</a></h3>    <div class="submitted">Submitted by sleepless on Wed, 2011-08-31 09:12.</div>
    <div class="content">
     <p>Vlada,</p>
<p>Thanks for trying to help to make the best possible guide in the world.<br>
Please keep sharing your experiences.</p>
<p>If you want to install the newest you have to update your system accordingly.<br>
I did not find it very hard to make the required updates. It is always possible to run in to an unsolvable conflict, than you will have to choose if you want to dispose some other package that comes in the way, or just keep running digikam from official repository. It is all up to you.</p>
<p>A .deb package does not garantee that you will not run in to a conflict.</p>
<p>Whit my next build I will dig into Michael G. Hansen's notes and will integrate my findings in the text. (I made them available already for who wants to use them.</p>
<p>As you said:<br>
¨Maybe some of my problems only came from me having used a custom digiKam package before, or from having forgotten to uninstall digikam2-data.¨ </p>
<p>The instructions stated clearly to remove former installations.</p>
<p>Rinus</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19992"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-19992" class="active">CPack</a></h3>    <div class="submitted">Submitted by yegorich (not verified) on Mon, 2011-08-29 08:17.</div>
    <div class="content">
     <p>What about introducing CPack support to create *.deb files? AFAIK it is still not implemented or am I missing something?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19995"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-19995" class="active">CPack</a></h3>    <div class="submitted">Submitted by sleepless on Wed, 2011-08-31 09:18.</div>
    <div class="content">
     <p>You could make your remark more helpful by elaborating about it a bit more.<br>
What is CPack, what makes you want it, what are the pro´s and con´s, how should it be implemented and so on.<br>
Rinus</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20010"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20010" class="active">Compilation of DigiKam</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-09-11 09:32.</div>
    <div class="content">
     <p>Compilation of DigiKam somehow does not work.<br>
I am Dummie. I have a distribution based on Linux - Debian. I proceed according to instructions, but always ends with compilation errors. I installed lots of packages cmake required. It now claims that the compilation is possible. But it is not. Still errors.<br>
I cannot spend rest of my life compiling Digikam and learning all the programming tools used in it.<br>
I wonder how many Dummies completed the compilation.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20012"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20012" class="active">installation</a></h3>    <div class="submitted">Submitted by linedubeth (not verified) on Sun, 2011-09-11 10:15.</div>
    <div class="content">
     <p>I would install digikam2 but it wouldn't </p>
<p>with ppa i have the same version<br>
and with compilation i have errors</p>
<p>Could you help me?</p>
<p>i use ubuntu 10.04.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20015"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20015" class="active">My PPA only work with</a></h3>    <div class="submitted">Submitted by <a href="https://launchpad.net/~philip5" rel="nofollow">Philip Johnsson</a> (not verified) on Sun, 2011-09-11 16:08.</div>
    <div class="content">
     <p>My PPA only work with (*)ubuntu 11.04 (natty) as Digikam 2.x need a newer version of KDE libraries and framework than you have in 10.04. The Kubuntu team have dropped KDE 4.6.x support for Ubuntu 10.10 too so you will only be able to use Digikam 2.x with Ubuntu 11.04 or newer (or somehow get/build KDE 4.6.x or newer on an older Ubuntu release).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20041"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20041" class="active">compiling with non-dynamic libraries</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-10-04 17:04.</div>
    <div class="content">
     <p>This will probably demonstrate my ignorance and people will start falling about laughing but....</p>
<p>For those of us who want to stick with 10.04 LTS (what does LTS stand for... cant remember....) but also want a newer Digikam would it be possible for a cleverer person than me to compile a version of Digikam that did not use dynamically linked libraries, so that all the needed libraries were hard wired in.  Big executable I know but workable?</p>
<p>Probably a silly idea.  You can start laughing now.</p>
<p>S</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20051"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20051" class="active">nothing to laugh about</a></h3>    <div class="submitted">Submitted by sleepless on Wed, 2011-10-05 20:12.</div>
    <div class="content">
     <p>Big Mike quoting Edmonton on http://www.thephotoforum.com:<br>
If you have an apple and I have an apple and we exchange apples then you and I will still each have one apple. But if you have an idea and I have an idea and we exchange these ideas, then each of us will have two ideas.<br>
Rinus</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20016"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/621#comment-20016" class="active">Users mailing list</a></h3>    <div class="submitted">Submitted by sleepless on Sun, 2011-09-11 16:21.</div>
    <div class="content">
     <p>Would you please sign up to the users mailing list, and put your questions and requests there. That gives better chance for support.</p>
<p>Rinus</p>
         </div>
    <div class="links">» </div>
  </div>

</div>