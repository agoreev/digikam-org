---
date: "2007-05-07T16:52:00Z"
title: "digiKam has a new Light Table..."
author: "digiKam"
description: "Today, after 1 week of developement, I have finished the first implementation of the new Light Table tool for digiKam. This is a first implementation,"
category: "news"
aliases: "/node/221"

---

<p>Today, after 1 week of developement, I have finished the first implementation of the new Light Table tool for digiKam.</p>
<p>This is a first implementation, certainly uncomplete but suitable as well. I have follow the user tips from this <a href="http://bugs.kde.org/show_bug.cgi?id=135048">bugzilla entry</a>.</p>
<p>Some screenshots of this tool in action can be seen following these links:</p>
<p>- <a href="http://digikam3rdparty.free.fr/Screenshots/lighttable_v2.png">The Light Table with thumbbar pop-up menu</a>.</p>
<p>- <a href="http://digikam3rdparty.free.fr/Screenshots/lighttable_v3.png">How to Insert new item to Light Table using Album GUI pop-up menu.i Left and right side bar enable</a>.</p>
<p>- <a href="http://digikam3rdparty.free.fr/Screenshots/lighttable_v4.png">The Light Table preview pop-up menu</a>.</p>
<p>- <a href="http://digikam3rdparty.free.fr/Screenshots/lighttable_v5.png">The Light Table thumbbar tool tip and left side bar enable</a>.</p>
<p>This version of Light Table support drag &amp; drop from album gui and zooming/panning on left and right preview panel. You can compare easily two images like this.</p>
<p>I hope this new tool will enjoy digiKam users. It will be released with digiKam 0.9.2 final planed for june.</p>

<div class="legacy-comments">

</div>