---
date: "2007-12-22T19:18:00Z"
title: "digiKam 0.9.3 release (final)"
author: "gerhard"
description: "Dear all digiKam users, the team is really proud to deliver just in time for Xmas the final. The tarball can be downloaded from SourceForge."
category: "news"
aliases: "/node/280"

---

<p>Dear all digiKam users,<br>
the team is really proud to deliver just in time for Xmas the final. The tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>. Don't forget to update Exiv2 to last stable release 0.15 for digiKam to compile successfully.</p>
<p>Many, many thanks go to Gilles, Marcel, Arnd, Mik, Fabien and all the others who worked hard to create this great new release. This release sends a big parcel of useability improvements under your Xmas tree: If it was not already, organizing and navigation have become a snap now with the quick filters, drag&amp;drop and GOTO features on top of an optimized lightable.</p>
<p><b>digiKam 0.9.3 - Release date: 2007-12-23</b><br>
Below I've summarized all new features and changes since the last stable version 0.9.2 to mark the progress.</p>
<p>NEW FEATURES (since 0.9.2):</p>
<p><u>AlbumGUI</u><br>
<b>Drag&amp;Drop</b> :<br> D&amp;D from anywhere into album tree. When the albumview is flattened (x Include Album Sub-tree) images can be d&amp;ded across horizontal separations. That one satisfies wish #142774.</p>
<p><b>GOTO</b> :<br> A new quick navigation feature has been added: it is now possible (by RMB context menu) to GOTO any other view with pertinent context, e.g. from date view right-click on an image and GOTO the Album or Tag view that this image is associated with. Try any other combination. That covers wish #128231.<br>
<b>Quick filters</b> :<br> Add new Filter Bar on bottom of Albums/Tags/Searches/TagsFilter view filter contents based on strings (file names, comments, and tags).<br>
Add Rating filter widget on status bar.<br>
Add MimeType filter widget on status bar.<br>
Add tool to navigate from album, tag and date view to any other view.<br>
Always force to show mediaplayer widget to preview video in embeded mode.</p>
<p><u>ImageEditor</u><br>
B&amp;W            : New black &amp; white Green color tone filter.</p>
<p><u>LightTable</u>     : Several usability improvements.</p>
<p><u>CameraGUI</u><br>
<b>streamlining</b>: New options to download images and delete it from camera at the same time.<br>
<b>Drag&amp;Drop</b>          : Support of Drag &amp; Drop to download files from camera GUI to album GUI.<br>
<b>space indicator</b>     : Add new bargraph to indicate statistics about free space available on Album library path.</p>
<p><u>General</u> :<br> Updated internal CImg library to last stable 1.2.4 (released at 2007/09/26).<br>
Color scheme theme are now XML based (instead X11 format).<br>
Camera interface is used to import new images into collections.</p>
<p><u>Bugfixes (B.K.O):</u><br>
001 ==&gt; 120450 : Strange helper lines behavior in ratio crop tool.<br>
002 ==&gt; 147248 : Image info doesn't follow image in the editor.<br>
003 ==&gt; 147147 : digiKam does not apply tags to some selected images.<br>
004 ==&gt; 143200 : Renaming like crasy with F2 crash digiKam.<br>
005 ==&gt; 147347 : Showfoto crashes when opening twice multiple files.<br>
006 ==&gt; 147269 : digiKam finds but fails to use libkdcraw.<br>
007 ==&gt; 147263 : Some icons are missing.<br>
008 ==&gt; 146636 : Adjust levels: helper line for sliders.<br>
009 ==&gt; 147671 : Portability problem in configure script.<br>
010 ==&gt; 147670 : Compilation problems on NetBSD in greycstoration.<br>
011 ==&gt; 147362 : Tool-tip for zoom indicator is below the screen if window is maximised.<br>
012 ==&gt; 145017 : Deleting an image from within digiKam does not update the digiKam database.<br>
013 ==&gt; 148925 : Light Table thumb bar not updated when deleting images.<br>
014 ==&gt; 148971 : Awful menu entry "digiKamThemeDesigner".<br>
015 ==&gt; 148930 : digiKam-0.9.2 does not compile with lcms-1.17.<br>
016 ==&gt; 141774 : Autorotate does not work however kipi rotate works.<br>
017 ==&gt; 103350 : Original image is silently overwritten when saving.<br>
018 ==&gt; 144431 : Add option in Camera Download Dialog.<br>
019 ==&gt; 131407 : Use camera GUI also for import of images from different locations.<br>
020 ==&gt; 143934 : Cannot download all photos from camera.<br>
021 ==&gt; 147119 : Can't link against static libjasper, configure reports jasper is missing.<br>
022 ==&gt; 147439 : It is too easy to delete a search.<br>
023 ==&gt; 147687 : Error when downloading and converting images.<br>
024 ==&gt; 137590 : Be able to modifiy the extension of images in the interface.<br>
025 ==&gt; 139024 : Camera GUI new items selection doesn't work.<br>
026 ==&gt; 139519 : digiKam silently fails to import when out of disc space.<br>
027 ==&gt; 149469 : Excessive trash confirmation dialogs after album is deleted.<br>
028 ==&gt; 148648 : Color managed previews not working in all plugins.<br>
029 ==&gt; 126427 : In "rename file" dialog, the 2nd picture is (and can't) not displayed.<br>
030 ==&gt; 144336 : Selecting pictures on the camera lets the scroll-bar jump back to<br>
                 the top of the list.<br>
031 ==&gt; 136927 : Failed to download file DCP_4321.jpg. Do you want to continue?<br>
                 and when continue is clicked the same warning comes for the next image and so on.<br>
032 ==&gt; 146083 : Bugs in drag and drop.<br>
033 ==&gt; 147854 : Put images into an emptied light-table.<br>
034 ==&gt; 149578 : libjpeg JPEG subsampling setting is not user-controlable.<br>
035 ==&gt; 149685 : Go to next photo after current photo deletion (vs. to previous photo).<br>
036 ==&gt; 148233 : Adding texture generates black image.<br>
037 ==&gt; 147311 : Ligth Table RAW images does not rotate as def. by exif info.<br>
038 ==&gt; 146773 : Metadata sources preference when sorting images.<br>
039 ==&gt; 140133 : Metadata Edit Picture Comments Syncs to IPTC and EXIF when option is de-selected.<br>
040 ==&gt; 147533 : New rating filter for the statusbar.<br>
041 ==&gt; 131963 : Add a file type filter tab to the right.<br>
042 ==&gt; 148993 : Filter images by rating in album view.<br>
043 ==&gt; 151357 : Ratings can exceed 5 stars.<br>
044 ==&gt; 144815 : Scroll left-pane to the selected album/date on start-up.<br>
045 ==&gt;  96894 : Easier navigation between albums, tags and collections.<br>
046 ==&gt; 147426 : Search for non-voted pics.<br>
047 ==&gt; 150296 : Images in LightTable are not compared in right order<br>
048 ==&gt; 151956 : Red eye enhance feature no longer works.<br>
049 ==&gt; 110136 : Filter textbox in album tree like in amaroks collection list.<br>
050 ==&gt; 152192 : Resize really bad qualitatively.<br>
051 ==&gt; 150801 : Thumbnail and image view does not update after editing image.<br>
052 ==&gt; 152522 : Support for *.3gp and *.mp4 video files in digiKam (already supported by KDE video players).<br>
053 ==&gt; 128231 : A way to view pictures recursively in albums and sub-albums at the same time would be cool.<br>
054 ==&gt; 133191 : Quick search/filter for albums like in amarok.<br>
055 ==&gt; 146364 : Incremental search for tags in all tag fields.<br>
056 ==&gt; 152843 : Live filter does not work if no text is displayed under thumbnails.<br>
057 ==&gt; 148629 : "Adjust Exif orientation tag" does not continue after error.<br>
058 ==&gt; 144165 : Numeric values in Histogram -&gt; Statistics frame should be right-aligned.<br>
059 ==&gt; 148037 : Tiff images are not displayed with correct colours.<br>
060 ==&gt; 152961 : Cannot remove album from tree.<br>
061 ==&gt; 121314 : More options to display the album tree.<br>
062 ==&gt; 141085 : Some strings are untranslated in digiKam.<br>
063 ==&gt; 151403 : Crashed trying to save a image downloaded from the camera.<br>
064 ==&gt; 148772 : Histogram should refresh after editor save.</p>

<div class="legacy-comments">

  <a id="comment-17918"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/280#comment-17918" class="active">good working .thanks  sohbet</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:11.</div>
    <div class="content">
     <p>good working .thanks  <a href="http://www.trsohbet.name">sohbet</a></p>
         </div>
    <div class="links">» </div>
  </div>

</div>