---
date: "2006-12-04T15:41:00Z"
title: "Kipi-plugins 0.1.3-beta1 released"
author: "digiKam"
description: "The kipi-plugins team is proud to announce the first beta release of kipi-plugins 0.1.3 and libkipi 0.1.5. The kipi-plugins 0.1.3 series has been under heavy"
category: "news"
aliases: "/node/198"

---

<p>The kipi-plugins team is proud to announce the first beta release of kipi-plugins 0.1.3 and libkipi 0.1.5.</p>
<p>The kipi-plugins 0.1.3 series has been under heavy development since November 2005 and offers you the opportunity to test and enjoy a variety of new features, additions and improvements.</p>
<p>The source for kipi-plugins and libkipi is available <a href="http://sourceforge.net/project/showfiles.php?group_id=149779">at this url</a></p>
<p>New features and bug fixes since kipi-plugins 0.1.2 are listed below:</p>
<p>Kipi-plugins NEW FEATURES</p>
<p>New Plugin    : MetadataEdit : New kipi plugin to edit EXIF and IPTC pictures metadata.<br>
New Plugin    : GPSSync      : New kipi plugin to sync photo metadata with a GPS device.<br>
New Plugin    : IpodExport   : New kipi plugin to export pictures to an ipod device (by Seb Ruiz).</p>
<p>GalleryExport : Support for multiple galleries.</p>
<p>HTMLExport    : New "s0" theme from Petr Vanek</p>
<p>JPEGLossLess  : Removed libmagic++ depency.<br>
JPEGLossLess  : Removed libkexif depency. Using Exiv2 instead.</p>
<p>RAWConverter  : New core to be compatible with recent dcraw release. A lot<br>
                of RAW decoding settings have been added.<br>
RAWConverter  : Embedding ouput color space in target image (JPEG/PNG/TIFF).<br>
RAWConverter  : Metadata preservation in target image during Raw conversion (JPEG/PNG).<br>
RAWConverter  : Removing external dcraw depency. Now plugin include a full supported version of<br>
                dcraw program in core.<br>
RAWConverter  : updated dcraw.c implementation to version 8.41.</p>
<p>SendImages    : Added image size limit x mail (Michael Hï¿½hstetter)</p>
<p>TimeAdjust    : Removed libkexif depency. Using Exiv2 instead.<br>
TimeAdjust    : New option to customize Date and Time to a specific timestamp.<br>
TimeAdjust    : New options sync EXIF/IPTC Creation Date with timestamp.</p>
<p>Kipi-plugins BUGFIX from B.K.O (http://bugs.kde.org):</p>
<p>001 ==&gt; 127101 : BatchProcess : expand sequence number start value in batch rename images.<br>
002 ==&gt;  94494 : GalleryExport: support for multiple galleries.<br>
003 ==&gt; 128394 : RAWCanverter : convertion of RAW files fails with dcraw 8.21<br>
004 ==&gt; 132659 : FlickrExport : "Missing signature" - Flickr API changed and upload of<br>
                                images is no longer possible.<br>
005 ==&gt; 107905 : RAWConverter : copy exif data from raw to converted images.<br>
006 ==&gt; 119537 : JPEGLossLess : Exif width and height are not corrected after lossless rotation.<br>
007 ==&gt; 91545  : Slideshow    : plugin does nothing if an album only contains subalbums, but no<br>
                                images directly or is empty.<br>
008 ==&gt; 134749 : GPS Sync     : altitude shown is wrong.<br>
009 ==&gt; 134298 : SimpleViewer : save settings / keep settings missing!<br>
010 ==&gt; 134747 : GPS Sync     : not optimal correlation.<br>
011 ==&gt; 135157 : GPS Sync     : warning about changes not applied always appear even when already applied.<br>
012 ==&gt; 135237 : GPS Sync     : filenames with multiple periods in them do not show up in the file<br>
                                listing (incorrect extension identification).<br>
013 ==&gt; 135484 : GPS Sync     : thumbnail generation for multible images can cause severe overload.<br>
014 ==&gt; 135353 : GPS Sync     : the name of the plugin is missleading.<br>
015 ==&gt; 136257 : MetadataEdit : Editing the EXIF-data overwrites all the data for selected files.<br>
016 ==&gt; 128341 : HTMLExport   : kipi html export should not resize images if "resize target images"<br>
                                is not checked.<br>
017 ==&gt; 127476 : PrintWizzard : Printing as very very slow (added a workaround running kjobviewer)<br>
018 ==&gt; 136941 : BatchProcess : graphical picture ordering and renaming.<br>
019 ==&gt; 136855 : MetadataEdit : Editing metadata on a few selected imagefiles and clicking forward<br>
                                or apply crashes digiKam.<br>
020 ==&gt; 135408 : BatchProcess : Window does not fit on screen.<br>
021 ==&gt; 117399 : BatchProcess : Usability of Target folder.<br>
022 ==&gt; 137921 : MetadataEdit : wrong country code in IPTC.</p>

<div class="legacy-comments">

</div>