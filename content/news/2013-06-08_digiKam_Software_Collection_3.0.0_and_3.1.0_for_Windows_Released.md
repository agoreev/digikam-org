---
date: "2013-06-08T16:45:00Z"
title: "digiKam Software Collection 3.0.0 and 3.1.0 for Windows Released"
author: "Ananta Palani"
description: "At long last I am pleased to announce the simultaneous release of digiKam 3.0.0 and 3.1.0 for Windows built against KDE 4.10.2. If you have"
category: "news"
aliases: "/node/696"

---

<a href="http://www.flickr.com/photos/digikam/8894333989/" title="digiKam3.3.0-windows by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8132/8894333989_898f22ee37_z.jpg" width="640" height="360" alt="digiKam3.3.0-windows"></a>

<p>At long last I am pleased to announce the simultaneous release of digiKam 3.0.0 and 3.1.0 for Windows built against KDE 4.10.2. If you have a short attention span and want to download it immediately see the attached link.</p>

<p>There were numerous bugs I had to fix in digiKam and KDE before release, and fixing them took more time than expected. Extremely sorry for the delay! Rest assured, it was worth it thanks to a major enhancement on the Windows side of things: a new 'solid' interface thanks to the KDE windows team which will drastically speed up time before album display when switching between albums and when viewing digiKam after some minutes away.</p>

<p>Bugs fixed for Windows include:</p>

<p>1. Inability to use UUIDs for volumeids on Windows</p>
<p>2. Loss of collection during upgrade for certain collection paths</p>
<p>3. JPEG rotation from album viewer failed to work correctly and often crashed (for some types of collections you will need to press F5 to refresh the thumbnails, I will try to fix this for 3.2.0 or 3.3.0)</p>
<p>4. Temporary thumbnails are were not cleaned up correctly</p>
<p>5. File paths were not displayed in native Windows format (i.e. a backslash - if you see any paths that are not in the correct format please file a bug report or reply to this)</p>
<p>6. KDE did not handles mixed case files on Windows in most cases correctly (i.e. you could not save mixed case files properly and filename comparison did not always work properly - there may be some lingering places where it does not)</p>
<p>7. Misc. compile time fixes and code clean-up</p>

<p>Known problems</p>

<p>1. After rotating an image using the rotate icons in the thumbnail view, the user may have to press F5 to see the change</p>
<p>2. Bugs listed on <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;list_id=676982&amp;op_sys=MS%20Windows&amp;product=digikam&amp;product=digikamimageplugins&amp;product=showfoto&amp;query_format=advanced&amp;query_based_on=&amp;columnlist=bug_severity%2Cpriority%2Copendate%2Cbug_status%2Cresolution%2Ccomponent%2Cop_sys%2Crep_platform%2Cshort_desc">KDE Bugzilla</a></p>

<p></p>Regardless, without further ado I give you Digikam 3.0.0:<p></p>

<p><a href="http://download.kde.org/stable/digikam/digiKam-installer-3.0.0-win32.exe">http://download.kde.org/stable/digikam/digiKam-installer-3.0.0-win32.exe</a></p>

<p>and digiKam 3.1.0:</p>

<p><a href="http://download.kde.org/stable/digikam/digiKam-installer-3.1.0-win32.exe">http://download.kde.org/stable/digikam/digiKam-installer-3.1.0-win32.exe</a></p>

<p>Please let me know if you encounter any problems or file a bug report.</p>

<p>Enjoy!</p>

<p>P.S. I hope to release 3.2.0 in the next week or two.</p>
<div class="legacy-comments">

  <a id="comment-20557"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20557" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by Carl (not verified) on Sat, 2013-06-08 20:06.</div>
    <div class="content">
     <p>Hello Ananta!</p>
<p>I haven't tried it out yet, but thank you very much for all your effort! I'm excited to try the Windows version soon (especially of the coming version with its facial recognition feature)! </p>
<p>/Carl</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20560"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20560" class="active">You're welcome!</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2013-06-10 15:35.</div>
    <div class="content">
     <p>I hope it works well for you. I'm looking forward to the face recognition feature as well :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20558"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20558" class="active">THANKS</a></h3>    <div class="submitted">Submitted by chrlau5 (not verified) on Sun, 2013-06-09 08:55.</div>
    <div class="content">
     <p>Thank you sp much I have waited for this so I can get started with sorting my Photo collection!!!! I´ll definetly donate if this time it doe not crash before getting to sort photos :-).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20559"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20559" class="active">You're welcome</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2013-06-10 15:34.</div>
    <div class="content">
     <p>And if it does crash, let us know so we can fix it this time!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20561"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20561" class="active">Digikam 3.1.0 win. Thumbnails</a></h3>    <div class="submitted">Submitted by HansMagnum (not verified) on Fri, 2013-06-14 16:28.</div>
    <div class="content">
     <p>Digikam 3.1.0 win. Thumbnails disappear after changing to other album view. Refresh (F5) does not help. Restart helps sometimes to show thumbnails at first, but error will pop up again.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20570"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20570" class="active">What error?</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2013-06-25 15:36.</div>
    <div class="content">
     <p>Do you see an actual error message pop up or do you mean that the problem shows back up again? If you wait a few minutes will the images finally show back up? And just to confirum, you downloaded and installed using the link at the top of this post? I ask because the effect you are seeing was one of the things I fixed. It could be that you have a device attached that is causing problems for solid to list. Would you mind <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.1.0&amp;format=guided">filing a bug report</a> and specify the problem you encountered and what drive devices you have attached to your system (ex. x-number of hard drives, cd-rom drive, dvd-drive, camera, flash, etc).   </p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20575"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20575" class="active">No error message - just a problem</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-07-09 14:47.</div>
    <div class="content">
     <p>I'm seeing this error on KUbuntu 13.04 running digiKam 3.1.0.  I have an album with 265 photos.  It shows 265 photos.  Then if I select them all, I only have 186.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20579"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20579" class="active">Sounds like a different issue</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-07-13 00:55.</div>
    <div class="content">
     <p>I'm pretty sure this is a different issue. You're talking about selecting all images in a folder showing only a fraction of the expected images selected, whereas original poster is talking about all images disappearing when switching albums.</p>
<p>Have you tries selecting ranges of photos or shift-clicking multiple ranges to see if you can select more than 186?</p>
<p>Either way, please file a bug report.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20562"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20562" class="active">In-painting crashes Digikam 3.0.0 and 3.1.0</a></h3>    <div class="submitted">Submitted by Jacal (not verified) on Sun, 2013-06-16 13:07.</div>
    <div class="content">
     <p>Digikam 3.0.0 and 3.1.0 on Windows 7/64-bit crashes whenever I try to use In-painting. Tested on two computers.</p>
<p>Thank you for all your work!</p>
<p>Jacal</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20563"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20563" class="active">Noticed the same problem with</a></h3>    <div class="submitted">Submitted by -OTSO- (not verified) on Mon, 2013-06-17 17:35.</div>
    <div class="content">
     <p>Noticed the same problem with 32 bit Win Vista Prof.  Another computer run with 64 bit Win 7.</p>
<p>After all, fantastic program and grate work!</p>
<p>P.S.  How about of importing pictures direct from camera?</p>
<p>-OTSO-</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20577"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20577" class="active">Fixed in next Windows release (and 3.3.0 codebase)</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-07-13 00:49.</div>
    <div class="content">
     <p>Took me a bit to find the cause of this, but was a thread leak + a config issue. Fixed with this commit:</p>
<p>   <a href="http://commits.kde.org/digikam/421b387878e84d272f63dc12d67bef37d5eeb0cf">http://commits.kde.org/digikam/421b387878e84d272f63dc12d67bef37d5eeb0cf</a></p>
<p>It's in the 3.3.0 codebase, but I will include it in 3.2.0 release that I'm going to do shortly. This bug affected digiKam on all platforms.</p>
<p>Thanks for your patience!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20571"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20571" class="active">Please file bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2013-06-25 15:38.</div>
    <div class="content">
     <p>Would you please <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.1.0&amp;format=guided">file a bug report</a> and list the steps to reproduce the error? Also, just to confirm, you tried the installer listed in this post?</p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20564"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20564" class="active"> geolocation edit bookmarks.....NOTHING happens</a></h3>    <div class="submitted">Submitted by JJL (not verified) on Wed, 2013-06-19 09:03.</div>
    <div class="content">
     <p>Hi!<br>
Great program, i will forget picasa, geosetter, lightroom...<br>
BUT:<br>
picture / geolocation / bookmarks / edit bookmarks....</p>
<p>nothing happens. There is no error, no program crash.<br>
I can switch between "google maps" and "marble virtual globe", the maps are ok,<br>
i can add bookmarks, geo-locations, new bookmark folder....<br>
only the right-mouse selection "edit bookmarks" doesn't work. no reaction.<br>
One month ago i installed digikam for the firts time (Version 2.9),<br>
it was the same error on two different win 7 computers.<br>
thanks<br>
Jürgen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20572"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20572" class="active">Please file a bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2013-06-25 15:42.</div>
    <div class="content">
     <p>Sorry you encountered the error! Would you please <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.1.0&amp;format=guided">file a bug report</a>? I'll try to fix for the next version.</p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20573"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20573" class="active"> geolocation edit bookmarks.....NOTHING happens</a></h3>    <div class="submitted">Submitted by jjl (not verified) on Thu, 2013-06-27 08:34.</div>
    <div class="content">
     <p>Hi!<br>
It's Bug 321274</p>
<p>Thank you!<br>
Jürgen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20578"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/696#comment-20578" class="active">Fixed in next Windows release</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-07-13 00:52.</div>
    <div class="content">
     <p>As I state in <a href="https://bugs.kde.org/show_bug.cgi?id=321274">bug 321274</a> keditbookmarks is in kde-baseapps and I wasn't building kde-baseapps. Will be included in next release. Sorry about that!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
