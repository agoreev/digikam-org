---
date: "2009-05-25T21:35:00Z"
title: "digiKam digest - 2009-05-24"
author: "Anonymous"
description: "In this week there was mostly continuation of last week work: - New expander widget in all plugins and whole interface - fixing of memory"
category: "news"
aliases: "/node/451"

---

<p>In this week there was mostly continuation of last week work:</p>
<p>- New expander widget in all plugins and whole interface<br>
<a href="http://www.flickr.com/photos/mikmach/3564550026/" title="liquid by mikmach, on Flickr"><img src="http://farm4.static.flickr.com/3614/3564550026_ceea7474d3.jpg" width="500" height="386" alt="liquid"></a><br>
- fixing of memory leaks<br>
- making MSVC happy (many thanks to developers working on MS-Windows<br>
  who are sending patches!)<br>
- preparation for libkdcraw 1.0<br>
- polishing and cleaning of code<br>
- making sure that MS-Windows users will get warm welcome (fixing issues<br>
  with welcome page in good shape)<br>
<a href="http://www.flickr.com/photos/mikmach/3564550012/" title="welcome by mikmach, on Flickr"><img src="http://farm3.static.flickr.com/2440/3564550012_42d26a82be.jpg" width="500" height="386" alt="welcome"></a><br>
- hard work on bug fixing and optimizations in new Model View Album GUI<br>
- option for better control what images should be scanned for creation<br>
  of thumbnails<br>
<a href="http://www.flickr.com/photos/mikmach/3564550022/" title="thumbs by mikmach, on Flickr"><img src="http://farm4.static.flickr.com/3404/3564550022_c4009bf084.jpg" width="500" height="400" alt="thumbs"></a><br>
- proof-reading</p>
<p><code><br>
------------------------------------------------------------------------</code></p>
<p>Bug/wish count</p>
<p> digikam          229   +18     -13     5       268     +7      -1      6<br>
 kipiplugins      103   +6      -2      4       138     +1      -2      -1</p>
<p>[1] Opened bugs<br>
[2] Opened last week<br>
[3] Closed last week<br>
[4] Change<br>
[5] Opened wishes<br>
[6] Opened last week<br>
[7] Closed last week<br>
[8] Change</p>
<p>Full tables:<br>
<a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a><br>
<a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969414 by jnarboux:</p>
<p>Use new DExpanderBox widget.</p>
<p>CCBUGS: 149485</p>
<p> M  +81 -58    contentawareresizetool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969370 by cgilles:</p>
<p>fix memory leak : use KUrl::List instead KUrl::List*<br>
signals =&gt; Q_SIGNALS<br>
slots =&gt; Q_SLOTS</p>
<p> M  +10 -5     advanceddialog.h<br>
 M  +1 -1      captiondialog.h<br>
 M  +23 -21    common.cpp<br>
 M  +3 -2      common.h<br>
 M  +1 -1      imageloadthread.h<br>
 M  +10 -8     listimageitems.h<br>
 M  +9 -3      listsounditems.h<br>
 M  +3 -7      maindialog.cpp<br>
 M  +14 -7     maindialog.h<br>
 M  +22 -14    playbackwidget.h<br>
 M  +6 -7      plugin_advancedslideshow.cpp<br>
 M  +3 -3      plugin_advancedslideshow.h<br>
 M  +2 -2      slideplaybackwidget.h<br>
 M  +2 -2      slideshow.h<br>
 M  +1 -4      slideshowconfig.cpp<br>
 M  +10 -4     slideshowconfig.h<br>
 M  +4 -1      slideshowgl.h<br>
 M  +2 -1      slideshowkb.h<br>
 M  +10 -3     soundtrackdialog.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969434 by jnarboux:</p>
<p>Add help for side switch frequency.</p>
<p>CCBUGS: 149485</p>
<p> M  +1 -1      contentawareresizetool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969602 by cgilles:</p>
<p>prepare digiKam to use future libkdcraw 1.0.0 with RExpanderBox (KDE 4.4)</p>
<p> M  +8 -0      rawsettingsbox.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969578 by gateau:</p>
<p>Start with a reasonable size</p>
<p> M  +5 -0      batchprocessimagesdialog.cpp<br>
 M  +1 -0      batchprocessimagesdialog.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969420 by cgilles:</p>
<p>GPSSync kipi-plugins : apply patch #33449 from Michael G. Hansen to copy<br>
and paste GPS coordinates with clipboard</p>
<p>BUG: 191914</p>
<p> M  +1 -0      CMakeLists.txt<br>
 M  +1 -1      gpsdataparser.h<br>
 M  +3 -3      gpseditdialog.h<br>
 AM            gpslistviewcontextmenu.cpp   [License: GPL (v2+)]<br>
 AM            gpslistviewcontextmenu.h   [License: GPL (v2+)]<br>
 M  +1 -1      gpsmapwidget.h<br>
 M  +7 -2      gpssyncdialog.cpp<br>
 M  +2 -2      gpssyncdialog.h<br>
 M  +4 -0      gpstracklisteditdialog.cpp<br>
 M  +3 -3      gpstracklisteditdialog.h<br>
 M  +1 -1      gpstracklistwidget.h<br>
 M  +3 -3      kmlexportconfig.h<br>
 M  +1 -1      plugin_gpssync.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969668 by cgilles:</p>
<p>compile with future libkdcraw 1.0.0</p>
<p> M  +10 -0     batchdialog.cpp<br>
 M  +6 -2      singledialog.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969901 by aclemens:</p>
<p>&lt;ul&gt; tags shouldn't be "alone" in HTML code. It is better to embed them<br>
in paragraphs.</p>
<p> M  +15 -11    welcomepageview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969870 by cgilles:</p>
<p>remove namespace extra qualification.<br>
move buildDate method to svnversion header to be recompiled at the right<br>
time</p>
<p> M  +0 -5      digikam/daboutdata.h<br>
 M  +5 -5      digikam/main.cpp<br>
 M  +12 -1     digikam/version.h.cmake<br>
 M  +8 -6      showfoto/main.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969855 by cgilles:</p>
<p>since it still just one file for sharpnesseditor to compile, move it to<br>
up dir.</p>
<p> M  +6 -7      CMakeLists.txt<br>
 A             sharpentool.cpp   sharpnesseditor/sharpentool.cpp#969851 [License: GPL (v2+)]<br>
 A             sharpentool.h   sharpnesseditor/sharpentool.h#969851 [License: GPL (v2+)]<br>
 D             sharpnesseditor (directory)  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969857 by cgilles:</p>
<p>store expander instance to d private internal class.<br>
don't use "m_" prefix with local member, it's dedicated to main member<br>
(here all main members are hosted to d container in fact) use<br>
readSettings() and writeSettings() to save and restore expander box<br>
state between sessions.<br>
CCMAIL: Julien@narboux.fr</p>
<p> M  +14 -8     contentawareresizetool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969802 by mboquien:</p>
<p>Filtering did not work for anything other than "Greater than or equals"<br>
because a method had "ratingCond   = ratingCondition;" where one was the<br>
object member and the other one was the method parameter. Apparently the<br>
object member took precedence over the paremeter so only the default<br>
setting worked.</p>
<p>BUG:193163</p>
<p> M  +2 -2      imagefiltersettings.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969949 by cgilles:</p>
<p>Warnings : sound like to use KTemporaryFile under windows is bugous if<br>
no prefix is set as lead temp folder.<br>
Under Linux, no problem : KDE tmp dir is used properly, but under<br>
windows, it's doesn't work. I don't know why...<br>
So, for digiKam welcome page and CSS file, we don't need a temp file.<br>
CSS content is just merged with XHTML code as well, and it's work<br>
perfectly...<br>
CCMAIL: digikam-devel@kde.org</p>
<p> M  +2 -2      data/about/main.html<br>
 M  +4 -21     digikam/welcomepageview.cpp<br>
 M  +0 -6      digikam/welcomepageview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 969937 by cgilles:</p>
<p>Simply ktemporaryfile handling. no need to use Qfile here,<br>
KTempopraryFile is enough.<br>
Remove CSS temp file properly, else we will fill temp dir...<br>
This patch must fix CSS file creation of welcome page view under<br>
windows, which is previouly not created duing both file instance open at<br>
the same time (another limitation of M$?)</p>
<p> M  +11 -13    welcomepageview.cpp<br>
 M  +1 -1      welcomepageview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970253 by coles:</p>
<p>Proof-reading.</p>
<p> M  +5 -5      gpseditdialog.cpp<br>
 M  +2 -2      gpslistviewcontextmenu.cpp<br>
 M  +1 -1      gpslistviewitem.cpp<br>
 M  +9 -9      gpssyncdialog.cpp<br>
 M  +1 -1      gpstracklisteditdialog.cpp<br>
 M  +3 -3      kmlexport.cpp<br>
 M  +5 -5      kmlexportconfig.cpp<br>
 M  +1 -1      plugin_gpssync.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970262 by mwiesweg:</p>
<p>I'm not sure about the intention here, but it resets the current item at<br>
every single mouse click.<br>
Change it to touch only the selection, not the current item. Leave<br>
clearing the selection to QItemSelection (which gets passed the flags).<br>
I assume it's needed to clear d-&gt;lastSelection before if the clear flag<br>
is set.</p>
<p> M  +4 -8      kcategorizedview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970261 by mwiesweg:</p>
<p>Optimization part 2:<br>
Use a binary search algorithm to limit number of access to data() for<br>
CategorySortRole.  Create indexes exactly once and reuse.</p>
<p> M  +122 -34   kcategorizedview.cpp<br>
 M  +5 -0      kcategorizedview_p.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970260 by mwiesweg:</p>
<p>Fix issue of grid size not set after starting with empty digikamrc.<br>
A ThumbSize object is default-constructed not with 0 but with 96, which<br>
happens to be the default value; then the check d-&gt;thumbSize == newSize<br>
will return true.</p>
<p> M  +2 -0      digikam/imagedelegate.cpp<br>
 M  +3 -1      libs/models/imagethumbnailmodel.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970286 by aclemens:</p>
<p>Default to the first rename method when no config option is present.</p>
<p>CCBUG:193226</p>
<p> M  +1 -1      renamecustomizer.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970756 by mwiesweg:</p>
<p>Optimization part 3.<br>
- QList can be inefficient for QModelIndex - in fact it will call<br>
  new/delete for every single insertion or deletion, which makes<br>
  complexity O(n). QVector is O(1).  Use QVector for storing indexes<br>
- elementsInfo is always completely filled and size is known in advance.<br>
  No need for a hash, QVector is fine.<br>
- for non-uniformItemSizes, store at least the view options when<br>
  iterating. This does not affect us anymore in digikam but<br>
  viewOptions() was initially the no.1 cost</p>
<p>Combine the branches for uniformItemSizes/non-uniform sizes again.</p>
<p> M  +33 -61    kcategorizedview.cpp<br>
 M  +3 -3      kcategorizedview_p.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970736 by coles:</p>
<p>Minor proof-reading.</p>
<p> M  +9 -9      contentawareresizetool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970755 by mwiesweg:</p>
<p>After exif rotation, delete thumbnail and signal image change</p>
<p> M  +4 -0      metadatamanager.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 970765 by mwiesweg:</p>
<p>Ok upperBound means one-past-the-last. It's better like this.<br>
Fix the problem that the first item of the next category<br>
was shown as the last item of the previous category.</p>
<p> M  +5 -5      kcategorizedview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 971111 by mwiesweg:</p>
<p>Switching on dynamicSortFilter is not good - it leads to numerous<br>
layoutChanged() signals, one for each dataChanged(). We emit dataChanged<br>
e.g. when a thumbnail is loaded.<br>
Be more specific and trigger invalidate() when a changeset is received.<br>
Add a signal to ImageModel to emit on changeset, and connect these to<br>
invalidate in the filter model.</p>
<p> M  +16 -2     imagefiltermodel.cpp<br>
 M  +3 -0      imagefiltermodel.h<br>
 M  +13 -4     imagemodel.cpp<br>
 M  +10 -0     imagemodel.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 971108 by mwiesweg:</p>
<p>I see no need to clear the selection here. We do not need to deal with<br>
it at all.  QItemSelection model handles this just fine.</p>
<p> M  +3 -1      kcategorizedview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 971112 by mwiesweg:</p>
<p>- Use our new API additions to KCategorizedView<br>
- if there was one selected item, which is then filtered out, ensure<br>
  there is one selected item again. This fixes the problem that the<br>
  preview view is empty when the currently viewed item is filtered out<br>
- adapt to model API changes</p>
<p> M  +77 -15    imagecategorizedview.cpp<br>
 M  +8 -6      imagecategorizedview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 971339 by aclemens:</p>
<p>Use the extracted ManualRenameInput widget in the cameraUI and BQM</p>
<p> M  +1 -228    cameragui/renamecustomizer.cpp<br>
 M  +0 -50     cameragui/renamecustomizer.h<br>
 M  +9 -9      queuemanager/queuelist.cpp<br>
 M  +1 -1      queuemanager/queuesettingsview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 971983 by aclemens:</p>
<p>I think we should stretch the settings box, otherwise we have too much<br>
space below it and a scrollbar will appear, although the editor is big<br>
enough to display everything (at least on my 1440x900 screen).</p>
<p> M  +9 -9      rawsettingsbox.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972107 by cgilles:</p>
<p>when DMetadata is used to play with image information, unforget to<br>
register flags from albumsettings to follow digiKam configuration.<br>
BUG: 191678</p>
<p> M  +1 -0      albumiconview.cpp<br>
 M  +3 -1      metadatamanager.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972163 by coles:</p>
<p>Proof-reading.</p>
<p> M  +2 -2      batchprocessimages/colorimagesdialog.cpp<br>
 M  +3 -3      batchprocessimages/convertimagesdialog.cpp<br>
 M  +1 -1      batchprocessimages/convertoptionsdialog.cpp<br>
 M  +2 -2      batchprocessimages/effectimagesdialog.cpp<br>
 M  +2 -2      batchprocessimages/filterimagesdialog.cpp<br>
 M  +1 -1      batchprocessimages/filteroptionsdialog.cpp<br>
 M  +1 -1      batchprocessimages/imagepreview.cpp<br>
 M  +1 -1      batchprocessimages/outputdialog.cpp<br>
 M  +2 -2      batchprocessimages/recompressimagesdialog.cpp<br>
 M  +2 -2      batchprocessimages/resizeimagesdialog.cpp<br>
 M  +2 -2      calendar/calevents.ui<br>
 M  +1 -1      calendar/caltemplate.ui<br>
 M  +3 -3      dngconverter/plugin/batchdialog.cpp<br>
 M  +2 -2      facebook/fbtalker.cpp<br>
 M  +4 -4      facebook/fbwidget.cpp<br>
 M  +1 -1      flickrexport/flickralbumdialog.ui<br>
 M  +5 -5      flickrexport/flickrtalker.cpp<br>
 M  +4 -4      flickrexport/flickrwidget.cpp<br>
 M  +1 -1      flickrexport/flickrwindow.cpp<br>
 M  +1 -1      galleryexport/gallerywindow.cpp<br>
 M  +1 -1      htmlexport/plugin.cpp<br>
 M  +2 -2      ipodexport/IpodExportDialog.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972293 by cgilles:</p>
<p>fix uri encryption path. Use KUrl::url() now, as Gwenview.<br>
BUG: 152877</p>
<p> M  +2 -2      thumbnailbasic.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972305 by mwiesweg:</p>
<p>Set recurseAlbums/Tags settings on model when action is changed</p>
<p> M  +2 -2      digikamapp.cpp<br>
 M  +11 -0     digikamview.cpp<br>
 M  +2 -0      digikamview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972362 by cgilles:</p>
<p>thumbnails can be rebuilt in background for<br>
- All items as well (take a while)<br>
- All missing items (faster)<br>
BUG: 140615</p>
<p> M  +26 -15    digikam/digikamapp.cpp<br>
 M  +2 -1      digikam/digikamapp.h<br>
 M  +6 -4      digikam/digikamapp_p.h<br>
 M  +2 -2      digikam/digikamui.rc<br>
 M  +10 -5     utilities/batch/batchthumbsgenerator.cpp<br>
 M  +2 -2      utilities/batch/batchthumbsgenerator.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972420 by mwiesweg:</p>
<p>Fix categoryAt(). Should affect dragging crashes.</p>
<p> M  +10 -8     kcategorizedview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972379 by coles:</p>
<p>Proof-reading.</p>
<p> M  +1 -1      firstrundlg.cpp<br>
 M  +18 -18    generalpage.cpp<br>
 M  +4 -4      simpleviewer.cpp<br>
 M  +1 -1      svedialog.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972402 by anaselli:</p>
<p>fix print order management:<br>
- crash if changing photo order with only two photos selected<br>
- fix move up photo button management</p>
<p> M  +16 -1     wizard.cpp  </p>
<p>------------------------------------------------------------------------<br>
 </p>

<div class="legacy-comments">

</div>