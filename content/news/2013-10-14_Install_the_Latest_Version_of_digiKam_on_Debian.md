---
date: "2013-10-14T08:05:00Z"
title: "Install the Latest Version of digiKam on Debian"
author: "Dmitri Popov"
description: "There are plenty of reasons to choose Debian, but having the most recent versions of your favorite applications is not one of them. Software updates"
category: "news"
aliases: "/node/707"

---

<p>There are plenty of reasons to choose Debian, but having the most recent versions of your favorite applications is not one of them. Software updates trickle down to the stable version of Debian very slowly, which means that packages in the distro's repositories are likely to be a few versions behind the current releases. digiKam is no exception: the Debian stable repositories contain a version of digiKam which can be considered outdated. However, if you don't mind the risk of making the Debian system unstable, or even breaking it altogether, you can opt to upgrade the distro to unstable and then install the latest version of digiKam from the experimental repositories.<a href="http://scribblesandsnaps.com/2013/10/14/install-the-latest-version-of-digikam-on-debian/">Continue to read</a></p>

<div class="legacy-comments">

</div>