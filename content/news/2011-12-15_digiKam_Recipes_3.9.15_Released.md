---
date: "2011-12-15T09:58:00Z"
title: "digiKam Recipes 3.9.15 Released"
author: "Dmitri Popov"
description: "First off, the digiKam Tricks book has got a new title: digiKam Recipes. Why? Because I like the word \"recipe\" better. :-) To celebrate this"
category: "news"
aliases: "/node/636"

---

<p>First off, the digiKam Tricks book has got a new title: digiKam Recipes. Why? Because I like the word "recipe" better. :-) To celebrate this&nbsp;momentous&nbsp;event,&nbsp;a new version of the digiKam Recipes book is available for your reading pleasure. The new version features the <em>Check and Optimize digiKam's Databases</em> recipe as well as a few minor tweaks and corrections. <a href="http://scribblesandsnaps.wordpress.com/2011/12/15/digikam-recipes-3-9-15-released/">Continue to read</a></p>
<p><a href="http://dmpop.homelinux.com/digikamrecipes"><img src="https://s3-eu-west-1.amazonaws.com/digikamrecipes/digikamrecipes_b.jpeg" alt="" width="300" height="500"></a></p>

<div class="legacy-comments">

</div>
