---
date: "2016-09-24T10:17:00Z"
title: "digiKam 5.2.0 is published..."
author: "digiKam"
description: "Dear digiKam fans and users, After a second release 5.1.0 published one month ago, the digiKam team is proud to announce the new release 5.2.0"
category: "news"
aliases: "/node/758"

---

<a href="https://www.flickr.com/photos/digikam/29211112294/in/dateposted-public/"><img src="https://c7.staticflickr.com/9/8336/29211112294_3b12945568_c.jpg" width="800" height="450"></a>

<p>Dear digiKam fans and users,</p>

<p>
After a <a href="https://www.digikam.org/node/756">second release 5.1.0</a> published one month ago, the digiKam team is proud to announce the new release 5.2.0 of digiKam Software Collection. This version introduces a new bugs triage and some fixes following new feedback from end-users.</p>

<p>
This release introduce also a new red eyes tool which automatize the <a href="https://en.wikipedia.org/wiki/Red-eye_effect">red-eyes effect</a> reduction process. Faces detection is processed on whole image and a new algorithm written by a Google Summer of Code 2016 student named <a href="https://plus.google.com/104197877957567047173/posts">
Omar Amin</a> is dedicated to recognize shapes and try to found eyes with direct flash reflection on retina.
</p>

<p>
This new tool is available in Image Editor and also in Batch Queue Manager to be able to process a set of photos at the same time. The algorithm have been very optimized for speed efficiency and for small memory fingerprint. End user can adjust just a single parameter about the reducing level of retina red color to the average of blue and green channels. The default threshold have been tested successfully with a trial and error basis from a large data-set of images. So typically, no user interaction are need with this new version of this tool compared to older one, where no red eyes detection was performed, and a lots of manual settings was required to process this kind of correction.
</p>

<a href="https://www.flickr.com/photos/digikam/29211111644/in/dateposted-public/"><img src="https://c5.staticflickr.com/9/8450/29211111644_cc06843d3f_c.jpg" width="800" height="450"></a>

<p>
With the help of <a href="https://plus.google.com/u/0/118393660693739377704/about">Wolfgang Scheffner</a>, we also start to update the <a href="http://docs.kde.org/development/en/extragear-graphics/digikam/index.html">digiKam</a> and <a href="http://docs.kde.org/development/en/extragear-graphics/showfoto/index.html">Showfoto</a> handbooks. Documents have not been updated since many years. We start to introduce description of 5.x features, but not all the documentation is yet done and re-writing still under progress. This take a while as documentation is huge. All help is welcome to <a href="https://quickgit.kde.org/?p=digikam-doc.git&amp;a=blobf=TODO">contribute to this project</a>, by writing sections, proof-reading, translating, etc...
</p>

<p>
For furher information, take a look into the list of more than <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=5.2.0&amp;product=digikam&amp;product=kipiplugins">42 files currently closed</a> in Bugzilla.
</p>

<p>
digiKam software collection source code tarball, OSX (&gt;= 10.8) package, and Windows 32/64 bits installers can be downloaded from <a href="http://download.kde.org/stable/digikam/">this repository</a>
</p>

<p>Happy digiKaming!</p>


<div class="legacy-comments">

  <a id="comment-21268"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21268" class="active">Wow!! This is great news. I'm</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Sat, 2016-09-24 10:49.</div>
    <div class="content">
     <p>Wow!! This is great news. I'm super excited to try out the 5.2 release. And thanks to all working on updating the Digikam Documentation. Digikam is a big tool now, and having up-to-date documentation is critical to it.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21270"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21270" class="active">thanks !</a></h3>    <div class="submitted">Submitted by Supercow (not verified) on Sat, 2016-09-24 11:11.</div>
    <div class="content">
     <p>Already installed on my archlinux machine. Great job as usual and devs have a very reactive support so Thank you !<br>
Olivier</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21271"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21271" class="active">KIPI plugin - autorotate missing</a></h3>    <div class="submitted">Submitted by gambler (not verified) on Sun, 2016-09-25 09:05.</div>
    <div class="content">
     <p>Hi all,<br>
I just upgrade my Manjaro 16.04. I was looking forward to the new ported KIPI plugin for Gwenview. But I was disappointed that the option "autorotate using EXIF information" is still missing. I did not found any infos via WWW.<br>
Do you know whats wrong?<br>
Thanx!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21272"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21272" class="active">RAW crash on windows 7</a></h3>    <div class="submitted">Submitted by madarexxx (not verified) on Mon, 2016-09-26 06:40.</div>
    <div class="content">
     <p>Digikam crashes on any attempt to open RAW image on Windows 7 64bit. Tried on 5.0, 5.1, 5.2 versions. The error is in:<br>
<code><br>
????????? ????????:<br>
  ??? ??????? ????????:	APPCRASH<br>
  ??? ??????????:	digikam.exe<br>
  ?????? ??????????:	0.0.0.0<br>
  ??????? ??????? ??????????:	aa60aa50<br>
  ??? ?????? ? ???????:	libdigikamcore.dll<br>
  ?????? ?????? ? ???????:	0.0.0.0<br>
  ??????? ??????? ?????? ? ???????:	00000000<br>
  ??? ??????????:	c0000005<br>
  ???????? ??????????:	0000000000612388<br>
  ?????? ??:	6.1.7601.2.1.0.256.48<br>
  ??? ?????:	1049<br>
  ?????????????? ???????? 1:	b855<br>
  ?????????????? ???????? 2:	b855dcddd37a9d0d2137ec2ce35db63d<br>
  ?????????????? ???????? 3:	1c9d<br>
  ?????????????? ???????? 4:	1c9d6f43a9c2fe91168ef217cefe30e6<br>
</code><br>
Could you fix it please? If you need more information, please contact me with madarexxx at gmail.com .<br>
Thank you for your awesome work - Digikam makes life of poor Russian student way easier :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21273"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21273" class="active">Fantastic! </a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2016-09-26 08:36.</div>
    <div class="content">
     <p>Thanks to the Digikam team!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21274"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21274" class="active">Great software, small glitches</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2016-09-27 11:24.</div>
    <div class="content">
     <p>Thanks for keeping up the good work! It is much appreciated to see this package under active development.</p>
<p>There are, however, some small glitches I found in 5.2 on Kubuntu 16.04:<br>
- The style keeps reverting to 'Fusion'. I'm using the 'Breeze' style. Still, digikam used 'Fusion' on first start-up. When I open Systemsettings, change it to something else and then back to 'Breeze', digikam also changes to 'Breeze' ... only until the next time I start it; then it's back to 'Fusion' again.<br>
- I'd like the tooltips for a thumbnail to include the title. When I set the corresponding tick mark in the digikam settings, it does show it. ... Only until the next time I start it; then it's untick'ed again.<br>
- digikam forgets that it has been closed, prior to shutting down the computer. Next time I start my computer, it starts up as if I had left it running on the previous log-off.</p>
<p>These are just small issues. digikam + darkroom are really a great combo for managing photographs.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21275"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21275" class="active">Planned features and updated release schedule</a></h3>    <div class="submitted">Submitted by dajomu (not verified) on Tue, 2016-09-27 12:09.</div>
    <div class="content">
     <p>Hi,</p>
<p>Where can I find an overview of planned features for the next version? I know there is a list where bugs and requests are listed, but they are not assigned to an upcoming version.</p>
<p>Will digikam, if it is not already implemented, launch a snap (ubuntu) package?</p>
<p>Great progress and good software. The updated handbook is one of the best news since there are so many functions and options.<br>
Thanks to all devs.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21276"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21276" class="active">Lossless JPEG editing?</a></h3>    <div class="submitted">Submitted by Calle (not verified) on Mon, 2016-10-10 15:24.</div>
    <div class="content">
     <p>Hello,</p>
<p>Is there any chance you would add support for lossless editing of JPEGs? For example the red-eye removal tool: could you make it only recompress the parts of the JPG that have been altered (the eyes)? Today, it recompresses the entire image (like most image programs), right?</p>
<p>Since I hate losing anything unnecessarily and since JPEG is the most common image format on the planet, it would be awesome if more image programs would do like the "Better JPEG" program and take advantage of the fact that JPEG images are made up of lots of independently encoded rectangular blocks of pixels, and only blocks that have been altered need to be recompressed when saving the image.<br>
( http://www.betterjpeg.com/features.htm )</p>
<p>Is this anything you've looked at / thought about?<br>
I'm sure it involves making a special solution just for JPEGs (that have been partially edited, cropped, etc), but, of course, it wouldn't work when the user wants to resize the image or recompress it at a higher compression ratio, for example.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21277"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21277" class="active">How to print ?</a></h3>    <div class="submitted">Submitted by MacWomble (not verified) on Mon, 2016-10-10 21:50.</div>
    <div class="content">
     <p>Using Mint 18 Cinnamon and Digikam 5.2 there is no function to print and no print assistent. Is there a plugin missing?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21278"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21278" class="active">Digikam 5 need to be built</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2016-10-13 00:36.</div>
    <div class="content">
     <p>Digikam 5 need to be built with KF5 sane support and dependent relevant libraries to give you printing features. It's there in Digikam 5 "item" menu if the above is activated during build of Digikam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21279"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21279" class="active">Installation on Kubuntu 16.10</a></h3>    <div class="submitted">Submitted by Stephan (not verified) on Mon, 2016-10-17 15:25.</div>
    <div class="content">
     <p>Hello,</p>
<p>is there a way to install this on Kubuntu 16.10? I get an unresolvable dependency-error when trying to install Digikam again after the upgrade (which automatically removed it). The problem is this: "Depends: libmarblewidget-qt5-23 (&gt;=4:15.12.0) but it is not installable".</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21280"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21280" class="active">Try the beta AppImage...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2016-10-17 15:29.</div>
    <div class="content">
     <p>Only for testing for the moment with a new account. It's not yet finalized and will be published officially with next 5.3.0 release...</p>
<p>https://drive.google.com/open?id=0BzeiVr-byqt5SUZRMGtWa3VCUDg</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21281"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/758#comment-21281" class="active">Issue solved</a></h3>    <div class="submitted">Submitted by Stephan (not verified) on Mon, 2016-10-17 19:19.</div>
    <div class="content">
     <p>Forget the question. The issue is solved. :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>