---
date: "2010-02-08T23:05:00Z"
title: "Picasaweb plugin refreshed"
author: "Jens Müller"
description: "Dear all digiKam fans and users! The ui of the picasaweb exporter gains some love and is refreshed in current trunk. Its look now fits"
category: "news"
aliases: "/node/501"

---

<p>Dear all digiKam fans and users!</p>

<p>The ui of the picasaweb exporter gains some love and is refreshed in current trunk. 
Its look now fits to other kipi export plugins with imagelistview support showing you the current processing element 
and a non-modal progressbar to let you work on your next photos while uploading.</p>

<a href="http://www.flickr.com/photos/47251968@N04/4338420625/" title="pw upload3 von tschensinger bei Flickr"><img src="http://farm5.static.flickr.com/4049/4338420625_bd8e612b4b.jpg" width="500" height="397" alt="pw upload3"></a>

<p>Besides the exporter, a basic importer has been added, to let you download all your favourite albums.</p>

<a href="http://www.flickr.com/photos/47251968@N04/4339163496/" title="pw download3 von tschensinger bei Flickr"><img src="http://farm5.static.flickr.com/4006/4339163496_731fba3679.jpg" width="296" height="500" alt="pw download3"></a>

<p>Please check out current svn and tell any bugs at bugs.kde.org.</p>
<div class="legacy-comments">

  <a id="comment-19035"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19035" class="active">Cool</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Mon, 2010-02-08 23:54.</div>
    <div class="content">
     <p>I like it!</p>
<p>Will it be possible in the feature to import from Flickr? If that is even possible...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19038"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19038" class="active">look this entry</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-02-09 08:00.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=175327</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19036"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19036" class="active">Arrows</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Tue, 2010-02-09 00:00.</div>
    <div class="content">
     <p>Arrows should be next to each other.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19037"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19037" class="active">Is it still possible to</a></h3>    <div class="submitted">Submitted by Ismael C (not verified) on Tue, 2010-02-09 03:51.</div>
    <div class="content">
     <p>Is it still possible to export tags? (Haven't build it yet, but I don't see the option in the screenshot).</p>
<p>Looks great, btw!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19041"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19041" class="active">with digikam 1.1 you can</a></h3>    <div class="submitted">Submitted by philippe (not verified) on Tue, 2010-02-09 14:15.</div>
    <div class="content">
     <p>with digikam 1.1 you can export<br>
for photo<br>
 caption<br>
 tags<br>
 geotag</p>
<p>for movie<br>
 caption<br>
 tags<br>
 not geotag see and vote<br>
https://bugs.kde.org/show_bug.cgi?id=215664</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19039"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19039" class="active">Great work</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Tue, 2010-02-09 10:20.</div>
    <div class="content">
     <p>Jens, great work - looking forward how import/export plugins improve with each release.</p>
<p>/me has to run now and work on SmugMug embedded progress indicator. ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19040"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19040" class="active">very cool, thanks so much!</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2010-02-09 11:44.</div>
    <div class="content">
     <p>very cool, thanks so much! been waiting for the importer, that is really great!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19042"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19042" class="active">don't like + -  to select</a></h3>    <div class="submitted">Submitted by philippe (not verified) on Tue, 2010-02-09 14:27.</div>
    <div class="content">
     <p>1. it's not handy to select a photo then to click "+" or "-"<br>
i prefer just to click the photo.<br>
one action instead of two</p>
<p>it is heterogeneous because in digikam in album you just click to select a photo</p>
<p>in all digikam it's a good thing to have same gui action for same function</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19043"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19043" class="active">Clarify</a></h3>    <div class="submitted">Submitted by Jens Müller on Tue, 2010-02-09 17:31.</div>
    <div class="content">
     <p>+ and - are not for selecting items to upload. + add additional elements to your upload queue (file chooser opens) and - let you remove some items. So your workflow stays the same. Select your images to upload in your favourite kde image app and open the exporter. Here you only have to select a album and you can hit the upload button.</p>
<p>Jens</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19061"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19061" class="active">How can I get refreshed</a></h3>    <div class="submitted">Submitted by WalDo (not verified) on Wed, 2010-02-24 20:07.</div>
    <div class="content">
     <p>How can I get refreshed plugin? I have Fedora 11 digikam 1.1.0 with kipi-plugins 1.1.0</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19086"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/501#comment-19086" class="active">Which theme?</a></h3>    <div class="submitted">Submitted by casper (not verified) on Wed, 2010-03-24 17:04.</div>
    <div class="content">
     <p>Which theme are you using? Looks great!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>