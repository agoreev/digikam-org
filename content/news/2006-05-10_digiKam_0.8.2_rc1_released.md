---
date: "2006-05-10T23:32:00Z"
title: "digiKam 0.8.2 rc1 released"
author: "Anonymous"
description: "The digiKam Team is pleased to announce digiKam 0.8.2-rc1. digiKam is a digital photo management application for KDE, focussed on importing, organizing and editing digital"
category: "news"
aliases: "/node/118"

---

<p>The digiKam Team is pleased to announce digiKam 0.8.2-rc1.</p>
<p>digiKam is a digital photo management application for KDE,<br>
focussed on importing, organizing and editing digital photos.<br>
Aimed at all types of users.</p>
<p>The source for 0.8.2-rc1 is available from:</p>
<p>    http://sourceforge.net/project/showfiles.php?group_id=42641</p>
<p>Changes since 0.8.2-beta1 include:</p>
<p>New feature:</p>
<p>        o add missing Rating info in album item file tip and image<br>
          properties dialog</p>
<p>Bugs fixed:</p>
<p>        o fix JFIF comments section encoding extraction to respect UTF8<br>
          (#120241)</p>
<p>        o fix x-raw.desktop: removed tiff extention</p>
<p>        o camera download: auto-rotated images lose EXIF meta data<br>
          (#126326)</p>
<p>        o fix message about missing dcraw program</p>
<p>        o EXIF info lost when saving a modified image that was readonly<br>
          (#118501)</p>
<p>        o Hash marks ('#') in album names cause problems. 0.7.* regression<br>
          (#118501)</p>
<p>        o something is broken with TIFF imlib2 loader and 16 bits<br>
          images. Use KDE/Qt TIFF loader instead. (#125916)</p>
<p>More details in the ChangeLog file.</p>
<p>Achim Bohnet<br>
digiKam team</p>

<div class="legacy-comments">

</div>