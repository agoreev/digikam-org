---
date: "2000-01-01T00:00:00Z"
title: "digiKam Recipes Book"
author: "Nobody"
description: "digiKam is an immensely powerful photo management application, and mastering it requires time and effort. This book can help you to learn the ropes in"
category: "news"
aliases: "/node/543"

---

<p>
digiKam is an immensely powerful photo management application, and mastering it requires time and effort. This book can help you to learn the ropes in the most efficient manner. Instead of explaining every digiKam’s feature, the book offers a collection of techniques that can help you to get the most out of this versatile tool.</p>
<p><img title="digikamrecipescoolreader" src="https://scribblesandsnaps.files.wordpress.com/2015/03/digikamrecipes-page.png" width="300" height="500" "="" alt="digiKam Recipes in Cool Reader"></p>
<p>Five facts about the digiKam Recipes ebook:</p>
<ol>
<li>This is the first and only ebook about digiKam.</li>
<li>The ebook was written in close cooperation with the digiKam developers.</li>
<li>50% of all ebook sales go to the digiKam project.</li>
<li>The ebook is DRM-free, so you can read it using any ebook reader or software that supports the EPUB or MOBI format.</li>
<li>You’ll receive all future editions of the ebook free of charge.</li>
</ol>

<p>
The ebook can be purchased through the <a href="http://www.amazon.com/dp/B004774LJS">Amazon Kindle Store</a>, <a href="https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ">Google Play Store</a>,&nbsp;and <a href="https://gumroad.com/l/digikamrecipes">Gumroad</a>.
</p>
<p>
The ebook released under the <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 Unported</a> license, so you are free to share the ebook and modify it as long as you share your modifications.
</p>
<p>Best,<br><br>
Dmitri</p>

<div class="legacy-comments">

</div>