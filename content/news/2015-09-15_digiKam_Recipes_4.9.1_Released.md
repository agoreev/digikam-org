---
date: "2015-09-15T10:04:00Z"
title: "digiKam Recipes 4.9.1 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your reading pleasure. This version features completely revised material on one of the most complicated subjects"
category: "news"
aliases: "/node/744"

---

<p>A new release of&nbsp;<a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a>&nbsp;is ready for your reading pleasure. This version features completely revised&nbsp;material&nbsp;on one of the most complicated subjects in digital photography: color management. The completely rewritten&nbsp;<em>Color Management in digiKam</em> recipe now provides an easy-to-understand introduction to key color management&nbsp;concepts and explains how to set up a color-managed workflow in digiKam. The recipe also&nbsp;offers practical advice on calibrating and profiling digital cameras and displays.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img class="alignnone size-full wp-image-5260" src="https://scribblesandsnaps.files.wordpress.com/2015/01/digikamrecipes-4-3-1.png" alt="digikamrecipes-4.3.1" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/09/15/digikam-recipes-4-9-1-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>