---
date: "2011-05-09T11:25:00Z"
title: "digiKam Software Collection 2.0.0 beta5 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 5th digiKam Software Collection 2.0.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/599"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 5th digiKam Software Collection 2.0.0 beta release!</p>

<p>With this release, digiKam include a lots of bugs fixes and a new tool to export on RajCe web service.</p>

<a href="http://www.flickr.com/photos/digikam/5702563277/" title="rajcetool by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2678/5702563277_bfbcaaf5cf_z.jpg" width="640" height="256" alt="rajcetool"></a>

<p>digiKam include since 2.0.0-beta4 <b>Group of items feature</b> and <b>MediaWiki export tool</b>.</p>

<a href="http://www.flickr.com/photos/digikam/5564467832/" title="wikimediatool-windows by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5062/5564467832_98ec55be99_z.jpg" width="640" height="211" alt="wikimediatool-windows"></a>

<a href="http://www.flickr.com/photos/digikam/5488098135/" title="digiKam-groupitems by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5179/5488098135_17e6307452_z.jpg" width="640" height="360" alt="digiKam-groupitems"></a>

<p>digiKam include since 2.0.0-beta3 <b>Color Labels</b> and <b>Pick Labels</b> feature to simplify image cataloging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5453984332/" title="digikampicklabel2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5013/5453984332_bba7f267da_z.jpg" width="640" height="256" alt="digikampicklabel2"></a>

<p>Also, digiKam include since 2.0.0-beta2 the <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>See also <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/df64822f3de6d25d8669dbeffa075c71fc644a4f/raw/NEWS">the list of file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19885"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19885" class="active">Mandriva digikam packages</a></h3>    <div class="submitted">Submitted by Philippe (not verified) on Mon, 2011-05-09 12:24.</div>
    <div class="content">
     <p>Hello</p>
<p>it's not about the beta but in general.<br>
in your download section for mandriva there is no link</p>
<p>MIB supllies digikam update regurlarly for Mandriva 2010.2</p>
<p>http://mib.pianetalinux.org/</p>
<p>to install the MIB sources for your Mandriva</p>
<p>Mandriva 32 bit<br>
<code><br>
urpmi.addmedia --update MIB-KDE455_release http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/32/release<br>
urpmi.addmedia --update MIB-KDE455_others http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/32/others<br>
urpmi.addmedia --update MIB-KDE455_updates http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/32/updates<br>
urpmi.addmedia --update MIB-KDE455_extras http://mib.pianetalinux.org/MIB/2010.2/others/kde/45X-extras/32/<br>
urpmi --auto-select --auto-update<br>
urpmi --auto-select --auto<br>
urpmi --auto-select --auto<br>
urpmi --auto-select --auto<br>
# bye, bye from mikala, NicCo and all MIB Team<br>
</code></p>
<p>Mandriva 64 bit<br>
<code><br>
urpmi.addmedia --update MIB-KDE455_release http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/64/release<br>
urpmi.addmedia --update MIB-KDE455_others http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/64/others<br>
urpmi.addmedia --update MIB-KDE455_updates http://mib.pianetalinux.org/MIB/2010.2/others/kde/4.5.5/64/updates<br>
urpmi.addmedia --update MIB-KDE455_extras http://mib.pianetalinux.org/MIB/2010.2/others/kde/45X-extras/64/<br>
urpmi --auto-select --auto-update<br>
urpmi --auto-select --auto<br>
urpmi --auto-select --auto<br>
urpmi --auto-select --auto<br>
# bye, bye from mikala, NicCo and all MIB Team<br>
</code></p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19886"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19886" class="active">old themes are gone</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Tue, 2011-05-10 00:55.</div>
    <div class="content">
     <p>Hi all.</p>
<p>At first I must say how great tool digiKam is. It is my killer application on Linux since 0.9.x version and still improving!</p>
<p>As I am using Kubuntu Natty (11.04) I use beta5 from philip's repository (http://www.mohamedmalik.com/?p=1019). Similarly to other natty user I get error with libkdcraw after upgrade to beta5 (http://web.archiveorange.com/archive/v/eXwCGLGySAUwf9XQBMqg) and according to this bugreport I decided to reinstall libkdcraw (which forced me to remove and install digikam2 (+dbg) kipi-plugins and showfoto2 as well).</p>
<p>What surprised me is that all non-system themes are gone! I must say i felt in love with grey theme and now I am browsing http://opendesktop.org/ for similar one. Anyone help?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19887"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19887" class="active">yes, old themes are removed...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-05-10 07:17.</div>
    <div class="content">
     <p>For compatibility purpose, under mac, win7 and Gnome windows manager, old colors themes have been removed.</p>
<p>Now all is managed by KDE Control Center. You can download and install by this panel new theme. Just choose the right one. You can also create new one !</p>
<p>If you want to take inspiration about old grey theme contents, look this file (XML based) :</p>
<p>https://projects.kde.org/projects/extragear/graphics/digikam/repository/entry/data/themes/Gray?rev=development%2F1.3</p>
<p>Colors are encoded has hexa (like HTML). You can re-use these values to create a new gray theme for KDE and share it to opendesktop.org...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19903"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19903" class="active">old themes</a></h3>    <div class="submitted">Submitted by crazyb0b (not verified) on Wed, 2011-06-01 00:18.</div>
    <div class="content">
     <p>This is very sub-optimal. A professional photo management application should use a neutral gray color scheme. The KDE color controls do not allow application specific color assignment and we will be forced to apply the proper color scheme to all applications. However, the color scheme that is proper for a photo management application is not well suited for applications such as web browsers and word processors. Can you provide a way to apply gray color scheme to digikam and not the entire KDE environment?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19904"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19904" class="active">You can do it</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-06-05 16:17.</div>
    <div class="content">
     <p>1/ it possible to select a specific color scheme to digiKam outside whole KDE color scheme (independently)<br>
2/ Do you take a look into KDE control center, to download new color scheme for KDE from desktop.org ? If Gray color scheme do not exist, KDE Control Center has a component to edit and create a new color scheme for KDE. Just share it to desktop.org for other digiKam users...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19893"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19893" class="active">Digikam 2.0 Beta5 on OpenSuSE 11.4-x86-64</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-05-10 21:25.</div>
    <div class="content">
     <p>Hello,</p>
<p>if anybody is interested: On my standard-OpenSuSE-11.4-64bit-Distri i compiled digikam 2.0 Beta5 from the downloadable sources. Error-messages during compilation were very informativ, so it was easy to find missing dependencies for me as non-profi. After all dependencies were fullfilled, compilation worked without any Problem. A rpm could be build sucessfully via "checkinstall".</p>
<p>For Sigma-Digicam-users: x3f-support is now included.</p>
<p>The developers have done a really great job!</p>
<p>Thanks and Bye<br>
Michael<br>
(from Bavaria with bad English)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19895"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19895" class="active">Blending</a></h3>    <div class="submitted">Submitted by Peter (not verified) on Fri, 2011-05-13 17:35.</div>
    <div class="content">
     <p>Hi, I am really surprised by how rapidly digiKam is evolving... It is definitely the most used application on my Gentoo box, so thank you very much for the effort you are putting into making it even better!<br>
Today I was playing a bit with image blending and I was curious whether it would be possible to change the preparation stage of blending (the first wizard that prepares images to be blended). In case I want to make HDR image from a RAW file (one RAW used to make three jpeg files with different exposure), I must manually use RAW converter tool to create those jpegs and then blend them. Would it be possible to automate this step, so that the blending wizard (when provided with one raw file only) would make those temporary under/over-exposed jpegs/tiffs and then blend them?<br>
Thanks for your answer!<br>
Peter</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19896"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19896" class="active">yes, it's possible...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-05-13 19:43.</div>
    <div class="content">
     <p>...and not too complicated to do i think. Please report this whish to bugzilla into kipi-plugins/expoblending component :</p>
<p>https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=kipiplugins&amp;component=ExpoBlending</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19897"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19897" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by Peter (not verified) on Sat, 2011-05-14 09:36.</div>
    <div class="content">
     <p>Thank you very much for your interest! I filled the wish (hopefully right) as bug <a href="https://bugs.kde.org/show_bug.cgi?id=273254">273254</a>. </p>
<p>Best regards,<br>
Peter</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19898"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19898" class="active">sponsorship</a></h3>    <div class="submitted">Submitted by Elmer (not verified) on Thu, 2011-05-19 10:05.</div>
    <div class="content">
     <p>Hi Gilles,</p>
<p>This is Elmer from chilidownload.com, i want to get in touch with you see how to get listed as a sponsor text link on the right top corner? Could you pls get back to me elmerbusiness@gmail.com...thank you in advance!</p>
<p>Thank you,<br>
Elmer</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19901"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/599#comment-19901" class="active">Inverse Filtering possible?</a></h3>    <div class="submitted">Submitted by Sjoerd (not verified) on Mon, 2011-05-30 09:57.</div>
    <div class="content">
     <p>Don't know if this is the right place to ask, but is it possible to do an inverse filtering?<br>
For instance i've placed labels with reject on some pictures and want to see all the pictures besides the rejected ones.<br>
If you filter now on rejected you get only the rejected pictures...</p>
<p>Cheers,<br>
Sjoerd</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
