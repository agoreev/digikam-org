---
date: "2015-11-09T19:53:00Z"
title: "digiKam Recipes 4.9.5 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your reading pleasure. This version features the Using Album Categories recipe and reworked material on using"
category: "news"
aliases: "/node/746"

---

<p>A new release of&nbsp;<a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a>&nbsp;is ready for your reading pleasure. This version features the <em>Using Album Categories</em> recipe and reworked material on using the tagging functionality in digiKam. As always, the new release includes updates, fixes and tweaks.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/11/digikamrecipes-375x-151109.png" alt="digikamrecipes-375x-151109" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/11/09/digikam-recipes-4-9-5-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>