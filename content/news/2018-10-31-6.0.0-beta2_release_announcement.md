---
date: "2018-10-31T00:00:00"
title: "digiKam 6.0.0 beta 2 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, following the first 6.0.0 beta release published in September,
we are proud to announce the second beta of digiKam 6.0.0."
category: "news"
---

[![](https://c2.staticflickr.com/2/1953/43574330340_3ca5822497_c.jpg "digiKam running Time Adjust tool")](https://www.flickr.com/photos/digikam/43574330340)

Dear digiKam fans and users, following the
[first beta release](https://www.digikam.org/news/2018-08-19-6.0.0-beta1_release_announcement/)
published in September, we are proud to announce the second beta of digiKam 6.0.0.


### Exiv2 0.27 support

With this new release we are following the Exiv2 project which is in release
stage for their next stable release to 0.27 planed for December 2018.  This
library is a main component of digiKam used to interact with file metadata, like
populating database contents, updating item textual information, or to handling
XMP side-car for read only files.

After 18 months of development,
[Exiv2 0.27 RC1](http://exiv2.dyndns.org/changelog.html#v0.27) was finally
released and digiKam has been fixed to support the new API of this library.

[![](https://c2.staticflickr.com/2/1969/45600298992_4880e88980_c.jpg "digiKam using Exiv2 0.27-RC1")](https://www.flickr.com/photos/digikam/45600298992)


### Time-Adjust Tool

With this new release the famous TimeAdjust tool is back to quickly patch
time-stamp of items, without having to use the Batch Queue Manager workflow.
Adjusting the date is a common task that wont necessarily require as complex a
tool as BQM. In all cases, even if TimeAdjust tool is now available everywhere
in AlbumView, ImageEditor, LightTable, and Showfoto, a workflow with the
possibility to fix date is always available as with previous releases.

[![](https://c2.staticflickr.com/2/1969/44736510705_960403d6a3_c.jpg "Showfoto running Time Adjust tool")](https://www.flickr.com/photos/digikam/44736510705)

### DrMinGW Support

digiKam under Windows is more and more popular, and the number of reports in
bugzilla has grown up seriously. This is not to imply that digiKam under Windows
is less stable than on Linux, but end users start to use more advanced features
day by day and discover new dysfunctions specific to Windows. After all, the
Windows version is still in beta stage since many years and a lots of
problems have been already fixed.  Also, the port to Qt5 with less KDE
dependencies has seriously stabilized the application and we now enter the last
stage to stabilize all source code on this platform well.

To complete this task, digiKam is cross-compiled for Windows under Linux through
MinGW (thanks to [MXE project](https://mxe.cc/)) and we include a special
package dedicated to catch all technical information generated when a crash
occurs, and to include better C++ exception support.
[DrMinGW](https://github.com/jrfonseca/drmingw) is the tool dedicated to this
task, similar to crash-course used under Linux to post debug traces in a report
when a problem appears at runtime.

This will speed-up the investigations in the future to process Windows bugs
entries, without having to ask end-users to install a debugger and run the
application in it. A recent example of DrMinGW feedback running digiKam under
Windows can be seen [on this file](https://bugsfiles.kde.org/attachment.cgi?id=115957),
with a clear crash located in Exiv2 shared library.

### Embeded Video Player Slider Fixes

digiKam has the capability to play video files as well, without requiring an
extra video player and dedicated codecs (thanks to
[FFmpeg](https://www.ffmpeg.org/) and [QtAV](http://www.qtav.org/) frameworks).
One major dysfunction reported with previous beta release was about the embeded
video slider used to jump into the stream contents which do not work
anymore. This regression, introduced by changes in QtAV API require a long
investigation to restore the feature.

[![](https://c2.staticflickr.com/2/1964/44925969334_2d8bb70540_c.jpg "digiKam embeded video player")](https://www.flickr.com/photos/digikam/44925969334)

### Iconview : Separate Album Contents Feature

With this release, the Album management gets a new feature: separating icon-view
items by properties.  The default option does not separate items and provides a
flat list. The new model can show album contents, automatically separated using
virtual sub-albums, accordingly by mime types (RAW, JPEG, PNG, TIFF), or by the
month they were shot in.  To use this feature, go to the View/Separate Items
menu entry.

[![](https://c2.staticflickr.com/2/1939/43832059450_07cf685d76_c.jpg "digiKam album content separate options")](https://www.flickr.com/photos/digikam/43832059450)

### 208 New Bugs Closed And Source Code Sanitation

With this release, 208 new files have been closed since 6.0.0 beta1.  The total
files closed for next 6.0.0 final release is more than
[The reports already closed](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=6.0.0)
due to new implementations are more than 550 files, but nothing is completed
yet before the final 6.0.0 release.

In parallel to purging pending files in bugzilla, the whole digiKam source code
([1.2M lines of C++ code](https://www.openhub.net/p/digikam))
is parsed by three static source code analyzers (open-source):
[Clang scan-build](https://clang-analyzer.llvm.org/scan-build.html),
[CppCheck](http://cppcheck.sourceforge.net/), and
[Krazy](http://krazy-collection.github.io/krazy/). We have reduced to the
minimum all reports generated by these tools and discovered some important
in-deep dysfunctions.  This permit also to review automatically all code
written by students, before to transitioning implementations to production.

### The Final Words: How to Test This Beta Version

As we already said at 6.0.0-beta1 announcement, due to many changes in database
schema, do not use this beta2 in production yet and backup your database files
before testing it. It's recommended to try this beta2 with a small collection of
items first, and to add new photo and video step by step.

Thanks to all users for your supports and help during the beta stage. We plan
[one other beta release](https://www.digikam.org/documentation/releaseplan/) during this year,
to finaly release the 6.0.0 in December.

digiKam 6.0.0 beta2 source code tarball, Linux 32/64 bits AppImage bundles,
MacOS package and Windows 32/64 bits installers can be downloaded from
[this repository](http://download.kde.org/unstable/digikam/).

Happy digiKam testing...
