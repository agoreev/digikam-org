---
date: "2008-11-28T16:29:00Z"
title: "Kipi-plugins 0.2.0-beta4 for KDE4 released"
author: "digiKam"
description: "The 4th beta release of digiKam plugins box is out. With this new release, Calendar and PrintWizard plugins have been ported to KDE4. Another tool"
category: "news"
aliases: "/node/410"

---

<p>The 4th beta release of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/3065165251/" title="printwizard-0.2.0-beta4-win32 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3074/3065165251_78a67c9365.jpg" width="500" height="374" alt="printwizard-0.2.0-beta4-win32"></a>

<p>With this new release, Calendar and PrintWizard plugins have been ported to KDE4. Another tool have been added to batch remove red eyes automatically. it's based on OpenCV library.</p>

<p>A plugin is available to batch convert RAW camera images to Adobe DNG container (see <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">this Wikipedia entry</a> for details). To be able to compile this plugin, you need last libkdcraw from svn trunk (will be released with KDE 4.2). To extract libraries source code from svn, look at bottom of <a href="http://www.digikam.org/drupal/download?q=download/svn">this page</a> for details.</p>

<p>Kipi-plugins are compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br>
<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br>
<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br>
<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digikam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>

<div class="legacy-comments">

</div>
