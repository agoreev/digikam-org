---
date: "2015-05-01T12:01:00Z"
title: "digiKam Recipes 4.7.1 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your reading pleasure. This version features completely rewritten material on using digiKam to emulate various photographic"
category: "news"
aliases: "/node/737"

---

<p>A new release of&nbsp;<a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a>&nbsp;is ready for your reading pleasure. This version features completely rewritten material&nbsp;on using digiKam to emulate various photographic effects (including the new recipe on how to create a faded vintage look). The book features two new recipes: <em>Geotag Photos with Geofix</em> and <em>Update the LensFun Database</em>.&nbsp;As always, the new release includes minor updates, fixes and tweaks.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/digikamrecipes-4-3-1.png" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/05/01/digikam-recipes-4-7-1-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>