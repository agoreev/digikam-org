---
date: "2006-07-18T19:38:00Z"
title: "digiKam and DigikamImagePlugins 0.8.2 released"
author: "digiKam"
description: "The digiKam Team is pleased to announce the release of digiKam 0.8.2 and digiKamimageplugins 0.8.2. digiKam is a digital photo management application for KDE, focused"
category: "news"
aliases: "/node/128"

---

<p>The digiKam Team is pleased to announce the release of digiKam 0.8.2 and digiKamimageplugins 0.8.2.</p>
<p>digiKam is a digital photo management application for KDE, focused on importing, organizing and editing digital images. It is aimed for all types of users.</p>
<p>digiKam imageplugins enhance digiKam's image editor with more than 2 dozen plugins that make improving you photos easier.</p>
<p>The source for digikam and digikamimageplugins 0.8.2 is available from:<br>
    <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">http://sourceforge.net/project/showfiles.php?group_id=42641</a></p>
<p>Changes since digikam 0.8.2-rc1 include:</p>
<p>Bugs fixed:</p>
<p>o Unsupported initialization of CameraList object (#129610)</p>
<p>o digikam "Mount and Download" Problems (#124952)</p>
<p>o Thumbnail generation fails with raw images (#128283)</p>
<p>o Camera gui always puts *.JPG and *.NEF in the configure - digikam dialog/File<br>
      Mime Types/ Image Files (#127697)</p>
<p>o Crash when moving an album to another album (#128069)</p>
<p>o Auto-rotion of photos may confuse user because of changes made he is not aware of (#126335)</p>
<p>o Communication between digiKam and its kioslaves broken if size_t != off_t (#127634)</p>
<p>o Minimize button missing on "Compaq Flash Reader" camera window (#116485)</p>
<p>o mimelnk/x-image-raw.desktop conflicts with kdegraphics-3.5.x (#121242)</p>
<p>Changes since digikamimageplugins 0.8.1 include:</p>
<p>Bugs fixed:</p>
<p>o White Balance plugin : Fixed Gamma correction slider values inversed</p>
<p>o Infrared plugins: fix unnecessary blurring effects</p>
<p>Translations:</p>
<p>Opensource wouldn't be such a great success without all the translators.</p>
<p>digiKam:</p>
<p>There are 46 languages available, this is a list of the current translators:</p>
<p>Ð—Ð»Ð°Ñ‚ÐºÐ¾ ÐŸÐ¾Ð¿Ð¾Ð²  (bg)<br>
Golam Mortuza Hossain  (bn)<br>
Thierry Vignaud  (br)<br>
Josep Ma. Ferrer  (ca)<br>
Klara Cihlarova  (cs)<br>
Thierry Vignaud  (cy)<br>
Erik KjÃ¦r Pedersen  (da)<br>
Thomas Reitelbach  (de)<br>
Spiros Georgaras  (el)<br>
Malcolm Hunter  (en_GB)<br>
Pablo de Vicente  (es)<br>
Marek Laane  (et)<br>
marcos  (eu)<br>
Mikael Lammentausta (fi)<br>
Tung Nguyen  (fr)<br>
Kevin Patrick Scannell  (ga)<br>
Xabi G. Feal  (gl)<br>
elcuco@kde.org (he)<br>
Tamas Szanto  (hu)<br>
Federico Zenith  (it)<br>
Yukiko BANDO  (ja)<br>
Leang Chumsoben  (km)<br>
Donatas Glodenis  (lt)<br>
Bozidar Proevski  (mk)<br>
Sharuzzaman Ahmat Raslan  (ms)<br>
Kevin Attard Compagno  (mt)<br>
Ã˜yvind A. Holm  (nb)<br>
Bram Schoenmakers  (nl)<br>
Karl Ove Hufthammer  (nn)<br>
Amanpreet Singh Brar  (pa)<br>
Robert Gomulka  (pl)<br>
Pedro Morais  (pt)<br>
Lisiane Sztoltz Teixeira  (pt_BR)<br>
Claudiu Costin  (ro)<br>
Nick Shaforostoff  (ru)<br>
Steve Murphy  (rw)<br>
Gregor Rakar  (sl)<br>
Chusslove Illich  (sr)<br>
Chusslove Illich  (sr@Latn)<br>
Stefan AsserhÃ¤ll  (sv)<br>
Tamil PC  (ta)<br>
Ä°smail ÅžimÅŸek  (tr)<br>
Ivan Petrouchtchak  (uk)<br>
Clytie Siddall  (vi)<br>
Funda Wang  (zh_CN)</p>
<p>We welcome bn eu km ms uk vi as new languages. </p>
<p>The following languages have improved their translations with 50% or more: ca fi hu</p>
<p>The following languages have 100% translations for the second release in row: da de el et it ja nl pt sv</p>
<p>digikamimageplugins:</p>
<p>There are 40 languages available, this is a list of the current translators:</p>
<p>Thierry Vignaud  (br)<br>
Josep Ma. Ferrer  (ca)<br>
Jakub Friedl   Klara Cihlarova  (cs)<br>
Thierry Vignaud  (cy)<br>
Erik KjÃ¦r Pedersen         Rune RÃ¸nde Laursen  (da)<br>
Oliver DÃ¶rr         Stephan Johach Thomas Reitelbach  (de)<br>
Spiros Georgaras         Toussis Manolis  (el)<br>
Andrew Coles  Malcolm Hunter  (en_GB)<br>
Enrique Matias Sanchez     Pablo de Vicente    Pablo Pita Leira  (es)<br>
Marek Laane  (et)<br>
Ion GaztaÃ±aga  (eu)<br>
Mikael Lammentausta (fi)<br>
Tung Nguyen  (fr)<br>
Kevin Patrick Scannell  (ga)<br>
mvillarino  (gl)<br>
Nadav Kavalerchik<br>
elcuco@kde.org (he)<br>
Gabor Dudas         Tamas Szanto  (hu)<br>
Federico Zenith  (it)<br>
Yukiko BANDO  (ja)<br>
Khoem Sokhem  Leang Chumsoben  (km)<br>
Donatas Glodenis  (lt)<br>
Sharuzzaman Ahmat Raslan  (ms)<br>
Kevin Attard Compagno  (mt)<br>
Nils Kristian Tomren  (nb)<br>
Rinse de Vries  (nl)<br>
Karl Ove Hufthammer  (nn)<br>
Amanpreet Singh Alam  (pa)<br>
Michal Rudolf    Robert Gomulka  (pl)<br>
JosÃ© Nuno Coelho Pires         Pedro Morais  (pt)<br>
Lisiane Sztoltz Teixeira   Marcus Gama  (pt_BR)<br>
Nick Shaforostoff  (ru)<br>
Steve Murphy  (rw)<br>
Chusslove Illich   Slobodan Simic  (sr)<br>
Chusslove Illich   Slobodan Simic  (sr@Latn)<br>
Stefan Asserhall  (sv)<br>
Tamil PC   VelMurugan  (ta)<br>
Ä°smail ÅžimÅŸek  (tr)<br>
Ivan Petrouchtchak  (uk)<br>
Clytie Siddall  (vi)<br>
Funda Wang  (zh_CN)</p>
<p>We welcome eu fi gl hu km ms uk vi as new languages</p>
<p>The following languages have improved their translations with 50% or more: ca</p>
<p>The following languages have 100% translations for the second release in row: da de el en_GB et it nl pl pt sr sr@Latn sv</p>
<p>More details in the ChangeLog files.</p>
<p>The future will be digiKam 0.9.0...</p>

<div class="legacy-comments">

</div>