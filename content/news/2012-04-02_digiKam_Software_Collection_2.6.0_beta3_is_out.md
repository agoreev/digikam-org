---
date: "2012-04-02T13:10:00Z"
title: "digiKam Software Collection 2.6.0 beta3 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 3rd digiKam Software Collection 2.6.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/650"

---

<a href="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" title="DSC00499_v1.JPG by digiKam team"><img src="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" width="284" height="270" alt="DSC00499_v1.JPG"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 3rd digiKam Software Collection 2.6.0 beta release!</p>

<p>With this release, digiKam include a lots of bugs fixes about XMP sidecar file supports. New features have been also introduced to last <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Coding Sprint from Genoa</a>. You can read a <a href="http://dot.kde.org/2012/02/22/digikam-team-meets-genoa-italy">resume of this event</a> to dot KDE web page. Thanks again to <a href="http://ev.kde.org">KDE-ev</a> to sponsorship digiKam team...</p>

<p>digiKam include now a progress manager to control all parallelized process running in background. This progress manager is also able to follow processing from Kipi-plugins.</p>

<a href="http://www.flickr.com/photos/digikam/6812381420/" title="progressmanager by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7055/6812381420_1463394eba_z.jpg" width="640" height="256" alt="progressmanager"></a>

<p>Also, a new Maintenance Tool have been implemented to simplify all maintenance tasks to process on your whole collections.</p>

<a href="http://www.flickr.com/photos/digikam/6812384920/" title="maintenancetool01 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7062/6812384920_cb3424fd22_z.jpg" width="640" height="256" alt="maintenancetool01"></a>

<p>Kipi-plugins includes a new tool to export your collections to <a href="http://imageshack.us/">ImageShack Web Service</a>. Thanks to <b>Victor Dodon</b> to contribute.</p>

<a href="http://www.flickr.com/photos/digikam/6812381534/" title="imageshackexport by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7205/6812381534_b1282f3bcb_z.jpg" width="640" height="256" alt="imageshackexport"></a>

<p>digiKam have been ported to <a href="http://www.littlecms.com/">LCMS version 2</a> color management library by <b>Francesco Riosa</b>. Also a new tool dedicated to manage colors from scanned <a href="http://en.wikipedia.org/wiki/Reversal_film">Reversal Films</a> have been implemented by <b>Matthias Welwarsky</b>.

<a href="http://www.flickr.com/photos/digikam/6955685535/" title="negativefimtool by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7059/6955685535_48cf2bcdfc_z.jpg" width="640" height="256" alt="negativefimtool"></a>

</p><p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/about/releaseplan">at this url</a>.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/9c6d823d357c3f62672d05a4cb8a83de41ea1320/entry/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/689e6442d1dbbe104a2b048b304ed9fe14e47950/entry/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20210"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/650#comment-20210" class="active">libkdcraw 2.1.0</a></h3>    <div class="submitted">Submitted by nucleo (not verified) on Mon, 2012-04-02 21:28.</div>
    <div class="content">
     <p>Hi,</p>
<p>This is what I found in libkdcraw from beta3 sources:</p>
<p># 2.1.0 =&gt; 21.0.0 (Released with KDE 4.8.1 - Remove deprecated members</p>
<p>But why actual libkdcraw version released with KDE 4.8.1 and 4.8.2 is 2.0.1?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20211"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/650#comment-20211" class="active">libkexiv2 2.3.0, libkipi 1.6.0</a></h3>    <div class="submitted">Submitted by nucleo (not verified) on Mon, 2012-04-02 21:38.</div>
    <div class="content">
     <p>The same problem with libkexiv and libkipi.<br>
libkexiv2 is 2.1.0 in KDE 4.8.2 instead of 2.3.0<br>
libkipi  - 1.3.0 instead of 1.6.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20212"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/650#comment-20212" class="active">tsk tsk tsk...</a></h3>    <div class="submitted">Submitted by <a href="http://www.gentoo.org/" rel="nofollow">Andreas</a> (not verified) on Sat, 2012-04-07 22:38.</div>
    <div class="content">
     <p>tsk tsk tsk... are you releasing digikam-2.6 for kde-4.9 again? :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20224"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/650#comment-20224" class="active">Links not working</a></h3>    <div class="submitted">Submitted by james (not verified) on Tue, 2012-05-08 21:37.</div>
    <div class="content">
     <p>The links at the end of your article are not working (show "403 Forbidden"):<br>
"See the list of digiKam file closed with this release into KDE bugzilla.<br>
See the list of Kipi-plugins file closed with this release into KDE bugzilla."</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
