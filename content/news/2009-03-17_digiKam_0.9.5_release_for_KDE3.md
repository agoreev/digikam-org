---
date: "2009-03-17T11:04:00Z"
title: "digiKam 0.9.5 release for KDE3"
author: "digiKam"
description: "Dear all digiKam fans and users! Official digiKam 0.9.5 release for KDE3 is out. It's a bug fix and translations updates. digiKam 0.9.5 tarball can"
category: "news"
aliases: "/node/432"

---

<p>Dear all digiKam fans and users!</p>

<p>Official digiKam 0.9.5 release for KDE3 is out. It's a bug fix and translations updates.</p>

<a href="http://www.flickr.com/photos/digikam/3361809273/" title="digikam0.9.5 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3654/3361809273_2ecc6f48d7.jpg" width="500" height="385" alt="digikam0.9.5"></a>

<p>digiKam 0.9.5 tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">at this url</a></p>

<p>Compared to digiKam 0.9.4, closed files and new features are listed below:</p>

<p>

<b>NEW FEATURES:</b><br><br>

General        : libkdcraw dependency updated to 0.1.6.<br>
General        : TIFF/PNG/JPEG2000 metadata can be edited or added (require Exiv2 &gt;= 0.18).<br>
General        : Internal CImg library updated to 1.3.0.<br><br>

Image Editor   : All image plugin tool settings provide default buttons to reset values.<br>
Image Editor   : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
Image Editor   : All image plugin dialogs are removed. All tools are embedded in editor window.<br>
Image Editor   : New composition guide based on Diagonal Rules.<br><br>

<b>BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</b><br><br>

001 ==&gt; 166867 : digiKam crashes when starting.<br>
002 ==&gt; 167026 : Crash on startup Directory ImageSubIfd0 not valid.<br>
003 ==&gt; 146870 : Don't reduce size of image when rotating.<br>
004 ==&gt; 167528 : Remove hotlinking URL from tips.<br>
005 ==&gt; 167343 : Albums view corrupted and unusable.<br>
006 ==&gt; 146033 : When switching with Page* image jumps.<br>
007 ==&gt; 127242 : Flashing 'histogram calculation in progress' is a bit annoying.<br>
008 ==&gt; 150457 : More RAW controls in color management pop-up please.<br>
009 ==&gt; 155074 : Edit Image should allow chance to adjust RAW conversion parameters.<br>
010 ==&gt; 155076 : RAW Conversion UI Needs to be more generic.<br>
011 ==&gt; 142975 : Better support for RAW photo handling.<br>
012 ==&gt; 147136 : When selecting pictures from folder showfoto got closed.<br>
013 ==&gt; 168780 : Loose the raw import.<br>
014 ==&gt; 160564 : No refresh of the number of pictures assigned to a tag after removing the tag from some pictures.<br>
015 ==&gt; 158144 : Showfoto crashes in settings window.<br>
016 ==&gt; 162845 : 'Ctrl+F6' Conflict with KDE global shortcut.<br>
017 ==&gt; 159523 : digiKam crashes while downloading images from Olympus MJU 810/Stylus 810 camera no xD card.<br>
018 ==&gt; 161369 : Quick filter indicator lamp is not working properly in recursive image folder view mode.<br>
019 ==&gt; 147314 : Renaming like crazy with F2 slows down my system.<br>
020 ==&gt; 164622 : Crash using timeline when switching from month to week to month view.<br>
021 ==&gt; 141951 : Blank empty context right-click menus.<br>
022 ==&gt; 116886 : Proposal for adding a new destripe/denoise technique.<br>
023 ==&gt; 163602 : Race condition during image download from Camera/ USB device: image corruption and/or loss.<br>
024 ==&gt; 165857 : Unreliable import when photo root is on network share.<br>
025 ==&gt; 147435 : Resize slider in sharpness dialog doesn't work correct.<br>
026 ==&gt; 168844 : Make the selection rectangle draggable over the image (not only resizable).<br>
027 ==&gt; 147151 : Compile error: multiple definition of `jpeg_suppress_tables'.<br>
028 ==&gt; 170758 : High load when tagging.<br>
029 ==&gt; 168003 : Drag&amp;dropping a photo to a folder in Dolphin begins copying the whole system:/media.<br>
030 ==&gt; 142457 : Temp files not cleaned up after crashes.<br>
031 ==&gt; 164573 : Better support for small screens.<br>
032 ==&gt; 175970 : digitaglinktree merges tags with same name in different subfolders.<br>
033 ==&gt; 108760 : Use collection image (or part of) as Tag/Album icon.<br>
034 ==&gt; 144078 : very slow avi startup.<br>
035 ==&gt; 146258 : Moving an album into waste basket didn't remove.<br>
036 ==&gt; 149165 : cannot edit anymore - sqlite lock?<br>
037 ==&gt; 146025 : Wrong image name when using info from EXIF.<br>
038 ==&gt; 167056 : Updating tags is slow when thumbnails are visible.<br>
039 ==&gt; 141960 : Problems with photos without EXIV data when updating me.<br>
040 ==&gt; 129379 : Renamed Album is shown multiple times although there is only on related picture directory.<br>
041 ==&gt; 171247 : Album creation error when name start by a number.<br>
042 ==&gt; 150906 : digiKam unable to connect to Panasonic LUMIX DMC-TZ3.<br>
043 ==&gt; 148812 : "Auto Rotate/Flip Using Exif Orientation" fails with some images.<br>
044 ==&gt; 150342 : Camera image window keeps scrolling to the currently downloaded picture.<br>
045 ==&gt; 147475 : digiKam Slideshow - Pause button does not stay sticky / work.<br>
046 ==&gt; 148899 : Image Editor does not get the focus after clicking on an image.<br>
047 ==&gt; 148596 : Empty entries in the "back" drop-down when changing month (date view).<br>
048 ==&gt; 161387 : Unable to import photos with digiKam even though it's detected.<br>
049 ==&gt; 165229 : Thumbnail complete update does not work reliably, even for jpgs.<br>
050 ==&gt; 176477 : Files disappear while importing.<br>
051 ==&gt; 179134 : Compile-error with libkdcraw &gt; 0.1.5.<br>
052 ==&gt; 162535 : Startup is extremely slow when famd is running.<br>
053 ==&gt; 179413 : Support restoring blown out highlights.<br>
054 ==&gt; 157313 : Option to reposition lighttable slide-list.<br>
055 ==&gt; 180671 : Scanner import does not work.<br>
056 ==&gt; 175387 : digikam-doc 0.9.4 sf.net tarball is outdated.<br>
057 ==&gt; 181712 : Ubuntu 8.10 - digiKam finds not the correct images.<br>
058 ==&gt; 169037 : Crash when tagging photos.<br>
059 ==&gt; 173314 : digiKam crashes while exiting the photo-editing mode in KDE 3.5.10.<br>
060 ==&gt; 185279 : Raw pictures from Panasonic LX2 have totally wrong colours in digikam show modus.<br>
</p>

<div class="legacy-comments">

  <a id="comment-18335"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/432#comment-18335" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://jjorge.free.fr/packages" rel="nofollow">Zézinho</a> (not verified) on Tue, 2009-03-17 11:42.</div>
    <div class="content">
     <p>Thanks for this awaited release. I will package it for Mandriva 2008 Spring and 2009 : there are conservative users waiting ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18336"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/432#comment-18336" class="active">Unforget libkdcraw and libkexiv2 packages...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-03-17 12:13.</div>
    <div class="content">
     <p>Unforget to backport last libkdcraw and libkexiv2 for KDE3 released recently. it's important...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18337"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/432#comment-18337" class="active">openSUSE packages </a></h3>    <div class="submitted">Submitted by <a href="http://www.kdedevelopers.org/blog/77" rel="nofollow">Will Stephenson</a> (not verified) on Tue, 2009-03-17 12:51.</div>
    <div class="content">
     <p>...are building in the KDE:KDE3 repo for all recent distributions, along with updated libkdcraw and kexiv.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
