---
date: "2011-01-27T11:08:00Z"
title: "Convert RAW Files to DNG with DNGConverter"
author: "Dmitri Popov"
description: "digiKam comes with a nifty batch utility that allows you to convert RAW files to the DNG format. The question is, of course, why you"
category: "news"
aliases: "/node/569"

---

<p>digiKam comes with a nifty batch utility that allows you to convert RAW files to the DNG format. The question is, of course, why you would want to do that. After all, digiKam can handle RAW files without any problem, so what’s the point of adding one more step to your photographic workflow? <a href="http://scribblesandsnaps.wordpress.com/2011/01/27/convert-raw-files-to-dng-with-dngconverter/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19811"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/569#comment-19811" class="active">Indeed why? Risky and pointless.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-01-30 19:10.</div>
    <div class="content">
     <p>Pointless exercise, actually risking more, gaining nothing.</p>
<p>http://www.bythom.com/dng.htm</p>
<p>Quoting Thom Hogan:</p>
<p>"there's this thing some DNG proponents keep trying to scare people with: "some day your camera maker might no longer support your raw format." So what? We already have open source code that interprets NEF (see libraw.org, amongst others). If the future some day will require you to move to another file format, you can do so then. In the meantime, for DNG to succeed the short-term benefits need to be bigger and clear. I don't believe they are."</p>
         </div>
    <div class="links">» </div>
  </div>

</div>