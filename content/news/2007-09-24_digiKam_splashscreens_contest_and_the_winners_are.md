---
date: "2007-09-24T18:02:00Z"
title: "digiKam splashscreens contest : and the winners are..."
author: "digiKam"
description: "This week end, the team have voted to choose the splashscreens used with next release. You can seen the pictures below: digiKam 0.9.3 (KDE3) :"
category: "news"
aliases: "/node/254"

---

This week end, the team have voted to choose the splashscreens used with next release. You can seen the pictures below:

<br><br>

digiKam 0.9.3 (KDE3) : Laurenz Gamper

<br><br><a href="http://websvn.kde.org/*checkout*/branches/extragear/kde3/graphics/digikam/data/pics/digikam-splash.png?revision=716217">
<img src="http://websvn.kde.org/*checkout*/branches/extragear/kde3/graphics/digikam/data/pics/digikam-splash.png?revision=716217" width="503" height="330">
</a><br><br>

Showfoto 0.7.0 (KDE3) : Joel Koop

<br><br><a href="http://websvn.kde.org/*checkout*/branches/extragear/kde3/graphics/digikam/showfoto/pics/showfoto-splash.png?revision=716219">
<img src="http://websvn.kde.org/*checkout*/branches/extragear/kde3/graphics/digikam/showfoto/pics/showfoto-splash.png?revision=716219" width="503" height="330">
</a><br><br>

digiKam 0.10.0 (KDE4) : Fernando Batista

<br><br><a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/digikam-splash.png?revision=716215">
<img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/digikam-splash.png?revision=716215" width="503" height="330">
</a><br><br>

Showfoto 0.8.0 (KDE4) : Udo Lembke

<br><br><a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/showfoto/pics/showfoto-splash.png?revision=716216">
<img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/showfoto/pics/showfoto-splash.png?revision=716216" width="503" height="330">
</a><br><br>

Thanks to all photographers for all contributions (more than 200 pictures...)


<div class="legacy-comments">

</div>