---
date: "2010-06-08T08:23:00Z"
title: "kipi-plugins 1.3.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.3.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/522"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.3.0 !</p>

<a href="http://www.flickr.com/photos/digikam/4681633266/" title="digikam-flickr by digiKam team, on Flickr"><img src="http://farm2.static.flickr.com/1308/4681633266_fcd61020f7.jpg" width="500" height="400" alt="digikam-flickr"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Pre-compiled packages can be downloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<p><b>NEW FEATURES:</b></p>

<b>General</b>                   : libkexiv2 dependency set to 1.1.0 release.<br>
<b>General</b>                   : libkdcraw dependency set to 1.1.0 release.<br><br>

<b>DNGConverter</b>              : Update Adobe DNG sdk to 1.3.0.<br>
                                   Support non-square pixels.
                                   Support 4-color sensors.
                                   Support Fuji-sensors (experimental).<br><br>

<b>SmugExport</b>                : Support export of Captions.<br>

<p><b>BUGFIXES FROM KDE BUGZILLA:</b></p>

001 ==&gt; PrintWizard       : 232382 : Setting custom page size is impossible.<br>
002 ==&gt; Libkdcraw         : 221542 : Build fails due to ambiguities.<br>
003 ==&gt; DNGConverter      : 191907 : DNG Converter damages white balance metadata.<br>
004 ==&gt; DNGConverter      : 233088 : Compile error on Win32 with MinGW4.<br>
005 ==&gt; DNGConverter      : 232991 : DngConverter of kipi-plugins 1.2.0 does not compile under OS X.<br>
006 ==&gt; AcquireImages     : 233676 : Desktop-entry-contains-deprecated-key usr/share/applications/kde4/scangui.desktop:105 TerminalOptions.<br>
007 ==&gt; General           : 233673 : Spelling-error-in-binary kipiplugin_metadataedit.so kipiplugin_printimages.so kipiplugin_rawconverter.so.<br>
008 ==&gt; PicasaWebExport   : 233728 : digiKam picasawebexport unable to display my picasaweb.<br>
009 ==&gt; AdvancedSlideShow : 225153 : digiKam advanced slide show crashes everytimes.<br>
010 ==&gt; JPEGLossLess      : 234021 : digikam crash on loading.<br>
011 ==&gt; GPSSync           : 234073 : digiKam gpssync no altitude.<br>
012 ==&gt; GPSSync           : 233725 : digiKam gpssync unable to geolocalize a picture or movie.<br>
013 ==&gt; PicasaWebExport   : 216604 : digiKam crashes on export to picasa.<br>
014 ==&gt; PicasaWebExport   : 230201 : Error when trying export to picasa.<br>
015 ==&gt; PicasaWebExport   : 216539 : Picasaweb can't select movie to export.<br>
016 ==&gt; JPEGLossLess      : 226838 : "Auto Rotate/Flip Using EXIF Information" does not work.<br>
017 ==&gt; PicasaWebExport   : 234099 : digiKam picasawebexport labels hierarchy disappears.<br>
018 ==&gt; AdvancedSlideShow : 210959 : Gwenview Advanced Slideshow Crash.<br>
019 ==&gt; PiwigoExport      : 235555 : digiKam Plugins Export Piwigo fail.<br>
020 ==&gt; DNGConverter      : 233393 : DNG converter doesn't work for Fuji Finepix S6500fd (= S6000fd).<br>
021 ==&gt; GPSSync           : 236846 : Geolocation does not work in gnome.<br>
022 ==&gt; DNGConverter      : 237914 : Can't compile svn version.<br>
023 ==&gt; JPEGLossLess      : 228483 : digiKam 1.1.0 with system libjpeg-8 breaks image rotation.<br>
024 ==&gt; PicasaWebExport   : 215688 : Attempted uploading pics to picasaweb.<br>
025 ==&gt; AcquireImages     : 238150 : Scangui is untranslated, because it loads no reqired translation catalog.<br>
026 ==&gt; SmugExport        : 240420 : Patch to export captions to smugmug.<br>
027 ==&gt; DNGConverter      : 240749 : DNGConverter produces black files (Canon 5D mkII).<br>
028 ==&gt; FlickrExport      : 221298 : digiKam segfaults on Flickr export.<br>

<div class="legacy-comments">

  <a id="comment-19223"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19223" class="active">libkdcraw &amp; libexiv2</a></h3>    <div class="submitted">Submitted by Jochen (not verified) on Tue, 2010-06-08 13:48.</div>
    <div class="content">
     <p>Hello,</p>
<p>I'm always amazed of new digikam releases. But this one is a little bit tricky to compile since I can't find neither libkexiv2 &gt;= 1.1.0 nor libkdcraw &gt;= 1.1.0.</p>
<p>Greetings,<br>
Jochen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19224"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19224" class="active">improvements</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-06-08 13:56.</div>
    <div class="content">
     <p>These dependencies have been increased due to severals improvements done in these libraries.</p>
<p>Just recompile and install these libs from kdegraphics. See sharedlibs page for details :</p>
<p>http://www.digikam.org/sharedlibs</p>
<p>These libraries are updated exclusively by digiKam team...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19242"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19242" class="active">Which version of kdegraphics</a></h3>    <div class="submitted">Submitted by jon (not verified) on Thu, 2010-06-10 13:12.</div>
    <div class="content">
     <p>Which version of kdegraphics are you referring to. The current stable version (4.4.4) doesn't work for me.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19243"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19243" class="active">code from trunk</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-06-10 13:48.</div>
    <div class="content">
     <p>kdegraphics/libs from trunk, which will be published officially with KDE 4.5.</p>
<p>Just checkout kdegraphics/libs content. It compile as well with KDE 4.4 or 4.3...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19225"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19225" class="active">So......</a></h3>    <div class="submitted">Submitted by taurnil (not verified) on Tue, 2010-06-08 14:07.</div>
    <div class="content">
     <p>libkdcraw and libkexiv2 from kdegraphics-4.4.4 is not current enough?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19226"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19226" class="active">not for this release...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-06-08 14:14.</div>
    <div class="content">
     <p>not for this release...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19227"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19227" class="active">Will 4.4.5 have the right</a></h3>    <div class="submitted">Submitted by taurnil (not verified) on Tue, 2010-06-08 14:18.</div>
    <div class="content">
     <p>Will 4.4.5 have the right versions?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19228"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19228" class="active">no : KDE 4.5.0...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-06-08 14:23.</div>
    <div class="content">
     <p>no : KDE 4.5.0...</p>
<p>But you can recompile yourself libkexiv2 and libkdcraw as well from kdegraphics. digiKam team maintain these parts</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19230"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19230" class="active">So, we as distro maintainers,</a></h3>    <div class="submitted">Submitted by Ozan Caglayan (not verified) on Tue, 2010-06-08 19:01.</div>
    <div class="content">
     <p>So, we as distro maintainers, will have to stick with digikam 1.2 until KDE releases 4.5 SC? That's very bad as no one will benefit from digiKam &gt;= 1.3.0 except people who're probably using kdegraphics from trunk.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19233"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19233" class="active">there is no problem really...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-06-08 23:24.</div>
    <div class="content">
     <p>You can use as well kdegraphics/libs form trunk with a previous version of KDE as 4.4 or 4.3 release. There is no KDEcore depencies in these library. All will compile fine.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19236"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19236" class="active">Dear Gilles!
I respect your</a></h3>    <div class="submitted">Submitted by JJochen (not verified) on Wed, 2010-06-09 07:32.</div>
    <div class="content">
     <p>Dear Gilles!</p>
<p>I respect your work and admire how digikam grew to what it is today, but this dependency on kdegraphics/libs from trunk really hurts digikam as no one will ship it until kde 4.5 will be released...</p>
<p>Greetings<br>
Jochen</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19239"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19239" class="active">there is a problem really...</a></h3>    <div class="submitted">Submitted by Charlie Rose (not verified) on Thu, 2010-06-10 06:41.</div>
    <div class="content">
     <p>kdegraphics from trunk does not compile against KDE 4.4.4 stable. error: ‘class KUrlNavigator’ has no member named ‘setLocationUrl’ Error 2</p>
<p>This was a bad move to release Digikam against KDE development code instead of KDE 4.4.4 stable.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19241"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19241" class="active">You is wrong...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-06-10 07:40.</div>
    <div class="content">
     <p>Here i use KDE 4.3.2 and kdegraphics/libs compile without any problem !</p>
<p>The point very important there : you only need kdegraphics/libs content, not the whole kdegraphics !</p>
<p>I'm sure that i said. I develop and maintain (as all others digiKam memebers) libkexiv2, libkdcraw, and libkipi hosted in kdegraphics/libs...</p>
<p>Checkout the right dir and try again. For more info, take a look there :</p>
<p>http://www.digikam.org/sharedlibs</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19287"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19287" class="active">Too bad that these libs</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2010-06-17 08:18.</div>
    <div class="content">
     <p>Too bad that these libs aren't released in separate tarballs ;/ Waiting for kde 4.5 final since I dislike having mess in my locally installed packages.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div></div><a id="comment-19240"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19240" class="active">How to compile kipi-plugins and digikam 1.3.0</a></h3>    <div class="submitted">Submitted by <a href="http://www.pclinuxos.com" rel="nofollow">Texstar</a> (not verified) on Thu, 2010-06-10 07:02.</div>
    <div class="content">
     <p>For Packagers:</p>
<p>Download the libs from kdegraphics trunk.<br>
svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdegraphics/libs</p>
<p>Remove the .svn files (not needed)<br>
find ./ -name .svn -exec rm -rf {} \;</p>
<p>Extract your kdegraphics-4.4.4.tar.bz2 file</p>
<p>Replace the libs folder with the libs folder you just downloaded from kdegraphics trunk.</p>
<p>Recompress the kdegraphics-4.4.4.tar.bz2 file.</p>
<p>rebuild kdegraphics package</p>
<p>install the kdegraphics devel package.</p>
<p>Now you can build kipi-plugins and digikam 1.3.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19245"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19245" class="active">Tried that. Installed the</a></h3>    <div class="submitted">Submitted by jon (not verified) on Thu, 2010-06-10 15:49.</div>
    <div class="content">
     <p>Tried that. Installed the libs into /usr/local/ and with the use of cmake-gui specified the exiv2, kipi and kdcraw lib_dirs.</p>
<p>-- Found Kdcraw library in cache: /usr/local/lib64/libkdcraw.so<br>
-- Found Kexiv2 library in cache: /usr/local/lib64/libkexiv2.so<br>
-- Found Kipi library in cache: /usr/local/lib64/libkipi.so<br>
-- Identified libjpeg version: 80<br>
-- checking for module 'libkdcraw&gt;=1.1.0'<br>
--   package 'libkdcraw&gt;=1.1.0' not found<br>
-- checking for module 'libkexiv2&gt;=1.1.0'</p>
<p>Got rid of the version checks and everything compiled fine but then digikam had no thumbs or full-size images and it wasn't obvious if it was using mysql or sqlite. Mysql settings appeared to check out ok but then when I go back into the settings dialog sqlite is selected.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19267"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/522#comment-19267" class="active">How can this be an</a></h3>    <div class="submitted">Submitted by <a href="http://alien.slackbook.org/blog/" rel="nofollow">Eric Hameleers</a> (not verified) on Mon, 2010-06-14 13:46.</div>
    <div class="content">
     <p>How can this be an instruction for packagers?</p>
<p>I call this "tips for hackers". How do you expect distro packagers to replace a piece of stable KDE with a hacked-up kdegraphics containing development code, for the sole benefit of getting Digikam to compile?</p>
<p>I will pass on this - no digikam-1.3.0 will enter my repository.</p>
<p>Get your act together please.</p>
<p>Eric</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
