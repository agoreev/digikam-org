---
date: "2015-04-24T12:09:00Z"
title: "Use Geofix to Geotag Photos in digiKam"
author: "Dmitri Popov"
description: "Geofix is a simple Python script that lets you use an Android device to record the geographical coordinates of your current position. The clever part"
category: "news"
aliases: "/node/736"

---

<p><a href="https://github.com/dmpop/geofix">Geofix</a> is a simple Python script that lets you&nbsp;use an Android device to record the geographical coordinates of your current position. The clever part is that the script stores&nbsp;the obtained latitude and longitude values in the digiKam-compatible format, so you can copy the saved coordinates and use them to geotag photos in digiKam's <strong>Geo-location</strong> module.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2015/04/geofix-web1.png" width="605"></p>
<p><a href="http://scribblesandsnaps.com/2015/04/24/use-geofix-to-geotag-photos-in-digikam/">Continue reading</a></p>

<div class="legacy-comments">

</div>