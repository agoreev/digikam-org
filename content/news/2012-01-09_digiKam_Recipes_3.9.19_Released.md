---
date: "2012-01-09T11:06:00Z"
title: "digiKam Recipes 3.9.19 Released"
author: "Dmitri Popov"
description: "This release features the new Add a Vintage Effect in digiKam recipe along with a raft of minor improvements, tweaks, and fixes. Continue to read"
category: "news"
aliases: "/node/639"

---

<p>This release features the new <em>Add a Vintage Effect in digiKam</em> recipe along with a raft of minor improvements, tweaks, and fixes.</p>
<p><a href="http://dmpop.homelinux.com/digikamrecipes/"><img class="alignnone" title="digikamrecipes_coolreader" src="https://s3-eu-west-1.amazonaws.com/digikamrecipes/digikamrecipes_coolreader.png" alt="" width="277" height="461"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/01/09/digikam-recipes-3-9-19-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>