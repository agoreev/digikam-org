---
date: "2011-09-11T12:44:00Z"
title: "How to create panorama’s in digiKam 2.1"
author: "Mohamed Malik"
description: "digikam 2.1 has been released a few days ago with many new features and improvements, one of the most exciting tools in this release is"
category: "news"
aliases: "/node/623"

---

<p>digikam 2.1 has been released a few days ago with many new features and improvements, one of the most exciting tools in this release is the panorama tool. Which is simple and quite effective.</p>
<p>Select the images that you want to stitch and go to<strong> tools&gt; stitch images into a panorama</strong><a href="http://www.mohamedmalik.com/?p=1106">( Continue to read...)</a><strong><br>
</strong></p>

<div class="legacy-comments">

</div>