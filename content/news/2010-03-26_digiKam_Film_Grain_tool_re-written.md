---
date: "2010-03-26T15:31:00Z"
title: "digiKam Film Grain tool re-written..."
author: "digiKam"
description: "Since a very long time, my TODO list included a complete review of digiKam Film Grain tool. Based on an old Gimp Guru tutorial, this"
category: "news"
aliases: "/node/510"

---

<a href="http://www.flickr.com/photos/digikam/4464847474/" title="filmgrain_old by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4023/4464847474_30803a3ebc_m.jpg" width="240" height="192" alt="filmgrain_old"></a>

<a href="http://www.flickr.com/photos/digikam/4464859048/" title="filmgrain_new by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2796/4464859048_521cd5470c_m.jpg" width="240" height="192" alt="filmgrain_new"></a>

<p>Since a very long time, my TODO list included a complete review of digiKam Film Grain tool. Based on <a href="http://gimpguru.org/Tutorials/FilmGrain">an old Gimp Guru tutorial</a>,
this tool lacks a lots of features as the capability to manage colors and luminosity noise, graininess size, and photo-electronic noise rendering.</p>

<p>I have always been impressionned by <a href="http://powerretouche.com/Film-Grain_plugin_tutorial.htm">PowerRetouche photoshop plugin</a>. This company make great tools for photographers, but of course, all are closed source and non-free. PowerRetouche collection has a Film Grain simulator has you can see in screen-shot below.</p>

<a href="http://powerretouche.com/Mac/OSX/Film-grain.jpg"><img src="http://powerretouche.com/Mac/OSX/Film-grain.jpg" width="500" height="385"></a>

<p>Few week ago, following <a href="https://bugs.kde.org/show_bug.cgi?id=148540">this entry in KDE bugzilla</a>, Julien Narboux has started to brain about Image Editor Film Grain tool. The first stage has been to remove old code and implement a new one working on HSL color space to apply noise on luminosity channel. He has added the capability to adjust graininess on Shadows, or MidTone, or highlight, as PowerRetouche tool.</p>

<p>But i have still not happy with this implementation, especially about to use HSL color space. If we want to simulate CCD sensor noise, playing with luminosity channel been not enough. So, i started to implement <a href="http://en.wikipedia.org/wiki/YCbCr">YCbCr color space</a> conversion in digiKam core pixels access class, and adjust noise generator to be able to play with Chrominance channels. The result is a tool which can reproduce all noise provided by a digital camera, as you can see below.</p>

<a href="http://www.flickr.com/photos/digikam/4461942821/" title="filmgrain_chromabr_size2 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4063/4461942821_b96f57e1c8.jpg" width="500" height="385" alt="filmgrain_chromabr_size2"></a>

<a href="http://www.flickr.com/photos/digikam/4461942139/" title="filmgrain_lumanoise_size2 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4048/4461942139_c8b503a529.jpg" width="500" height="385" alt="filmgrain_lumanoise_size2"></a>

<p>For each Channel, you can apply and mix noise as you want. You can also change the graininess size, as with PowerRetouche, and use two different noise generators : one based on Gaussian distribution, and another one on Poisson. This last one is more realistic to render film graininess, but take more time to compute.</p>

<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/8zItPTbyJa8&amp;hl=fr_FR&amp;fs=1&amp;rel=0"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><embed src="http://www.youtube.com/v/8zItPTbyJa8&amp;hl=fr_FR&amp;fs=1&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></object>

<p>If you want to process Film Grain tool in batch, no problem, digiKam Batch Queue Manager is there for you. You will save a lot of time in your work-flow...</p>

<a href="http://www.flickr.com/photos/digikam/4466061147/" title="bqm-filmgrain by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4055/4466061147_ae512fcaab.jpg" width="500" height="313" alt="bqm-filmgrain"></a>

<p>Finally, this new version of Film Grain tool will be available for digiKam 1.2.0, which will be released in few days. I hope that you will enjoy this tool... Note the huge difference between digiKam 0.9 and 1.x : it doesn't look like a professional photo software now ?</p> 
<div class="legacy-comments">

  <a id="comment-19091"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19091" class="active">question regarding unknown error</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2010-03-26 15:56.</div>
    <div class="content">
     <p>hi i've just tried digikam again after quite a long time (almost two years i think) and i am very impressed. there is one thing i don't know how to solve though, when i try to export an image to picasa web service (never done that before) it tries to see if there is still a valid token, then asks me for pw&amp;un which i provide and then fails ("Error occurred: Unknown error. Unable to proceed further") - what do i do?</p>
<p>thank you for your help in advance and let me say again that i am very impressed by digikam!</p>
<p>marco</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19093"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19093" class="active">kipi-plugins version ?</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-03-26 16:50.</div>
    <div class="content">
     <p>Sound like an old problem from kipi-plugins package. I recommend to update to last 1.1.0 release.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19092"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19092" class="active">pgf graphic file</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Fri, 2010-03-26 16:17.</div>
    <div class="content">
     <p>After Digikam allowed .pgf storage on images i try it and it saved space with good quality. But only Digikam can read and write it. Nothing else. Even Krita do not use it. Or Gimp. Is it not for users? Or too new?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19094"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19094" class="active">PGF is like JPEG-XR...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-03-26 16:52.</div>
    <div class="content">
     <p>PGF image format is like JPEG-XR from M$ : too new. The huge difference is that PGF is an open source project !</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19095"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19095" class="active">RedEyeRemoval</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Fri, 2010-03-26 18:07.</div>
    <div class="content">
     <p>Gilles, once again thanks for this great software.</p>
<p>Is there also something on your to-do list to improve the red eye removal tool? Right know it is not working very good and creates a lot of artifacts.</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19096"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/510#comment-19096" class="active">yes, it's planed</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-03-26 18:53.</div>
    <div class="content">
     <p>yes, this is another tedious code to review, especially to simplify tool and add non-human eyes correction.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
