---
date: "2011-05-09T10:57:00Z"
title: "digiKam Tricks 3.3 Released"
author: "Dmitri Popov"
description: "This release adds a concise introduction to the Rawstudio tool for processing RAW files. Readers who already purchased the book will receive the new version"
category: "news"
aliases: "/node/598"

---

<p>This release adds a concise introduction to the <a href="http://rawstudio.org/">Rawstudio</a> tool for processing RAW files. Readers who already purchased the book will receive the new version free of charge. If you haven't received your copy, please send your order confirmation as proof of purchase to dmpop@linux.com, and I'll email you the latest version of the book. <a href="http://scribblesandsnaps.wordpress.com/2011/05/09/digikam-tricks-3-3-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>