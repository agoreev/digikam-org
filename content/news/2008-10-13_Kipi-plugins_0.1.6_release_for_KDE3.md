---
date: "2008-10-13T19:53:00Z"
title: "Kipi-plugins 0.1.6 release for KDE3"
author: "digiKam"
description: "Kipi-plugins 0.1.6 for KDE3 has been just released. The tarball can be downloaded from SourceForge at this url. This release will be the last one"
category: "news"
aliases: "/node/380"

---

<p>Kipi-plugins 0.1.6 for KDE3 has been just released. The tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=165761&amp;release_id=632559">at this url</a>.</p>
<p>This release will be the last one for KDE3 desktop. The team will concentrate work to KDE4 code where few plugins are not yet ported</p>
<p>This release include severals fix for Flickr and PicasaWeb export plugins. See below the list of new features and bugs report closed:</p>
<p>FlickrExport   : dialog layout re-written to be more suitable.<br>
FlickrExport   : list of item to upload is now display in dialog.<br>
FlickrExport   : Support RAW files format to upload as JPEG preview.<br>
PicasaWebExport: Support RAW files format to upload as JPEG preview.</p>
<p>001 ==&gt; 162687 : FlickrExport       : Make Flickr Export screen non-modal.<br>
002 ==&gt; 162683 : FlickrExport       : Export to picassaweb doesn't works.<br>
003 ==&gt; 164152 : FlickrExport       : Geotagging when exporting to Flickr.<br>
004 ==&gt; 166712 : PrintWizard        : Choosing the part which is cut off for printing only works for the first photo.<br>
005 ==&gt; 162994 : PicasaWebExport    : Picasa album list does not contain "not listed" albums, contains only "public" albums.<br>
006 ==&gt; 150912 : PicasaWebExport    : Uploader does not upload.<br>
007 ==&gt; 164908 : PicasaWebExport    : Private albums are not listed (and then not usable).<br>
008 ==&gt; 150979 : PicasaWebExport    : Picasa Export-Plugins does not work.<br>
009 ==&gt; 149890 : TimeAdjust         : Adjust date and time from Exif does not work.<br>
010 ==&gt; 154273 : RAWConverter       : Quality setting for jpegs saved by raw converter is insanely high.<br>
011 ==&gt; 157190 : GPSSync            : Misleading description for geotagging images.<br>
012 ==&gt; 154289 : FlickrExport       : Cannot upload RAW images.<br>
013 ==&gt; 153758 : FlickrExport       : FlickrUploader fails to upload photos whose caption contains                                       accented characters or a trailing space.<br>
014 ==&gt; 128211 : FlickrExport       : "Ok" Button in "Add Photos" shouldn't fire upload directly.<br>
015 ==&gt; 159081 : FlickrExport       : Upload to Flickr not working.<br>
016 ==&gt; 162096 : FlickrExport       : When selecting all photos reverses the order.<br>
017 ==&gt; 158483 : PicasaWebExport    : Plugin cause digiKam crash.<br>
018 ==&gt; 160453 : PicasaWebExport    : Crashes when exporting to Picasaweb.<br>
019 ==&gt; 155270 : FlickrExport       : Tags with non-Latin characters dropped during image export.<br>
020 ==&gt; 153207 : FlickrExport       : Without using application tags: Flickr still reads embedded metadata into tags.<br>
021 ==&gt; 153206 : FlickrExport       : Provide option to remove spaces in tags during export.<br>
022 ==&gt; 145149 : FlickrExport       : Flickr uploading from digiKam does not cache auth data.</p>
<p>Have an happy digiKam with this new release...</p>

<div class="legacy-comments">

  <a id="comment-17989"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/380#comment-17989" class="active">Current Subject</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-11-20 19:12.</div>
    <div class="content">
     <p>That's very interesting. I'll have to take another look at that.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>