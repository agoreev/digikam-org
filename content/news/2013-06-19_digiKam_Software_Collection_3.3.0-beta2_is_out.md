---
date: "2013-06-19T13:55:00Z"
title: "digiKam Software Collection 3.3.0-beta2 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the second beta release of digiKam Software Collection 3.3.0. This version currently under"
category: "news"
aliases: "/node/697"

---

<a href="http://www.flickr.com/photos/digikam/8744026072/" title="showfoto 3.3.0 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7287/8744026072_10de801c4f.jpg" width="500" height="307" alt="showfoto 3.3.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the second beta release of digiKam Software Collection 3.3.0.
This version currently under development, including a new core implementation to manage faces, especially face recognition feature which have never been completed with previous release. <a href="http://en.wikipedia.org/wiki/Face_detection">Face detection</a> feature still always here and work as expected.</p>

<p><a href="https://plus.google.com/113704327590506304403/posts">Mahesh Hegde</a> who has work on <a href="http://en.wikipedia.org/wiki/Facial_recognition_system">Face Recognition</a> implementation has published a proof of concept demo on YouTube.</p>

<iframe width="560" height="315" src="http://www.youtube.com/embed/iaFGy0n0R-g" frameborder="0" allowfullscreen=""></iframe>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.3.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.3.0-beta2.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b> Release plan <a href="http://www.digikam.org/about/releaseplan">can be seen here...</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20565"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20565" class="active">Nice preview video. It's</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2013-06-20 09:32.</div>
    <div class="content">
     <p>Nice preview video. It's great to see the progress in Digikam. The best Open Source tool for photo management if you ask me :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20566"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20566" class="active">Ordering photos</a></h3>    <div class="submitted">Submitted by gorn (not verified) on Thu, 2013-06-20 12:28.</div>
    <div class="content">
     <p>Now only add manual ordering of photos and it will be the best tool for photo management.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20568"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20568" class="active">"Manual ordering"? Oh, that'd</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Fri, 2013-06-21 22:15.</div>
    <div class="content">
     <p>"Manual ordering"? Oh, that'd be so cool! Fiddling with timestamps and names is really crap when compiling a slide show.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20567"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20567" class="active">Windows compiled version</a></h3>    <div class="submitted">Submitted by wQuick (not verified) on Thu, 2013-06-20 13:40.</div>
    <div class="content">
     <p>Please someone have the binary version for windows?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20569"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20569" class="active">Version for Windows</a></h3>    <div class="submitted">Submitted by John (not verified) on Sun, 2013-06-23 12:37.</div>
    <div class="content">
     <p>I think it would make more sense to work on the Windows version of 3.3.0 rather than working on the version for 3.2.0 i.e. to skip the 3.2.0 Windows-version to have the 3.3.0 Windows-version earlier.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20574"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20574" class="active">Any plans to have this for</a></h3>    <div class="submitted">Submitted by Fabio (not verified) on Wed, 2013-07-03 02:42.</div>
    <div class="content">
     <p>Any plans to have this for windows compiled anytime soon ? I installed the 3.1.0 (latest one I found for windows) Can you provide the URL to this version (or a more recent one that mine)?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20581"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/697#comment-20581" class="active">Intermediate versions have their uses</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-07-13 01:16.</div>
    <div class="content">
     <p>Actually, releasing intermediate versions do have their uses. For instance, it's easier for me to determine what was incrementally changed so I can fix bugs between versions both to prepare for the next version and also later to determine if a bug in a current version existed in a previous version. If I skip versions then this can become difficult. Regardless, 3.2.0 is almost done. I've been fixing bugs, some of which were reported in the 3.0.0/3.1.0 for Windows <a href="www.digikam.org/node/696">release announcement page</a> and on g+.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
