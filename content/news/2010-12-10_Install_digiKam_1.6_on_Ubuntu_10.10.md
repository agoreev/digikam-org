---
date: "2010-12-10T10:23:00Z"
title: "Install digiKam 1.6 on Ubuntu 10.10"
author: "Dmitri Popov"
description: "Running Ubuntu 10.10? Want the latest version of digiKam with it? Philip Johnsson’s got you covered. His PPA contains the very latest version of the"
category: "news"
aliases: "/node/556"

---

<p>Running Ubuntu 10.10? Want the latest version of digiKam with it? Philip Johnsson’s got you covered. His PPA contains the very latest version of the photo management application ready to be installed on your system. <a href="http://scribblesandsnaps.wordpress.com/2010/12/10/install-digikam-1-6-on-ubuntu-10-10/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19664"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/556#comment-19664" class="active">Warning!</a></h3>    <div class="submitted">Submitted by Sputnik (not verified) on Fri, 2010-12-10 20:22.</div>
    <div class="content">
     <p>I would like to warn everyone to use this repository!</p>
<p>As far as I could see it does not only contain Digikam but also other software that you may not want to upgrade.</p>
<p>You may destabilize or destroy your whole system. If something goes wrong you will at least loose a lot of time to repair it.</p>
<p>Although I would like to thank everyone contributing to Digikam and Ubuntu I am a bit embarrassed to read this recommendation here without a warning hint or just a proper description that gives information about the other software available on this repository. I just lost some essential functions. My software is not working correctly after using this repository. And I will be unable to work for some time! NOT GOOD! Sorry!</p>
<p>P.S.<br>
You can see here yourself what will be added:<br>
https://launchpad.net/~philip5/+archive/extra<br>
… and decide afterwards if this is what you want on your own computer.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19666"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/556#comment-19666" class="active">Agreed</a></h3>    <div class="submitted">Submitted by <a href="http://templetons.com" rel="nofollow">Brad Templeton</a> (not verified) on Fri, 2010-12-10 22:41.</div>
    <div class="content">
     <p>I would love PPAs of the latest Digikam for Maverick and other distros that just rely on the version of KDE and other libraries found there unless it's very difficult to build them without the newer libraries.    I have had problems with most of the digikam PPAs I have tried over the years -- is there something about the way that digikam is built that makes it hard to build on the more stock systems?</p>
<p>Much as Windows is otherwise a terrible OS, I do like that you can take just about any program people publish in binary form and it will install even on 8 year old versions of Windows.  Linux folks assume you should upgrade everything all the time because it's free, of course, but upgrades come with risk and can't be done willy-nilly.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19667"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/556#comment-19667" class="active">In Ubuntu 10.10 compile Digikam</a></h3>    <div class="submitted">Submitted by zdenda (not verified) on Fri, 2010-12-10 23:41.</div>
    <div class="content">
     <p>In Ubuntu 10.10 isn't problem to compile Digikam from sources. There are all required actual libraries. Follow download-&gt;tarball menu. But in Ubuntu 10.04 I didn't succeed to compile newest Digikam because there weren't newest required libraries.<br>
Digikam is the best photo tool! :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>