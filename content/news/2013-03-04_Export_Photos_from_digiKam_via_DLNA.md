---
date: "2013-03-04T09:39:00Z"
title: "Export Photos from digiKam via DLNA"
author: "Dmitri Popov"
description: "Need to quickly push photos in digiKam to a device on the same network? Provided the device supports DLNA, you can do this using the"
category: "news"
aliases: "/node/685"

---

<p>Need to quickly push photos in digiKam to a device on the same network? Provided the device supports <a href="http://en.wikipedia.org/wiki/Digital_Living_Network_Alliance">DLNA</a>, you can do this using the DLNAExport Kipi plugin. Choose <strong>Export ? Export via DLNA</strong> to open the <strong>DLNA Export</strong> wizard. From the <strong>Choose the implementation</strong> drop-down list, select either&nbsp;<em>HUPnP API</em> or <em>miniDLNA</em>. Both options have their advantages and drawbacks.</p>
<p><img class=" wp-image-3453 " alt="Using the DLNA Export Kipi plugin" src="http://scribblesandsnaps.files.wordpress.com/2013/02/digikam-dlnaexport1.png?w=456" width="375"></p>
<p><a href="http://scribblesandsnaps.com/2013/03/04/export-photos-from-digikam-via-dlna/">Continue to read</a></p>

<div class="legacy-comments">

</div>