---
date: "2011-10-04T08:58:00Z"
title: "digiKam Software Collection 2.2.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.2.0, as bug-fixes release,"
category: "news"
aliases: "/node/627"

---

<a href="http://www.flickr.com/photos/digikam/6210520128/" title="digiKam-image-title by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6161/6210520128_8d6548d9c8_z.jpg" width="640" height="256" alt="digiKam-image-title"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.2.0, as bug-fixes release, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.2.0">36 fixed bugs</a>. In this release, Photo Title metadata support have been added to customized item names, outside file names.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.2.0. This release features a new tool coming from <a href="http://community.kde.org/GSoC/2011/Ideas#digiKam">Season of KDE 2011 (SoK)</a> project dedicated to create photos layout, and <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.2.0">13 bug-fixes</a>.</p>

<a href="http://www.flickr.com/photos/digikam/6210006335/" title="digiKam-photo-layout-editor by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6225/6210006335_245905c92b_z.jpg" width="640" height="256" alt="digiKam-photo-layout-editor"></a>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20036"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20036" class="active">Any hope for a windows installer</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2011-10-04 09:32.</div>
    <div class="content">
     <p>Thank you very much for the new release !!!</p>
<p>Is there any hope to have a windows-installer as well. The application is the best I've seen to manage fotos and I'm not a linux user.</p>
<p>Thanks in advance,<br>
 Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20037"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20037" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by eMerzh (not verified) on Tue, 2011-10-04 09:47.</div>
    <div class="content">
     <p>Thanks a lot for this release...</p>
<p>the face tagging interface is still crashing after 5-10 new tags in 2.2 :/<br>
i'm trying to rebuild my package to do a proper bug report....</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20038"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20038" class="active">Windows installer</a></h3>    <div class="submitted">Submitted by <a href="http://www.sylvain-crouzillat.com" rel="nofollow">croucrou</a> (not verified) on Tue, 2011-10-04 11:32.</div>
    <div class="content">
     <p>Like Daniel I hope for a windows digikam installer</p>
<p>pleaaaaasssseee.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20040"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20040" class="active">I third this...</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Tue, 2011-10-04 13:16.</div>
    <div class="content">
     <p>I third this...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20055"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20055" class="active">i am also waiting as windows</a></h3>    <div class="submitted">Submitted by <a href="http://www.fightlockdown.com/" rel="nofollow">mma news</a> (not verified) on Fri, 2011-10-07 12:46.</div>
    <div class="content">
     <p>i am also waiting as windows user should also take advantage of this software</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20057"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20057" class="active">I fourth this!</a></h3>    <div class="submitted">Submitted by b_a_slacker (not verified) on Sat, 2011-10-08 04:13.</div>
    <div class="content">
     <p>I use both Windows and Linux... Starting to prefer the linux but still need to have the windows systems around. Would be nice to have familar programs between the two.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20039"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20039" class="active">Roadmap</a></h3>    <div class="submitted">Submitted by Alex (not verified) on Tue, 2011-10-04 12:26.</div>
    <div class="content">
     <p>Thanks a lot for the new version! Digikam is such a great piece of software.</p>
<p>I've just seen the roadmap is out of date. Do you have any new features planned, or is it just a new bugfix release each month? Which is still great.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20042"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20042" class="active">Any hope for a Gallery3 export?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-10-04 17:31.</div>
    <div class="content">
     <p>Is there any hope for a Gallery3 Export plugin as the supplied Gallery2 one doesn't work in 3.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20043"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20043" class="active">Any hope for a Debian package?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-10-04 20:16.</div>
    <div class="content">
     <p>Is there any hope to have a debian package as well?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20060"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20060" class="active">digikam Debian packages now up in experimental</a></h3>    <div class="submitted">Submitted by <a href="http://packages.debian.org/digikam" rel="nofollow">Mark Purcell</a> (not verified) on Sat, 2011-10-08 22:35.</div>
    <div class="content">
     <p>I have uploaded digikam 2.2.0 to Debian experimental.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20067"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20067" class="active">Nice</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-10-11 18:47.</div>
    <div class="content">
     <p>Will it be built for other architectures too?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20044"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20044" class="active">New interface</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-10-04 21:36.</div>
    <div class="content">
     <p>Hi,<br>
thanks you for your hard work: I think Digikam is one of the best photo management software for Linux. </p>
<p>I'd like to know if a revamped interface is on the to-do list.</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20048"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20048" class="active">export to wikimedia</a></h3>    <div class="submitted">Submitted by Marie (not verified) on Wed, 2011-10-05 16:39.</div>
    <div class="content">
     <p>Hello,</p>
<p>I installed 2.1 a while ago and was looking for the commons.wikimedia export tool but could not find it. Is it implemented in the 2.2 version?</p>
<p>regards, Marie</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20053"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20053" class="active">There is a problem with this tool</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-10-05 20:58.</div>
    <div class="content">
     <p>This tool is disabled. There is a uncomplete implementation with wikimedia API. 90% is implemented. Still 10% left. Look details here :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=206842#c10</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20054"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20054" class="active">export to wikimedia</a></h3>    <div class="submitted">Submitted by Marie (not verified) on Thu, 2011-10-06 15:47.</div>
    <div class="content">
     <p>thanks for this information - I am very much looking forward to it, hope to see it soon be activated</p>
<p>thanks a lot for digikam in general - it's a great software!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20049"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20049" class="active">Does not run on OpenSuse 11.4 - undefined symbol</a></h3>    <div class="submitted">Submitted by Corvus (not verified) on Wed, 2011-10-05 19:21.</div>
    <div class="content">
     <p>Hi All,</p>
<p>I've just upgraded to DK2.2 and now it doesn't start, need you expert help.</p>
<p>First of all - the upgrade:<br>
- Opensuse 11.4 64bit<br>
- Upgraded from DG 2.1.0<br>
- Upgraded to DK 2.2 from Backports of latest application releases to stable openSUSE releases (openSUSE_11.4). http://download.opensuse.org/repositories/KDE:/UpdatedApps/openSUSE_11.4/</p>
<p>The Error:</p>
<p>after running exif from konsole I get</p>
<p>"QSqlDatabasePrivate::removeDatabase: connection 'ConnectionTest' is still in use, all queries will cease to work.<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZN11KExiv2Iface14AltLangStrEdit15setLinesVisibleEj"</p>
<p>and DK just won't start</p>
<p>I've read that people on Kubuntu had this issue with DK 2.1 release so I've checked and I have libkexiv2-10 version 4.7.1-2.3 and libkexiv2-9 version 4.6.0-6.11.1 installed.</p>
<p>Any ideas how to fix it?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20061"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20061" class="active">I have the exact same issue</a></h3>    <div class="submitted">Submitted by Haegar (not verified) on Sun, 2011-10-09 15:20.</div>
    <div class="content">
     <p>I have the exact same issue an I'm very disappointed with the quality of the Digikam releases. I remember at least 4(!) updates of Dikikam where it just didn't start anymore without any meaningful message.<br>
Renaming of the database- and config-files can't always be the right solution...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20062"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20062" class="active">this is a UPSTREAM problem.</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-10-09 15:32.</div>
    <div class="content">
     <p>libkexiv2 shared library installed on your system is not updated with digiKam. Typically, your digiKam have been compiled with a libkexiv2 more recent, and this lib have not been updated on your system. It's a broken resolved dependecy from your distro. Contact your linux distro team to solve the problem...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20050"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20050" class="active">digikam 2.2.0 crashes by using import via USB from known camera</a></h3>    <div class="submitted">Submitted by Juergen (not verified) on Wed, 2011-10-05 20:02.</div>
    <div class="content">
     <p>Used OS OpenSuse 11.4 (Tumbleweed)<br>
I updated from 2.1.1<br>
First digikam crashes at every start.<br>
After rebooting OS digikam starts fine.<br>
Trying to import pictures from my camera (Canon SX10IS) digikam crashes.<br>
Same behavior, when I use the automaticaly detected Camera (detected as Canon SX10IS)</p>
<p>Here the crash information:</p>
<p>Application: digiKam (digikam), signal: Segmentation fault<br>
[Current thread is 1 (Thread 0x7f1815e6c7c0 (LWP 13272))]</p>
<p>Thread 6 (Thread 0x7f17f62f7700 (LWP 13273)):<br>
#0  0x00007f180d42738c in pthread_cond_wait@@GLIBC_2.3.2 () from /lib64/libpthread.so.0<br>
#1  0x00007f1811054bbb in QWaitCondition::wait(QMutex*, unsigned long) () from /usr/lib64/libQtCore.so.4<br>
#2  0x00000000005be038 in _start ()</p>
<p>Thread 5 (Thread 0x7f17f5af6700 (LWP 13274)):<br>
#0  0xffffffffff600137 in ?? ()<br>
#1  0x00007fff1f1ff7e7 in ?? ()<br>
#2  0x00007f1807c422b3 in clock_gettime () from /lib64/librt.so.1<br>
#3  0x00007f18110a5cf2 in ?? () from /usr/lib64/libQtCore.so.4<br>
#4  0x00007f1811168ffd in ?? () from /usr/lib64/libQtCore.so.4<br>
#5  0x00007f1811169375 in ?? () from /usr/lib64/libQtCore.so.4<br>
#6  0x00007f1811167e3c in ?? () from /usr/lib64/libQtCore.so.4<br>
#7  0x00007f1811167ee5 in ?? () from /usr/lib64/libQtCore.so.4<br>
#8  0x00007f1807991087 in g_main_context_prepare () from /lib64/libglib-2.0.so.0<br>
#9  0x00007f1807991fa9 in ?? () from /lib64/libglib-2.0.so.0<br>
#10 0x00007f1807992650 in g_main_context_iteration () from /lib64/libglib-2.0.so.0<br>
#11 0x00007f1811168636 in QEventDispatcherGlib::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#12 0x00007f181113cc22 in QEventLoop::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#13 0x00007f181113ce35 in QEventLoop::exec(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#14 0x00007f1811051be4 in QThread::exec() () from /usr/lib64/libQtCore.so.4<br>
#15 0x00007f181111e358 in ?? () from /usr/lib64/libQtCore.so.4<br>
#16 0x00007f18110544d5 in ?? () from /usr/lib64/libQtCore.so.4<br>
#17 0x00007f180d422a3f in start_thread () from /lib64/libpthread.so.0<br>
#18 0x00007f180f6ac66d in clone () from /lib64/libc.so.6<br>
#19 0x0000000000000000 in ?? ()</p>
<p>Thread 4 (Thread 0x7f17f52f5700 (LWP 13276)):<br>
#0  0x00007f180d42738c in pthread_cond_wait@@GLIBC_2.3.2 () from /lib64/libpthread.so.0<br>
#1  0x00007f1811054bbb in QWaitCondition::wait(QMutex*, unsigned long) () from /usr/lib64/libQtCore.so.4<br>
#2  0x00007f181373d6a7 in ?? () from /usr/lib64/libdigikamcore.so.2<br>
#3  0x00007f18110544d5 in ?? () from /usr/lib64/libQtCore.so.4<br>
#4  0x00007f180d422a3f in start_thread () from /lib64/libpthread.so.0<br>
#5  0x00007f180f6ac66d in clone () from /lib64/libc.so.6<br>
#6  0x0000000000000000 in ?? ()</p>
<p>Thread 3 (Thread 0x7f17e1118700 (LWP 13299)):<br>
#0  0xffffffffff600137 in ?? ()<br>
#1  0x00007fff1f1ff7e7 in ?? ()<br>
#2  0x00007f1807c422b3 in clock_gettime () from /lib64/librt.so.1<br>
#3  0x00007f18110a5cf2 in ?? () from /usr/lib64/libQtCore.so.4<br>
#4  0x00007f1811168ffd in ?? () from /usr/lib64/libQtCore.so.4<br>
#5  0x00007f1811169375 in ?? () from /usr/lib64/libQtCore.so.4<br>
#6  0x00007f1811167e3c in ?? () from /usr/lib64/libQtCore.so.4<br>
#7  0x00007f1811167ee5 in ?? () from /usr/lib64/libQtCore.so.4<br>
#8  0x00007f1807991087 in g_main_context_prepare () from /lib64/libglib-2.0.so.0<br>
#9  0x00007f1807991fa9 in ?? () from /lib64/libglib-2.0.so.0<br>
#10 0x00007f1807992650 in g_main_context_iteration () from /lib64/libglib-2.0.so.0<br>
#11 0x00007f1811168636 in QEventDispatcherGlib::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#12 0x00007f181113cc22 in QEventLoop::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#13 0x00007f181113ce35 in QEventLoop::exec(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#14 0x00007f1811051be4 in QThread::exec() () from /usr/lib64/libQtCore.so.4<br>
#15 0x00007f181111e358 in ?? () from /usr/lib64/libQtCore.so.4<br>
#16 0x00007f18110544d5 in ?? () from /usr/lib64/libQtCore.so.4<br>
#17 0x00007f180d422a3f in start_thread () from /lib64/libpthread.so.0<br>
#18 0x00007f180f6ac66d in clone () from /lib64/libc.so.6<br>
#19 0x0000000000000000 in ?? ()</p>
<p>Thread 2 (Thread 0x7f17dc6a3700 (LWP 13315)):<br>
[KCrash Handler]<br>
#6  0x00007f180f65139d in realloc () from /lib64/libc.so.6<br>
#7  0x00007f18029bc187 in register_platform () from /usr/lib64/libdc1394.so.22<br>
#8  0x00007f17da746ec6 in gp_port_library_list () from /usr/lib64/libgphoto2_port/0.8.0/usb.so<br>
#9  0x00007f1812e13568 in ?? () from /usr/lib64/libgphoto2_port.so.0<br>
#10 0x00007f180a6eda41 in ?? () from /usr/lib64/libltdl.so.7<br>
#11 0x00007f180a6ed697 in ?? () from /usr/lib64/libltdl.so.7<br>
#12 0x00007f180a6ee681 in lt_dlforeachfile () from /usr/lib64/libltdl.so.7<br>
#13 0x00007f1812e1397d in gp_port_info_list_load () from /usr/lib64/libgphoto2_port.so.0<br>
#14 0x000000000074456f in ?? ()<br>
#15 0x00000000004e5292 in _start ()</p>
<p>Thread 1 (Thread 0x7f1815e6c7c0 (LWP 13272)):<br>
#0  0x00007f180f6b99b2 in __libc_disable_asynccancel () from /lib64/libc.so.6<br>
#1  0x00007f180f6a3507 in poll () from /lib64/libc.so.6<br>
#2  0x00007f180572dc2a in ?? () from /usr/lib64/libxcb.so.1<br>
#3  0x00007f180572e1a3 in ?? () from /usr/lib64/libxcb.so.1<br>
#4  0x00007f180572e234 in xcb_writev () from /usr/lib64/libxcb.so.1<br>
#5  0x00007f180d9b3cc6 in _XSend () from /usr/lib64/libX11.so.6<br>
#6  0x00007f180d9b4207 in _XReply () from /usr/lib64/libX11.so.6<br>
#7  0x00007f180d9afb13 in XSync () from /usr/lib64/libX11.so.6<br>
#8  0x00007f17e8c046b1 in ?? () from /usr/lib64/gstreamer-0.10/libgstximagesink.so<br>
#9  0x00007f17e8c047f6 in ?? () from /usr/lib64/gstreamer-0.10/libgstximagesink.so<br>
#10 0x00007f17e8c04b09 in ?? () from /usr/lib64/gstreamer-0.10/libgstximagesink.so<br>
#11 0x00007f180723c114 in g_object_unref () from /lib64/libgobject-2.0.so.0<br>
#12 0x00007f17f31a9a2e in ?? () from /usr/lib64/kde4/plugins/phonon_backend/phonon_gstreamer.so<br>
#13 0x00007f17f31cd159 in ?? () from /usr/lib64/kde4/plugins/phonon_backend/phonon_gstreamer.so<br>
#14 0x00007f17f31c9ed3 in ?? () from /usr/lib64/kde4/plugins/phonon_backend/phonon_gstreamer.so<br>
#15 0x00007f17f31c9f29 in ?? () from /usr/lib64/kde4/plugins/phonon_backend/phonon_gstreamer.so<br>
#16 0x00007f18155ec7b4 in Phonon::MediaNodePrivate::deleteBackendObject() () from /usr/lib64/libphonon.so.4<br>
#17 0x00007f18155de7c8 in ?? () from /usr/lib64/libphonon.so.4<br>
#18 0x00007f18155dea79 in ?? () from /usr/lib64/libphonon.so.4<br>
#19 0x00007f180f60e5a1 in __run_exit_handlers () from /lib64/libc.so.6<br>
#20 0x00007f180f60e5f5 in exit () from /lib64/libc.so.6<br>
#21 0x00007f18102f6128 in ?? () from /usr/lib64/libQtGui.so.4<br>
#22 0x00007f1811b847d8 in KApplication::xioErrhandler(_XDisplay*) () from /usr/lib64/libkdeui.so.5<br>
#23 0x00007f180d9b678e in _XIOError () from /usr/lib64/libX11.so.6<br>
#24 0x00007f180d9b403d in _XEventsQueued () from /usr/lib64/libX11.so.6<br>
#25 0x00007f180d9a48df in XEventsQueued () from /usr/lib64/libX11.so.6<br>
#26 0x00007f181032f0a7 in ?? () from /usr/lib64/libQtGui.so.4<br>
#27 0x00007f1807991087 in g_main_context_prepare () from /lib64/libglib-2.0.so.0<br>
#28 0x00007f1807991fa9 in ?? () from /lib64/libglib-2.0.so.0<br>
#29 0x00007f1807992650 in g_main_context_iteration () from /lib64/libglib-2.0.so.0<br>
#30 0x00007f1811168636 in QEventDispatcherGlib::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#31 0x00007f181032f1ae in ?? () from /usr/lib64/libQtGui.so.4<br>
#32 0x00007f181113cc22 in QEventLoop::processEvents(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#33 0x00007f181113ce35 in QEventLoop::exec(QFlags) () from /usr/lib64/libQtCore.so.4<br>
#34 0x00007f181073f89e in QDialog::exec() () from /usr/lib64/libQtGui.so.4<br>
#35 0x00000000004e6389 in _start ()</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20052"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20052" class="active">UPSTREAM problem</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-10-05 20:54.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=268267</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20056"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20056" class="active">Online update is on its way</a></h3>    <div class="submitted">Submitted by Will Stephenson (not verified) on Fri, 2011-10-07 21:23.</div>
    <div class="content">
     <p>There is an online update to libdc1394 coming to openSUSE 11.4 that includes the upstream patch.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20079"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20079" class="active">I have the same camera and</a></h3>    <div class="submitted">Submitted by Scintilla72 (not verified) on Thu, 2011-10-20 21:42.</div>
    <div class="content">
     <p>I have the same camera and the same issue, I'm trying everything, but there is no way, with the last update of DK it wont crash, but it doesn't start the download and the peview of the camera.<br>
I'm with DigiKam from the beginning of it, I like and I hope this issue will be fixed soon...</p>
<p>Giorgio</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20058"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20058" class="active">kipi-plugins 2.2.0 ?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-10-08 09:52.</div>
    <div class="content">
     <p>where is the tar ball of kipi-plugins 2.2.0 ?</p>
<p>at SF there is only the 1.9 version</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20059"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20059" class="active">in digiKam SC tarball</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2011-10-08 09:56.</div>
    <div class="content">
     <p>look in "extra" subfolder...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20063"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20063" class="active">Won't work on natty, won't on</a></h3>    <div class="submitted">Submitted by Qui (not verified) on Sun, 2011-10-09 19:26.</div>
    <div class="content">
     <p>Won't work on natty, won't on Oneiric :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20068"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20068" class="active">What i dont understand, why</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-10-12 06:51.</div>
    <div class="content">
     <p>What i dont understand, why Digikam 2.1.1 work on oneric with KDE 4.7.2<br>
and Digikam 2.2.0 wont work on same platform ...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20064"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20064" class="active">git repos</a></h3>    <div class="submitted">Submitted by Will in Sydney (not verified) on Mon, 2011-10-10 02:21.</div>
    <div class="content">
     <p>Hi DigiKam team!<br>
I'm just wondering if digikam-2.2.0 is now available in the git repository at:<br>
http://anongit.kde.org/digikam-software-compilation</p>
<p>This is really the easiest way to get an update for Ubuntu .. but I can't see anywhere if it's been updated with the latest version.</p>
<p>//Will</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20065"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20065" class="active">Look git commit ID of digiKam and kipi-plugins</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-10-10 06:18.</div>
    <div class="content">
     <p>Checkout code from git/master using digiKam Software Collection and switch digiKam and kipi-plugins sub -repositories to right revision for 2.2.0 release. Look in git history  for that :</p>
<p>https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/changes/project/NEWS.2.2.0</p>
<p>https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.2.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20069"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20069" class="active">Support MS Windows</a></h3>    <div class="submitted">Submitted by Wim (not verified) on Wed, 2011-10-12 08:04.</div>
    <div class="content">
     <p>Dear people,</p>
<p>I? a enthousiast user of Digikam on Linux, Ubuntu 11.04. Version 2.2 runs flawlessy here. But I would recommend further development of a windows port of Digikam, multiplying users of the program and attracking more developers and donators. Whoever does the job: KDE for windows or The Digikam team or a yet unknown party, it's equal to me. I've got version 2.0RC running reasonable on an XP Pro-machine.</p>
<p>Greetz<br>
Wim</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20070"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20070" class="active">Do NOT bother for a windows version!</a></h3>    <div class="submitted">Submitted by ex windows lover (not verified) on Wed, 2011-10-12 08:32.</div>
    <div class="content">
     <p>Please do NOT bother for a windows version!<br>
Linux is the perfect desktop environment anyway and I can not conceive digikam in the closed restricted space of windows.<br>
An OS with only a virtual desktop!<br>
Personally I discovered the world of Linux because of digikam and I am glad I did!</p>
<p>Keep up the awesome work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20075"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20075" class="active">1. Not all people have the</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Mon, 2011-10-17 13:02.</div>
    <div class="content">
     <p>1. Not all people have the option to run Linux as their main OS. And a dual boot system just for DigiKam is a little oversized.</p>
<p>2. Luck at the posts above yours. Getting newer program versions in Linux is a real pain. A newbie like me is completely lost.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20076"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20076" class="active">2.2.0 windows installer is under finalization</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-10-17 13:08.</div>
    <div class="content">
     <p>A new contributor named <a href="https://plus.google.com/u/0/113757549228860309311/about">Ananta Palani</a> has started to update digiKam 2.2.0 installer for Windows. It must be released next week end on SourceForge.net repository.</p>

<a href="http://www.flickr.com/photos/digikam/6244272762/" title="digikam-win7 by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6211/6244272762_ac95296029_z.jpg" width="640" height="180" alt="digikam-win7"></a>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20077"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20077" class="active">Will DigiKAM for Win support AVCHD metadata in sidecars?</a></h3>    <div class="submitted">Submitted by polarbeer (not verified) on Wed, 2011-10-19 21:52.</div>
    <div class="content">
     <p>Hi,</p>
<p>I hope this version of DigiKAM will run stable on Windows. </p>
<p>Can any kind person with enough expertise in these things tell, if DigiKAM will support writing metadata also for AVCHD videos (*.mts, *.m2ts) in xmp sidecar files? If I remember right metadata can be attached to RAW image formats in sidecars by DigiKAM. But can this xmp-sidecar -functionality be applied also for all other file types people are using - for example mts and mp3 files?</p>
<p>(I hope also that DigiKam can display thumbnails for mts files in Win 7, because Windows Explorer can do that.)</p>
<p>All this would make DigiKam a real digital asset manager - not only photo manager - for win.</p>
<p>-pb</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20074"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20074" class="active">YUPI Digikam 2.2.0 UBUNTU ONERIC</a></h3>    <div class="submitted">Submitted by Qui (not verified) on Sun, 2011-10-16 06:57.</div>
    <div class="content">
     <p>Jupiiii :) last digikam 2.2.0 work in ubuntu Oneric 11.10 with last KDE 4.7.2 </p>
<p>I found this PPA for Ubuntu </p>
<p>https://launchpad.net/~hrvojes/+archive/kde-goodies</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20078"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/627#comment-20078" class="active">It worked for me on Ubuntu 11.10 and Gnome 3.2</a></h3>    <div class="submitted">Submitted by <a href="http://hypermodern.net" rel="nofollow">Pete Ippel</a> (not verified) on Wed, 2011-10-19 22:05.</div>
    <div class="content">
     <p>I'm an artist with 72,000 photos and manage them with DigiKam.  I'm so happy about how well this software and community work.</p>
<p>Thank you for all your efforts, they are most appreciated!  </p>
<p>All my best,<br>
Pete Ippel</p>
<p>"I make art and share."</p>
<p>http://peteippel.com<br>
http://hypermodern.net</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
