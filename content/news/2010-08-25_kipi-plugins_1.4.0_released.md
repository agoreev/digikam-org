---
date: "2010-08-25T16:34:00Z"
title: "kipi-plugins 1.4.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.4.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/536"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.4.0 !</p>

<a href="http://www.flickr.com/photos/digikam/4919224056/" title="kipiplugins1.4.0 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4096/4919224056_b87311b2c2.jpg" width="500" height="431" alt="kipiplugins1.4.0"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<p><b>BUGFIXES FROM KDE BUGZILLA:</b></p>

001 ==&gt; IpodExport      : 240469 : IpodExport fails to build with glib &gt;= 2.25.<br>
002 ==&gt; MetadataEdit    : 242624 : Wrong IPTC hyperlink.<br>
003 ==&gt; General         : 239841 : I18n bug - missing extraction of messages in expoblending, flickrexport, metadataedit, ...<br>
004 ==&gt; GPSSync         : 246266 : Search for location when adding geo coordinates.<br>
005 ==&gt; PicasaWebExport : 247106 : Google picassa plugin does not work anymore since google changed parts of the interface.<br>
006 ==&gt; GPSSync         : 246897 : GPSSync bad coordinate rounding.<br>
<div class="legacy-comments">

  <a id="comment-19451"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19451" class="active">Pixelpipe</a></h3>    <div class="submitted">Submitted by <a href="http://eothred.wordpress.com" rel="nofollow">Yngve</a> (not verified) on Thu, 2010-08-26 18:43.</div>
    <div class="content">
     <p>I mentioned it before, I think this would be a huge gain for the kipi-plugins package. Wish submitted here: https://bugs.kde.org/show_bug.cgi?id=234726</p>
<p>Ask any person with the Nokia N900 and he can probably explain why this would be useful. Actually I got intrigued enough that I asked Pixelpipe for a developer key and are currently compiling Digikam. I am 99% sure I wont get anywhere since I am quite useless as a programmer though, just wanted to see if I can understand how you write the plugins and such.. You have any tutorials to recommend?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19452"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19452" class="active">kipi-plugins dng converter with canon cr2</a></h3>    <div class="submitted">Submitted by Rene (not verified) on Thu, 2010-08-26 21:02.</div>
    <div class="content">
     <p>I use the current 1.4.0 version of kipi-plugins. This is a problem on the dng converter. I converted a lot cr2 to dng. Camera is one canon t1i 500d. All are now broken. The colours are wrong.<br>
How i fix this? On travel i not have space on keep all cr2. I only have the broken dng now.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19481"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19481" class="active">The red tint should be gone</a></h3>    <div class="submitted">Submitted by Jens (not verified) on Fri, 2010-08-27 20:40.</div>
    <div class="content">
     <p>The red tint should be gone with libkdcraw from KDE 4.5.1. But I also see the exif markernotes broken and have currently no clue why. Dng files from Nikon images are fine though.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19486"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19486" class="active">upload progress bar?</a></h3>    <div class="submitted">Submitted by <a href="http://unhammer.wordpress.com" rel="nofollow">Kevin</a> (not verified) on Mon, 2010-08-30 12:10.</div>
    <div class="content">
     <p>Does the upload progress bar finally show progress within each file? That (and a kb/s indicator) would be enough reason for me to switch back from KFlickr...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19512"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19512" class="active">Hello,
i want to install</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-09-12 12:33.</div>
    <div class="content">
     <p>Hello,</p>
<p>i want to install kipi-plugins under kde 4.5.1 but it doesn't work.<br>
Follow error message it is shown.</p>
<p>But how can i install kipi-plugins?<br>
i couldn't find infos about libkipi.</p>
<p><code><br>
-- WARNING: you are using the obsolete 'PKGCONFIG' macro use FindPkgConfig<br>
-- PKGCONFIG() indicates that libkipi is not installed (install the package which contains libkipi.pc if you want to support this feature)<br>
CMake Error at /usr/share/kde4/apps/cmake/modules/FindKipi.cmake:90 (message):<br>
  Could NOT find libkipi header files<br>
Call Stack (most recent call first):<br>
  CMakeLists.txt:117 (FIND_PACKAGE)</code></p>
<p>-- Configuring incomplete, errors occurred!<br>
</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19558"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19558" class="active">DigiKam progress...</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Wed, 2010-09-22 21:18.</div>
    <div class="content">
     <p>Thanks for the new version! DigiKam development progress does increase very quick. New versions are released even quicker than it was before (before 1.2.0 version). The 1.2.0 was great for me and then I've upgraded to 1.3.0... Now I'm going to check if 1.4.0 is at least as stable as 1.2.0.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19559"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19559" class="active">Window-Borders</a></h3>    <div class="submitted">Submitted by Heiko (not verified) on Sat, 2010-09-25 21:51.</div>
    <div class="content">
     <p>I'm excited to try out the new version!<br>
This may be a bit off topic, but I couldn't figure it out by myself: how do I obtain that dark window-borders when using a dark theme? - is there a way to have only for digikam-windows a dark border?<br>
cheers,<br>
Heiko</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19561"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19561" class="active">Please remove libs from kdegraphics and make independant</a></h3>    <div class="submitted">Submitted by wsl (not verified) on Mon, 2010-09-27 18:31.</div>
    <div class="content">
     <p>Hi Digikam Team</p>
<p>I have a very stable KDE 4.4.5 running on Fedora 12 and I am forced to use Digikam 1.2.0/Kipi plugins 1.2.0...</p>
<p>Fedora 13 users also have the same problem - namely the kdegraphics/libs which aren't actually KDE version specific are tied to the KDE version through the kdegraphics dependancies.</p>
<p>Neither fedora nor rpmfusion are supporting Digikam 1.3.0 or 1.40 on Fedora 12 or Fedora 13. Arghh!!!</p>
<p>Please remove these libs from KDE SC and make an independant libary package so that all users can enjoy the latest digikam releases by installing from repos..</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19564"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/536#comment-19564" class="active">facebook plugin permissions</a></h3>    <div class="submitted">Submitted by LoneStar (not verified) on Sat, 2010-10-09 17:21.</div>
    <div class="content">
     <p>I think facebook plugin needs to ask for additional permission to access photos, cause at the moment it can only access the profile pictures album.<br>
FB has recently furtherly segmented the amount of permissions to request when accessing users' photo albums.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
