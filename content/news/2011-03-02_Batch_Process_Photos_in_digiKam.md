---
date: "2011-03-02T10:21:00Z"
title: "Batch Process Photos in digiKam"
author: "Dmitri Popov"
description: "When you need to apply the same action to multiple photos, digiKam’s batch processing capabilities can come in rather handy. And the photo management application"
category: "news"
aliases: "/node/580"

---

<p>When you need to apply the same action to multiple photos, digiKam’s batch processing capabilities can come in rather handy. And the photo management application provides different ways to apply actions to a photo batch in one fell swoop. <a href="http://scribblesandsnaps.wordpress.com/2011/03/02/batch-process-photos-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>