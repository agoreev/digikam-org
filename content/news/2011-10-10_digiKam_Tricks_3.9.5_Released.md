---
date: "2011-10-10T08:31:00Z"
title: "digiKam Tricks 3.9.5 Released"
author: "Dmitri Popov"
description: "Besides a few tweaks and fixes as well as a new front cover, this release includes the following new material: Show Photos on Google Earth"
category: "news"
aliases: "/node/628"

---

<p>Besides a few tweaks and fixes as well as a new front cover, this release includes the following new material:</p>
<ul>
<li>Show Photos on Google Earth and Google Maps</li>
<li>The <em>Host Your Own Photo Gallery with Piwigo</em> appendix now covers the Piwigo app for Android</li>
</ul>
<p><a href="http://dmpop.homelinux.com/digikamtricks"><img class="alignnone size-medium wp-image-2049" title="digikamtricks_cr" src="http://scribblesandsnaps.files.wordpress.com/2011/10/digikamtricks_cr.png?w=300" alt="" width="300" height="500"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/10/10/digikam-tricks-3-9-5-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>