---
date: "2008-10-06T15:44:00Z"
title: "Kipi-plugins 0.2.0-beta2 for KDE4 released"
author: "digiKam"
description: "To prepare the future Coding Sprint with kipi-plugins developpers, a second beta release of digiKam plugins box is out. See below the list of new"
category: "news"
aliases: "/node/375"

---

<p>To prepare the future <a href="http://www.digikam.org/drupal/node/374">Coding Sprint</a> with kipi-plugins developpers, a second beta release of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/2887768310/" title="raw2dngconverter by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3290/2887768310_0872c0b392.jpg" width="500" height="400" alt="raw2dngconverter"></a>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white              methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent                 backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.

<div class="legacy-comments">

  <a id="comment-17843"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-17843" class="active">Link and build instructions</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-10-06 16:40.</div>
    <div class="content">
     KDE4 link and build instructions <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">are here</a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17845"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-17845" class="active">Metadata</a></h3>    <div class="submitted">Submitted by dukat (not verified) on Tue, 2008-10-07 11:49.</div>
    <div class="content">
     <p>How do I love Digikam, and especially their improved support for metadata. However metadata are a PITA, and even the Big Companies are seeing this. They have produced Guidelines for Handling Metadata in Images, and it would be great if Digikam would follow their recommendation as a prove to be a major player in this field :-)</p>
<p>http://www.metadataworkinggroup.com/specs</p>
<p>Keep up the good work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17846"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-17846" class="active">Already done...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-10-07 11:50.</div>
    <div class="content">
     <p>I'm working on metadata handling since a long time now in digiKam. Metadata are very important for photographers. A photo without metadata is not a photo.</p>
<p>There is no news from specification published recently by Metadata Working Group. in digiKam and kipi-plugins, we use a common C++ wrapper aroud Exiv2 library : libkexiv2. All recomendations from this spec. are already implemented since one year in this wrapper...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17855"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-17855" class="active">You're my hero!</a></h3>    <div class="submitted">Submitted by dukat (not verified) on Wed, 2008-10-08 11:27.</div>
    <div class="content">
     <p>Wow, I'm happy to hear that. Did you have all the recommendations implemented? I'm asking especially about the second part of the spec, where they give hints which information should be stored in which type of Metadata (Exif, IPTC, XMP), how to synchronize overlapping information and how to chose which is the "correct" information if overlapping fields contradict.</p>
<p>And if you already have this done - you should definitely promote this in the Splash Screen / About box, everywhere :-) Your fame would even increase higher!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-17922"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-17922" class="active">I'm working on metadata</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:21.</div>
    <div class="content">
     <p>I'm working on metadata handling since a long time now in digiKam. Metadata are very important for photographers. A photo without metadata is not a photo.</p>
<p>There is no news from specification published recently by Metadata Working Group. in digiKam and kipi-plugins, we use a common C++ wrapper aroud Exiv2 library : libkexiv2. All recomendations from this spec. are already implemented since one year in this wrapper...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18138"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-18138" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://www.seyretvideo.net" rel="nofollow">izle</a> (not verified) on Sat, 2009-01-10 20:52.</div>
    <div class="content">
     <p>Wow, I'm happy to hear that. Did you have all the recommendations implemented? I'm asking especially about the second part of the spec, where they give hints which information should be stored in which type of Metadata (Exif, IPTC, XMP), how to synchronize overlapping information and how to chose which is the "correct" information if overlapping fields contradict.</p>
<p>And if you already have this done - you should definitely promote this in the Splash Screen / About box, everywhere :-) Your fame would even increase higher!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18140"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/375#comment-18140" class="active">&gt;Wow, I'm happy to hear that.</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-01-10 20:59.</div>
    <div class="content">
     <p>&gt;Wow, I'm happy to hear that. Did you have all the recommendations implemented? I'm asking especially about &gt;the second part of the spec,</p>
<p>Yes, and since a long time now (yes before spec. publication).</p>
<p>The wrapper library is libkexiv2. Look <a href="http://www.digikam.org/sharedlibs">here for details</a></p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
