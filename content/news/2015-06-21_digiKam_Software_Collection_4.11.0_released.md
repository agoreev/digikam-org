---
date: "2015-06-21T17:51:00Z"
title: "digiKam Software Collection 4.11.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.11.0. This release is the result of"
category: "news"
aliases: "/node/740"

---

<a href="https://www.flickr.com/photos/digikam/18841755995"><img src="https://c2.staticflickr.com/6/5525/18841755995_479ccb45ec_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.11.0. 
This release is the result of huge bugs triage on KDE bugzilla where more than 250 files have been closed as duplicate, invalid, or upstream states.
Thanks to <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while 
<a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port</a> and <a href="https://community.kde.org/GSoC/2015/Ideas#digiKam">GSoC 2015 projects</a> are in progress. Both are planed to be completed before end of this year.
</p>

<p>
With this 4.11.0 version, we hacked packaging release scripts for OSX. PKG installers are now available officially for Mountain Lion, Maverick, and Yosemite. Each one require one day of tunes and compilations with virtual machines. We have tried with older OSX releases, as Lion, but due to the huge list of ports to compile, some dependencies are not compilable as well, and require a lots of manual adjustments. So, we have limited OSX versions to support voluntary.
</p>

<p>
Under OSX, digiKam bundle package built with Macports is installed in a dedicated place /opt/digikam to prevent all conflicts with a standard Macports install. It will be easy to uninstall too, but deleting this directory as well. Later we will add a simpler way to perform uninstall task, but it's not a priority for the moment. Note that if you have installed and older digiKam version, installing a new one will uninstall previous one safety.
</p>

<a href="https://www.flickr.com/photos/digikam/18899673026"><img src="https://c1.staticflickr.com/1/293/18899673026_12cbdb5b8d_c.jpg" width="800" height="237"></a>

<p>
Considerate digikam under OSX as beta stage, not stable as under Linux, but at least now, you can install it on your Apple computer in few seconds instead to wait some hours of compilation.
The PKG prepare bundle background KDE used by digiKam to a suitable configuration and environment. Standard Macports install do not tune well all these points for digiKam.
The OSX hack will continue with next KDE4 releases of digiKam. Some dysfunctions have been already identified and we will try to fix it step by step.
</p>

<p>
Outside the fact that main components under OSX are managed through Macports to build PKG, we have tune compilation of last 
<a href="http://www.exiv2.org/changelog.html">Exiv2 0.25</a>, 
<a href="http://www.libraw.org/download#stable">LibRaw 0.16.2</a>, 
and <a href="http://lensfun.sourceforge.net/changelog/2015/05/10/Release-0.3.1-Changelog/">LensFun 0.3.1</a> 
which do not exist yet in Macports repository. Exiv2 includes huge fixes about video support which can crash digiKam, and Libraw/Lensfun add new recent camera supports. In the future, we plan to include Hugin CLI tools too required at run-time by Panorama and ExpoBlending tools.
</p>

<p>
The OSX digiKam PKG is not an Apple signed bundle installer. To do it we must pay 99$ by year which is very expensive for an Open Source project. If your computer is protected by Apple GateKeeper, you can bypass safety this protection at install time without to touch your computer settings. Just follow simple instructions given <a href="http://www.bu.edu/infosec/howtos/bypass-gatekeeper-safely/">in this tutorial</a>.
</p>

<p>
You can consult the huge list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.11.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.11.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball, OSX, and Windows installer can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE mirrors</a>.
</p>

<p>Have fun to use this new important digiKam release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-21046"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21046" class="active">That's great news! I'm on</a></h3>    <div class="submitted">Submitted by Alex C. (not verified) on Mon, 2015-06-22 12:41.</div>
    <div class="content">
     <p>That's great news! I'm on Windows, but digiKam grew in me as the best photo mgmt software around (and have tested most) because of its versatility and clear workflow. Unfortunately, this update seems to put Windows in a 2nd row, and there are some palpaple KDE bugs that hope had been addressed. Any chance to get a Win build soon?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21047"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21047" class="active">in pending...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-06-22 13:55.</div>
    <div class="content">
     <p>We have contacted Windows packager to build new Windows installer. I don't receive yet a response...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21049"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21049" class="active">Windows installer now available</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2015-07-07 12:10.</div>
    <div class="content">
     <p>Windows installer for 4.11.0 is now available on the KDE <a href="http://download.kde.org/stable/digikam/">download server</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21050"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21050" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by cc (not verified) on Sat, 2015-07-11 15:26.</div>
    <div class="content">
     <p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21051"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21051" class="active">4.11.0 windows install error</a></h3>    <div class="submitted">Submitted by Thijs Aartsma (not verified) on Sat, 2015-07-18 11:47.</div>
    <div class="content">
     <p>Error installing digiKam 4.11.0 on Windows 8.1:<br>
"Extract: error writing to file plasma_engine_mpris2.dll"</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21048"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21048" class="active">Well done!</a></h3>    <div class="submitted">Submitted by Hendric (not verified) on Tue, 2015-06-30 19:08.</div>
    <div class="content">
     <p>I am delighted to see yet another stable Digikam 4 release (while version 5 is in the making). The list of closed issues is impressively long as well!</p>
<p>Many thanks to the developers and keep up the good work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21052"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21052" class="active">Question about getting Digikam to use Nautilus in this version </a></h3>    <div class="submitted">Submitted by <a href="http://dkeats.com" rel="nofollow">Derek</a> (not verified) on Sat, 2015-07-18 19:27.</div>
    <div class="content">
     <p>Hi,Just wondering if anyone knows how to get Digikam to use Nautilus in this version. You used to be able to use systemsettings to dot hat, but now there is no more systemsettings, only systemsettings5, which gives the message "System Settings was unable to find any views, and hence has nothing to display." I am sure I am not the only one experiencing this, so if anyone has an answer it would be appreciated. Thanks, derek</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21053"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21053" class="active">Nice to see an easier way to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-07-23 11:41.</div>
    <div class="content">
     <p>Nice to see an easier way to install in OSX, but after opening I can't see any of old photos. "Could not start process Unable to create io-slave".<br>
Previously tried updating from 4.0 to 4.9 with mac ports but digikam wouldn't open afterwards.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21054"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21054" class="active">MacOSX</a></h3>    <div class="submitted">Submitted by <a href="http://www.etiennebretteville.com" rel="nofollow">Etienne</a> (not verified) on Thu, 2015-07-30 21:18.</div>
    <div class="content">
     <p>Hye and thanks for this huge software.</p>
<p>As for now, it's not compatible with macosx. What about opening a subscription for you to be able to use the mac App Store? I'll be really happy to donate.</p>
<p>All the infos can be found on macports: <a href="https://trac.macports.org/ticket/48081" target="_blank">https://trac.macports.org/ticket/48081</a></p>
<p>Can't wait to replace photos from google and apple as my picasa is no longer usable.</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21055"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/740#comment-21055" class="active">4.12.0</a></h3>    <div class="submitted">Submitted by Alex C. (not verified) on Thu, 2015-08-06 10:56.</div>
    <div class="content">
     <p>4.12.0 has become available:<br>
<a href="http://download.kde.org/stable/digikam/">http://download.kde.org/stable/digikam/</a></p>
         </div>
    <div class="links">» </div>
  </div>

</div>