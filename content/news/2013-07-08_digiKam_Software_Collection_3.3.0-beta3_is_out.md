---
date: "2013-07-08T09:03:00Z"
title: "digiKam Software Collection 3.3.0-beta3 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 3rd beta release of digiKam Software Collection 3.3.0. This version currently under"
category: "news"
aliases: "/node/699"

---

<a href="http://www.flickr.com/photos/digikam/9238609814/" title="digiKam3.3.0-beta3 by digiKam team, on Flickr"><img src="http://farm6.staticflickr.com/5443/9238609814_76e77b8652_z.jpg" width="640" height="512" alt="digiKam3.3.0-beta3"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 3rd beta release of digiKam Software Collection 3.3.0.
This version currently under development, including a new core implementation to manage faces, especially face recognition feature which have never been completed with previous release. <a href="http://en.wikipedia.org/wiki/Face_detection">Face detection</a> feature still always here and work as expected.</p>

<p><a href="https://plus.google.com/113704327590506304403/posts">Mahesh Hegde</a> who has work on <a href="http://en.wikipedia.org/wiki/Facial_recognition_system">Face Recognition</a> implementation has published a proof of concept demo on YouTube.</p>

<iframe width="560" height="315" src="http://www.youtube.com/embed/iaFGy0n0R-g" frameborder="0" allowfullscreen=""></iframe>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.3.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.3.0-beta3.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b> Release plan <a href="http://www.digikam.org/about/releaseplan">can be seen here...</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20576"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20576" class="active">Release plan</a></h3>    <div class="submitted">Submitted by jon (not verified) on Wed, 2013-07-10 16:53.</div>
    <div class="content">
     <p>Nice to hear from you. Do you really plan to release 3.4.0 without any beta-version prior to that? Do you already know when the windows version is ready?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20580"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20580" class="active">I plan to release 3.3.0 final for Windo without any beta-version</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-07-13 01:09.</div>
    <div class="content">
     <p>I'm almost done with 3.2.0 (just back-ported a few fixes and patched some things users reported on the 3.0.0/3.1.0 for Windows <a href="http://www.digikam.org/node/696">release announcement page</a>) and will release it in the next few days. I'll then try building 3.3.0-beta3 and provided that there's nothing glaring I should release 3.3.0 final about the same time as the final codebase is ready (so early August). Any bugs that affect 3.3.0 as far as Windows is concerned should be the same as for 3.2.0 and 3.1.0, so feel free to try 3.1.0. Otherwise, any bugs in 3.3.0 are likely to affect all platforms so I'd rather focus on fixing bugs and releasing 3.2.0 and 3.3.0 final rather than spend that time preparing a beta release.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20582"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20582" class="active">Thanks</a></h3>    <div class="submitted">Submitted by jon (not verified) on Sat, 2013-07-13 14:46.</div>
    <div class="content">
     <p>Thank you very much for your work and your quick answers. </p>
<p>The question about 3.4.0 was no special question for you but for the whole developer team because I am curious how one could release a new version without any beta versions (though I understand that this might be possible if you "only" port it for windows).</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20583"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20583" class="active">Great effort - thank you!</a></h3>    <div class="submitted">Submitted by Carl (not verified) on Sun, 2013-07-14 01:06.</div>
    <div class="content">
     <p>Just thought I'd say "thanks"! I've checked out some of those bugs you've been working on...  Great job there!! </p>
<p>Thanks again!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20592"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20592" class="active">I have used digiKam (Windows</a></h3>    <div class="submitted">Submitted by RobtA (not verified) on Sun, 2013-07-21 23:25.</div>
    <div class="content">
     <p>I have used digiKam (Windows and Linux) for awhile. This is not a bug, but it is in the category of "unexpected behavior" :</p>
<p>When editing an image (raw) in digiKam, I try the color adjust saturation controls. Even though I use the selection box for per-channel adjustment (r, g, b) it seems to be the case that my adjustment is applied to ALL channels equally. That is, if I select the red channel and increase saturation there, the change is also applied to green and blue. This behavior is different from Gimp, where my adjustment is applied only to the selected channel.</p>
<p>I am working in 16-bit integer mode from CR2 RAW, if that makes a difference.</p>
<p>I do not know how long this behavior has been in digiKam. Or, do I mis-understand usage?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20601"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/699#comment-20601" class="active">Release 3.3.0</a></h3>    <div class="submitted">Submitted by Antoni Olivella (not verified) on Mon, 2013-08-05 20:41.</div>
    <div class="content">
     <p>Any date for the release of 3.3.0 Windows?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
