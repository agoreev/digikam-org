---
date: "2011-09-09T13:52:00Z"
title: "digiKam Software Collection 2.1.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.1.0 as bug-fixes release,"
category: "news"
aliases: "/node/622"

---

<a href="http://www.flickr.com/photos/digikam/6093026801/" title="digiKam-sonyA500-panorama by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6085/6093026801_bd2563322b_z.jpg" width="640" height="360" alt="digiKam-sonyA500-panorama"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.1.0 as bug-fixes release, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.1.0">53 fixed bugs</a>.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.1.0. This release features a new tool ready for production coming from Google Summer of Code 2011 project dedicated to stitch photos as panorama, and <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.1.0">10 bug-fixes</a>.</p>

<a href="http://www.flickr.com/photos/digikam/6060217962/" title="digiKam-panorama-tool by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6207/6060217962_6d8e6801cc_z.jpg" width="296" height="640" alt="digiKam-panorama-tool"></a>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-19999"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-19999" class="active">Just in the nick of time :)</a></h3>    <div class="submitted">Submitted by BajK (not verified) on Fri, 2011-09-09 15:42.</div>
    <div class="content">
     <p>Today I had a meeting with one of my customers and he asked if I could put a panorama view on his webpage, sure, took photos. And now you write this blog post :) You can’t believe how happy this makes me right now :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20000"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20000" class="active">Win32</a></h3>    <div class="submitted">Submitted by <a href="http://www.croucrou.com" rel="nofollow">croucrou</a> (not verified) on Fri, 2011-09-09 17:52.</div>
    <div class="content">
     <p>hi<br>
Is there a version for windows in the pipe ?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20001"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20001" class="active">Where to download the package?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-09-10 00:55.</div>
    <div class="content">
     <p>Is there a distribution which provides its package for a new versions of DigiKam?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20002"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20002" class="active">Yep</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-09-10 10:45.</div>
    <div class="content">
     <p>Yes there is, its name is Gentoo. Or Funtoo or Sabayon.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20003"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20003" class="active">Cannot open database</a></h3>    <div class="submitted">Submitted by Oliver Wieland (not verified) on Sat, 2011-09-10 13:15.</div>
    <div class="content">
     <p>I upgraded from 2.0.0 to 2.1.0 and now I get an error "Could not open database" (Message is in german, so I don't know the exact english text).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20004"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20004" class="active">I had the same, and this</a></h3>    <div class="submitted">Submitted by BroX (not verified) on Sat, 2011-09-10 14:41.</div>
    <div class="content">
     <p>I had the same, and this solved it:</p>
<p>In digikam, go to Settings -&gt; Configure digiKam -&gt; Database -&gt; OK</p>
<p>and it opens your database.</p>
<p>Hope this helps.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20005"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20005" class="active">yes, that helped, thanks!</a></h3>    <div class="submitted">Submitted by Oliver Wieland (not verified) on Sat, 2011-09-10 15:02.</div>
    <div class="content">
     <p>yes, that helped, thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20018"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20018" class="active">I have the same bug with db not found..</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Sun, 2011-09-11 19:30.</div>
    <div class="content">
     <p>and additionally, when I click to skip it, digikam fails during starting on "Initializing Main View". I have Kubuntu 11.04 with KDE4.7 (as mentioned lower in the discussion, I do have both repo's "kubuntu-backports" and the "extra").</p>
<p>After years of using digiKam I must tell that, even it has some bugs time after time, this is the best photo library tool I have ever played with.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20021"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20021" class="active">my bad,I did overlook that it is Philip's kubuntu-backports repo</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Mon, 2011-09-12 21:47.</div>
    <div class="content">
     <p>so I just add ppa:philip5/kubuntu-backports as everyone else with kubuntu and KDE 4.7 and it is running ok now.</p>
<p>Once again, I must congrats to digiKam developers for such a killer application!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20006"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20006" class="active">dng converter for digiKam v2.1.0</a></h3>    <div class="submitted">Submitted by Bert Broekstra (not verified) on Sat, 2011-09-10 22:17.</div>
    <div class="content">
     <p>DNG Converter shows a blank (black) photo result when converting Nikon NEF to DNG using release v2.1.0.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20029"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20029" class="active">Fixed...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-09-16 21:30.</div>
    <div class="content">
     <p>See this entry : https://bugs.kde.org/show_bug.cgi?id=282101</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20007"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20007" class="active">Error after upgrading from 2.0 to 2.1</a></h3>    <div class="submitted">Submitted by ruedihofer (not verified) on Sat, 2011-09-10 22:39.</div>
    <div class="content">
     <p>Dear Gilles</p>
<p>I love Digikam so much and use it very often. Such a great software!! Unfortunately after upgrading, I get</p>
<p>digikam: symbol lookup error: digikam: undefined symbol: _ZN11KExiv2Iface14AltLangStrEdit15setLinesVisibleEj</p>
<p>and digikam 2.1 refuses to start! Any idea?</p>
<p>Cheers, Ruedi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20008"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20008" class="active">I am getting the exact same</a></h3>    <div class="submitted">Submitted by patrick wilken (not verified) on Sun, 2011-09-11 01:02.</div>
    <div class="content">
     <p>I am getting the exact same error on Kubuntu 11.04 with KDE 4.7, after upgrading KDE.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20009"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20009" class="active">binary compatibility...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-09-11 08:38.</div>
    <div class="content">
     <p>It's a binary compatibility issue with libkexiv2. Package must be updated...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20011"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20011" class="active">Which version of libexiv2? I</a></h3>    <div class="submitted">Submitted by patrick wilken (not verified) on Sun, 2011-09-11 09:50.</div>
    <div class="content">
     <p>Which version of libexiv2? I have libkexiv2-10 installed. When I try to update I get options for:</p>
<p>libkexiv2-10<br>
 libkexiv2-8<br>
libkexiv2-9<br>
libkexiv2-10-dev<br>
libkexiv2-data<br>
libkexiv2-dbg<br>
libkexiv2-dev </p>
<p>Is there a newer version not in the repositories yet?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20014"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20014" class="active">libs</a></h3>    <div class="submitted">Submitted by ruedihofer (not verified) on Sun, 2011-09-11 15:13.</div>
    <div class="content">
     <p>I still have these problems:</p>
<p>QFileSystemWatcher: failed to add paths: /home/hofer/.config/ibus/bus<br>
&gt; solved by creating respecive files manually</p>
<p>digikam: symbol lookup error: digikam: undefined symbol: _ZN11KExiv2Iface14AltLangStrEdit15setLinesVisibleEj<br>
&gt; so idea yet. I have these libs installed:<br>
&gt; ldd /usr/bin/digikam |  grep exiv<br>
&gt;        libkexiv2.so.10 =&gt; /usr/lib/libkexiv2.so.10 (0x00007f890e71c000)<br>
&gt;        libexiv2.so.10 =&gt; /usr/lib/libexiv2.so.10 (0x00007f8904236000)<br>
&gt; Is there a ppa with the right one?</p>
<p>Cheers, Ruedi</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20017"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20017" class="active">If you are using KDE 4.7 with</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-09-11 18:24.</div>
    <div class="content">
     <p>If you are using KDE 4.7 with Ubuntu 11.04 and the packages from my PPA then you need both my "kubuntu-backports" and the "extra" PPA activated too which have Digikam built against KDE 4.7 instead if KDE 4.6 that comes with standard Ubuntu 11.04.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20019"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20019" class="active">ppa added .. works now with kde4.7</a></h3>    <div class="submitted">Submitted by ruedihofer (not verified) on Sun, 2011-09-11 21:16.</div>
    <div class="content">
     <p>Dear all</p>
<p>I had to add ppa:philip5/kubuntu-backports to the repos. Do an update .. and it works. Thanks all for your help!</p>
<p>Ruedi</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20020"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20020" class="active">going well</a></h3>    <div class="submitted">Submitted by <a href="http://directsalesmarketing.org/" rel="nofollow">Anonymous</a> (not verified) on Mon, 2011-09-12 07:41.</div>
    <div class="content">
     <p>a similar issue went same for me when i used nokia n86 yeaqrs back.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20022"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/622#comment-20022" class="active">Digikam2.1 for win</a></h3>    <div class="submitted">Submitted by <a href="http://materia.grix.mine.nu" rel="nofollow">grix</a> (not verified) on Tue, 2011-09-13 13:31.</div>
    <div class="content">
     <p>I'm interested in digikam 2.1 for win too ¿ will be a binary for it ?<br>
Thanks</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
