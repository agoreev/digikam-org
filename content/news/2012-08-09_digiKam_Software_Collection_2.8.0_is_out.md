---
date: "2012-08-09T14:20:00Z"
title: "digiKam Software Collection 2.8.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month since 2.70 release, digiKam team is proud to announce the digiKam Software Collection 2.8.0, as bug-fixes"
category: "news"
aliases: "/node/662"

---

<a href="http://www.flickr.com/photos/digikam/7746269100/" title="splash2.8.0 by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8445/7746269100_fe9945392f.jpg" width="500" height="307" alt="splash2.8.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month since 2.70 release, digiKam team is proud to announce the digiKam Software Collection 2.8.0, as bug-fixes release.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=2.8.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20321"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20321" class="active">version number</a></h3>    <div class="submitted">Submitted by moltonel (not verified) on Thu, 2012-08-09 16:15.</div>
    <div class="content">
     <p>Thanks for another update :)</p>
<p>I wonder about version numbering though : if there are only bugfixes in the release, why isn't it 2.7.1 ? When I see a x.y.z version scheme, I expect x=major feature/rewrite, y=misc feature z=bugfix. Digikam doesn't seem to follow that definition lately. Or if it seems like too much trouble deciding which number to bump, what about YYYY.MM ? No indication of what's in the release, but at least no flawed user expectation either.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20322"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20322" class="active">I think the last digit should</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-08-10 00:55.</div>
    <div class="content">
     <p>I think the last digit should just be dropped and be done with it. Minor releases can be done when ever seen necessary (from minor bug fixes to small feature releases) and major releases when big changes are done or when the minor number gets too big.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20323"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20323" class="active">Congrats</a></h3>    <div class="submitted">Submitted by <a href="http://www.thebluemint.net" rel="nofollow">ronnoc</a> (not verified) on Fri, 2012-08-10 00:59.</div>
    <div class="content">
     <p>Looks like a lot of bugs were squashed here. Congratulations!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20324"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20324" class="active">I love digiKan</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-08-13 21:31.</div>
    <div class="content">
     <p>I love digiKam (I'm running v. 2.5 on my Vista-Computer without any serious troubles)! But when will you release a Windows-version 2.80?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20400"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20400" class="active">digiKam 2.8.0 and 2.9.0 for Windows released today</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:05.</div>
    <div class="content">
     <p>Unfortunately, release schedules for Windows are a little irregular because KDE SC for Windows (the software that digiKam relies on) is rather unstable, including the build environment. This means that to get the latest bug fixes the build environment has to be constantly updated. There are very few developers working on KDE SC for Windows, so many compilation problems are encountered with each release. Since we are doing this in our spare time it can take quite a bit of time with each release, and releases will be sporadic. Unfortunately, that is just the way it will have to be for the foreseeable future. Regardless, you can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20325"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20325" class="active">Cool</a></h3>    <div class="submitted">Submitted by <a href="http://www.archlinux.me/msx" rel="nofollow">Martin</a> (not verified) on Tue, 2012-08-14 19:57.</div>
    <div class="content">
     <p>Can't wait to have it running in my Arch system!<br>
Thanks for all your work :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20326"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20326" class="active">crash</a></h3>    <div class="submitted">Submitted by lobo (not verified) on Thu, 2012-08-16 08:03.</div>
    <div class="content">
     <p>digikam crashed on my debian sid 64 bit - form 2.7 verzion when I try geotag, date repair and more :( - probably something with libkipi :-/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20338"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20338" class="active">For me the same on a OpenSUSE</a></h3>    <div class="submitted">Submitted by uli (not verified) on Sat, 2012-08-25 16:25.</div>
    <div class="content">
     <p>For me the same on a OpenSUSE 12.1 x86_64</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20339"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20339" class="active">no libkgeomap...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2012-08-25 18:38.</div>
    <div class="content">
     <p>...but it's not reproducible here. Sound like a packaging problem...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20341"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20341" class="active">On big problem with digikam</a></h3>    <div class="submitted">Submitted by uli (not verified) on Wed, 2012-08-29 17:24.</div>
    <div class="content">
     <p>On big problem with digikam is, that I always have to use the unstable repositories for digikam. It would be really nice, if digikam would run without any special considerations on a recent Linux-Distribution.</p>
<p>The main problem seems to be the libkgeomap 2.8.0+git20120828-1.1-x86_64 from the openSUSE_12.1 unstable repository. With this libkgeomap also digikam 2.5 crashes.</p>
<p>So currently I have to revert to the stable repositories, where I only can get digikam 2.5.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20327"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20327" class="active">how to lose a good reputation</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-08-17 16:44.</div>
    <div class="content">
     <p>Hello,</p>
<p>first, I like KDE, k3b, and other K based stuff. In past I spent some time for digikam to send some bug reports to pkg debian team. And I further use this application with kipi-plugins but I'm searching for an alternative.</p>
<p>Reasons are not some temporary bugs, for some weeks there were problems with white-balance, some problems with cameras have to do with gphoto. Sometimes there were some discussions with pkg-debian team, they didn't accept, that some versions of digikam were build against  developement stuff of kde graphic libraries, which wasn't released as stable. I'm not a developer and cannot judge about it.</p>
<p>But since version 2.6.0 there is a problem with all functions, which need cimg library and digikam crashes. As a reaction of this bug I've found this comment of a core developer of digikam:<br>
"It crash in CImg library, which is not maintained anymore. I cannot<br>
update or fix it.</p>
<p>I try to find a student to work on this code to port from CImg to Gmic<br>
library which is the replacement... "</p>
<p>It seems, Gilles, that you've not found a student yet, with version 2.7.0 digikam crashes, now with 2.8.0 digikam crashes. I've changed the system, to verify, if this bug has to do with the status of Sid / siduction, but on OpenSUSE 12.1 with digikam 2.7.0 it is the same behaviour. Yesterday I've installed a fresh Kubuntu 12.04, added the digikam ppa of Philipp Johnson, and digikam 2.8.0 crashes.</p>
<p>"Bearbeiten Sie Ihre Fotos wie ein Profi", this you can read when starting digikam. Sorry, but such a behavoiur of an application, which is dicussed as a reference application under Linux and such a reaction of a core developer has not to do with professional work.</p>
<p>And if all cimg stuff isn't important for you, we user have to accept this but then it would be better to deactivate this features. Offering broken features with now three versions of digikam and talking about a professional work with digikam is not acceptable for a reference application.</p>
<p>Sorry for my bad english, and sorry for this rant, but I'm annoyed, not because of a bug, but because of handling it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20328"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20328" class="active">i agree with that :)</a></h3>    <div class="submitted">Submitted by <a href="http://www.netlibel.com/" rel="nofollow">Net Libel</a> (not verified) on Sun, 2012-08-19 11:27.</div>
    <div class="content">
     <p>i agree with that :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20332"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20332" class="active">A Suggestion</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-08-21 16:06.</div>
    <div class="content">
     <p>I haven't verified the accuracy of the above comment myself but if any of the developers of digiKam happen to be reading this, I would suggest simply copying CImg into the digiKam project as a static lib for now and fixing the bug there. (heck, even include an old version of CImg if it worked before) Then put porting to Gmic on the roadmap for the future. That way it will keep users happy and the project should remain stable until the port is done.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20333"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20333" class="active">digikam</a></h3>    <div class="submitted">Submitted by Holgerw (not verified) on Wed, 2012-08-22 07:41.</div>
    <div class="content">
     <p>Hello,</p>
<p>first comment about reputation was from me, I've forgotten to put my name in the name-field.</p>
<p>Your suggestion could be an option, but about the best procedere I cannot judge because I'm a user, not a developer.</p>
<p>But you can be sure, the described error is not a fake and the qutoe about finding a student is from Gilles, not from me.</p>
<p>And I hope, I've pointed out, that I don't want to start a stupid flame like KDE versus Gnome or anything similar.</p>
<p>Kind regards,<br>
  Holger</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20334"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20334" class="active">investiguations are under progress for next 2.9.0</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2012-08-23 19:18.</div>
    <div class="content">
     <p>Look here :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=303119#c7</p>
<p>Crash is relevant o CImg library bug, not digiKam as well... digiKam is not the rest of the opensource world...</p>
<p>Before to critic digiKam project, take a care !</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20340"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20340" class="active">Hello Gilles,
"digiKam is not</a></h3>    <div class="submitted">Submitted by Holgerw (not verified) on Sun, 2012-08-26 14:57.</div>
    <div class="content">
     <p>Hello Gilles,</p>
<p>"digiKam is not the rest of the opensource world..."</p>
<p>In this point you're right, indeed, we've a lot of stuff in free software world and I've clarified, that digikam is part of a software pool, you can call reference applications for Linux. :-)</p>
<p>"Crash is relevant o CImg library bug, not digiKam as well..."</p>
<p>That digikam need other stuff, a lot of libraries, is known by me, I'm not a developer but a linux user for a long time. :-)</p>
<p>"Before to critic digiKam project, take a care !"</p>
<p>Gilles, I did not critzise, that there are some bugs in stuff, digikam depends on, I've mentioned gphoto as an example for some errors, which have not to do with your work. My rant is against your reaction after getting a bug report about it. And digikam 2.7.0 and also 2.8.0 furthermore offers functions, which are building against broken cimg stuff. Knowing about it but offering it and commenting it with "I've to find a student" german spoken "Mal schauen, wann ich jemanden dafür finde" that is the purpose of my rant, not the bug situation itself.</p>
<p>But it seems, that in 2.8.0+git20120821-1.2 this bug has gone, thanks a lot.</p>
<p>Apart from that and it is not a joke: I spent some time here for my criticism because I like digikam and kipi-plugins. And I hope, you understand it in a right way.</p>
<p>Kind regards,<br>
  Holger</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20330"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20330" class="active">Is there any news about Bug 303850?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-08-21 08:30.</div>
    <div class="content">
     <p>As discussed with the previous version, digiKam won't start on MS Windows with an USB drive plugged in. This is rather annoying and makes digiKam unusable for me.<br>
Is there any intention to fix this? Maybe a roadmap?<br>
Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20335"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20335" class="active">Where did you find a windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2012-08-23 19:30.</div>
    <div class="content">
     <p>Where did you find a windows version 2.8.0. after all?</p>
<p>It really becomes annoying to always having to come back here, look for a new windows version and not finding anything. I see it's not made for windows users, though.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20336"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20336" class="active">Where to find the Windows Version</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-08-24 07:40.</div>
    <div class="content">
     <p>Look at:<br>
http://sourceforge.net/projects/digikam/files/digikam/</p>
<p>You find the link here under "Download"\"Package", at the bottom of the screen.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20337"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20337" class="active">Sorry, no 2.8 until now</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-08-24 07:44.</div>
    <div class="content">
     <p>Sorry, just saw 2.8.0 and did not check, no Windows Installer for 2.8.0 yet.</p>
<p>But doesn't matter for me as long as this bug is in.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20401"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20401" class="active">digiKam 2.8.0 and 2.9.0 for Windows released today</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:08.</div>
    <div class="content">
     <p>You can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>. As mentioned above, <a href="https://bugs.kde.org/show_bug.cgi?id=303850">bug 303850</a> has been fixed with the update from KDE SC 4.8.3 to KDE SC 4.8.5.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20402"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/662#comment-20402" class="active">Bug fixed in digiKam 2.8.0 and 2.9.0 for Windows released today</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:12.</div>
    <div class="content">
     <p>Sorry for the inconvenience, its annoying for us as well. Unfortunately time is very limited as there are very few Windows developers for KDE SC (the software that digiKam depends upon). It turns out the bug was due to KDE SC for Windows, not digiKam. This has been fixed by moving from KDE SC 4.8.3 that I used for digiKam 2.6.0 and 2.7.0 to KDE SC 4.8.5 for digiKam 2.8.0 and 2.9.0. You can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>. Please file a new bug report if you encounter any further troubles.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
