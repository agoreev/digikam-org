---
date: "2005-09-01T20:38:00Z"
title: "digiKam wins TUX 2005 Readers Choice Award"
author: "site-admin"
description: "digiKam has been awarded the TUX 2005 Readers' Choice Award in the category Favorite Digital Photo Management Tool. We are very proud we received this"
category: "news"
aliases: "/node/31"

---

<img border="0" align="right" id="txtimg" src="/files/tuxaward.png" alt="tuxarward">

digiKam has been <a href="http://dot.kde.org/1125833662">awarded</a> the TUX 2005 Readers' Choice Award in the category <b>Favorite Digital Photo Management Tool</b>.<p></p>

We are very proud we received this prize, especially because it is a prize for which the users have voted. <a href="http://www.tuxmagazine.com">TUX</a> is the first and only magazine for the new Linux user and is dedicated to promoting and simplifying the use of Linux on the PC.<p></p>

We are in the middle of an impressive list of Winners: KDE (as favorite desktop environment), Firefox, Thunderbird, Gaim, gpilot, OpenOffice, XMMS, MPlayer, Frozen Bubble, vi and The Gimp.<p></p>

The people behind digiKam are: Renchi Raju, JÃ¶rn Ahrens, a lot of contributors and a large amount of translators!<p></p>

<b>Thanks for voting on digiKam.</b>

<div class="legacy-comments">

</div>