---
date: "2015-08-11T09:25:00Z"
title: "digiKam at Randa Meeting 2015"
author: "digiKam"
description: "The Randa Meeting is an annual KDE sprint that takes place in Randa, Switzerland. The KDE project is holding a fundraiser to support the Randa"
category: "news"
aliases: "/node/742"

---

<img src="http://randa-meetings.ch/wp-content/uploads/2012/11/cropped-web2.png" width="800" height="170">

<p>The Randa Meeting is an annual KDE sprint that takes place in Randa, Switzerland. The KDE project is holding a fundraiser to support the Randa Meetings. Participants donate their time to help improve the software you love and this is why we need money to cover hard expenses like accommodation and travel to get the volunteer contributors to Randa.</p>

<p>
The Randa Meetings will benefit everyone who uses KDE software. This year, digiKam team will go to Randa meeting to continue and finalize the KF5/Qt5 port. You can support the Randa Meetings by making a donation, <a href="https://www.kde.org/fundraisers/kdesprints2015/">following the official page</a>. Thanks in advance...
</p>
<div class="legacy-comments">

</div>