---
date: "2011-10-12T10:15:00Z"
title: "Show Photos on Google Earth and Google Maps with digiKam"
author: "Dmitri Popov"
description: "digiKam offers several ways to showcase your photos. You can view images as a slideshow, push them to a photo sharing service of your choice,"
category: "news"
aliases: "/node/629"

---

<p>digiKam offers several ways to showcase your photos. You can view images as a slideshow, push them to a photo sharing service of your choice, and even export them as a static HTML gallery.</p>
<p><img class="alignnone size-medium wp-image-2032" title="digikam_kmlexport" src="http://scribblesandsnaps.files.wordpress.com/2011/10/digikam_kmlexport.png?w=500" alt="" width="500" height="392"></p>
<p>But that's not all; digiKam can output selected photos as a KML bundle, so you can view your snaps on the <a href="http://maps.google.com/">Google Maps</a> service and the Google Earth application.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/10/12/show-photos-on-google-earth-and-google-maps-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>