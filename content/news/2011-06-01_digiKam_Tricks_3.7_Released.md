---
date: "2011-06-01T09:57:00Z"
title: "digiKam Tricks 3.7 Released"
author: "Dmitri Popov"
description: "This release includes the following new material: Assign Keyboard Shortcuts to Tags Configure the Main Toolbar in digiKam Host Your Own Photo Gallery with Piwigo"
category: "news"
aliases: "/node/606"

---

<p>This release includes the following new material:</p>
<ul>
<li>Assign Keyboard Shortcuts to Tags</li>
<li>Configure the Main Toolbar in digiKam</li>
<li>Host Your Own Photo Gallery with&nbsp;Piwigo</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/06/01/digikam-tricks-3-7-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>