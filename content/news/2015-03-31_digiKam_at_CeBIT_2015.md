---
date: "2015-03-31T12:54:00Z"
title: "digiKam at CeBIT 2015"
author: "Dmitri Popov"
description: "The Computec Media publishing company had kindly invited yours truly to this year's CeBIT trade show in Hannover to give a talk on a digiKam-related"
category: "news"
aliases: "/node/734"

---

<a href="https://www.flickr.com/photos/120883042@N02/16308821284" title="Linux New Media Awards 2015 – Group shot showing the winners and presentators by Computec Media GmbH, on Flickr"><img src="https://farm8.staticflickr.com/7587/16308821284_9f2d8b24ec_z.jpg" width="640" height="412" alt="Linux New Media Awards 2015 – Group shot showing the winners and presentators"></a>

<p>The Computec Media publishing company had kindly invited yours truly to this year's CeBIT trade show in Hannover to give a talk on a digiKam-related topic at the Open Source Forum. The talk entitled <a href="http://scribblesandsnaps.com/2015/03/31/digital-asset-management-with-digikam/">Digital Assets Management with digiKam</a> is available for your perusal on my <a href="http://scribblesandsnaps.com/2015/03/31/digital-asset-management-with-digikam/">Scribbles and Snaps</a> blog. I also had the pleasure of presenting the Readers' Choice award at the Linux Media Awards ceremony. And I was particularly happy to do that, since the award for the best open source photo management application went to digiKam. Unfortunately, none of digiKam's developers could make it to CeBIT to accept the award, but I'll deliver it to them when the opportunity presents itself.</p>

<p>Here is the laudatory speech I gave at the award ceremony.</p>

<p><em>Digital photography is as much about digital cameras as it is about software that lets you manage and process your snapshots. And there is a cornucopia of Linux applications that can help you to process and manage images. The GIMP, darktable, Lightzone, gThumb -- there are dozens of Linux photography applications out there. Some of these applications are perfect for keeping tabs on large collections of photos, while others excel at processing RAW and image files. But there is probably only one Linux-based application that offers a comprehensive tool set that covers the entire photographic workflow: from transferring photos from the camera, to processing and sharing them. And the name of this wonder application is digiKam. The application basically allows you to do two things: arrange all your photos into albums and manage photos, RAW files, and videos using a wide selection of tools. digiKam also features powerful editing tools for retouching and improving your snapshots as well as processing RAW files.</em></p>

<p><em>When I switched to Linux, digiKam was the first photo management application I used to manage and process my photos, and I've been using and writing about digiKam ever since. Even after many years of almost daily use and a dozen of articles, I still discover new ways to use digiKam.</em></p>

<p><em>digiKam is often compared to Adobe Lightroom, which is an application popular among enthusiasts and professional photographers using Windows and Mac OS. This actually can be considered an achievement in its own right: a free software project maintained by a relatively small group of developers is actually pitched against a commercial application maintained by an army of paid developers working for a multi-billion dollar company. But the best part is that digiKam gives Lightroom a serious run for its money. digiKam is not only free in every sense of the word, it also offers functionality that sometimes beats its commercial rival. To give you an example, digiKam offered geotagging capabilities long before Lightroom.</em></p>

<p><em>Recently, I had the pleasure of meeting the digiKam developers by attending one of their regular coding sprints as an observer. This is a fantastic group of talented coders who are passionate about digiKam. And they have some serious stamina too: it's simply mind-boggling how they can code for hours on end several days in a row.</em></p>

<p><em>Apparently, I'm not the only one who appreciates digiKam. Several thousand readers of Computec's magazines and websites have spoken, and the award for the best open source photo management application this year goes to digiKam. digiKam has won by a significant margin, beating two other capable applications: the GIMP and darktable. digiKam celebrates its 14th anniversary this year, and this award is a token of recognition and appreciation of what digiKam's developers have achieved.</em></p>

<p><em>Unfortunately, none of the digiKam's developers could make it to CeBIT, but, rest assured, this award will be delivered to them by yours truly.</em></p>
<div class="legacy-comments">

</div>