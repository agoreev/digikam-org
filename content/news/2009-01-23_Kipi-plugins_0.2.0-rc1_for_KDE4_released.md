---
date: "2009-01-23T13:34:00Z"
title: "Kipi-plugins 0.2.0-rc1 for KDE4 released"
author: "digiKam"
description: "The first KDE4 release candidate of digiKam plugins box is out. With this new release, Smug tool is now able to import pictures from SmugMug"
category: "news"
aliases: "/node/421"

---

<p>The first KDE4 release candidate of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/3218329006/" title="smugmugimporttool-kde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3484/3218329006_c398ddf5bb.jpg" width="500" height="313" alt="smugmugimporttool-kde4"></a>

<p>With this new release, <b>Smug tool</b> is now able to import pictures from <a href="http://www.smugmug.com">SmugMug web service</a> to your computer.</p>

<p>Since beta6, a new plugin named <b>FbExport</b> is dedicated to export images to <a href="http://www.facebook.com">FaceBook web service</a>.</p>

<p>A plugin is available to batch convert RAW camera images to Adobe DNG container (see <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">this Wikipedia entry</a> for details). To be able to compile this plugin, you need last libkdcraw from svn trunk (will be released with KDE 4.2). To extract libraries source code from svn, look at <a href="http://www.digikam.org/download?q=download/svn#checkout-kde4">this page</a> for details.</p>

<p>Kipi-plugins is compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>General</b>        : Plugins now use metadata settings shared with kipi host application.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br>
<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br>
<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br>
<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br>
<b>RemoveRedEyes</b>  : Added an option to preview the correction of the currently selected image.<br>
(http://www.smugmug.com).<br>
<b>FbExport</b>       : New plugin to export images to a remote Facebook web service (http://www.facebook.com).<br>
<b>FlickrExport</b>   : Add support of Photosets.<br>
<b>Smug</b>           : New plugin to export images to a remote SmugMug web service.<br> <b>Smug</b>           : Add support for Album Templates.<br>
<b>Smug</b>           : Import (download) of images from SmugMug web service.<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digiKam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>
018 ==&gt; 169637 : MetadataEdit       : EXIF Data Editing Error.<br>
019 ==&gt; 174954 : HTMLExport         : Support for remote urls.<br>
020 ==&gt; 176640 : MetadataEdit       : Wrong reference to website.<br>
021 ==&gt; 176749 : TimeAdjust         : Incorrect time/date setting with digiKam.<br>
022 ==&gt; 165370 : GPSSync            : crash when saving geocoordinates.<br>
023 ==&gt; 165486 : GPSSync            : Next and Previous photo button for editing geolocalization coordinates.<br>
024 ==&gt; 175296 : GPSSync            : Geolocalization a handy gui to manage all about geotagging.<br>
025 ==&gt; 152520 : MetadataEdit       : Unified entry of metadata.<br>
026 ==&gt; 135320 : Calendar           : Calendar year do not match locale.<br>
027 ==&gt; 116970 : Calendar           : New calendar generator plugin features.<br>
028 ==&gt; 177999 : General            : Kipi-plugins Shortcuts are not listed in the shortcuts editor from Kipi host application.<br>
029 ==&gt; 160236 : JPEGLossLess       : Image Magick convert operations return with errors.<br>
030 ==&gt; 144183 : RawConverter       : Path in RAW-converter broken with the letter "ß"<br>
031 ==&gt; 178495 : MetadataEdit       : Problem with metadata editor.<br>
032 ==&gt; 175219 : PicasaWebExport    : Album creating wrong date in picasaweb.<br>
033 ==&gt; 175222 : PicasaWebExport    : Unable to create non listed album.<br>
034 ==&gt; 166672 : FbExport           : Export to Facebook.<br>
035 ==&gt; 129623 : FlickrExport       : Option to upload to specific photoset.<br>
036 ==&gt; 161775 : FlickrExport       : Photoset support.<br>
037 ==&gt; 179439 : JPEGLossLess       : Rotation or flip do not update internal thumbnail.<br>
038 ==&gt; 180245 : TimeAdjust         : Timeadjust is not building on mac os x leopard.<br>
039 ==&gt; 180656 : PicasaWebExport    : Wrong file selection filter in export picasaweb.<br>

<div class="legacy-comments">

  <a id="comment-18157"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/421#comment-18157" class="active">Kubuntu packages</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Fri, 2009-01-23 14:04.</div>
    <div class="content">
     <p>Packages for Kubuntu/Jaunty are already in the repository. Kubuntu/Intrepid users (that use KDE 4.2/RC from kubuntu-experimental), can get packages here:<br>
https://launchpad.net/~digikam-experimental/+archive/ppa</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18179"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/421#comment-18179" class="active">galleryexport bug</a></h3>    <div class="submitted">Submitted by SOULflyB (not verified) on Tue, 2009-01-27 21:58.</div>
    <div class="content">
     <p>Hello,</p>
<p>I'm afraid to see that the bug https://bugs.kde.org/show_bug.cgi?id=178204 is still alive.<br>
I'm hoping it's just a matter of time : the galleryexport plugin is totally useless ... and it's very sad because the rest of digikam works great !</p>
<p>Thanks for your work, hoping someone will have time to look at this bug.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18199"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/421#comment-18199" class="active">Batch Raw Converter fails to saves JPEG... sort of (WINDOWS)</a></h3>    <div class="submitted">Submitted by GenoSV (not verified) on Fri, 2009-01-30 11:48.</div>
    <div class="content">
     <p>I love how this works on Windows now (my stupid laptop works no good with Linux).<br>
Anyway, when running the Batch Raw Converter from within Digikam (or Gwenview too for that matter) and try to batch process to DNG images to JPEG it starts to process, but after a while gives me the error:<br>
"Failed to save image E:/Digicam pictures/2009-01-26/imgp7689.jpg".</p>
<p>Although, strangest thing is that if I look in the folder it has created a file called ".kipi-rawconverter-tmp-1233312081", which if changed to "picture.jpg" etc is a perfect JPEG picture. </p>
<p>Converting to PNG works like a charm though. This is not the biggest problem since I can just add the .jpg-extension myself, but thought I inform about this.<br>
(don't know how/where to file for bugs really so sorry if this isn't the place).</p>
<p>also,<br>
this is on Windows XP, MinGW packages, Kipi-plugins 0.2.0-rc1, DNG images from a Pentax K10D</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18558"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/421#comment-18558" class="active">I'm afraid to see that the</a></h3>    <div class="submitted">Submitted by <a href="http://www.club-penguin.org/" rel="nofollow">clubpenguin</a> (not verified) on Mon, 2009-05-25 08:33.</div>
    <div class="content">
     <p>I'm afraid to see that the bug https://bugs.kde.org/show_bug.cgi?id=178204 is still alive. I'm hoping it's just a matter of time : the galleryexport plugin is totally useless and it's very sad because the rest of digikam works great.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
