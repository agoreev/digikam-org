---
date: "2010-01-25T17:06:00Z"
title: "New tool to add vignetting"
author: "julien"
description: "Accordingly with KDE bugzilla entries #177827 and #218906, i have slightly modified the vignetting tool, now you can add vignetting to your pictures: Vignetting can"
category: "news"
aliases: "/node/496"

---

<p>Accordingly with KDE bugzilla entries <a href="https://bugs.kde.org/show_bug.cgi?id=177827">#177827</a> and <a href="https://bugs.kde.org/show_bug.cgi?id=218906">#218906</a>, i have slightly modified the vignetting tool, now you can add vignetting to your pictures:</p>
<p><a href="http://www.flickr.com/photos/julien_narboux/4281258900/" title="Adding vignetting to a picture using Digikam by JulienNarboux, on Flickr"><img src="http://farm5.static.flickr.com/4036/4281258900_373b5ff95f.jpg" width="500" height="386" alt="Adding vignetting to a picture using Digikam"></a></p>
<p>Vignetting can be used as a creative tool to focus attention to the center of the picture.</p>
<p>This feature will be included in digiKam 1.1.0. I hope you will like it. Please provide feedback !</p>

<div class="legacy-comments">

  <a id="comment-18993"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-18993" class="active">great work, helpful tool</a></h3>    <div class="submitted">Submitted by Sergio (not verified) on Mon, 2010-01-25 19:18.</div>
    <div class="content">
     <p>great work, helpful tool</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18994"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-18994" class="active">Confused</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Tue, 2010-01-26 10:19.</div>
    <div class="content">
     <p>My curiosity has gotten the better of me and I have to ask:<br>
If the vignetting tool has been slightly modified to allow<br>
us to add vignetting to our pictures, what did the tool do<br>
before it was slightly modified?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18995"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-18995" class="active">Confused?</a></h3>    <div class="submitted">Submitted by <a href="http://www.shodokan.ch" rel="nofollow">Hendric</a> (not verified) on Tue, 2010-01-26 12:16.</div>
    <div class="content">
     <p>Well, it was initially designed to -&gt;remove&lt;- vignetting...<br>
Hendric</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18997"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-18997" class="active">Confused? Not Any Longer</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Wed, 2010-01-27 15:07.</div>
    <div class="content">
     <p>Ah ha! Thanks for that. I must have been suffering<br>
from vignetting of my peripheral thinking not to<br>
have thought of that one.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18998"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-18998" class="active">cool !</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2010-01-27 21:31.</div>
    <div class="content">
     <p>très attendu et *très* apprécié ! merci. Autant de petits outils qui facilitent la vie.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19016"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/496#comment-19016" class="active">Testing</a></h3>    <div class="submitted">Submitted by Fri13 on Tue, 2010-02-02 17:19.</div>
    <div class="content">
     <p>Does this need a patch because I can not get it from SVN version (revision 1084162)?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>