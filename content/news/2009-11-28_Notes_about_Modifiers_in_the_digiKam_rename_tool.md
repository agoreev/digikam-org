---
date: "2009-11-28T09:31:00Z"
title: "Notes about \"Modifiers\" in the digiKam rename tool"
author: "Andi Clemens"
description: "Some users have asked me what modifiers are. They don't seem to know this concept in a renaming tool and are a little bit confused"
category: "news"
aliases: "/node/487"

---

<p>Some users have asked me what modifiers are. They don't seem to know this concept in a renaming tool and are a little bit confused by it.<br>
I will try to explain what the modifiers do and why I implemented the renaming tool in such a way.<br>
<a href="http://www.flickr.com/photos/26732399@N05/4139674915/" title="Modifiers in digiKam rename tool von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2755/4139674915_5a1aabcd4d.jpg" width="500" height="166" alt="Modifiers in digiKam rename tool"></a> </p>
<h2>Update: Error Handling</h2>
<p><a href="http://www.flickr.com/photos/26732399@N05/4160400658/" title="Error Handling in Rename tool von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2548/4160400658_bd228981f8_o.png" width="443" height="28" alt="Error Handling in Rename tool"></a><br>
Wrongly placed modifiers are highlighted now, so you can easily see if you made a mistake. A modifier is invalid when it is not added directly behind an option or an option-modifier pair.</p>
<h2>Why Modifiers?</h2>
<p>As mentioned in an earlier article, renaming in digiKam was not quite powerful, so I started to implement this new widget. I didn't want to have a full-blown<br>
dialog for this, because it should be embedded in many places like in the MainUI, the importer and the BatchQueueManager. The concept of entering a template<br>
string for renaming should already be known to users from tools such as KRename and similar batch renaming applications.</p>
<p>I tested some batch renamer tools and most of them seem to allow modifications only for the original filename, but not for all the other options they provide for renaming.<br>
I never understood this limitation. What if I add the directory name and it is all uppercase, but I want it to be lowercase, to match the look of the overall new filename?<br>
There doesn't seem to be a way to do this.<br>
I know Metamorphose2 seems to do have something like modifiers, too, but for me the GUI is way too complex.<br>
<i>Edit: KRename seems to have modifier for all their options, too.</i></p>
<p>So I started my own tool and added keywords, that can be applied to every renaming option currently available. I'm going to explain the modifiers a little bit in the next section,<br>
to show how they can be used.</p>
<h2>Examples</h2>
<h2>Case Modifiers</h2>
<p>Case modifiers change the case of a renaming option. Currently there are three modifiers available: Lower, Upper and FirstLetterUpper.<br>
In the future these modifiers might be controllable with range parameters, so that you can, for example, have the first three characters of a renaming option in uppercase and leave the rest as is.</p>
<h3>Lower</h3>
<p>    This modifier allows to convert a renaming option to lowercase characters.</p>
<p>    <a href="http://www.flickr.com/photos/26732399@N05/4140436432/" title="LowerModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2547/4140436432_d86381fa7e_o.jpg" width="470" height="305" alt="LowerModifier"></a></p>
<h3>Uppper</h3>
<p>    This modifier allows to convert a renaming option to uppercase characters.</p>
<h3>FirstUpper</h3>
<p>    This modifier allows to convert each word in a renaming option to start with an uppercase character.</p>
<p>    <a href="http://www.flickr.com/photos/26732399@N05/4140436572/" title="UpperModifier_FirstUpperModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2637/4140436572_e9aaaa08b2_o.jpg" width="470" height="305" alt="UpperModifier_FirstUpperModifier"></a></p>
<h2>Others</h2>
<h3>Trimmed</h3>
<p>This modifier removes leading, trailing and extra whitespace from a renaming option. As you can see in the example screenshot, I added some whitespace to the replace modifier, that is removed again by the TrimmedModifier.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4140436698/" title="TrimmedModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2801/4140436698_c9c237f97e_o.jpg" width="470" height="305" alt="TrimmedModifier"></a></p>
<h3>Unique</h3>
<p>This modifier makes renaming options unique, avoiding name collisions. For example if you use the pattern shown in the screenshot, it is likely to have duplicate names.<br>
The rename tool wouldn't let you process these files and mark colliding names in red. The UniqueModifier can help to avoid this. As you can see in the example, it adds a suffix number<br>
to each [date:hhmmss] parse result when the value has already been used before.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4140436820/" title="UniqueModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2727/4140436820_e10a9d7b76_o.jpg" width="470" height="304" alt="UniqueModifier"></a></p>
<h3>Default</h3>
<p>The DefaultModifier can be used to set some value for options that don't return a string for some specific images. This can most likely happen with the camera or metadata renaming option.<br>
To avoid a gap in the filename, you can set a default value, that is inserted in this case.<br>
See the following screenshot:</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4139675593/" title="DefaultValueModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2511/4139675593_dcdb41fbfd_o.jpg" width="470" height="304" alt="DefaultValueModifier"></a></p>
<p>Let's assume that some of these images where scanned in, so they have no camera name. We set the default value to "Scanner" in this case, and fill in the gaps.</p>
<h3>Replace</h3>
<p>This modifier replaces text in a renaming option, you can use regular expressions for more advanced pattern matching.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4140437038/" title="ReplaceModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2672/4140437038_c4d0cbbf97_o.jpg" width="470" height="318" alt="ReplaceModifier"></a> <a href="http://www.flickr.com/photos/26732399@N05/4218347719/" title="replaceModifier_regexp.png von andiclemens bei Flickr"><img src="http://farm5.static.flickr.com/4055/4218347719_8fecd6faa0.jpg" width="500" height="387" alt="replaceModifier_regexp.png"></a></p>
<h3>Range</h3>
<p>This modifier only displays a given range of a renaming option.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4140437176/" title="RangeModifier von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2629/4140437176_5a74a242ab_o.jpg" width="470" height="318" alt="RangeModifier"></a></p>
<h2>Plans for the future</h2>
<p>As you have seen in the screenshots, modifiers can be chained together. In the future I plan to add grouping, so that you can enclose renaming options and apply modifiers on each of them in one go, for example:</p>
<p><code>[group][cam]_####_[file][/group]{upper}</code></p>
<p>or similar. I'm not yet sure how to do it, but this could be one way.</p>

<div class="legacy-comments">

  <a id="comment-18866"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18866" class="active">why not regular expresions</a></h3>    <div class="submitted">Submitted by Michau (not verified) on Tue, 2009-12-01 19:48.</div>
    <div class="content">
     <p>And what about <em>regular expressions</em> in this rename field? Andi, have you thought about it? This is some 'standard', I know quite complicated for not experienced user, some geek stuff, but it will be nice to have it here. Regards,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18868"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18868" class="active">Yes, I thought about RegExp,</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-12-02 20:11.</div>
    <div class="content">
     <p>Yes, I thought about RegExp, but unfortunately we have a feature freeze now.<br>
I guess you mean the "ReplaceModifier"? Right now I can not think of another place where to use them.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18941"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18941" class="active">Regular expressions can be</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sun, 2009-12-27 15:28.</div>
    <div class="content">
     <p>Regular expressions can be used in current SVN...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18867"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18867" class="active">Unique modifier</a></h3>    <div class="submitted">Submitted by Michal T. (not verified) on Wed, 2009-12-02 01:10.</div>
    <div class="content">
     <p>Modifiers are very nice. Now the renaming is quite much more powerful. I hope to see option to put metadata fields in the name soon too.</p>
<p>I was just renaming some batch of files and wanted to employ unique modifier isntead of standard counter (####). I like the idea, because I always get the number of photographs that I took at one day. My idea was like this: something-[date:yyMMdd]{unique}.ext</p>
<p>Though I realized that using this pattern will produce confusing results, when images will be sorted by name - first image - which is without unique modifier - will appear last and just the second image will appear as first. The alphabetical sorting can result to something like this (on some systems):</p>
<p>something-091202_1.ext<br>
something-091202_10.ext<br>
something-091202_11.ext<br>
...<br>
something-091202_19.ext<br>
something-091202_2.ext<br>
something-091202_21.ext<br>
something-091202_1.ext<br>
something-091202.ext</p>
<p>I suggest that we can set minimum number of digits for unique modifier. So we get something like this:</p>
<p>something-091202-0000.ext<br>
something-091202-0001.ext<br>
something-091202-0002.ext<br>
something-091203-0000.ext<br>
something-091203-0001.ext</p>
<p>...</p>
<p>I believe this will work on all systems.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18869"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18869" class="active">To have the "Metadata"</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-12-02 20:17.</div>
    <div class="content">
     <p>To have the "Metadata" option, you need the latest libkexiv2, compiled from SVN (or shipped with KDE 4.4).</p>
<p>We have a feature / string freeze now so I only can add this option without users noticing it.<br>
I guess I would simply use a syntax like that:</p>
<p>{unique}<br>
{unique:n}, where 'n' is the digits count.</p>
<p>I can add this in SVN, but you will not see it in the tooltip, nor in a config dialog, because the string freeze doesn't allow such changes now.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18870"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18870" class="active">I added the parameter to</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-12-02 20:30.</div>
    <div class="content">
     <p>I added the parameter to current SVN, but you can not see it in the tooltip or somewhere else, due to the string freeze.<br>
But at least it is there :D</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18871"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18871" class="active">I also thought about setting</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-12-02 20:32.</div>
    <div class="content">
     <p>I also thought about setting a minimal digits count of 4, but then again some users don't like this.<br>
I kept the default to 1 by now, if you want to use more digits you can call the modifier like:<br>
{unique:4}</p>
<p>But maybe I will still go with 4 digits as the default... ;-) Don't know yet.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18872"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18872" class="active">Thank you very much! I'm</a></h3>    <div class="submitted">Submitted by Michal T. (not verified) on Thu, 2009-12-03 02:13.</div>
    <div class="content">
     <p>Thank you very much! I'm impressed that my wish is fulfilled so immediately! Kudos to you!</p>
<p>I think having 4 digits by default is not that good. There are uses where the current default behaviour will do very well. I mean the cases where you don't need naming to follow time sequence, just you need to assure uniqueness.</p>
<p>Also I can imagine situation where 4 digits are not enough, and at this case unique modifier won't do what is expected to do - to assure uniqueness... </p>
<p>I would also suggest option to have something like {unique:auto} which will automatically create as many digits as is needed to make all files unique. (If there are 99 files with same name it will put 2 digits, if there is 100 it will put 3 digits etc.) I believe that would be useful quite universally.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18873"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/487#comment-18873" class="active">Yes I thought of that, too.</a></h3>    <div class="submitted">Submitted by Andi Clemens on Thu, 2009-12-03 07:41.</div>
    <div class="content">
     <p>Yes I thought of that, too. Right now it is not possible with current structure, and due to the feature freeze I can not change this now.<br>
Please post wishes on <a href="http://bugs.kde.org">bugzilla</a> instead in the future, I don't receive notifications from our website anymore (don't know why) and need to look manually every time if new comments appeared (which I don't do often).</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
