---
date: "2009-11-02T10:51:00Z"
title: "Kipi-plugins 0.8.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.8.0 maintenance release for KDE4 is out. kipi-plugins tarball can be downloaded from SourceForge at this url Kipi-plugins"
category: "news"
aliases: "/node/483"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.8.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/4036373881/" title="shwuptool by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2449/4036373881_c781cbb014.jpg" width="500" height="400" alt="shwuptool"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>KioImportExport</b> : New plugin to import or export images via FTP, Samba, Fish (SSH) and many more using KIO slaves.<br>
<b>AcquireImages</b>   : Under windows, use Twain interface from libksane to factorize code.<br>
<b>Shwup</b>           : New plugin to export images to Shwup web service (http://www.shwup.com).<br>
<b>General</b>         : All plugins ported to Qt4/KDE4. Qt3 transition class support is now dropped.<br>
<b>PrintAssistant</b>  : New templates management.<br>
<b>PrintAssistant</b>  : Icon preview for photo size choice.<br>
<b>PrintAssistant</b>  : Caption per photo management.<br>
<b>PrintAssistant</b>  : Added new templates for passport photos (6 photos 3.5x4cm and 4 photos 4.5x5cm).<br><br>

001 ==&gt; KioExport          : 187291 : FTP KIPI plugin for uploading a selection of images.<br>
002 ==&gt; KioExport          : 098279 : Exporting photos organised in tags into external directories.<br>
003 ==&gt; TimeAdjust         : 151346 : Kipi Time Adjust change the wrong date, should be.<br>
004 ==&gt; libkexiv2          : 204291 : Country of Korea impossible to translate in libexiv2 (kipiplugins).<br>
005 ==&gt; Handbook           : 169543 : Additional docbook ID tags to make outputted docs consistent between compiles.<br>
006 ==&gt; AcquireImages      : 192979 : Scanned images are placed in the wrong album.<br>
007 ==&gt; GalleryExport      : 194276 : Click on a file name closes the "select pictures" window.<br>
008 ==&gt; BatchProcessImages : 195112 : It is not possible to use Module | Batch menu twice.<br>
009 ==&gt; BatchProcessImages : 126603 : Resize  2-dim. broken (creates just a 2k  file).<br>
010 ==&gt; DngConverter       : 194736 : Extent dngconvert to set filedate to the dates taken from exif [patch].<br>
011 ==&gt; AdvancedSlideshow  : 186644 : digiKam crashes when exiting slideshow.<br>
012 ==&gt; BatchProcessImages : 158825 : Batch image resizing doesn't use the measures provided.<br>
013 ==&gt; PrintWizard        : 209165 : Kipi PrintWizard does not print svgz images.<br>
014 ==&gt; FlickrExport       : 208410 : Patch: Allow custom tags per image.<br>
015 ==&gt; KioImportExport    : 154183 : Import tool does not recognize Fish KIO Slave.<br>
016 ==&gt; JPEGLossLess       : 200032 : digiKam, inconsistency data about rotation.<br>
017 ==&gt; GPSSync            : 210611 : Kipi-plugin add gps data crash after write.<br>
018 ==&gt; BatchProcessImages : 210874 : Wish for new option in resize-tool: fit into given dimensions.<br>
019 ==&gt; RawConverter       : 211532 : Batch raw converter doesn't clean file list.<br>
020 ==&gt; BatchProcessImages : 211792 : Preview of images is not shown [patch].<br>
021 ==&gt; DNGConverter       : 206747 : DngConverter temp files not properly renamed.<br>
022 ==&gt; DNGConverter       : 210371 : Converting a CRW file results in an unusable DNG.<br>
023 ==&gt; DNGConverter       : 203959 : Kipi-plugins fails to compile: plugin_dngconverter.o: file not recognized.<br>
024 ==&gt; DNGConverter       : 210710 : digiKam error when open a dng image for edit after conversion from NEF.<br>
025 ==&gt; BatchProcessImages : 212116 : Rename images: sorting does not work [patch].<br>
026 ==&gt; AcquireImages      : 212295 : Kipi-plugins does not compile under Mac OS X.<br>
027 ==&gt; BatchProcessImages : 212328 : Rename images: use host application to render thumbnails [patch].<br>
<div class="legacy-comments">

</div>
