---
date: "2011-03-01T20:04:00Z"
title: "Writing Meta Data To Raw Files In digiKam"
author: "Mohamed Malik"
description: "Almost all images contain meta-data and these data contains all the setting that you used to create the picture, these include, shutter speed, aperture, focal"
category: "news"
aliases: "/node/579"

---

<p>Almost all images contain meta-data and these data contains all the setting that you used to create the picture, these include, shutter speed, aperture, focal length etc.</p>
<p>Most image management applications allows you to add and remove custom meta data to your files. This can be easily done while using JPEG's files. However the same is not very true for RAW files. <a href="http://www.mohamedmalik.com/?p=841" target="_blank"><strong>Continue to read...</strong></a></p>

<div class="legacy-comments">

</div>