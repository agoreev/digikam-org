---
date: "2007-02-19T13:39:00Z"
title: "First beta of KDcraw shared library"
author: "digiKam"
description: "After 8 days of intensive work, I'm very proud to announce the first LibKdcraw shared library 0.1.0 beta release. LibKdcraw is a C++ interface around"
category: "news"
aliases: "/node/208"

---

<p>After 8 days of intensive work, I'm very proud to announce the first LibKdcraw shared library 0.1.0 beta release.</p>
<p>LibKdcraw is a C++ interface around a dcraw binary program used to decode RAW picture files. This library is actually used by kipi-plugins (trunk), and later on by digiKam (with future 0.9.2 release). A RAWConverter screenshot can be seen <a href="http://digikam3rdparty.free.fr/Screenshots/newkipirawconverter.png">at this url</a></p>
<p>The library documentation is available in the header files. Check <a href="http://websvn.kde.org/trunk/extragear/libs/libkdcraw">at svn trunk repository</a> for details.</p>
<p>Libkdcraw includes the latest dcraw program (version 8.54) from Dave Coffin. It provides a kdcraw binary which is used instead dcraw. This eliminates _all_ compatibility problems with call options we have seen with dcraw. In fact the dcraw author does not respect a compatibilty between dcraw releases which breaks the RAW workflow! Also, he won't provide dcraw as a shared library. We have  wasted a lot time with these problems in the digiKam project. Using a specific version of dcraw in the library, we will validate this one with the library API and preserve the compatibility for the future.</p>
<p>The dcraw source code can be updated easily using this <a href="http://websvn.kde.org/*checkout*/trunk/extragear/libs/libkdcraw/dcraw/README?rev=635195"> README file</a> where i have written an instruction for that. The library doesn't touch the dcraw source code. It uses it well...</p>
<p>With this library, you can extract preview image (used by camera to display picture on TV screen) as a QImage, and extract all informations given by dcraw to identify a RAW file. Of course, you can decode and extract the RAW pictures with a lot of settings. The image data (8 or 16 bits color depth) is returned in a QByteArray container. The library include <a href="http://digikam3rdparty.free.fr/Screenshots/rawsettings.png"> a widget</a> to control all RAW decoding settings in your application.</p>
<p>Using the last dcraw source code, the libary support all last Camera models, especially all models out at Christmas 2006. The complete list is given below :</p>
<p>Adobe Digital Negative (DNG)<br>
AVT F-145C<br>
AVT F-201C<br>
AVT F-510C<br>
AVT F-810C<br>
Canon PowerShot 600<br>
Canon PowerShot A5<br>
Canon PowerShot A5 Zoom<br>
Canon PowerShot A50<br>
Canon PowerShot A610<br>
Canon PowerShot A620<br>
Canon PowerShot Pro70<br>
Canon PowerShot Pro90 IS<br>
Canon PowerShot G1<br>
Canon PowerShot G2<br>
Canon PowerShot G3<br>
Canon PowerShot G5<br>
Canon PowerShot G6<br>
Canon PowerShot S2 IS<br>
Canon PowerShot S3 IS<br>
Canon PowerShot S30<br>
Canon PowerShot S40<br>
Canon PowerShot S45<br>
Canon PowerShot S50<br>
Canon PowerShot S60<br>
Canon PowerShot S70<br>
Canon PowerShot Pro1<br>
Canon EOS D30<br>
Canon EOS D60<br>
Canon EOS 5D<br>
Canon EOS 10D<br>
Canon EOS 20D<br>
Canon EOS 30D<br>
Canon EOS 300D / Digital Rebel / Kiss Digital<br>
Canon EOS 350D / Digital Rebel XT / Kiss Digital N<br>
Canon EOS 400D / Digital Rebel XTi / Kiss Digital X<br>
Canon EOS D2000C<br>
Canon EOS-1D<br>
Canon EOS-1DS<br>
Canon EOS-1D Mark II<br>
Canon EOS-1D Mark II N<br>
Canon EOS-1Ds Mark II<br>
Casio QV-2000UX<br>
Casio QV-3000EX<br>
Casio QV-3500EX<br>
Casio QV-4000<br>
Casio QV-5700<br>
Casio QV-R51<br>
Casio QV-R61<br>
Casio EX-S100<br>
Casio EX-Z4<br>
Casio EX-Z50<br>
Casio EX-Z55<br>
Casio Exlim Pro 505<br>
Casio Exlim Pro 600<br>
Casio Exlim Pro 700<br>
Contax N Digital<br>
Creative PC-CAM 600<br>
Epson R-D1<br>
Foculus 531C<br>
Fuji FinePix E550<br>
Fuji FinePix E900<br>
Fuji FinePix F700<br>
Fuji FinePix F710<br>
Fuji FinePix F800<br>
Fuji FinePix F810<br>
Fuji FinePix S2Pro<br>
Fuji FinePix S3Pro<br>
Fuji FinePix S20Pro<br>
Fuji FinePix S5000<br>
Fuji FinePix S5100/S5500<br>
Fuji FinePix S5200/S5600<br>
Fuji FinePix S6000fd<br>
Fuji FinePix S7000<br>
Fuji FinePix S9000/S9500<br>
Imacon Ixpress 16-megapixel<br>
Imacon Ixpress 22-megapixel<br>
Imacon Ixpress 39-megapixel<br>
ISG 2020x1520<br>
Kodak DC20 (see Oliver Hartman's page)<br>
Kodak DC25 (see Jun-ichiro Itoh's page)<br>
Kodak DC40<br>
Kodak DC50<br>
Kodak DC120 (also try kdc2tiff)<br>
Kodak DCS200<br>
Kodak DCS315C<br>
Kodak DCS330C<br>
Kodak DCS420<br>
Kodak DCS460<br>
Kodak DCS460A<br>
Kodak DCS520C<br>
Kodak DCS560C<br>
Kodak DCS620C<br>
Kodak DCS620X<br>
Kodak DCS660C<br>
Kodak DCS660M </p>
<p>Kodak DCS720X<br>
Kodak DCS760C<br>
Kodak DCS760M<br>
Kodak EOSDCS1<br>
Kodak EOSDCS3B<br>
Kodak NC2000F<br>
Kodak ProBack<br>
Kodak PB645C<br>
Kodak PB645H<br>
Kodak PB645M<br>
Kodak DCS Pro 14n<br>
Kodak DCS Pro 14nx<br>
Kodak DCS Pro SLR/c<br>
Kodak DCS Pro SLR/n<br>
Kodak P850<br>
Kodak P880<br>
Kodak KAI-0340<br>
Konica KD-400Z<br>
Konica KD-510Z<br>
Leaf Aptus 17<br>
Leaf Aptus 22<br>
Leaf Aptus 65<br>
Leaf Aptus 75<br>
Leaf Cantare<br>
Leaf CatchLight<br>
Leaf CMost<br>
Leaf DCB2<br>
Leaf Valeo 6<br>
Leaf Valeo 11<br>
Leaf Valeo 17<br>
Leaf Valeo 22<br>
Leaf Volare<br>
Leica Digilux 2<br>
Leica Digilux 3<br>
Leica D-LUX2<br>
Leica D-LUX3<br>
Leica V-LUX1<br>
Logitech Fotoman Pixtura<br>
Micron 2010<br>
Minolta RD175<br>
Minolta DiMAGE 5<br>
Minolta DiMAGE 7<br>
Minolta DiMAGE 7i<br>
Minolta DiMAGE 7Hi<br>
Minolta DiMAGE A1<br>
Minolta DiMAGE A2<br>
Minolta DiMAGE A200<br>
Minolta DiMAGE G400<br>
Minolta DiMAGE G500<br>
Minolta DiMAGE G530<br>
Minolta DiMAGE G600<br>
Minolta DiMAGE Z2<br>
Minolta Alpha/Dynax/Maxxum 5D<br>
Minolta Alpha/Dynax/Maxxum 7D<br>
Nikon D1<br>
Nikon D1H<br>
Nikon D1X<br>
Nikon D2H<br>
Nikon D2Hs<br>
Nikon D2X<br>
Nikon D40<br>
Nikon D50<br>
Nikon D70<br>
Nikon D70s<br>
Nikon D80<br>
Nikon D100<br>
Nikon D200<br>
Nikon E700 ("DIAG RAW" hack)<br>
Nikon E800 ("DIAG RAW" hack)<br>
Nikon E880 ("DIAG RAW" hack)<br>
Nikon E900 ("DIAG RAW" hack)<br>
Nikon E950 ("DIAG RAW" hack)<br>
Nikon E990 ("DIAG RAW" hack)<br>
Nikon E995 ("DIAG RAW" hack)<br>
Nikon E2100 ("DIAG RAW" hack)<br>
Nikon E2500 ("DIAG RAW" hack)<br>
Nikon E3200 ("DIAG RAW" hack)<br>
Nikon E3700 ("DIAG RAW" hack)<br>
Nikon E4300 ("DIAG RAW" hack)<br>
Nikon E4500 ("DIAG RAW" hack)<br>
Nikon E5000<br>
Nikon E5400<br>
Nikon E5700<br>
Nikon E8400<br>
Nikon E8700<br>
Nikon E8800<br>
Olympus C3030Z<br>
Olympus C5050Z<br>
Olympus C5060WZ<br>
Olympus C7070WZ<br>
Olympus C70Z,C7000Z<br>
Olympus C740UZ<br>
Olympus C770UZ<br>
Olympus C8080WZ<br>
Olympus E-1<br>
Olympus E-10<br>
Olympus E-20<br>
Olympus E-300<br>
Olympus E-330<br>
Olympus E-400<br>
Olympus E-500<br>
Olympus SP310<br>
Olympus SP320<br>
Olympus SP350<br>
Olympus SP500UZ<br>
Panasonic DMC-FZ30<br>
Panasonic DMC-FZ50<br>
Panasonic DMC-L1<br>
Panasonic DMC-LC1<br>
Panasonic DMC-LX1<br>
Panasonic DMC-LX2<br>
Pentax *ist D<br>
Pentax *ist DL<br>
Pentax *ist DL2<br>
Pentax *ist DS<br>
Pentax *ist DS2<br>
Pentax K10D<br>
Pentax K100D<br>
Pentax Optio S<br>
Pentax Optio S4<br>
Pentax Optio 33WR<br>
Phase One LightPhase<br>
Phase One H 10<br>
Phase One H 20<br>
Phase One H 25<br>
Phase One P 20<br>
Phase One P 25<br>
Phase One P 30<br>
Phase One P 45<br>
Pixelink A782<br>
Polaroid x530<br>
Rollei d530flex<br>
RoverShot 3320af<br>
Samsung GX-1S<br>
Sarnoff 4096x5440<br>
Sigma SD9<br>
Sigma SD10<br>
Sinar 3072x2048<br>
Sinar 4080x4080<br>
Sinar 4080x5440<br>
Sinar STI format<br>
SMaL Ultra-Pocket 3<br>
SMaL Ultra-Pocket 4<br>
SMaL Ultra-Pocket 5<br>
Sony DSC-F828<br>
Sony DSC-R1<br>
Sony DSC-V3<br>
Sony DSLR-A100<br>
Sony XCD-SX910CR<br>
STV680 VGA</p>
<p>About digiKam and this library, I have a big patch on my computer to use this library with all RAW files. I will commit this code in svn repository later digiKam 0.9.1 release. It will be available for 0.9.2 release. If you is impatient to test it, the patch against current svn implementation of digiKam can be downloaded <a href="http://digikam3rdparty.free.fr/misc.tarballs/libkdcrawdigikamport.patch"> at this url</a></p>
<p>Of course, you need to checkout, compile and install LibKdcraw first. It is stored on extragear/libs folder of svn trunk.</p>
<p>All feedbacks are welcome, especially all RAW pictures sample taken with a recent camera to perform advanced test. Thanks in advance for your help...</p>

<div class="legacy-comments">

  <a id="comment-17919"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/208#comment-17919" class="active">good working .thanks</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:14.</div>
    <div class="content">
     <p>good working .thanks </p>
<p> <a href="http://www.trsohbet.name">sohbet</a></p>
         </div>
    <div class="links">» </div>
  </div>

</div>