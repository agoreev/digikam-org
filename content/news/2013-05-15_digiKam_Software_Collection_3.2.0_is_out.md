---
date: "2013-05-15T16:42:00Z"
title: "digiKam Software Collection 3.2.0 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 3.2.0 release of digiKam Software Collection. This version include a new album"
category: "news"
aliases: "/node/694"

---

<a href="http://www.flickr.com/photos/digikam/8740630619/" title="digiKam3.2.0 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7293/8740630619_6edb3572b5_c.jpg" width="800" height="225" alt="digiKam3.2.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 3.2.0 release of digiKam Software Collection.
This version include a new album interface display mode named list-view. Icon view can be switched to a flat item list, where items can  
be sorted by properties columns as in a simple file manager. Columns can be customized to show file, image, metadata, or digiKam properties.</p>

<p>Another important feature introduced in this release is the capability to filter and search items based on aspect ratio property. This information can be displayed over icon-view and item tool-tip. Advanced search tool have take some benefits of junior jobs given to future GSoC students to implement search video items based on properties.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.2.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.2.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Next main release 3.3.0 will introduce a new and suitable Face Recognition implementation. Look in Release plan <a href="http://www.digikam.org/about/releaseplan">for details...</a></p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20535"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20535" class="active">digikam 3.2.0 for Windows</a></h3>    <div class="submitted">Submitted by Wolfgang (not verified) on Wed, 2013-05-15 19:14.</div>
    <div class="content">
     <p>Hello,</p>
<p>there will be the version 3.2.0 for Windows?<br>
Until when will the Windows version will be available?</p>
<p>regards<br>
Wolfgang</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20537"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20537" class="active">Windows</a></h3>    <div class="submitted">Submitted by <a href="http://www.sylvain-crouzillat.com" rel="nofollow">croucrou</a> (not verified) on Wed, 2013-05-15 22:06.</div>
    <div class="content">
     <p>I dream of a truly usable windows version.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20544"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20544" class="active">I dream of a truly windows free environment</a></h3>    <div class="submitted">Submitted by Christian Winger Nyman (not verified) on Wed, 2013-05-22 01:38.</div>
    <div class="content">
     <p>Ever tried kubuntu, fantastic :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20538"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20538" class="active">Coming soon</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2013-05-15 22:39.</div>
    <div class="content">
     <p>I realize I've been saying this for a while, but a release will happen very soon. I've built 3.0.0 and 3.1.0 using final 4.10.2 KDE windows code base and have fixed the collection path issue that would have caused digikam to lose track of migrated databases, and am doing final testing. I have found some minor bugs and when these are fixed I will release 3.0.0 and 3.1.0 at the same time. 3.2.0 will follow about a week later as I will have to get some new components like Eigen 3 compiling before I can release it. Very sorry for the delay!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20539"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20539" class="active">Ananta, many thanks for your</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Thu, 2013-05-16 14:13.</div>
    <div class="content">
     <p>Ananta, many thanks for your effort to give us a Windows version of this great software.</p>
<p>I'm looking forward to using it.</p>
<p>Thanks again,<br>
 Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20540"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20540" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Carl (not verified) on Thu, 2013-05-16 14:32.</div>
    <div class="content">
     <p>Thanks for your effort in making a fully functional Windows version of digiKam! </p>
<p>I don't know much about the process, but I know enough to have realized that it can be much more complicated than what most people initially believe. </p>
<p>I'm sure most Windows users want a ready-made installation file (I know I do :-)), but at the same time it would be interesting to read a little about the steps needed to produce that installation file. If you feel up for it, I'm sure it would prove an educational read if you were to list the steps you take (perhaps similarly to <a href="http://forums.gcstar.org/viewtopic.php?pid=10534#p10534">this list</a> by a GCstar user who gave that Perl project a little Windows love (although GCstar probably still needs more Windows attention)). </p>
<p>Anyway, thanks again, Ananta - and everyone involved in the making of digiKam!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20545"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20545" class="active">Great news</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2013-05-23 02:46.</div>
    <div class="content">
     <p>Glad to hear that there will be an updated version for windows. I know some folks think why bother, but there are those of us that must use it and we appreciate the support that has been given.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20548"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20548" class="active">We need all the build steps...</a></h3>    <div class="submitted">Submitted by Pietro (not verified) on Thu, 2013-05-30 09:30.</div>
    <div class="content">
     <p>Hi Ananta,</p>
<p>please, write and and <a>make public all the steps</a> you take to build digiKam on Windows.<br>
I cannot use a prebuilt binary since I have to make a few additions to the program.</p>
<p>Thank you,<br>
Pietro</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20536"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20536" class="active">Awesome :) Can't wait till</a></h3>    <div class="submitted">Submitted by Daniel Meissner (not verified) on Wed, 2013-05-15 21:48.</div>
    <div class="content">
     <p>Awesome :) Can't wait till this shows up in openSUSE. </p>
<p>digiKam is just great software and I wanted to say thank you (big time) ^^</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20541"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20541" class="active">Great software</a></h3>    <div class="submitted">Submitted by Wim (not verified) on Thu, 2013-05-16 18:34.</div>
    <div class="content">
     <p>My sincere thanks to the Digikam-team for this great program!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20542"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20542" class="active">What about opening Pictures in Full-Window mode on a 2nd screen?</a></h3>    <div class="submitted">Submitted by <a href="http://freie-software.info/" rel="nofollow">Lanzi</a> (not verified) on Sat, 2013-05-18 18:27.</div>
    <div class="content">
     <p>Salut Gilles,<br>
Thanks for your efforts and your commitment!</p>
<p>As a photographer I really would like to see my fotos and films in a complete (borderless) fullscreen mode.<br>
Is that possible now?<br>
I never found out how to do that!</p>
<p>Would also be nice, if that could also (!) happen on my second monitor.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20546"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20546" class="active">Lanzi, that is an AMAZING</a></h3>    <div class="submitted">Submitted by Jose (not verified) on Thu, 2013-05-23 20:34.</div>
    <div class="content">
     <p>Lanzi, that is an AMAZING suggestion. Maybe you should create a wish on https://bugs.kde.org??</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20547"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20547" class="active">When I want to do this I use</a></h3>    <div class="submitted">Submitted by T Middleton (not verified) on Wed, 2013-05-29 16:05.</div>
    <div class="content">
     <p>When I want to do this I use the slide show mode (F9 key).</p>
<p>In the digikam settings for slideshow there is a "Start with current image" option, so whatever image is highlighted will immediately pop up full screen. (Then press space to pause the slide show, if you just want the one image. And Press Esc to go back out of full screen mode.) A bit of a hack, but works well enough for me when I need it.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20549"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20549" class="active">MAC OS X Version</a></h3>    <div class="submitted">Submitted by Phlogi (not verified) on Tue, 2013-06-04 20:35.</div>
    <div class="content">
     <p>Any idea when this release is available through mac ports? </p>
<p>http://trac.macports.org/browser/trunk/dports/kde/digikam/Portfile?rev=104235</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20552"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/694#comment-20552" class="active">contact maintainer...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2013-06-05 06:17.</div>
    <div class="content">
     <p>... by email: jan at hyper-world dot de</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
