---
date: "2007-12-08T21:55:00Z"
title: "digiKam 0.9.3-rc1 Release"
author: "gerhard"
description: "The digiKam team releases 0.9.3-rc1. The tarball can preferably be downloaded from SourceForge. There is again a new very practical feature integrated: In the Albums/Tags/Searches/TagsFilter"
category: "news"
aliases: "/node/278"

---

<p>The digiKam team releases <a href="http://digikam3rdparty.free.fr/0.9 releases/digikam-0.9.3-rc1.tar.bz2">0.9.3-rc1</a>. The  tarball can preferably be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.</p>
<p>There is again a <u>new</u> very practical <u>feature</u> integrated:<br>
In the Albums/Tags/Searches/TagsFilter sidebars the is a <b>live filter</b> at the<br>
bottom that lets you search for a string in Albums/Tags/Searches/TagsFilter<br>
hierarchy, much as amarok does it int he collections.</p>
<p>A word on <u>stability</u>. I know that many of you wait until a 'stable' sees the<br>
day of light in order to upgrade digiKam. I will not tell you to do<br>
otherwise. But! I update digikam daily per cron job from svn since 4 years<br>
now. I have never lost a single image, nor have I lost the database or<br>
metadata. And I have 35K images in the album. Bugs are generally functions<br>
that don't work, but they are not destructive. Now do what you want :-)</p>
<p>And of course several bug fixes:<br>
digiKam BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</p>
<p>052 ==&gt; 152522 : Support for *.3gp and *.mp4 video files in digiKam (already<br>
supported by KDE video players).<br>
053 ==&gt; 128231 : A way to view pictures recursively in albums and sub-albums<br>
                 at the same time would be cool.<br>
054 ==&gt; 133191 : Quick search/filter for albums like in amarok.<br>
055 ==&gt; 146364 : Incremental search for tags in all tag fields.<br>
056 ==&gt; 152843 : Live filter does not work if no text is displayed under<br>
                 thumbnails.<br>
057 ==&gt; 148629 : "Adjust Exif orientation tag" does not continue after error.<br>
058 ==&gt; 144165 : Numeric values in Histogram -&gt; Statistics frame should be<br>
                 right-aligned.<br>
059 ==&gt; 148037 : Tiff images are not displayed with correct colours.<br>
060 ==&gt; 152961 : Cannot remove album from tree.<br>
061 ==&gt; 121314 : More options to display the album tree.<br>
062 ==&gt; 141085 : Some strings are untranslated in digiKam.<br>
063 ==&gt; 151403 : Crashed trying to save a image downloaded from the camera.</p>
<p>Be happy, run digiKam<br>
Gerhard</p>

<div class="legacy-comments">

  <a id="comment-17897"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/278#comment-17897" class="active">digiKam 0.10.0 for Windows!!</a></h3>    <div class="submitted">Submitted by Paul (not verified) on Wed, 2008-10-29 07:37.</div>
    <div class="content">
     <p>Fantastic - thank you.  Until Ubuntu deals with the "not seeing Nikon" problem, I'm stuck with Windows for a while longer. This will help tremendously.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>