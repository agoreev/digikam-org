---
date: "2011-03-17T11:41:00Z"
title: "Performing Fuzzy Searches In digiKam"
author: "Mohamed Malik"
description: "One of the useful tools that can be found in digiKam is the ability to perform fuzzy searches, where the user can guess and sketch"
category: "news"
aliases: "/node/586"

---

<p>One of the useful tools that can be found in digiKam is the ability to perform fuzzy searches, where the user can guess and sketch the colors that the image may contain.</p>
<p>Inorder to perform a fuzzy search you need to rebuild the image finger prints in your database. <a href="http://www.mohamedmalik.com/?p=949" target="_blank"><strong>Continue to read</strong></a></p>

<div class="legacy-comments">

</div>