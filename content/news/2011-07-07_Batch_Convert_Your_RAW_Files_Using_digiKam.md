---
date: "2011-07-07T20:32:00Z"
title: "Batch Convert Your RAW Files Using digiKam"
author: "Mohamed Malik"
description: "Processing RAW files can be time consuming, in case you have certain amount of RAW files which need to be converted into a more accessible"
category: "news"
aliases: "/node/612"

---

<p>Processing RAW files can be time consuming, in  case you have certain amount of RAW files which need to be converted into a more accessible for format within a few minutes , then digiKam's batch RAW converting tool can be very handy and efficient and it takes a few clicks.</p>
<p><a href="http://www.mohamedmalik.com/?p=1055">(Continue to read..)</a></p>

<div class="legacy-comments">

</div>