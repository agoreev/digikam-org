---
date: "2010-12-21T18:20:00Z"
title: "kipi-plugins 1.7.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! Just at Christmas time, digiKam team is proud to announce Kipi-plugins 1.7.0 ! kipi-plugins tarball can be downloaded from"
category: "news"
aliases: "/node/558"

---

<p>Dear all digiKam fans and users!</p>

<p>Just at Christmas time, digiKam team is proud to announce Kipi-plugins 1.7.0 !</p>

<a href="http://www.flickr.com/photos/digikam/5280783744/" title="kipiplugins1.7.O by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5286/5280783744_c168f4ef84_z.jpg" width="640" height="360" alt="kipiplugins1.7.O"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of new features and bugs-fix coming with this release.</p>

<p>Enjoy digiKam and Happy new year.</p>

<h5>NEW FEATURES:</h5><br>

<b>General</b> : New plugin to export images to Debian Screenshoots web service (http://screenshots.debian.net).<br>
<b>General</b> : New plugin to export images to an instant messaging contact.<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 225174 : Facebook           : Support new permission API for applications.<br>
002 ==&gt; 254110 : Facebook           : Unable to upload photos to a selected album.<br>
003 ==&gt; 254457 : Facebook           : Album target fails.<br>
004 ==&gt; 247107 : Facebook           : Export to notes, export to pinboard, export to profile.<br>
005 ==&gt; 219284 : Facebook           : digiKam facebook export album.<br>
006 ==&gt; 241046 : Facebook           : Export Facebook.<br>
007 ==&gt; 232015 : Facebook           : KPhotoAlbum Facebook import plugin.<br>
008 ==&gt; 223601 : Facebook           : digiKam can't upload image to facebook.<br>
009 ==&gt; 243381 : ImageViewer        : Gwenview (gwenview), signal: Segmentation fault.<br>
010 ==&gt; 256714 : DebianScreenshoots : Add export to screenshots.debian.net.<br>
011 ==&gt; 258953 : DNGConverter       : DngConverter changes luminosity of sony ARW files.<br>
012 ==&gt; 255069 : SendImage          : Error with spaces in file names in send by email.<br>
013 ==&gt; 251235 : FlashExport        : Flash export plugin does not install.<br>
014 ==&gt; 244702 : PicasaWebExport    : Picasawebexport unable to select movie.<br>
015 ==&gt; 258824 : PicasaWebExport    : After export images to Picasaweb cancel button should change to close.<br>
016 ==&gt; 256713 : Kopete             : Send to Kopete contact plugin.<br>
017 ==&gt; 250449 : AdvancedSlideshow  : Slideshow does not work with compositing on.<br>
018 ==&gt; 256284 : RemoveRedEyes      : digiKam crash on start.<br>


<div class="legacy-comments">

  <a id="comment-19681"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19681" class="active">Huzzah!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Bugsbane</a> (not verified) on Wed, 2010-12-22 03:51.</div>
    <div class="content">
     <p>Great to see Advance Slide Show plugin (Ken Burns effect FTW!) working with compositing. Any idea if the bug with Advance Slideshow never looping was fixed? I never reported it (Kipi 1.4), so if you had no idea it existed, my bad :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19682"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19682" class="active">Facebook Album bug not fixed?</a></h3>    <div class="submitted">Submitted by Stephen S (not verified) on Wed, 2010-12-22 04:04.</div>
    <div class="content">
     <p>I grabbed the 1.7.0 version, but I'm still running into the issue of not being able to select the album to upload pictures into. This was one of the things I was hoping would be fixed, and the release notes say it is, but I don't see a change in behavior.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19684"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19684" class="active">Nice job</a></h3>    <div class="submitted">Submitted by amorphous (not verified) on Wed, 2010-12-22 06:42.</div>
    <div class="content">
     <p>Nice work. I was wondering how did you put the Lock/Logout widget to be vertical. By default it's horizontal. I really like the way it is in the photo.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19689"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19689" class="active">Lock/Logout widget</a></h3>    <div class="submitted">Submitted by Nico (not verified) on Wed, 2010-12-22 22:24.</div>
    <div class="content">
     <p>if you increase the vertical size of the panel enough, the lock/logout widget will automatically get vertical - that's how it worked for me anyway ;)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19685"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19685" class="active">Where did the duplicate</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2010-12-22 08:26.</div>
    <div class="content">
     <p>Where did the duplicate images search tool go? It was a perfect plugin for Gwenview when managing my images. I have tried Digikam but it's overly complex for my tastes.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19686"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19686" class="active">Archive plugin</a></h3>    <div class="submitted">Submitted by yegorich (not verified) on Wed, 2010-12-22 09:34.</div>
    <div class="content">
     <p>Is Archive plugin included again? k3b is already a part of the new KDE. If not included are there plans to include it?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19720"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19720" class="active">Archive plugin</a></h3>    <div class="submitted">Submitted by thomas (not verified) on Sat, 2011-01-01 18:03.</div>
    <div class="content">
     <p>as it still does not seem to be included - I'd like to vote for this too!  Would be a great extension.<br>
Btw, the kipi-plugins.org site is quite outdated.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19690"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19690" class="active">flashexport translations</a></h3>    <div class="submitted">Submitted by Nico (not verified) on Wed, 2010-12-22 22:45.</div>
    <div class="content">
     <p>what happened to the translations of the flashexport plugin? - they have vanished from the po directory of kipi-plugins 1.7.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19691"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19691" class="active">same for</a></h3>    <div class="submitted">Submitted by Nico (not verified) on Wed, 2010-12-22 23:12.</div>
    <div class="content">
     <p>same for kipiplugin_flickrexport, kipiplugin_galleryexport, kipiplugin_htmlexport, kipiplugin_picasawebexport, kipiplugin_piwigoexport, kipiplugin_ipodexport, kipiplugin_kioexportimport, kipiplugin_expoblending</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19694"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/558#comment-19694" class="active">tarballs fixed in sf.net</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-12-23 13:35.</div>
    <div class="content">
     <p>Today, Nicolas has updated tarballs in sourceforge.net repository to include missing translations files.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
