---
date: "2012-12-29T08:07:00Z"
title: "digiKam Software Collection 3.0.0-RC is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! After one summer of active development with Google Summer of Code 2012 students, and 3 beta releases, digiKam team"
category: "news"
aliases: "/node/678"

---

<a href="http://www.flickr.com/photos/digikam/8189996351/" title="splash-digikam by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8348/8189996351_8fa777f1a4.jpg" width="500" height="307" alt="splash-digikam"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one summer of active development with Google Summer of Code 2012 students, and 3 beta releases, digiKam team is proud to announce the release candidate of digiKam Software Collection 3.0.0.
This version is currently under development, following GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.0.0-rc.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Happy new year and digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20470"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20470" class="active">Face recognition?</a></h3>    <div class="submitted">Submitted by urcindalo (not verified) on Sat, 2012-12-29 23:45.</div>
    <div class="content">
     <p>What's the real status of the face recognition feature? It has been at "Under Progress" for way too long, with no further explanation. Even past questions regarding its status have been simply ignored. It looks like a taboo theme.</p>
<p>Will it finally make it into the final 3.0 version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20471"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20471" class="active">yes, it should really be</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-12-30 00:06.</div>
    <div class="content">
     <p>yes, it should really be communicated better whats happening with the face detection and recocgnition feature and when it's usable after so many announcements an half-baked attempts</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20472"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20472" class="active">Response already given...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2012-12-30 09:26.</div>
    <div class="content">
     ...by Marcel <a href="http://www.digikam.org/node/674#comment-20429">in this thread</a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20474"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20474" class="active">Thanks for the link. What I</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-12-31 13:42.</div>
    <div class="content">
     <p>Thanks for the link. What I find unfortunate is that some labels "recognition" appear in Digikam 3.0 RC if it is not already functional.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20473"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20473" class="active">Database update v6 to v7 in digikam v3.0.0 fails</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Sun, 2012-12-30 12:42.</div>
    <div class="content">
     <p>Since the beta versions of 3.0.0, I have trouble starting digikam, also RC shows this behavior. compile goes fine, but on startup it seems to be trying an database update, which immediately fails (currently I'm on digikam v2.9.0, DB binary is digikam4.db):</p>
<p>QSqlDatabasePrivate::removeDatabase: connection 'ConnectionTest' is still in use, all queries will cease to work.<br>
digikam(2803)/digikam (core): No DB action defined for "UpdateSchemaFromV6ToV7" ! Implementation missing for this database type.<br>
digikam(2803)/digikam (core): Attempt to execute null action<br>
digikam(2803)/digikam (core): Schema update to V 7 failed!<br>
*** glibc detected *** ./core/digikam/digikam: corrupted double-linked list: 0x0c4e49f8 ***</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20475"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20475" class="active">Are there any plans for RAW</a></h3>    <div class="submitted">Submitted by Redi (not verified) on Tue, 2013-01-01 01:18.</div>
    <div class="content">
     <p>Are there any plans for RAW editor that is worth using? Something that works like Darktable with all the digiKam features would be nice.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20477"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20477" class="active">RAW</a></h3>    <div class="submitted">Submitted by Izanik (not verified) on Sun, 2013-01-06 11:42.</div>
    <div class="content">
     <p>I agree. I have been using digiKam until I started shooting in DNG format... In fact, there is a big gap in Linux for RAW editing. I have tried migrating to Darktable or UFRaw from Lightroom, but I don't find this software usable enought. The image processing should be easy and fast. I think that digiKam has a great opportunity there.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20480"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20480" class="active">I use Corel Aftershot Pro</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2013-01-07 00:29.</div>
    <div class="content">
     <p>I use Corel Aftershot Pro (former named Bibble) for RAW-editing and is much like Adobe Lightroom. It's neither free or open source but great for RAW-editing on Linux. It's miles ahead from Darktable and the others so far in my book.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20478"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20478" class="active">Basic questions: 1. Software Collection ?   2. New features ?</a></h3>    <div class="submitted">Submitted by gary (not verified) on Sun, 2013-01-06 23:20.</div>
    <div class="content">
     <p>I've  been poking around this web site for some time now, and I have two simple questions:</p>
<p>1.  Does Digikam Software Collection contain the Digikam application?  In other words, if I want the latest digikam, do I download Digikam Software Collection?  Or is Digikam Software Collection a collection of related, but different, applications?</p>
<p>2. 3.0.0 sounds like a major release.  How does it differ from the 2.9 series?</p>
<p>Better: point me to where these are answered on the site<br>
Best:  I think these are basic questions, no?  If you agree, make the answers more find-able from the homepage.</p>
<p>thanks, gary</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20479"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20479" class="active">see below</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2013-01-06 23:24.</div>
    <div class="content">
     <p>1 ==&gt; yes<br>
2 ==&gt; https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/NEWS</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20481"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20481" class="active">OS X build?</a></h3>    <div class="submitted">Submitted by <a href="http://risacher.org/" rel="nofollow">Dan Risacher</a> (not verified) on Mon, 2013-01-07 20:47.</div>
    <div class="content">
     <p>Any word on a OS X build or portfile?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20482"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20482" class="active">windows installer is available...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2013-01-08 22:44.</div>
    <div class="content">
     ... on <a href="http://download.kde.org/unstable/digikam/digiKam-installer-3.0.0-rc-win32.exe.mirrorlist"> KDE repository</a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20485"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20485" class="active">I am a Windows 7 user and</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2013-02-02 22:25.</div>
    <div class="content">
     <p>I am a Windows 7 user and would love to use DK, but it takes around 30 min for the program to start. What am I doing wrong?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20483"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/678#comment-20483" class="active">opencv/cvaux.h not found and FIX on Ubuntu 12.04</a></h3>    <div class="submitted">Submitted by <a href="http://pixpelado.blogspot.com" rel="nofollow">Horacio</a> (not verified) on Sun, 2013-01-13 18:03.</div>
    <div class="content">
     <p>Hi! I just want to report that building Digikam 3.0.0-rc on Ubuntu 12.04 *may* fail, even when the 'bootstrap' script returned successfully. The problem is resolved installing "libcvaux-dev", the package that contains "opencv/cvaux.h".</p>
<p>So, if you get:</p>
<p><code><br>
$ ./bootstrap.local<br>
(...)<br>
-- First try at finding OpenCV...<br>
-- Great, found OpenCV on the first try.<br>
-- OpenCV Root directory is /usr/share/OpenCV<br>
(...)<br>
$ cd build/<br>
$ make -j2<br>
(...)<br>
Scanning dependencies of target kface<br>
[ 42%] Building CXX object extra/libkface/libkface/CMakeFiles/kface.dir/kface_automoc.cpp.o<br>
[ 42%] Building CXX object extra/libkface/libkface/CMakeFiles/kface.dir/database.cpp.o<br>
In file included from /mnt/sin_encriptar/src/digikam-3.0.0-rc/extra/libkface/libkface/database.cpp:46:0:<br>
/mnt/sin_encriptar/src/digikam-3.0.0-rc/extra/libkface/libkface/libopencv.h:40:26: fatal error: opencv/cvaux.h: No such file or directory<br>
compilation terminated.<br>
Linking CXX shared library ../../../lib/libkvkontakte.so<br>
make[2]: *** [extra/libkface/libkface/CMakeFiles/kface.dir/database.cpp.o] Error 1<br>
make[1]: *** [extra/libkface/libkface/CMakeFiles/kface.dir/all] Error 2<br>
make[1]: *** Waiting for unfinished jobs....<br>
[ 42%] Built target kvkontakte<br>
make: *** [all] Error 2<br>
</code></p>
<p>you should install the package "libcvaux-dev" (I've installed all of the 'transitional' packages: libcv2.3 libcvaux-dev and libcvaux2.3, but I think 'libcvaux-dev' is what fix the problem, since it contains 'opencv/cvaux.h').</p>
<p>Digikam is AWESOME! Great work!!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
