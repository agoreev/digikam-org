---
date: "2014-11-05T10:14:00Z"
title: "digiKam Recipes 4.1.1 Released"
author: "Dmitri Popov"
description: "Another month, another digiKam Recipes update. This version features the updated and expanded Edit Photos with the Levels and Curves Adjustment Tools recipe which now"
category: "news"
aliases: "/node/722"

---

<p>Another month, another <a href="http://dmpop.dhcp.io/#!digikamrecipes.md">digiKam Recipes</a>&nbsp;update. This version features the updated and expanded&nbsp;<em>Edit Photos with the Levels and Curves Adjustment Tools</em> recipe which now covers the powerful curves tool. As an experiment, the EPUB version of <em>digiKam Recipes</em> now comes with the embedded Open Sans font family. As usual, there are also minor tweaks and fixes.</p>
<p><img src="http://scribblesandsnaps.files.wordpress.com/2014/08/digikam-31701.png" width="375" height="500"></p>
<p><a href="http://scribblesandsnaps.com/2014/11/05/digikam-recipes-4-1-1-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>