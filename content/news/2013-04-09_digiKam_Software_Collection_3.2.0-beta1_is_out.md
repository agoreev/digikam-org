---
date: "2013-04-09T06:17:00Z"
title: "digiKam Software Collection 3.2.0-beta1 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the first beta release of digiKam Software Collection 3.2.0. This version currently under"
category: "news"
aliases: "/node/690"

---

<a href="http://www.flickr.com/photos/digikam/8632965113/" title="digiKam-listview by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8112/8632965113_e8362a17b2_z.jpg" width="640" height="400" alt="digiKam-listview"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the first beta release of digiKam Software Collection 3.2.0.
This version currently under development, include a new album interface display mode named list-view. Icon view can be switched to a flat item list, where items can  
be sorted by properties columns as in a simple file manager. Columns can be customized to show file, image, metadata, or digiKam properties. This list-view mode 
is not yet fully completed and need to be review by users.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.2.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.2.0-beta1.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b> Release plan <a href="http://www.digikam.org/about/releaseplan">can be seen here...</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20530"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/690#comment-20530" class="active">Nepomuk</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2013-05-01 08:47.</div>
    <div class="content">
     <p>Will Nepomuk Support be available again?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20531"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/690#comment-20531" class="active">Nepomuk API have changed a</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2013-05-01 08:58.</div>
    <div class="content">
     <p>Nepomuk API have changed a lots and digiKam Nepomuk interface is broken. There is a plan to review interface through a GoSC 2013 project, if it's retained...</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
