---
date: "2009-05-06T14:32:00Z"
title: "New 'auto-correction-mode' for the Free Rotation image plugin"
author: "Andi Clemens"
description: "A few weeks ago I added a new feature to the Free-Rotation imageplugin: 'Automatic (Horizon) Correction'. This is nothing new, Gimp and Photoshop (and for"
category: "news"
aliases: "/node/445"

---

<p>A few weeks ago I added a new feature to the Free-Rotation imageplugin: 'Automatic (Horizon) Correction'.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/3507603498/" title="digiKam - FreeRotation auto mode von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3373/3507603498_914da80328_o.gif" width="457" height="236" alt="digiKam - FreeRotation auto mode"></a></p>
<p>This is nothing new, Gimp and Photoshop (and for sure other applications) have such a feature, too.<br>
You set two points in the preview widget and hit "Adjust" to rotate the image accordingly to these markers.</p>
<p>The difference is that it is suitable to adjust horizontal and vertical lines and determines automatically which of the modes it should use, based on<br>
the angle between the two marker points. Also the rotation direction is detected by the plugin, so that it is actually quite easy to use, you do not have to<br>
say if you want to rotate clockwise or counter-clockwise.</p>
<p>The user interface could be better, for example it would be nice to have zooming capabilities for better marker control. But right now it is not possible with the<br>
current preview widget implementation (we are working on it... :-)).</p>

<div class="legacy-comments">

  <a id="comment-18514"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18514" class="active">Thanks for this new feature! </a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Wed, 2009-05-06 16:42.</div>
    <div class="content">
     <p>Thanks for this new feature!  I find it to be very useful and will likely use this alot.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18516"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18516" class="active">Just love it</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2009-05-06 20:38.</div>
    <div class="content">
     <p>I noticed this feature when you added it to SVN and I just like it. I was thinking should it be more automatic feature where you just select the tool and you drag a line, but it makes harder to select correct points what this allows. And it is nice that buttons then show the pixel position of points. :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18517"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18517" class="active">I don't know if the point</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-05-06 20:47.</div>
    <div class="content">
     <p>I don't know if the point coordinates are really that necessary, but well right now they are displayed :-)<br>
I had planned to simply drag a line over the preview, but with current preview widget it is not possible and the points are already a "dirty" hack.<br>
When we re-design the preview widget to have a canvas, those points will be draggable (or I will go with the line implementation, don't know now).</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18522"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18522" class="active">I like the general idea. I</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Thu, 2009-05-07 13:14.</div>
    <div class="content">
     <p>I like the general idea. I use digiKam more and more for photo editing these days and only "bring out the GIMP" on special occasions, so I welcome any features that reduce the amount of switching between applications that I have to do. Keep up the good work.</p>
<p>Here are some other observations:</p>
<p>The point coordinates are probably useful for fine-tuning, but I don't like the "Click to set" button. Surely there is room to just put the values in text boxes and allow the user to change them directly?</p>
<p>Can the image be zoomed while positioning the lines?</p>
<p>I assume the automatic correction to vertical or horizontal orientation works by figuring out if the line is more or less than 45 degrees from the horizontal. If that is so, it assumes that the picture has already been rotated to the correct orientation before the finer adjustment to the rotation is made. Perhaps an option to override the automatic adjustment would be useful for some whose cameras do not have an orientation sensor and whose images do not get rotated automatically.</p>
<p>Lastly, if instead of allowing just one line to indicate what should be horizontal or vertical you allowed a second, optional line, you could use the same feature to perform perspective correction. For example, in the photo of the church, you could put two vertical lines on either side and have the rotation and perspective corrected at the same time. (Perhaps the perspective correction would allow you to indicate just how much correction you want.) Combining the two features would make it easier to make a single decision about the required cropping.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18524"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18524" class="active">&gt; The point coordinates are</a></h3>    <div class="submitted">Submitted by Andi Clemens on Thu, 2009-05-07 13:57.</div>
    <div class="content">
     <p><cite>&gt; The point coordinates are probably useful for fine-tuning, but I don't like the "Click to set" button.<br>
&gt; Surely there is room to just put the values in text boxes and allow the user to change them directly?<br>
&gt; Can the image be zoomed while positioning the lines?</cite></p>
<p>Right now the button solution is the easiest way to set two points, because we don't have a canvas for the preview widget where it would be possible to drag the points / line around. Also the plugin doesn't use zooming at the moment, so chances are that when clicking in the preview widget, you don't set the point precisely. This is why we use buttons at the moment. You can adjust your selection until it is perfect, then hit the button to set a marker point.<br>
I guess this will be changed when we use a new preview widget some day.<br>
The coordinates are actually totally useless, you can use the two sliders for fine-tuning the angle. I guess I will remove the coords again.</p>
<p><cite>&gt; I assume the automatic correction to vertical or horizontal orientation works by figuring<br>
&gt; out if the line is more or less than 45 degrees from the horizontal. If that is so, it<br>
&gt; assumes that the picture has already been rotated to the correct orientation before the finer<br>
&gt; adjustment to the rotation is made. Perhaps an option to override the automatic adjustment would<br>
&gt; be useful for some whose cameras do not have an orientation sensor and whose images do not get rotated automatically.</cite></p>
<p>Right, alignment is determined by the angle between the two marker points.<br>
If your image is not rotated correctly, you can do so by using the sliders. Auto-rotation will work on already rotated images.<br>
Let's say the image is rotated to the right, you can use the following workflow:</p>
<p>1. Roughly rotate the image to the left by using the sliders.<br>
2. Finetune the rotation by settings the marker points on the horizon<br>
3. Click "Adjust" to set the new angle.<br>
4. If it's still not aligned perfectly (although I doubt that :-)), you can use the sliders again to rotate to your liking.</p>
<p><cite>&gt; Lastly, if instead of allowing just one line to indicate what should be horizontal or vertical you<br>
&gt; allowed a second, optional line, you could use the same feature to perform perspective correction.<br>
&gt; For example, in the photo of the church, you could put two vertical lines on either side and have the rotation<br>
&gt; and perspective corrected at the same time. (Perhaps the perspective correction would allow you to indicate just how<br>
&gt; much correction you want.) Combining the two features would make it easier to make a single decision about the required cropping.</cite></p>
<p>Maybe I misunderstood this, but we have a perspective correction tool. It should do what you are describing here.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18526"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18526" class="active">The coordinates are actually</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Thu, 2009-05-07 15:09.</div>
    <div class="content">
     <p><cite>The coordinates are actually totally useless, you can use the two sliders for fine-tuning the angle. I guess I will remove the coords again.</cite></p>
<p>It would probably make things simpler.</p>
<p><cite>Auto-rotation will work on already rotated images. [...]</cite></p>
<p>I assumed wrongly, then. That sounds perfect.</p>
<p><cite>Maybe I misunderstood this, but we have a perspective correction tool. It should do what you are describing here.</cite></p>
<p>No misunderstanding; I haven't used the perspective correction tool before. I don't know what it looks like and haven't got access to digiKam at present to take a look. It just struck me that drawing one line for rotation correction and two lines for some perspective correction could be handled in a similar manner and might be rolled into one feature. Of course you'll need four lines to correct perspective fully and then you'll have what GIMP has. Does digiKam have the same? Given that rotation and perspective correction both require some cropping and anti-aliasing, I thought it would be easier to combine the two operations given that they can be performed in a similar manner.</p>
<p>Feel free to dismiss my ramblings.</p>
<p>(Have to run the CAPTCHA gauntlet now. It normally takes me several attempts to get it right.)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-18518"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18518" class="active">Woohoo! I recognize that</a></h3>    <div class="submitted">Submitted by Pedro Almeida (not verified) on Thu, 2009-05-07 00:46.</div>
    <div class="content">
     <p>Woohoo! I recognize that photo! Jerónimos Monastery, in Lisbon! Hope you've had a good time here! Keep the innovations coming, man! You (the Digikam devs) rock really hard!</p>
<p>Best wishes! =D</p>
<p>Pedro</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18519"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/445#comment-18519" class="active">I have not taken this photo,</a></h3>    <div class="submitted">Submitted by Andi Clemens on Thu, 2009-05-07 07:20.</div>
    <div class="content">
     <p>I have not taken this photo, and actually I didn't want to use it for the demonstration, because I don't have rights for the image.<br>
Somehow I messed this up, I'm pretty sure I had another image of mine loaded before as I started to do the animation.<br>
The image is from our test image repository, I hope whoever took it is ok with that.<br>
If you recognize your photo, please answer here so we can have the correct credits.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>