---
date: "2010-05-31T19:58:00Z"
title: "Perfect Cloud-based Photo Setup with digiKam and Piwigo"
author: "Dmitri Popov"
description: "Using digiKam's Kipi plugins, you can upload your photos to a variety of popular photo services, including Flickr, Picasaweb, and SmugMug. But what if you"
category: "news"
aliases: "/node/519"

---

<p>Using digiKam's Kipi plugins, you can upload your photos to a variety of popular photo services, including Flickr, Picasaweb, and SmugMug. But what if you want to host your own photo album and still be able to populate it with photos directly from within digiKam? In this case, you might want to try Piwigo, a nifty photo application that has everything you need to host and share your photos on the Web. <a href="http://www.linux-magazine.com/Online/Blogs/Productivity-Sauce-Dmitri-s-open-source-blend-of-productive-computing/Perfect-Cloud-based-Photo-Setup-with-digiKam-and-Piwigo">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19217"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/519#comment-19217" class="active">missing feature?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2010-06-03 13:24.</div>
    <div class="content">
     <p>Hi all,</p>
<p>Some time ago, I used this feature to upload some picture to picassa. It is very nice by the way. The aim of this was to share them with some relatives. However I am a bit paranoid ;-) and I missed a nice feature which is anonymazation of the pictures before uploading them (meaning removing all exif -and others- information from the files).</p>
<p>Is this feature now present (I did not find it in the documentation last time I checked), if not is it possible to implement it? Thanks.</p>
<p>Keep up the very good job, digikam rocks!</p>
<p>Cheers,</p>
<p>BrunoD.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>