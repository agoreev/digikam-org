---
date: "2014-02-25T18:51:00Z"
title: "digiKam Software Collection 4.0.0-beta3 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the third beta release of digiKam Software Collection 4.0.0. This version currently under"
category: "news"
aliases: "/node/710"

---

<a href="https://www.flickr.com/photos/digikam/12774770733"><img src="https://farm4.staticflickr.com/3689/12774770733_ef142c5754_c.jpg" width="800" height="225"></a>

<p>Dear all digiKam fans and users!</p>

<p>
digiKam team is proud to announce the third beta release of digiKam Software Collection 4.0.0.
This version currently under development, include many new feature as:
</p>

<ul>

<li>
A new tool dedicated to organize whole tag hierarchies. This new feature is relevant of <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Tag_Manager">GoSC-2013 project</a> from <b>Veaceslav Munteanu</b>. This project include also a back of Nepomuk support in digiKam broken since a while due to important KDE API changes. Veaceslav has also implemented multiple selection and multiple drag-n-drop capabilities on Tags Manager and Tags View from sidebars, and the support for writing face rectangles metadata in Windows Live Photo format.
</li>

<li>
A new maintenance tool dedicated to parse image quality and auto-tags items automatically using Pick Labels. This tool is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Image_Quality_Sorter_for_digiKam">GoSC-2013 project</a> from <b>Gowtham Ashok</b>. This tool require feedback and hack to be finalized for this release.
</li>

<li>
<p>Showfoto thumbbar is now ported to Qt model/view in order to switch later full digiKam code to Qt5. This project is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Port_Showfoto_Thumb_bar_to_Qt4_Model.2FView">GoSC-2013 project</a> from <b>Mohamed Anwer</b>.</p>
</li>

<li>
<p>A lots of work have been done into Import tool to fix several dysfunctions reported since a very long time. For example, The status to indicate which item have been already downloaded from the device is back. Thanks to <b>Teemu Rytilahti</b> and <b>Islam Wazery</b> to contribute. All fixes are not yet completed and the game continue until the next beta release.</p>
</li>

<li>
<p>This release is now fully ported to Qt4 model-view and drop last Q3-Support classes. The last pending part was Image Editor Canvas ported and completed by <b>Yiou Wang</b> through this <a href="http://community.kde.org/Digikam/GSoC2013#Port_Image_digiKam_Editor_Canvas_Classes_to_Qt4_Model.2FView">GoSC-2013 project</a>. 
In the future, port to Qt5 will be easy and quickly done, when KDE 5 API will stabilized and released.</p>
</li>

</ul>

<p>Other pending GoSC-2013 projects will be included in next 4.0.0 beta release, step by step, when code will be considerate as good quality, and ready to test by end users. See the <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">release plan</a> for details.</p>

<p>As usual with a major release, we have a long list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-4.0.0-beta3.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Thanks in advance for your reports.

</p><p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20665"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20665" class="active">Great news! I like especially</a></h3>    <div class="submitted">Submitted by Michal Sylwester (not verified) on Wed, 2014-02-26 03:55.</div>
    <div class="content">
     <p>Great news! I like especially the fixed importer, no more worrying about forgetting to download everything. I really missed it, but never got enough courage to try and fix it myself.</p>
<p>If someone is interested, I've tried to set up a ppa for saucy users at ppa:msylwester/digikam-beta. I'm not sure whether I will be able to maintain it over time, but for now it should work.</p>
<p>PS. The link above points to beta2.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20666"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20666" class="active">The link above points to beta2.</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2014-02-26 07:01.</div>
    <div class="content">
     <p>&gt;The link above points to beta2.</p>
<p>Fixed...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20667"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20667" class="active">Windowsversion</a></h3>    <div class="submitted">Submitted by wolfgang (not verified) on Sat, 2014-03-01 16:02.</div>
    <div class="content">
     <p>Hello,</p>
<p>there is a schedule of when the version 4 is available for Windows?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20668"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20668" class="active">Yes Windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2014-03-02 03:14.</div>
    <div class="content">
     <p>Yes when will a Windows version be available.... would love to test.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20673"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20673" class="active">Hello,
I would like to know</a></h3>    <div class="submitted">Submitted by Sebastian (not verified) on Sun, 2014-03-16 19:05.</div>
    <div class="content">
     <p>Hello,</p>
<p>I would like to know also if there are any plans for windows. Would be great!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20669"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20669" class="active">I as well, would like to get</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2014-03-02 22:46.</div>
    <div class="content">
     <p>I as well, would like to get a windows package. Any plans for a date of a windows release for version 4?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20670"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20670" class="active">Digikam/kipi-plugins does not</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2014-03-12 08:29.</div>
    <div class="content">
     <p>Digikam/kipi-plugins does not compile with latest libjpeg 90, do you plan to support it in the next release?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20671"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20671" class="active">planed...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2014-03-12 10:12.</div>
    <div class="content">
     <p>Yes, there is a report in macports where a bug file is already registered :</p>
<p>http://trac.macports.org/ticket/42710</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20672"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20672" class="active"> How To Compile digiKam Under Microsoft Windows Seven</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2014-03-15 08:30.</div>
    <div class="content">
     <p>How To Compile digiKam Under Microsoft Windows Seven http://www.digikam.org/drupal/node/525</p>
<p>this version possible to compile?(window)</p>
<p>how to compile digikam, and kipi-plugin....very very hard.</p>
<p>only, if possible to comfile dngconverter(in kipi-plugin).. how to compile?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20674"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20674" class="active">Nepomuk?</a></h3>    <div class="submitted">Submitted by <a href="http://www.gentoo.org/" rel="nofollow">Andreas K. Huettel (dilfridge)</a> (not verified) on Wed, 2014-03-19 23:58.</div>
    <div class="content">
     <p>Hi, just diffing the source from 3.5.0 to 4.0.0b3... I hope you do realize that Nepomuk is going to be replaced by Baloo? It's a bit strange to introduce a new dependency in still-to-appear digikam that is already being phased out in the rest of KDE... Cheers, Andreas</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20675"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20675" class="active">Picasa import</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-03-21 18:39.</div>
    <div class="content">
     <p>Hi all</p>
<p>The only thing that's keep me on Windows is Picasa. I've spent hours and hours to tag faces and I d'on't want to lose that.<br>
I read that this functionnality has been coded in GoSC-2013 project (http://community.kde.org/Digikam/GSoC2013#Read_face_from_Picasa_metadata_to_populate_digiKam_database). Would it be included in Digikam 4.0?</p>
<p>Thanks</p>
<p>Nicolas</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20676"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/710#comment-20676" class="active">you won't loose the tags,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-03-25 14:56.</div>
    <div class="content">
     <p>you won't loose the tags, that you already tagged.<br>
Better said, if you use the newest picasa and configured it so, that the facetags get saved in the image metainformationen, digikam will read the face tags. Even digikam 3 does it</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>