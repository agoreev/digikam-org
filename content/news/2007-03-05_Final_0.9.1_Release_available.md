---
date: "2007-03-05T08:41:00Z"
title: "Final 0.9.1 Release available"
author: "gerhard"
description: "After two release candidates the digiKam team is proud again to present a major improvement to digiKam and its plugins. The many new features are"
category: "news"
aliases: "/node/210"

---

<p>After two release candidates the digiKam team is proud again to present a major improvement to digiKam and its plugins. The many new features are highlighted below. For more information and resolved issues we kindly refer you to the Changelog.</p>
<p>The digiKam developer team thanks everybody who has gone through the pains of compiling, has been helping finding bugs and keep digiKam ahead of the pack.</p>
<p>The tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/">SourceForge</a>. Kubuntu packages will be available on short notice <a href="http://www.mpe.mpg.de/~ach/kubuntu/edgy/Pkgs.php">here</a> (thanks to Achim).</p>
<p><em>New Features</em><br>
---------------------<br>
General :<br>
- Native JPEG2000 image loader. Warning for lossy JPEG file saving<br>
- Tags View from Comments &amp; Tags side bar support drag &amp; drop.<br>
- Batch tool to sync all pictures metadata (EXIF/IPTC) with digiKam database content.<br>
- Status bar in Album Gui with a progress bar, text bar, and navigate bar.<br>
- Native SlideShow tool which uses the Image Preview feature. RAW files can be slided very fast. Configurable data display. Album recursive mode available<br>
-The tags trees are stored as such into IPTC fields<br>
- Usability improvements everywhere</p>
<p>AlbumGUI :<br>
- Improvement of pop-up menu of Tags Filter View and Comment &amp; Tags ON auto selection/deselection of parents/child tags in Tags treeview.<br>
- Preview picture mode use a memory cache to speed-up loading.<br>
- Preview mode has better a context pop-up menu.<br>
- Prefer Exif DateTimeOriginal to sort images (DateTimeDigitized and DateTime used as fallback)</p>
<p>Image Plugins :<br>
- All tools remember settings between sessions<br>
- All tools render properly preview of image using Color Managed View<br>
- All tools use the same keyboard shortcuts as PhotoShop<br>
- Brightness/Contrast/Gamma : setting value excursion are the same as Photoshop.<br>
- New option in all Color corrections Tools to show under-exposed and over-exposed areas of corrected picture before applying corrections<br>
- Add Border Tool : add new option to preserve aspect ratio. Border Width can be set in pixels or in percentage<br>
- Perspective Tool : add a grid and vertical/horizontal guide lines.<br>
- Ratio-crop Tool : usability improvements from Jaromir Malenko.<br>
- Auto Color Correction Tool : add new filter to perform auto-exposure corrections.</p>
<p>Image Editor :<br>
- Advanced options to keep ratio and alignment when printing pictures.<br>
- Color profiles are tested now to avoid invalid files.<br>
- Add a progress bar to Image editor/showfoto about IO image files access.</p>

<div class="legacy-comments">

  <a id="comment-18051"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/210#comment-18051" class="active">Thanks for the resource</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-12-17 13:13.</div>
    <div class="content">
     <p>Thanks for the resource</p>
         </div>
    <div class="links">» </div>
  </div>

</div>