---
date: "2008-12-16T17:32:00Z"
title: "Kipi-plugins 0.2.0-beta5 for KDE4 released"
author: "digiKam"
description: "The 5th KDE4 beta release of digiKam plugins box is out. With this new release, a new plugin named SmugExport is born. It's dedicated to"
category: "news"
aliases: "/node/414"

---

<p>The 5th KDE4 beta release of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/3097063377/" title="smugmugexportkipiplugin-kde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3079/3097063377_1bdc1932ef.jpg" width="500" height="400" alt="smugmugexportkipiplugin-kde4"></a>

<p>With this new release, a new plugin named <b>SmugExport</b> is born. It's dedicated to export images to <a href="http://www.smugmug.com">SmugMug web service</a>.</p>

<p>A plugin is available to batch convert RAW camera images to Adobe DNG container (see <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">this Wikipedia entry</a> for details). To be able to compile this plugin, you need last libkdcraw from svn trunk (will be released with KDE 4.2). To extract libraries source code from svn, look at <a href="http://www.digikam.org/download?q=download/svn#checkout-kde4">this page</a> for details.</p>

<p>Kipi-plugins are compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>JPEGLossLess</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : XMP metadata support.<br>
<b>TimeAdjust</b>     : XMP metadata support.<br>
<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br>
<b>SendImages</b>     : Complete re-write of emailing tool.<br>
<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br>
<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br>
<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br>
<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br>
<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br>
<b>RemoveRedEyes</b>  : Added an option to preview the correction of the currently selected image.<br>
<b>SmugExport</b>     : New plugin to export images to a remote SmugMug web service (http://www.smugmug.com).<br><br>

001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digiKam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>
018 ==&gt; 169637 : MetadataEdit       : EXIF Data Editing Error.<br>
019 ==&gt; 174954 : HTMLExport         : Support for remote urls.<br>
020 ==&gt; 176640 : MetadataEdit       : Wrong reference to website.<br>
021 ==&gt; 176749 : TimeAdjust         : Incorrect time/date setting with digiKam.<br>
022 ==&gt; 165370 : GPSSync            : crash when saving geocoordinates.<br>
023 ==&gt; 165486 : GPSSync            : Next and Previous photo button for editing geolocalization coordinates.<br>
024 ==&gt; 175296 : GPSSync            : Geolocalization a handy gui to manage all about geotagging.<br>
025 ==&gt; 152520 : MetadataEdit       : Unified entry of metadata.<br>
026 ==&gt; 135320 : Calendar           : Calendar year do not match locale.<br>
027 ==&gt; 116970 : Calendar           : New calendar generator plugin features.<br>

<div class="legacy-comments">

  <a id="comment-18047"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18047" class="active">Some time ago I mentioned it</a></h3>    <div class="submitted">Submitted by <a href="http://juriansluiman.nl" rel="nofollow">mithras</a> (not verified) on Tue, 2008-12-16 22:28.</div>
    <div class="content">
     <p><a href="http://www.digikam.org/drupal/node/382#comment-17936">Some time ago</a> I mentioned it was a pity Ubuntu 8.10 did not support the latest digikam releases with kipi-plugins. I found after some search the <a href="http://wiki.kde.org/tiki-index.php?page=Digikam+Compilation+on+Kubuntu+Intrepid">KDE wiki page</a> with a howto to compile digikam for Interpid. It's working fine and now I'm more realizing the great work you're doing.</p>
<p>Still one question left: what is the KDE colour theme used for the screenshots?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18048"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18048" class="active">KDE4 Color theme is...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-12-16 22:38.</div>
    <div class="content">
     <p>"Wonton Soup"... from KDE4.2 Control panel (Mandriva 2009.0)</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18063"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18063" class="active">Wonton Soup??</a></h3>    <div class="submitted">Submitted by François (not verified) on Wed, 2008-12-17 16:19.</div>
    <div class="content">
     <p>Where did the author eat a Wonton soup to use these colours?? I hope they didn't get too sick... :) </p>
<p>Out topic, yes I know :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18064"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18064" class="active">Set Beta7 free plz!</a></h3>    <div class="submitted">Submitted by François (not verified) on Wed, 2008-12-17 16:26.</div>
    <div class="content">
     <p>Gilles, you are surrounded! We saw in your flickr photostream that you have the Digikam Beta7 on your computer! Let it go free and we promise not to hurt you! ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18065"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18065" class="active">beta 7 is in the way...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-17 17:38.</div>
    <div class="content">
     <p>digiKam 0.10.0-beta7 is the next comming soon release for KDE4. svn version is already bumped. This is why you seen the release info in my screenshots.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18062"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18062" class="active">google webalbums support</a></h3>    <div class="submitted">Submitted by mxttie (not verified) on Wed, 2008-12-17 16:19.</div>
    <div class="content">
     <p>hi,</p>
<p>I'm just curious about the status of the picasaweb export plugin..? any news there? :)</p>
<p>also, could you please remove the captcha for registered logged in users? ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18066"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18066" class="active">About PicasaWebExport plugin...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-17 17:41.</div>
    <div class="content">
     <p>Plugin is already ported to KDE4 and work perfectly under Windows. <a href="http://www.digikam.org/node/394">Look here</a></p>

     </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18068"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18068" class="active">wow, cool! thanks
about the</a></h3>    <div class="submitted">Submitted by mxttie (not verified) on Wed, 2008-12-17 19:34.</div>
    <div class="content">
     <p>wow, cool! thanks</p>
<p>about the captcha: I understand you need a captcha, but it is quite unusual to have it enabled for registered users (since they alreqdy needed to enter one at registration time). anyhow, it was just a suggestion :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18067"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18067" class="active">About Captcha</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-17 17:45.</div>
    <div class="content">
     <p>Definitively, we will never remove it. The site is spamed a lots using an hard Captcha entry, and sometimes i need to remove dumy/weird entries. My time is limited as webmaster, and i prefer to code...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18080"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18080" class="active">Bug in galleryexport plugin</a></h3>    <div class="submitted">Submitted by SOULfly_B (not verified) on Fri, 2008-12-19 18:38.</div>
    <div class="content">
     <p>There's a big bug in galleryexport plugin, I don't know if it appears with beta5 as I did'nt tested previous betas.</p>
<p>I filled a bug here http://bugs.kde.org/show_bug.cgi?id=178204</p>
<p>Anyway, thanks for your work, digikam is trully wonderfull !</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18090"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/414#comment-18090" class="active">Where's the download link?</a></h3>    <div class="submitted">Submitted by Ian (not verified) on Sat, 2008-12-27 15:41.</div>
    <div class="content">
     <p>Maybe I'm blind, but where's the link to download the sources?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
