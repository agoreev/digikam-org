---
date: "2011-04-11T07:24:00Z"
title: "New Features in digiKam 2.0: Face Recognition"
author: "Dmitri Popov"
description: "Face recognition has been one of the most requested digiKam features, and the latest version of the photo management application provides this functionality. As the"
category: "news"
aliases: "/node/593"

---

<p>Face recognition has been one of the most requested digiKam features, and the latest version of the photo management application provides this functionality.</p>
<p>As the name suggests, the face recognition functionality can be used to find photos containing faces and&nbsp;attach face tags to persons in photos. This lets you quickly locate all photos of a specific person using digiKam filtering&nbsp;capabilities. <a href="http://scribblesandsnaps.wordpress.com/2011/04/11/new-features-in-digikam-2-0-face-recognition/">Continue to read</a></p>

<div class="legacy-comments">

</div>