---
date: "2011-03-03T22:07:00Z"
title: "digiKam Software Collection 2.0.0 beta3 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 3rd digiKam Software Collection 2.0.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/582"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 3rd digiKam Software Collection 2.0.0 beta release!</p>

<a href="http://www.flickr.com/photos/digikam/5453984332/" title="digikampicklabel2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5013/5453984332_bba7f267da_z.jpg" width="640" height="256" alt="digikampicklabel2"></a>

<p>With this release, digiKam include <b>Color Labels</b> and <b>Pick Labels</b> feature to simplify image cataloging operations in your photograph workflow.</p>

<p>Also, digiKam include since 2.0.0 beta2 the <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>This beta release is not stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>See also <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/5627473e632c06f02cee021781e3ed4e12507007/entry/NEWS">the list of file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19850"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19850" class="active">Why did you add that</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-03-04 22:10.</div>
    <div class="content">
     <p>Why did you add that "software collection" to the name? I think that if the name is shorter, then it is better.</p>
<p>P.D. Please, use a better captcha, it not so easy wight now for a human (me)!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19852"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19852" class="active">manual face tag button</a></h3>    <div class="submitted">Submitted by LoneStar (not verified) on Sat, 2011-03-05 13:54.</div>
    <div class="content">
     <p>still no code for the manual face tagging button??</p>
<p>this is the third beta, and still the "add a face tag" button does nothing!<br>
come on, it's something obviously crucial in the whole face tagging management.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19853"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19853" class="active">...beta4...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2011-03-05 13:57.</div>
    <div class="content">
     <p>Just implemented by Marcel in trunk...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19854"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19854" class="active">awesome!!</a></h3>    <div class="submitted">Submitted by LoneStar (not verified) on Sat, 2011-03-05 17:54.</div>
    <div class="content">
     <p>awesome!!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19855"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19855" class="active">compilation, error</a></h3>    <div class="submitted">Submitted by vince06fr (not verified) on Sun, 2011-03-06 15:24.</div>
    <div class="content">
     <p>i can't compile  :</p>
<p> <code>digiKam 2.0.0-beta3 dependencies results<br>
--<br>
--  Qt4 SQL module found..................... YES<br>
--  Qt4 SCRIPT module found.................. YES<br>
--  Qt4 SCRIPTTOOLS module found............. YES<br>
--  MySQL Server found....................... YES<br>
--  MySQL install_db tool found.............. YES<br>
--  libtiff library found.................... YES<br>
--  libpng library found..................... YES<br>
--  libjasper library found.................. YES<br>
--  liblcms library found.................... YES<br>
--  Boost Graph library found................ YES<br>
--  libkipi library found.................... YES<br>
--  libkexiv2 library found.................. YES<br>
--  libkdcraw library found.................. YES<br>
--  libkface library found................... YES<br>
--  libkmap library found.................... YES<br>
--  libpgf library found..................... NO  (optional - internal version used instead)<br>
--  libclapack library found................. NO  (optional - internal version used instead)<br>
--  libgphoto2 library found................. YES (optional)<br>
--  libkdepimlibs library found.............. YES (optional)<br>
--  Nepomuk libraries found.................. YES (optional)<br>
--  libglib2 library found................... YES (optional)<br>
--  liblqr-1 library found................... YES (optional)<br>
--  liblensfun library found................. YES (optional)<br>
--  Doxygen found............................ NO  (optional)<br>
--<br>
--  You will not be able to to generate the API documentation.<br>
--  This does not affect building digiKam in any way.<br>
--<br>
--  digiKam can be compiled.................. YES</code></p>
<p>but :</p>
<p><code>[  1%] Building CXX object extra/libkexiv2/libkexiv2/CMakeFiles/kexiv2.dir/rotationmatrix.cpp.o<br>
make[2]: *** Pas de règle pour fabriquer la cible « /usr/local/lib/libexiv2.so », nécessaire pour « lib/libkexiv2.so.10.0.0 ». Arrêt.<br>
make[1]: *** [extra/libkexiv2/libkexiv2/CMakeFiles/kexiv2.dir/all] Erreur 2<br>
make: *** [all] Erreur 2</code></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19856"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19856" class="active">Sound like a problem with your Exiv2 library...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-03-06 15:34.</div>
    <div class="content">
     <p>linker cannot found your Exiv2 shared library. Check your installation</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19857"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19857" class="active">can't compil</a></h3>    <div class="submitted">Submitted by vince06fr (not verified) on Sun, 2011-03-06 16:28.</div>
    <div class="content">
     <p>it's strange because the beta2 compile fine</p>
<p>What is the matter with the beta3?</p>
<p>:</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19858"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19858" class="active">nothing...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-03-06 17:52.</div>
    <div class="content">
     <p>nothing special about Exiv2.</p>
<p>the script about Exiv2 detection come from KDELibs. Do you have updated KDE ?</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19859"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19859" class="active">i can't compile</a></h3>    <div class="submitted">Submitted by vince06fr (not verified) on Sun, 2011-03-06 18:05.</div>
    <div class="content">
     <p>ok i find that :</p>
<p><code>Could NOT find Exiv2:  Found version "0.19.0", but required is at least "0.20" (found /usr/local/lib/libexiv2.so)</code></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-19860"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19860" class="active">A picture doesn't always paint a thousand words...</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Wed, 2011-03-09 18:55.</div>
    <div class="content">
     <p><cite>"digiKam include Color Labels and Pick Labels feature"</cite></p>
<p>Great! Eh...but what are they?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19861"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19861" class="active">Look these pages...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-03-09 20:34.</div>
    <div class="content">
     <p>http://www.mohamedmalik.com/?p=819<br>
http://www.mohamedmalik.com/?p=825</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19862"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19862" class="active">Colour and Pick Labels</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Thu, 2011-03-10 19:37.</div>
    <div class="content">
     <p>Thanks, Gilles, these look very useful. I can see myself using them all the time.</p>
<p>Combined with full side-car support (which has a major implication for anyone who invests a lot of time in tagging and wants to perform regular back-ups to the cloud), I think I'm going enjoy 2.0.</p>
<p>Well done.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19864"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19864" class="active">You know what would be awesome?  Bordeless Printing</a></h3>    <div class="submitted">Submitted by Alexis (not verified) on Fri, 2011-03-11 17:18.</div>
    <div class="content">
     <p>I've tried days to get this to work and I still can't do this properly.  I've noticed that there is a photo printing assistance, but it does not help:</p>
<p>I want to print one 4x6 photo, using the photo tray in my printer.   When I try using the photo printing assistant, there isn't even an option to select this paper size!  In this normal print dialogue there is, but it doesn't work.   </p>
<p>This has to be the single most frustrating thing about digikam, and printing photos in linux, in general.   Apart from that, great software!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19866"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19866" class="active">Is qtScript support present ?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-03-15 10:43.</div>
    <div class="content">
     <p>It was part if the gsoc 2010 but is not present in the announcement ...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19867"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/582#comment-19867" class="active">Software Collection?</a></h3>    <div class="submitted">Submitted by <a href="http://blog.linux-redaktion.com" rel="nofollow">lmd</a> (not verified) on Thu, 2011-03-24 10:48.</div>
    <div class="content">
     <p>is there a difference between Digikam and Digikam Software Collection?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
