---
date: "2010-06-18T09:54:00Z"
title: "Improve Photos with digiKam’s Local Contrast Tool"
author: "Dmitri Popov"
description: "digiKam offers several features that can improve photos containing under- or overexposed areas. For example, the Exposure Blending tool lets you merge multiple shots with"
category: "news"
aliases: "/node/527"

---

<p>digiKam offers several features that can improve photos containing under- or overexposed areas. For example, the Exposure Blending tool lets you merge multiple shots with different exposures into one perfectly exposed photo. But what if you have just a single image? In this case, you might want to give the Local Contrast feature a try. <a href="http://scribblesandsnaps.wordpress.com/2010/06/18/improve-photos-with-digikams-local-contrast-tool/">Continue to read</a></p>

<div class="legacy-comments">

</div>