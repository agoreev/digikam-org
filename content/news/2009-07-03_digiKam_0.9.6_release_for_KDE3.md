---
date: "2009-07-03T08:20:00Z"
title: "digiKam 0.9.6 release for KDE3"
author: "digiKam"
description: "Dear all digiKam fans and users! Utimate digiKam 0.9.6 release for KDE3 is out. It's a bug fix and translations updates. digiKam 0.9.6 tarball can"
category: "news"
aliases: "/node/462"

---

<p>Dear all digiKam fans and users!</p>

<p>Utimate digiKam 0.9.6 release for KDE3 is out. It's a bug fix and translations updates.</p>

<a href="http://www.flickr.com/photos/digikam/3684042676/" title="digiKam0.9.6 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3650/3684042676_17ed34136b.jpg" width="500" height="400" alt="digiKam0.9.6"></a>

<p>digiKam 0.9.6 tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">at this url</a></p>

<p>No more KDE3 release is planed in the future... </p>
<div class="legacy-comments">

</div>
