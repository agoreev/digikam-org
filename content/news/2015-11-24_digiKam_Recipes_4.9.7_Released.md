---
date: "2015-11-24T11:40:00Z"
title: "digiKam Recipes 4.9.7 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your perusal. This version features the new Use Exposure Indicators recipe along with the updated Find"
category: "news"
aliases: "/node/747"

---

<p>A new release of&nbsp;<a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a>&nbsp;is ready for your&nbsp;perusal. This version features the new&nbsp;<em>Use Exposure Indicators</em> recipe along with the updated&nbsp;<em>Find the Shutter Count Value with digiKam</em> recipe. Astute readers will also notice the new cover. That's all there is to it this time around.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/11/digikamprecipes-4-9-7.png" alt="digikamprecipes-4.9.7" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/11/24/digikam-recipes-4-9-7-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>