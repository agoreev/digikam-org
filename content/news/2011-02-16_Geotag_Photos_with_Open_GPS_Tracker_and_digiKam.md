---
date: "2011-02-16T10:40:00Z"
title: "Geotag Photos with Open GPS Tracker and digiKam"
author: "Dmitri Popov"
description: "You don’t need a fancy camera with a built-in GPS receiver to geotag your photos. An Android device with the Open GPS Tracker app and"
category: "news"
aliases: "/node/576"

---

<p>You don’t need a fancy camera with a built-in GPS receiver to geotag your photos. An Android device with the Open GPS Tracker app and digiKam can do the job just fine. The app lets you track your route and save it as a GPX file which you can then use to geocorrelate your photos in digiKam. <a href="http://scribblesandsnaps.wordpress.com/2011/02/16/geotag-photos-with-open-gps-tracker-and-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>