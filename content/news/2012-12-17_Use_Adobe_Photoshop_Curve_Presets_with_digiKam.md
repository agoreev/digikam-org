---
date: "2012-12-17T11:36:00Z"
title: "Use Adobe Photoshop Curve Presets with digiKam"
author: "Dmitri Popov"
description: "While digiKam can't handle Adobe Photoshop curve presets directly, the application supports curves in the GIMP-compatible CRV format. So if you want to use Adobe"
category: "news"
aliases: "/node/677"

---

<p>While digiKam can't handle Adobe Photoshop curve presets directly, the application supports curves in the GIMP-compatible CRV format. So if you want to use Adobe Photoshop curve presets with digiKam, you have to convert them into <em>.crv</em> files — and the <em>acv2gimp.py</em> Python script can help you with that. <a href="http://scribblesandsnaps.com/2012/12/17/use-adobe-photoshop-curve-presets-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>