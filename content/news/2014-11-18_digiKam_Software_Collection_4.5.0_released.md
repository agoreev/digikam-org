---
date: "2014-11-18T09:56:00Z"
title: "digiKam Software Collection 4.5.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.5.0. This release includes bugs fixes and"
category: "news"
aliases: "/node/724"

---

<a href="https://www.flickr.com/photos/digikam/15819454062" title="digiKam 4.5.0 Windows 7"><img src="https://farm9.staticflickr.com/8683/15819454062_6559dd16bb_c.jpg" width="800" height="500" alt="digiKam 4.5.0 Windows 7"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.5.0. This release includes bugs fixes and switch as optional some dependencies as libkipi, libkface, libkgeomap dedicated respectively to support Kipi plugins, Face management, and Geo-location maps. By this way we will be able to port digiKam to KF5/Qt5 step by step.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.5.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.5.0 available through the KDE Bugtracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.5.0.tar.bz2.mirrorlist">KDE repository</a>. The <a href="http://download.kde.org/stable/digikam/digiKam-installer-4.5.0-win32.exe.mirrorlist">Windows installer</a> is also available at this place.
</p>

<p>Have fun playing with your photos using this new release,</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20933"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20933" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by cc (not verified) on Tue, 2014-11-18 21:30.</div>
    <div class="content">
     <p>Works like a charm!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20934"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20934" class="active">XMP file bug under Windows is solved</a></h3>    <div class="submitted">Submitted by Piter Dias (not verified) on Wed, 2014-11-19 02:18.</div>
    <div class="content">
     <p>I just tested it under Windows and a bug related to Digikam not writing to XMP sidecar files is solved. Can't wait to catalogue the remaining photos...</p>
<p>Thanks and congratulations.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20936"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20936" class="active">Digikam 4.5.0 does not work with Windows 8.1</a></h3>    <div class="submitted">Submitted by Pascal (not verified) on Sun, 2014-11-23 11:21.</div>
    <div class="content">
     <p>Hello,</p>
<p>I tried to install Digikam on Windows 8.1 since version 3.x. The latest version 4.5.0 does not work better than previous.</p>
<p>I was able to create an album containing about thirty photographs. I entered captions, tags and geolocation for each photograph.</p>
<p>The software systematically crash each time I want to display a specific photograph. I guess this is due to KDE because a lot of kioslave processes are running (25 processes).</p>
<p>Does the latest version of Digikam include the latest version of KDE?</p>
<p>When does a stable version of Digikam/KDE will be available for Windows?</p>
<p>Kind regards</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20937"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20937" class="active">testing on 8.1</a></h3>    <div class="submitted">Submitted by cc (not verified) on Sun, 2014-11-23 20:07.</div>
    <div class="content">
     <p>I am running 4.5.0 on Windows 8.1 right now. I have 7 kioslave processes right after setup and start of the program.</p>
<p>That number does not change for me at all, regardless of what i do in digikam. Could you describe your problem (and setup) in more detail? I would then try to verify your finfings.</p>
<p>I also think that the comments section is a poor place to discuss bugs. I have brought two bugs to the devs attention via the KDE bugtracker: https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;format=guided</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20939"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20939" class="active">Hi,
still very unstable on</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2014-11-24 11:46.</div>
    <div class="content">
     <p>Hi,<br>
still very unstable on Ubuntu 14.04.<br>
thanks for your work</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20941"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20941" class="active">Check which version of</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Mon, 2014-11-24 16:10.</div>
    <div class="content">
     <p>Check which version of libsqlite3-0 you are using on ubuntu 14.04. If you still use 3.8.2 that comes with 14.04 as standard I suggest you backport or install libsqlite3-0 (and other sqlite3 packages you might use) from ubuntu 14.10 which bring you version 3.8.6 that will help from some sqlite3 related crashes of digikam. Think that will give you a more stable experience of Digikam.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20940"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20940" class="active">Sounds like something external to digiKam</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2014-11-24 13:33.</div>
    <div class="content">
     <p>It sounds like digiKam is having difficulty connecting to something, so another kioslave process gets launched. As cc suggested, it would be ideal if you could <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;format=guided">submit a bug report</a> and we can figure out this problem on there. One thing to try is disabling any firewall / anti-virus software temporarily and seeing if the problem goes away, or 'whitelisting' digiKam and kioslave. Another possibility is a conflict between IPv6 and IPv4 settings in your hosts file and you could try <a href="https://bugs.kde.org/show_bug.cgi?id=285821#c35">this trick</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20942"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20942" class="active">Crash on Win 8.1?</a></h3>    <div class="submitted">Submitted by KMAC (not verified) on Tue, 2014-11-25 04:35.</div>
    <div class="content">
     <p>Hello.  I have been trying to get DigiKam working on my 64-bit Windows 8.1 machine for the past several versions.  I uninstall each one and install the new version.  Anytime I click Import at the top, the program crashes.  I cannot import anything.  I do not understand what is happening.  I want to like this product as it appears to be a great application, but I clearly am either doing something wrong or something.  I have even tried right-clicking the installer and running as admin and also doing the same to run the program after install as admin.</p>
<p>This is the error I see in the crash screen:  Executable: digikam.exe PID: 39576 Signal: EXCEPTION_ACCESS_VIOLATION (0xc0000005)</p>
<p>HELP!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20944"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20944" class="active">I also haven't been able to</a></h3>    <div class="submitted">Submitted by aidan (not verified) on Sat, 2014-11-29 12:35.</div>
    <div class="content">
     <p>I also haven't been able to import anything since 4.2. Still import from memory card doesn't work. Tried several Windows 8.1 machines.<br>
I tried lodging a bug but even that process crashes - says can't login, but I have logged in. I manually submitted the bug on the website but still is broken.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20943"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20943" class="active">FACE RECOGNITION WORKS ! ! !</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-11-25 20:27.</div>
    <div class="content">
     <p>Yeah! The first Digikam version where face recognition works for me (openSuse 13.1). Great - thank you very much. Now I can crap Picasa via VirtualBox.</p>
<p>Go on - Digikam is great!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20945"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20945" class="active">Lot of noise in RAW development</a></h3>    <div class="submitted">Submitted by Reeja (not verified) on Wed, 2014-12-03 11:13.</div>
    <div class="content">
     <p>Hi all,</p>
<p>with digikam 4.4.0 raw development of my Fuji X20 RAF files worked like a charm with pretty good results. After update to digikam 4.5.0 I get lots of noise when developing the same image - has been anyone experiencing the same effect?</p>
<p>Regards<br>
Reeja</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20946"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20946" class="active">Thanks and a few proposals/questions</a></h3>    <div class="submitted">Submitted by eBop (not verified) on Fri, 2014-12-12 18:29.</div>
    <div class="content">
     <p>Hi busy developers!<br>
At first let me say: Digikam is unique in the field of open source and freeware photo management! Thanks for that great piece of software!<br>
But there are a few points I want to mention where improvements are needed:<br>
- writing speed of SQLite seems to be very low. When do you think will Digikam offer MySQL-Support that isn’t „experimental“ any more?<br>
- face recognition is getting better with every new version of Digikam, but it still makes many, many mistakes: a lot of objects which aren’t faces are recognized as faces and have to be erased manually.<br>
- face recognition running over a collection of less than 7,000 photos lasts many, many hours and makes Digikam crash (Windows 8.1 64bit with latest updates, notebook with Intel Core2 Duo CPU 2.1 GHz, 6 GB RAM).</p>
<p>I hope this isn’t the wrong place to report these questions/bugs.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20947"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20947" class="active">Viewing thumbnails of offline archives</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2014-12-13 10:29.</div>
    <div class="content">
     <p>Just wondering whether there is any plan to implement viewing of previews of offline image archives?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20948"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/724#comment-20948" class="active">Showing thumbnails of offline archives</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2014-12-15 23:10.</div>
    <div class="content">
     <p>This is a feature I'm waiting for a long time as well: would it be possible to show thumbnails of pictures on removable media even if the media is not attached since all thumbnails are stored in the SQLite database? Thumbnails could be shown with a small symbol highlighting that the original picture is on a currently not attached removable media.<br>
Where should we submit this suggestion?</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>