---
date: "2012-09-19T14:09:00Z"
title: "digiKam Software Collection 3.0.0-beta1 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one summer of active development with Google Summer of Code 2012 students, digiKam team is proud to announce"
category: "news"
aliases: "/node/665"

---

<a href="http://www.flickr.com/photos/digikam/7981970248/" title="splash-digikam by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8306/7981970248_776c829bbb.jpg" width="500" height="307" alt="splash-digikam"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one summer of active development with Google Summer of Code 2012 students, digiKam team is proud to announce the first beta of digiKam Software Collection 3.0.0.
This version is currently under development, following GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.0.0-beta1a.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20374"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20374" class="active">Nice to see. But once again</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-09-19 14:34.</div>
    <div class="content">
     <p>Nice to see. But once again the old questions: Is the windows version abandoned? How about the USB-bug? I'm sure the program becomes better and better, but alas, I haven't been able to use it for months, since it always crashes on Win XP as long as USB storage is plugged in.<br>
An answer would help a lot, because I need to archive my photographs and I'd like to know if I finally have to switch to Photoshop, which I don't like, because it is by far not as useful as digiKam for archiving.<br>
So, do you still care for a windows version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20376"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20376" class="active">Windoze?</a></h3>    <div class="submitted">Submitted by <a href="http://www.archlinux.me/msx" rel="nofollow">Martin C.</a> (not verified) on Wed, 2012-09-19 17:57.</div>
    <div class="content">
     <p>You shouldn't be running such and old and insecure OS anyways, why not use this great opportunity and try GNU/Linux?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20378"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20378" class="active">Why I stick with windows ...</a></h3>    <div class="submitted">Submitted by Thomas Schweikle (not verified) on Thu, 2012-09-20 01:50.</div>
    <div class="content">
     <p>Why not switching compleateley to GNU/Linux (or FreeBSD)? One, and only one point: not all software needed is available for GNU/Linux. The Camera I own has software not ported to GNU/Linux.</p>
<p>And there is other software not available for GNU/Linux: Geogrid Viewer for example – and since no one is willing to port these (there not running using wine) I'll stick with windows.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20384"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20384" class="active">Virtualisation</a></h3>    <div class="submitted">Submitted by Logi (not verified) on Mon, 2012-09-24 11:34.</div>
    <div class="content">
     <p>I understand that there can be reasons for sticking with Windows, but if it's only 1-2 programs that are holding you back, or if they are rarely used, then you might get away with running XP in a virtual machine hosted under linux. Perhaps you'll find a replacement for your camera app when you are able to explore the available software. Maybe you'll still need the VM. But it's worth considering.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20408"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20408" class="active">virtual friends</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2012-10-11 12:18.</div>
    <div class="content">
     <p>u can, 4 now, get a free virtual machine from yer dear friends at oracle, and test linux from that...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20382"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20382" class="active">Well, there are certainly</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2012-09-20 20:58.</div>
    <div class="content">
     <p>Well, there are certainly good arguments to switch to Linux. But I also have very good reasons to keep window and mac os x - for example I don't think there's anything like ProTools for Linux, is there? Or anything like foobar2000?</p>
<p>The developers decided to provide a windows version but never really supported it. All I'm asking is whether the windows version is abandoned. If so, it's really sad because digiKam is one fantastic piece of software and deserves to be made available for the much greater windows user-base. If they give me a sign and say they're still interested in developing for windows but need more time - great. I'm willing to wait. But I'd like to know if there actually is any planned new version to wait for.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20391"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20391" class="active">"Is there anything like</a></h3>    <div class="submitted">Submitted by Adam (not verified) on Sun, 2012-09-30 11:53.</div>
    <div class="content">
     <p>"Is there anything like foobar2000?"  Do you have any idea how many music players there are for Linux?</p>
<p>I don't know anything about ProTools, but if it doesn't work in WINE, it surely would work in VirtualBox.  I use Windows XP in VirtualBox just to run two apps, and VirtualBox makes them work seamlessly, as if I were running Windows and Linux at the same time...which, I suppose, I am.  I can even connect my phone through USB to iTunes running in VirtualBox.  If it weren't for the fact that one of the apps uses .NET instead of a decent toolkit like Qt, I wouldn't even need to use a VM.  Of course, the complicated mess that is .NET doesn't work well in WINE--they're too busy reimplementing Win32 and DirectX, etc. to do .NET too.</p>
<p>Free yourself from enslavement to Microsoft.  :)</p>
<p>(THESE CAPTCHAS ARE IMPOSSIBLE!  DOES ANYONE ACTUALLY TEST THESE THINGS?!)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20420"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20420" class="active">Apache Commons FTP - Uploading files to server</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-10-24 04:35.</div>
    <div class="content">
     <p>Lately we are seeing several tutorials on how to consume data from a web site or service, this time it is going to present the other side, to upload files to a server.<br>
This task can be done in many different ways, in this tutorial we are going to do it via FTP, in a practical and simple really.<br>
The first thing we note Is that we will rely on one of the gems That Can Be Found out there at the library, On this occasion we will use the Apache Commons FTP.</p>
<p>Once downloaded file will include the commons-net-3.0.1.jar in buid path of our project. When we Indicate the name of the file is Because It is the current version, logically add the file to the latest version.<br>
Once this jar in your folder / libs, and before we go, <a href="http://hostgatorallabout.com">click here</a> we will endow our project only permission Requiring this tutorial, the manifest will<br>
&nbsp;<br>
And start with the code. For this occasion I have added a button That When Pressed will call the method originally called "SubirArchivo ()" in an action That Try, instantiate the FtpClient, will pass the data from our file server and Indicate That We want to raise our terminal (and location), and in That folder we want to keep our server.<br>
This really is not much mystery, as it is the process That is Followed Whenever something move or climb to a server.<br>
Lets see the commented code.<br>
The button listener is already Known to All, But That Will not be to put<br>
&nbsp;<br>
Now we come to the interesting part, the method will raise our file to the server, we will see next to commented and then if you want to try on a project no more than your copy.</p>
<p>This tutorial is a basis on Which to start working, the possibilities are many, and now the thing is to adapt and add the Necessary Things That requires our project.<br>
With This Simple example and a simple FTP client FileZilla rápidmente type can be the results.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20397"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20397" class="active">hmmm</a></h3>    <div class="submitted">Submitted by <a href="http://www.ignaz.org" rel="nofollow">Vamp898</a> (not verified) on Tue, 2012-10-09 19:33.</div>
    <div class="content">
     <p>What about Ardour or Rosegarden?</p>
<p>And foobar2000, there are 2034809238409324 replacements, just to name Juk, amarok, banshee, vlc, rhythmbox, `whatever your package manager finds`</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20418"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20418" class="active">I Agree with Adam, Use Linux</a></h3>    <div class="submitted">Submitted by MrRickle (not verified) on Wed, 2012-10-17 22:40.</div>
    <div class="content">
     <p>digiKam is a good excuse to go open source, you know it's the "right thing" to do anyway.<br>
I was scared to switch, so I first switched and put only the  windows programs I absolutely needed in a Virtual Box.<br>
At this point the only one I am still using in the Virtual Box is Quick Books, that because I can't move my data easy.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20377"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20377" class="active">Windows Version would be really great</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Wed, 2012-09-19 19:57.</div>
    <div class="content">
     <p>It's really a pity that there is no new windows version. If I see the download counts for the windows version on sourceforge it would worth the effort to support it a bit more.<br>
Digikam is a great software and I would also love to use it on my windows system to manage all my images.<br>
Unfortunately there is not a lot of information here on the page. The release plan is totally outdated and there are seldom helpful responses of the developers to the comments on the blog.<br>
Instead of having new versions every month I would prefer an update on the release plan and a kind of a roadmap. From my opinion digikam has enough features right now and it would be helpful to focus on fixing the issues that prevent this great software to run on other operating systems...<br>
Anyway thanks for your effort to develop this great open source software.</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20405"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20405" class="active">digiKam is developed for free</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:31.</div>
    <div class="content">
     <p>To achieve the goals you require would either necessitate additional developers or paid developers. digiKam is developed gratis in the free time of the developers. This means that unfortunately releases for Windows will only occur when possible. A source code release is much easier to time because it only depends on packaging up a directory of files, so developers just make sure they get code they want to include committed in time. A compiled release is much more complicated, especially in this case, because the underlying software, KDE SC for Windows, is unstable. This means that to receive the latest bug fixes in the underlying code base requires updating the KDE SC build environment, which unfortunately means fixing a lot of bugs at build time. Sometimes this takes days, sometimes months. Especially since, as I said before, this is done in developer's free time. You are welcome to contribute your own time or money if you would like to see things released faster or more regularly. This is not just open source software, this is free software. There is a big difference.  You can download digiKam 2.9.0 for Windows from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20409"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20409" class="active">Thanks a lot for your effort.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-10-12 22:38.</div>
    <div class="content">
     <p>Thanks a lot for your effort. Sorry to say that 2.9.0. on WinXP at first still crashed a lot when opening. Now it seems to run, but it doesn't create any thumbnails. Sometimes it shows the numbers of pictures in every album correctly in the album-tree, sometimes it shows the albums but tells me there are no pictures. Ruebuilding thumbnails doesn't work, the progress bar stays at 0%.</p>
<p>I have no clue where else to post this, so I hope Ananta Palani reads this...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20410"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20410" class="active">Thanks for the report, please file a bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2012-10-13 16:42.</div>
    <div class="content">
     <p>Sorry you are experiencing such frustrating problems! It would help a lot if you could file a bug report <a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=2.9.0">here</a> for this version which would allow you to track any updates more easily, and serves as a reminder to us on what needs to be fixed. Plus we can notify you when we have made changes so that you can verify the fix! Please include details about your setup, such as the approximate number of photos, previous version of digiKam that you used, what type of storage system you are using (solid state, external, internal, network, etc.), what type of database (local, remote MySQL, etc.) you use, and any additional information you can, plus the detailed description of the problem. Did you experience this problem in the previous version you used? Hopefully we can get it fixed for you!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20379"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20379" class="active">And Once Again</a></h3>    <div class="submitted">Submitted by Bob  (not verified) on Thu, 2012-09-20 03:20.</div>
    <div class="content">
     <p>With all the photography tools that Windows has you should be asking. Hey Adobe, Corel, HDR Soft, OnOne, where is the Linux versions!!. These companies continue to abandon Linux!. Because all they give a shit about is the OSX and Windows platforms. So stop whining!!!. Dualboot or run Linux alone and help support open-source software and platforms. At least you will show that you care, unlike these stupid companies!!.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20383"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20383" class="active">Corel AfterShot Pro (formerly</a></h3>    <div class="submitted">Submitted by Tim (not verified) on Sun, 2012-09-23 21:23.</div>
    <div class="content">
     <p>Corel AfterShot Pro (formerly Bibble Pro) is quite nice on Linux. I use it for my RAW workflow. Still like digikam overall better though for organization and viewing.</p>
<p>(And I too wish digikam for windows got more love, as I have to use it sometimes. But it seems windows people have other priorities. Can't blame volunteer linux developers for not caring about windows.)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20389"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20389" class="active">Just Fix It and Stop Your Whining</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-09-28 02:58.</div>
    <div class="content">
     <p>Sadly Bob, it's people like you that alienate all potential Linux users and ultimately causes software producers to stay with the mainstream/safe bet.  For many user Linux is just a pain in so many ways - a pain to install and update software - a pain to use and learn.  Some open source developers get it - the people working on the Gimp or Blender are good examples. They've created good cross platform applications.  For people like me with a variety of machines - several Windows 7 machines, Linux Mint 64 and 32 bit machines and Windows XP all on the same network - cross platform applications are key.  Why don't the Digikam people get that?  Don't get me wrong DigiKam is an amazing pieces of software.  But the development team seems to be struggling to get a solid Linux release at time. And for Window?  I know let's  blame it on KDE - BS!!!!  Just find a way to fix it.  To be taken seriously and stay viable over the long run, any application needs to be cross platform compatible.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20388"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20388" class="active">I am Windows 7 user (for</a></h3>    <div class="submitted">Submitted by askan (not verified) on Thu, 2012-09-27 21:11.</div>
    <div class="content">
     <p>I am Windows 7 user (for whatever reason) and I would be more than glad to have the new DigiKam available for my OS as well. </p>
<p>Kind regards<br>
askan.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20394"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20394" class="active">Hi,
for all Windows User, I</a></h3>    <div class="submitted">Submitted by Holgerw (not verified) on Tue, 2012-10-02 07:06.</div>
    <div class="content">
     <p>Hi,</p>
<p>for all Windows User, I think, it would be a good idea to look at other stuff for managing fotos. Why?</p>
<p>In my opinion digikam is a very complex software with many dependencies and so there are many possibilities for bugs. So the developers have to do a lot of work to offer a stable piece of software. </p>
<p>And digikam is a primary free software project. The developers have to decide but I'm sure, Linux as a free OS has priority and not other non-free stuff like Mac or Win.</p>
<p>To argue with cross-platform: The situation between firefox or an office suite like OpenOffice and a program for managing and modifying fotos like digikam is different: A lot of hardware producer like canon, logitech and other spent software with their products, only for Mac and Windows. Linux will be ignored. So why should a developer of free software handle Win or Mac with same priority like a free operating system?</p>
<p>If I like a piece of software, if I must have it, but I can only use it with Windows, then I've to use it with its necessary context, in this case with Windows. And if I like a piece of software, if I must have it, and new versions are only available for Linux, then I've accept this and have to install Linux. But there is a difference between case one and two: With case one I've to buy an operation system, building as closed software. In case two I can choose between a lot of distributions offering free software and I've to paid nothing.</p>
<p>So, dear users of Windows: If you like digikam so much, feel free to download a Debian based distribution or OpenSUSE, Mageia and install it in Vbox or with a dual boot solution and you'll get actual stuff of digikam.</p>
<p>Weeping but having a lot of alternatives is a little bit strange.</p>
<p>Apart from that: I think digikam 3.0.0 is on a good way. I was a little bit abrasively because of offering some broken functions, coming with broken cimg stuff. But it seems, the developers have solved this with this new version. Thanks a lot for your work.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20395"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20395" class="active">hanging on a hook</a></h3>    <div class="submitted">Submitted by <a href="http://www.theofruendt.com" rel="nofollow">Téo</a> (not verified) on Tue, 2012-10-02 23:58.</div>
    <div class="content">
     <p>I fully agree with "Holgerw" in his post "...for all Win user", writing: " Linux as a free OS has priority and not other non-free stuff like Mac or Win."</p>
<p>I hear this "do this, do that" from all kind of pic-desk-editors,  complaining about me working with digikam &amp; gimp. They say this is suposingly not "professional". But the same smart-butts don't even would pay higher fees. So it was my pleasure to kick ehm all out incl mickysoft. "This is not working" is often just the result of not reading manuals. Ok, sometimes there are problems but still  better working on non-closed software then hanging on a hook. I rather donate a good amount to authors of open-software then being forced to pay a fixed price!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20406"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20406" class="active">digiKam 2.8.0 and 2.9.0 for Windows now available</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:34.</div>
    <div class="content">
     <p>You can download digiKam 2.9.0 for Windows from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>. I probably won't be releasing digiKam 3.0.0 until it is out of beta, unless demand is high enough due to the time required to make a release.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20396"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20396" class="active">Digikam 2.7 for windows is taking its time when starting up.</a></h3>    <div class="submitted">Submitted by Wilco (not verified) on Sat, 2012-10-06 16:39.</div>
    <div class="content">
     <p>I am trying Digikam 2.7 for windows. Starting it is taking very long. And there is no activity anywhere on the computer. It is almost sleeping with 3% CPU on four cores. Also no network activity. I have only 45000 pictures out of 70000, which are on an nfs, now in Digikam. The Linux version of Digikam didn't need more than a minute or two to get started with the same pictures. </p>
<p>I have been using digikam for ages, can't even remember when I started using it. I am very fond of it because it makes it very easy for me to tag all those photo's. So I hope that the start issue in the windows version can be solved.</p>
<p>Unfortunately my Linux computer is completely dead.<br>
Probably I need a new power unit.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20404"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20404" class="active">Please try digiKam 2.9.0 for Windows released today</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:23.</div>
    <div class="content">
     <p>Please try digiKam 2.9.0 for Windows released today and if you continue to encounter problems, <a href="https://bugs.kde.org/component-report.cgi?product=digikam">file a bug report</a>. You can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20413"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20413" class="active">2.9 is also slow when starting up (windows 7)</a></h3>    <div class="submitted">Submitted by Wilco (not verified) on Sun, 2012-10-14 09:16.</div>
    <div class="content">
     <p>Hi, thanks.</p>
<p>I have 2.9 for a few days now. I start it in the morning and about 1,5 hour later I can use it without any problems ;-). So I will post a bug report later today, but I think somebody else allready did that.</p>
<p>Except for the long starting time it works excellent. It took a while, but the database with tags has been rebuild completely from the metadata in the photo's.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20403"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20403" class="active">digiKam 2.8.0 and 2.9.0 for Windows released today</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-10-10 00:20.</div>
    <div class="content">
     <p>It has not been abandoned, digiKam 2.7.0 for Windows was released less than 3 months ago, and new versions of digiKam have only been available for about 2 months. Building digiKam on Windows has many complications due to the instability of KDE SC for Windows that digiKam for Windows relies on. For instance, the USB <a href="https://bugs.kde.org/show_bug.cgi?id=303850">bug 303850</a> turns out to be a problem with KDE SC that was resolved somewhere between KDE SC 4.8.3 and 4.8.5, which I updated to for this release. Each update of KDE SC requires many hours of bug fixing and patching in the KDE SC build environment. Since this is done in free time for free it just takes time. Regardless, you can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>. I probably will wait to package digiKam 3.0.0 until it is no longer in beta due to the time required to create a package.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20407"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20407" class="active">A big thanks to the developers</a></h3>    <div class="submitted">Submitted by Paulo (not verified) on Wed, 2012-10-10 11:35.</div>
    <div class="content">
     <p>I would like to express my gratitude for making this fantastic program available (in both Linux and Windows). I am primarily using it on Linux. I do have some programs only available for Windows (Nikon software) so I am running that on Virtual box. A solution that works pretty well as I can use my Digikam database and still access the same photos with e.g., Nikon Capture NX2.  </p>
<p>My wife runs Windows so for her to access our photo database, she needs digikam on Windows. So of course I would like to see the Window version release dates be in line with those for Linux. But I also fully appreciate that time and other constraints or priorities of the developers may prevent this. </p>
<p>I find it strange that some people seem to be unable to accept that developers of open source software have their own priorities (often dictated by resource and time constraints I reckon), while they at the same time think it to be completely acceptable for commercial companies to ignore Linux. (don't get me wrong, I think both commercial companies and open source software developers are in their full right deciding on their own priorities - even though I might not always like it).</p>
<p>But especially for open source software developers, there is no reason to complain or become offensive I would say... it is their time, not yours they are spending to make a great piece of software! Gratitude that someone is spending that time to make something great for you is much more appropriate! And if you really think the developer has its priorities wrong, step in and contribute (time, money, whatever).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20411"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20411" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2012-10-13 16:45.</div>
    <div class="content">
     <p>Thanks for taking the time to post this Paulo, it is much appreciated.</p>
<p>Best,<br>
-Ananta</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20375"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20375" class="active">Face Recognition in Digikam</a></h3>    <div class="submitted">Submitted by <a href="http://bridgehosting.net/" rel="nofollow">Bridge Hosting</a> (not verified) on Wed, 2012-09-19 16:10.</div>
    <div class="content">
     <p>Face Recognition - "Status : Not completed."</p>
<p>:0(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20385"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20385" class="active">Yeah, it's a pity.
Face</a></h3>    <div class="submitted">Submitted by urcindalo (not verified) on Tue, 2012-09-25 11:21.</div>
    <div class="content">
     <p>Yeah, it's a pity.<br>
Face recognition is the one digikam feature I've been dreaming of for sooo long.<br>
IMHO, when implemented, it will make digikam a truly KILLER application (provided it will work well, of course) :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20417"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20417" class="active">Face recognition needed badly</a></h3>    <div class="submitted">Submitted by MrRickle (not verified) on Wed, 2012-10-17 22:26.</div>
    <div class="content">
     <p>I was using Picasa on windows, switched to linux, still used picasa in wine, then Face Recognition looked so close in DigiKam, I started using DigiKam, That was years ago, and now I don't have my 30000+ pictures ID'd by faces.<br>
(I did have that with Picasa, but Picasa on the desktop is a dead horse now)<br>
I was soo... hoping Libkface 3 would have it working. </p>
<p>Except for Face Recognition, after some getting used to it, I'm happy with digiKam.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20392"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20392" class="active">Dude, face recognition is</a></h3>    <div class="submitted">Submitted by Adam (not verified) on Sun, 2012-09-30 11:56.</div>
    <div class="content">
     <p>Dude, face recognition is hard.  Even Apple can't get it right.  I don't think this is a valid complaint, even if made mildly.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20381"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20381" class="active">Sweet!</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Thu, 2012-09-20 18:35.</div>
    <div class="content">
     <p>With everyone here so far complaining and criticizing I just wanted to let the developers know that this is the BEST photo organizing software I have ever seen. Thank you so much for giving me the tools I need to manage the plethora of photos my digital camera has enabled me to produce : ) I used to really like Picasa but digiKam has surpassed Picasa in nearly every respect that is important to ME. I appreciate most the excellent tagging support and that there is a 'settings' section with enough check boxes to make most any geek happy. Keep up the great work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20386"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20386" class="active">How about porting it to Android or iOS?</a></h3>    <div class="submitted">Submitted by urcindalo (not verified) on Tue, 2012-09-25 11:26.</div>
    <div class="content">
     <p>Since Android is based on Linux and iOS on FreeBSD, will the devs consider porting digikam to modern smartphones and tablets?</p>
<p>These devices represent the future of computing for the masses, so I think this is not such a crazy idea.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20387"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20387" class="active">Non-destructive editing</a></h3>    <div class="submitted">Submitted by Redi (not verified) on Thu, 2012-09-27 19:56.</div>
    <div class="content">
     <p>Do you think that it would be possible to add TRUE non-destructive editing like RawTherapee or Darktable already have?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20390"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20390" class="active">backup</a></h3>    <div class="submitted">Submitted by marek (not verified) on Sun, 2012-09-30 09:44.</div>
    <div class="content">
     <p>I have an idea:</p>
<p>add "backup button" - for example some simple "gui" for rdiff-backup and rdiff-backu-fs (for mee is rdiff-backup the best - simple and powerfull)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20393"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20393" class="active">Summary please?</a></h3>    <div class="submitted">Submitted by Adam (not verified) on Sun, 2012-09-30 11:57.</div>
    <div class="content">
     <p>I have no idea what is new in Digikam 3.</p>
<p>P.S.  I've had it with these captchas.  Innocent people are suffering more than the bad guys.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20422"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/665#comment-20422" class="active">Thanks </a></h3>    <div class="submitted">Submitted by <a href="http://jandmofnaples.com" rel="nofollow">Wes</a> (not verified) on Thu, 2012-11-01 22:58.</div>
    <div class="content">
     <p>Thanks for the update...I am going to check it out =)</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
