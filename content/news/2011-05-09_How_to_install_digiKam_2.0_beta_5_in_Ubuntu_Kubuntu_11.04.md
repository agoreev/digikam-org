---
date: "2011-05-09T21:22:00Z"
title: "How to install digiKam 2.0 beta 5 in Ubuntu & Kubuntu 11.04"
author: "Mohamed Malik"
description: "If you are using ubuntu 11.04 and if you are eager to test the upcoming digiKam 2.0 beta releases then you should use Philip Johnson's"
category: "news"
aliases: "/node/600"

---

<p>If you are using ubuntu 11.04 and if you are eager to test the upcoming digiKam 2.0 beta releases then you should use Philip Johnson's PPA to install the latest beta of digiKam 2.0.( <a href="http://www.mohamedmalik.com/?p=1019">Continue to read...</a>)</p>

<div class="legacy-comments">

  <a id="comment-19890"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19890" class="active">beta 5</a></h3>    <div class="submitted">Submitted by Jack (not verified) on Tue, 2011-05-10 16:30.</div>
    <div class="content">
     <p>I just installed beta 5 from Philip Johnson's PPA and it won't launch.  I get this message:</p>
<p>QSqlDatabasePrivate::removeDatabase: connection 'ConnectionTest' is still in use, all queries will cease to work.<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZN11KDcrawIface12RExpanderBox12readSettingsER12KConfigGroup</p>
<p>Any idea why?</p>
<p>jack</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19891"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19891" class="active">libkdcraw</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-05-10 16:37.</div>
    <div class="content">
     <p>Check your libkdcraw install. there is a binary uncompatibility. Probably the package is not updated...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19892"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19892" class="active">beta 5</a></h3>    <div class="submitted">Submitted by Jack (not verified) on Tue, 2011-05-10 16:42.</div>
    <div class="content">
     <p>Nevermind.  Uninstalled and reinstalled and it works fine now.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19894"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19894" class="active">Great</a></h3>    <div class="submitted">Submitted by Noeck (not verified) on Tue, 2011-05-10 22:07.</div>
    <div class="content">
     <p>There are really great improvements. Usually I do not use thrid party ppas, but Digikam 2 is worth it.<br>
Face recognition is an amazing feature. The detection works very good, the recognition not so much.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19899"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19899" class="active">debian</a></h3>    <div class="submitted">Submitted by marek (not verified) on Thu, 2011-05-19 19:29.</div>
    <div class="content">
     <p>and how I install digikam 2 in debian? compilation from source fail :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19900"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19900" class="active">I've installed on 11.04, and</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-05-25 06:44.</div>
    <div class="content">
     <p>I've installed on 11.04, and found it slow, prone to crash and suffering from several othre bugs. The face recognition works to the extent that faces are detected, but it hasn't put a name to a single one when given some training data. Maybe it works better on KDE than Unity.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19902"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/600#comment-19902" class="active">If you are using ubuntu 11.04</a></h3>    <div class="submitted">Submitted by sid (not verified) on Tue, 2011-05-31 15:20.</div>
    <div class="content">
     <p>If you are using ubuntu 11.04 and if you are eager to test the upcoming digiKam<br>
<a href="http://www.msilaptops.info">msi laptops</a></p>
         </div>
    <div class="links">» </div>
  </div>

</div>
