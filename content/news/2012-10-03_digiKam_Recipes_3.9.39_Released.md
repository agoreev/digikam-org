---
date: "2012-10-03T08:27:00Z"
title: "digiKam Recipes 3.9.39 Released"
author: "Dmitri Popov"
description: "A new version of the digiKam Recipes ebook is available for your perusal. Besides the usual round of tweaks and fixes, the new version of"
category: "news"
aliases: "/node/667"

---

<p>A new version of the digiKam Recipes ebook is available for your perusal.&nbsp;Besides the usual round of tweaks and fixes, the new version of the&nbsp;<a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a>&nbsp;ebook includes the following new material:</p>
<ul>
<li>Geotag Photos with Android Camera and digiKam</li>
<li>Stitch Panoramas in digiKam</li>
</ul>
<p><img class="size-medium wp-image-2676" title="digikamrecipes-3935-fbreader" src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikamrecipes-3935-fbreader.png?w=281" alt="" width="281" height="500"></p>
<p><a href="http://scribblesandsnaps.com/2012/10/03/digikam-recipes-3-9-39-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>