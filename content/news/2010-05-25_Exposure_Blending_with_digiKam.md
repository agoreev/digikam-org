---
date: "2010-05-25T18:19:00Z"
title: "Exposure Blending with digiKam"
author: "Dmitri Popov"
description: "No matter how good your camera is, taking a well-exposed photo of a high-contrast scene like a black bird on snow can be really tricky."
category: "news"
aliases: "/node/517"

---

<p>No matter how good your camera is, taking a well-exposed photo of a high-contrast scene like a black bird on snow can be really tricky. Even if you switch to the manual mode and tweak the exposure settings, there is still a risk that you will end up with a shot containing under- or overexposed areas. One way to solve this problem is to use exposure blending. This technique involves taking several shots of the same scene or subject with different exposures and then fusing these shots into one perfectly exposed photo. <a href="http://scribblesandsnaps.wordpress.com/2010/05/25/exposure-blending-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19214"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/517#comment-19214" class="active">Next functions?</a></h3>    <div class="submitted">Submitted by Fri13 on Thu, 2010-05-27 08:07.</div>
    <div class="content">
     <p>Now we have exposure blending, next state would be getting a DOF (Depth of Field) bracketing and then panorama straight to the UI.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19215"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/517#comment-19215" class="active">DOF bracketing is already</a></h3>    <div class="submitted">Submitted by languitar on Sat, 2010-05-29 10:54.</div>
    <div class="content">
     <p>DOF bracketing is already possible as enfuse, which is used for the plugin, is also designed to support this. The idea is to set the contrast slider to maximum and the other two sliders to zero. With these setting enblend selects the areas with highest contrast from every source image. Highest contrast should also be a good approximation for in-focus areas.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>