---
date: "2010-10-11T12:00:00Z"
title: "kipi-plugins 1.5.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.5.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/540"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.5.0 !</p>

<a href="http://www.flickr.com/photos/digikam/5070798943/" title="kipiplugins1.5.0 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4133/5070798943_4fef6ab0a3.jpg" width="500" height="200" alt="kipiplugins1.5.0"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<h5>NEW FEATURES:</h5><br>

<b>General</b>                 : Fix compilation under windows with TDM-GCC and MSVC compilers.<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; FlickrExport    : 218169 : Less/More options buttons don't change label.<br>
002 ==&gt; DngConverter    : 249785 : DNG Converter adds 1 hour to timestamp of Sony .arw files.<br>
003 ==&gt; RedEyesRemoval  : 251391 : OpenCV not detected during configuration.<br>
004 ==&gt; JPEGLossLess    : 252362 : digiKam crash when launching.<br>
005 ==&gt; JPEGLossLess    : 224043 : Rotating certain images results in zero length file.<br>
006 ==&gt; PicasaWebExport : 252799 : After successful export of photo it should not be exported again.<br>
007 ==&gt; RAWConverter    : 238823 : Batch RAW Conversion should copy tags / captions / ratings to the converted file.<br>
008 ==&gt; RAWConverter    : 211558 : Raw converter: copy tags and rating.<br>
009 ==&gt; RAWConverter    : 199318 : RAW to JPG Export doesn't use Digikam description.<br>
010 ==&gt; BatchProcess    : 152208 : Comments lost when converting from png to jpeg.<br>
011 ==&gt; ImageViewer     : 231763 : Crash when closing opengl viewer.<br>

<div class="legacy-comments">

  <a id="comment-19591"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/540#comment-19591" class="active">I'd really like to know</a></h3>    <div class="submitted">Submitted by <a href="http://strider.zona-m.net" rel="nofollow">Marco</a> (not verified) on Tue, 2010-10-12 21:04.</div>
    <div class="content">
     <p>what mountain is shown in the snapshot. It's really interesting. Thanks</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
