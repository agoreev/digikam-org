---
date: "2006-12-18T12:06:00Z"
title: "It's a Masterpiece! digiKam & digikamImagePlugins 0.9.0 major release"
author: "gerhard"
description: "The digiKam team is very proud to release the final 0.9.0 sources as a Xmas present to you. digiKam is complemented by a corresponding digikamImagePlugin"
category: "news"
aliases: "/node/202"

---

<p>The digiKam team is very proud to release the final 0.9.0 sources as a Xmas present to you. digiKam is complemented by a corresponding digikamImagePlugin release and a kipi-plugins introducing enticing new features. Binaries from your Linux distribution will be available shortly.</p>
<p>digiKam has made big headway towards a professional image management tool that compares favorably to heavy weights from Adobe and Apple. <strong>16 bit Colormanagement workflow, Raw file support, full Metadata features including GPS data</strong>, to name only the most important ones. And it remains very user friendly!<br>
All digiKam 0.9.0 features can be seen <a href="http://www.digikam.org/?q=about/features09x">at this url</a></p>
<p>The 0.9 series has been under heavy development since November 2005. After having published several release candidates, this final version includes many bug fixes and the updated internationalization strings.</p>
<p>So, download, compile and enjoy it !<br>
The source for digiKam and digikamImagePlugins 0.9.0 is available <a href="http://sourceforge.net/project/showfiles.php?group_id=42641"> from SourceForge.net</a> </p>
<p>Bug fixes since rc2 are listed below:<br>
BUG: 134999     Workaround for problem in QLatin15Codec<br>
BUG: 138616     exiv2 dependency 0.12 detection<br>
BUG: 138715     fix a race condition in threaded image I/O witch can crash digiKam<br>
BUG: 138747     Force QComboBox to use internally a QListBox independently of<br>
current widget style used by QT<br>
BUG: 117248     fix hotplug digiKam script<br>
BUG: 138540     if file path is read-only, do not try to save metadata on pictures<br>
BUG: 135442     Added missing "Rename..." option on Album RMB menu<br>
BUG: 127112     fix kgamma to display module from KDE control center<br>
BUG: 121651     add Export menu into Album context pop-up menu<br>
BUG: 119205     never handle root album with Drag &amp; Drop, Copy &amp; Paste operations<br>
-                Color Management plugin BugFix : if input|proof|workspace ICC profile files path are wrong (files do not exist or file not readable, or path is not a file), ask to user to check settings before to perform color transformations...else digiKam will crash with an error from LCMS library<br>
BUG: 138620     Call prompt UserSave _before_ changing variables<br>
BUG: 133091     using SHIFT instead of 7 8 9 with Flip/Rotate keyboard shortcuts using KDE_PKG_CHECK_MODULES instead PKG_CHECK_MODULES<br>
BUG: 137993     modification date in digikamalbums::put<br>
BUG: 137770     Don't touch read-only file when metadata need to be updated.<br>
BUG: 138253     add keybd shortcut to toggle on/off Monitor Color Corrections F12<br>
-                Improvements of icons to gain space</p>

<div class="legacy-comments">

</div>