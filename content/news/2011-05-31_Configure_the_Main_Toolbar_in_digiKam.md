---
date: "2011-05-31T08:31:00Z"
title: "Configure the Main Toolbar in digiKam"
author: "Dmitri Popov"
description: "The main toolbar in digiKam provides quick access to several tools and features, and you can tweak it to fit your particular photographic workflow. To"
category: "news"
aliases: "/node/605"

---

<p>The main toolbar in digiKam provides quick access to several tools and features, and you can tweak it to fit your particular photographic workflow. To do this, choose the&nbsp;Settings »&nbsp;Configure Toolbars command. This opens the Configure Toolbars dialog window where you can add, remove, and tweak toolbar buttons. <a href="http://scribblesandsnaps.wordpress.com/2011/05/31/configure-the-main-toolbar-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>