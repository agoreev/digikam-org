---
date: "2011-09-14T20:54:00Z"
title: "digiKam Software Collection 2.1.1 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! In order to fix this issue from KDE bugzilla, digiKam 2.1.1 have been just released. digiKam software collection tarball"
category: "news"
aliases: "/node/624"

---

<a href="http://www.flickr.com/photos/digikam/6094450872/" title="digiKam-sonyA500-panorama3 by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6205/6094450872_83c945d6ba_z.jpg" width="640" height="360" alt="digiKam-sonyA500-panorama3"></a>

<p>Dear all digiKam fans and users!</p>

<p>In order to fix <a href="https://bugs.kde.org/show_bug.cgi?id=281767">this issue</a> from KDE bugzilla, digiKam 2.1.1 have been just released.</p>


<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20023"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20023" class="active">I know panorama tool is based</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-09-15 12:07.</div>
    <div class="content">
     <p>I know panorama tool is based on Hugin. Why should I prefer the panorama tool to hugin? Is there some improvement/better picture composition?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20024"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20024" class="active">Simplicity...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2011-09-15 12:26.</div>
    <div class="content">
     <p>The tool provided by digiKam team is more simple.</p>

<p>Just select images to assemble, and follow stages of assistant. All is automatized. There is only few options to tune. At end if result is not perfect, you can load project to hugin. else, the target image is ready to use in your collection.</p>

<p>All tool stages are described in <a href="http://www.flickr.com/photos/digikam/6060217962/sizes/o/in/photostream/">this screenshot</a></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20025"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20025" class="active">Thanks for your</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-09-15 20:16.</div>
    <div class="content">
     <p>Thanks for your explaination.<br>
R</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20028"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20028" class="active">X3F-support has gone again</a></h3>    <div class="submitted">Submitted by Michael (not verified) on Fri, 2011-09-16 17:35.</div>
    <div class="content">
     <p>Hi,</p>
<p>while in the digikam-2.0-beta-versions the support for Sigma-RAW-Pictures (X3F) found it's way back to digikam i was hopeful, that it will be so for the future versions of digikam. But this was not right. In the newer versions of digikam (2.1) the support for these pictures is gone again. So Users of the Sigma SD9, 10, 14, 15 and 1 cannot use digikam.</p>
<p>I'm very surprised about this fact. In the early versions of digikam (1.x) the X3F-support was included, in the later versions it was gone, then in the 2.0-betas the support was back again and now it is gone again. Terrible.</p>
<p>Bye<br>
Michael<br>
from Bavaria</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20031"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20031" class="active">Configuring incomplete, errors occurred!</a></h3>    <div class="submitted">Submitted by vince (not verified) on Mon, 2011-09-19 21:04.</div>
    <div class="content">
     <p>I don't understand why i can't compile while everything seems to be ok<br>
exept optional</p>
<p><code>CMake Error at CMakeLists.txt:28 (cmake_policy):<br>
  Policy "CMP0017" is not known to this version of CMake.</code></p>
<p>-- The C compiler identification is GNU<br>
-- The CXX compiler identification is GNU<br>
-- Check for working C compiler: /usr/bin/gcc<br>
-- Check for working C compiler: /usr/bin/gcc -- works<br>
-- Detecting C compiler ABI info<br>
-- Detecting C compiler ABI info - done<br>
-- Check for working CXX compiler: /usr/bin/c++<br>
-- Check for working CXX compiler: /usr/bin/c++ -- works<br>
-- Detecting CXX compiler ABI info<br>
-- Detecting CXX compiler ABI info - done<br>
-- Looking for Q_WS_X11<br>
-- Looking for Q_WS_X11 - found<br>
-- Looking for Q_WS_WIN<br>
-- Looking for Q_WS_WIN - not found.<br>
-- Looking for Q_WS_QWS<br>
-- Looking for Q_WS_QWS - not found.<br>
-- Looking for Q_WS_MAC<br>
-- Looking for Q_WS_MAC - not found.<br>
-- Found Qt-Version 4.7.2 (using /usr/bin/qmake)<br>
-- Looking for XOpenDisplay in /usr/lib/x86_64-linux-gnu/libX11.so;/usr/lib/x86_64-linux-gnu/libXext.so;/usr/lib/x86_64-linux-gnu/libXft.so;/usr/lib/x86_64-linux-gnu/libXau.so;/usr/lib/x86_64-linux-gnu/libXdmcp.so<br>
-- Looking for XOpenDisplay in /usr/lib/x86_64-linux-gnu/libX11.so;/usr/lib/x86_64-linux-gnu/libXext.so;/usr/lib/x86_64-linux-gnu/libXft.so;/usr/lib/x86_64-linux-gnu/libXau.so;/usr/lib/x86_64-linux-gnu/libXdmcp.so - found<br>
-- Looking for gethostbyname<br>
-- Looking for gethostbyname - found<br>
-- Looking for connect<br>
-- Looking for connect - found<br>
-- Looking for remove<br>
-- Looking for remove - found<br>
-- Looking for shmat<br>
-- Looking for shmat - found<br>
-- Looking for IceConnectionNumber in ICE<br>
-- Looking for IceConnectionNumber in ICE - found<br>
-- Found X11: /usr/lib/x86_64-linux-gnu/libX11.so<br>
-- Looking for include files CMAKE_HAVE_PTHREAD_H<br>
-- Looking for include files CMAKE_HAVE_PTHREAD_H - found<br>
-- Looking for pthread_create in pthreads<br>
-- Looking for pthread_create in pthreads - not found<br>
-- Looking for pthread_create in pthread<br>
-- Looking for pthread_create in pthread - found<br>
-- Found Threads: TRUE<br>
-- Looking for _POSIX_TIMERS<br>
-- Looking for _POSIX_TIMERS - found<br>
-- Found Automoc4: /usr/bin/automoc4<br>
-- Found Perl: /usr/bin/perl<br>
-- Found Phonon: /usr/include<br>
-- Performing Test _OFFT_IS_64BIT<br>
-- Performing Test _OFFT_IS_64BIT - Success<br>
-- Performing Test HAVE_FPIE_SUPPORT<br>
-- Performing Test HAVE_FPIE_SUPPORT - Success<br>
-- Performing Test __KDE_HAVE_W_OVERLOADED_VIRTUAL<br>
-- Performing Test __KDE_HAVE_W_OVERLOADED_VIRTUAL - Success<br>
-- Performing Test __KDE_HAVE_GCC_VISIBILITY<br>
-- Performing Test __KDE_HAVE_GCC_VISIBILITY - Success<br>
-- Found KDE 4.6 include dir: /usr/include<br>
-- Found KDE 4.6 library dir: /usr/lib<br>
-- Found the KDE4 kconfig_compiler preprocessor: /usr/bin/kconfig_compiler<br>
-- Found automoc4: /usr/bin/automoc4<br>
-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libksane<br>
-- Found Sane: /usr/lib/libsane.so </p>
<p>-----------------------------------------------------------------------------<br>
-- The following external packages were located on your system.<br>
-- This installation will have the extra features provided by these packages.<br>
-----------------------------------------------------------------------------<br>
   * SANE development toolkit - Scanner Access Now Easy (SANE) development package</p>
<p>-----------------------------------------------------------------------------<br>
-- Congratulations! All external packages have been found.<br>
-----------------------------------------------------------------------------</p>
<p>-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libkipi<br>
-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libkexiv2<br>
-- Found Exiv2: /usr/lib/libexiv2.so  (found version "0.21.1", required is "0.21")</p>
<p>-----------------------------------------------------------------------------<br>
-- The following external packages were located on your system.<br>
-- This installation will have the extra features provided by these packages.<br>
-----------------------------------------------------------------------------<br>
   * Exiv2 - Required to build libkexiv2.</p>
<p>-----------------------------------------------------------------------------<br>
-- Congratulations! All external packages have been found.<br>
-----------------------------------------------------------------------------</p>
<p>-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libkdcraw<br>
-- checking for module 'lcms'<br>
--   found lcms, version 1.18<br>
-- Found LCMS: /usr/lib/liblcms.so<br>
-- Found JPEG: /usr/lib/x86_64-linux-gnu/libjpeg.so<br>
-- Try OpenMP C flag = [-fopenmp]<br>
-- Performing Test OpenMP_FLAG_DETECTED<br>
-- Performing Test OpenMP_FLAG_DETECTED - Success<br>
-- Try OpenMP CXX flag = [-fopenmp]<br>
-- Performing Test OpenMP_FLAG_DETECTED<br>
-- Performing Test OpenMP_FLAG_DETECTED - Success<br>
-- Found OpenMP: -fopenmp </p>
<p>-----------------------------------------------------------------------------<br>
-- The following external packages were located on your system.<br>
-- This installation will have the extra features provided by these packages.<br>
-----------------------------------------------------------------------------<br>
   * lcms - A small-footprint color management engine<br>
   * libjpeg - JPEG image format support</p>
<p>-----------------------------------------------------------------------------<br>
-- Congratulations! All external packages have been found.<br>
-----------------------------------------------------------------------------</p>
<p>-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libmediawiki<br>
-- checking for module 'QJson&gt;=0.5'<br>
--   found QJson, version 0.7.1<br>
-- Found QJSON: qjson;QtCore </p>
<p>-----------------------------------------------------------------------------<br>
-- The following external packages were located on your system.<br>
-- This installation will have the extra features provided by these packages.<br>
-----------------------------------------------------------------------------<br>
   * QJSON - Qt library for handling JSON data</p>
<p>-----------------------------------------------------------------------------<br>
-- Congratulations! All external packages have been found.<br>
-----------------------------------------------------------------------------</p>
<p>-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libkgeomap<br>
-- Found Qt-Version 4.7.2 (using /usr/bin/qmake)<br>
-- Found X11: /usr/lib/x86_64-linux-gnu/libX11.so<br>
-- Found marble: /usr/include/marble<br>
-- Check Kexiv2 library in local sub-folder...<br>
-- Found Kexiv2 library in local sub-folder: /opt/digikam-2.1.1/extra/libkexiv2<br>
-- kexiv2 found, the demo application will be compiled.<br>
-- Looking for valgrind/valgrind.h<br>
-- Looking for valgrind/valgrind.h - not found<br>
-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: libkface<br>
-- Found Qt-Version 4.7.2 (using /usr/bin/qmake)<br>
-- Found X11: /usr/lib/x86_64-linux-gnu/libX11.so<br>
-- First try at finding OpenCV...<br>
-- Great, found OpenCV on the first try.<br>
-- OpenCV Root directory is /usr/share/opencv<br>
-- External libface was not found, use internal version instead...<br>
-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: kipi-plugins<br>
-- Check Kexiv2 library in local sub-folder...<br>
-- Found Kexiv2 library in local sub-folder: /opt/digikam-2.1.1/extra/libkexiv2<br>
-- Check for Kdcraw library in local sub-folder...<br>
-- Found Kdcraw library in local sub-folder: /opt/digikam-2.1.1/extra/libkdcraw<br>
-- Check Kipi library in local sub-folder...<br>
-- Found Kipi library in local sub-folder: /opt/digikam-2.1.1/extra/libkipi<br>
-- Found ZLIB: /usr/include (found version "1.2.3.4")<br>
-- Found PNG: /usr/lib/x86_64-linux-gnu/libpng.so<br>
-- Found TIFF: /usr/lib/x86_64-linux-gnu/libtiff.so<br>
-- libkdcraw: Found version 2.0.0 (required: 1.1.0)<br>
-- Found EXPAT: /usr/lib/x86_64-linux-gnu/libexpat.so<br>
-- Found LibXml2: /usr/lib/libxml2.so<br>
-- Found LibXslt: /usr/lib/libxslt.so<br>
-- checking for one of the modules 'libgpod-1.0'<br>
-- Found libgpod-1 0.8.0<br>
-- libgpod dir: /usr/include/glib-2.0;/usr/lib/x86_64-linux-gnu/glib-2.0/include;/usr/include/gpod-1.0<br>
-- libgpod lib: gpod;gobject-2.0;gthread-2.0;rt;glib-2.0<br>
-- libgpod def: -pthread;-I/usr/include/glib-2.0;-I/usr/lib/x86_64-linux-gnu/glib-2.0/include;-I/usr/include/gpod-1.0<br>
-- libgpod ver: 0.8.0<br>
-- checking for module 'gdk-pixbuf-2.0'<br>
--   found gdk-pixbuf-2.0, version 2.23.3<br>
-- Found Gdk: /usr/include/gdk-pixbuf-2.0<br>
-- Found GLIB2: /usr/lib/x86_64-linux-gnu/libglib-2.0.so<br>
-- checking for module 'gobject-2.0'<br>
--   found gobject-2.0, version 2.28.6<br>
-- Found GObject libraries: /usr/lib/x86_64-linux-gnu/libgobject-2.0.so;/usr/lib/x86_64-linux-gnu/libgmodule-2.0.so;/usr/lib/x86_64-linux-gnu/libgthread-2.0.so;/usr/lib/x86_64-linux-gnu/libglib-2.0.so<br>
-- Found GObject includes : /usr/include/glib-2.0/gobject<br>
-- Found KdepimLibs: /usr/lib/cmake/KdepimLibs/KdepimLibsConfig.cmake<br>
-- Found QCA2: /usr/lib/libqca.so<br>
-- Check for Ksane library in local sub-folder...<br>
-- Found Ksane library in local sub-folder: /opt/digikam-2.1.1/extra/libksane<br>
-- Check for KGeoMap library in local sub-folder...<br>
-- Found KGeoMap library in local sub-folder: /opt/digikam-2.1.1/extra/libkgeomap<br>
-- Check Mediawiki library in local sub-folder...<br>
-- Found Mediawiki library in local sub-folder: /opt/digikam-2.1.1/extra/libmediawiki<br>
-- Check Vkontakte library in local sub-folder...<br>
-- Found Vkontakte library in local sub-folder: /opt/digikam-2.1.1/extra/libkvkontakte<br>
-- libkgeomap: Found version 2.0.0<br>
-- Found X11: /usr/lib/x86_64-linux-gnu/libX11.so<br>
-- CMake version: cmake version 2.8.3</p>
<p>-- CMake version (cleaned): cmake version 2.8.3</p>
<p>--<br>
-- ----------------------------------------------------------------------------------<br>
--  kipi-plugins 2.1.1 dependencies results<br>
--<br>
--  libjpeg library found.................... YES<br>
--  libtiff library found.................... YES<br>
--  libpng library found..................... YES<br>
--  libkipi library found.................... YES<br>
--  libkexiv2 library found.................. YES<br>
--  libkdcraw library found.................. YES<br>
--  libxml2 library found.................... YES (optional)<br>
--  libxslt library found.................... YES (optional)<br>
--  libexpat library found................... YES (optional)<br>
--  native threads support library found..... YES (optional)<br>
--  libopengl library found.................. YES (optional)<br>
--  Qt4 OpenGL module found.................. YES<br>
--  libopencv library found.................. YES (optional)<br>
--  QJson library found...................... YES (optional)<br>
--  libgpod library found.................... YES (optional)<br>
--  Gdk library found........................ YES (optional)<br>
--  libkdepim library found.................. YES (optional)<br>
--  qca2 library found....................... YES (optional)<br>
--  libkgeomap library found................. YES (optional)<br>
--  libmediawiki library found............... YES (optional)<br>
--  libkvkontakte library found.............. YES (optional)<br>
--  OpenMP library found..................... YES (optional)<br>
--  libX11 library found..................... YES (optional)<br>
--  libksane library found................... YES (optional)<br>
--<br>
--  kipi-plugins will be compiled............ YES<br>
--  Shwup will be compiled................... YES (optional)<br>
--  YandexFotki will be compiled............. YES (optional)<br>
--  HtmlExport will be compiled.............. YES (optional)<br>
--  AdvancedSlideshow will be compiled....... YES (optional)<br>
--  ImageViewer will be compiled............. YES (optional)<br>
--  AcquireImages will be compiled........... YES (optional)<br>
--  DNGConverter will be compiled............ YES (optional)<br>
--  RemoveRedEyes will be compiled........... YES (optional)<br>
--  Debian Screenshots will be compiled...... YES (optional)<br>
--  Facebook will be compiled................ YES (optional)<br>
--  VKontakte will be compiled............... YES (optional)<br>
--  IpodExport will be compiled.............. YES (optional)<br>
--  Calendar will be compiled................ YES (optional)<br>
--  GPSSync will be compiled................. YES (optional)<br>
--  Mediawiki will be compiled............... YES (optional)<br>
-- ----------------------------------------------------------------------------------<br>
--<br>
-- Check if the system is big endian<br>
-- Searching 16 bit integer<br>
-- Looking for sys/types.h<br>
-- Looking for sys/types.h - found<br>
-- Looking for stdint.h<br>
-- Looking for stdint.h - found<br>
-- Looking for stddef.h<br>
-- Looking for stddef.h - found<br>
-- Check size of unsigned short<br>
-- Check size of unsigned short - done<br>
-- Using unsigned short<br>
-- Check if the system is big endian - little endian<br>
-- ----------------------------------------------------------------------------------<br>
-- Starting CMake configuration for: digikam<br>
-- Found Jasper: /usr/lib/x86_64-linux-gnu/libjasper.so<br>
-- Check for Kdcraw library in local sub-folder...<br>
-- Found Kdcraw library in local sub-folder: /opt/digikam-2.1.1/extra/libkdcraw<br>
-- Check Kexiv2 library in local sub-folder...<br>
-- Found Kexiv2 library in local sub-folder: /opt/digikam-2.1.1/extra/libkexiv2<br>
-- Check Kipi library in local sub-folder...<br>
-- Found Kipi library in local sub-folder: /opt/digikam-2.1.1/extra/libkipi<br>
-- Check Kface library in local sub-folder...<br>
-- Found Kface library in local sub-folder: /opt/digikam-2.1.1/extra/libkface<br>
-- Check for KGeoMap library in local sub-folder...<br>
-- Found KGeoMap library in local sub-folder: /opt/digikam-2.1.1/extra/libkgeomap<br>
-- checking for module 'libpgf'<br>
--   package 'libpgf' not found<br>
-- Could NOT find any working clapack installation<br>
-- Boost version: 1.42.0<br>
-- Identified libjpeg version: 62<br>
-- Found MySQL server executable at: /usr/sbin/mysqld<br>
-- Found MySQL install_db executable at: /usr/bin/mysql_install_db<br>
-- libkdcraw: Found version 2.0.0 (required: 2.0.0)<br>
-- libkgeomap: Found version 2.0.0<br>
-- Found gphoto2: -L/usr/lib -lgphoto2_port;-L/usr/lib -lgphoto2 -lgphoto2_port -lm<br>
-- Found LIBUSB: /usr/lib/libusb.so<br>
-- WARNING: you are using the obsolete 'PKGCONFIG' macro use FindPkgConfig<br>
-- Found LensFun: /usr/include<br>
-- WARNING: you are using the obsolete 'PKGCONFIG' macro use FindPkgConfig<br>
-- Performing Test HAVE_LQR_0_4<br>
-- Performing Test HAVE_LQR_0_4 - Success<br>
-- Found Lqr-1: /usr/include/lqr-1<br>
-- Found Soprano: /usr/include<br>
-- Found SharedDesktopOntologies: /usr/share/ontology<br>
-- Found Nepomuk: /usr/lib/libnepomuk.so<br>
-- Found SharedDesktopOntologies: /usr/share/ontology  (found version "0.5.0", required is "0.2")<br>
-- Found Doxygen: /usr/bin/doxygen<br>
--<br>
-- ----------------------------------------------------------------------------------<br>
--  digiKam 2.1.1 dependencies results<br>
--<br>
--  Qt4 SQL module found..................... YES<br>
--  MySQL Server found....................... YES<br>
--  MySQL install_db tool found.............. YES<br>
--  libtiff library found.................... YES<br>
--  libpng library found..................... YES<br>
--  libjasper library found.................. YES<br>
--  liblcms library found.................... YES<br>
--  Boost Graph library found................ YES<br>
--  libkipi library found.................... YES<br>
--  libkexiv2 library found.................. YES<br>
--  libkdcraw library found.................. YES<br>
--  libkface library found................... YES<br>
--  libkgeomap library found................. YES<br>
--  libpgf library found..................... NO  (optional - internal version used instead)<br>
--  libclapack library found................. NO  (optional - internal version used instead)<br>
--  libgphoto2 and libusb libraries found.... YES (optional)<br>
--  libkdepimlibs library found.............. YES (optional)<br>
--  Nepomuk libraries found.................. YES (optional)<br>
--  libglib2 library found................... YES (optional)<br>
--  liblqr-1 library found................... YES (optional)<br>
--  liblensfun library found................. YES (optional)<br>
--  Doxygen found............................ YES (optional)<br>
--  digiKam can be compiled.................. YES<br>
-- ----------------------------------------------------------------------------------<br>
--<br>
-- Adjusting compilation flags for GCC version ( 4.5.2 )<br>
-- Looking for dgettext<br>
-- Looking for dgettext - found<br>
-- Found Gettext: built in libc<br>
-- Configuring incomplete, errors occurred!<br>
</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20032"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20032" class="active">Your version of cmake (too</a></h3>    <div class="submitted">Submitted by Philip Johnsson (not verified) on Tue, 2011-09-20 15:51.</div>
    <div class="content">
     <p>Your version of cmake (too old) doesn't support Policy "CMP0017" as stated in almost the first line.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20033"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20033" class="active">compilation ok</a></h3>    <div class="submitted">Submitted by vince (not verified) on Tue, 2011-09-20 18:01.</div>
    <div class="content">
     <p>yes i ended up noticing I installed a newer version<br>
I'm on ubuntu and for a newer version of cmake i use ppa:pgquiles/ppa<br>
note that i had tio install these packages  :<br>
libhighgui-dev libcvaux-dev</p>
<p>it's ok it's work</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20034"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/624#comment-20034" class="active">Remember Last Saved directory</a></h3>    <div class="submitted">Submitted by <a href="http://temporaryland.wordpress.com/" rel="nofollow">rm42</a> (not verified) on Wed, 2011-09-28 16:31.</div>
    <div class="content">
     <p>I am sorry if this is not the best place to ask this question.  I use digiKam for processing my RAW images.  I like to have my RAW files on a separate directory tree from my finished images.  So, the way I work is like this:</p>
<p>1.- In digiKam navigate to the directory where my RAW images reside.<br>
2.- Process<br>
3.- Save the file into my finished images directory tree.</p>
<p>I can't seem to find an option for making digiKam remember the last directory I saved to.  So, every time I save a new image I have to click, click, click... to get to the right directory.  Is there a setting I could change in order to avoid all that clicking?  Could there be?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
