---
date: "2011-02-22T09:32:00Z"
title: "Work with Photo Metadata in digiKam"
author: "Dmitri Popov"
description: "Inside each digital photo hides metadata in the EXIF, IPTC, or XMP formats, and digiKam provides tools for viewing and editing this useful information. For"
category: "news"
aliases: "/node/577"

---

<p>Inside each digital photo hides metadata in the EXIF, IPTC, or XMP formats, and digiKam provides tools for viewing and editing this useful information.</p>
<p>For starters, digiKam’s main window features the dedicated Metadata sidebar which lets you view EXIF, Makernote, IPTC, and XMP metadata. You can switch between concise and full views as well as print the metadata, save them as a file, and copy them into the clipboard. <a href="http://scribblesandsnaps.wordpress.com/2011/02/22/work-with-photo-metadata-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>