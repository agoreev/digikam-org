---
date: "2011-04-04T07:53:00Z"
title: "digiKam Tricks 3.0 Released"
author: "Dmitri Popov"
description: "This is a major release of the digiKam Tricks book, featuring a completely revised and updated content that reflects changes in the upcoming version 2.0"
category: "news"
aliases: "/node/592"

---

<p>This is a major release of the <a href="http://scribblesandsnaps.wordpress.com/digikam-tricks-book/">digiKam Tricks</a> book, featuring a completely revised and updated content that reflects changes in the upcoming version 2.0 of the digiKam photo management application. In addition to that, the book includes the following new material:</p>
<ul>
<li>Batch Process Photos in&nbsp;digiKam</li>
<li>Manage Photos from Multiple digiKam&nbsp;Installations</li>
<li>Use Color Labels and Picks <em>(digiKam 2.0)</em></li>
<li>Tag Faces with the Face Recognition Feature <em>(digiKam 2.0)</em></li>
<li>Use Versioning for Non-destructive Editing <em>(digiKam 2.0)</em></li>
<li>APPENDIX A: Set up Photographic Workflow with digiKam</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/04/04/digikam-tricks-3-0-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>