---
date: "2006-04-27T02:01:00Z"
title: "digikam 0.8.2-beta1 released"
author: "Anonymous"
description: "We are pleased to announce digiKam 0.8.2 beta1. New features: o display JPEG that use CMYK color space o Rating image using keyboard shortcuts CTRL+0/1/2/3/4/5"
category: "news"
aliases: "/node/117"

---

<p>We are pleased to announce digiKam 0.8.2 beta1.</p>
<p><strong>New features:</strong></p>
<p>        o display JPEG that use CMYK color space</p>
<p>        o Rating image using keyboard shortcuts CTRL+0/1/2/3/4/5 from<br>
          main interface (#123646)</p>
<p>        o Buttons to set the album date to oldest, yougest of the<br>
          images in the album (#120963)</p>
<p><strong>Bugs fixed:</strong></p>
<p>        o #123742: preview-pictures seem to be handled differently by<br>
          Digikam and Konqueror</p>
<p>        o #121905: albums not displayed corectly and digikam crashes<br>
          with St9bad_alloc</p>
<p>        o #120053: digikam: whatthis info not closed when albumview is<br>
          scrolled up or down with cursur keys</p>
<p>        o #120052: digikam: redraw problem with whatthis info of an<br>
          image show and starts scrolling with the mouse</p>
<p>        o #119946: thumbnails not correctly rotated according to exif<br>
          information</p>
<p>        o #116248: Ask user which plugins should be enabled on first<br>
          startup (all plugins are enabled by default first startup)</p>
<p>        o #115460: opening and closing right pane with tag filter also<br>
          changes width of left pane and vv</p>
<p>        o #121646: Digikam on PPC has problem identifying JPEG and tries<br>
          to use dcraw with them</p>
<p>        o #120479: Search problem for not tagged or commented images<br>
          #120775: Search doesn't find pictures without rating</p>
<p><strong>Source download:</strong><br>
       http://sourceforge.net/project/showfiles.php?group_id=42641</p>

<div class="legacy-comments">

</div>