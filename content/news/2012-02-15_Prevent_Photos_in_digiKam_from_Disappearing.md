---
date: "2012-02-15T11:02:00Z"
title: "Prevent Photos in digiKam from Disappearing"
author: "Dmitri Popov"
description: "Versioning in digiKam provides an excellent mechanism for non-destructive editing, but it does have a tiny quirk that can be a bit confusing if you"
category: "news"
aliases: "/node/644"

---

<p>Versioning in digiKam provides an excellent mechanism for non-destructive editing, but it does have a tiny quirk that can be a bit confusing if you are not aware of it.</p>
<p><a href="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_snapshots.png"><img class="alignnone size-medium wp-image-2248" title="digikam_snapshots" src="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_snapshots.png?w=500" alt="" width="500" height="403"></a></p>
<p>With the Versioning feature enabled, digiKam automatically displays only the most recent version of a photo and hides all the previous revisions, including the original file. This functionality helps to avoid clutter in the main thumbnail view, but this creature comfort can also cause panic when you all of a sudden can’t find the original photos. <a href="http://scribblesandsnaps.wordpress.com/2012/02/15/prevent-photos-in-digikam-from-disappearing/">Continue to read</a></p>

<div class="legacy-comments">

</div>