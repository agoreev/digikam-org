---
date: "2012-03-12T10:03:00Z"
title: "Tethered Shooting with digiKam"
author: "Dmitri Popov"
description: "While digiKam won't rival dedicated software for tethered shooting, you can use the application's Import interface to trigger the connected camera and instantly fetch photos"
category: "news"
aliases: "/node/648"

---

<p>While digiKam won't rival dedicated software for tethered shooting, you can use the application's <strong>Import</strong> interface to&nbsp;trigger the connected camera and instantly fetch photos from it. This functionality can come in handy when you want to have an instant preview of photos you take on a large screen.</p>
<p><a href="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_capture.png"><img class="alignnone size-medium wp-image-2317" title="digikam_capture" src="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_capture.png?w=500" alt="" width="500" height="399"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/03/12/tethered-shooting-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>