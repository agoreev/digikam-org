---
date: "2006-11-21T22:36:00Z"
title: "digikam and DigikamImagePlugins 0.9.0 Release Candidate 1 published!"
author: "digiKam"
description: "The digiKam team is proud to announce the first release candidate of digiKam 0.9.0 and DigikamImagePlugins 0.9.0. The 0.9 series has been under heavy development"
category: "news"
aliases: "/node/189"

---

<p>The digiKam team is proud to announce the first release candidate of digiKam 0.9.0 and DigikamImagePlugins 0.9.0.</p>
<p>The 0.9 series has been under heavy development since November 2005 (more than <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/NEWS?rev=606694&amp;view=auto">200 files in KDE bugzilla</a> have been closed) and offers you the opportunity to test and enjoy a variety of new features, additions and improvements.</p>
<p>The source for digiKam and DigikamImagePlugins 0.9.0-rc1 is available <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">at this url</a></p>
<p>All digiKam 0.9-rc1 features can be seen <a href="http://www.digikam.org/?q=about/features09x">at this url</a></p>
<p>New features and bug fixes since beta3 are listed below:</p>
<ul>
<li>103255 : Add (not edit) EXIF headers like date, comment etc.</li>
<li>91812  : Viewer/editor for IPTC meta data.</li>
<li>136138 : set as album thumbnail doesn't change the icon immediately.</li>
<li>135851 : Wish to view IPTC in different order.</li>
<li>136162 : ISO Slider label is incorrectly labeled as "sensibility", should be sensitivity.</li>
<li>133026 : Crashes on systems using hyperthreading.</li>
<li>136260 : Awkward management of metadata and digikam-tags and comments.</li>
<li>36769  : digiKam crashes when resetting Album icon (with no album selected).</li>
<li>136749 : Tags are not kept with images when album is moved in album hierarchy.</li>
<li>123623 : digiKam freezes or is very slow.</li>
<li>96993  : Ability to view next next image in folder in showfoto.</li>
<li>136643 : showfoto can open CRW, but not shown in file dialogue.</li>
<li>137063 : Keyboard shortcut for 'paste' action not working.</li>
<li>137461 : Typo croping must be cropping.</li>
<li>137282 : Comments are lost when copying or moving a picture to another album.</li>
<li>132805 : Crash when assinging keywords.</li>
<li>137204 : Crash when applying IPTC data.</li>
<li>135407 : Reproducible crash selecting photos from collection.</li>
<li>136086 : Crash when saving any type of files.</li>
<li>137612 : Showfoto doesn't refresh the photo preview when deleting a picture.</li>
<li>Internal dcraw implementation updated to version 8.41.</li>
</ul>

<div class="legacy-comments">

</div>