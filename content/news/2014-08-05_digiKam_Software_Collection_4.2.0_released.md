---
date: "2014-08-05T06:20:00Z"
title: "digiKam Software Collection 4.2.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.2.0. This release includes important features to"
category: "news"
aliases: "/node/715"

---

<a href="https://www.flickr.com/photos/digikam/14822555991"><img src="https://farm4.staticflickr.com/3889/14822555991_7672b71a96_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.2.0. This release includes important features to simplify photograph workflow :
</p>

<ul>

<li>
New view on left sidebar to <a href="http://mohamedanwer.wordpress.com/2014/07/17/the-great-merging-day/">search quickly items with assigned Labels</a>.
</li>

<li>
On tags tree-view a new option <a href="http://mohamedanwer.wordpress.com/2014/07/11/tags-left-sidebar-supports-new-action-now/">to show items with no tag</a>.
</li>

</ul>

<p>These features have been introduced by <a href="https://community.kde.org/Digikam/GSoC2014#Add_a_quick_access_to_Labels_in_dedicated_tree-view">Mohamed Anwer Google Summer of Code project</a> which have been completed in time and judged ready for production.</p>

<p>
As usual, we have worked hard to close your reported issues since the <a href="http://www.digikam.org/node/714">previous stable release 4.1.0</a>. 
A list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.2.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.2.0 is available through the KDE Bugtracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.2.0.tar.bz2.mirrorlist">KDE repository</a>.
</p>

<p>The updated (2014-08-08) Windows installer can now be downloaded from the <a href="http://download.kde.org/stable/digikam/digiKam-installer-4.2.0-win32-2.exe.mirrorlist">KDE repository</a> as well.<br>You only need the updated version if you previously got an error saying that <i>cudart32_55.dll</i> was missing when you tried to run digiKam. This update fixes that problem.</p>

<a href="https://www.flickr.com/photos/digikam/14402474860"><img src="https://farm4.staticflickr.com/3907/14402474860_57d88e073b_c.jpg" width="800" height="453"></a>


<p>Have fun playing with your photos using this new release,</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20788"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20788" class="active">compile error</a></h3>    <div class="submitted">Submitted by <a href="http://www.cuestrin.de" rel="nofollow">Andy Steinhauf</a> (not verified) on Tue, 2014-08-05 09:44.</div>
    <div class="content">
     <p>Hello,</p>
<p>make fails with this error message:</p>
<p>In file included from /home/***/digikam-4.2.0/extra/kipi-plugins/gpssync/track_correlator_thread.h:36:0,<br>
                 from /home/***/digikam-4.2.0/extra/kipi-plugins/gpssync/moc_track_correlator_thread.cpp:10,<br>
                 from /home/***/digikam-4.2.0/extra/kipi-plugins/gpssync/kipiplugin_gpssync_automoc.cpp:4:<br>
/home/***/digikam-4.2.0/extra/kipi-plugins/gpssync/track_correlator.h:32:31: Schwerwiegender Fehler: libkgeomap/tracks.h: Datei oder Verzeichnis nicht gefunden</p>
<p>best regards</p>
<p>Andy</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20789"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20789" class="active">libkgeomap</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-08-05 10:45.</div>
    <div class="content">
     <p>you must use libkgeomap from digiKam tarball to compile. You use previous version from your system.<br>
To force compilation with local one, use this cmake argument : -DDIGIKAMSC_USE_PRIVATE_KDEGRAPHICS=ON</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20796"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20796" class="active">libkgeomap</a></h3>    <div class="submitted">Submitted by <a href="http://www.cuestrin.de" rel="nofollow">Andy Steinhauf</a> (not verified) on Thu, 2014-08-07 12:09.</div>
    <div class="content">
     <p>Hello,</p>
<p>thanks for yoru quick response. I tried it, but the result is the same as above...</p>
<p>best regards</p>
<p>Andy</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20797"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20797" class="active">cleanup cmake cache.</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2014-08-07 12:45.</div>
    <div class="content">
     <p>Before to reconfigure cmake with new option, clean up cmake cache, or delete cmake buil dir...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20798"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20798" class="active">Hello,
I've deleted the whole</a></h3>    <div class="submitted">Submitted by <a href="http://www.cuestrin.de" rel="nofollow">Andy Steinhauf</a> (not verified) on Thu, 2014-08-07 13:36.</div>
    <div class="content">
     <p>Hello,</p>
<p>I've deleted the whole source dir and begun from scratch.</p>
<p>best regards</p>
<p>Andy</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20843"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20843" class="active">Not sure if it's still</a></h3>    <div class="submitted">Submitted by Pennes (not verified) on Wed, 2014-09-03 13:30.</div>
    <div class="content">
     <p>Not sure if it's still relevant to you, but if anyone else get the error, you can just remove your current version of libkgeomap with apt-get.</p>
<p>Then it will compile and install correctly. ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20845"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20845" class="active">Fix for building on OS X</a></h3>    <div class="submitted">Submitted by Vidar (not verified) on Wed, 2014-09-10 19:25.</div>
    <div class="content">
     <p>I had the same error when building on OS X (macports).<br>
I found that FindKGeoMap.cmake looks for libkgeomap/version.h.cmake, but in that folder I found libkgeomap/version.h.cmake.in instead. So I changed FindKGeoMap.cmake (in 3 places) to look for FindKGeoMap.cmake.in instead, and I got past the error.<br>
My solution may be terribly wrong, but it seems to have worked. I have not tried the geomapping functionality of digiKam yet.<br>
PS: I found this when building latest from git, but I see the situation is the same in the 4.2.0 tarball.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20846"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20846" class="active">make a patch please</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2014-09-10 19:36.</div>
    <div class="content">
     <p>Please, can you provide a patch to check all changes made. thanks</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20790"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20790" class="active">Face Reconition, faces not yet confirmed filter</a></h3>    <div class="submitted">Submitted by sfrique (not verified) on Tue, 2014-08-05 18:20.</div>
    <div class="content">
     <p>Is there a way to show all and only these faces that have been detected &amp; recognized by digikam but not yet confirmed by the user?</p>
<p>with a large collection of pictures, it gets hard to confirm new faces!</p>
<p>Here is a filled wish!</p>
<p>Thanks</p>
<p>https://bugs.kde.org/show_bug.cgi?id=336253</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20791"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20791" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Anonymous from Finland (not verified) on Tue, 2014-08-05 21:13.</div>
    <div class="content">
     <p>Thanks for release!<br>
I just installed 4.1.0 yesterday, and I was getting frequent crashes while browsing photos and changing albums.<br>
I was going to file a bug report, but then update was announced and crash -problem disappered! Great timing! ;)<br>
(using Ubuntu 14.04 and digiKam packages from Philips ppa)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20792"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20792" class="active">Attempt to upgrade to digikam4.2.0 failed</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-08-05 23:30.</div>
    <div class="content">
     <p>I have been working with digikam 4.0.0 and having issues with sudden crashes. Today I saw the announcement of digikam v 4.2.0.<br>
Tried to upgrade using the recommended way for kubuntu:<br>
~$ sudo add-apt-repository ppa:kubuntu-ppa/backports<br>
~$ sudo apt-get update<br>
~$ sudo apt-get install digikam<br>
this last command triggered the following message in the terminal:<br>
Reading package lists... Done<br>
Building dependency tree<br>
Reading state information... Done<br>
digikam is already the newest version.<br>
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.<br>
~$<br>
to ensure I had the right version I followed with the command<br>
~$ digikam -v<br>
that triggered the following message<br>
Qt: 4.8.6<br>
KDE Development Platform: 4.13.2<br>
digiKam: 4.0.0</p>
<p>What gives, is it that Kubuntu backports are not up to date?<br>
are there other ways to do it?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20793"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20793" class="active">Can not start digiKam</a></h3>    <div class="submitted">Submitted by snooker (not verified) on Wed, 2014-08-06 20:52.</div>
    <div class="content">
     <p>Hello,</p>
<p>digiKam 4.2 installed on Windows 7 x64. DigiKam can not start. Error message missing cudart32_55.dll. </p>
<p>Best regards</p>
<p>Snooker</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20794"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20794" class="active">I reported the bug on the kde tracking system</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-08-07 06:21.</div>
    <div class="content">
     <p>I experience the same problem as Snooker also on Win7 x64. Uninstalling and reinstalling digikam 4.2 resulted in the same error message. I reported the bug on the kde bug tracking system.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20795"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20795" class="active">Compilation Mistake</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2014-08-07 09:05.</div>
    <div class="content">
     <p>I Accidentally left CUDA support enabled when I built the OpenCV libraries. You probably do not have a NVidia graphics card or a recent enough version of the CUDA libraries. Either way, that was my mistake and it should not have been included. I will rebuild and make a new release in a day or so. The bug you referred to is <a href="https://bugs.kde.org/show_bug.cgi?id=338085">#338085</a>. Very sorry for the troubles!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20799"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20799" class="active">It did solve the issue.</a></h3>    <div class="submitted">Submitted by Joe (not verified) on Fri, 2014-08-08 02:25.</div>
    <div class="content">
     <p>It did solve the issue. Thanks for reissuing the windows installer :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20807"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20807" class="active">I tried to install digikam</a></h3>    <div class="submitted">Submitted by JL (not verified) on Sat, 2014-08-09 21:32.</div>
    <div class="content">
     <p>I tried to install digikam (windows) a few minutes ago but the cudart 32_55.dll file is still missing.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20809"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20809" class="active">The windows test installer,</a></h3>    <div class="submitted">Submitted by JL (not verified) on Sun, 2014-08-10 16:29.</div>
    <div class="content">
     <p>The windows test installer, linked on the bug report, worked fine. Apparently is the KDE repository that need to be updated.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20812"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20812" class="active">Correct</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2014-08-11 09:53.</div>
    <div class="content">
     <p>Yes, you are correct. <strike>I am waiting on the new installer to be mirrored across KDE official hosts so I can link the installer from there, so for now use the link from the bug report. Sorry for the trouble!</strike> Updated installer on KDE servers is now linked in main text.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-20800"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20800" class="active">no smoothing in Preview</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Fri, 2014-08-08 10:25.</div>
    <div class="content">
     <p>No idea if it is due to the Digikam upgrade to 4.2 or something else, but I'm now getting a very grainy image in the preview panel as if it was using some bilinear filtering. At 1:1 it is alright but there is no smoothing for viewing at less than 100%. In Slideshow it works fine again. Anyone else experiencing the same problem?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20801"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20801" class="active">see bug #337231</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2014-08-08 11:15.</div>
    <div class="content">
     <p>With 4.2.0 release we have introduced a new code to handle HQ screen<br>
resolution as retina display from Apple. Entry in bugzilla is this<br>
one :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=337231</p>
<p>We check this code under Linux, OSX and Windows with different screen<br>
sizes, and all work fine.</p>
<p>Perhaps screensize returned by your system is wrong.</p>
<p>We recommend to post a comment in this bugzilla file to have feedback<br>
from original contributor and start investigations...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20802"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20802" class="active">The description doesn't seem</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Fri, 2014-08-08 12:37.</div>
    <div class="content">
     <p>The description doesn't seem to fit. I'm not using OSX but Linux and a standard 24" 1920x1200 display. The pixel size looks right in all modules, at 1:1 it is perfect. It's only the preview image when downscaling uses most likely a bilinear filter instead of some smoothing. Is there a hidden setting somewhere for that?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20804"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20804" class="active">no settings...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2014-08-08 14:50.</div>
    <div class="content">
     <p>there is no settings for that...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20803"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20803" class="active">I am experiencing the same</a></h3>    <div class="submitted">Submitted by Robert Zeller (not verified) on Fri, 2014-08-08 12:38.</div>
    <div class="content">
     <p>I am experiencing the same problem on my system. When "Preview Load Full Image Size" is set to true, I am getting on some photos strange patterns in the preview, which vanish only when I am increasing the preview to 50 or more percent. My photos have a size of 36 Mpx. Obviously the algorithm that reduces the image to the screen size, creates some kind of a moiree structure if the underlying picture has a regular pattern  e.g. a brick wall. This did not happen with DK 4.1.0. Also I can confirm the grainy picture in the preview. These effects disappear if I set "Preview Full Image Size" to false. BUT in that case I am loosing almost totally the high resolution of the photos when taking a 100% view. I am downgrading to DK 4.1.0 .<br>
My Screen is a BenQ flat panel with a resolution of 1920 x 1080. Operating system: openSUSE 13.1</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20805"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20805" class="active">Ditto</a></h3>    <div class="submitted">Submitted by <a href="http://www.londonlight.org/" rel="nofollow">DrSlony</a> (not verified) on Sat, 2014-08-09 09:50.</div>
    <div class="content">
     <p>I am experiencing the same bug on Gentoo, 1920x1080 screen. When no scaling is performed, the image preview is as it should be. When scaling is performed, it's horrible.<br>
Robin: bilinear filtering would result in a smooth image. This looks more like no filtering.</p>
<p>See: https://bugs.kde.org/show_bug.cgi?id=338138</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20806"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20806" class="active">Ah, thank you for filing</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Sat, 2014-08-09 12:29.</div>
    <div class="content">
     <p>Ah, thank you for filing it.</p>
<p>I thought bilinear being the fastest and worst quality filter it would be it, but you're right there's probably no filtering at all</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20808"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20808" class="active">Digikam 4.2 is going very well under Windows</a></h3>    <div class="submitted">Submitted by Piter Dias (not verified) on Sat, 2014-08-09 22:04.</div>
    <div class="content">
     <p>I was a bit upset due to slow Digikam performance in my Linux box (a weak HP Microserver 40L), which was built to provide Samba shares and web serving.<br>
My desktop is a old Intel Q6600 + 4GB + GTX650 + Windows 8 + 1Gbps network. Digikam database is MySQL installed in my Microserver.<br>
The performance is very good, totally usable and I can also play a bit in my notebook over wi-fi as well<br>
Great job. I almost bought Ligthroom some months and regret nothing for moving to Digikam.<br>
Congratulations</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20810"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20810" class="active">Finally, a stable windows build</a></h3>    <div class="submitted">Submitted by Sebastian (not verified) on Sun, 2014-08-10 21:25.</div>
    <div class="content">
     <p>I can absolutely agree, digiKam 4.2 works like a charm under windows, since the OpenCV issues have been resolved. Just remember to get the <a href="http://download.kde.org/stable/digikam/digiKam-installer-4.2.0-win32-2.exe.mirrorlist">digiKam-installer-4.2.0-win32-2.exe</a> (the article still links to the first build).<br>
For me it's the first time that I can use digiKam under Windows without crashes and having access to all the features is just awesome! Keep up the great work :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20811"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20811" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2014-08-11 09:51.</div>
    <div class="content">
     <p>Thank you both for the kind feedback, very glad to hear it is working so well for you!</p>
<p><strike>I have been waiting on the KDE servers to mirror the new installer so I can link it on the official server and will link the official one as soon as its ready (it's identical to the one on Google docs that you linked).</strike></p>
<p>Updated installer on KDE servers is now linked in main article text.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20813"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20813" class="active">No previews, no thumbnails...?</a></h3>    <div class="submitted">Submitted by Adobesucks (not verified) on Mon, 2014-08-11 13:01.</div>
    <div class="content">
     <p>I just installed on Windows 7/64, the newest version without that "cudart" error message. </p>
<p>Installation works fine, everything looks okay. </p>
<p>BUT... I see no thumbnails or previews, all photos are shown as generic icons, even JPGs.</p>
<p>In preview mode I see an error message below the image beside the little icons: "Failed to load image". Well, it does not load one of many that I have, no matter what file format. </p>
<p>What can I do?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20814"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20814" class="active">Please file a bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2014-08-11 15:34.</div>
    <div class="content">
     <p>Please <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=4.2.0&amp;format=guided">file a bug report</a> and attach a screenshot. Please give information like where your photos are physically located (like external hard drive, network, etc), whether you are using a roaming profile (like at a university), what type of database you are using and any other information you think might be relevant. Also, can you click/double-click on the thumbnail to see the full-size image?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20815"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20815" class="active">Latest KDE update in Kubuntu backports breaks digikam install</a></h3>    <div class="submitted">Submitted by ChrisR (not verified) on Tue, 2014-08-12 09:44.</div>
    <div class="content">
     <p>Hello, </p>
<p>since yesterday I've been experiencing the following problem (and it seems I'm not the only one according to #kubuntu on IRC):</p>
<p>The latest backports update in Kubuntu removes digikam (and a whole lot of other apps it seems) due to unmet dependencies. I know this is probably a packaging issue with Kubuntu, but maybe you have an idea...</p>
<p>When I do apt-get install digikam I get:</p>
<p> The following packages have unmet dependencies:<br>
 digikam : Depends: libkgeomap1 (&gt;= 1.0~digikam4.0.0) but it is not going to be installed</p>
<p>I tried resolving the issue manually, but this just results in more dependency issues due to newer versions of other libraries.</p>
<p>Any help would be greatly appreciated.</p>
<p>Keep up the good work!<br>
Chris</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20816"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20816" class="active">I don't know how you update</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2014-08-12 13:01.</div>
    <div class="content">
     <p>I don't know how you update or what other packages and/or PPAs you are using that could make that conflict but try to install/update libkgeomap1 and look what kind of conflicts it gives as it doesn't want to be installed. As for now this is too little information for knowing the root of the problem.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20817"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20817" class="active">So sudo apt-get install</a></h3>    <div class="submitted">Submitted by ChrisR (not verified) on Tue, 2014-08-12 14:15.</div>
    <div class="content">
     <p>So sudo apt-get install libkgeomap1 gives:</p>
<p>The following packages have unmet dependencies:<br>
 libkgeomap1 : Depends: libmarblewidget18 (&gt;= 4:4.9.90) but it is not going to be installed</p>
<p>Then sudo apt-get install libmarblewidget18 yields:</p>
<p>The following packages have unmet dependencies:<br>
 libmarblewidget18 : Depends: libastro1 (= 4:4.13.3-0ubuntu0.1~ubuntu14.04.1~ppa5) but 4:4.13.97-0ubuntu1~ubuntu14.04~ppa2 is to be installed<br>
                     Recommends: marble-plugins (= 4:4.13.3-0ubuntu0.1~ubuntu14.04.1~ppa5) but 4:4.13.97-0ubuntu1~ubuntu14.04~ppa2 is to be installed</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20818"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20818" class="active">I guess then the problem</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2014-08-12 14:32.</div>
    <div class="content">
     <p>I guess then the problem comes from that you are using the new plasma 5 (beta?) from an outside source and the digikam packages you are installing are build to use plasma that comes with kubuntu (backport)? Therefor you get a version conflict with kde and plasma packages and different software depending on different version. I know that my packages are not build against plasma 5 anyway but you are using the kubuntu standard packages of digikam and I guess they don't build against it or either in backports.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20830"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20830" class="active">KDE 4.14.0 available in kubuntu backports</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Wed, 2014-08-20 21:22.</div>
    <div class="content">
     <p>New KDE 4.14.0 is now available in kubuntu backports, replacing libmarblewidget18 with libmarblewidget19. As libkgeomap1 depends on libmarblewidget18 (and digikam depends on libkgeomap1 which can not be installed), would it be possible to check if libkgeomap1 works OK with libmarblewidget19 and enhance the dependency to allow users with KDE 4.14 to continue using digikam?</p>
<p>BTW: I really do admire your job, allowing Linux users to effectively manage large collections of photos in such advanced tool. I am seriously suffering as I can not sort photos of my kids from vacation and share them with family abroad (tags+edit+mass upload) in any other tools than digikam ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20833"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20833" class="active">Does libkgeomap1 works with libmarblewidget19 ?</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Mon, 2014-08-25 14:24.</div>
    <div class="content">
     <p>Could you please verify if libkgeomap1 works with libmarblewidget19 and enhance the dependency list not to stick on libmarblewidget18, which is not available on KDE 4.14 ?<br>
In case of I am wrong in what I wrote, could you verify how this was worked out for libmarblewidget15 or libmarblewidget16, as I do not remember having issue like this.<br>
I am really missing digikam, especially when holidays are gone.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20835"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20835" class="active">It's not just a matter of</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Mon, 2014-08-25 23:41.</div>
    <div class="content">
     <p>It's not just a matter of expanding a dependency list to make it work. All digikam and it's packages need to be rebuilt against KDE 4.14 that are in the kubuntu teams Backports PPA to make it work with that version of KDE and that breaks my Digikam packages for anyone using the KDE 4.13 version that comes with standard (k)ubunt 14.04 instead. For it to work for all users there need to be two different setups och digikam builds and packages (PPAs), one build for people using standard KDE 4.13 that comes with (k)ubuntu 14.04 and one build for people using the Kubuntu teams backports PPA with KDE 4.14.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20838"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20838" class="active">Hi Philip5, thank you for</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Thu, 2014-08-28 14:40.</div>
    <div class="content">
     <p>Hi Philip5, thank you for your answer and the explanation which is understandable also for non-developers.<br>
I have tried to solve the issue on my side by purging backports repository, but there was 500+ packages issues identified if I would continue, so I gave up.<br>
As digikam is using marble libraries for some time already, how was this resolved in the past? I found https://bbs.archlinux.org/viewtopic.php?id=134602, but it doesn't describe the solution. Do digiKam developers know more?<br>
I understand that creating new repo for who-know-how-much users could be too much extra work, but would it be possible for you to prepare build for KDE 4.14 users and send it to backports repository admins? It should not break anything as backports repository users are already using KDE 4.14.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20839"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20839" class="active">It's not that big of a</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2014-08-30 11:01.</div>
    <div class="content">
     <p>It's not that big of a problem really for me to setup packages for kde 4.14. I have done so before and have a PPA that just need to be added for those using kde 4.14. It's just that I haven't uploaded anything there for some time. I just need to setup a virtual machine with kubuntu 14.04 and kde 4.14 to test it first before upload.</p>
<p>If there is demand for it I might be able to do it tomorrow.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20840"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20840" class="active">Oh, that would be great!</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Mon, 2014-09-01 13:14.</div>
    <div class="content">
     <p>That is a great news for all users with KDE 4.14! I think it would even deserve brief article in news section, to inform all users who stops waiting for this to be resolved (I can imagine a lot of people are now returning from holiday, running updates...).<br>
Hope you will have no trouble in the way!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20841"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20841" class="active">Now digikam 4.2 is up there</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2014-09-02 13:16.</div>
    <div class="content">
     <p>Now digikam 4.2 is up there on my kubuntu-backports PPA for(k)ubuntu users with KDE 4.14.x. You need both my "extra" and the "kubuntu-backports" PPA to make it work and it's ment to be used with KDE 4.14.x packages provided by the Kubuntu teams "backports" PPA. If you don't use the kubuntu teams backports PPA then you shouldn't use this kubuntu-backports PPA of mine but only my "extra" PPA.</p>
<p>https://launchpad.net/~philip5</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20842"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20842" class="active">Great job, it works smoothly</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Wed, 2014-09-03 10:25.</div>
    <div class="content">
     <p>Great job, it works smoothly now!<br>
I do encourage anyone with KDE 4.14.x to add Philip's kubuntu-backports repo to have digikam again.<br>
Philip, many thanks to you, especially when considering end of holidays with a lot of new photos, it really helped users like me a lot!<br>
Tomas</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-20847"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20847" class="active">force install of libkgeomap1 to allow digikam install</a></h3>    <div class="submitted">Submitted by <a href="http://alicious.com" rel="nofollow">pbhj</a> (not verified) on Wed, 2014-09-10 21:47.</div>
    <div class="content">
     <p>I remove libmarblewidget18 and forced the install of libkgeomap1 (downloaded from the http://packages.ubuntu.com pages) and then digikam installed without a problem.AFAICT kgeomap allows both googlemaps and marble to be used, presumably the marble part is going to fail - but then for me there are already so many crashes in recent digikams [the sqlite problems on the whole I think] that I probably won't notice ...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20848"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20848" class="active">dpkg force install</a></h3>    <div class="submitted">Submitted by <a href="http://alicious.com" rel="nofollow">pbhj</a> (not verified) on Wed, 2014-09-10 21:49.</div>
    <div class="content">
     <p>Sorry should have said, I used dpkg to force the install or the already downloaded (from website) package:</p>
<p>-$ sudo dpkg --install --force-depends ./libkgeomap1*</p>
<p>couldn't work out how to do that with just apt-get.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20824"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20824" class="active">I removed libastro1 and then</a></h3>    <div class="submitted">Submitted by Micha (not verified) on Wed, 2014-08-13 13:39.</div>
    <div class="content">
     <p>I removed libastro1 and then I was able to install the libmarble, libkgeomap and digikam.  Another problem crept up though: when I start up digikam, it finishes the initialization, shows the main screen with "no active process".  However, at about this time it starts using all available CPU (400%) and grabbing memory very quickly.  When it gets to about 83% of the system memory (and brings the system to practical halt), the system kills it - and I can never know what it is trying to do there with all those resources.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20825"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20825" class="active">confirm,</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Sat, 2014-08-16 09:49.</div>
    <div class="content">
     <p>happened to me as well</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20819"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20819" class="active">Crashes all the time</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-08-12 21:15.</div>
    <div class="content">
     <p>I really like digikam, but unfortunately for me it's been unusable since version 3.5 (approximately).  It crashes non-stop: I can't use it for more than 5 minutes before it crashes.   I even went as far as installing a fresh copy of kubuntu 14.04, with a new home directory to see if this would solve the problems.   Unfortunately, the crashes persist.</p>
<p>It seems that specifically looking at videos, and also browsing the tags makes things worse.</p>
<p>How do I report a bug, or find out what is causing the crashes?  Is there a log file that I can look through?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20820"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20820" class="active">libsqlite UPSTREAM bug...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-08-12 22:00.</div>
    <div class="content">
     <p>Look this entry in bugzilla. It's an upstream problem relevant of SQlite library...</p>
<p>https://bugs.kde.org/show_bug.cgi?id=329697</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20821"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20821" class="active">Digikam Win, Malicious file ?</a></h3>    <div class="submitted">Submitted by Grix (not verified) on Wed, 2014-08-13 01:37.</div>
    <div class="content">
     <p>Chrome is blocking the download from kde</p>
<p>"digiKam-installer-4.2.0-win32-2.exe es un archivo malicioso y Chrome lo ha bloqueado."</p>
<p>It says it's a malicious file and insists when I try to unblock it</p>
<p>Does anyone have the same issue ? is there any problem with the file ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20822"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20822" class="active">Just tried...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2014-08-13 07:00.</div>
    <div class="content">
     <p>...and no problem here to download with Chrome.</p>
<p>Do you have any 3rd party anti-virus working in background ?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20823"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20823" class="active">Chrome being presumptuous</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2014-08-13 09:50.</div>
    <div class="content">
     <p>There is no problem with the file, it is Chrome being annoying. When a program is self-extracting installer like this one and is not hosted on a secure server, Chrome will often assume it is malicious. You can read more about it <a href="http://blog.chromium.org/2012/01/all-about-safe-browsing.html">here</a> and <a href="https://productforums.google.com/forum/#!topic/chrome/r-9JQIboUmc">here</a>.</p>
<p>We could solve this problem by getting a signing certificate, but that is expensive and we don't have the money for it at the moment. As a test, you could see if Chrome still gives you the warning if you download the exact same file from my <a href="https://drive.google.com/file/d/0B4Esh3gAgC9QdzllM2Rzc3Uyb00/edit?usp=sharing"> Google docs</a>. You could also try downloading from a different mirror server as Google may have a problem with that specific host/country, so perhaps a mirror in a European country would eliminate the warning.</p>
<p>You can check that the file hash matches those listed on the <a href="http://download.kde.org/stable/digikam/digiKam-installer-4.2.0-win32-2.exe.mirrorlist">KDE mirrorlist</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20826"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20826" class="active">Digikam window missing "digikam toolbar" ?</a></h3>    <div class="submitted">Submitted by Pascal (not verified) on Sat, 2014-08-16 14:58.</div>
    <div class="content">
     <p>Hello,</p>
<p>I have installed digikam 4.2.0 on Win7 (installer ...win32-2) and after several crashes (systematic problems with caption/tags) I just saw that the "digikam toolbar" (Browse/Album/Tag/.../Export/Settings/Help) that I can see on some example pictures is missing.<br>
Do you know how can I find it ?<br>
I don't think I played with settings, it is a freshly re-installed digikam version...</p>
<p>Thanks,</p>
<p>Pascal</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20827"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20827" class="active">Sorry,
I just found the</a></h3>    <div class="submitted">Submitted by Pascal (not verified) on Sat, 2014-08-16 15:16.</div>
    <div class="content">
     <p>Sorry,</p>
<p>I just found the "Ctrl+M" command...</p>
<p>But I still have these crashes linked to caption/Tag...</p>
<p>Regards,</p>
<p>Pascal</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20828"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20828" class="active">Please file a bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2014-08-16 15:30.</div>
    <div class="content">
     <p>Glad you sorted it out! Sometimes the menu fails to re-appear after entering/exiting fullscreen mode. In future you could also try quitting digiKam, moving your digikamrc file from 'C:\Users\&lt;your user name&gt;\AppData\Roaming\.kde\share\config' and restarting digiKam so it is recreated and seeing if that solves the problem.</p>
<p>As to the crashing, please <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=4.2.0&amp;format=guided">file a bug report</a> and give information like where your photos are physically located (like external hard drive, network, etc), whether you are using a roaming profile (like at a university), what type of database you are using, and any other information you think might be relevant, like minimum steps you take to reproduce the crash and an image that causes the crash.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20829"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20829" class="active">No more tags-linked crash...</a></h3>    <div class="submitted">Submitted by Pascal (not verified) on Sat, 2014-08-16 20:51.</div>
    <div class="content">
     <p>I don't know if it is linked to menu toolbar or because I just made a "fuzzy update" but everything seems ok :)) !<br>
Thanks,<br>
Pascal</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20831"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20831" class="active">Windows vers 4.2.0 Digikam won't resize images </a></h3>    <div class="submitted">Submitted by Garth (not verified) on Mon, 2014-08-25 05:23.</div>
    <div class="content">
     <p>Hi<br>
I was so pleased to discover Digikam running under windows. I first downloaded vers 3.4.0 and it installed fine but the Tools/Resize Images failed on everything I tried (single images and batch).  I then discovered that there was a vers 4.2.0 which I now have running and the resize still does not work.<br>
I normally run Digikam on my home Suse 13.1 Linux platform and love the editor.<br>
I have tried both proportional and non-proportional settings and it fails every image.<br>
This is a result using Proportional (1 din) with 640 size set.</p>
<p>convert -resize 640x640 -filter Bessel -quality 75 -verbose C:/Users/GarthDesktop/Desktop/MyPictures/Geelong 8Aug2014 night/Half Size/DSC_0259.png[0] C:/Users/GarthDesktop/Desktop/MyPictures/Geelong 8Aug2014 night/Half Size//DSC_0259_1.png</p>
<p>Invalid Parameter - 640x640</p>
<p>It seems to me that it hasn't figued out the sizing of the other dimension (eg 480 or whatever).<br>
I have looked through the Configuration setup for the Edit functions and can't see anything I need to set.  In Linux there is nothing to do - the resize just works :-)<br>
Is there anything I can do to sort this out?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20832"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20832" class="active">I have now tried the Batch</a></h3>    <div class="submitted">Submitted by Garth (not verified) on Mon, 2014-08-25 07:04.</div>
    <div class="content">
     <p>I have now tried the Batch Queue manager and that does work - just a long winded way of resizing.<br>
I should have mentioned in my 1st post that I'm running this on a Windows 7 64bit system.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20834"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20834" class="active">Resize tool uses ImageMagick</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2014-08-25 16:09.</div>
    <div class="content">
     <p>For some reason the batch tool seems to use internal methods to resize, while the Tools-&gt;Resize menu is trying to use ImageMagick's <strong>convert</strong> program. On Windows, even with ImageMagick installed, digiKam sees the <strong>convert</strong> program from Windows, which is meant to convert a FAT hard drive to NTFS. Could you <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=4.2.0&amp;format=guided">file a bug report</a>?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20836"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20836" class="active">Bug report submitted - thanks</a></h3>    <div class="submitted">Submitted by Garth (not verified) on Tue, 2014-08-26 01:19.</div>
    <div class="content">
     <p>Bug report submitted - thanks for the help.<br>
Hope this is resoved soon. Batch editing is not often provided in photo editors and of all the programs I have used Digikam is by far the best.  Thanks guys for the work you've done on this.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20837"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20837" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://www.opsis.cl" rel="nofollow">Christian</a> (not verified) on Tue, 2014-08-26 16:43.</div>
    <div class="content">
     <p>Hello!<br>
I just want to thank the team for this outstanding work. Digikam is the only Linux photography management program that fits all my needs.<br>
I'm currently use the stable 3.5.0 version.</p>
<p>Cheers.<br>
Christian</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20844"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/715#comment-20844" class="active">Still (since 4.0.0) when you</a></h3>    <div class="submitted">Submitted by Timophey (not verified) on Fri, 2014-09-05 08:27.</div>
    <div class="content">
     <p>Still (since 4.0.0) when you want to add a face tag, you're presented a combo list of what matches what you're typing, but you cannot click it because mouse click is ignored. You have to select the right tag with arrow keys and press Enter.</p>
<p>Both in album view and in face detection/recognition view.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>