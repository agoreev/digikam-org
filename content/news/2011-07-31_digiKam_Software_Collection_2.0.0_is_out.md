---
date: "2011-07-31T13:28:00Z"
title: "digiKam Software Collection 2.0.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the digiKam Software Collection 2.0.0! The road to version 2.0.0 took more than"
category: "news"
aliases: "/node/616"

---

<a href="http://www.flickr.com/photos/digikam/5964495890/" title="digikam2.0.0 by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6124/5964495890_e0082c5ee3_z.jpg" width="640" height="360" alt="digikam2.0.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the digiKam Software Collection 2.0.0!</p>

<p>The road to version 2.0.0 took <b>more than a year of heavy development</b>. The team proudly announces the first release of the new generation of digiKam. This version features long awaited face detection and recognition, image versioning support, XMP metadata sidecar files support, big improvements in tagging and marking photos, reversed geo-tagging and many other improvements, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.0.0">236 fixed bugs</a>.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.0.0. This release features new export tools to three web services - Yandex.Fotki, MediaWiki and Rajce. The GPSSync plugin now has the ability to do reverse-geocoding. And as usual, <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.0.0">48 bugfixes</a>.</p>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-19950"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19950" class="active">What about non linux stand alone instalers?</a></h3>    <div class="submitted">Submitted by Mikel  (not verified) on Sun, 2011-07-31 14:08.</div>
    <div class="content">
     <p>Hi there!</p>
<p>As usual, thanks a lot for all the effort and work all of you, members of the community put in this superb piece of software.<br>
I don't use as much as I'd like Gnu/Linux; nowadays I'm a MacOSX user. Despite I have tried Digikam on Macports, I must say it's not enough and that's a real pitty, because I love Digikam, but in the last year I have spent much more time compiling and trying Digikam than working on my photos; and it never worked properly. Digikam is not to be blamed, in fact no one is to be blamed; but, it just doesn't fit. So I must ask whether stand alone installers are foreseen for Digikam. I'm mostly interested on MacOsX, but I think that answering this question several times for diferent SO, just will get you tired. So If you could, just do it as for several SO, Win, MacOs, etc?</p>
<p>Recalling my gratefullness, a milion thanks, </p>
<p>Mikel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19958"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19958" class="active">YES, I've been</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-08-01 03:57.</div>
    <div class="content">
     <p>YES, I've been looking/waiting for a Mac OS X binary for literally years. It would be amazing to get an installer for non-Linux OSes, such as Windows and Mac OS X.<br>
By the way, how did you get Macports to compile digiKam? It has never worked for me...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19971"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19971" class="active">macport file is updated...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-08-08 15:35.</div>
    <div class="content">
     <p>digiKam 2.0.0 have been updated to macport project:</p>
<p>http://www.macports.org/ports.php?by=name&amp;substr=digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19985"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19985" class="active">Digikam MacPort never worked for me</a></h3>    <div class="submitted">Submitted by sa (not verified) on Tue, 2011-08-16 20:45.</div>
    <div class="content">
     <p>The installation via MacPorts never worked for me neither. Just tried the new release and it failed again. Missing Digikam since I switched from Linux to OS X ...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19990"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19990" class="active">latest working version for me</a></h3>    <div class="submitted">Submitted by wolf (not verified) on Tue, 2011-08-23 14:45.</div>
    <div class="content">
     <p>latest working version for me was 1.8.0 (but after 4-th attempt), and compilation take more than 12 hours :-(<br>
2.0.0 is now "working", but actually I can't import CR2 files</p>
<p>I really want normal installer for OSX</p>
<p>AND please remove dependency on kdelibs, marble and mysql.</p>
<p>I think, that the quality and stability goes down with KDE4 :-(</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19951"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19951" class="active">Congratulation</a></h3>    <div class="submitted">Submitted by Olivier Becquaert (not verified) on Sun, 2011-07-31 16:00.</div>
    <div class="content">
     <p>Congratulation to all the team. This software is juste great ! indeed the face recognition was a needed feature. Reverse geotagging sounds pretty cool as well.</p>
<p>Can't wait to fully test it.</p>
<p>regards<br>
Olivier</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19961"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19961" class="active">Great</a></h3>    <div class="submitted">Submitted by Tommy S (not verified) on Mon, 2011-08-01 09:13.</div>
    <div class="content">
     <p>Maybe most important feature that face regocnition as now i can easily find all people from my images for easy tagging. and now finally i can suggest digikam to everyone (including my mother!) as everyone want this great feature!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19963"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19963" class="active">face detection works pretty</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-08-01 14:45.</div>
    <div class="content">
     <p>face detection works pretty good<br>
recognition is not working :'(<br>
(philip5 ppa, kubuntu)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19964"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19964" class="active">not yet finalized</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-08-01 14:52.</div>
    <div class="content">
     <p>Face recognition is under developement into libface...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19979"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19979" class="active">I don't understand...the face</a></h3>    <div class="submitted">Submitted by eMerzh (not verified) on Wed, 2011-08-10 13:46.</div>
    <div class="content">
     <p>I don't understand...the face detection works well but digikam does not recognise people from 1 photo to another...<br>
is it normal? is it already implented or not yet?<br>
Anyway this new version is awesome :)</p>
<p>(it'd be great to have some howto use those new functions (vids , doc, ...)</p>
<p>Thanks for you work</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19952"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19952" class="active">Documentation</a></h3>    <div class="submitted">Submitted by jeff (not verified) on Sun, 2011-07-31 16:17.</div>
    <div class="content">
     <p>Is the documentation up-to-date to describe the use of these new features. For example, is there any clear explanation of how to use the XMP side-car support and what the behaviour is when, say, there are files like "img_1234.jpg" and "img_1234.cr2" present?</p>
<p>I look forward to using many of the new features, but I don't want to experiment with some of those features until I know how they will behave.</p>
<p>In the meantime, well done and congratulations on your latest exciting release!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19953"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19953" class="active">will we get a windows</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Sun, 2011-07-31 16:32.</div>
    <div class="content">
     <p>will we get a windows installer or waQs the RC the released version ?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19954"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19954" class="active">Spectacular photo manager</a></h3>    <div class="submitted">Submitted by Kumar (not verified) on Sun, 2011-07-31 17:52.</div>
    <div class="content">
     <p>Congrats on the release of this spectacular photo manager. It's one of the things that made me to finally switch to GNU/Linux and since I work a lot with photos this alone was well worth it. Thanks for working on the software and making it available for free and even opensource. You are the greatest!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19955"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19955" class="active">Probably outdated link</a></h3>    <div class="submitted">Submitted by Xerdomii (not verified) on Sun, 2011-07-31 18:05.</div>
    <div class="content">
     <p>First congrats to this release. I really like digiKam.</p>
<p>Looking for the sources I found a little thing to report:<br>
As kipi-plugins is now inside the digikam tarball, this site:<br>
http://digikam.org/drupal/download?q=download/tarball<br>
is not up to date, it links to<br>
http://sourceforge.net/projects/kipi/files/<br>
but the files there look outdated.</p>
<p>Xerdomii</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19956"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19956" class="active">Alter ihr seid die Geilsten!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-07-31 22:03.</div>
    <div class="content">
     <p>Alter ihr seid die Geilsten! :-) :-) Digikam hooray !!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19957"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19957" class="active">Nothing to say but... CONGRATULATIONS!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Bugsbane</a> (not verified) on Sun, 2011-07-31 22:27.</div>
    <div class="content">
     <p>I always appreciate the hard work and dedication that the Digikam team put into making what has become one of KDE and FOSS in general's flagship products. With many, many gig of photos I don't know how I'd get by without Digikam. Thanks for keeping photography fun. You all rock!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19959"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19959" class="active">New Version - Great but how to install?</a></h3>    <div class="submitted">Submitted by JowiKrause (not verified) on Mon, 2011-08-01 06:11.</div>
    <div class="content">
     <p>Hi!<br>
I'm sure the new version is great. But I am not fit enough in Linux-Ubuntu to get it installed.<br>
Of course there are lots of How-Tos on the IN regarding the installation of Tarballs, but none seems to work for me.<br>
Or do I have to wait until this version get available through Ubuntu?<br>
Cheers<br>
Johann</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19960"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19960" class="active">Hi,
You might get some hints</a></h3>    <div class="submitted">Submitted by falolaf (not verified) on Mon, 2011-08-01 07:23.</div>
    <div class="content">
     <p>Hi,</p>
<p>You might get some hints if you go through the digiKam users mailing list:<br>
https://mail.kde.org/mailman/listinfo/digikam-users</p>
<p>Not on Ubuntu myself but there has been some discussions about ubuntu PPAs there.</p>
<p>/Anders</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19965"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19965" class="active">PPA</a></h3>    <div class="submitted">Submitted by Poborskiii (not verified) on Mon, 2011-08-01 19:31.</div>
    <div class="content">
     <p>PPA with Digikam 2 for Natty with KDE 4.6: <a href="https://launchpad.net/~philip5/+archive/extra">https://launchpad.net/~philip5/+archive/extra</a><a><br>
or rebuild for KDE 4.7 (from Kubuntu Backports PPA): </a><a href="https://launchpad.net/~philip5/+archive/kubuntu-backports">https://launchpad.net/~philip5/+archive/kubuntu-backports</a><a> (for 4.7 need both repository enabled).</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19962"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19962" class="active">digiKam 2.0</a></h3>    <div class="submitted">Submitted by Kamil Stepinski (not verified) on Mon, 2011-08-01 12:25.</div>
    <div class="content">
     <p>Congratulations on achieving this major milestone. It's a real pleasure watching this application grow. Great job team!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19966"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19966" class="active">Windows installer</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-08-03 14:59.</div>
    <div class="content">
     <p>Any plan on releasing a binary installer for windows? it would be much appreciated.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19968"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19968" class="active">done...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-08-08 08:15.</div>
    <div class="content">
     <p>digiKam 2.0.0 Windows installer uploaded to SourceForge.net repository...</p>
<p>https://sourceforge.net/projec?ts/digikam/files/digikam/2.0.0?/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19969"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19969" class="active">The correct URL</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-08-08 14:47.</div>
    <div class="content">
     <p>The correct URL is:</p>
<p>http://sourceforge.net/projects/digikam/files/digikam/2.0.0/</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19970"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19970" class="active">Seems to work, but...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-08-08 15:27.</div>
    <div class="content">
     <p>I just installed the Windows version on a Windows7 64 bit machine.  It seems to have installed fine, except for the fact that no shortcut to digiKam was created.  I had to manually make one pointing to:</p>
<p>"C:\Program Files (x86)\digiKam\KDE4-MSVC\bin\digikam.exe"</p>
<p>But, other than that, so far so good.  Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19972"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19972" class="active">Oops, never mind.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-08-08 15:54.</div>
    <div class="content">
     <p>Oops.  Actually, the program crashes when I try to edit an image with the Image Editor from the preview. This happens even if I run it in Compatibility mode.</p>
<p>Well, actually, it also crashes when mousing over a preview in places such as the Auto-Correction tool.  All in all, it is not yet ready for prime time.  But, it shows a lot of promise.  Of course, in Linux this tool is awesome.</p>
<p>For regular Windows users, at moment, better option is to use the old installed as described here:</p>
<p>http://temporaryland.wordpress.com/2011/01/06/amarok-and-digikam-on-windows/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19973"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19973" class="active">I have not had much issue</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Mon, 2011-08-08 17:36.</div>
    <div class="content">
     <p>I have not had much issue with crashes , but i am on XP SP3 and running digikam 2.0 RC still .</p>
<p>Note , I did find one pic that digikam crashes but only one so far .</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19974"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19974" class="active">It crashes here too.</a></h3>    <div class="submitted">Submitted by Anonymous2 (not verified) on Mon, 2011-08-08 20:27.</div>
    <div class="content">
     <p>I get a lot of crashes too, I'm on win 7 x64 and I work with 10Mpx JPGs, I get the "Digikam has stopped working" window even while simply viewing photos, but more often while tagging faces.<br>
It seems to crash more often than the RC...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19975"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19975" class="active">yes, I didn't really test</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Mon, 2011-08-08 20:56.</div>
    <div class="content">
     <p>yes, I didn't really test this, but going through my pic album I am getting random crashes when the next pic goes to preview mode .</p>
<p>That seems to be one problem with Digikam, its not focused for Win OS's  .</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19976"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19976" class="active">disable preview mode</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Mon, 2011-08-08 22:44.</div>
    <div class="content">
     <p>Update : try disable preview pic and go right into edit window. So far this seems more stable but needs more testing .<br>
BTW I use 12mp Canon 450D/XSI camera .</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-19980"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19980" class="active">Digikam 2.0.0 crashes</a></h3>    <div class="submitted">Submitted by <a href="http://materia.grix.mine.nu" rel="nofollow">Anonymous</a> (not verified) on Thu, 2011-08-11 02:21.</div>
    <div class="content">
     <p>Great program. A must!<br>
But version 2 final for windows crashes in winXP SP3 when loading or editing images and instalation is messy.I Tried with previous Release Candidate and seems to work fine. That makes no sense!<br>
Final win version was tested in three machines with XP home SP3 and crashes in all of them (AMD dual core and Intel singles) Previous version 1.7 was working fine on them.<br>
Great program, can´t live without it</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19981"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19981" class="active">More about version 2.0</a></h3>    <div class="submitted">Submitted by <a href="http://materia.grix.mine.nu" rel="nofollow">Materia Grix</a> (not verified) on Fri, 2011-08-12 02:43.</div>
    <div class="content">
     <p>Let's see: Digikam 2.0 win32 seems to hang on systems with XP. I have solved the problem in a rather clumsy way. Since the RC does not seem to crash but has flaws, like not being able to change the language, after installing the final version I installed the previous RC on top and voila! seems to work. This has to do with the KDE libraries for windows. It's a shame because the win32 version 1.7 works really well. In Linux Ubuntu 11.04 I could not try because I couldn't install from the repositories because of problems with broken packages, and version 1.9 works smoothly. It is a pity that so great a program, have dependency problems with KDE in both environments. I hope there will be solved and I would like to help but I am not a programmer I can only write this and I thank all those involved in the project their great effort and enthusiasm. Thank you all!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19982"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19982" class="active">Portable Version</a></h3>    <div class="submitted">Submitted by Royi (not verified) on Sat, 2011-08-13 09:26.</div>
    <div class="content">
     <p>Hello.<br>
Could you offer a portable version of digikam?</p>
<p>Maybe with the assistance of PortableApps.com.</p>
<p>Thank You.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19983"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19983" class="active">Release plan</a></h3>    <div class="submitted">Submitted by I am not Anonymous (not verified) on Sat, 2011-08-13 12:36.</div>
    <div class="content">
     <p>In your "Release Plan" page there's nothing listed after 2.0.0, do you have any idea about when we can expect a 2.0.1 release?</p>
<p>PS: Am I the only one who can't answer the CHAPTA correctly in less than 5 tries?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19984"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19984" class="active">2.1.0 in september</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2011-08-13 17:28.</div>
    <div class="content">
     <p>there are already more than 40 files closed in bugzilla...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19986"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19986" class="active">Reverse geotagging sounds</a></h3>    <div class="submitted">Submitted by <a href="http://beanbags.shoppershub.co.uk" rel="nofollow">Bean Bags</a> (not verified) on Wed, 2011-08-17 12:59.</div>
    <div class="content">
     <p>Reverse geotagging sounds really cool.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19989"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19989" class="active">Major disapointment</a></h3>    <div class="submitted">Submitted by <a href="http://www.sgvulcan.com" rel="nofollow">icebox</a> (not verified) on Sun, 2011-08-21 11:05.</div>
    <div class="content">
     <p>I'm sorry to say that even if I was eagerly awaiting this release upgrading to it from digikam 2.0beta6 has been a nightmare. I started optimistically by rebuilding and upgrading digikam. It would refuse to start (mysql db). So I tried migrating the database from mysql to sqlite - doesn't work because " Details: columns name, pid are not unique" for the tags table. I'm pretty sure that it's normal for pid not to be unique - different tags can have the same parent. I tried starting from scratch with a new mysql db. Doesn't work. I tried starting from scratch with new sqlite db. I had to erase the config and start over 3-4 times until it finally didn't hang with sql connection fail. </p>
<p>I'm reverting to beta6 hopefully the next versions will be more usable. </p>
<p>Don't get me wrong digikam is the reason I'm finally satisfied with processing my pictures on linux but after all the fun &amp; features the beta brought this stable version is like returning to an alpha.</p>
<p>Thank you for this peace of software and I'm eagerly awaiting a new version!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19996"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/616#comment-19996" class="active">Custom Sort in Icon View?</a></h3>    <div class="submitted">Submitted by Teo76 (not verified) on Wed, 2011-08-31 18:51.</div>
    <div class="content">
     <p>I haven't found any info about custom sort in icon view?<br>
See: http://digikam.1695700.n4.nabble.com/Bug-236249-New-wish-custom-sort-in-album-view-td2124665.html</p>
<p>Digikam is a really great program and I think this is its only flaw. I do really looking forward to you to solve it.</p>
<p>Thanks for your great job!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
