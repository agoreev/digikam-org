---
date: "2011-12-19T11:46:00Z"
title: "Check and Optimize digiKam's Databases"
author: "Dmitri Popov"
description: "By default, digiKam uses two SQLite databases for storing essential data: digikam4.db and thumbnails-digikam.db. And to make the application run fast and smoothly, it’s a"
category: "news"
aliases: "/node/637"

---

<p>By default, digiKam uses two SQLite databases for storing essential data:&nbsp;<em>digikam4.db</em> and <em>thumbnails-digikam.db</em>. And to make the application run fast and smoothly, it’s a good idea to check and optimize the databases every now and then.  <a href="http://scribblesandsnaps.wordpress.com/2011/12/19/check-and-optimize-digikams-databases/">Continue to read</a></p>

<div class="legacy-comments">

</div>