---
date: "2008-04-29T11:26:00Z"
title: "digiKam 0.9.4-beta4 release"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release 0.9.4-beta4. The digiKam tarball can be downloaded from SourceForge as well."
category: "news"
aliases: "/node/312"

---

Dear all digiKam fans and users!<br><br>

The digiKam development team is happy to release 0.9.4-beta4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a> as well.

<br>
<a href="http://www.flickr.com/photos/digikam/2584020910/" title="digikam0.9.4-beta4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3272/2584020910_d6418172a3_m.jpg" width="240" height="183" alt="digikam0.9.4-beta4"></a>
<br>

<h5>NEW FEATURES (since 0.9.3):</h5><br>

<b>General</b>  : Updated internal CImg library to last stable 1.2.8 (released at 2008/04/18).<br>
<b>General</b>  : New search text filter for all metadata sidebar tabs.<br>
<b>General</b>  : New dialog to list all RAW camera supported. A search camera model tool is available to find easy a camera from list.<br>
<b>General</b>  : Creation of tags simplified everywhere. Multiple Tags hierarchy can be created at the same time. Tags creation dialog re-designed.<br>
<b>General</b>  : Color theme scheme are pervasively applied in graphical interfaces, giving digiKam a real Pro-look when dark schemes are selected.<br>
<b>General</b>  : Color theme scheme can be changed from either Editor or LightTable.<br>
<b>General</b>  : Add capability to display items count in all Album, Date, Tags, and Tags Filter folder views. The number of items contained in virtual or physical albums can be displayed next to its name. If a tree branch is collapsed, parent views sum-up the number of items from all undisplayed children views. Items count is performed in background by digiKam KIO-Slaves. A new option from Setup/Album dialog page can toggle on/off this feature.<br><br>

<b>ImageEditor</b> : Raw files can be decoded in 16 bits color depth without to use Color Management.  An auto-gamma and auto-white balance is processed using the same method than dcraw with 8 bits color depth RAW image decoding. This usefull to to have a speed-up RAW workflow with suitable images.<br>
<b>ImageEditor</b> : New saveas image file dialog with photo thumbnail/informations.<br><br>

<b>Showfoto</b> : Added support of color theme schemes.<br>
<b>Showfoto</b> : Added 2 options to setup images ordering with "File/Open Folder" action.<br>
<b>Showfoto</b> : New open image file dialog with photo thumbnail/informations.<br><br>

<b>AlbumGUI</b> : Add a new tool to perform Date search around whole albums collection: Time-Line. Timeline is a new left sidebar tab. It's a great tool complementary to the calendar. Try it out!<br>
<b>AlbumGUI</b> : In Calendar View, selecting Year album shows all images of the full year.<br>
<b>AlbumGUI</b> : New status-bar indicator to report album icon view filtering status.<br>
<b>AlbumGUI</b> : Auto-completion in all search text filter.<br><br>

<h5>BUGS FIXES:</h5><br><br>

001 ==&gt; 146393 : No gamma adjustment when opening RAW file.<br>
002 ==&gt; 158377 : digiKam duplicates downloaded images while overwriting existing ones.<br>
003 ==&gt; 151122 : Opening Albums Has Become Very Slow.<br>
004 ==&gt; 145252 : Umask settings used for album directory, not for image files.<br>
005 ==&gt; 144253 : Failed to update old Database to new Database format.<br>
006 ==&gt; 159467 : Misleading error message with path change in digikamrc file.<br>
007 ==&gt; 157237 : Connect the data when using the left tabs.<br>
008 ==&gt; 157309 : digiKam mouse scroll direction on lighttable.<br>
009 ==&gt; 158398 : Olympus raw images shown with wrong orientation.<br>
010 ==&gt; 152257 : Crash on saving images after editing.<br>
011 ==&gt; 153730 : Too many file format options when saving raw image from editor in digiKam.<br>
012 ==&gt; 151157 : Right and left keys goes to first picture and last picture without shortcuts.<br>
013 ==&gt; 151451 : Application crashed using edit.<br>
014 ==&gt; 139657 : Blueish tint in low saturation images converted to CMYK.<br>
015 ==&gt; 147600 : Showfoto Open folder - files shown in reverse order.<br>
016 ==&gt; 149851 : Showfoto asks if you want to save changes when deleting photo, if changes have been made.<br>
017 ==&gt; 138378 : Showfoto opens file browser window to last the folder used upon startup.<br>
018 ==&gt; 148964 : images are blur only inside shofoto.<br>
019 ==&gt; 146938 : Themes Don't Apply To All Panels.<br>
020 ==&gt; 135378 : Single-click on picture open it in editor.<br>
021 ==&gt; 142469 : Splash screen to be the very first thing.<br>
022 ==&gt; 147619 : Crash when viewing video.<br>
023 ==&gt; 136583 : Album icon does not show preview-image after selecting given image as preview.<br>
024 ==&gt; 129357 : Thumbnails not rotated for (D200)-NEF.<br>
025 ==&gt; 132047 : Faster display of images and/or prefetch wished for.<br>
026 ==&gt; 150259 : media-gfx/digikam-0.9.2 error/typo in showfoto.desktop.<br>
027 ==&gt; 150674 : Turn of live update of preview when moving points in curves dialog or in histogram.<br>
028 ==&gt; 141703 : Show file creation date in tooltip.<br>
029 ==&gt; 128377 : Adjust levels: histograms don't match, bad "auto level" function.<br>
030 ==&gt; 141755 : Sidebar: Hide unused tabs, Add tooltips.<br>
031 ==&gt; 146068 : Mixer does not work for green and blue selected as main channel and monochrome mode.<br>
032 ==&gt; 135931 : Reorganise Batch Processing tools from 'Image' and 'Tools' menus.<br>
033 ==&gt; 159664 : Background color of live/quick filter not updated on folder/date change.<br>
034 ==&gt; 096388 : Show number of images in the album.<br>
035 ==&gt; 155271 : Configure suggests wrong parameter for libjasper.<br>
036 ==&gt; 155105 : Broken png image present in albums folder causes digikam to crash when starting.<br>
037 ==&gt; 146760 : Providing a Timeline-View for quickly narrowing down the date of photos.<br>
038 ==&gt; 146635 : Ratio crop doesn't remember orientation.<br>
039 ==&gt; 144337 : There should be no "empty folders".<br>
040 ==&gt; 128293 : Aspect ratio crop does not respect aspect ratio.<br>
041 ==&gt; 157149 : digiKam crash at startup.<br>
042 ==&gt; 141037 : Search using Tag Name does not work.<br>
043 ==&gt; 158174 : Precise aspect ratio crop feature.<br>
044 ==&gt; 142055 : Which whitebalance is used.<br>
045 ==&gt; 158558 : Delete Function in Tag Filters panel needs to make sure that the tag is unselected when the tag is deleted.<br>
046 ==&gt; 120309 : Change screen backgroundcolor of image tools.<br>
047 ==&gt; 153775 : Download from camera: Renaming because of already existing file does not work.<br>
048 ==&gt; 154346 : Can't rename in digiKam.<br>
049 ==&gt; 154625 : Image-files are not renamed before they are saved to disk.<br>
050 ==&gt; 154746 : Album selection window's size is wrong.<br>
051 ==&gt; 159806 : Cannot display nikon d3 raw files.<br>
052 ==&gt; 148935 : digiKam hot pixel plugin can not read canon RAW files.<br>
053 ==&gt; 158776 : Confirm tag delete if tag assigned to photos.<br>
054 ==&gt; 148520 : Change of rating causes high CPU load.<br>
055 ==&gt; 140227 : Remove or modify D&amp;D tags menu from icon view area.<br>
056 ==&gt; 154421 : Live search doesn't search filenames.<br>
057 ==&gt; 118209 : digiKam hotplug script kde user detection problem.<br>
058 ==&gt; 138766 : JPEG Rotations: Give a warning when doing Lossy rotations (and offer to do lossless when applicable).<br>
059 ==&gt; 156007 : Canon Raw+Jpg Locks up download image window during thumbnail retrieval.<br>
060 ==&gt; 114465 : Simpler entry of tags.<br>
061 ==&gt; 159236 : Corrupted file when downloading from CF card via digiKam.<br>
062 ==&gt; 121309 : Camera list sorted by brand.<br>
063 ==&gt; 137836 : Usability: Configure camera gui add and auto-detect camera.<br>
064 ==&gt; 160323 : Changing the album name from sth to #sth and then to zsth causes the album to be deleted along with the files.<br>
065 ==&gt; 145083 : Space and Shift-Space isn't used for navigation between images.<br>
066 ==&gt; 158696 : digiKam image viewer crashes when "Save As" is clicked.<br>
067 ==&gt; 156564 : Light table crashes when choosing channel.<br>
068 ==&gt; 147466 : digiKam crashes on termination if an audio file played.<br>
069 ==&gt; 146973 : Crashes when clicking on preview image.<br>
070 ==&gt; 148976 : digiKam Deleted 2.3 g of Pictures.<br>
071 ==&gt; 145252 : Umask settings used for album directory, not for image files.<br>
072 ==&gt; 125775 : digiKam way to save and delete "Searches".<br>
073 ==&gt; 150908 : Canon g3 not recognised works version 0.9.1.<br>
074 ==&gt; 152092 : Showfoto crashes ( signal 11 ) on exit.<br>
075 ==&gt; 160840 : Wrong filtering on Album-Subtree-View.<br>
076 ==&gt; 160846 : Blurred preview of pics.<br>
077 ==&gt; 157314 : Zoom-slider has no steps.<br>
078 ==&gt; 161047 : Shift and wheel mouse doesn't work.<br>
079 ==&gt; 161087 : Conflicting directions of mouse scroll when zooming.<br>
080 ==&gt; 161084 : Not properly updates status bar info.<br>
<div class="legacy-comments">

</div>
