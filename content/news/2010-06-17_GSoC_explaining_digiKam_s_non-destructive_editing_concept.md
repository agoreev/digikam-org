---
date: "2010-06-17T11:54:00Z"
title: "GSoC: explaining digiKam's non-destructive editing concept"
author: "Martin Klapetek"
description: "Hi digiKamers, hi KDErs, hi planet, few days ago I blogged about the progress of non-destructive editing in digiKam. According to your comments, I take"
category: "news"
aliases: "/node/526"

---

<p>Hi digiKamers, hi KDErs, hi planet,</p>
<p>few days ago I blogged about the progress of non-destructive editing in digiKam. According to your comments, I take it that there is some misunderstaning in the whole concept. So let me explain it to you :)</p>
<p>The non-destructive editing will be done in such way, that if (and only if) you do some edit to some image, there is automatically created new file, which is a copy of the original with applied changes <strong><em>and</em></strong> with list of edits you've made saved in metadata. Next edit you do, is again applied to the same already-created version and the list of edits in metadata is updated. This is done for example for you to be able to edit the edited file with some external app, let's say GIMP. If there would be only the list of edits stored somewhere, you wouldn't be able to edit it with nothing else until you'd export it from digiKam.</p>
<p>However, when you do some non-reproducible changes, like using some effect, which uses random values for initialization, we need to preserve this exact output, because it can't be reproduced in the same way (thanks for the randomness), so this gets saved again to the same file, but next edit you'll do, will need to go to new file. This will still be presented as one version in digiKam. But there is some ideas about adjusting the nondeterministic filters in such way, that even those could be reproduced when the proper values are stored. This approach would be much better, but I'm not sure if that could be done. But we'll definitely look into it.</p>
<p>About the "Originals" folder. As I wrote, this is supposed to be <strong><em>fully customizable</em></strong>. Think about some photos you take, you pull out the card from your camera, stick it to your PC/notebook/netbook/whatever, do some changes in your digiKam and then you want to fly away with the card to give some presentation. But you want to show only the better versions (the edited ones) and not go through three almost-same pictures in presentation again and again. That's why there will be this option, to move the originals into some other folder, so you can still have them present, but they won't get in your way when not using digiKam. Also think about mobile phones. You connect your phone to your PC, do some changes and then you want to show your friends some photos you take, but again, you don't want to show the originals, you want to show only the better versions. Think about having 100 pictures and that you'll do some edits (autolevels for example) to 50 of them. Then you would have 150 images in your mobile gallery and you would show every second picture twice. Again, that's what the "Originals" folder is for. And again again, it will be customizable. Or optional if you want. So you can just turn it off completely and your images will stay all in the same place.</p>
<p>Then there is the version management. Every edit you do to the original file, will create another version. If you edit the already edited version, the it's simply saved to the already created version file. Only when editing the original you'll get a new version. Or you'll have a possibility to 'fork' it to manually to create a new version. So for example you do several crops to your images. You simply open the original, do one crop, new version is there, saved, ready for other operations. You switch back to the original, you do different crop and another version gets created, again, every other edit you'll do will be saved in this same file with updated metadata. No one-effect-one-file-thing. Then you should be able to display all your versions along. Something like virtual album. So you can see all your cropped images next to them. But not only cropped, imagine you do some color changes or some other effects. Then you can see all your versions next to each other and pick up the best.</p>
<p>Next thing is GUI. There will be some kind of indicator, that you have more than one version, or that you have done some edits and you're able to revert them. The looks of this is not clear yet. But we're open to any proposals or mockups you may have. Also, with every opened version, you'll see a list of changes you've made and you should be able to remove any edit in the list. It would be cool if you could simply change the edit's values and have it reapplied, but that will probably take some more time to accomplish. But I'd like to see that happened.</p>
<p>And if you're thinking about buttons in the editor GUI, Save As... definitely won't disappear. Probably Save button could go away as it will be automatically saved after each applied (meaning you click Ok) effect. Instead of Save button there could be a Fork button, which would create completely new version (copy of the current edited version). Other changes in the UI haven't been decided yet. I'm focusing on the foundation right now, GUI will come later (but soon;)</p>
<p>That's basically how the non-destructive editing will be done in digiKam. Hopefully it's much clearer now to all of you :)</p>
<p>Marty</p>

<div class="legacy-comments">

  <a id="comment-19296"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19296" class="active">Very nice, but may I suggest...</a></h3>    <div class="submitted">Submitted by jsager (not verified) on Thu, 2010-06-17 14:56.</div>
    <div class="content">
     <p>Your approach seems reasonable based on what you are trying to accomplish. I haven't used digiKam because it does not have the non-destructive editing capability. Having this will definitely move me towards its use.</p>
<p>I have a suggestion with respect to moving the original to a different folder. Have you pondered the idea of keeping the original where it is and giving users the option to hide/show the original. It could be both a global default option as well as picture specific. In the hierarchy, when the original is visible, the other versions are displayed as children of the original. When the original is hidden, the children are listed as separate nodes. As you have noted in your blog, there will be some indication of what is original and what is a version of an original. I would prefer to keep my pictures where I put them.</p>
<p>Since I don't currently use digiKam I am not familiar with its abilities so please excuse me if the things I am suggesting already exist in the application.</p>
<p>Another thing that would be nice, since you are storing the edits made to the pictures in metadata, is the ability to copy the settings of one picture to another. Pasting the settings on a original would automatically create a new version of the photo. When pasting the settings on a versioned picture, the user could be presented with the option of replacing the settings, appending them or creating a new version. You get the idea. </p>
<p>Also, instead of using the lable "Fork" you should probably use something more like "Duplicate". I know what the term Fork means only because I follow development efforts on Linux and learned of its meaning that way. However, a general user may not immediately know what that means. Duplicate (or something along that line) is a lot easier for the common user to understand on first glance, without having to consult the documentation for its meaning.</p>
<p>Keep up the great work. I look forward to the inclusion of non-destructive editing in digiKam because I would definitely love to use it for all my photo management and editing needs.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19297"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19297" class="active">Should have looked at the screenshots first...</a></h3>    <div class="submitted">Submitted by jsager (not verified) on Thu, 2010-06-17 15:03.</div>
    <div class="content">
     <p>Disregard my comments regarding how the items would be shown in the hierarchy. I should have looked at the screenshots before commenting. Only folders are shown. However, I'm not sure how you would want to handle the picture count when originals are hidden/shown.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19302"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19302" class="active">Another thing that would be</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 17:07.</div>
    <div class="content">
     <p><cite>Another thing that would be nice, since you are storing the edits made to the pictures in metadata, is the ability to copy the settings of one picture to another. Pasting the settings on a original would automatically create a new version of the photo. When pasting the settings on a versioned picture, the user could be presented with the option of replacing the settings, appending them or creating a new version. You get the idea.</cite></p>
<p>Yeah, that's actually another idea of using the stored changes. But as much as I'd like, I probably won't be able to make it during this GSoC. But if there will be time, I will at least start working on this ;)</p>
<p><cite>Also, instead of using the lable "Fork" you should probably use something more like "Duplicate".</cite></p>
<p>Definitely. "Fork" is for programmers, not for common users. I just used it, you know, like the best fitting one-word that best describes what does it do in terms of version management ;)</p>
<p><cite>Keep up the great work. I look forward to the inclusion of non-destructive editing in digiKam because I would definitely love to use it for all my photo management and editing needs.</cite></p>
<p>Thanks :)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19298"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19298" class="active">clear and robust</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Thu, 2010-06-17 16:07.</div>
    <div class="content">
     <p>A slight concern is how does digiKam "see" changes to a file that has been edited by another application.</p>
<p>Consider face.pgf with history</p>
<p>original =&gt; (desaturate) =&gt; (crop) =&gt; current_image</p>
<p>Then the user edits the image in gimp (say), either</p>
<p>(a) digikam doesn't know anything about it and see's the images history as</p>
<p>original =&gt; (desaturate) =&gt; (crop) =&gt; current_image</p>
<p>...so if the image is recreated from it's history, it will revert to what it was before being edited in gimp. This is clearly "wrong" but might be the only practical approach.</p>
<p>(b) digiKam sees the edited image as a totally new image with no history. In which case if digiKam still has the record of the cropped, desaturated image does it forget that information or re-create that image with a different name, or combine the records in order to do:</p>
<p>(c) digiKam sees that the image has been edited and saves the history as:</p>
<p>original =&gt; (desaturate) =&gt; (crop) =&gt; (edited in external application) =&gt; current_image</p>
<p>This is clearly the most "correct" way of doing it short of the external application recording it's own edits to the metadata.</p>
<p>If the "correct" approach (c) is taken, what happens if the user then edits the image again in digiKam? The history would then be </p>
<p>original =&gt; (desaturate) =&gt; (crop) =&gt; (edited-in-external-application) =&gt; (darkened) =&gt; current_image</p>
<p>however, since digiKam cannot reconstruct what is done in the external application it would need to keep a copy of the original image and the image directly after it was edited by the external application as well as the current state.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19303"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19303" class="active">The way Lightroom does this</a></h3>    <div class="submitted">Submitted by Adam (not verified) on Thu, 2010-06-17 17:07.</div>
    <div class="content">
     <p>The way Lightroom does this is it differentiates clearly between the original images and exported images. The only way to view your images with the edits is either inside Lightroom, or by "exporting" the photo. This to me makes perfect intuitive sense.</p>
<p>Lightroom also handles versioning like an undo stack. Every time you make an edit, a new entry appears (e.x. saturation + 5). By clicking on any action you can revert back to the photo as it was at that point. You can then click back at the top action to get the most recent version, or continue making edits to throw everything on top away.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19307"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19307" class="active">Lightroom has a good idea ...</a></h3>    <div class="submitted">Submitted by Gary (not verified) on Fri, 2010-06-18 00:13.</div>
    <div class="content">
     <p>I was going to make exactly this point, but you beat me to it.</p>
<p>LR does not reproduce the edited image until you have a need to use the image outside of LR.  No problem. Marty's reasoning (you *might* want to use the image outside of DigiKam, so, just in case, let's render and save everything) seems to me to just use up disk space and processing time.  Thousands of LR users are quite happy with their system; it causes no confusion whatsoever.  In fact, it gives a certain sense of safety and wellbeing knowing that there aren't multiple copies of the same image floating around.  I wouldn't hesitate to "steal" this idea: it's a good one.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19308"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19308" class="active">Oh, and by the way, if you</a></h3>    <div class="submitted">Submitted by Gary (not verified) on Fri, 2010-06-18 00:23.</div>
    <div class="content">
     <p>Oh, and by the way, if you want a version of the image in a different format, under M's scheme, you'd have to open up DigiKam and export it anyway.</p>
<p>And I forgot to say that I can't thank you enough for this high quality application.  I'm a BIG fan of Lightroom, but DigiKam has recently gotten close in functionality, and now with non-destructive editing coming ... wow.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19299"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19299" class="active">New Filetype</a></h3>    <div class="submitted">Submitted by Zach L (not verified) on Thu, 2010-06-17 16:29.</div>
    <div class="content">
     <p>I'm no programmer, but would it be possible to create a filetype (for example .dki) that is, in effect, a .zip (or .gz or whatever) file that has the original image, an .xml file showing the changes, and any necessary saved versions (due to effects' randomness).  One could then (in Digikam) click on the file, and in the Editor, click through the different versions.  They could also 'export' any version of the file through the editor (or manually, if they knew what they were doing)</p>
<p>This would prevent having a massive database of dozens of versions of the same image in one's library.</p>
<p>By the way, I love Digikam!  It is a fabulous program!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19300"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19300" class="active">That would address the issue</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Thu, 2010-06-17 17:05.</div>
    <div class="content">
     <p>That would address the issue I mentioned in that in order to edit the file it would either have to be exported (remove it's history) or edited with something that supports the format (whatever that means).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19311"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19311" class="active">The individual files as .png?</a></h3>    <div class="submitted">Submitted by Zach L (not verified) on Fri, 2010-06-18 03:40.</div>
    <div class="content">
     <p>The individual files could be .png.  Again, I'm no programmer, but it seems like writing an import plugin for GIMP would be fairly straightforward.  The plugin could allow you to select which version you want to edit, and then save a .png file in the archive as the newest version, as well as editing the necessary .xml data therein.  </p>
<p>It sounds simple to my non-programming mind, but it could be very complex for all I know.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19313"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19313" class="active">A ZIP format would not really</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Fri, 2010-06-18 10:42.</div>
    <div class="content">
     <p>A ZIP format would not really make any difference to the storage space, as the image files cannot be compressed much more than they already are. Putting them in a ZIP or putting them in a sub-folder are roughly equivalent in terms of organisation, but the latter would be more portable and eliminates the need to uncompress them before viewing, which would slow things down.</p>
<p>An XML-based description of a kind of "edit decision list" can be maintained as an XMP segment within the image (as well as in the database). DigiKam already stores the tags in an XMP segment when you enable the option to save metadata to the image files.</p>
<p>What remains is the ability to associate the files with each other in a "group". There are plenty of suggestions about how this should work in the comments on the various wish-list items in the bug tracking system. I trust that Martin Klapetek has familiarised himself with these and wish him luck in his endeavours to make DigiKam even better for all of us.</p>
<p>Thank you in advance, Marty.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19314"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19314" class="active">An XML-based description of a</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Fri, 2010-06-18 14:09.</div>
    <div class="content">
     <p><cite>An XML-based description of a kind of "edit decision list" can be maintained as an XMP segment within the image (as well as in the database).</cite></p>
<p>Yes, that's exactly how it is done. The format itself is a XML document.</p>
<p><cite>What remains is the ability to associate the files with each other in a "group". There are plenty of suggestions about how this should work in the comments on the various wish-list items in the bug tracking system. I trust that Martin Klapetek has familiarised himself with these and wish him luck in his endeavours to make DigiKam even better for all of us.</cite></p>
<p>I sure am ;)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19301"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19301" class="active">I'm looking forward to</a></h3>    <div class="submitted">Submitted by Vivek (not verified) on Thu, 2010-06-17 17:06.</div>
    <div class="content">
     <p>I'm looking forward to non-destructive editing.  My only concern with the way you've laid it out here is that constantly saving after each effect/correction can slow down the process.  Maybe digikam should have a "save changes for this folder" type option like Picasa does.  (That might also speed up tagging images when writing metadata to files.. hmm)</p>
<p>As another comment pointed out, a tool to show/hide "duplicate" images might be useful.  Digikam can already detect duplicate images, so it might not be hard to create.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19305"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19305" class="active">I like the idee of the</a></h3>    <div class="submitted">Submitted by Pieter (not verified) on Thu, 2010-06-17 22:20.</div>
    <div class="content">
     <p>I like the idee of the 'Originals' folder. It would also be nice to move a photo to the 'Originals' folder without creating a new version (the new version is a delete). I sometimes have multiple similar photo's, I only like to have one photo in my album, but I don't want to throw away the other photos.<br>
Look forward for these features.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19306"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19306" class="active">varying needs</a></h3>    <div class="submitted">Submitted by <a href="http://www.panopixel.org/" rel="nofollow">DrSlony</a> (not verified) on Thu, 2010-06-17 22:34.</div>
    <div class="content">
     <p>Sounds interesting, but remember that digiKam/showFoto is used by people with varying needs - some shoot typical 10 mega pixel photos, others, such as myself, can use it for huge stitched panoramas. I hope that this change will not result in slower performance every time I do something to a 25 000 x 12 500 px image. I also hope that RAM will be used more effectively - despite having 8GB RAM, showFoto still saves images during editing to disk. I could create a RAM drive and use that as temp storage for images being edited, but I'm not sure I can do that with 1.3.0 yet (no ebuild available for Gentoo yet).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19309"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19309" class="active">I'm also concerned about the</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Fri, 2010-06-18 01:02.</div>
    <div class="content">
     <p>I'm also concerned about the performance. I know that saving the file after every edit is not that good and I'll try to do something about it. I have some thoughts about it already, so we'll see how these will work :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19324"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19324" class="active">I don't yet use digiKam, but</a></h3>    <div class="submitted">Submitted by Herbert S. (not verified) on Fri, 2010-06-25 02:59.</div>
    <div class="content">
     <p>I don't yet use digiKam, but I expect to try it out sometime in the future after it suppports non-destructive editing.</p>
<p>My input would be that I don't understand why you're not trying as much as possible to replicate the way Lightroom handles nondestructive editing.   There are a number of reasons I say this:</p>
<p>(1)  Many prospective users for digiKam will be Lightroom users who may be switching to an open source solution or just moving to Linux and looking for a replacement.  It will be easier for them to use if it works like Lightroom.</p>
<p>(2)  Lightroom is a well-designed, well-tested program used by hundreds of thousands of people. The way its non-destructive editing is implemented has passed the test of time.</p>
<p>(3)  Lightroom and its design decisions were heavily tested both inside Adobe and by thousands of beta testers during a lengthy beta program.  I'm not saying you can't come up with something better, but it might be wise to just adopt their model, which is the result of lots of careful thought and feedback.  As one example, if you follow Lightroom's model then you can be pretty sure there's no performance problem (or at least that the performance is likely to be comparable to Lightroom's).</p>
<p>To think you'll come up with something better by just "going with your gut" seems misguided to me, and somewhat like a mistake of youth.  Even if you could come up with something a little bit better in some way, Lightroom is a sort of "standard" and there is value in building according to that standard.</p>
<p>Just my two cents.  I thank you and I will be trying out digiKam because of your non-destructive editing feature, however it gets implemented.  I wish it were being implemented as just a clone of Lightroom's non-destructive editing, though.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19312"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19312" class="active">"Dad" use case</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Fri, 2010-06-18 08:45.</div>
    <div class="content">
     <p>Hello,</p>
<p>this sounds absolutely fabulous! It seems like the current sketch of the design already takes the below into account, but I would just like to suggest that you keep what I call the "dad" use cases in mind while implementing this. Here are the use cases:</p>
<p>1) Dad uses Digikam to manage his photos. Dad is sketchy on file management concepts, so he does not really know about the version control magic going on. But he likes to experiment and try things out. Sometimes he also uses other programs, such as the file manager, to move and copy photos around. Actually, he is not always aware of whether he is using Digikam or Dolphin; his photo folders look quite similar to him in either program.</p>
<p>2) Dad's son tells him: "Go ahead and edit away. As long as you use Digikam, and don't use expressly destructive operations, your original photos will never be touched. Have fun!"</p>
<p>3) Dad edits his pictures in Digikam, and with external tools launched from Digikam. Sometimes he makes clumsy edits of his most favorite photos. The originals are always preserved. When dad's son later comes to visit, the two of them can make a better edit, starting from the original, for large-format printing.</p>
<p>4) Dad tries to edit a picture using Krita, directly. The file is write protected, so he makes a copy first, using Dolphin, and edits that instead. This does no confuse Digikam too much.</p>
<p>5) Dad chooses a destructive action, like "delete" or "drop other versions". A warning is displayed, explaining the difference between hidden versions/photos and truly deleted ones.</p>
<p>6) Dad uses Dolphin and K3b to burn some pictures to a DVD. The edited versions end up on the disc.</p>
<p>7) Dad runs out of hard drive space and moves a bunch of photo folders to an external drive, using the file manager. The edit history for those photos is recoverable.</p>
<p>Basically: a) it should be HARD to screw up your original photos b) the edited versions should be the ones most prominently exposed on the file system.</p>
<p>Again, I think you are already doing all of this, but I thought these use cases could be helpful to keep track of.</p>
<p>Thank you so much for doing this! And good luck!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19318"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19318" class="active">Exiflow filename pattern</a></h3>    <div class="submitted">Submitted by <a href="http://traversin.org" rel="nofollow">Piergi</a> (not verified) on Mon, 2010-06-21 01:25.</div>
    <div class="content">
     <p>I think that keeping track of the versions in the file name could do the trick. I am actually thinking about the Exiflow schema (exiflow.sourceforge.net/).</p>
<p>The file name is something like 20100620-c123456-xy100.jpg, where the -xy100 part says «this is version 1.0.0 made by xy».</p>
<p>This way one can group photos in the same directory, or export/slideshow/whatever everything but the last version in a relatively easy way. </p>
<p>At the same time it should be easy to move the oldest version to a separate folder if one wish so.</p>
<p>As a (soon-to-be ex-)user of f-spot, I am concerned about file naming policies: f-spot uses by default irritating Windows-like names (i.e. «IMG_1234 (Modified).jpg») polluted with white spaces and «()» that make any script a PITA.</p>
<p>The version control is in my opinion a must-have feature.</p>
<p>(The other one in the wish-list would be the «Hidden» tag . . .)</p>
<p>Thanks for this great piece of software, by the way!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19320"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/526#comment-19320" class="active">The F-Spot way of managing versions is OK, yet...</a></h3>    <div class="submitted">Submitted by Mathieu Avoine (not verified) on Wed, 2010-06-23 18:34.</div>
    <div class="content">
     <p>The F-Spot way of managing versions isn't so bad, and it is very intuitive. It works as a stack where only the "current" version is visible. You can change which one you consider the best to make it appear on top whenever you want. The original is clearly identified so it's easy to revert back to it. </p>
<p>I agree totally with Piergi that the naming of versions is really bad. I'd prefer to have the new version files named with the original name plus a version number, ex. IMG_0453.jpg becomes IMG_0453_v1.jpg for the first edit. The "Edited in the GIMP" information should be metadata in the database (and maybe in the comment field in the file metadata) that you use to display in the stack, like: IMG_0453_v1.jpg (Edited in the GIMP), or something similar. After some edits, the version selection box would look like:</p>
<p>IMG_0453.jpg (Original)<br>
IMG_0453_v1.jpg (Edited in the GIMP)<br>
IMG_0453_v2.jpg (Desaturate)<br>
IMG_0453_v3.jpg (Contrast)</p>
<p>Regarding the fact that is actually creates a new file on every edit, sounds OK to me. After all, I'd like to have full control over my photos (originals AND versions), and not depend on usage of a particular piece of software. I'd like to be able to see all my versions in the file browser, and open them all in another software (ex. gThumb) without having to export them to a temporary folder.</p>
<p>Hard drives are quite cheap nowadays anyway, so regarding saving disk space is very much a matter of taste and budget. I'd give to ability to the user to decide whether or not he/she is willing to use extra disk space to store several copies of the "same" photo, or prefer to save disk space and use only 1 edited version that compounds all the edits, but then he/she would lose the ability to undo the changes.</p>
<p>Math</p>
         </div>
    <div class="links">» </div>
  </div>

</div>