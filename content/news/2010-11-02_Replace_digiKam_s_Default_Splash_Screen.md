---
date: "2010-11-02T10:56:00Z"
title: "Replace digiKam's Default Splash Screen"
author: "Dmitri Popov"
description: "Don't fancy digiKam’s default splash screen? No problem, you can easily replace it with your own photo. First off, you need to prepare your own"
category: "news"
aliases: "/node/546"

---

<p>Don't fancy digiKam’s default splash screen? No problem, you can easily replace it with your own photo. First off, you need to prepare your own photo for use in the splash screen. To do this, use an image editing application like the GIMP to resize the photo you want to 500×307 pixels. Save the resized image in the PNG format. Next, grab the splash-digikam.svgz file from digiKam's source code repository and open the downloaded file in the Inkscape vector graphics editor. <a href="http://scribblesandsnaps.wordpress.com/2010/11/02/replace-the-default-digikams-splash-screen/">Continue to read</a></p>

<div class="legacy-comments">

</div>