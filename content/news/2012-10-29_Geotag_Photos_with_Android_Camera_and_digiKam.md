---
date: "2012-10-29T09:16:00Z"
title: "Geotag Photos with Android Camera and digiKam"
author: "Dmitri Popov"
description: "Thanks to digiKam's geocorrelation capabilities, you can geotag photos using a GPX file created with apps like Open GPS Tracker. But there is also another"
category: "news"
aliases: "/node/671"

---

<p>Thanks to digiKam's geocorrelation capabilities, you can geotag photos using a GPX file created with apps like <a href="http://code.google.com/p/open-gpstracker/">Open GPS Tracker</a>. But there is also another way to use your Android device for geotagging. The built-in camera app of most Android devices is capable of geotagging photos. This means that you can take a geotagged snap with the Android camera and then transfer geographical coordinates from it to other photos using digiKam. So next time, when you are done shooting with your main camera, remember to take a reference snapshot with your Android device (make sure that the geotagging option is enabled).</p>
<p><a href="http://scribblesandsnaps.files.wordpress.com/2012/09/digikam-copygeocoords.png"><img class="size-medium wp-image-3025" title="digikam-copygeocoords" alt="" src="http://scribblesandsnaps.files.wordpress.com/2012/09/digikam-copygeocoords.png?w=500" height="380" width="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2012/10/29/geotag-photos-with-android-camera-and-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>